# The Narrator [Web Server]

## Discord Bot Setup

### **Invite the bot to your server** 
* [`Located Here`](https://discordapp.com/oauth2/authorize?client_id=384543567034449920&scope=bot)

### **Setup Permissions**
* Create a Bot role that can do the following.  _Rember to add the role to the Narrator!_
#### BotRole Permissions
* **Manage Roles**
  * Can create the _Living_ role which allows people to talk only if they're alive, removing it if they die
  
#### HomeChannel Permissions (_the home channel of the bot_)
* **Manage Messages**
  * Delete other's messages, for less spam.  Like vote, prefer, or basic chat enforcement _(to be added)_
* **Create Instant Invite**
  * For ease of use going back to the main chat channel, and for sending out invites to people subscribed.
* **Read Message History**
  * Partially to keep a changing timer on how long nighttime is
* **Embed Links**
  * Allows it to pretty print game info, vote count, deaths, you name it
* **Send Messages**
  * Silly, but because the bot stops @Everyone from talking in the channel, it'll also stop itself.  Grant it the power to always talk in this channel
* **Manage Webhooks**
  * Create webhooks enabling a synchronized chat between discord and the UI

> I'd also recommend blocking the bot from talking in the other channels.  Otherwise people can start games anywhere.

## Dev Setup
1. clone the repo
`git clone git@gitlab.com:the-narrator/NarratorNode.git`

### Create Config
#### Install jq 
* for mac, this can be done using `brew install jq`
* for windows, this can be done using `choco install jq`

#### Add Environment variables:
Next, we're going to add some environment variables that the backend needs.
 Add the following lines to ~/.bash_profile.  End of the file is fine.
* `export NARRATOR_CONFIG_LOCATION=<REPO_LOCATION>/config.json` # full path to this repo's location
* `export NARRATOR_ENVIRONMENT=local` # instructs the backend how to use the config 

run `source ~/.bash_profile` when you're done

#### Create config file
Now, we're going to need to create the config file that the narrator will reference. 
* for mac, run sh scripts/create_config.sh
* for windows, cd into scripts and run create_config.bat
1. If you're on windows, make sure you update config.json's `pc_env` to be true.

### Java Setup
1. install java sdk.  You'll need this to compile the files
* I use 1.8

2. Temporarily comment or delete out the flyway block in `build.gradle`
3. Open Eclipse
4. Go to File -> Import -> Gradle> Existing Gradle Project
5. Click next/finish.  For project directory, set it as the narrator repo directory.  All the defaults should be fine
8. Run LogicTests as Junit by right clicking on LogicTests.java in the Package explorer.  It'll angrily fail.  That's okay.
9. Open run configurations by clicking the little black arrow by the green triangle.
10. Go to Junit -> Logic Tests.  Click the environment tab.  Input a new variable.  The name being `NARRATOR_CONFIG_LOCATION` and the value being the location of config.json
11. Logic tests should now run! 

### Node Setup

1. Download nodejs (currently on 12.18.3)
* for mac, I recommend installing nvm https://github.com/nvm-sh/nvm#installing-and-updating
* for pc, https://nodejs.org/en/download/
2. run `npm install`

### DB Setup
1. Install mariadb 
* for mac: https://mariadb.com/kb/en/library/installing-mariadb-on-macos-using-homebrew/
* for pc: https://chocolatey.org/packages/mariadb
* go ahead and follow all the steps until you log in
2. run `sudo mysql_secure_installation`
3. Create a database called `narrator` by opening up a mariadb terminal, and running `CREATE DATABASE narrator;`
4. Install flyway 
* for mac it's `brew install flyway`
* for pc, visit https://flywaydb.org/
5. Add the following to your environment variables, just like you did with `NARRATOR_CONFIG_LOCATION`
* DB_USERNAME -> your db username
* DB_PASSWORD -> your db password
* DB_HOSTNAME -> localhost
* DB_NAME -> narrator
6. Run database migrations `npm run cleanMigrate` or `npm run cleanMigrateWindows`
7. Open up config.json, and edit the following keys (they should be at the bottom of this file)
* db_username -> your db username
* db_password -> your db password
* db_hostname -> localhost
* db_name -> narrator
8. Run NodeTests with Junit by right clicking on NodeTests.java in the Package explorer.  It'll fail.  That's okay
9. Open run configurations by clicking the little black arrow by the green triangle.
10. Go to Junit -> Node Tests.  Click the environment tab.  Input a new variable.  The name being `NARRATOR_CONFIG_LOCATION` and the value being the location of config.json
11. Node Tests should now work!

### Miscellaneous setup
1. You'll need a copy of the firebase key.  Here's one way how to get it
* `scp ${SERVER_USERNAME}@narrator.systeminplace.net:/home/voss/prod/firebase.json .` to get firebase file

1. Whenever you make a change to the java files, you'll need to compile them so that node can use them
* sh scripts/compile.sh

# You've finishined setting up the backend for Narrator!

1. Run `npm run dev` to run it.  You'll need to spin up the front end to see the UI.

### How to extract backup sql data
1. ssh into prod
2. verify mariadb docker instance is running `docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:10.1.34`
3. get container id `docker ps -a`
4. start container `docker start <id>`
5. bash into mariadb docker `docker exec -it <id> bash`
6. dump database `mysqldump -h <db hostname> -u <db_user> narrator -p > backup.sql`
7. bring backup into server files `docker cp <id>:/backup.sql backup.sql`
8. copy backup to local `scp <server>:/home/voss/backup.sql .`
9. copy database into local `mysql -u root narrator < backup.sql`
```

const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');


async function create(userID){
    const args = { userID };
    const javaResponse = await gretelClient.sendRequest('POST', 'moderators', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function del(userID, playerName){
    playerName = playerName || 'oldmoddy';
    const args = { userID, playerName };
    const javaResponse = await gretelClient.sendRequest('DELETE', 'moderators', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function repick(gameID, userID, repickTarget){
    const app = require('../app');
    const activeUserIDs = await app.browserWrapper.getActiveGameUsersByUserID(gameID);
    repickTarget = repickTarget || '';
    const args = { activeUserIDs: [...activeUserIDs], userID, repickTarget };
    const javaResponse = await gretelClient.sendRequest('POST', 'moderators', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);

    // StringChoice sc = new StringChoice(repicker.player);
    // sc.add(repicker.player, "You");
    // StringChoice sc2 = new StringChoice(hostName);
    // sc2.add(host.player, "you");
    // StringChoice have = new StringChoice("has");
    // have.add(repicker.player, "have");
    //
    // PlayerList webUsers = game.getWebUsers();
    // webUsers.remove(repicker.player);
    // webUsers.warn(sc, " ", have, " voted to repick ", sc2, ".");
    //
    return javaResponse.response;
}

module.exports = {
    create,
    delete: del,
    repick,
};

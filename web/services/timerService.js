const config = require('../../config');


const joinIDToTimeout = {};

function setGameExpiration(joinID){
    const { timeout } = config.discord; // TODO change to a non discord timeout value

    const milliseconds = (new Date()).getTime();
    // if(!joinIDToTimeout[joinID])
    // setTimeout(onTimeout.bind(null, joinID), timeout);
    joinIDToTimeout[joinID] = milliseconds + timeout;
}

function stopTimeout(joinID){
    delete joinIDToTimeout[joinID];
}

module.exports = {
    setGameExpiration,
    stopTimeout,
};

// function onTimeout(joinID){
//     const cTime = (new Date()).getTime();
//     const remTime = joinIDToTimeout[joinID]; // when it ends
//     if(!Number.isInteger(remTime))
//         return;
//     if(cTime >= remTime){
//         const gameService = require('./gameService');
//         gameService.deleteGame(joinID);
//     }else{
//         setTimeout(onTimeout.bind(null, joinID), remTime - cTime);
//     }
// }

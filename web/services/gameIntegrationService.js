const integrationTypes = require('../models/enums/integrationTypes');

const util = require('../utils/helpers');


async function getIntegrations(gameID){
    const integrationResults = await Promise.all(
        integrationTypes.getAll().map(async integrationType => {
            const channel = util.getWrapper(integrationType);
            return [integrationType, await channel.hasIntegration(gameID)];
        }),
    );
    return integrationResults
        .filter(([, result]) => result)
        .map(([integrationType]) => integrationType);
}

module.exports = {
    getIntegrations,
};

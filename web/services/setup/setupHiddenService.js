const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const hiddenRepo = require('../../repos/hiddenRepo');
const setupHiddenRepo = require('../../repos/setupHiddenRepo');


async function add(userID, setupHidden){
    const isEditable = await hiddenRepo.isEditable([setupHidden.hiddenID]);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const args = {
        userID,
        ...setupHidden,
    };
    const javaResponse = await gretelClient.sendRequest('POST', 'setupHiddens', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    require('../eventService').setupHiddenAdd(javaResponse.response);
    return javaResponse.response;
}

function addV2(userID, setupHidden){
    return setupHiddenRepo.create(userID, setupHidden);
}

async function remove(userID, setupHiddenID){
    const isEditable = await setupHiddenRepo.isEditable([setupHiddenID]);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const args = {
        userID,
        setupHiddenID,
    };
    const javaResponse = await gretelClient.sendRequest('DELETE', 'setupHiddens', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    require('../eventService').setupHiddenRemove({
        gameID: javaResponse.response.gameID,
        setupHiddenID,
    });
}

module.exports = {
    add,
    addV2,
    remove,
};

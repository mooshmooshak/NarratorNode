const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const factionRepo = require('../../repos/factionRepo');


async function create(userID, args){
    const setupService = require('./setupService');
    await setupService.ensureEditable(args.setupID, userID);
    args = Object.assign({}, args, { userID });
    const response = await gretelClient.sendRequest('POST', 'factions', args);
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
    const eventService = require('../eventService');
    await eventService.factionCreate(response);
    return response.response;
}

async function updateFaction({
    description, factionID, name, setupID,
}){
    const args = {
        description,
        factionID,
        name,
        setupID,
    };
    const response = await gretelClient.sendRequest('PUT', 'factions', args);
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
}

async function deleteFaction(args){
    await ensureEditableByUser(args.userID, args.factionID);
    const response = await gretelClient.sendRequest('DELETE', 'factions', args);
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
    const eventService = require('../eventService');
    await eventService.factionDelete(response);
    return response.response;
}

async function addAbility(userID, factionID, abilityName){
    await ensureEditableByUser(userID, factionID);
    const response = await gretelClient.sendRequest('POST', 'factions',
        { userID, factionID, abilityName });
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
    return response.response;
}

async function deleteAbility({ abilityID, factionID }){
    const response = await gretelClient.sendRequest('DELETE', 'factions',
        { abilityID, factionID });
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
}

async function addEnemy(userID, factionID, enemyID){
    await ensureEditableByUser(userID, factionID);
    const response = await gretelClient.sendRequest('POST', 'factions',
        { userID, factionID, enemyID });
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
    return response.response;
}

async function deleteEnemy({ factionID, enemyFactionID, setupID }){
    const response = await gretelClient.sendRequest('DELETE', 'factions',
        { enemyFactionID, factionID, setupID });
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
}

async function addSheriffCheckable(userID, factionID, checkableID){
    await ensureEditableByUser(userID, factionID);
    const response = await gretelClient.sendRequest('POST', 'factions',
        { userID, factionID, checkableID });
    if(response.errors.length)
        throw httpHelpers.httpError(response.errors, 422);
    return response.response;
}

async function ensureEditableByUser(userID, factionID){
    const { ownerID, isEditable } = await factionRepo.getEdibilityDetails(factionID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    if(ownerID !== userID)
        throw httpHelpers.httpError('This setup is not editable by this user.', 403);
}

module.exports = {
    create,
    updateFaction,
    deleteFaction,
    addAbility,
    deleteAbility,
    addEnemy,
    deleteEnemy,
    addSheriffCheckable,
    ensureEditableByUser,
};

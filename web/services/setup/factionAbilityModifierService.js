const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const factionAbilityRepo = require('../../repos/factionAbilityRepo');


async function upsert(userID, factionAbilityID, args){
    const isEditable = await factionAbilityRepo.isEditable(factionAbilityID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    args = { ...args, userID, factionAbilityID };
    const javaResponse = await gretelClient.sendRequest('POST', 'factionAbilityModifiers', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

module.exports = {
    upsert,
};

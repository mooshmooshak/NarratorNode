const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const factionRoleRepo = require('../../repos/factionRoleRepo');


async function create(userID, args){
    const factionService = require('./factionService');
    await factionService.ensureEditableByUser(userID, args.factionID);
    args = Object.assign({}, args, { userID });
    const javaResponse = await gretelClient.sendRequest('POST', 'factionRoles', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function get(userID, factionRoleID){
    const javaResponse = await gretelClient.sendRequest('GET', 'factionRoles',
        { userID, factionRoleID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function update(userID, factionRoleID, args){
    if(!Object.keys(args).length)
        return;
    const isEditable = await factionRoleRepo.isEditable(factionRoleID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    if(factionRoleID === args.receivedFactionRoleID)
        args.receivedFactionRoleID = null;
    const javaResponse = await gretelClient.sendRequest('PUT', 'factionRoles',
        { ...args, factionRoleID, userID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
}

async function delFactionRole(userID, factionRoleID){
    const isEditable = await factionRoleRepo.isEditable(factionRoleID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const javaResponse = await gretelClient.sendRequest('DELETE', 'factionRoles',
        { userID, factionRoleID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
}

module.exports = {
    create,
    get,
    update,
    delete: delFactionRole,
};

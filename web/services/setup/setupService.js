const { minBy } = require('lodash');

const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const { MAX_PLAYER_COUNT } = require('../../utils/constants');

const setupFeaturedRepo = require('../../repos/setupFeaturedRepo');
const setupIterationRepo = require('../../repos/setupIterationRepo');
const setupIterationSpawnRepo = require('../../repos/setupIterationSpawnsRepo');
const setupRepo = require('../../repos/setupRepo');

const sqlErrors = require('../../repos/util/sqlErrors');


async function getByID(setupID){
    const args = {
        setupID,
    };
    const javaResponse = await gretelClient.sendRequest('GET', 'setups', args);
    return javaResponse.response;
}

async function getDefaultSetupID(userID){
    const featuredSetups = await getFeaturedSetups();
    if(featuredSetups.length)
        return minBy(featuredSetups.filter(s => s.id), 'priority').id;
    const setup = await create(userID, {
        name: 'Default setup',
        isDefault: true,
    });
    const setupHiddenService = require('./setupHiddenService');
    const tpr = {
        hiddenID: setup.hiddens.find(h => h.name === 'Town Random').id,
        isExposed: false,
        mustSpawn: false,
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
    };
    const mpr = {
        hiddenID: setup.hiddens.find(h => h.name === 'Mafia Random').id,
        isExposed: false,
        mustSpawn: false,
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
    };
    await setupHiddenService.addV2(userID, tpr);
    await setupHiddenService.addV2(userID, tpr);
    await setupHiddenService.addV2(userID, tpr);
    await setupHiddenService.addV2(userID, tpr);
    await setupHiddenService.addV2(userID, tpr);
    await setupHiddenService.addV2(userID, tpr);
    await setupHiddenService.addV2(userID, mpr);
    await setupHiddenService.addV2(userID, mpr);
    await setPriority(setup.id, 100, 'key');
    return setup.id;
}

function getFeaturedSetups(){
    return setupRepo.getFeaturedSetups();
}

async function getIterations(setupID, playerCount){
    const javaResponse = await gretelClient.sendRequest('GET', 'setups', { playerCount, setupID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

function getUserSetups(userID){
    return setupRepo.getUserSetups(userID);
}

async function setPriority(setupID, priority, key){
    try{
        await setupFeaturedRepo.setPriority(setupID, priority, key);
    }catch(err){
        if(err.errno === sqlErrors.DUPLICATE_ROW)
            throw httpHelpers.httpError([err.sqlMessage], 422);
        throw err;
    }
    return setupRepo.setUneditable(setupID);
}

async function setSetup(args){
    const javaResponse = await gretelClient.sendRequest('PUT', 'setups', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    require('../eventService').setupChange(javaResponse.response);
    return javaResponse.response;
}

async function create(userID, setup){
    const args = {
        setup,
        userID,
    };
    const javaResponse = await gretelClient.sendRequest('POST', 'setups', args);
    return javaResponse.response;
}

async function clone(setupID, userID){
    const args = {
        setupID,
        userID,
    };
    const { errors, response } = await gretelClient.sendRequest('POST', 'setups', args);
    if(errors.length)
        throw httpHelpers.httpError(errors, 422);
    return response;
}

async function createIteration(setupID, iteration, authorID){
    const iterationID = await setupIterationRepo.createIteration(setupID,
        iteration.spawns.length,
        authorID);
    return setupIterationSpawnRepo.createSpawns(iterationID, iteration.spawns);
}

async function deleteSetup(setupID, userID){
    await setupRepo.deleteEditableSetups(setupID, userID);
    return setupRepo.deactivateNoneditableSetups(setupID, userID);
}

async function ensureEditable(setupID, userID){
    const setup = await setupRepo.getByID(setupID);
    if(!setup.isEditable)
        throw httpHelpers.frozenSetupError();
    if(setup.ownerID !== userID)
        throw httpHelpers.httpError('This setup is not editable by this user.', 403);
}

async function onSetupChange(setupID){
    const gameService = require('../gameService');

    const eventService = require('../eventService');
    const games = await gameService.getUnstartedBySetupID(setupID);
    games.forEach(game => eventService.setupChange(game));
}

async function removeFeatured(setupID){
    await setupFeaturedRepo.removeBySetupID(setupID);
    await setupRepo.setEditable(setupID);
}

module.exports = {
    clone,
    create,
    createIteration,
    getByID,
    getDefaultSetupID,
    getFeaturedSetups,
    getIterations,
    getUserSetups,
    setPriority,
    setSetup,
    deleteSetup,
    ensureEditable,
    onSetupChange,
    removeFeatured,
};

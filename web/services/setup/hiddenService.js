const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const hiddenRepo = require('../../repos/hiddenRepo');
const setupRepo = require('../../repos/setupRepo');


async function create(userID, request){
    const setup = await setupRepo.getByID(request.setupID);
    if(!setup.isEditable)
        throw httpHelpers.frozenSetupError();
    request.userID = userID;
    const javaResponse = await gretelClient.sendRequest('POST', 'hiddens', request);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function createV2(ownerID, setupID, hiddenName){
    const setupService = require('./setupService');
    await setupService.ensureEditable(setupID, ownerID);
    return hiddenRepo.create(setupID, hiddenName);
}

async function deleteHidden(userID, hiddenID){
    const isEditable = await hiddenRepo.isEditable([hiddenID]);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const javaResponse = await gretelClient.sendRequest('DELETE', 'hiddens', { userID, hiddenID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
}

module.exports = {
    create,
    createV2,
    deleteHidden,
};

const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');

const integrationTypes = require('../models/enums/integrationTypes');

const lobbyChatRepo = require('../repos/lobbyChatRepo');


function getLobbyMessages(gameID){
    return lobbyChatRepo.get(gameID);
}

async function getUserChat(gameID, userID){
    return (await gretelClient.sendRequest('GET', 'chats', { gameID, userID })).response;
}

async function say(userID, chatKey, text, source = integrationTypes.BROWSER, args = {}){
    const { errors } = await gretelClient.sendRequest('POST', 'chats', {
        args,
        chatKey,
        source,
        text,
        userID,
    });
    if(errors.length)
        throw httpHelpers.httpError(errors, 422);
}

async function sendLobbyMessage(gameID, userID, { text }, source, args = {}){
    const messageID = await lobbyChatRepo.createMessage({ gameID, text, userID });
    const response = {
        id: messageID,
        createdAt: new Date(),
        text,
        userID,
    };
    const eventService = require('./eventService');
    await eventService.lobbyChatMessage({
        ...response,
        args,
        gameID,
        source,
    });

    return response;
}

module.exports = {
    getLobbyMessages,
    getUserChat,
    say,
    sendLobbyMessage,
};

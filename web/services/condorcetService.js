const gretelClient = require('../utils/gretelClient');


async function resolveInput(votes){
    const args = {
        mayorResolve: false,
        votes,
    };
    const serverResponse = await gretelClient.sendRequest('GET', 'condorcet', args);
    return serverResponse.response;
}

module.exports = {
    resolveInput,
};

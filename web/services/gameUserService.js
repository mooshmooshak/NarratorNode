const gameUserRepo = require('../repos/gameUsersRepo');

const httpHelpers = require('../utils/httpHelpers');


async function assertIsModerator(gameID, userID, errorMessage){
    const gameUsers = await gameUserRepo.getByGameID(gameID);
    const moderator = gameUsers.find(user => user.userID === userID && user.isModerator);
    if(!moderator)
        throw httpHelpers.httpError([errorMessage], 403);
}

module.exports = {
    assertIsModerator,
    ensureModerator,
};

async function ensureModerator(userID, gameID){
    const users = await gameUserRepo.getByGameID(gameID);
    const moderator = users.find(user => user.isModerator && user.userID === userID);
    if(!moderator)
        throw httpHelpers.httpError('This user is not allowed to start the game.', 403);
}

const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');

const gamePhase = require('../models/enums/gamePhase');

const gameRepo = require('../repos/replayRepo');


const joinIDToEndTime = {};

function getEndTime(joinID){
    return joinIDToEndTime[joinID].endTime;
}

async function getPercentTimeLeftByUserID(userID){
    const joinID = await gameRepo.getJoinIDByUserID(userID);
    return getPercentTimeLeftByJoinID(joinID);
}

function getPhaseLength(joinID){
    return joinIDToEndTime[joinID].phaseLength;
}

function setExpirationFromEvent(request){
    const secondsRemaining = request.phase.length;
    if(joinIDToEndTime[request.joinID])
        stopOldTimeouts(joinIDToEndTime[request.joinID]);

    if(secondsRemaining <= 0)
        return endPhaseNow(request.joinID);
    if(!secondsRemaining)
        return new Date().getTime();

    const millisecondsRemaining = secondsRemaining * 1000;
    const timeLeft = 'timeLeft' in request.phase ? request.phase.timeLeft : 1;
    const newEndTime = new Date().getTime() + (millisecondsRemaining * timeLeft);

    joinIDToEndTime[request.joinID] = {
        endTime: newEndTime,
        phaseLength: secondsRemaining,
        phaseSubEvents: getPhaseSubEvents(request, millisecondsRemaining),
        hash: request.phase.hash,
    };
    return newEndTime;
}

async function setExpiration(joinID, seconds){
    const phaseObject = joinIDToEndTime[joinID];
    if(!phaseObject)
        throw httpHelpers.httpError('Phase with that id not found.', 422);
    const newEndTime = await setExpirationFromEvent({
        joinID,
        phase: {
            length: seconds,
            hash: phaseObject.hash,
        },
    });
    if(seconds){
        const game = await gameRepo.getByLobbyID(joinID);
        require('./eventService').timerUpdate({
            gameID: game.id,
            joinID,
            endTime: newEndTime,
            seconds,
        });
    }
    return newEndTime;
}

module.exports = {
    getEndTime,
    getPercentTimeLeftByUserID,
    getPhaseLength,
    setExpirationFromEvent,
    setExpiration,
};

function endPhaseNow(joinID){
    return gretelClient.sendRequest('PUT', 'phases', {
        joinID,
        phaseHash: joinIDToEndTime[joinID].hash,
        timeLeft: getPercentTimeLeftByJoinID(joinID),
    });
    // return 0; // this might need to be new Date().getTime()
}

function getPhaseSubEvents(request, millisecondsRemaining){
    const phaseEndWarning = request.phase.name === gamePhase.NIGHTTIME && setTimeout(async() => {
        const gameService = require('./gameService');
        const game = await gameService.getByLobbyID(request.joinID);
        if(game)
            require('./eventService').phaseEndBid({
                event: 'phaseEndBid',
                gameID: game.id,
                players: game.players,
            });
    }, millisecondsRemaining * 0.6666);

    const checkVotes = [];
    if(request.phase.name === gamePhase.VOTES_OPEN)
        for(let i = 1; i < request.phase.livePlayerCount; i++){
            const timeLeft = i / (request.phase.livePlayerCount - 1) * millisecondsRemaining;
            const checkVoteFunc = setTimeout(() => {
                const args = {
                    joinID: request.joinID,
                    phaseHash: request.phase.hash,
                    checkVotes: true,
                    timeLeft: getPercentTimeLeftByJoinID(request.joinID),
                };
                gretelClient.sendRequest('PUT', 'phases', args);
            }, timeLeft + 1); // 1 is because if it's 50% of votes on a person, you need one more milli to trigger it
            checkVotes.push(checkVoteFunc);
        }


    const endPhase = setTimeout(() => {
        const args = {
            joinID: request.joinID,
            phaseHash: request.phase.hash,
            timeLeft: getPercentTimeLeftByJoinID(request.joinID),
        };
        gretelClient.sendRequest('PUT', 'phases', args);
    }, millisecondsRemaining);

    return [phaseEndWarning, ...checkVotes, endPhase];
}

function getPercentTimeLeftByJoinID(joinID){
    const { endTime, phaseLength } = joinIDToEndTime[joinID];
    const now = new Date().getTime();
    return (endTime - now) / (1000 * phaseLength);
}

function stopOldTimeouts({ phaseSubEvents }){
    phaseSubEvents.map(subEventFunc => clearTimeout(subEventFunc));
}

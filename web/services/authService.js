const httpHelpers = require('../utils/httpHelpers');

const userPermissionRepo = require('../repos/userPermissionRepo');


async function validateModRequest(request, permission){
    const userService = require('./userService');
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const permissions = await userPermissionRepo.getByUserID(user.id);
    if(!permissions.includes(permission))
        throw httpHelpers.httpError('Authentication failed.', 403);
}

module.exports = {
    validateModRequest,
};

const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');


async function cancelAction(userID, actionIndex, command){
    const phaseService = require('./phaseService');
    const timeLeft = await phaseService.getPercentTimeLeftByUserID(userID);
    const args = {
        userID, actionIndex, command, timeLeft,
    };
    const { errors } = await gretelClient.sendRequest('DELETE', 'actions', args);
    if(errors.length)
        throw httpHelpers.httpError(errors, 422);
}

async function getAbilities(userID, gameID){
    const { errors, response } = await gretelClient.sendRequest(
        'GET', 'actions', { userID, gameID },
    );
    if(errors.length)
        throw httpHelpers.httpError(errors, 422);
    return response;
}

async function submitActionMessage(userID, message){
    const phaseService = require('./phaseService');
    const timeLeft = await phaseService.getPercentTimeLeftByUserID(userID);
    const args = Object.assign({ userID }, { ...message, timeLeft });
    const responseObj = await gretelClient.sendRequest('PUT', 'actions', args);
    if(responseObj.errors.length)
        throw httpHelpers.httpError(responseObj.errors, 422);
    return responseObj.response;
}

module.exports = {
    cancelAction,
    getAbilities,
    submitActionMessage,
};

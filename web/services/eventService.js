const config = require('../config');
const helpers = require('../utils/helpers');

const integrationTypes = require('../models/enums/integrationTypes');

const phaseService = require('./phaseService');
const timerService = require('./timerService');


function actionSubmit(request){
    return pushEventToClients(request, 'onActionSubmit');
}

function broadcast(request){
    return pushEventToClients(request, 'onBroadcast');
}

function dayDeath(request){
    return pushEventToClients(request, 'onDayDeath');
}

function dayStart(request){
    phaseService.setExpirationFromEvent(request);
    return pushEventToClients(request, 'onDayStart');
}

function factionCreate(request){
    return pushEventToClients(request, 'onFactionCreate');
}

function factionDelete(request){
    return pushEventToClients(request, 'onFactionDelete');
}

function factionModifierUpsert(request){
    return pushEventToClients(request, 'onFactionModifierUpdate');
}

function factionRoleAbilityModifierUpsert(request){
    return pushEventToClients(request, 'onFactionRoleAbilityModifierUpdate');
}

function factionRoleModifierUpsert(request){
    return pushEventToClients(request, 'onFactionRoleModifierUpdate');
}

function gameChatMessage(request){
    return pushEventToClients(request, 'onGameChatMessage');
}

function gameEnd(request){
    return pushEventToClients(request, 'onGameEnd');
}

function gameStart(request){
    return pushEventToClients(request, 'onGameStart');
}

function hostChange(request){
    return pushEventToClients(request, 'onHostChange');
}

function lobbyChatMessage(request){
    return pushEventToClients(request, 'onLobbyChatMessage');
}

function lobbyCreate(request){
    return pushEventToClients(request, 'onLobbyCreate');
}

function lobbyDelete(request){
    timerService.stopTimeout(request.joinID);
    return pushEventToClients(request, 'onLobbyDelete');
}

function newPlayer(request){
    return pushEventToClients(request, 'onNewPlayer');
}

function newVote(request){
    return pushEventToClients(request, 'onNewVote');
}

function nightStart(request){
    phaseService.setExpirationFromEvent(request);
    return pushEventToClients(request, 'onNightStart');
}

function phaseEndBid(request){
    return pushEventToClients(request, 'onPhaseEndBid');
}

function phaseReset(request){
    phaseService.setExpirationFromEvent(request);
    return pushEventToClients(request, 'onPhaseReset');
}

function playerNameUpdate(request){
    return pushEventToClients(request, 'onPlayerNameUpdate');
}

function playerRemove(request){
    return pushEventToClients(request, 'onPlayerRemove');
}

function playerUpdate(request){
    return pushEventToClients(request, 'onPlayerUpdate');
}

function roleCardUpdate(request){
    return pushEventToClients(request, 'onRoleCardUpdate');
}

function roleAbilityModifierUpsert(request){
    return pushEventToClients(request, 'onRoleAbilityModifierUpdate');
}

function rolePickingStart(request){
    return phaseService.setExpirationFromEvent(request);
}

function setupChange(request){
    return pushEventToClients(request, 'onSetupChange');
}

function setupHiddenAdd(request){
    return pushEventToClients(request, 'onSetupHiddenAdd');
}

function setupHiddenRemove(request){
    return pushEventToClients(request, 'onSetupHiddenRemove');
}

function setupModifierUpsert(request){
    return pushEventToClients(request, 'onSetupModifierUpdate');
}

function timerUpdate(request){
    return pushEventToClients(request, 'onTimerUpdate');
}

function trialStart(request){
    phaseService.setExpirationFromEvent(request);
    return pushEventToClients(request, 'onTrialStart');
}

function votePhaseReset(request){
    phaseService.setExpirationFromEvent(request);
    return pushEventToClients(request, 'onVotePhaseReset');
}

function votePhaseStart(request){
    phaseService.setExpirationFromEvent(request);
    return pushEventToClients(request, 'onVotePhaseStart');
}

module.exports = {
    actionSubmit,
    broadcast,
    dayDeath,
    dayStart,
    factionCreate,
    factionDelete,
    factionModifierUpsert,
    factionRoleAbilityModifierUpsert,
    factionRoleModifierUpsert,
    gameChatMessage,
    gameEnd,
    gameStart,
    hostChange,
    lobbyChatMessage,
    lobbyCreate,
    lobbyDelete,
    newPlayer,
    newVote,
    nightStart,
    phaseEndBid,
    phaseReset,
    playerNameUpdate,
    playerUpdate,
    playerRemove,
    roleCardUpdate,
    roleAbilityModifierUpsert,
    rolePickingStart,
    setupChange,
    setupHiddenAdd,
    setupHiddenRemove,
    setupModifierUpsert,
    timerUpdate,
    trialStart,
    votePhaseReset,
    votePhaseStart,
};

async function pushEventToClients(request, eventName){
    const promise = getPushEventToClientsPromise(request, eventName);
    if(config.isWaitingForEvents())
        return promise;

    promise.catch(helpers.log);
}

async function getPushEventToClientsPromise(request, eventName){
    const clients = integrationTypes.getAll().map(helpers.getWrapper);
    const eventPromises = clients.map(async client => {
        try{
            if(client[eventName])
                return client[eventName](request);
        }catch(err){
            helpers.log(err);
        }
    });
    return Promise.all(eventPromises);
}

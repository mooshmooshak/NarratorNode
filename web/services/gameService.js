const { keyBy } = require('lodash');
const helpers = require('../utils/helpers');
const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');

const gameRepo = require('../repos/replayRepo');

const gamePhase = require('../models/enums/gamePhase');


// 0 -> public
// null/undefined -> mod
async function filterNeededUserInfo(game, requestingUserID){
    game.integrations = await require('./gameIntegrationService').getIntegrations(game.id);
    game.players = await require('./playerService').filterKeys(game, requestingUserID);

    if(game.isStarted && !game.phase.name !== gamePhase.FINISHED)
        game.phase.endTime = require('./phaseService').getEndTime(game.joinID);
    const users = await require('./userService').getUsersByGameID(game.id);
    game.users = combineUserData(users, game.users);
    return game;
}

async function create(userID, hostName = 'guest', setupID){
    const args = {
        userID,
        setupID,
        isPublic: false,
        hostName,
    };

    const { response, errors } = await gretelClient.sendRequest('POST', 'games', args);
    if(errors.length)
        throw httpHelpers.httpError(errors, 422);
    const game = await filterNeededUserInfo(response, userID);
    const eventService = require('./eventService');
    await eventService.lobbyCreate(game);
    return game;
}

async function deleteGame(gameID){
    const args = {
        gameID,
    };
    const javaResponse = await gretelClient.sendRequest('DELETE', 'games', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
}

async function getAll(){
    const { errors, response: games } = await gretelClient.sendRequest('GET', 'games');
    if(errors.length)
        throw httpHelpers.httpError(errors, 422);
    return games;
}

async function getByID(gameID){
    const { response: game } = await gretelClient.sendRequest('GET', 'games', { gameID });
    if(helpers.isEmptyObject(game))
        return null;
    return game;
}

async function getByLobbyID(lobbyID){
    let gameID;
    try{
        const gameSchema = await gameRepo.getByLobbyID(lobbyID);
        gameID = gameSchema.id;
    }catch(err){
        return null;
    }
    return getByID(gameID);
}

async function getUnstartedBySetupID(setupID){
    const game = await gretelClient.sendRequest('GET', 'games', { setupID });
    if(helpers.isEmptyObject(game.response))
        return null;
    return filterNeededUserInfo(game.response);
}


async function getUserState(userID){
    const args = { userID };
    const { response } = await gretelClient.sendRequest('GET', 'getGameState', args);
    if(!response)
        return {};
    if(response.gameStart && !response.isFinished){
        const phaseService = require('./phaseService');
        response.timer = phaseService.getEndTime(response.joinID);
    }
    return response;
}

async function updateModifier(userID, args){
    args.userID = userID;
    const javaResponse = await gretelClient.sendRequest('POST', 'gameModifiers', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response.modifier;
}

async function start(gameID){
    const args = { gameID, start: true };
    const javaResponse = await gretelClient.sendRequest('POST', 'games', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return filterNeededUserInfo(javaResponse.response);
}

module.exports = {
    filterNeededUserInfo,
    create,
    deleteGame,
    getAll,
    getByID,
    getByLobbyID,
    getUnstartedBySetupID,
    getUserState,
    updateModifier,
    start,
};

function combineUserData(usersWithNames, usersWithModerator){
    const userNamesMap = keyBy(usersWithNames, 'id');
    return usersWithModerator.map(({ id, isModerator }) => ({
        id,
        name: userNamesMap[id].name,
        isModerator,
    }));
}

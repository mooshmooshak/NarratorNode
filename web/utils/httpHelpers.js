function frozenSetupError(){
    return {
        errors: ['This setup is frozen and cannot be edited.'],
        statusCode: 422,
    };
}

function getJson(req){
    return new Promise((resolve, reject) => {
        let bodyStr = '';
        req.on('data', chunk => {
            bodyStr += chunk.toString();
        });
        req.on('end', () => {
            try{
                resolve(JSON.parse(bodyStr));
            }catch(err){
                reject(httpError('Body was not of json type.', 400));
            }
        });
    });
}

function httpError(errors, statusCode){
    if(!Array.isArray(errors))
        errors = [errors];
    return {
        errors,
        statusCode,
    };
}

module.exports = {
    frozenSetupError,
    getJson,
    httpError,
};

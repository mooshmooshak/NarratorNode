module.exports = {
    SKIP_DAY_NAME: 'Skip Day',

    MAX_LOBBY_CHAT_COUNT: 5000,
    MAX_PLAYER_COUNT: 255,
};

function isSpawnable(playerCount, hiddenSpawn){
    return playerCount >= hiddenSpawn.minPlayerCount && playerCount <= hiddenSpawn.maxPlayerCount;
}

module.exports = {
    isSpawnable,
};

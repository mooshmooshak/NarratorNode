function intersection(set1, set2){
    const newSet = new Set();
    set1.forEach(elem => {
        if(set2.has(elem))
            newSet.add(elem);
    });
    return newSet;
}

module.exports = {
    intersection,
};

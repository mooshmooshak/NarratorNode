const baseValidator = require('./baseValidator');


const createRequestSchema = {
    type: 'object',
    properties: {
        setupID: {
            type: 'number',
            required: true,
        },
    },
};

const joinIDRequestSchema = {
    type: 'string',
    required: 'true',
    minLength: 4,
    maxLength: 4,
};

function createRequest(query){
    baseValidator.validate(query, createRequestSchema);
}

function joinIDRequest(query){
    baseValidator.validate(query, joinIDRequestSchema);
}

module.exports = {
    createRequest,
    joinIDRequest,
};

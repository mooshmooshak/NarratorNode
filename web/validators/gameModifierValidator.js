const baseValidator = require('./baseValidator');


const updateGameModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
    },
};

function updateGameModifierRequest(query){
    baseValidator.validate(query, updateGameModifierRequestSchema);
}

module.exports = {
    updateGameModifierRequest,
};

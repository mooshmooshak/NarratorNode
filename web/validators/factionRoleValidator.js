const baseValidator = require('./baseValidator');


const createFactionRoleRequestSchema = {
    type: 'object',
    properties: {
        factionID: {
            type: 'number',
            required: true,
        },
        roleID: {
            type: 'number',
            required: true,
        },
    },
};

const updateFactionRoleRequestSchema = {
    type: 'object',
    properties: {
        receivedFactionRoleID: {
            type: ['number', 'null'],
        },
    },
};

function createFactionRoleRequest(query){
    baseValidator.validate(query, createFactionRoleRequestSchema);
}

function updateFactionRoleRequest(query){
    baseValidator.validate(query, updateFactionRoleRequestSchema);
}

module.exports = {
    createFactionRoleRequest,
    updateFactionRoleRequest,
};

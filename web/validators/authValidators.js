const baseValidator = require('./baseValidator');

const socketAuthSchema = {
    type: 'object',
    properties: {
        token: {
            type: 'string',
            required: true,
        },
        wrapperType: {
            enum: [
                'firebase',
                'tempAuth',
            ],
            required: true,
        },
    },
};

function socketAuth(object){
    baseValidator.validate(object, socketAuthSchema, 403);
}

module.exports = {
    socketAuth,
};

const baseValidator = require('./baseValidator');


const idSchema = {
    type: 'number',
    required: true,
    minimum: 0,
};

function idRequest(query, statusCode = 404){
    query = parseInt(query, 10);
    baseValidator.validate(query, idSchema, statusCode);
    return query;
}

module.exports = {
    idRequest,
};

const baseValidator = require('./baseValidator');


const createSetupRequestSchema = {
    type: 'object',
    properties: {
        description: {
            type: 'string',
            maxLength: '512',
        },
        isDefault: {
            type: 'boolean',
        },
        name: {
            type: 'string',
            required: true,
            minLength: '1',
            maxLength: '25',
        },
        slogan: {
            type: 'string',
            maxLength: '45',
        },
    },
};

const updateSetupPriorityRequestSchema = {
    type: 'object',
    properties: {
        key: {
            type: 'string',
            required: true,
            minLength: '1',
            maxLength: '11',
        },
        priority: {
            type: 'number',
            minimum: 0,
            required: true,
        },
    },
};

const updateSetupRequestSchema = {
    type: 'object',
    properties: {
        gameID: {
            type: 'number',
            required: true,
        },
        setupID: {
            type: 'number',
            required: true,
        },
    },
};

const userSetupResponseSchema = {
    type: 'object',
    properties: {
        isEditable: {
            type: 'boolean',
        },
    },
};

function createRequest(query){
    baseValidator.validate(query, createSetupRequestSchema);
}

function setPriorityRequest(query){
    baseValidator.validate(query, updateSetupPriorityRequestSchema);
}

function setSetupRequest(query){
    baseValidator.validate(query, updateSetupRequestSchema);
}

function userSetupResponse(query){
    baseValidator.validate(query, userSetupResponseSchema);
}

module.exports = {
    createRequest,
    setPriorityRequest,
    setSetupRequest,
    userSetupResponse,
};

const baseValidator = require('./baseValidator');


const addBotsRequestSchema = {
    type: 'object',
    properties: {
        botCount: {
            type: 'number',
            required: true,
        },
    },
};

const kickRequestSchema = {
    type: 'number',
    required: true,
};

const updatePlayerRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
        },
    },
};

function addBotsRequest(query){
    baseValidator.validate(query, addBotsRequestSchema);
}

function kickUserRequest(query){
    baseValidator.validate(query, kickRequestSchema);
}

function updatePlayerRequest(query){
    baseValidator.validate(query, updatePlayerRequestSchema);
}

module.exports = {
    addBotsRequest,
    kickUserRequest,
    updatePlayerRequest,
};

const baseValidator = require('./baseValidator');


const createHiddenRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        setupID: {
            type: 'number',
            required: true,
        },
    },
};

function createHiddenRequest(query){
    baseValidator.validate(query, createHiddenRequestSchema);
}

module.exports = {
    createHiddenRequest,
};

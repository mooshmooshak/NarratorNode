const baseValidator = require('./baseValidator');


const cancelActionRequestSchema = {
    type: 'object',
    properties: {
        actionIndex: {
            type: 'number',
            required: true,
        },
        command: {
            type: 'string',
            required: true,
            length: '1',
        },
    },
};

function cancelActionRequest(query){
    baseValidator.validate(query, cancelActionRequestSchema);
}

module.exports = {
    cancelActionRequest,
};

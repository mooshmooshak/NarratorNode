const fs = require('fs');

const config = require('../config.json');
const db = require('./db_communicator');
const helpers = require('./utils/helpers');
const gretelClient = require('./utils/gretelClient');

const userService = require('./services/userService');

const authValidators = require('./validators/authValidators');
const miscValidators = require('./validators/miscValidators');

const { app } = require('./routes');

const browserWrapper = require('./channels/browser/browserWrapper');
const { DiscordWrapper } = require('./channels/discord/discordWrapper');
const { NotificationWrapper } = require('./channels/notification/notificationWrapper');
const sc2mafiaClient = require('./channels/sc2mafia/sc2mafiaClient');

const discordWrapper = new DiscordWrapper();
const notificationWrapper = new NotificationWrapper();


process.chdir('./web');

process.on('unhandledRejection', (reason, promise) => {
    console.log('###### UNHANDLED REJECTION PROMISE #####'); /* eslint-disable-line no-console */
    console.log(reason); /* eslint-disable-line no-console */
    console.log(promise); /* eslint-disable-line no-console */
    console.log('########################################'); /* eslint-disable-line no-console */
});

function extractHTTPData(request){
    return new Promise((resolve, reject) => {
        let body = [];
        request.on('data', chunk => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            if(body.length)
                return resolve(body);
            const err = 'Expected data with request';
            reject(err);
        });
    });
}

function onRequest(request, response){
    if(request.method !== 'POST')
        return;
    if(request.url === '/requestStory')
        return extractHTTPData(request)
            .then(getStory)
            .then(happenings => {
                response.statusCode = 200;
                response.end(happenings);
            }).catch(err => {
                response.statusCode = 404;
                response.end(err.toString());
            });

    if(request.url === '/storySubmit')
        return extractHTTPData(request)
            .then(submitStory)
            .then(getStory)
            .then(happenings => {
                response.statusCode = 200;
                response.end(happenings);
            })
            .catch(err => {
                response.statusCode = 404;
                response.end(err.toString());
            });

    if(request.url === '/instanceMessage')
        return extractHTTPData(request)
            .then(data => {
                data = JSON.parse(data);
                data.server = false;
                data.httpRequest = false;
                data.token = request.headers.auth;
                gretelClient.sendInstanceRequest(data);
                response.statusCode = 200;
                response.end();
            }).catch(err => {
                if(err === 'not json'){
                    response.statusCode = 400;
                    response.end('Must be a json object');
                }else{
                    response.statusCode = 500;
                    response.end(err);
                }
            });
}

const webServer = config.env !== 'prod'
    ? { server: require('http'), serverArgs: [onRequest] }
    : {
        server: require('https'),
        serverArgs: [{
            key: fs.readFileSync('key.pem'),
            cert: fs.readFileSync('cert.pem'),
        }, onRequest],
    };

webServer.server.createServer(...webServer.serverArgs);

const connectionsMapping = {};
function socketPush(userID, message){
    const c = connectionsMapping[userID];
    if(!c)
        return;
    let socket;
    for(let i = 0; i < c.length; i++){
        socket = c[i];
        if(socket.readyState === 3){
            helpers.log('err: socket is closed');
            continue;
        }
        if(socket.readyState !== 1){
            helpers.log('socket ready state wasn\'t 1', socket.readyState);
            continue;
        }
        socket.send(message, helpers.log);
    }
}

function getSocketUserIDs(){
    return new Set(Object.keys(connectionsMapping)
        .map(userID => parseInt(userID, 10)));
}

function printErrorDelimiter(){
    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'); /* eslint-disable-line no-console */
}

app.ws('/napi', ws => {
    let userID;
    let unauthenticatedMessages = [];
    ws.on('message', async message => {
        let object;
        try{
            // TODO figure out how to not have to parse this every time a message comes in.
            // let javaRequests do it.
            object = JSON.parse(message);
        }catch(err){
            return helpers.log(err, 'Failed to parse socket object');
        }

        if(userID)
            return cleanMessage(object, message, userID);

        try{
            authValidators.socketAuth(object);
        }catch(err){
            return unauthenticatedMessages.push([object, message]);
        }

        try{
            const user = await userService.getByAuthToken(object);
            userID = user.id;
        }catch(err){
            helpers.log(err, 'Failed to authenticate socket');
            return ws.close();
        }

        const priorMapping = connectionsMapping[userID];
        if(!priorMapping){
            connectionsMapping[userID] = [ws];
            browserWrapper.onUserActivityStatusChange(userID, true);
        }else{
            priorMapping.push(ws);
        }

        unauthenticatedMessages.forEach(objectMessage => {
            cleanMessage(objectMessage[0], objectMessage[1], userID);
        });
        unauthenticatedMessages = null;
    });

    ws.on('error', err => {
        printErrorDelimiter();
        helpers.log(err, `websocket error with user ${userID}`);
        printErrorDelimiter();
    });

    ws.on('close', () => onConnectionClose(ws));
});

function onConnectionClose(ws){
    Object.keys(connectionsMapping)
        .forEach(userID => {
            const connectionsList = connectionsMapping[userID];
            const index = connectionsList.indexOf(ws);

            if(index === -1)
                return;

            if(connectionsList.length !== 1)
                return connectionsList.splice(index, 1);

            delete connectionsMapping[userID];
            browserWrapper.onUserActivityStatusChange(parseInt(userID, 10), false);
        });
}

function cleanMessage(o, message, userID){
    if(o.discord){
        delete o.discord;
        message = JSON.stringify(o);
    }
    if(!o.message)
        return;

    if(o.userID !== userID){
        o.userID = userID;
        message = JSON.stringify(o);
    }

    gretelClient.sendInstanceRequest(message);
}

function getStory(args){
    return helpers.runJavaProg('HappeningGenerator', args);
}

function getRoleID(roleTypes){
    const promiseToReturn = [];
    for(let i = 0; i < roleTypes.length; i++)
        promiseToReturn.push(getValueID(roleTypes[i], 'role_type_stories', 'role_id', 'role'));

    return Promise.all(promiseToReturn);
}

function getDeathID(deathTypes){
    const promiseToReturn = [];
    for(let i = 0; i < deathTypes.length; i++)
        promiseToReturn.push(getValueID(deathTypes[i], 'death_type_stories', 'death_id', 'death'));

    return Promise.all(promiseToReturn);
}

function getValueID(type, tableName, typeID, typeValue){
    let qString;
    if(!type)
        qString = `SELECT ${typeID} FROM ${tableName} WHERE ${typeValue} is null;`;
    else
        qString = `SELECT ${typeID} FROM ${tableName} WHERE ${typeValue} = '${type}';`;

    return db.query(qString)
        .then(r => r[0][typeID]);
}

async function submitStory(args){
    const o = JSON.parse(args);
    let roles = o.applicableRoles;
    let dtypes = o.applicableDeaths;
    roles = roles.filter(helpers.onlyUniqueFilterFunction());
    dtypes = dtypes.filter(helpers.onlyUniqueFilterFunction);

    const data = await Promise.all([getRoleID(roles), getDeathID(dtypes)]);
    let roleIDs = data[0];
    let deathIDs = data[1];
    roleIDs.sort();
    deathIDs.sort();

    roleIDs = roleIDs.join('');
    deathIDs = deathIDs.join('');
    const teams = o.applicableTeams.join('');

    const genre = o.genre || null;

    const qString = 'INSERT INTO stories (story, role, death, team, genre) '
    + 'VALUES (?, ?, ?, ?, ?);';
    return db.query(qString, [o.story, roleIDs, deathIDs, teams, genre]);
}

async function start(){
    let promises = [gretelClient.connect()];
    if(process.argv.indexOf('--statsRefresh') !== -1)
        promises.push(helpers.runJavaProg('StatRefresher'));
    await Promise.all(promises);

    const serverStarter = new Promise(resolve => {
        app.listen(config.port_number, resolve);
    });
    promises = [serverStarter];
    if(helpers.serviceEnabled('discord'))
        promises.push(discordWrapper.connect());
    if(helpers.serviceEnabled('sc2mafia'))
        promises.push(sc2mafiaClient.connect());

    await Promise.all(promises);

    helpers.log(`Web server listening on ${config.port_number}.`);
}

function close(){
    return Promise.all([db.close(), gretelClient.close()]);
}

module.exports = {
    start,

    socketPush,

    browserWrapper,
    close,
    discordWrapper,
    getSocketUserIDs,
    notificationWrapper,
};

app.use((req, res, next) => { /* eslint-disable-line no-unused-vars */
    // console.log(req.url); // dont delete
    next();
});

app.use((err, req, res, next) => { /* eslint-disable-line no-unused-vars */
    try{
        miscValidators.serviceError(err);
    }catch(miscError){
        helpers.log(err);
        return res.status(500).json({
            errors: helpers.complexObjectToString(err),
        });
    }

    const serviceError = {
        errors: err.errors,
        response: [],
    };
    res.status(err.statusCode).json(serviceError);
});

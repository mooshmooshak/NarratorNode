const db = require('../db_communicator');


async function getPriority(setupID){
    const queryText = ''
        + 'SELECT priority, `key` FROM featured_setups '
        + 'WHERE setup_id = ?;';
    const results = await db.query(queryText, [setupID]);
    if(results.length)
        return {
            priority: results[0].priority,
            key: results[0].key,
        };
    return null;
}

function setPriority(setupID, priority, key){
    const queryText = ''
        + 'INSERT INTO featured_setups (setup_id, priority, `key`) '
        + 'VALUES (?, ?, ?);';
    return db.query(queryText, [setupID, priority, key, priority]);
}

function removeBySetupID(setupID){
    const queryText = 'DELETE FROM featured_setups WHERE setup_id = ?;';
    return db.query(queryText, [setupID]);
}

module.exports = {
    getPriority,
    setPriority,
    removeBySetupID,
};

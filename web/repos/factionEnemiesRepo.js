const db = require('../db_communicator');


function deleteAll(){
    const queryText = 'DELETE FROM faction_enemies;';
    return db.query(queryText);
}

async function getBySetupID(setupID){
    const queryText = 'SELECT enemy_a, enemy_b FROM faction_enemies '
        + 'WHERE setup_id = ?;';
    const results = await db.query(queryText, [setupID]);
    return results.map(result => [result.enemy_a, result.enemy_b]);
}

function setEnemies(factionID1, factionID2, setupID){
    if(factionID1 > factionID2)
        return setEnemies(factionID2, factionID1, setupID);
    const queryText = 'INSERT INTO faction_enemies (enemy_a, enemy_b, setup_id) '
        + 'VALUES (?, ?, ?);';
    return db.query(queryText, [factionID1, factionID2, setupID]);
}

module.exports = {
    deleteAll,
    getBySetupID,
    setEnemies,
};

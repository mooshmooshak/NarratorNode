const db = require('../db_communicator');


async function getUserIDsByGameID(gameID){
    const userQuery = db.query('SELECT user_id '
        + 'FROM game_users '
        + 'WHERE game_users.game_id = ?;', [gameID]);
    const userResults = await userQuery;
    return new Set(userResults.map(result => parseInt(result.user_id, 10)));
}

async function getBySiblingUserID(userID){
    const userResults = await db.query('SELECT user_id AS userID '
        + 'FROM game_users '
        + 'WHERE game_users.game_id IN ('
        + '  SELECT game_id '
        + '  FROM game_users '
        + '  LEFT JOIN replays ON replays.id = game_users.game_id '
        + '  WHERE replays.instance_id IS NOT NULL '
        + '  AND user_id = ?'
        + ');', [userID]);
    return new Set(userResults.map(result => parseInt(result.userID, 10)));
}

async function getByGameID(gameID){
    const results = await db.query(
        'SELECT user_id, is_moderator FROM game_users WHERE game_id = ?;',
        [gameID],
    );
    return results.map(r => ({
        userID: parseInt(r.user_id, 10),
        isModerator: !!r.is_moderator,
    }));
}

async function getIDsByLobbyID(joinID){
    const userQuery = db.query('SELECT user_id '
        + 'FROM game_users '
        + 'LEFT JOIN replays '
        + 'ON replays.id = game_users.game_id '
        + 'WHERE replays.instance_id = ?;', [joinID]);
    const userResults = await userQuery;
    return new Set(userResults.map(result => parseInt(result.user_id, 10)));
}

module.exports = {
    getUserIDsByGameID,
    getBySiblingUserID,
    getByGameID,
    getIDsByLobbyID,
};

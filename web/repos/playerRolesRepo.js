const db = require('../db_communicator');


async function getByGameID(gameID){
    const queryText = ''
        + 'SELECT setup_hidden_id, player_id '
        + 'FROM player_roles '
        + 'WHERE game_id = ?;';
    const results = await db.query(queryText, [gameID]);
    const spawns = {};
    results.forEach(result => {
        spawns[result.player_id] = result.setup_hidden_id;
    });
    return spawns;
}

function insert(gameID, setupHiddenID, playerID){
    const queryText = ''
        + 'INSERT INTO player_roles (game_id, setup_hidden_id, player_id) '
        + 'VALUES (?, ?, ?);';
    return db.query(queryText, [gameID, setupHiddenID, playerID]);
}

module.exports = {
    getByGameID,
    insert,
};

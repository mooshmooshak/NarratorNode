const { pick } = require('lodash');
const db = require('../db_communicator');


async function create(userID, {
    hiddenID, maxPlayerCount, minPlayerCount, mustSpawn, isExposed,
}){
    const queryText = ''
        + 'INSERT INTO setup_hiddens '
        + 'SELECT null as id, ? as hidden_id, hiddens.setup_id as setup_id, '
        + '? as isExposed, ? as must_spawn, ? as min_player_count, ? as max_player_count '
        + 'FROM setups '
        + 'LEFT JOIN hiddens ON setups.id = hiddens.setup_id '
        + 'LEFT JOIN setup_hiddens ON setup_hiddens.setup_id = hiddens.setup_id '
        + 'WHERE hiddens.id = ? AND setups.owner_id = ? AND setups.is_editable = true LIMIT 1;';
    const result = await db.query(queryText, [hiddenID, isExposed, mustSpawn, minPlayerCount,
        maxPlayerCount, hiddenID, userID]);
    return result.insertID;
}

async function isEditable(setupHiddenID){
    const queryText = ''
        + 'SELECT setups.is_editable FROM setup_hiddens '
        + 'INNER JOIN setups ON setups.id = setup_hiddens.setup_id '
        + 'WHERE setup_hiddens.id = ?;';
    const results = await db.query(queryText, [setupHiddenID]);
    return results.length && results[0].is_editable;
}

async function getBySetupID(setupID){
    const queryText = ''
        + 'SELECT is_exposed AS isExposed, must_spawn AS mustSpawn, '
        + 'max_player_count AS maxPlayerCount, id, '
        + 'min_player_count AS minPlayerCount '
        + 'FROM setup_hiddens WHERE setup_id = ?;';
    const results = await db.query(queryText, [setupID]);
    return results.map(result => ({
        ...pick(result, ['maxPlayerCount', 'minPlayerCount']),
        id: parseInt(result.id, 10),
        isExposed: !!result.isExposed,
        mustSpawn: !!result.mustSpawn,
    }));
}

module.exports = {
    create,
    isEditable,
    getBySetupID,
};

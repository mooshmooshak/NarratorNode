const { flattenDeep } = require('lodash');
const db = require('../db_communicator');


function createSpawns(setupIterationID, spawns){
    const queryTextParams = spawns.map(() => '(?, ?, ?)').join(', ');
    const queryText = ''
        + 'INSERT INTO setup_iteration_spawns '
        + '(setup_iteration_id, setup_hidden_id, faction_role_id) '
        + `VALUES ${queryTextParams};`;
    const params = spawns.map(({ factionRoleID, setupHiddenID }) => [
        setupIterationID, setupHiddenID, factionRoleID]);
    return db.query(queryText, flattenDeep(params));
}

module.exports = {
    createSpawns,
};

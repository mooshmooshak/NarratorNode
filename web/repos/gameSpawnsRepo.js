const db = require('../db_communicator');


async function getByGameID(gameID){
    const queryText = ''
        + 'SELECT setup_hidden_id, faction_role_id '
        + 'FROM game_spawns '
        + 'WHERE game_id = ?;';
    const results = await db.query(queryText, [gameID]);
    const spawns = {};
    results.forEach(result => {
        spawns[result.setup_hidden_id] = result.faction_role_id;
    });
    return spawns;
}

function insert(gameID, setupHiddenID, factionRoleID){
    const queryText = ''
        + 'INSERT INTO game_spawns (game_id, setup_hidden_id, faction_role_id) '
        + 'VALUES (?, ?, ?);';
    return db.query(queryText, [gameID, setupHiddenID, factionRoleID]);
}

module.exports = {
    getByGameID,
    insert,
};

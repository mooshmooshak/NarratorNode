const db = require('../db_communicator');


function create(userID, permission){
    const queryText = 'INSERT INTO user_permissions (user_id, permission_type) VALUES (?, ?);';
    return db.query(queryText, [userID, permission]);
}

async function getByUserID(userID){
    const queryText = 'SELECT permission_type FROM user_permissions WHERE user_id = ?;';
    const results = await db.query(queryText, [userID]);
    return results.map(result => parseInt(result.permission_type, 10));
}

module.exports = {
    create,
    getByUserID,
};

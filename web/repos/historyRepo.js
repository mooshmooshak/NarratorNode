const db = require('../db_communicator');


function get(userID, limit){
    let query = 'SELECT players.replay_id, roles.name as roleName '
        + 'FROM players '
        + 'LEFT JOIN factionRoles '
        + 'ON players.faction_role_id = faction_roles.id '
        + 'LEFT JOIN roles '
        + 'ON roles.id = faction_roles.role_id '
        + 'WHERE user_id = ?';
    if(limit)
        query += `LIMIT ${limit};`;
    else
        query += ';';
    return db.query(query, [userID]);
}

module.exports = {
    get,
};

const hiddenSpawnValidators = require('../validators/hiddenSpawnValidators');
const intValidator = require('../validators/intValidator');

const hiddenSpawnService = require('../services/setup/hiddenSpawnService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function create(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    hiddenSpawnValidators.createHiddenSpawnsRequest(json);
    return hiddenSpawnService.createMany(user.id, json);
}

async function deleteSpawn(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const hiddenSpawnID = intValidator.idRequest(request.params.hiddenSpawnID);
    await hiddenSpawnService.deleteSpawn(user.id, hiddenSpawnID);
}

module.exports = {
    create,
    deleteSpawn,
};

const factionAbilityModifierValidator = require('../validators/factionAbilityModifierValidator');
const intValidator = require('../validators/intValidator');

const factionAbilityModifierService = require('../services/setup/factionAbilityModifierService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function upsert(request){
    const factionAbilityID = intValidator.idRequest(request.params.factionAbilityID);
    const [user, args] = await Promise.all([userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    }), httpHelpers.getJson(request)]);
    factionAbilityModifierValidator.updateFactionAbilityModifierRequest(args);
    return factionAbilityModifierService.upsert(
        user.id, factionAbilityID, args,
    );
}

module.exports = {
    upsert,
};

const baseAbilityService = require('../services/setup/baseAbilityService');


async function getBaseAbilities(){
    return baseAbilityService.getBaseAbilities();
}

module.exports = {
    getBaseAbilities,
};

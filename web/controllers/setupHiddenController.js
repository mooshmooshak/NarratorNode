const setupHiddenValidators = require('../validators/setupHiddenValidators');
const setupHiddenService = require('../services/setup/setupHiddenService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function createSetupHidden(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    setupHiddenValidators.createSetupHiddenRequest(json);
    return setupHiddenService.add(user.id, json);
}

async function deleteSetupHidden(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const setupHiddenID = parseInt(request.params.setupHiddenID, 10);
    await setupHiddenService.remove(user.id, setupHiddenID);
}

module.exports = {
    createSetupHidden,
    deleteSetupHidden,
};

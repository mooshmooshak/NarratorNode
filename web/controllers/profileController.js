const profileService = require('../services/profileService');
const userService = require('../services/userService');


async function get({ headers }){
    const user = await userService.getByAuthToken({
        token: headers.auth,
        wrapperType: headers.authtype, // headers are lowercased
    }, true); // allow creates
    return profileService.get(user.id);
}

module.exports = {
    get,
};

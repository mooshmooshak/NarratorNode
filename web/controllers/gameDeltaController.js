const gameDeltasService = require('../services/gameDeltasService');


async function findGameDeltas(){
    return gameDeltasService.findGameDeltas();
}

module.exports = {
    findGameDeltas,
};

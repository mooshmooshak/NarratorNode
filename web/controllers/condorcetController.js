const condorcetService = require('../services/condorcetService');

const httpHelpers = require('../utils/httpHelpers');


async function resolveInput(request){
    const data = await httpHelpers.getJson(request);
    return condorcetService.resolveInput(data);
}

module.exports = {
    resolveInput,
};

const userPermissions = require('../models/enums/userPermissions');

const authService = require('../services/authService');
const factionService = require('../services/setup/factionService');
const setupService = require('../services/setup/setupService');
const userService = require('../services/userService');

const factionValidator = require('../validators/factionValidators');
const intValidator = require('../validators/intValidator');
const setupValidator = require('../validators/setupValidator');
const setupIterationValidator = require('../validators/setupIterationValidator');
const gameUserService = require('../services/gameUserService');

const httpHelpers = require('../utils/httpHelpers');


async function clone(request){
    const setupID = intValidator.idRequest(request.params.setupID);
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype,
    });

    return setupService.clone(setupID, user.id);
}

async function createFeatured(request){
    const [data] = await Promise.all([
        httpHelpers.getJson(request),
        authService.validateModRequest(request, userPermissions.ADMIN),
    ]);

    setupValidator.setPriorityRequest(data);

    const setupID = intValidator.idRequest(request.params.setupID, 10);
    await setupService.setPriority(setupID, data.priority, data.key);
}

async function deleteFeatured(request){
    await authService.validateModRequest(request, userPermissions.ADMIN);
    const setupID = intValidator.idRequest(request.params.setupID);
    await setupService.removeFeatured(setupID);
}

async function create(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    setupValidator.createRequest(args);
    return setupService.create(user.id, args);
}

function getFeatured(){
    return setupService.getFeaturedSetups();
}

function getByID(request){
    const setupID = intValidator.idRequest(request.params.setupID);
    return setupService.getByID(setupID);
}

function getIterations(request){
    const playerCount = intValidator.idRequest(request.query.playerCount);
    const setupID = intValidator.idRequest(request.params.setupID);
    return setupService.getIterations(setupID, playerCount);
}

async function createIteration(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype,
    });

    const setupID = intValidator.idRequest(request.params.setupID);

    const iteration = await httpHelpers.getJson(request);
    setupIterationValidator.createSetupIteration(iteration);

    await setupService.createIteration(setupID, iteration, user.id);
}

function getUserSetups(request){
    const userID = intValidator.idRequest(request.query.userID);
    return setupService.getUserSetups(userID);
}

async function updateFaction(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);

    factionValidator.updateFactionRequest(json);

    const setupID = intValidator.idRequest(request.params.setupID);
    await setupService.ensureEditable(setupID, user.id);

    const factionID = intValidator.idRequest(request.params.factionID);
    await factionService.updateFaction({ factionID, setupID, ...json });
}

async function deleteFactionEnemy(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });

    const setupID = intValidator.idRequest(request.params.setupID);
    await setupService.ensureEditable(setupID, user.id);

    const factionID = intValidator.idRequest(request.params.factionID);

    const enemyFactionID = intValidator.idRequest(request.params.enemyFactionID);

    await factionService.deleteEnemy({ factionID, setupID, enemyFactionID });
}

async function updateGameSetup(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    setupValidator.setSetupRequest(json);
    await gameUserService.assertIsModerator(json.gameID, user.id,
        'Only moderators can edit the setup !');
    const args = {
        gameID: json.gameID,
        setupID: json.setupID,
    };
    return setupService.setSetup(args);
}

async function deleteSetup(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const setupID = intValidator.idRequest(request.params.setupID);
    await setupService.deleteSetup(setupID, user.id);
}

module.exports = {
    clone,
    createFeatured,
    deleteFeatured,
    create,
    getFeatured,
    getByID,
    getIterations,
    createIteration,
    getUserSetups,
    updateFaction,
    deleteFactionEnemy,
    updateGameSetup,
    deleteSetup,
};

const Discord = require('discord.js');

const config = require('../../../config');
const Constants = require('../../channels/discord/Constants');

const userTypes = require('../../models/enums/userTypes');
const { PRIVATE } = require('../../channels/discord/enums/channelTypes');

const profileService = require('../../services/profileService');
const gameService = require('../../services/gameService');
const setupService = require('../../services/setup/setupService');
const userService = require('../../services/userService');

const helpers = require('../../utils/helpers');
const modifierUtil = require('../../utils/modifierUtil');

const quasiDiscordChannel = require('./discordEntities/quasiDiscordChannel');

// https://discord.js.org/#/
// https://github.com/jagrosh/MusicBot/wiki/Adding-Your-Bot-To-Your-Server

/*
Bot.
https://discordapp.com/oauth2/authorize?client_id=394259731172163587&scope=bot
1.
https://discordapp.com/oauth2/authorize?client_id=383529750049718272&scope=bot
2.
https://discordapp.com/oauth2/authorize?client_id=385650126418345987&scope=bot
3.
https://discordapp.com/oauth2/authorize?client_id=387719001704366082&scope=bot
4.
https://discordapp.com/oauth2/authorize?client_id=387848473606619156&scope=bot
5.
https://discordapp.com/oauth2/authorize?client_id=387848894366613504&scope=bot
6.
https://discordapp.com/oauth2/authorize?client_id=387849191029866496&scope=bot
7.
https://discordapp.com/oauth2/authorize?client_id=387849406927601664&scope=bot
8.
https://discordapp.com/oauth2/authorize?client_id=389663582473814016&scope=bot
*/


function QuasiDiscordUser(){
    this.mListeners = [];
    this.discordClient = new Discord.Client();
    this.discordClient.on('message', message => {
        for(let i = 0; i < this.mListeners.length; i++)
            if(this.mListeners[i](message)){
                this.mListeners.splice(i, 1);
                i--;
            }
    });
    this.id = helpers.getRandomString(10);
}

QuasiDiscordUser.prototype.joinGuild = function(guildID){
    return this.discordClient.joinGuild(guildID, this.discordClient);
};

QuasiDiscordUser.prototype.waitForMessage = function(waitFunction){
    return new Promise(resolve => {
        this.mListeners.push(message => {
            if(this.discordClient.user.id === message.author.id)
                return;
            if(typeof(message.content) === 'string' && message.content.startsWith('```'))
                return;
            if(waitFunction(message)){
                resolve(message);
                return true;
            }
        });
    });
};

QuasiDiscordUser.prototype.getDiscordID = function(){
    return this.discordClient.user.id;
};

QuasiDiscordUser.prototype.getUserID = async function(){
    const user = await userService.getOrCreateByExternalID(this.getName(),
        this.getDiscordID(), userTypes.DISCORD);
    return user.id;
};

QuasiDiscordUser.prototype.getName = function(){
    return this.discordClient.nickname;
};

QuasiDiscordUser.prototype.getMention = function(){
    return `<@!${this.getDiscordID()}>`;
};

QuasiDiscordUser.prototype.say = function(message, toWaitFor = '',
    channelID = config.discord.test_channel_id){
    if(!message)
        throw new Error('Must submit a command');

    return new Promise(resolve => {
        this.waitForMessage(m => {
            if(!toWaitFor){
                resolve(m);
                return true;
            }
            // console.log(toWaitFor);
            // console.log(m.content);
            if(typeof toWaitFor === typeof m.content){
                if(typeof toWaitFor === 'string' && m.content.includes(toWaitFor)){
                    resolve(m);
                    return true;
                }
                if(typeof toWaitFor === 'object')
                    if(helpers.isDictEqual(m.content, toWaitFor)){
                        // console.log(JSON.stringify(m.content, null, 2));
                        // console.log(JSON.stringify(toWaitFor, null, 2));
                        resolve(m);
                        return true;
                    }
            }
            if(typeof toWaitFor === 'function')
                if(toWaitFor(m)){
                    resolve(m);
                    return true;
                }
        });

        const channel = this.discordClient.channels.cache.get(channelID);
        channel.send(`!${message}`);
    });
};

QuasiDiscordUser.prototype.whisper = async function(message, toWaitFor){
    if(!message)
        throw new Error('Must submit a command');

    return new Promise(async(resolve, reject) => {
        this.waitForMessage(m => {
            if(!toWaitFor){
                resolve(m);
                return true;
            }
            if(typeof toWaitFor === 'function')
                if(toWaitFor(m)){
                    resolve(m);
                    return true;
                }

            if(typeof toWaitFor !== typeof m.content)
                return false;
            if(typeof toWaitFor === 'string' && m.content.includes(toWaitFor)){
                resolve(m);
                return true;
            }
        });

        const discordJsonClient = require('../../channels/discord/discordJsonClient');
        const user = await this.discordClient.users.fetch(discordJsonClient._client.user.id);
        const dm = await user.createDM();
        try{
            await dm.send(message);
        }catch(err){
            reject(err);
        }

        // just in case nothing comes back from the command, but all commands should have feedback
        // right not it appears that "end night" does not have feedback
        if(!toWaitFor)
            setTimeout(resolve, 500);
    });
};

QuasiDiscordUser.prototype.chatter = function(chatMessage,
    channelID = config.discord.test_channel_id){
    return new Promise((resolve, reject) => {
        this.mListeners.push(m => {
            if(typeof m.content === 'string' && m.content.includes(chatMessage)){
                resolve(m);
                return true;
            }
        });

        const channel = this.discordClient.channels.cache.get(channelID);
        channel.send(chatMessage).catch(reject);
    });
};


QuasiDiscordUser.prototype.failChatter = async function(chatMessage, channelID){
    try{
        await this.chatter(chatMessage, channelID);
    }catch(err){
        if(err !== quasiDiscordChannel.SEND_CHANNEL_ERROR)
            throw err;
        return;
    }
    const err = `Successfully sent "${chatMessage}" but shouldn't have.`;
    throw err;
};

QuasiDiscordUser.prototype.subscribe = function(){
    return this.say(Constants.subscribe, 'will let you know if any games');
};

QuasiDiscordUser.prototype.host = async function(channelID){
    await this.say(Constants.host, 'A game is being started by', channelID);
    this.inGame = true;
    return this.getGameObj();
};

QuasiDiscordUser.prototype.join = async function(){
    let conditions = 2;
    await this.say(Constants.join, m => {
        if(m.content === `${this.getName()} has joined the lobby.`)
            conditions--;
        if(m.content.embed && m.content.embed.title === 'Setup Link')
            conditions--;
        return !conditions;
    });
    this.inGame = true;
};

QuasiDiscordUser.prototype.leave = function(channelID, hostLeaving){
    if(!channelID)
        channelID = config.discord.test_channel_id;

    if(hostLeaving)
        return this.say(Constants.leave, 'Game canceled.', channelID);
    return this.say(Constants.leave, `${this.getName()} has left the lobby.`, channelID);
};

QuasiDiscordUser.prototype.setPhaseStart = async function(shouldDayStart){
    const game = await this.getGameObj();
    if(modifierUtil.getValue(game.setup.setupModifiers, 'DAY_START') && !shouldDayStart)
        return this.say(Constants.nightstart, 'Game will now start at nighttime.');
    if(!modifierUtil.getValue(game.setup.setupModifiers, 'DAY_START') && shouldDayStart)
        return this.say(Constants.daystart, 'Game will now start at daytime.');
};

QuasiDiscordUser.prototype.setSetup = async function(setupID){
    const command = `${Constants.setup} ${setupID}`;
    if(setupID === 'custom')
        return this.say(command, 'Setup has been changed to Custom.');

    const setupData = await setupService.getFeaturedSetups();
    for(let i = 0; i < setupData.length; i++)
        if(setupData[i].key === setupID)
            return this.say(command, `Setup has been changed to ${setupData[i].name}.`);

    const error = `setupID not found: ${setupID}`;
    throw error;
};

QuasiDiscordUser.prototype.prefer = function(roleName){
    return this.whisper(`${Constants.prefer} ${roleName}`, 'Prefer noted.');
};

QuasiDiscordUser.prototype.getProfile = async function(){
    const userID = await this.getUserID();
    return profileService.get(userID);
};

QuasiDiscordUser.prototype.getGameObj = async function(){
    const profile = await this.getProfile();
    return gameService.getByID(profile.gameID);
};

QuasiDiscordUser.prototype.start = async function(){
    const gameObj = await this.getGameObj();

    const dayStart = gameObj.setup.setupModifiers
        .find(modifier => modifier.name === 'DAY_START').value;
    await this.say(Constants.start, m => {
        if(m.channel.type !== PRIVATE)
            return;
        // these prefixes come from roleCardUpdate
        if(m.content.startsWith('**Day 1**') && dayStart)
            return true;

        if(m.content.startsWith('**Night 0**') && !dayStart)
            return true;
    });
    // this is a hack because I need to figure out a way for discord to send a message
    // AFTER everything's been done on the discordWrapper's side.
    await new Promise(resolve => {
        setTimeout(resolve, 300);
    });
};

QuasiDiscordUser.prototype.killGame = async function(){
    const profile = await this.getProfile();
    if(!profile.gameID)
        return;

    return new Promise(async resolve => {
        this.waitForMessage(m => {
            if(m.channel.type === PRIVATE)
                return;
            if(m.content === 'Game canceled.'){
                resolve();
                return true;
            }
        });
        await gameService.deleteGame(profile.gameID);
    });
};

QuasiDiscordUser.prototype.moderate = function(){
    return this.say(`${Constants.moderate} on`, 'will be moderating this game.');
};

QuasiDiscordUser.prototype.vote = function(tBot){
    return this.say(`vote ${tBot.getName()}`, `${this.getName()} voted for ${tBot.getName()}`);
};

QuasiDiscordUser.prototype.unvote = function(tBot){
    const waitText = tBot ? `${this.getName()} unvoted ${tBot.getName()}` : ' unvoted ';
    const voteCommand = tBot ? `unvote ${tBot.getName()}` : 'unvote';
    return this.say(voteCommand, waitText);
};

QuasiDiscordUser.prototype.setTrialLength = function(length){
    return this.say(`${Constants.triallength} ${length}`,
        `Trial length changed to ${length} minutes.`);
};

QuasiDiscordUser.prototype.setDiscussionLength = function(length){
    return this.say(`${Constants.discussionlength} ${length}`,
        `Discussion length changed to ${length} minutes.`);
};

QuasiDiscordUser.prototype.setVoteSystem = async function(voteSystem){
    const userID = await this.getUserID();
    return gameService.updateModifier(userID, {
        name: 'VOTE_SYSTEM',
        value: voteSystem,
    });
};

QuasiDiscordUser.prototype.setLastWill = async function(text){
    return this.whisper(`${Constants.lw} ${text}`, 'Last will updated');
};

// this gamestate is deprecated sigh
QuasiDiscordUser.prototype.getGameState = async function(){
    const discordID = this.discordClient.user.id;
    // user should always be here, right?
    const user = await userService.findByExternal(discordID, userTypes.DISCORD);
    return gameService.getUserState(user.id);
};

module.exports = {
    QuasiDiscordUser,
};

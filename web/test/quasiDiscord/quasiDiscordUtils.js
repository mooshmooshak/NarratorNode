const config = require('../../../config');
const { NIGHTTIME } = require('../../models/enums/gamePhase');


async function filterColor(players, color){
    const [gameObj, results] = await Promise.all([
        players[0].getGameObj(),
        Promise.all(players.map(p => p.getProfile())),
    ]);
    const factionID = gameObj.setup.factions.find(f => f.color === color).id;
    const ret = [];
    for(let i = 0; i < results.length; i++)
        try{
            if(results[i].roleCard.factionID === factionID)
                ret.push(players[i]);
        }catch(err){}

    return ret;
}

async function filterRole(players, roleName){
    const ps = [];
    for(let i = 0; i < players.length; i++)
        ps.push(players[i].getGameState());

    const results = await Promise.all(ps);

    const ret = [];
    for(let i = 0; i < results.length; i++)
        try{
            if(results[i].roleInfo.roleName === roleName)
                ret.push(players[i]);
        }catch(err){}


    return ret;
}

async function massEndNight(players){
    const gameObject = await players[0].getGameObj();
    const skipNightPromises = players
        .map(player => player.whisper('end night', `**Day ${gameObject.dayNumber + 1}**`));

    return Promise.all(skipNightPromises);
}

async function massSkipDay(players){
    let gameObject = await players[0].getGameObj();
    for(let i = 0; i < players.length; i++){
        if(gameObject.phase.name === NIGHTTIME)
            return;
        const player = players[i];
        await player.whisper('skip day', `${player.getName()} voted for eliminating no one.`);
        gameObject = await player.getGameObj();
    }
}

function massVote(target, players){
    const votePromises = players
        .map(player => player.vote(target));
    return Promise.all(votePromises);
}

function removeChannelPermission(discordUser, permission){
    const channels = discordUser.discordClient.channels.cache;
    const channel = channels.get(config.discord.home_channel_id).channel;
    channel.permissionOverwrites.put(permission.value, false);
}

module.exports = {
    filterColor,
    filterRole,
    massEndNight,
    massSkipDay,
    massVote,
    removeChannelPermission,
};

const helpers = require('../../../utils/helpers'); /* eslint-disable-line no-unused-vars */

const { ManagerReference } = require('./managerReference');


function QuasiDiscordChannelReference(channel, author, name, options = {}){
    this.author = author;
    this.id = channel.id;
    this.channel = channel;
    this.messages = new ManagerReference();
    this.name = name;
    this.permissionOverwrites = channel.permissionOverwrites;
    this.topic = options.topic;
    this.type = channel.type;

    if(channel.type === 'text')
        this.guild = author.guilds.cache.get(channel.guild.id);
}

QuasiDiscordChannelReference.prototype.createInvite = function(){
    return Promise.resolve(`${this.id}:${this.guild.id}`);
};

// eslint-disable-next-line no-unused-vars
QuasiDiscordChannelReference.prototype.createWebhook = async function(name, options){
    return this.channel._createWebhook(name, this.author.id);
};

QuasiDiscordChannelReference.prototype.createOverwrite = function(entity, permissionObj){
    if(!entity || typeof entity !== 'string'){
        const message = ['bad request', entity];
        return Promise.reject(message);
    }

    return this.channel._createOverwrite(entity, permissionObj);
};

QuasiDiscordChannelReference.prototype.fetchWebhooks = function(){
    return this.channel._fetchWebhooks();
};

QuasiDiscordChannelReference.prototype.send = async function(message){
    const channelMessage = await this.channel.send(message, this);

    /* eslint-disable-next-line no-unused-vars */
    const logMessage = typeof message === 'string'
        ? message : JSON.stringify(message).substring(0, 30);
    let color; /* eslint-disable-line no-unused-vars */
    let prefix; /* eslint-disable-line no-unused-vars */
    if(this.type === 'text'){
        color = 'green';
        prefix = '~~~';
    }else{
        color = 'blue';
        prefix = '~';
    }
    // helpers.colorLog(color, ` ${prefix} ${this.author.nickname}: ${logMessage}`);

    return channelMessage;
};

QuasiDiscordChannelReference.prototype.fetchMessage = function(messageID){
    return Promise.resolve(this.channel.messageHistory[messageID]);
};

QuasiDiscordChannelReference.prototype.delete = function(){};

QuasiDiscordChannelReference.prototype.permissionsFor = function(){
    return this.channel.permissionsFor();
};

module.exports = {
    QuasiDiscordChannelReference,
};

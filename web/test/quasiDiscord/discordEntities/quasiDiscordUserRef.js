const { PRIVATE } = require('../../../channels/discord/enums/channelTypes');


function QuasiDiscordUserRef(client, userClient){
    this._client = client;
    this._userClient = userClient;
    this.presence = {
        status: 'online',
    };
    this.username = userClient.username;
}

QuasiDiscordUserRef.prototype.createDM = async function(){
    return this._client.channels.cache.find(channelRef => channelRef.type === PRIVATE
        && channelRef.channel.clients.find(client => client.id === this._userClient.id));
};

module.exports = {
    QuasiDiscordUserRef,
};

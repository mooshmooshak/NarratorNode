const config = require('../../../../config');

const helpers = require('../../../utils/helpers');

const quasiDiscordChannel = require('./quasiDiscordChannel');
const quasiDiscordGuild = require('./quasiDiscordGuild');

const { Manager } = require('./manager');

const { QuasiDiscordChannel } = quasiDiscordChannel;
const { QuasiDiscordGuild } = quasiDiscordGuild;
const { QuasiDiscordUserRef } = require('./quasiDiscordUserRef');


const homeChannelID = config.discord.test_channel_id;
const testChannel2 = config.discord.test_channel_id2;
const nightChatGuildID = config.discord.night_chat_server;

const narratorClientID = 'narratorClientID';

const nightChatGuild = new QuasiDiscordGuild(nightChatGuildID);
const mainGuild = new QuasiDiscordGuild();

const mainChannel = new QuasiDiscordChannel(homeChannelID, mainGuild, 'home-main-channel');
const mainChannel2 = new QuasiDiscordChannel(testChannel2, mainGuild);

mainGuild.channels._put(homeChannelID, mainChannel);
mainGuild.channels._put(testChannel2, mainChannel2);

let counter = -1;
const clients = {};
const globalGuilds = {
    [nightChatGuildID]: nightChatGuild,
};

function QuasiDiscordClient(){
    this.channels = new Manager();
    this.guilds = new Manager();
    this.listeners = {};
    this.users = new Manager();
    let id;
    if(counter++ === -1){
        id = narratorClientID;
        this.username = 'TheNarrator';
        this.nickname = 'TheNarrator';
        this.id = id;
        nightChatGuild._join(this);
    }else{
        id = `user_${helpers.getRandomString(5)}`;
        this.username = `ogPlayer${counter}`;
        this.nickname = `Player${counter}`;
    }
    this.user = {
        id,
        username: this.username,
    };
    this.id = id;
    mainGuild._join(this);

    Object.values(clients).forEach(client => {
        new QuasiDiscordChannel([this, client]); /* eslint-disable-line no-new */
        client.users._put(this.user.id, new QuasiDiscordUserRef(client, this));
        this.users._put(client.id, new QuasiDiscordUserRef(this, client));
    });

    clients[this.user.id] = this;
}

function resetCounter(){
    counter = 0;
}

QuasiDiscordClient.prototype.createDM = function(){

};

QuasiDiscordClient.prototype.joinGuild = function(guildID, client){
    return globalGuilds[guildID]._join(this, client);
};

QuasiDiscordClient.prototype.login = function(){
    const readyListeners = this.listeners.ready || [];
    readyListeners.forEach(f => {
        f();
    });
};

QuasiDiscordClient.prototype.on = function(eventType, f){
    if(this.listeners[eventType])
        this.listeners[eventType].push(f);
    else
        this.listeners[eventType] = [f];
};

module.exports = {
    Client: QuasiDiscordClient,
    narratorClientID,
    resetCounter,
};

const { Collections: Collection } = require('./collections');

function ManagerReference(_creatorFunc){
    this.cache = new Collection();
    this._creatorFunc = _creatorFunc;
}

ManagerReference.prototype.create = async function(...args){
    return this._creatorFunc(...args);
};

ManagerReference.prototype.fetch = async function(key){
    return this.cache.get(key);
};

ManagerReference.prototype.put = function(key, value){
    this.cache.put(key, value);
};

module.exports = {
    ManagerReference,
};

const helpers = require('../../../utils/helpers');

const { PRIVATE } = require('../../../channels/discord/enums/channelTypes');
const { MANAGE_MESSAGES, MANAGE_WEBHOOKS } = require('../../../channels/discord/enums/permissions');

const Collection = require('./collections').Collections;
const Permissions = require('./permissions').Permissions;

const SEND_CHANNEL_ERROR = 'You can\'t talk here.';


function QuasiDiscordChannel(id, guild, name, options = {}){
    this.clients = [];
    this.guild = guild;
    this.name = name;
    this.topic = options.topic;
    this.messageHistory = {};
    this.webhooks = new Collection();
    this.permissionOverwrites = new Collection();
    this._resetPermissions();
    addPermissionOverwrites(this, options.permissionOverwrites);

    if(typeof id === 'string'){
        this.id = id;
        this.type = 'text';
    }else{
        this.id = `dm_${id[0].id}_${id[1].id}`;
        this.type = PRIVATE;

        this._join(id[0]);
        this._join(id[1]);
    }
}

function addPermissionOverwrites(channel, permissions){
    if(!permissions)
        return;
    // this implementation might not be correct, but it's ok for now
    permissions.forEach(permission => {
        channel.permissionOverwrites.put(permission.id, permission);
    });
}

QuasiDiscordChannel.prototype._canSendMessage = function(id){
    const quasiDiscordClient = require('./quasiDiscordClient');
    if(!Object.keys(this._permittedSpeakers).length) // no restrictions put in this channel
        return true;
    if(id === quasiDiscordClient.narratorClientID)
        return true;
    if(id in this._permittedSpeakers)
        return this._permittedSpeakers[id];
    if(this.guild.roles.everyone.id in this._permittedSpeakers)
        return this._permittedSpeakers[this.guild.roles.everyone.id];
    return false;
};

QuasiDiscordChannel.prototype._editMessage = function(
    oldMessageText, newMessageText, messageID,
    channelReference,
){
    const { author } = channelReference;
    const messageUpdateListeners = getListeners(this.clients, 'messageUpdate');
    this.messageHistory[messageID].content = newMessageText;

    messageUpdateListeners.forEach(clientListenerFunction => {
        const { client } = clientListenerFunction;
        const { listenerFunction } = clientListenerFunction;
        const clientChannel = client.channels.cache.get(channelReference.id);
        listenerFunction({ content: oldMessageText }, {
            _edits: [{ content: oldMessageText }],
            author,
            channel: clientChannel,
            content: newMessageText,
            reply: replyMessage => {
                const message = `<@!${author.user.id}>, ${replyMessage}`;
                return clientChannel.send(message);
            },
        });
    });
};

QuasiDiscordChannel.prototype._join = function(client, name, options){
    const quasiDiscordChannelReference = require('./quasiDiscordChannelReference');
    const { QuasiDiscordChannelReference } = quasiDiscordChannelReference;

    this.clients.push(client);
    const channelReference = new QuasiDiscordChannelReference(this, client, name, options);
    if(this.guild) // this.guild is null if it's a private channel
        client.guilds.cache.get(this.guild.id).channels.put(this.id, channelReference);
    client.channels.cache.put(this.id, channelReference);

    return channelReference;
};

QuasiDiscordChannel.prototype._createOverwrite = async function(entity, permissionObj){
    if('SEND_MESSAGES' in permissionObj)
        this._permittedSpeakers[entity] = permissionObj.SEND_MESSAGES;
};


QuasiDiscordChannel.prototype._createWebhook = async function(name, ownerID){
    const webhook = {
        delete: () => {}, // not really deleting webhooks anymore
        edit: () => {},
        id: `webhook${helpers.getRandomString(5)}`,
        owner: { id: ownerID },
        send: content => this.sendWebhookMessage(name, content, this),
    };
    this.webhooks.put(webhook.id, webhook);
    return webhook;
};

QuasiDiscordChannel.prototype._fetchWebhooks = async function(){
    return this.webhooks;
};

QuasiDiscordChannel.prototype._resetPermissions = function(){
    this._permittedSpeakers = {};

    this.permissionOverwrites.deleteAll();
    this.permissionOverwrites.put('', {
        delete: () => {
            this._resetPermissions();
            return Promise.resolve();
        },
        type: 'member',
    });
};

QuasiDiscordChannel.prototype.permissionsFor = function(){
    return new Permissions(this.permissionOverwrites);
};

QuasiDiscordChannel.prototype.send = function(messageText, channelReference){
    const { author } = channelReference;
    if(!this._canSendMessage(author.id))
        return Promise.reject(SEND_CHANNEL_ERROR);
    return pushMessageOut(author, messageText, this, channelReference);
};

QuasiDiscordChannel.prototype.sendWebhookMessage = function(speakerName, content, channelReference){
    if(!this.permissionsFor().has(MANAGE_WEBHOOKS.value))
        throw new Error('Missing permission');
    return pushMessageOut({ bot: true, name: speakerName }, content, this, channelReference);
};

async function pushMessageOut(author, content, channel, channelReference){
    const messageListeners = getListeners(channel.clients, 'message');
    const messageID = helpers.getRandomString(5);

    const message = {
        author,
        content,
        edit: async newMessageText => channel._editMessage(
            content, newMessageText, messageID, channelReference,
        ),
        channel: channelReference,
        delete: async() => {},
        id: messageID,
    };
    channel.messageHistory[message.id] = message;

    await Promise.all(messageListeners.map(clientListenerFunction => {
        const { client } = clientListenerFunction;
        const { listenerFunction } = clientListenerFunction;
        const clientChannel = client.channels.cache.get(channelReference.id);
        return listenerFunction({
            author: message.author,
            channel: clientChannel,
            content: message.content,
            delete: async() => {
                if(!channel.permissionsFor().has(MANAGE_MESSAGES.value))
                    throw new Error('Missing permission');
            },
            edit: message.edit,
            id: messageID,
            member: {
                nickname: author.nickname,
            },
            reply: replyMessage => {
                replyMessage = `<@!${author.user.id}>, ${replyMessage}`;
                return clientChannel.send(replyMessage);
            },
        });
    }));

    channel.clients.forEach(client => {
        const channelRef = getChannelRef(channel.id, client);
        channelRef.messages.put(message.id, message);
    });

    return Promise.resolve(message);
}

module.exports = {
    QuasiDiscordChannel,

    PUBLIC: 'text',
    SEND_CHANNEL_ERROR,
};

function getListeners(clients, listenerType){
    const listeners = clients
        .map(client => (client.listeners[listenerType] || [])
            .map(listenerFunction => ({
                client,
                listenerFunction,
            })));
    return [].concat(...listeners);
}

function getChannelRef(channelID, client){
    return client.channels.cache.get(channelID);
}

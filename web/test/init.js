// eslint-disable-next-line import/no-extraneous-dependencies
const mock = require('mock-require');

mock('firebase-admin', './fakeFirebase');
mock('discord.js', './quasiDiscord/discordEntities/quasiDiscordClient');
mock('../channels/sc2mafia/vBulletinClient', './fakeVBulletin');

const app = require('../app');

const gameService = require('../services/gameService');

const setupRepo = require('../repos/setupRepo');
const userRepo = require('../repos/userRepo');

const sc2mafiaGameRepo = require('../channels/sc2mafia/repos/sc2mafiaGameRepo');

const channelPrefixRepo = require('../channels/discord/discordRepos/channelCommandsRepo');
const publicChannelRepo = require('../channels/discord/discordRepos/publicChannelsRepo');
const nightChannelsRepo = require('../channels/discord/discordRepos/nightChannelsRepo');

const quasiDiscordClient = require('./quasiDiscord/discordEntities/quasiDiscordClient');


before(async() => {
    await app.start(4502);
    await app.discordWrapper.connect();
    await publicChannelRepo.deleteAll();
});

beforeEach(async() => {
    const games = await gameService.getAll();
    await Promise.all(games.map(game => gameService.deleteGame(game.id)));

    await setupRepo.deleteAll();
    const usersDelete = userRepo.deleteAll();
    const sc2mafiaGamesDelete = sc2mafiaGameRepo.deleteAll();

    await Promise.all([usersDelete, sc2mafiaGamesDelete, channelPrefixRepo.deleteAll(),
        nightChannelsRepo.deleteAll()]);
});

afterEach(() => {
    quasiDiscordClient.resetCounter();
});

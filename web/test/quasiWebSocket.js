const WebSocket = require('ws');
const config = require('../../config');


async function connect(headers){
    const ws = new WebSocket(`ws://localhost:${config.port_number}/napi`);
    await new Promise(resolve => {
        ws.on('open', resolve);
    });
    ws.send(JSON.stringify({ wrapperType: headers.authType, token: headers.auth }));
    const messages = [];
    ws.on('message', data => {
        messages.push(JSON.parse(data));
    });
    return {
        socket: ws,
        messages,
    };
}

module.exports = {
    connect,
};

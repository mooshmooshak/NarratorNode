const userPermissionRepo = require('../../repos/userPermissionRepo');

const userService = require('../../services/userService');

const userPermissions = require('../../models/enums/userPermissions');

const { QuasiDiscordUser } = require('../quasiDiscord/quasiDiscordUser');


async function createMod(){
    const user = await userService.create('someModName');
    await userPermissionRepo.create(user.id, userPermissions.GAME_EDITING);
    await userPermissionRepo.create(user.id, userPermissions.ADMIN);
    return user;
}

function createUser(name = 'userName'){
    return userService.create(name);
}

function createDiscordUser(){
    return new QuasiDiscordUser();
}

function createDiscordUsers(count){
    const ret = [];
    for(let i = 0; i < count; i++)
        ret.push(new QuasiDiscordUser());
    return ret;
}

module.exports = {
    createDiscordUser,
    createDiscordUsers,
    createMod,
    createUser,
};

const { MAX_PLAYER_COUNT } = require('../../utils/constants');

const setupFeaturedRepo = require('../../repos/setupFeaturedRepo');
const setupRepo = require('../../repos/setupRepo');

const setupHiddenService = require('../../services/setup/setupHiddenService');
const setupModifierService = require('../../services/setup/setupModifierService');
const setupService = require('../../services/setup/setupService');

const quasiUser = require('./quasiUser');

const testHelpers = require('../testHelpers');

const { FEATURED_KEY, SETUP_NAME } = require('../fakeConstants');


async function create(args = {}){
    const ownerID = args.ownerID || (await quasiUser.createUser()).id;
    args.name = args.name || SETUP_NAME;
    const setup = await setupService.create(ownerID, args);
    if(setup.errors)
        throw setup.errors;
    if(typeof(args.isActive) !== 'undefined' && !args.isActive)
        await setupRepo.deactivateNoneditableSetups(setup.id, ownerID);
    if(args.priority)
        await setupFeaturedRepo.setPriority(setup.id, args.priority, FEATURED_KEY);
    if(args.modifiers)
        await Promise.all(args.modifiers
            .map(modifier => setupModifierService.createV2(ownerID, setup.id,
                ensurePlayerLimit(modifier))));
    if(args.setupHiddenCount)
        return addSetupHiddensByCount(setup, args.setupHiddenCount);
    if(args.setupHiddens)
        return addSetupHiddens(setup, args.setupHiddens);

    return setup;
}

module.exports = {
    create,
};

async function addSetupHiddensByCount(setup, setupHiddenCount){
    const goonHiddenID = await testHelpers.createHiddenSingle(setup, 'Mafia', 'Goon');
    const citHiddenID = await testHelpers.createHiddenSingle(setup, 'Town', 'Citizen');
    const isExposed = false;
    const mustSpawn = false;
    await setupHiddenService.addV2(setup.ownerID,
        {
            hiddenID: goonHiddenID,
            maxPlayerCount: MAX_PLAYER_COUNT,
            minPlayerCount: 0,
            isExposed,
            mustSpawn,
        });
    for(let i = 1; i < setupHiddenCount; i++)
        await setupHiddenService.addV2(setup.ownerID, {
            hiddenID: citHiddenID,
            maxPlayerCount: MAX_PLAYER_COUNT,
            minPlayerCount: 0,
            isExposed,
            mustSpawn,
        });
    return setupService.getByID(setup.id);
}

async function addSetupHiddens(setup, setupHiddens){
    const hiddenNameMap = {};
    for(let i = 0; i < setupHiddens.length; i++){
        const setupHidden = setupHiddens[i];
        const { factionName, name: roleName } = setupHidden;
        const key = `${factionName}:${roleName}`;
        const hiddenID = key in hiddenNameMap ? hiddenNameMap[key]
            : await testHelpers.createHiddenSingle(setup, setupHidden.factionName,
                setupHidden.name);
        hiddenNameMap[key] = hiddenID;
        await setupHiddenService.addV2(setup.ownerID, {
            hiddenID,
            maxPlayerCount: MAX_PLAYER_COUNT,
            minPlayerCount: 0,
            isExposed: false,
            mustSpawn: false,
        });
    }
}

function ensurePlayerLimit({
    name, value, minPlayerCount, maxPlayerCount,
}){
    return {
        name,
        value,
        minPlayerCount: minPlayerCount || 0,
        maxPlayerCount: maxPlayerCount || MAX_PLAYER_COUNT,
    };
}

const { flattenDeep } = require('lodash');

const { MAX_PLAYER_COUNT } = require('../utils/constants');

const factionRoleService = require('../services/setup/factionRoleService');
const hiddenService = require('../services/setup/hiddenService');
const hiddenSpawnService = require('../services/setup/hiddenSpawnService');
const profileService = require('../services/profileService');
const roleService = require('../services/setup/roleService');
const setupHiddenService = require('../services/setup/setupHiddenService');

const util = require('../utils/helpers');


const defaultArgs = {
    isExposed: false,
    mustSpawn: false,
    minPlayerCount: 0,
    maxPlayerCount: MAX_PLAYER_COUNT,
};

async function createHidden(setup, name, factionRoleID){
    const hidden = await hiddenService.create(setup.ownerID, {
        name,
        setupID: setup.id,
    });
    await hiddenSpawnService.createMany(setup.ownerID, [{
        setupID: setup.id,
        hiddenID: hidden.id,
        factionRoleID,
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
    }]);
    return hidden;
}

// used for setups that are being edited in game
async function addSetupHidden(setup, args){
    const role = await getOrCreateRole(setup, args);
    const faction = setup.factions.find(f => f.name === args.factionName);
    const factionRole = await getOrCreateFactionRole(setup, faction.id, role.id);
    const hidden = await createHidden(setup, role.name, factionRole.id);
    return setupHiddenService.add(setup.ownerID, {
        ...defaultArgs,
        hiddenID: hidden.id,
    });
}

async function addSetupHiddenAny(setup, args = {}){
    const hiddenID = setup.hiddens[0].id;
    args = { ...defaultArgs, ...args, hiddenID };
    return setupHiddenService.addV2(setup.ownerID, args);
}

async function createHiddenSingle(setup, factionName, roleName){
    const faction = setup.factions.find(f => f.name === factionName);
    const role = setup.roles.find(r => r.name === roleName);
    const factionRole = faction.factionRoles.find(fr => fr.roleID === role.id);
    const hiddenID = await hiddenService.createV2(setup.ownerID, setup.id, roleName);
    await hiddenSpawnService.createSpawn(setup.ownerID, setup.id, hiddenID, factionRole.id, 0,
        MAX_PLAYER_COUNT);
    return hiddenID;
}

function getFactionByID(game, factionID){
    return game.setup.factions.find(faction => faction.id === factionID);
}

function getFactionRoleWithName(game, roleName){
    const role = getRoleWithName(game, roleName);
    const factionRolesList = game.setup.factions
        .map(faction => faction.factionRoles
            .filter(factionRole => factionRole.roleID === role.id));
    return util.flattenList(factionRolesList)[0];
}

async function getProfileWithRole(game, roleName){
    const profilePromises = game.users.map(user => profileService.get(user.id));
    const profiles = await Promise.all(profilePromises);
    return profiles.find(profile => profile.roleCard.roleName === roleName);
}

function getRoleWithName(game, roleName){
    return game.setup.roles.find(role => role.name === roleName);
}

function pause(milliseconds){
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

module.exports = {
    createHidden,
    addSetupHidden,
    addSetupHiddenAny,
    getFactionByID,
    getFactionRoleWithName,
    createHiddenSingle,
    getRoleWithName,
    getProfileWithRole,
    pause,
};

function getOrCreateRole(setup, { roleName, roleAbilities }){
    const role = setup.roles.find(r => r.name === roleName);
    if(role)
        return role;
    return roleService.create(setup.ownerID, {
        name: roleName,
        abilities: roleAbilities,
        setupID: setup.id,
    });
}

async function getOrCreateFactionRole(setup, factionID, roleID){
    const factionRoles = flattenDeep(setup.factions.map(f => f.factionRoles));
    return factionRoles.find(fr => fr.roleID === roleID && fr.factionID === factionID)
        || factionRoleService.create(setup.ownerID, {
            factionID,
            roleID,
        });
}

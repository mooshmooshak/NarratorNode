const baseValidator = require('../../validators/baseValidator');


const baseAbilitySchema = {
    type: 'object',
    properties: {
        isFactionAllowed: {
            type: 'boolean',
            required: true,
        },
        type: {
            type: 'string',
            required: true,
        },
    },
};

function check(query){
    baseValidator.validate(query, baseAbilitySchema);
}

module.exports = {
    check,
};

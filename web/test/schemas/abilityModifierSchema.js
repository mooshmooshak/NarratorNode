const baseValidator = require('../../validators/baseValidator');


const abilityModifierDefinition = {
    label: {
        type: 'string',
        minLength: 1,
        required: true,
    },
    name: {
        type: 'string',
        minLength: 1,
        required: true,
    },
    minPlayerCount: {
        type: 'number',
        required: true,
    },
    maxPlayerCount: {
        type: 'number',
        required: true,
    },
    upsertedAt: {
        type: 'date-time',
        required: true,
    },
};

const abilityModifierSchema = {
    type: 'object',
    properties: abilityModifierDefinition,
};

function check(query){
    baseValidator.validate(query, abilityModifierSchema);
}

module.exports = {
    abilityModifierDefinition,
    check,
};

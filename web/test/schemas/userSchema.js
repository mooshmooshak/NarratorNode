const baseValidator = require('../../validators/baseValidator');


const userDefinition = {
    id: {
        type: 'number',
        required: true,
    },
    name: {
        type: 'string',
        required: true,
    },
};

const userSchema = {
    type: 'object',
    properties: userDefinition,
};

function check(query){
    baseValidator.validate(query, userSchema);
}

module.exports = {
    check,
    userDefinition,
};

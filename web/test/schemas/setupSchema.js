const { roleDefinition } = require('./roleSchema');


const setupDefinition = {
    isEditable: {
        type: 'boolean',
        required: true,
    },
    roles: {
        type: 'array',
        items: {
            type: 'object',
            properties: roleDefinition,
            required: true,
        },
        required: true,
    },
};


module.exports = {
    setupDefinition,
};

const baseValidator = require('../../validators/baseValidator');

const iterationSchema = {
    type: 'array',
    items: {
        type: 'object',
        properties: {
            setupHiddenID: {
                type: 'number',
                required: true,
            },
        },
        required: true,
    },
};

function check(query){
    baseValidator.validate(query, iterationSchema);
}

module.exports = {
    check,
};

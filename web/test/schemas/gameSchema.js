const baseValidator = require('../../validators/baseValidator');

const { phaseDefinition } = require('./phaseSchema');
const { playerInGameDefinition } = require('./playerSchema');
const { setupDefinition } = require('./setupSchema');
const { userDefinition } = require('./userSchema');


function getGameSchema(playerDefinition){
    return {
        type: 'object',
        properties: {
            integrations: {
                type: 'array',
                required: true,
            },
            phase: {
                type: 'object',
                properties: phaseDefinition,
                required: true,
            },
            players: {
                type: 'array',
                items: {
                    type: 'object',
                    properties: playerDefinition,
                    required: true,
                },
                required: true,
            },
            setup: {
                type: 'object',
                properties: setupDefinition,
                required: true,
            },
            users: {
                type: 'array',
                items: {
                    type: 'object',
                    required: true,
                    properties: userDefinition,
                },
                required: true,
            },
        },
    };
}

const gameCreateEventSchema = {
    type: 'object',
    properties: {
        event: {
            type: 'string',
            required: true,
        },
        modifiers: {
            type: 'object',
            required: true,
        },
        phase: {
            type: 'object',
            properties: phaseDefinition,
            required: true,
        },
        players: {
            type: 'array',
            items: {
                type: 'object',
                required: true,
            },
            required: true,
        },
        setup: {
            type: 'object',
            properties: setupDefinition,
            required: true,
        },
        users: {
            type: 'array',
            items: {
                type: 'object',
                required: true,
                properties: userDefinition,
            },
            required: true,
        },
    },
};

const gameDeleteEventSchema = {
    type: 'object',
    properties: {
        event: {
            type: 'string',
            required: true,
        },
        gameID: {
            type: 'number',
            required: true,
        },
    },
};

function checkNotStarted(query){
    baseValidator.validate(query, getGameSchema({}));
}

function checkInGame(query){
    baseValidator.validate(query, getGameSchema(playerInGameDefinition));
}

function checkGameCreateEvent(query){
    baseValidator.validate(query, gameCreateEventSchema);
}

function checkGameDeleteEvent(query){
    baseValidator.validate(query, gameDeleteEventSchema);
}

module.exports = {
    checkNotStarted,
    checkInGame,
    checkGameCreateEvent,
    checkGameDeleteEvent,
};

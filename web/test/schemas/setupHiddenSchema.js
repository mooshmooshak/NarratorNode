const baseValidator = require('../../validators/baseValidator');


const setupHiddenDefinition = {
    id: {
        type: 'number',
        required: true,
    },
    isExposed: {
        type: 'boolean',
        required: true,
    },
    mustSpawn: {
        type: 'boolean',
        required: true,
    },
    maxPlayerCount: {
        required: true,
        type: 'number',
    },
    minPlayerCount: {
        required: true,
        type: 'number',
    },
};

const setupHidden = {
    type: 'object',
    properties: setupHiddenDefinition,
};

const setupHiddenAddEvent = {
    type: 'object',
    properties: {
        setupHidden: {
            type: 'object',
            required: true,
            properties: setupHiddenDefinition,
        },
    },
};

function check(query){
    baseValidator.validate(query, setupHidden);
}

function checkAddEvent(query){
    baseValidator.validate(query, setupHiddenAddEvent);
}

module.exports = {
    check,
    checkAddEvent,
};

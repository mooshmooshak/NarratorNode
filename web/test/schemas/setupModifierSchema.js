const baseValidator = require('../../validators/baseValidator');


const setupModifierDefinition = {
    name: {
        type: 'string',
        minLength: 1,
        required: true,
    },
    minPlayerCount: {
        type: 'number',
        required: true,
    },
    maxPlayerCount: {
        type: 'number',
        required: true,
    },
    upsertedAt: {
        type: 'date-time',
        required: true,
    },
};

const setupModifierSchema = {
    type: 'object',
    properties: setupModifierDefinition,
};

function check(query){
    baseValidator.validate(query, setupModifierSchema);
}

module.exports = {
    check,
};

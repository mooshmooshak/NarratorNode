const baseValidator = require('../../validators/baseValidator');


const setupOverviewSchema = {
    type: 'object',
    properties: {
        id: {
            type: 'number',
            required: true,
        },
        key: {
            type: 'string',
            required: true,
        },
        name: {
            type: 'string',
            required: true,
        },
        priority: {
            type: 'number',
            required: true,
        },
    },
};

function check(query){
    baseValidator.validate(query, setupOverviewSchema);
}

module.exports = {
    check,
};

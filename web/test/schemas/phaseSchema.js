const phaseDefinition = {
    dayNumber: {
        type: 'number',
        required: true,
    },
};

module.exports = {
    phaseDefinition,
};

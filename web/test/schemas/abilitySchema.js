const { abilityModifierDefinition } = require('./abilityModifierSchema');


const abilityDefinition = {
    id: {
        type: 'number',
        required: true,
    },
    modifiers: {
        type: 'array',
        items: {
            type: 'object',
            properties: abilityModifierDefinition,
            required: true,
        },
        required: true,
    },
    name: {
        type: 'string',
        required: true,
    },
    setupModifierNames: {
        type: 'array',
        items: {
            type: 'string',
        },
    },
};

module.exports = {
    abilityDefinition,
};

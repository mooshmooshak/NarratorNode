require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const gamePhase = require('../../../models/enums/gamePhase');
const setupModifiers = require('../../../models/enums/setupModifiers');
const voteSystemTypes = require('../../../models/enums/voteSystemTypes');

const actionService = require('../../../services/actionService');
const gameService = require('../../../services/gameService');
const setupModifierService = require('../../../services/setup/setupModifierService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');

const testHelpers = require('../../testHelpers');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Phases', () => {
    let clock;
    let setup;
    beforeEach(async() => {
        clock = sinon.useFakeTimers();
        setup = await quasiSetup.create({
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }],
            setupHiddenCount: 8,
        });
    });

    afterEach(() => {
        clock.restore();
    });

    it('Should end the role picking phase', async() => {
        const rolePickingLength = 1;
        const discussionLength = 1;
        let gameObj = await quasiGame.create({
            hostID: setup.ownerID,
            setupID: setup.id,
            playerCount: 5,
        });
        await testHelpers.addSetupHidden(gameObj.setup, {
            factionName: 'Benigns',
            roleName: 'Thief',
            roleAbilities: ['Thief'],
        });
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await setPluralityVoting(gameObj);
        await setRolePickingMinuteLength(gameObj, rolePickingLength);
        await setupModifierService.create(setup.ownerID, {
            name: 'DAY_START',
            value: true,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        });
        await gameService.start(gameObj.id);

        advanceMinutes(rolePickingLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_CLOSED);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should not end the role picking phase if time hasn\'t run out', async() => {
        const rolePickingLength = 2;
        let gameObj = await quasiGame.create({
            hostID: setup.ownerID,
            setupID: setup.id,
            playerCount: 5,
        });
        await testHelpers.addSetupHidden(gameObj.setup, {
            factionName: 'Benigns',
            roleName: 'Thief',
            roleAbilities: ['Thief'],
        });
        await setPluralityVoting(gameObj);
        await setRolePickingMinuteLength(gameObj, rolePickingLength);
        await gameService.start(gameObj.id);

        advanceMinutes(rolePickingLength / 2);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.ROLE_PICKING_PHASE);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should end the discussion phase', async() => {
        const discussionLength = 5;
        let gameObj = await quasiGame.create({ playerCount: 7, setupID: setup.id });
        expect(gameObj.setup.setupHiddens.length).to.be.equal(8);
        await setPluralityVoting(gameObj);
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await gameService.start(gameObj.id);

        advanceMinutes(discussionLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should auto end the night', async() => {
        const nightLength = 44;
        const discussionLength = 0;
        let gameObj = await quasiGame.create({ playerCount: 8 });
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await setNightMinuteLength(gameObj, nightLength);
        await gameService.start(gameObj.id);

        advanceMinutes(nightLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should not auto the next phase if everyone ended night', async() => {
        const nightLength = 44;
        const discussionLength = nightLength * 2;
        let gameObj = await quasiGame.create({ playerCount: 8 });
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await setNightMinuteLength(gameObj, nightLength);
        await gameService.start(gameObj.id);

        await Promise.all(gameObj.players.map(endNight));
        advanceMinutes(nightLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_CLOSED);
        expect(gameObj.dayNumber).to.be.equal(1);
    });

    it('Should keep the game in the voting phase if there\'s a tie', async() => {
        const discussionLength = 0;
        const dayLength = 50;
        let gameObj = await quasiGame.create({
            hostID: setup.ownerID,
            playerCount: 7,
            setupID: setup.id,
        });
        await setDayMinuteLength(gameObj, dayLength);
        await setDiscussionMinuteLength(gameObj, discussionLength);
        await gameService.start(gameObj.id);

        await submitVote(gameObj.players[0], gameObj.players[1]);
        await submitVote(gameObj.players[1], gameObj.players[0]);
        advanceMinutes(dayLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);

        await submitVote(gameObj.players[2], gameObj.players[1]);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.NIGHTTIME);
    });

    it('Should decrease day length on subsequent days', async() => {
        const dayLength = 50;
        const decrease = 5;
        let gameObj = await quasiGame.create({
            hostID: setup.ownerID,
            playerCount: 7,
            setupID: setup.id,
        });
        await setDayMinuteLength(gameObj, dayLength);
        await setDayDecreaseLength(gameObj, decrease);
        gameObj = await gameService.start(gameObj.id);
        const citizens = gameObj.players.filter(p => p.factionRole.name === 'Citizen');

        await submitVote(citizens[0], citizens[1]);
        await advanceMinutes(dayLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.NIGHTTIME);

        await Promise.all(gameObj.players.filter(p => !p.flip).map(endNight));
        gameObj = await gameService.getByLobbyID(gameObj.joinID);
        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);

        await submitVote(citizens[2], citizens[3]);
        await advanceMinutes(dayLength - decrease);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.NIGHTTIME);
    });

    it('Should end trials', async() => {
        const discussionLength = 0;
        const dayLength = 50;
        const trialLength = 22;
        let gameObj = await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            playerCount: 8,
        });
        await Promise.all([
            setDiscussionMinuteLength(gameObj, discussionLength),
            setDayMinuteLength(gameObj, dayLength),
            setTrialMinuteLength(gameObj, trialLength),
            gameService.updateModifier(setup.ownerID,
                { name: 'VOTE_SYSTEM', value: voteSystemTypes.DIMINISHING }),
        ]);
        await gameService.start(gameObj.id);

        await submitVote(gameObj.players[0], gameObj.players[2]);
        await submitVote(gameObj.players[1], gameObj.players[2]);
        advanceMinutes(dayLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.TRIALS);
        expect(gameObj.dayNumber).to.be.equal(1);

        await advanceMinutes(trialLength);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(gameObj.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
        expect(gameObj.dayNumber).to.be.equal(1);

        await submitVote(gameObj.players[2], gameObj.players[0]);
        await submitVote(gameObj.players[1], gameObj.players[0]);
    });

    function advanceMinutes(minutes){
        clock.tick(minutes * 60 * 1000);
    }
});

function setPluralityVoting(gameObj){
    const userID = gameObj.users.find(user => user.isModerator).id;
    return gameService.updateModifier(userID, {
        name: 'VOTE_SYSTEM',
        value: voteSystemTypes.PLURALITY,
    });
}

function setDayMinuteLength(gameObj, minutes){
    const userID = gameObj.users.find(user => user.isModerator).id;
    const value = minutes * 60;
    return gameService.updateModifier(userID, { name: 'DAY_LENGTH_START', value });
}

function setDayDecreaseLength(gameObj, minutes){
    const userID = gameObj.users.find(user => user.isModerator).id;
    const value = minutes * 60;
    return gameService.updateModifier(userID, { name: 'DAY_LENGTH_DECREASE', value });
}

function setTrialMinuteLength(gameObj, minutes){
    const userID = gameObj.users.find(user => user.isModerator).id;
    const value = minutes * 60;
    return gameService.updateModifier(userID, { name: 'TRIAL_LENGTH', value });
}

function setNightMinuteLength(gameObj, minutes){
    const userID = gameObj.users.find(user => user.isModerator).id;
    const value = minutes * 60;
    return gameService.updateModifier(userID, { name: 'NIGHT_LENGTH', value });
}

function setDiscussionMinuteLength(gameObj, minutes){
    const userID = gameObj.users.find(user => user.isModerator).id;
    const value = minutes * 60;
    return gameService.updateModifier(userID, { name: 'DISCUSSION_LENGTH', value });
}

function setRolePickingMinuteLength(gameObj, minutes){
    const userID = gameObj.users.find(user => user.isModerator).id;
    const value = minutes * 60;
    return gameService.updateModifier(userID, { name: 'ROLE_PICKING_LENGTH', value });
}

function endNight(player){
    return actionService.submitActionMessage(player.userID, { message: 'end night' });
}

async function submitVote(voter, voted){
    const { userID } = voter;
    const message = `vote ${voted.name}`;
    return actionService.submitActionMessage(userID, { message });
}

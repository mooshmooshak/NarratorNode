require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const {
    JOIN_ID,
    PLAYER_NAME,
    SC2MAFIA_GAME_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
    SC2MAFIA_SETUP_THREAD_ID,
    SC2MAFIA_POST_ID,
} = require('../../fakeConstants');


describe('Sc2mafia Game End', () => {
    let getThreadStub;
    let newPostStub;

    afterEach(() => {
        getThreadStub.restore();
        newPostStub.restore();
    });

    it('Should announce who won', async() => {
        getThreadStub = sinon.stub(vBulletinClient, 'getThread');
        getThreadStub.onCall(0).returns({
            FIRSTPOSTID: SC2MAFIA_POST_ID,
        });
        newPostStub = sinon.stub(vBulletinClient, 'newPost');
        await sc2mafiaGameRepo.insert(JOIN_ID,
            SC2MAFIA_SETUP_THREAD_ID,
            SC2MAFIA_SIGNUP_THREAD_ID);
        await sc2mafiaGameRepo.updateGameThreadID(JOIN_ID, SC2MAFIA_GAME_THREAD_ID);
        const request = {
            joinID: JOIN_ID,
            winners: [{
                name: PLAYER_NAME,
            }],
        };
        const expectedMessage = `${PLAYER_NAME} has won!`;

        await sc2mafiaClient.onGameEnd(request);
        const sc2Game = await sc2mafiaGameRepo.getByJoinID(JOIN_ID);

        expect(newPostStub.getCall(0).args[0].threadid).to.be.equal(SC2MAFIA_GAME_THREAD_ID);
        expect(newPostStub.getCall(0).args[0].message).to.be.equal(expectedMessage);
        expect(sc2Game).to.be.a('null');
    });
});

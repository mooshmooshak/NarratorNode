require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const quasiGame = require('../../quasiModels/quasiGame');

const fakeConstants = require('../../fakeConstants');


describe('Sc2mafia Game Posting', async() => {
    let newThreadStub;
    let sendPMStub;
    afterEach(() => {
        newThreadStub.restore();
        sendPMStub.restore();
    });

    it('Should post the current game setup to the thread', async() => {
        newThreadStub = sinon.stub(vBulletinClient, 'newThread');
        newThreadStub.onCall(0).returns({ threadid: fakeConstants.SC2MAFIA_SETUP_THREAD_ID });
        newThreadStub.onCall(1).returns({ threadid: fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID });
        sendPMStub = sinon.stub(vBulletinClient, 'sendPrivateMessage');
        const game = await quasiGame.create();
        const request = {
            externalUserID: fakeConstants.EXTERNAL_USER_ID,
            name: fakeConstants.PLAYER_NAME,
        };
        const expectedResponse = {
            setupThreadID: fakeConstants.SC2MAFIA_SETUP_THREAD_ID,
            signupThreadID: fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID,
        };
        const exepctedRepoResponse = {
            setupThreadID: fakeConstants.SC2MAFIA_SETUP_THREAD_ID,
            signupThreadID: fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID,
            gameThreadID: null,
            joinID: game.joinID,
            latestPostID: 1,
        };
        const authToken = fakeConstants.AUTH_TOKEN;

        const externalInfo = await sc2mafiaClient.postGame(request, game, authToken);
        const repoExternalInfo = await sc2mafiaGameRepo.getByJoinID(game.joinID);

        expect(externalInfo).to.eql(expectedResponse);
        expect(repoExternalInfo).to.eql(exepctedRepoResponse);
        expect(sendPMStub.getCall(0).args[0].recipients).to.be.equal(request.name);
    });
});

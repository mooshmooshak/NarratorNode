require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const setupModifiers = require('../../../models/enums/setupModifiers');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');

const postModel = require('../../quasiSc2mafia/postModel');
const threadModel = require('../../quasiSc2mafia/threadModel');

const {
    EXTERNAL_USER_ID,
    JOIN_ID,
    PLAYER_NAME,
    SC2MAFIA_GAME_THREAD_ID,
    SC2MAFIA_POST_ID,
    SC2MAFIA_SETUP_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
} = require('../../fakeConstants');


describe('Sc2mafia New Post', async() => {
    let getThreadStub;

    beforeEach(() => {
        getThreadStub = sinon.stub(vBulletinClient, 'getThread');
    });

    afterEach(() => {
        getThreadStub.restore();
    });

    it('Should detect new player signs', async() => {
        const newPostID = SC2MAFIA_POST_ID * 100;
        applyStub(getThreadStub, threadModel.get({
            posts: [
                postModel.get({
                    externalUserID: PLAYER_NAME,
                    text: '/sign',
                    id: newPostID,
                }),
            ],
        }));
        const threadID = SC2MAFIA_SIGNUP_THREAD_ID;
        const postID = SC2MAFIA_POST_ID;
        await sc2mafiaGameRepo.insert(JOIN_ID,
            SC2MAFIA_SETUP_THREAD_ID, threadID, postID);
        const expectedCall = { threadid: threadID, postid: postID };

        const response = await sc2mafiaClient.onNewPost(threadID);
        const setup = await sc2mafiaGameRepo.getByThreadID(threadID);

        expect(response.playerJoins.length).to.be.equal(1);
        expect(response.playerJoins[0].externalID).to.be.equal(PLAYER_NAME);
        expect(response.playerJoins[0].joinID).to.be.equal(JOIN_ID);
        expect(response.playerJoins[0].name).to.be.equal(PLAYER_NAME);
        expect(getThreadStub.getCall(0).calledWith(expectedCall)).to.be.equal(true);
        expect(setup.latestPostID).to.be.equal(newPostID);
    });

    it('Should not redetect player signs', async() => {
        const newPostID = SC2MAFIA_POST_ID * 100;
        applyStub(getThreadStub, threadModel.get({
            posts: [
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID,
                    text: '/sign',
                    id: newPostID,
                }),
            ],
        }));
        const threadID = SC2MAFIA_SIGNUP_THREAD_ID;
        await sc2mafiaGameRepo.insert(JOIN_ID,
            SC2MAFIA_SETUP_THREAD_ID, threadID, newPostID);
        const expectedCall = { threadid: threadID, postid: newPostID };

        const response = await sc2mafiaClient.onNewPost(threadID);
        const setup = await sc2mafiaGameRepo.getByThreadID(threadID);

        expect(response.playerJoins.length).to.be.equal(0);
        expect(getThreadStub.getCall(0).calledWith(expectedCall)).to.be.equal(true);
        expect(setup.latestPostID).to.be.equal(newPostID);
    });

    it('Should detect votes', async() => {
        const setup = await quasiSetup.create({
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }],
            setupHiddenCount: 7,
        });
        const game = await quasiGame.create({
            isStarted: true,
            setupID: setup.id,
            playerCount: 7,
        });
        const voteTarget = game.users.find(user => user.name).name;
        const newPostID = SC2MAFIA_POST_ID * 100;
        applyStub(getThreadStub, threadModel.get({
            posts: [
                postModel.get({
                    externalUserID: PLAYER_NAME,
                    text: `[vOtE] ${voteTarget}[/VoTe]`,
                    id: newPostID,
                }),
            ],
        }));
        const threadID = SC2MAFIA_GAME_THREAD_ID;
        await sc2mafiaGameRepo.insert(JOIN_ID,
            SC2MAFIA_SETUP_THREAD_ID, threadID, SC2MAFIA_POST_ID);

        const response = await sc2mafiaClient.onNewPost(threadID);
        const setupThread = await sc2mafiaGameRepo.getByThreadID(threadID);

        expect(response.playerVotes.length).to.be.equal(1);
        expect(response.playerVotes[0].externalID).to.be.equal(PLAYER_NAME);
        expect(response.playerVotes[0].voteTarget).to.be.equal(voteTarget);
        expect(response.playerVotes[0].joinID).to.be.equal(JOIN_ID);
        expect(setupThread.latestPostID).to.be.equal(newPostID);
    });
});

function applyStub(stub, thread){
    for(let i = 0; i < thread.getPageCount(); ++i)
        stub.onCall(i).returns(thread.getPage(i + 1)); // pages are 1 indexed, not 0
}

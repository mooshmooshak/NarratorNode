require('../../init');

const { expect } = require('chai');
const moment = require('moment');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const fakeConstants = require('../../fakeConstants');


describe('Sc2mafia Game Delta Detection', async() => {
    const pmID = fakeConstants.SC2MAFIA_PM_ID;
    const pmID2 = `${fakeConstants.SC2MAFIA_PM_ID}2`;
    const expectedUserID = fakeConstants.EXTERNAL_USER_ID;
    const expectedPlayerName = fakeConstants.PLAYER_NAME;
    let getPMsStub;
    let getPMStub;

    afterEach(() => {
        getPMsStub.restore();
        getPMStub.restore();
    });

    it('Should collect a new delta', async() => {
        getPMsStub = sinon.stub(vBulletinClient, 'getPrivateMessages');
        getPMsStub.onCall(0).returns(getExamplePMsCall(pmID));
        getPMStub = sinon.stub(vBulletinClient, 'getPrivateMessage');
        getPMStub.onCall(0).returns(getPM());
        const now = new Date();

        const requests = await sc2mafiaClient.findGameDeltas(now);

        expect(requests.length).to.be.equal(1);
        expect(requests[0].name).to.be.equal(expectedPlayerName);
        expect(requests[0].externalUserID).to.be.equal(expectedUserID);
        expect(getPMsStub.calledWith()).to.be.equal(true);
        expect(getPMStub.getCall(0).calledWith({ pmid: pmID })).to.be.equal(true);
    });

    it('Should not collect old requests', async() => {
        getPMsStub = sinon.stub(vBulletinClient, 'getPrivateMessages');
        getPMsStub.onCall(0).returns(getExamplePMsCall(pmID));
        getPMStub = sinon.stub(vBulletinClient, 'getPrivateMessage');
        getPMStub.onCall(0).returns(getPM({ offset: -1 }));
        const now = new Date();

        const requests = await sc2mafiaClient.findGameDeltas(now);

        expect(requests.length).to.be.equal(0);
    });

    it('Should handle multi message groups', async() => {
        getPMsStub = sinon.stub(vBulletinClient, 'getPrivateMessages');
        getPMsStub.onCall(0).returns(getExamplePMsCall([pmID]));
        getPMStub = sinon.stub(vBulletinClient, 'getPrivateMessage');
        getPMStub.onCall(0).returns(getPM());
        const now = new Date();

        const requests = await sc2mafiaClient.findGameDeltas(now);

        expect(requests.length).to.be.equal(1);
    });

    it('Should not output duplicate game requests', async() => {
        getPMsStub = sinon.stub(vBulletinClient, 'getPrivateMessages');
        getPMsStub.onCall(0).returns(getExamplePMsCall(pmID, pmID2));
        getPMStub = sinon.stub(vBulletinClient, 'getPrivateMessage');
        getPMStub.onCall(0).returns(getPM({ pmID, externalUserID: expectedUserID }));
        getPMStub.onCall(1).returns(getPM({ pmID: pmID2, externalUserID: expectedUserID }));
        const now = new Date();

        const requests = await sc2mafiaClient.findGameDeltas(now);

        expect(requests.length).to.be.equal(1);
    });
});

function getExamplePMsCall(...args){
    const periodGroups = [];
    for(let i = 0; i < args.length; i++){
        const pmID = args[i];
        if(!Array.isArray(pmID)){
            periodGroups.push({
                messagelistbits: {
                    pm: {
                        pmid: pmID,
                    },
                },
            });
            continue;
        }

        const multiListBits = pmID.map(innerPMID => ({
            pm: {
                pmid: innerPMID,
            },
        }));
        periodGroups.push({ messagelistbits: multiListBits });
    }
    return {
        response: {
            HTML: {
                messagelist_periodgroups: periodGroups,
            },
        },
    };
}

function getPM(pm){
    pm = pm || {};
    pm.pmID = pm.pmID || fakeConstants.SC2MAFIA_PM_ID;
    pm.message = pm.message || '/host';
    pm.username = pm.username || fakeConstants.PLAYER_NAME;
    pm.externalUserID = pm.externalUserID || fakeConstants.EXTERNAL_USER_ID;
    pm.offset = pm.offset || 1;
    return {
        response: {
            HTML: {
                pm: {
                    pmid: pm.pmID,
                },
                postbit: {
                    post: {
                        message: pm.message,
                        username: pm.username,
                        userid: pm.externalUserID,
                        posttime: moment().add(pm.offset, 'm').toDate().getTime() / 1000,
                    },
                },
            },
        },
    };
}

require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const fakeConstants = require('../../fakeConstants');


describe('Sc2mafia Thread Cleanup', async() => {
    let createPostStub;
    afterEach(() => {
        createPostStub.restore();
    });

    it('Should delete threads when a lobby closes', async() => {
        createPostStub = sinon.stub(vBulletinClient, 'newPost');
        const request = {
            joinID: fakeConstants.JOIN_ID,
        };
        await sc2mafiaGameRepo.insert(fakeConstants.JOIN_ID,
            fakeConstants.SC2MAFIA_SETUP_THREAD_ID,
            fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID);

        await sc2mafiaClient.onLobbyClose(request);
        const results = await sc2mafiaGameRepo.getByJoinID(request.joinID);

        expect(results).to.be.a('null');
        expect(createPostStub.calledWithExactly({
            message: 'This game has been abandoned.',
            signature: true,
            threadid: fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID,
        })).to.equal(true);
        expect(createPostStub.calledWithExactly({
            message: 'This setup has been abandoned.',
            signature: true,
            threadid: fakeConstants.SC2MAFIA_SETUP_THREAD_ID,
        })).to.equal(true);
    });
});

require('../../init');
const { expect } = require('chai');
const authHelpers = require('../../authHelpers');

const gameService = require('../../../services/gameService');

const requests = require('../../requests');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiWebSocket = require('../../quasiWebSocket');


describe('Vote event', () => {
    let ws;

    afterEach(() => {
        if(ws)
            ws.close();
    });

    it('Will send a vote update event on vote', async() => {
        const setup = await quasiSetup.create({
            setupHiddenCount: 3,
            modifiers: [{
                name: 'DAY_START',
                value: true,
            }],
        });
        const game = await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            playerCount: 3,
        });
        const { players } = await gameService.start(game.id);
        const playerName = players.find(player => player.userID !== setup.ownerID).name;
        const message = { command: 'vote', targets: [playerName] };
        const headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        const messages = await addWebSocket(headers);

        const serverResponse = await requests.put('api/actions', message, headers);
        await new Promise(resolve => {
            setTimeout(resolve, 100);
        });

        expect(serverResponse.statusCode).to.equal(200);
        const updateMessage = messages.find(m => m.event === 'voteUpdate');
        expect(updateMessage).to.be.an('object');
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});

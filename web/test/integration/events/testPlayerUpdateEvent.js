require('../../init');
const authHelpers = require('../../authHelpers');
const { expectStatusCode } = require('../../assertions');

const gameService = require('../../../services/gameService');

const playerSchema = require('../../schemas/playerSchema');

const requests = require('../../requests');
const testHelpers = require('../../testHelpers');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiWebSocket = require('../../quasiWebSocket');


describe('Player update event', () => {
    let ws;

    afterEach(() => {
        if(ws)
            ws.close();
    });

    it('Will send a player update event on mayor reveal', async() => {
        const setup = await quasiSetup.create({
            setupHiddenCount: 2,
            modifiers: [{
                name: 'DAY_START',
                value: true,
            }],
        });
        const game = await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            playerCount: 3,
        });
        await testHelpers.addSetupHidden(setup, { factionName: 'Town', roleName: 'Mayor' });
        const gameObj = await gameService.start(game.id);
        const mayorProfile = await testHelpers.getProfileWithRole(gameObj, 'Mayor');
        const message = { command: 'reveal', targets: [] };
        const headers = authHelpers.fakeTempAuthHeader(mayorProfile.userID);
        const messages = await addWebSocket(headers);

        const serverResponse = await requests.put('api/actions', message, headers);
        await new Promise(resolve => {
            setTimeout(resolve, 100);
        });

        expectStatusCode(200, serverResponse);
        const updateMessage = messages.find(m => m.event === 'playerUpdate');
        playerSchema.checkInGame(updateMessage.player);
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});

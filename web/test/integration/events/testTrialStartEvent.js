require('../../init');
const { expect } = require('chai');
const authHelpers = require('../../authHelpers');

const gameService = require('../../../services/gameService');

const requests = require('../../requests');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiWebSocket = require('../../quasiWebSocket');

const voteSystemType = require('../../../models/enums/voteSystemTypes');


describe('Trial start event', () => {
    let ws;

    afterEach(() => {
        if(ws)
            ws.close();
    });

    it('Will send a trial start event when trial starts', async() => {
        const setup = await quasiSetup.create({
            setupHiddenCount: 3,
            modifiers: [{
                name: 'DAY_START',
                value: true,
            }],
        });
        const game = await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            modifiers: [{
                name: 'VOTE_SYSTEM',
                value: voteSystemType.MULTIVOTE_PLURALITY,
            }, {
                name: 'TRIAL_LENGTH',
                value: 3000,
            }],
            playerCount: 3,
        });
        const { players } = await gameService.start(game.id);
        const playerName = players[2].name;
        const message = { command: 'vote', targets: [playerName] };
        const messages = await addWebSocket(authHelpers.fakeTempAuthHeader(players[2].userID));

        await requests.put('api/actions', message,
            authHelpers.fakeTempAuthHeader(players[0].userID));
        await requests.put('api/actions', message,
            authHelpers.fakeTempAuthHeader(players[1].userID));
        await new Promise(resolve => {
            setTimeout(resolve, 100);
        });

        const updateMessage = messages.find(m => m.event === 'trialStart');
        expect(updateMessage).to.be.an('object');
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});

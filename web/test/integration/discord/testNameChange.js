require('../../init');
const { expect } = require('chai');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Name change', () => {
    it('Will report errors when changing a name to something unallowed', async() => {
        const host = await quasiUser.createDiscordUser();
        await host.host();

        const response = await host.say(`${Constants.name} ${host.getName()}.2`);

        expect(response.content).to.contain('Names must be alphanumeric.');
    });
});

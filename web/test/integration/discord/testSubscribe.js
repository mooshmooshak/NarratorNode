require('../../init');

const { expect } = require('chai');

const userService = require('../../../services/userService');

const userType = require('../../../models/enums/userTypes');

const subscribersRepo = require('../../../channels/discord/discordRepos/subscribersRepo');

const quasiUser = require('../../quasiModels/quasiUser');


describe('Subscribe', async() => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
    });

    afterEach(async() => {
        await host.killGame();
    });

    it('Should tell new subscribers of new games', async() => {
        const subscriber = await quasiUser.createDiscordUser();
        await subscriber.subscribe();

        await new Promise(resolve => {
            subscriber.waitForMessage(m => {
                if(m.channel.type !== 'dm')
                    return false;
                return m.content.includes('has started a Narrator Mafia Game');
            }).then(resolve);
            host.host();
        });
    });

    it('Should tell previously opted subscribers of new games', async() => {
        const subscriber = await quasiUser.createDiscordUser();
        await subscriber.subscribe();
        await subscriber.subscribe();
        const user = await userService.findByExternal(subscriber.discordClient.id,
            userType.DISCORD);

        const results = await subscribersRepo.getByUserID(user.id);

        expect(results.length).to.be.equal(1);
    });

    // multi subscribe
});

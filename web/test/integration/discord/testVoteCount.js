require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');

const { SKIP_DAY_NAME } = require('../../../utils/constants');


describe('Vote count outputs', async() => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            modifiers: [{ name: setupModifiers.DAY_START, value: true }],
            ownerID,
            setupHiddenCount: 8,
        });
        await host.host();
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
    });

    it('Should output all votes for a moderator', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await host.setSetup('custom'); // is day start setup
        await Promise.all([
            host.moderate(),
            host.setDiscussionLength(1),
            ...players.map(player => player.join()),
        ]);

        await host.say(Constants.start, '**Day 1**');

        await host.say('end phase', 'Voting polls are now open');

        await Promise.all([
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ]);

        const vcMessage = await host.say('vc', m => {
            if(typeof m.content !== 'object')
                return;
            return m.content.embed.description.includes('Vote Counts');
        });
        const columnsWithVotes = vcMessage.content.embed.fields
            .filter(field => {
                for(let i = 0; i < players.length; i++)
                    if(field.value.includes(players[i].getName()))
                        return true;
                return false;
            });
        expect(columnsWithVotes.length).to.be.at.least(1);
        const notVotingBlock = vcMessage.content.embed.fields
            .find(field => field.name.includes('Not Voting'));
        expect(notVotingBlock.value).to.not.include(SKIP_DAY_NAME);
    });

    it('Should output all votes for a moderator when whisper', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await host.setSetup('custom');
        await Promise.all([
            host.moderate(),
            host.setDiscussionLength(1),
            ...players.map(player => player.join()),
        ]);
        await host.say(Constants.start, '**Day 1**');
        await host.say('end phase', 'Voting polls are now open');
        await Promise.all([
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ]);

        await host.whisper('vc', m => {
            if(typeof m.content !== 'object')
                return;
            const columnsWithVotes = m.content.embed.fields
                .filter(field => {
                    for(let i = 0; i < players.length; i++)
                        if(field.value.includes(players[i].getName()))
                            return true;
                    return false;
                });
            return columnsWithVotes.length;
        });
    });
});

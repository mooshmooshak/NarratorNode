require('../../../init');
const { expect } = require('chai');

const setupService = require('../../../../services/setup/setupService');

const setupModifiers = require('../../../../models/enums/setupModifiers');

const quasiSetup = require('../../../quasiModels/quasiSetup');
const quasiUser = require('../../../quasiModels/quasiUser');

const modifierUtils = require('../../../../utils/modifierUtil');

const Constants = require('../../../../channels/discord/Constants');

const { FEATURED_PRIORITY, FEATURED_KEY } = require('../../../fakeConstants');


describe('Setup Manipulation', async() => {
    let host;
    let featuredSetup;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddenCount: 9,
            modifiers: [{ name: setupModifiers.DAY_START, value: true }],
        });
        featuredSetup = await quasiSetup.create();
        await setupService.setPriority(featuredSetup.id, FEATURED_PRIORITY, FEATURED_KEY);
        await host.host();
    });

    afterEach(async() => {
        await host.killGame();
    });

    it('Should start games on "Classic" mode', async() => {
        // Given happens in before Each

        const gameObj = await host.getGameObj();

        expect(gameObj.setup.name).to.equal(featuredSetup.name);
    });

    it('Should edit day length', async() => {
        // Given happens in before Each

        await host.say(`${Constants.daylength} 8`, 'Day length changed to 8 minutes.');
        const gameObj = await host.getGameObj();

        expect(gameObj.modifiers.DAY_LENGTH_START.value).to.equal(8 * 60);
    });

    it('Should edit night length', async() => {
        // Given happens in before Each

        await host.say(`${Constants.nightlength} 12`, 'Night length changed to 12 minutes.');
        const gameObj = await host.getGameObj();

        expect(gameObj.modifiers.NIGHT_LENGTH.value).to.equal(12 * 60);
    });

    it('Should set the game start to day', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say(Constants.daystart, 'Game will now start at daytime.');
        const gameObj = await host.getGameObj();

        expect(modifierUtils.getValue(gameObj.setup.setupModifiers, 'DAY_START')).to.equal(true);
    });

    it('Should set the game start to day with an alternative command', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say('day start', 'Game will now start at daytime.');
        const gameObj = await host.getGameObj();

        expect(modifierUtils.getValue(gameObj.setup.setupModifiers, 'DAY_START')).to.equal(true);
    });

    it('Should set the game start to night', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say(Constants.nightstart, 'Game will now start at nighttime.');
        const gameObj = await host.getGameObj();

        expect(modifierUtils.getValue(gameObj.setup.setupModifiers, 'DAY_START')).to.equal(false);
    });

    it('Should set the game start to night with an alternative command', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say('night start', 'Game will now start at nighttime.');
        const gameObj = await host.getGameObj();

        expect(modifierUtils.getValue(gameObj.setup.setupModifiers, 'DAY_START')).to.equal(false);
    });

    it('Should notify the user when the skip vote option" has been turned off', async() => {
        // Given happens in before Each

        await host.setSetup('custom');
        await host.say(`${Constants.skip} off`, 'Players must vote someone out to end the day.');
        const gameObj = await host.getGameObj();

        expect(modifierUtils.getValue(gameObj.setup.setupModifiers, 'SKIP_VOTE')).to.equal(false);
    });

    it('Should notify the user when the "skip vote option" has been turned on', async() => {
        const awaitMessage = 'Players may skip the day, avoiding a public execution.';

        await host.setSetup('custom');
        await host.say(`${Constants.skip} on`, awaitMessage);
        const gameObj = await host.getGameObj();

        expect(modifierUtils.getValue(gameObj.setup.setupModifiers, 'SKIP_VOTE')).to.equal(true);
    });

    it('Should notify the players when moderating has been turned on', async() => {
        // Given happens in before Each

        await host.say(`${Constants.moderate} on`, 'will be moderating this game.');
        await host.say(`${Constants.moderate} off`, 'will no longer be moderating this game.');

        // assertions are the text that comes out
    });

    it('Should not allow non hosts to use this command', async() => {
        const user2 = await quasiUser.createDiscordUser();
        await user2.join();

        await user2.say(`${Constants.moderate} on`, 'Only hosts can use this command.');

        // assertions are the text that comes out
    });

    it('Should show vote systems', async() => {
        // given in before each

        const voteSystemOutput = await host.say(`${Constants.votesystems}`);

        expect(voteSystemOutput.content).to.be.eql(getVoteSystemOutput());
    });

    it('Should change vote systems', async() => {
        const newVoteSystem = 3;

        const voteSystemOutput = await host.say(`${Constants.votesystem} ${newVoteSystem}`, '');
        const gameObj = await host.getGameObj();

        expect(voteSystemOutput.content).to.be.equal('Vote system changed.');
        expect(gameObj.modifiers.VOTE_SYSTEM.value).to.be.equal(newVoteSystem);
    });
});

function getVoteSystemOutput(){
    return '__Available Vote Systems__:\n'
        // eslint-disable-next-line max-len
        + '1. **Plurality Hammer** - Single vote. Day ends early when one person has +50% of the vote.\n'
        // eslint-disable-next-line max-len
        + '2. **Diminishing Pool** - Single vote. Day phase alternates between voting and trials.  To be on trial, two people must have nominated you.  After trials, non trialed people are not eligible for daytime elimination.  Trials happen only once per player per day.  51% of votes on a person ends the day.\n'
        // eslint-disable-next-line max-len
        + '3. **MultiPlurality** - Multi vote.  Vote for as many people simultaneously.  When day time ends, person with most votes gets eliminated.\n'
        // eslint-disable-next-line max-len
        + '4. **No Elimination** - Single vote.  Votes can be submitted, but no one gets eliminated.\n'
        + '\n'
        + 'Example usage: **!voteSystem 3** for multi vote';
}

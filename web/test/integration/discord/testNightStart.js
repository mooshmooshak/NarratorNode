require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');

const discordUtils = require('../../quasiDiscord/quasiDiscordUtils');

const { CITIZEN, GOON } = require('../../definitions/factionRoles');


describe('Night start', () => {
    let clock;

    afterEach(() => {
        clock.restore();
    });

    it('Should output when the night will end', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: true,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, GOON, CITIZEN, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: true }],
            },
            playerCount: 5,
        });

        // want to start the frozen timer here because startGame waits for 300 millis
        // alternatively, I could advance the clock 300 millis instead
        // see quasiDiscordUser.startGame();
        clock = sinon.useFakeTimers();

        const changingMessage = await new Promise(resolve => {
            players[0].waitForMessage(m => m.content && m.content.includes
                && m.content.includes('It is now nighttime. ')).then(resolve);
            discordUtils.massSkipDay(players);
        });

        // this is when the content should change
        clock.tick(5001);

        const newContent = await new Promise(resolve => {
            players[0].discordClient.on('messageUpdate', () => {
                const { channel, id } = changingMessage;
                resolve(channel.channel.messageHistory[id].content);
            });
        });

        expect(newContent).to.include('**00:00:5');
    });
});

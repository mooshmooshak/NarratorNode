require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Modkill', () => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddenCount: 8,
            modifiers: [{ name: setupModifiers.DAY_START, value: true }],
        });
        await host.host();
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
    });

    it('Should be able to modkill players', async() => {
        await host.moderate();
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('custom');
        await host.say(Constants.start, '**Day 1**');

        await host.say(`modkill ${players[0].getName()}`, ' has/have inexplicably died.');
    });

    it('Should give feedback when modkilling players via pm', async() => {
        await host.moderate();
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('custom');
        await host.say(Constants.start, '**Day 1**');
        const game = await host.getGameObj();

        const message = await host.whisper(`modkill ${players[0].getName()}`,
            m => m.content.includes('modkill'));
        const { name: roleName } = game.players
            .find(p => p.name === players[0].getName()).factionRole;
        expect(message.content).to.be.equal(`${players[0].getName()}(${roleName}) was modkilled.`);
    });
});

require('../../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../../models/enums/setupModifiers');

const quasiDiscordGame = require('../../../quasiDiscord/quasiDiscordGame');
const quasiDiscordUtils = require('../../../quasiDiscord/quasiDiscordUtils');

const { CITIZEN, GOON } = require('../../../definitions/factionRoles');


describe('Last will', () => {
    let players;
    beforeEach(async() => {
        const discordGame = await quasiDiscordGame.get({
            isStarted: true,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, GOON, CITIZEN, GOON],
                modifiers: [
                    { name: setupModifiers.DAY_START, value: true },
                    { name: setupModifiers.LAST_WILL, value: true },
                    { name: setupModifiers.SELF_VOTE, value: true },
                ],
            },
            playerCount: 5,
        });
        players = discordGame.players;
    });

    it('Will show the name of the user for a nighttime death event', async() => {
        const goons = await quasiDiscordUtils.filterRole(players, GOON.name);
        const [citizen] = await quasiDiscordUtils.filterRole(players, CITIZEN.name);

        await Promise.all(goons.map(p => p.vote(citizen)));
        await citizen.setLastWill('sad');
        const { content } = await citizen.whisper(`vote ${citizen.getName()}`,
            'left a last will');

        expect(content).to.include(`${citizen.getName()} left a last will`);
    });

    it('Will show the current last will', async() => {
        const [player] = players;
        await player.setLastWill('sad');

        const { content } = await player.whisper('lw');

        expect(content).to.equal('Your current last will is:\nsad');
    });
});

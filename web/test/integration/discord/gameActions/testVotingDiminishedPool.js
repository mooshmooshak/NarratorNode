require('../../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../../models/enums/setupModifiers');
const voteSystemTypes = require('../../../../models/enums/voteSystemTypes');

const preferService = require('../../../../services/preferService');

const quasiSetup = require('../../../quasiModels/quasiSetup');
const quasiUser = require('../../../quasiModels/quasiUser');

const Constants = require('../../../../channels/discord/Constants');


describe('Voting in Diminishing Pool System', async() => {
    let host;
    let players;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        const setupHiddenCount = 8;
        const setup = await quasiSetup.create({
            ownerID,
            setupHiddenCount,
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }],
        });
        players = quasiUser.createDiscordUsers(setupHiddenCount - 1);
        await host.host();
        await host.setSetup('custom'); // is day start setup
        await Promise.all([
            host.moderate(),
            host.setDiscussionLength(1),
            host.setTrialLength(1),
            host.setVoteSystem(voteSystemTypes.DIMINISHING),
            ...players.map(player => player.join()),
        ]);
        const lastUserID = await (players[setupHiddenCount - 2].getUserID());
        await preferService.prefer(lastUserID, 'Goon', setup.id);

        await host.say(Constants.start, '**Day 1**');
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
    });

    it('Should not allow players to vote during discussion', async() => {
        const reply = await players[0].say(Constants.commands, 'Bot commands');
        expect(reply.content).to.not.include('Vote to eliminate someone during the day');

        await players[0].say(`vote ${players[1].getName()}`, 'Unknown command');

        await host.say('end phase', 'Voting polls are now open');

        await players[0].say('end phase', 'Unknown command');
    });

    it('Should follow basic diminished pool voting rules', async() => {
        await host.say('end phase', 'Voting polls are now open');

        let votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.say('end phase', ' will give their defense first');
        await host.say('end phase', ' is now on trial for crimes against humanity');
        await host.say('end phase', 'Voting polls are now open');

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        let waitText = 'There is no vote consensus.  Votes have been reset.  '
            + 'You have 2 more chances before everyone dies.';
        await host.say('end phase', waitText);

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        waitText = 'There is no vote consensus.  Votes have been reset.  '
            + 'This is your last chance before everyone dies.';
        await host.say('end phase', waitText);
    });

    it('Should announce the vote count and the trialed voter order', async() => {
        await host.say('end phase', 'Voting polls are now open');

        const votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        const expectedTrialOrder1 = `The voting round has concluded.\n${players[1].getMention()} `
            + `will give their defense first, followed by ${players[4].getMention()}.`;
        const expectedTrialOrder2 = `The voting round has concluded.\n${players[4].getMention()} `
            + `will give their defense first, followed by ${players[1].getMention()}.`;
        let voteCountFound = false;
        await host.say('end phase', m => {
            if(m.content.embed && m.content.embed.description === '**Vote Counts**')
                voteCountFound = true;
            if(!voteCountFound)
                return;
            if(m.content === expectedTrialOrder1 || m.content === expectedTrialOrder2)
                return true;
        });
    });

    it('Should announce votes when a tie had been reached', async() => {
        await host.say('end phase', 'Voting polls are now open');

        let votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.say('end phase', ' will give their defense first');
        await host.say('end phase', ' is now on trial for crimes against humanity');
        await host.say('end phase', 'Voting polls are now open');

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
            players[1].vote(players[4]),
            players[3].vote(players[4]),
        ];
        await Promise.all(votes);

        await host.say('end phase', m => {
            if(typeof m.content !== 'object')
                return false;
            if(m.content.embed.description !== '**Vote Counts**')
                return false;
            const fields = m.content.embed.fields;
            for(let i = 0; i < fields.length; i++){
                const field = fields[i];
                if(field.name.includes('Not Voting'))
                    continue;
                expect(field.value).to.not.be.equal('*No votes*');
            }
            return true;
        });
    });

    it('Should announce the vote count at the end of the day', async() => {
        await host.say('end phase', 'Voting polls are now open');

        let votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
        ];
        await Promise.all(votes);

        await host.say('end phase',
            m => m.content.embed && m.content.embed.description === '**Vote Counts**');

        await host.say('end phase',
            m => m.content.includes && m.content.includes('Voting polls are now open'));

        votes = [
            players[4].vote(players[1]),
            players[2].vote(players[1]),
        ];
        await Promise.all(votes);

        await host.say('end phase', m => {
            if(!m.content.embed)
                return false;
            return m.content.embed.description === '**Vote Counts**';
        });
    });
});

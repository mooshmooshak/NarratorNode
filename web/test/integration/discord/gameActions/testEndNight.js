require('../../../init');
const { expect } = require('chai');

const quasiUser = require('../../../quasiModels/quasiUser');


describe('End night', () => {
    it('Will not repeat args after end if command is invalid', async() => {
        const host = quasiUser.createDiscordUser();
        await host.host();

        const { content } = await host.say('end garbage');
        expect(content).to.include('Unknown command.  Try "end night" or "end phase".');
    });
});

require('../../../init');
const { expect } = require('chai');

const quasiDiscordGame = require('../../../quasiDiscord/quasiDiscordGame');

const gameModifiers = require('../../../../models/enums/gameModifiers');
const setupModifiers = require('../../../../models/enums/setupModifiers');
const voteSystemTypes = require('../../../../models/enums/voteSystemTypes');

const Constants = require('../../../../channels/discord/Constants');

const voteService = require('../../../../services/voteService');

const { CITIZEN, GOON } = require('../../../definitions/factionRoles');


describe('Multi vote', () => {
    let players;
    let gameID;
    beforeEach(async() => {
        const discordGame = await quasiDiscordGame.get({
            isStarted: true,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, GOON, CITIZEN, GOON],
                modifiers: [
                    { name: setupModifiers.DAY_START, value: true },
                    { name: setupModifiers.SELF_VOTE, value: true },
                ],
            },
            modifiers: [{
                name: gameModifiers.VOTE_SYSTEM,
                value: voteSystemTypes.MULTIVOTE_PLURALITY,
            }],
            playerCount: 5,
        });
        players = discordGame.players;
        gameID = discordGame.game.id;
    });

    it('Will allow player to vote twice and unvote one', async() => {
        await players[0].vote(players[1]);
        await players[0].vote(players[2]);

        await assertVoteTargets(players[0], [players[1], players[2]]);

        await players[0].unvote(players[2]);

        await assertVoteTargets(players[0], [players[1]]);
    });

    it('Will not show L- in vc', async() => {
        const { content } = await players[0].say(Constants.votecount);

        expect(content).to.deep.equal(noLMinusContent());
    });

    it('Will unvote everyone', async() => {
        await players[0].vote(players[1]);
        await players[0].vote(players[2]);

        await players[0].unvote();

        await assertVoteTargets(players[0], []);
    });

    async function assertVoteTargets(voter, expectedTargets){
        const votes = await voteService.getVotes(gameID);
        const voteTargets = votes.voterToVotes[voter.getName()];
        expect(voteTargets.length).to.be.equal(expectedTargets.length);
        expectedTargets.forEach(
            expectedTarget => expect(voteTargets).to.include(expectedTarget.getName()),
        );
    }
});

function noLMinusContent(){
    return {
        embed: {
            description: '**Vote Counts**',
            fields: [{
                inline: true,
                name: '**[1] Player1**',
                value: '*No votes*',
            }, {
                inline: true,
                name: '**[2] Player2**',
                value: '*No votes*',
            }, {
                inline: true,
                name: '**[3] Player3**',
                value: '*No votes*',
            }, {
                inline: true,
                name: '**[4] Player4**',
                value: '*No votes*',
            }, {
                inline: true,
                name: '**[5] Player5**',
                value: '*No votes*',
            }, {
                inline: false,
                name: '**Not Voting**',
                value: 'Player1\nPlayer2\nPlayer3\nPlayer4\nPlayer5',
            }],
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}

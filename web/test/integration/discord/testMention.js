require('../../init');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');
const quasiUser = require('../../quasiModels/quasiUser');


describe('Mention', () => {
    let host;
    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should allow people to mention to target', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        await host.say(`vote <@${players[0].discordClient.user.id}>`,
            `${host.getName()} voted for ${players[0].getName()}`);
    });

    it('Should allow people to mention to target with "!"', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        await host.say(`vote <@!${players[0].discordClient.user.id}>`,
            `${host.getName()} voted for ${players[0].getName()}`);
    });
});

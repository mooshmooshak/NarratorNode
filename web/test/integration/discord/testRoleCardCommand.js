require('../../init');
const { expect } = require('chai');

const quasiDiscordUser = require('../../quasiModels/quasiUser');
const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');

const Constants = require('../../../channels/discord/Constants');

const { CITIZEN, EXECUTIONER, GOON } = require('../../definitions/factionRoles');


describe('Role card', () => {
    describe('In game', () => {
        let players;
        beforeEach(async() => {
            const game = await quasiDiscordGame.get({
                isStarted: true,
                setup: {
                    setupHiddens: [CITIZEN, CITIZEN, GOON, EXECUTIONER, GOON],
                    modifiers: [
                        { name: setupModifiers.DAY_START, value: true },
                    ],
                },
                playerCount: 5,
            });
            players = game.players;
        });

        it('Will show the faction kill ability for a goon', async() => {
            const [goon] = await quasiDiscordUtils.filterRole(players, GOON.name);
            const { content } = await goon.whisper(Constants.rolecard);

            assertRoleCardText(content, [
                'You are a **Mafia Goon**.',
                'You must eliminate the following : Cult, Threat, Town, Yakuza.',
                'Your ally is **Player5(Goon)**.',
                '',
                // eslint-disable-next-line max-len
                'You have more weight in deciding who controls factional abilities (typically the mafia kill).',
                'Ability to kill at night.',
            ]);
        });

        it('Will show the target of an executioner', async() => {
            const [executioner] = await quasiDiscordUtils.filterRole(players, EXECUTIONER.name);
            const { content } = await executioner.whisper(Constants.rolecard);

            assertRoleCardText(content, [
                'You are a **Benigns Executioner**.',
                'To win, you must get Player4 voted out.',
                '',
                'Sole purpose is to get a given target killed. Do it.',
                'You must try to Player4 publicly executed to win.',
                'Your target may be of any alignment!',
            ]);
        });

        function assertRoleCardText(content, items){
            content = content.split('\n');
            expect(content).to.deep.equal(items);
        }
    });

    it('handles not being in a started game', async() => {
        const player = quasiDiscordUser.createDiscordUser();
        await player.host();

        const { content } = await player.whisper(Constants.rolecard, '');

        expect(content).to.include('You do not have a role card.');
    });
});

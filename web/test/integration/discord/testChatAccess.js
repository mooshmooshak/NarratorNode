require('../../init');

const setupModifiers = require('../../../models/enums/setupModifiers');

const { MANAGE_ROLES } = require('../../../channels/discord/enums/permissions');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');
const { removeChannelPermission } = require('../../quasiDiscord/quasiDiscordUtils');

const { ASSASSIN, CITIZEN, GOON } = require('../../definitions/factionRoles');

const { pause } = require('../../testHelpers');


const setupHiddens = [
    ASSASSIN, GOON, CITIZEN, CITIZEN, CITIZEN, CITIZEN, CITIZEN, CITIZEN, CITIZEN,
];

describe('Chat access', () => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddens,
            modifiers: [{ name: setupModifiers.DAY_START, value: true }],
        });
    });
    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should allow everyone to speak if the game hasn\'t started', async() => {
        const user2 = await quasiUser.createDiscordUser();

        await host.host();

        await host.chatter('I, as the host can speak.');
        await user2.chatter('I, as user2, can speak.');
    });

    it('Should stop users from speaking on night game start', async() => {
        await host.host(); // classic setup starts at night
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);

        await host.start();

        const chatterPromises = players
            .map(player => player.failChatter(`${player.getName()} can't speak.`));
        return Promise.all(chatterPromises);
    });

    it('Should allow people to speak again on day start', async() => {
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);

        const chatterPromises = players
            .map(player => player.chatter(`${player.getName()} can speak.`));
        return Promise.all(chatterPromises);
    });

    it('Should allow everyone to speak if the game finished', async() => {
        const user2 = await quasiUser.createDiscordUser();
        const game = await host.host();
        await host.setPhaseStart(false);// NIGHT START
        await quasiGame.addPlayersToGame(game.joinID, 5);
        await user2.join();
        await host.start();

        await host.killGame();
        const user1 = host;
        host = null;

        await user1.chatter('I, as user1, can speak after the game stopped.');
        await user2.chatter('I, as user2, can also speak after the game stopped.');
    });

    it('Should stop out of game players from speaking', async() => {
        const user2 = await quasiUser.createDiscordUser();
        const user3 = await quasiUser.createDiscordUser();
        const game = await host.host();
        await Promise.all([user2.join(), quasiGame.addPlayersToGame(game.joinID, 5)]);
        await host.start();

        // assertion is user not being able to talk
        await user3.failChatter('I shouldn\'t interrupt the games of other players.');
    });

    it('Should stop night killed people from talking the next day', async() => {
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.setPhaseStart(false); // NIGHT START
        await host.start();

        const goon = (await quasiDiscordUtils.filterColor(players, '#FF0000'))[0];
        const sheriff = (await quasiDiscordUtils.filterColor(players, '#6495ED'))[0];
        await goon.whisper(`kill ${sheriff.getName()}`, `You will kill ${sheriff.getName()}`);
        await quasiDiscordUtils.massEndNight(players);

        await sheriff.failChatter('I shouldn\'t be able to talk as a dead person.');
    });

    it('Should stop voted out people from talking the next day', async() => {
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.setSetup('custom');
        await host.start();

        const townMembers = await quasiDiscordUtils.filterColor(players, '#6495ED');
        await quasiDiscordUtils.massVote(townMembers[0], townMembers.slice(1, 4));
        await townMembers[4].say(`vote ${townMembers[0].getName()}`, 'It is now nighttime.');
        await quasiDiscordUtils.massEndNight(players.filter(player => player !== townMembers[0]));

        await townMembers[0].failChatter('I shouldn\'t be able to talk as a dead person.');
    });

    it('Should stop daykilled people from talking that day', async() => {
        await host.host();
        await host.setSetup('custom');
        const players = quasiUser.createDiscordUsers(setupHiddens.length - 1);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        const assassin = (await quasiDiscordUtils.filterRole(players, 'Assassin'))[0];
        const target = (await quasiDiscordUtils.filterRole(players, 'Citizen'))[0];
        await assassin.whisper(`assassinate ${target.getName()}`,
            `${assassin.getName()} assassinated ${target.getName()}`);

        await target.failChatter('I shouldn\'t be able to talk as a dead daykilled person.');
    });

    it('Will alert channel when lacking permissions to stop players from talking', async() => {
        setNoManageRolesPermission();
        // eslint-disable-next-line max-len
        const expectedText = "`Lacking 'Manage Permissions' permission. Players will not be stopped from talking during nighttime.`";
        const promiseMessage = host.waitForMessage(
            m => m.content.includes && m.content.includes(expectedText),
        );

        host.host();

        await promiseMessage;
    });

    it('Should allow people to send two messages quickly, by default', async() => {
        await host.host();
        await host.setSetup('custom');
        const players = quasiUser.createDiscordUsers(setupHiddens.length - 1);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await players[0].chatter('someChatter');
        await pause(50);
        await players[0].chatter('someChatter');
    });

    function setNoManageRolesPermission(){
        removeChannelPermission(host, MANAGE_ROLES);
    }
});

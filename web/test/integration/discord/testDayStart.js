require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const Constants = require('../../../channels/discord/Constants');

const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');

const { CITIZEN, GOON } = require('../../definitions/factionRoles');


describe('Day start', () => {
    it('Will show correct day end time text', async() => {
        const { host } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: true }],
            },
            playerCount: 4,
        });
        await host.moderate();
        const minuteLength = 8;
        await host.say(`${Constants.daylength} ${minuteLength}`,
            'Day length changed to 8 minutes.');

        const message = await host.say(Constants.start, '**Day 1**');

        expect(message.content).to.include('**Day 1**\nVoting polls will close in **8** minutes.');
    });
});

require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');

const Constants = require('../../../channels/discord/Constants');


describe('Player Actions command', () => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddenCount: 3,
            modifiers: [{ name: setupModifiers.DAY_START, value: false }],
        });
        await host.host();
        await host.moderate();
        await host.setSetup('custom');
    });

    it('Should get players actions when command is given', async() => {
        const players = quasiUser.createDiscordUsers(3);
        await Promise.all(players.map(player => player.join()));
        await host.say(Constants.start, 'It is now nighttime');
        const [goon] = await quasiDiscordUtils.filterRole(players, 'Goon');
        const cit = players.find(p => p !== goon);
        await goon.whisper(`kill ${cit.getName()}`);

        let message = await host.say(Constants.playeractions, '');
        expect(message.content).to.deep.equal(getPlayerActionsMessage(goon, cit));

        message = await players[0].whisper(Constants.playeractions);
        expect(message.content).to.include('Only participating moderators can use this command.');

        await host.say(Constants.commands, 'View player actions');
    });
});

function getPlayerActionsMessage(killer, target){
    return `**PlayerActions**\n- ${killer.getName()} : _Will kill ${target.getName()}_`;
}

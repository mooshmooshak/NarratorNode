require('../../init');
const { expect } = require('chai');

const setupService = require('../../../services/setup/setupService');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');

const { FEATURED_KEY, FEATURED_PRIORITY, SETUP_SLOGAN } = require('../../fakeConstants');


describe('Setup Command', async() => {
    it('Will show a list of setups', async() => {
        const lowerPriority = FEATURED_PRIORITY;
        const higherPriority = FEATURED_PRIORITY + 1;
        const [user] = await Promise.all([
            quasiUser.createDiscordUser(),
            quasiSetup.create({ slogan: SETUP_SLOGAN })
                .then(s => setupService.setPriority(s.id, higherPriority,
                    `${FEATURED_KEY.substring(1)}`)),

            // listed first because the priority is lower... but will fix
            quasiSetup.create({ slogan: `${SETUP_SLOGAN}2` })
                .then(s => setupService.setPriority(s.id, lowerPriority, FEATURED_KEY)),
        ]);

        const { content } = await user.say(Constants.setup,
            m => m.content.includes(`*${FEATURED_KEY}*`));
        expect(content).to.not.include('undefined');
        const entry1 = `. *${FEATURED_KEY}* - ${SETUP_SLOGAN}2`;
        const entry2 = `. *${FEATURED_KEY.substring(1)}* - ${SETUP_SLOGAN}`;
        expect(content).to.include(entry1);
        expect(content).to.include(entry2);
        expect(content.indexOf(entry1) < content.indexOf(entry2)).to.be.equal(true);
    });

    it('Will change to a featured setup by key', async() => {
        const [user, setup] = await Promise.all([
            quasiUser.createDiscordUser(),
            quasiSetup.create({ slogan: SETUP_SLOGAN })
                .then(async s => {
                    await setupService.setPriority(s.id, FEATURED_PRIORITY, FEATURED_KEY);
                    return s;
                }),
        ]);
        await user.host();

        await user.setSetup(FEATURED_KEY);
        const gameObj = await user.getGameObj();

        expect(gameObj.setup.id).to.be.equal(setup.id);
    });
});

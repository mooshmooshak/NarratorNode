require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');
const { PRIVATE } = require('../../../channels/discord/enums/channelTypes');

const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');

const Constants = require('../../../channels/discord/Constants');

const { CITIZEN, GOON } = require('../../definitions/factionRoles');


describe('Living', () => {
    let players;
    beforeEach(async() => {
        const discordGame = await quasiDiscordGame.get({
            isStarted: true,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, GOON, CITIZEN, GOON, CITIZEN, CITIZEN],
                modifiers: [
                    { name: setupModifiers.DAY_START, value: true },
                    { name: setupModifiers.LAST_WILL, value: true },
                    { name: setupModifiers.SELF_VOTE, value: true },
                ],
            },
            playerCount: 7,
        });
        players = discordGame.players;
    });

    it('Should show living', async() => {
        const message = await players[0].say(Constants.living, m => m.content);

        expect(message.content).to.eql(expectedLivingMessage());
    });

    it('Will keep the same numbers after death', async() => {
        players.sort((p1, p2) => p1.getName().localeCompare(p2.getName()));

        await Promise.all(
            players.slice(1, 5)
                .map(p => p.say('vote 1', `${p.getName()} voted for ${players[0].getName()}`)),
        );

        const { content } = await players[1].whisper(
            Constants.living, m => m.channel.type === PRIVATE,
        );
        expect(content).to.eql(expectedLivingMessagePostDeath());
    });
});

function expectedLivingMessage(){
    return {
        embed: {
            fields: [
                {
                    inline: false,
                    name: '**The Living**',
                    // eslint-disable-next-line max-len
                    value: '**[1]** Player1\n**[2]** Player2\n**[3]** Player3\n**[4]** Player4\n**[5]** Player5\n**[6]** Player6\n**[7]** Player7\n',
                },
            ],
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}

function expectedLivingMessagePostDeath(){
    return {
        embed: {
            fields: [
                {
                    inline: false,
                    name: '**The Living**',
                    // eslint-disable-next-line max-len
                    value: '**[2]** Player2\n**[3]** Player3\n**[4]** Player4\n**[5]** Player5\n**[6]** Player6\n**[7]** Player7\n',
                },
            ],
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}

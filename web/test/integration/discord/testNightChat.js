require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');

const discordUtils = require('../../quasiDiscord/quasiDiscordUtils');

const Constants = require('../../../channels/discord/Constants');

const { CITIZEN, GOON } = require('../../definitions/factionRoles');


describe('Night chat', () => {
    it('Will create night chats channel with a topic and permissions', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: false }],
            },
            playerCount: 5,
        });

        const [nightChatChannelID, guildID, player] = await getNightChatInviteAfterStart(players);
        player.joinGuild(guildID);

        const guild = player.discordClient.guilds.cache.get(guildID);
        const { permissionOverwrites, topic } = guild.channels.cache.get(nightChatChannelID);
        expect(topic).to.be.equal('For common commands, type: __!commands__');
        const notEveryoneVisiblePermission = permissionOverwrites
            .find(({ id, deny }) => id === guild.roles.everyone.id
            && hasDenyEveryoneViewPermission(deny));
        expect(!!notEveryoneVisiblePermission).to.be.eq(true);
    });

    function hasDenyEveryoneViewPermission(denyPermissions){
        return denyPermissions.includes('VIEW_CHANNEL');
    }

    it('Will show living players in the mafia chat', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: false }],
            },
            playerCount: 5,
        });

        const [nightChatChannelID, guildID, player] = await getNightChatInviteAfterStart(players);
        player.joinGuild(guildID);

        const message = await player.say(`${Constants.living}`, m => m.content.embed
                && m.content.embed.fields[0].name.includes('Living'), nightChatChannelID);
        expect(message.channel.id).to.be.equal(nightChatChannelID);
    });

    it('Will create new night chats each game', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: false }],
            },
            playerCount: 5,
        });

        const [nightChatChannelID] = await getNightChatInviteAfterStart(players);

        await discordUtils.massEndNight(players);
        const goons = await discordUtils.filterRole(players, GOON.name);
        const citizens = players.filter(p => !goons.includes(p));
        await discordUtils.massVote(goons[0], citizens);
        await discordUtils.massEndNight([goons[1], ...citizens]);
        await discordUtils.massVote(goons[1], citizens.slice(1, 3));
        await citizens[0].say(`vote ${goons[1].getName()}`, 'Game canceled.'); // i need a better way to wait for game to end

        const { players: players2 } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: false }],
            },
            playerCount: 5,
        });

        const [nightChatChannelID2] = await getNightChatInviteAfterStart(players2);

        expect(nightChatChannelID).to.not.be.equal(nightChatChannelID2);
    });

    it('Will not allow late joiners to talk in their night channels during the day', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: false }],
            },
            playerCount: 5,
        });

        const [nightChatChannelID, guildID, player] = await getNightChatInviteAfterStart(players);
        await discordUtils.massEndNight(players);

        await player.joinGuild(guildID);

        await player.failChatter('some garbage chatter', nightChatChannelID);
    });

    it('Will allow late joiners to talk in their night channels during the night', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: false }],
            },
            playerCount: 5,
        });

        const [nightChatChannelID, guildID, player] = await getNightChatInviteAfterStart(players);

        await player.joinGuild(guildID);

        await player.chatter('some garbage chatter', nightChatChannelID);
    });
});

async function getNightChatInviteAfterStart(players){
    const [message, player] = await new Promise(resolve => {
        players.map(p => p.waitForMessage(m => m.content.includes && m.content
            .includes('You can access')).then(m => resolve([m, p])));
        players[0].say('start');
    });
    const text = message.content.replace('You can access your secret night chats here: ', '');
    const [channelID, guildID] = text.split(':');
    return [channelID, guildID, player];
}

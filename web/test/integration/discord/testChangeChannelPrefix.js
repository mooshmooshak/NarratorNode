require('../../init');
const { expect } = require('chai');
const config = require('../../../../config');

const channelCommandsRepo = require('../../../channels/discord/discordRepos/channelCommandsRepo');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Change trigger prefix', () => {
    it('Will change the trigger prefix', async() => {
        const player = quasiUser.createDiscordUser();
        await player.say(`${Constants.changecommand} w.`, 'Command changed to');

        await new Promise(resolve => {
            player.waitForMessage(m => m.content.includes('No active games')).then(resolve);
            player.chatter('w.info');
        });
    });

    it('Will change the trigger prefix twice', async() => {
        const player = quasiUser.createDiscordUser();

        await player.say(`${Constants.changecommand} w.`, 'Command changed to \'w.\'.');

        await new Promise(resolve => {
            player.waitForMessage(
                m => m.content.includes('Command changed to \'q.\'.'),
            ).then(resolve);
            player.chatter(`w.${Constants.changecommand} q.`);
        });

        await new Promise(resolve => {
            player.waitForMessage(m => m.content.includes('No active games')).then(resolve);
            player.chatter('q.info');
        });

        const prefix = await channelCommandsRepo.get(config.discord.test_channel_id);
        expect(prefix).to.be.equal('q.');
    });

    it('Will use no prefix when outputting help commands in dms', async() => {
        const player = quasiUser.createDiscordUser();
        const garbageCommand = 'garbage';

        const response = await player.whisper('garbage');

        // eslint-disable-next-line max-len
        const expectedMessage = `Unknown command \`${garbageCommand}\`! Use **commands** to see available commands.`;
        expect(response.content).to.be.equal(expectedMessage);
    });
});

require('../../init');

const helpers = require('../../../utils/helpers');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Echo', async() => {
    it('Echos text publicly back to you', async() => {
        const user = await quasiUser.createDiscordUser();
        const gibberish = helpers.getRandomString(7);

        await user.say(`${Constants.echo} ${gibberish}`, gibberish);
    });

    it('Echos text privately back to you', async() => {
        const user = await quasiUser.createDiscordUser();
        const gibberish = helpers.getRandomString(7);

        await user.whisper(`${Constants.echo} ${gibberish}`, gibberish);
    });
});

require('../../init');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');

const Constants = require('../../../channels/discord/Constants');


describe('Setup Overview', async() => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const hostUserID = await host.getUserID();
        await quasiSetup.create({
            ownerID: hostUserID,
            setupHiddenCount: 20,
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }],
        });
        await host.host();
    });

    afterEach(async() => {
        await host.killGame();
    });

    it('Should give the correct values for players in "info" v1', async() => {
        await Promise.all([host.setSetup('custom'), host.moderate()]);

        // test 1
        await host.say(Constants.info, m => {
            if(!m.content.embed)
                return;
            if(m.content.embed.fields[1].value.includes('*3 '))
                return true;
        });
    });

    it('Should give the correct values for players in "info" v2', async() => {
        await Promise.all([host.setSetup('custom'), host.moderate()]);
        await quasiUser.createDiscordUser().join();

        // test 2
        await host.say(Constants.info, m => {
            if(m.content.embed.fields[1].value.includes('*2 '))
                return true;
        });
    });

    it('Should give the correct values for players in "info" v3', async() => {
        await Promise.all([host.setSetup('custom'), host.moderate()]);
        await Promise.all(quasiUser.createDiscordUsers(2).map(user => user.join()));

        // test 3
        await host.say(Constants.info, m => {
            if(m.content.embed.fields[1].value.includes('*1 '))
                return true;
        });
    });

    it('Should give the correct values for players in "info" v4', async() => {
        await Promise.all([host.setSetup('custom'), host.moderate()]);
        await Promise.all(quasiUser.createDiscordUsers(3).map(user => user.join()));

        // test 4
        await host.say(Constants.info, m => {
            if(!m.content.embed)
                return false;
            if(m.content.embed.fields[1].value.includes(' 17 '))
                return true;
        });
    });

    it('Should give the correct values for players in "info" v5', async() => {
        await Promise.all([host.setSetup('custom'), host.moderate()]);
        await Promise.all(quasiUser.createDiscordUsers(4).map(user => user.join()));

        // test 5
        await host.say(Constants.info, m => {
            if(!m.content.embed)
                return false;
            if(m.content.embed.fields[1].value.includes(' 16 '))
                return true;
        });
    });

    it('Should give the correct values for players in "info" v6', async() => {
        await Promise.all([host.setSetup('custom'), host.moderate()]);
        await Promise.all(quasiUser.createDiscordUsers(19).map(user => user.join()));

        // test 6
        await host.say(Constants.info, m => {
            if(!m.content.embed)
                return false;
            if(m.content.embed.fields[1].value.includes('1 '))
                return true;
        });
    });

    it('Should show dead players in the overview', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await host.moderate();
        // the next 2 promises should be able to be set at the same time
        // but it's not working?...
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('custom');
        await players[0].start();
        const [voted, hammerer, ...firstVoters] = await quasiDiscordUtils
            .filterRole(players, 'Citizen');

        const votes = firstVoters.slice(-3).map(voter => voter.vote(voted));
        await Promise.all(votes);

        await hammerer.say(`vote ${voted.getName()}`, ' was democratically eliminated.');
        await host.say(Constants.info, m => {
            if(!m.content.embed)
                return;
            if(!m.content.embed.fields)
                return;
            const { fields } = m.content.embed;
            if(fields.length < 2)
                return;
            return fields[0].value && fields[1].value;
        });
    });

    it('Should show player roles to the moderator on whisper', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await host.moderate();
        await host.setSetup('custom');
        await Promise.all(players.map(player => player.join()));
        await host.say(Constants.start, 'Day 1');

        await host.whisper(Constants.info, m => {
            if(!m.content.embed)
                return;
            if(!m.content.embed.fields)
                return;
            const { fields } = m.content.embed;
            if(fields.length < 2)
                return;
            const m1 = `:blue_car: ${players[0].getName()} - *Citizen*`;
            const m2 = `:red_circle: ${players[0].getName()} - *Goon*`;
            return fields[1].value.includes(m1) || fields[1].value.includes(m2);
        });
    });

    it('Should not show player roles to the moderator on public info request', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await host.moderate();
        await host.setSetup('custom');
        await Promise.all(players.map(player => player.join()));
        await host.say(Constants.start, 'Day 1');

        await host.say(Constants.info, m => {
            if(!m.content.embed)
                return;
            if(!m.content.embed.fields)
                return;
            const { fields } = m.content.embed;
            if(fields.length < 2)
                return;
            const m1 = `:blue_car: ${players[0].getName()} - *Citizen*`;
            const m2 = `:red_circle: ${players[0].getName()} - *Goon*`;
            return !fields[1].value.includes(m1) && !fields[1].value.includes(m2);
        });
    });

    it('Should not show player roles to regular players on whisper', async() => {
        const players = quasiUser.createDiscordUsers(7);
        await host.moderate();
        await host.setSetup('custom');
        await Promise.all(players.map(player => player.join()));
        await host.setPhaseStart(true); // DAY START
        await host.say(Constants.start, 'Day 1');

        await players[0].whisper(Constants.info, m => {
            if(!m.content.embed)
                return;
            if(!m.content.embed.fields)
                return;
            const { fields } = m.content.embed;
            if(fields.length < 2)
                return;
            const m1 = `:blue_car: ${players[0].getName()} - *Citizen*`;
            const m2 = `:red_circle: ${players[0].getName()} - *Goon*`;
            return !fields[1].value.includes(m1) && !fields[1].value.includes(m2);
        });
    });
});

require('../../init');
const { expect } = require('chai');

const requests = require('../../requests');

const setupIterationSchema = require('../../schemas/setupIterationSchema');

const quasiSetup = require('../../quasiModels/quasiSetup');
const authHelpers = require('../../authHelpers');


describe('Setup iteration', () => {
    let setup;
    const SETUP_HIDDEN_COUNT = 5;
    beforeEach(async() => {
        setup = await quasiSetup.create({ setupHiddenCount: SETUP_HIDDEN_COUNT });
    });

    it('Will output variations of a setup', async() => {
        // given in before each

        const responseObj = await getIterations();

        expect(responseObj.statusCode).to.be.equal(200);
        const iterations = responseObj.json.response;
        expect(iterations.length).to.be.greaterThan(0);
        iterations.forEach(setupIterationSchema.check);
    });

    it('Will fail when playerCount is not a number', async() => {
        // given in before each

        const responseObj = await getIterations({ playerCount: 'a' });

        expect(responseObj.statusCode).to.be.equal(404); // maybe this should be 422
    });

    it('Will save an iteration', async() => {
        // given in beforeEach

        const responseObj = await saveIteration();

        expect(responseObj.statusCode).to.be.equal(204);
    });

    it('Will fail request if no auth is given', async() => {
        const headers = undefined;

        const responseObj = await saveIteration({ headers });

        expect(responseObj.statusCode).to.be.equal(403);
    });

    function getIterations(args = {}){
        const playerCount = 'playerCount' in args ? args.playerCount : SETUP_HIDDEN_COUNT;
        return requests.get(`api/setups/${setup.id}/iterations?playerCount=${playerCount}`);
    }

    function saveIteration(arg){
        const args = {
            spawns: setup.setupHiddens.map(sh => {
                const hidden = setup.hiddens.find(h => h.id === sh.hiddenID);
                return {
                    setupHiddenID: sh.id,
                    factionRoleID: hidden.spawns[0].factionRoleID,
                };
            }),
        };
        const headers = arg && 'headers' in arg
            ? arg.headers
            : authHelpers.fakeTempAuthHeader(setup.ownerID);
        return requests.post(`api/setups/${setup.id}/iterations`,
            args,
            headers);
    }
});

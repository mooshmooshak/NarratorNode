require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');
const authHelpers = require('../../authHelpers');

const requests = require('../../requests');
const testHelpers = require('../../testHelpers');
const actionService = require('../../../services/actionService');

const gameService = require('../../../services/gameService');
const setupHiddenService = require('../../../services/setup/setupHiddenService');
const voteService = require('../../../services/voteService');

const voteSystemType = require('../../../models/enums/voteSystemTypes');
const { MAX_PLAYER_COUNT } = require('../../../utils/constants');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiWebSocket = require('../../quasiWebSocket');
const { getHostName } = require('../../../utils/lobbyUtil');

const { SKIP_DAY_NAME } = require('../../../utils/constants');


describe('Action Controller', () => {
    let clock;
    let ws;

    afterEach(() => {
        if(clock)
            clock.restore();
        if(ws)
            ws.close();
    });

    it('Will submit a skip vote', async() => {
        const setup = await quasiSetup.create({
            modifiers: [{
                name: 'DAY_START',
                value: true,
            }],
            setupHiddenCount: 5,
        });
        const gameObj = await quasiGame.create({
            hostID: setup.ownerID,
            isStarted: true,
            playerCount: 5,
            setupID: setup.id,
        });
        const message = { command: 'vote', targets: [SKIP_DAY_NAME] };
        const headers = authHelpers.fakeTempAuthHeader(setup.ownerID);

        const serverResponse = await requests.put('api/actions', message, headers);
        const votes = await voteService.getVotes(gameObj.id);
        const hostName = getHostName(gameObj);

        expect(serverResponse.statusCode).to.equal(200);
        expect(serverResponse.json.response.voteInfo.voterToVotes[hostName])
            .to.deep.equal([SKIP_DAY_NAME]);
        expect(votes.voterToVotes[hostName]).to.deep.equal([SKIP_DAY_NAME]);
    });

    it('Will submit a kill action', async() => {
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
        });
        const goonProfile = await testHelpers.getProfileWithRole(gameObj, 'Goon');
        const citProfile = await testHelpers.getProfileWithRole(gameObj, 'Citizen');
        const headers = authHelpers.fakeTempAuthHeader(goonProfile.userID);
        const args = {
            command: 'kill',
            targets: [citProfile.name],
        };

        const serverResponse = await requests.put('api/actions', args, headers);

        expect(serverResponse.statusCode).to.equal(200);
    });

    it('Will submit a frame action', async() => {
        const [gameObj, factionName] = await createGameWithFramer();
        const framerProfile = await testHelpers.getProfileWithRole(gameObj, 'Framer');
        const playerName = gameObj.players
            .find(player => player.userID !== framerProfile.userID).name;
        const headers = authHelpers.fakeTempAuthHeader(framerProfile.userID);
        const args = {
            command: 'frame',
            option: factionName,
            targets: [playerName],
        };

        const serverResponse = await requests.put('api/actions', args, headers);

        expect(serverResponse.statusCode).to.equal(200);
    });

    it('Will gracefully fail when no team is provided', async() => {
        const [gameObj] = await createGameWithFramer();
        const framerProfile = await testHelpers.getProfileWithRole(gameObj, 'Framer');
        const headers = authHelpers.fakeTempAuthHeader(framerProfile.userID);
        const args = {
            command: 'frame',
            targets: [framerProfile.name],
        };

        const serverResponse = await requests.put('api/actions', args, headers);

        expect(serverResponse.statusCode).to.equal(422);
        expect(serverResponse.json.errors[0]).to.equal('Unknown team: null');
    });

    it('Will cancel a vote action', async() => {
        const setup = await quasiSetup.create({
            modifiers: [{
                name: 'DAY_START',
                value: true,
            }],
            setupHiddenCount: 7,
        });
        const gameObj = await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            isStarted: true,
            playerCount: 7,
            modifiers: [{
                name: 'VOTE_SYSTEM',
                value: voteSystemType.MULTIVOTE_PLURALITY,
            }],
        });
        const headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        const playerName = gameObj.players.find(player => player.userID !== setup.ownerID).name;
        await actionService.submitActionMessage(setup.ownerID, {
            command: 'vote',
            targets: [playerName],
        });

        const serverResponse = await requests.delete(
            'api/actions?actionIndex=0&command=vote', headers,
        );

        expect(serverResponse.statusCode).to.equal(204);
    });

    it('Will cancel an action', async() => {
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
        });
        const goonProfile = await testHelpers.getProfileWithRole(gameObj, 'Goon');
        const citProfile = await testHelpers.getProfileWithRole(gameObj, 'Citizen');
        await actionService.submitActionMessage(goonProfile.userID, {
            command: 'kill',
            targets: [citProfile.name],
        });
        const headers = authHelpers.fakeTempAuthHeader(goonProfile.userID);

        const serverResponse = await requests.delete(
            'api/actions?actionIndex=0&command=kill', headers,
        );
        const userState = await gameService.getUserState(goonProfile.userID);

        expect(serverResponse.statusCode).to.equal(204);
        expect(userState.actions.actionList.length).to.equal(0);
    });

    it('Will relay a motion to end the night if within 2/3rds time limit', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
            modifiers: [{
                name: 'DAY_LENGTH_START',
                value: 10000,
            }, {
                name: 'NIGHT_LENGTH',
                value: 60,
            }],
        });
        const nonHostID = gameObj.users.find(user => !user.isModerator).id;
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        const hostHeaders = authHelpers.fakeTempAuthHeader(moderatorID);
        const nonHostHeaders = authHelpers.fakeTempAuthHeader(nonHostID);
        const messages = await addWebSocket(nonHostHeaders);

        advanceSeconds(41);
        clock.restore();
        const serverResponse = await requests.put('api/actions', { message: 'end night' },
            hostHeaders);
        await new Promise(resolve => {
            setTimeout(resolve, 100);
        });

        expect(serverResponse.statusCode).to.equal(200);
        const hasEndNightUpdate = messages.some(message => message.event === 'phaseEndBid'
            && message.players.some(player => player.endedNight
                && player.userID === moderatorID));
        expect(hasEndNightUpdate).to.be.equal(true);
    });

    it('Will not relay a motion to end the night if within 2/3rds time limit', async() => {
        const gameObj = await quasiGame.create({
            isStarted: true,
            playerCount: 7,
            modifiers: [{
                name: 'NIGHT_LENGTH',
                value: 60,
            }],
        });
        const nonHostID = gameObj.users.find(user => !user.isModerator).id;
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        const hostHeaders = authHelpers.fakeTempAuthHeader(moderatorID);
        const nonHostHeaders = authHelpers.fakeTempAuthHeader(nonHostID);
        const messages = await addWebSocket(nonHostHeaders);

        const serverResponse = await requests.put('api/actions', { message: 'end night' },
            hostHeaders);
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        expect(serverResponse.statusCode).to.equal(200);
        const hasEndNightUpdate = messages.some(message => message.event === 'phaseEndBid'
            && message.players.some(player => player.endedNight
                && player.userID === moderatorID));
        expect(hasEndNightUpdate).to.be.equal(false);
    });

    it('Will push ended night players at 2/3rds mark', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({
            playerCount: 7,
            modifiers: [{
                name: 'DAY_LENGTH_START',
                value: 10000,
            }, {
                name: 'NIGHT_LENGTH',
                value: 2,
            }],
        });
        const nonHostID = gameObj.users.find(user => !user.isModerator).id;
        const moderatorId = gameObj.users.find(user => user.isModerator).id;
        const hostHeaders = authHelpers.fakeTempAuthHeader(moderatorId);
        const nonHostHeaders = authHelpers.fakeTempAuthHeader(nonHostID);
        const messages = await addWebSocket(nonHostHeaders);
        await gameService.start(gameObj.id);
        const args = { message: 'end night' };

        const serverResponse = await requests.put('api/actions', args, hostHeaders);
        advanceSeconds(4 / 3);
        clock.restore();
        await new Promise(resolve => {
            setTimeout(resolve, 100);
        });

        expect(serverResponse.statusCode).to.equal(200);
        const endNightEvent = messages.find(message => message.event === 'phaseEndBid');
        expect(!!endNightEvent).to.be.equal(true);
        const hasVisibleEndNightUpdate = messages.some(message => message.event === 'phaseEndBid'
            && message.players.some(player => player.endedNight));
        expect(hasVisibleEndNightUpdate).to.be.equal(true);
    });

    function advanceSeconds(minutes){
        clock.tick(minutes * 1000);
    }

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }

    async function createGameWithFramer(){
        const setup = await quasiSetup.create();
        const userID = setup.ownerID;
        const gameObj = await quasiGame.create({
            hostID: userID,
            playerCount: 3,
            setupID: setup.id,
        });

        const framer = testHelpers.getFactionRoleWithName(gameObj, 'Framer');
        const framerFaction = testHelpers.getFactionByID(gameObj, framer.factionID);
        const hiddenFramer = await testHelpers.createHidden(setup, 'Framer', framer.id);

        const citizen = testHelpers.getFactionRoleWithName(gameObj, 'Citizen');
        const hiddenCitizen = await testHelpers.createHidden(setup, 'Citizen', citizen.id);

        await Promise.all([hiddenFramer, hiddenCitizen, hiddenFramer]
            .map(hidden => setupHiddenService.add(userID, {
                isExposed: false,
                mustSpawn: false,
                hiddenID: hidden.id,
                minPlayerCount: 0,
                maxPlayerCount: MAX_PLAYER_COUNT,
            })));

        return [await gameService.start(gameObj.id), framerFaction.name];
    }
});

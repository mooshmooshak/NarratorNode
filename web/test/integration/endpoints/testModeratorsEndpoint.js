require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const moderatorService = require('../../../services/moderatorService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const { getModeratorID } = require('../../../utils/lobbyUtil');

const { JOIN_ID } = require('../../fakeConstants');


describe('Moderators', async() => {
    let ws;
    let hostHeaders;
    let setup;
    beforeEach(async() => {
        setup = await quasiSetup.create({ playerCount: 7, setupHiddenCount: 7 });
        hostHeaders = authHelpers.fakeTempAuthHeader(setup.ownerID);
    });

    afterEach(() => {
        if(ws)
            ws.close();
    });

    it('Removes a player from the game, but maintains the hostID', async() => {
        let gameObj = await quasiGame.create({ hostID: setup.ownerID });

        const responseObj = await requests.post('api/moderators', null, hostHeaders);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(gameObj.players.length).to.be.equal(0);
        const moderatorIDs = gameObj.users
            .filter(({ isModerator }) => isModerator)
            .map(({ id }) => id);
        expect(moderatorIDs).to.deep.equal([setup.ownerID]);
    });

    it('Will not allow a moderator to rejoin an already filled game', async() => {
        let gameObj = await quasiGame.create();
        const moderatorID = getModeratorID(gameObj);
        await moderatorService.create(moderatorID);
        await quasiGame.addPlayersToGame(gameObj.joinID, 16);
        const setup2 = await quasiSetup.create();
        await setupService.setSetup({ setupID: setup2.id, joinID: gameObj.joinID });

        const responseObj = await requests.delete('api/moderators', hostHeaders);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(responseObj.json.errors.length).to.be.equal(1);
        expect(gameObj.players.length).to.be.equal(16);
    });

    it('Will allow a moderator to rejoin a game that is not full', async() => {
        let gameObj = await quasiGame.create({ hostID: setup.ownerID, setupID: setup.id });
        await moderatorService.create(setup.ownerID);
        await quasiGame.addPlayersToGame(gameObj.joinID, 5);

        const responseObj = await requests.delete('api/moderators', hostHeaders);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(gameObj.setup.setupHiddens.length).to.be.equal(7);
        expect(gameObj.players.length).to.be.equal(6);
    });

    it('Will repick to another random player', async() => {
        const gameObj = await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            playerCount: 2,
        });
        const otherUserID = gameObj.users.find(user => !user.isModerator).id;
        const args = {
            repickTarget: '',
            gameID: gameObj.id,
        };

        const responseObj = await requests.post('api/moderators/repick', args, hostHeaders);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.moderatorIDs).to.deep.equal([otherUserID]);
    });

    it('Will repick to another specified host', async() => {
        const gameObj = await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            playerCount: 7,
        });
        const otherUser = gameObj.users.find(user => !user.isModerator);
        const otherUserHeaders = authHelpers.fakeTempAuthHeader(otherUser.id);
        const { messages } = await addWebSocket(otherUserHeaders);
        const args = {
            repickTarget: otherUser.name,
            gameID: gameObj.id,
        };

        const responseObj = await requests.post('api/moderators/repick', args, hostHeaders);
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });
        const hostChangeMessage = messages.find(m => m.event === 'hostChange');

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.moderatorIDs).to.deep.equal([otherUser.id]);
        expect(hostChangeMessage.hostID).to.be.equal(otherUser.id);
    });

    it('Will gracefully fail on a string gameID', async() => {
        const user = await quasiUser.createUser();
        const args = {
            repickTarget: user.name,
            gameID: JOIN_ID,
        };

        const responseObj = await requests.post('api/moderators/repick', args, hostHeaders);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    async function addWebSocket(inputHeaders){
        const response = await quasiWebSocket.connect(inputHeaders);
        ws = response.socket;
        return response;
    }
});

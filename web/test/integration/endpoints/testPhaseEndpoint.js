require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const phaseService = require('../../../services/phaseService');
const userPermissionService = require('../../../services/userPermissionService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const { JOIN_ID } = require('../../fakeConstants');


describe('Phase Endpoint', () => {
    let clock;
    let ws;

    afterEach(() => {
        clock.restore();
        if(ws)
            ws.close();
    });

    it('Should alter the remaining time left on the phase', async() => {
        clock = sinon.useFakeTimers();
        const gameObj = await quasiGame.create({ playerCount: 7 });
        const userID = gameObj.users.find(user => user.isModerator).id;
        await userPermissionService.setAdmin(userID);
        const headers = authHelpers.fakeTempAuthHeader(userID);
        const messages = await addWebSocket(headers);
        await setDayStart(gameObj, 100);
        await gameService.start(gameObj.id);
        const args = {
            seconds: 50,
            joinID: gameObj.joinID,
        };

        const responseObj = await requests.put('api/phases', args, headers);
        const remainingTime = phaseService.getEndTime(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(remainingTime).to.be.equal(new Date().getTime() + (args.seconds * 1000));

        clock.restore();
        await new Promise(resolve => {
            setTimeout(resolve, 2000); // not sure why i need to wait 2 seconds
        });
        const timerUpdateMessage = messages.find(m => m.event === 'timerUpdate');
        expect(timerUpdateMessage.endTime).to.be.equal(remainingTime);
    });

    it('Should force end the phase', async() => {
        clock = sinon.useFakeTimers();
        let gameObj = await quasiGame.create({ playerCount: 8 });
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        await userPermissionService.setAdmin(moderatorID);
        const headers = authHelpers.fakeTempAuthHeader(moderatorID);
        await setDayStart(gameObj, 100);
        gameObj = await gameService.start(gameObj.id);
        const args = {
            seconds: 0,
            joinID: gameObj.joinID,
        };
        const phase = gameObj.phase.name;

        const responseObj = await requests.put('api/phases', args, headers);
        const newPhase = (await gameService.getByLobbyID(gameObj.joinID)).phase.name;

        expect(responseObj.statusCode).to.be.equal(200);
        expect(phase).to.not.be.be.equal(newPhase);
    });

    it('Should lengthen the phase', async() => {
        clock = sinon.useFakeTimers();
        let gameObj = await quasiGame.create({ playerCount: 8 });
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        await userPermissionService.setAdmin(moderatorID);
        const headers = authHelpers.fakeTempAuthHeader(moderatorID);
        await setDayStart(gameObj, 100);
        gameObj = await gameService.start(gameObj.id);
        const args = {
            seconds: 200,
            joinID: gameObj.joinID,
        };
        const phase = gameObj.phase;

        const responseObj = await requests.put('api/phases', args, headers);
        advanceSeconds(101);
        const remainingTime = phaseService.getEndTime(gameObj.joinID);
        gameObj = await gameService.getByLobbyID(gameObj.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(remainingTime).to.be.equal(new Date().getTime() + (args.seconds * 1000) - 101000);
        expect(phase.name).to.be.equal(gameObj.phase.name);
    });

    it('Should gracefully fail when seconds isn\'t specified', async() => {
        const gameObj = await quasiGame.create({ playerCount: 8 });
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        await userPermissionService.setAdmin(moderatorID);
        const headers = authHelpers.fakeTempAuthHeader(moderatorID);
        const args = {
            seconds: 200,
        };
        await setDayStart(gameObj, 100);
        await gameService.start(gameObj.id);

        const responseObj = await requests.put('api/phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Should gracefully fail when joinID isn\'t specified', async() => {
        const gameObj = await quasiGame.create({ playerCount: 8 });
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        await userPermissionService.setAdmin(moderatorID);
        const headers = authHelpers.fakeTempAuthHeader(moderatorID);
        const args = {
            joinID: gameObj.joinID,
        };
        await setDayStart(gameObj, 100);
        await gameService.start(gameObj.id);

        const responseObj = await requests.put('api/phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Should not allow edits unless is admin', async() => {
        const gameObj = await quasiGame.create({ playerCount: 8 });
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        const headers = authHelpers.fakeTempAuthHeader(moderatorID);
        const args = {
            seconds: 200,
            joinID: gameObj.joinID,
        };

        const responseObj = await requests.put('api/phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(401);
    });

    it('Should not allow edits if not in game', async() => {
        const user = await quasiUser.createUser();
        await userPermissionService.setAdmin(user.id);
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const args = {
            seconds: 200,
            joinID: JOIN_ID,
        };

        const responseObj = await requests.put('api/phases', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    function advanceSeconds(minutes){
        clock.tick(minutes * 1000);
    }

    async function addWebSocket(socketHeaders){
        const response = await quasiWebSocket.connect(socketHeaders);
        ws = response.socket;
        return response.messages;
    }
});

function setDayStart(gameObj, seconds){
    const moderatorID = gameObj.users.find(user => user.isModerator).id;
    return gameService.updateModifier(moderatorID, {
        name: 'DAY_LENGTH_START',
        value: seconds,
    });
}

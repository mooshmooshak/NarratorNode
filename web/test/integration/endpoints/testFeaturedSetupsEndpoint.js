require('../../init');
const { expect } = require('chai');

const helpers = require('../../../utils/helpers');

const requests = require('../../requests');

const setupsFeaturedRepo = require('../../../repos/setupFeaturedRepo');
const setupRepo = require('../../../repos/setupRepo');

const setupService = require('../../../services/setup/setupService');

const authHelpers = require('../../authHelpers');
const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const { FEATURED_PRIORITY, FEATURED_KEY } = require('../../fakeConstants');


describe('Featured setups endpoint', () => {
    it('Will make a setup featured', async() => {
        const [modUser, setup] = await Promise.all([
            quasiUser.createMod(),
            quasiSetup.create(),
        ]);
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);
        const priority = FEATURED_PRIORITY;
        const key = FEATURED_KEY;

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`,
            { priority, key }, headers);
        const dbPriority = await setupsFeaturedRepo.getPriority(setup.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(dbPriority.priority).to.be.equal(FEATURED_PRIORITY);
        expect(dbPriority.key).to.be.equal(FEATURED_KEY);
    });

    it('Will gracefully error if \'key\' is missing', async() => {
        const modUser = await quasiUser.createMod();
        const setup = await quasiSetup.create({ ownerID: modUser.id });
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`,
            { priority: FEATURED_PRIORITY }, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully error if \'priority\' is missing', async() => {
        const modUser = await quasiUser.createMod();
        const setup = await quasiSetup.create({ ownerID: modUser.id });
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`,
            { key: FEATURED_KEY }, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully error if the key is too long', async() => {
        const modUser = await quasiUser.createMod();
        const setup = await quasiSetup.create({ ownerID: modUser.id });
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`,
            { key: helpers.getRandomString(12), priority: FEATURED_PRIORITY }, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully error if the key is too short', async() => {
        const modUser = await quasiUser.createMod();
        const setup = await quasiSetup.create({ ownerID: modUser.id });
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`,
            { key: '', priority: FEATURED_PRIORITY }, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow duplicate featured setup keys', async() => {
        const modUser = await quasiUser.createMod();
        const [setup, setup2] = await Promise.all([
            quasiSetup.create({ ownerID: modUser.id }),
            quasiSetup.create({ ownerID: modUser.id }),
        ]);
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);
        const args = {
            priority: FEATURED_PRIORITY,
            key: FEATURED_KEY,
        };
        await setupService.setPriority(setup2.id, args.priority, args.key);

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`, args, headers);
        const updatedSetup = await setupRepo.getByID(setup.id);

        expect(updatedSetup.isEditable).to.be.equal(true);
        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow negative numbers', async() => {
        const modUser = await quasiUser.createMod();
        const setup = await quasiSetup.create({ ownerID: modUser.id });
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);
        const priority = FEATURED_PRIORITY * -1;
        const key = FEATURED_KEY;

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`,
            { priority, key }, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow regular users to set setups as featured', async() => {
        const user = await quasiUser.createUser();
        const setup = await quasiSetup.create({ ownerID: user.id });
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const priority = FEATURED_PRIORITY;
        const key = FEATURED_KEY;

        const responseObj = await requests.post(`api/setups/${setup.id}/featured`,
            { priority, key }, headers);

        expect(responseObj.statusCode).to.be.equal(403);
    });

    it('Will delete featured references on setup delete', async() => {
        const modUser = await quasiUser.createMod();
        const setup = await quasiSetup.create({ ownerID: modUser.id });
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);
        const priority = FEATURED_PRIORITY;

        await requests.post(`api/setups/${setup.id}/featured`, { priority }, headers);
        const setupDeleteResponse = await requests.delete(`api/setups/${setup.id}`, headers);
        const dbPriority = await setupsFeaturedRepo.getPriority(setup.id);

        expect(setupDeleteResponse.statusCode).to.be.equal(204);
        expect(dbPriority).to.be.a('null');
    });

    it('Will set a setup editable on featured remove', async() => {
        const modUser = await quasiUser.createMod();
        let setup = await quasiSetup.create({ ownerID: modUser.id });
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const serverResponse = await requests.delete(`api/setups/${setup.id}/featured`, headers);
        setup = await setupService.getByID(setup.id);
        const dbPriority = await setupsFeaturedRepo.getPriority(setup.id);

        expect(serverResponse.statusCode).to.be.equal(204);
        expect(dbPriority).to.be.a('null');
        expect(setup.isEditable).to.be.equal(true);
    });

    it('Will not set a setup with a started game editable on featured remove', async() => {
        const modUser = await quasiUser.createMod();
        const headers = authHelpers.fakeTempAuthHeader(modUser.id);
        let { setup } = await quasiGame.create({
            isStarted: true,
            hostID: modUser.id,
            playerCount: 4,
        });
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const serverResponse = await requests.delete(`api/setups/${setup.id}/featured`, headers);
        setup = await setupService.getByID(setup.id);

        expect(serverResponse.statusCode).to.be.equal(204);
        expect(setup.isEditable).to.be.equal(false);
    });

    it('Will not allow regular users to removed setups from featured', async() => {
        const user = await quasiUser.createUser();
        const headers = authHelpers.fakeTempAuthHeader(user.id);
        const setup = await quasiSetup.create({ ownerID: user.id });

        const serverResponse = await requests.delete(`api/setups/${setup.id}/featured`, headers);

        expect(serverResponse.statusCode).to.be.equal(403);
    });
});

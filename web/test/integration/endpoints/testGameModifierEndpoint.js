require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameModifierRepo = require('../../../repos/gameModifierRepo');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');

const { getModeratorID } = require('../../../utils/lobbyUtil');

const { FEATURED_KEY, FEATURED_PRIORITY } = require('../../fakeConstants');


describe('Game Modifier Endpoint', () => {
    let headers;
    let gameObject;
    let modifierName;
    let modifierOldValue;
    let modifierNewValue;
    beforeEach(async() => {
        gameObject = await quasiGame.create();
        const moderatorID = getModeratorID(gameObject);
        headers = authHelpers.fakeTempAuthHeader(moderatorID);
        const modifier = Object.values(gameObject.modifiers)
            .find(m => typeof(m.value) === 'boolean');
        modifierName = modifier.name;
        modifierOldValue = modifier.value;
        modifierNewValue = !modifier.value;
    });

    it('Will update the game\'s modifier (boolean)', async() => {
        const boolModifier = Object.values(gameObject.modifiers)
            .find(modifier => typeof(modifier.value) === 'boolean');
        const newValue = !boolModifier.value;

        const responseObj = await createRequest({
            modifierName: boolModifier.name,
            modifierValue: newValue,
        });
        gameObject = await gameService.getByID(gameObject.id);
        const dbModifiers = await gameModifierRepo.getByGameID(gameObject.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = Object.values(gameObject.modifiers)
            .find(modifier => modifier.name === boolModifier.name);
        expect(newModifier.value).to.be.equal(newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
        expect(dbModifiers[boolModifier.name]).to.be.equal(newModifier.value);
    });

    it('Will update the game\'s modifier (integer)', async() => {
        const intModifier = Object.values(gameObject.modifiers)
            .find(modifier => typeof(modifier.value) === 'number');
        const newValue = intModifier.value + 1;

        const responseObj = await createRequest({
            modifierName: intModifier.name,
            modifierValue: newValue,
        });
        gameObject = await gameService.getByID(gameObject.id);
        const dbModifiers = await gameModifierRepo.getByGameID(gameObject.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = Object.values(gameObject.modifiers)
            .find(modifier => modifier.name === intModifier.name);
        expect(newModifier.value).to.be.equal(newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
        expect(dbModifiers[intModifier.name]).to.be.equal(newModifier.value);
    });

    it('Will allow updates if the game is featured', async() => {
        await setupService.setPriority(gameObject.setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await createRequest();

        expect(responseObj.statusCode).to.be.equal(200);
    });

    it('Will not allow updates if the user is not the host', async() => {
        const user2 = await quasiUser.createUser();

        const responseObj = await createRequest({
            headers: authHelpers.fakeTempAuthHeader(user2.id),
        });
        gameObject = await gameService.getByID(gameObject.id);

        expect(responseObj.statusCode).to.be.equal(422);
        const newModifier = Object.values(gameObject.modifiers)
            .find(modifier => modifier.name === modifierName);
        expect(newModifier.value).to.be.equal(modifierOldValue);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        // given setup in before each

        const responseObj = await createRequest({
            modifierName: modifierName + modifierName,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        // given setup in before each

        const responseObj = await createRequest({
            modifierValue: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        // given setup in before each

        const responseObj = await createRequest({
            modifierName: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        const boolModifier = Object.values(gameObject.modifiers)
            .find(modifier => typeof(modifier.value) === 'boolean');

        const responseObj = await createRequest({
            modifierName: boolModifier.name,
            modifierValue: 400,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        const intModifier = Object.values(gameObject.modifiers)
            .find(modifier => typeof(modifier.value) === 'number');

        const responseObj = await createRequest({
            modifierName: intModifier.name,
            modifierValue: true,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail updates when the vote system type does not exist', async() => {
        // given in before each

        const responseObj = await createRequest({
            modifierName: 'VOTE_SYSTEM',
            modifierValue: 50, // just something out of range of the vote systems i have now and will ever have
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    function createRequest(args = {}){
        const payload = {
            name: 'modifierName' in args ? args.modifierName : modifierName,
            value: 'modifierValue' in args ? args.modifierValue : modifierNewValue,
        };
        return requests.post('api/gameModifiers', payload, args.headers || headers);
    }
});

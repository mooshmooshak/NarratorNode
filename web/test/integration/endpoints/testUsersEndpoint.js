require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const userSchema = require('../../schemas/userSchema');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const { getModeratorID } = require('../../../utils/lobbyUtil');


describe('Users', async() => {
    let ws;
    afterEach(() => {
        if(ws)
            ws.close();
    });

    describe('Get auth token', () => {
        it('Will fail gracefully when the authToken no longer exists', async() => {
            const authToken = new Array(128 + 1).join('A');

            const responseObj = await requests.get(`api/users/?auth_token=${authToken}`);

            expect(responseObj.statusCode).to.be.equal(422);
        });
    });

    describe('Add temp auth', () => {
        it('Will add a temporary auth token', async() => {
            const resonseObj = await createTempAuth();

            expectStatusCode(200, resonseObj);
        });

        async function createTempAuth(){
            const user = await quasiUser.createUser();
            return requests.post(`api/users/${user.id}/auth_token`);
        }
    });

    it('Get all users', () => {
        it('Will get all users', async() => {
            await quasiUser.createUser();

            const responseObj = await getUsers();

            expect(responseObj.statusCode).to.be.equal(200);
            const users = responseObj.json.response;
            expect(users.length).to.be.equal(1);
            users.forEach(userSchema.check);
        });

        function getUsers(){
            return requests.get('api/users');
        }
    });

    it('Will get online users', () => {
        it('Will get online users', async() => {
            const game = await quasiGame.create();
            const headers = authHelpers.fakeTempAuthHeader(game.users[0].id);
            await addWebSocket(headers);

            const responseObj = await getOnlineUsers();

            expect(responseObj.statusCode).to.be.equal(200);
            const users = responseObj.json.response;
            expect(users.length).to.be.equal(1);
            users.forEach(userSchema.check);
            expect(users[0].name).to.be.equal(game.players[0].name);
        });

        it('Will not return duplicate userIDs for a user that was in multiple games', async() => {
            const gameObj = await quasiGame.create({ playerCount: 3, isFinished: true });
            const moderatorID = getModeratorID(gameObj);
            await quasiGame.create({ hostID: moderatorID });
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);
            await addWebSocket(headers);

            const responseObj = await getOnlineUsers();

            expect(responseObj.statusCode).to.be.equal(200);
            const users = responseObj.json.response;
            expect(users.length).to.be.equal(1);
            users.forEach(userSchema.check);
        });

        it('Will return no online users', async() => {
            // No Given

            const responseObj = await getOnlineUsers();

            expect(responseObj.statusCode).to.be.equal(200);
            const users = responseObj.json.response;
            expect(users.length).to.be.equal(0);
        });

        function getOnlineUsers(){
            return requests.get('api/users/online');
        }
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});

require('../../init');
const { expect } = require('chai');
const { flatten } = require('lodash');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupService = require('../../../services/setup/setupService');
const gameService = require('../../../services/gameService');

const hiddenSpawnSchema = require('../../schemas/hiddenSpawnSchema');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');

const testControllerHelpers = require('../../testControllerHelpers');
const { MAX_PLAYER_COUNT } = require('../../../utils/constants');

const { FEATURED_PRIORITY, FEATURED_KEY } = require('../../fakeConstants');


describe('Hidden Spawns Endpoint', () => {
    let setup;
    let headers;
    let factionRoleID;
    let hiddenID;
    let hiddenSpawnID;
    let gameObject;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        const [hidden] = setup.hiddens;
        const spawnableFactionRoleIDs = hidden.spawns.map(spawn => spawn.factionRoleID);
        hiddenID = hidden.id;
        const factionRoleIDs = flatten(setup.factions.map(f => f.factionRoles));
        factionRoleID = factionRoleIDs
            .find(fr => !spawnableFactionRoleIDs.includes(fr.id)).id;
        hiddenSpawnID = hidden.spawns[0].id;
        gameObject = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
    });

    it('Will create a hidden spawn', async() => {
        // given in beforeEach

        const responseObj = await createRequest();

        expect(responseObj.statusCode).to.be.equal(200);
        hiddenSpawnSchema.check(responseObj.json.response[0]);
    });

    it('Will allow creates of a hidden spawn with different player count constraints', async() => {
        const divider = 10;

        let responseObj = await createRequest({ maxPlayerCount: divider });

        expect(responseObj.statusCode).to.be.equal(200);

        responseObj = await createRequest({ minPlayerCount: divider + 1 });

        expect(responseObj.statusCode).to.be.equal(200);
    });

    it('Will not allow creates of duplicate hidden spawns', async() => {
        // given in beforeEach

        let responseObj = await createRequest();

        expect(responseObj.statusCode).to.be.equal(200);

        responseObj = await createRequest();

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the min player count is too low', async() => {
        // given in before each

        const responseObj = await createRequest({ minPlayerCount: -1 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the max player count is too low', async() => {
        // given in before each

        const responseObj = await createRequest({ maxPlayerCount: 2 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the min player count is too high', async() => {
        // given in before each

        const responseObj = await createRequest({ minPlayerCount: MAX_PLAYER_COUNT + 1 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the max player count is too high', async() => {
        // given in before each

        const responseObj = await createRequest({ maxPlayerCount: MAX_PLAYER_COUNT + 1 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the hidden id is missing', async() => {
        // given in before each

        const responseObj = await createRequest({ hiddenID: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the faction role id is missing', async() => {
        // given in before each

        const responseObj = await createRequest({ factionRoleID: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the max player count is missing', async() => {
        // given in before each

        const responseObj = await createRequest({ maxPlayerCount: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail create requests if the min player count is missing', async() => {
        // given in before each

        const responseObj = await createRequest({ minPlayerCount: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow create hidden spawns with hiddens don\'t exist', async() => {
        const nonExistantHiddenID = Math.max(...setup.hiddens.filter(h => h.id)) + 1;

        const responseObj = await createRequest({ hiddenID: nonExistantHiddenID });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow create hidden spawns with faction roles don\'t exist', async() => {
        const factionRoles = flatten(setup.factions.map(f => f.factionRoles));
        const nonExistantFactionRoleID = Math.max(...factionRoles.map(fr => fr.id)) + 1;

        const responseObj = await createRequest({ factionRoleID: nonExistantFactionRoleID });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow create hidden spawns on a featured setup', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await createRequest();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    // eslint-disable-next-line max-len
    it('Will not allow associations of hidden spawns hiddens with faction roles that are not in the setup', async() => {
        const setup2 = await quasiSetup.create();
        const otherHiddenID = setup2.hiddens[0].id;

        const responseObj = await createRequest({ hiddenID: otherHiddenID });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will delete a hidden spawn', async() => {
        // given in beforeEach

        const responseObj = await requests.delete(`api/hiddenSpawns/${hiddenSpawnID}`, headers);
        gameObject = await gameService.getByID(gameObject.id);
        const updatedHidden = gameObject.setup.hiddens.find(h => h.spawns
            .map(spawn => spawn.id)
            .includes(hiddenSpawnID));

        expect(responseObj.statusCode).to.be.equal(204);
        expect(updatedHidden).to.be.an('undefined');
    });

    it('Will not delete a hidden spawn in a featured setup', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await requests.delete(`api/hiddenSpawns/${hiddenSpawnID}`, headers);

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function createRequest(args = {}){
        const payload = {
            hiddenID: 'hiddenID' in args ? args.hiddenID : hiddenID,
            factionRoleID: 'factionRoleID' in args ? args.factionRoleID : factionRoleID,
            minPlayerCount: 'minPlayerCount' in args ? args.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in args ? args.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        return requests.post('api/hiddenSpawns', [payload], headers);
    }
});

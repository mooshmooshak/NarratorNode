require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');

const authHelpers = require('../../authHelpers');
const helpers = require('../../../utils/helpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const factionAbilitySchema = require('../../schemas/factionAbilitySchema');
const factionSchema = require('../../schemas/factionSchema');

const testControllerHelpers = require('../../testControllerHelpers');

const {
    COLOR, FACTION_NAME, FEATURED_PRIORITY, FEATURED_KEY,
} = require('../../fakeConstants');


describe('Faction Endpoint', () => {
    let defaultHeaders;
    let setup;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        defaultHeaders = authHelpers.fakeTempAuthHeader(setup.ownerID);
    });

    describe('Create faction', () => {
        it('Will create a faction', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await createFaction();

            expect(responseObj.statusCode).to.be.equal(200);
            factionSchema.check(responseObj.json.response);
        });

        it('Will not add factions if the setup id does not match the game setup id', async() => {
            const setup2 = await quasiSetup.create({ ownerID: setup.ownerID });

            const responseObj = await createFaction({ setupID: setup2.id });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not create factions in featured setups', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

            const responseObj = await createFaction();

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        it('Will allow one to create factions if they\'ve hosted a game before', async() => {
            await quasiGame.create({ hostID: setup.ownerID, isFinished: true, playerCount: 7 });
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await createFaction();

            expect(responseObj.statusCode).to.be.equal(200);
        });

        it('Will not create factions with bad characters in the name', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await createFaction({ name: 'Mafia.' });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not allow create requests with no setup id', async() => {
            // given created in beforeEach

            const responseObj = await createFaction({ setupID: undefined });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not allow duplicate faction names', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionName = setup.factions[0].name;

            const responseObj = await createFaction({ name: factionName });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not allow duplicate faction colors', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionColor = setup.factions[0].color;

            const responseObj = await createFaction({ color: factionColor });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not allow faction names that are too long', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionName = helpers.getRandomString(41);

            const responseObj = await createFaction({ name: factionName });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not allow faction names that are too short', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionName = '';

            const responseObj = await createFaction({ name: factionName });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not allow a description that is too long', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const description = helpers.getRandomString(501);

            const responseObj = await createFaction({ description });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        function createFaction(input = {}){
            const inputKeys = Object.keys(input);
            const args = {
                color: inputKeys.includes('color') ? input.color : COLOR,
                description: inputKeys.includes('description') ? input.description : '',
                name: inputKeys.includes('name') ? input.name : FACTION_NAME,
                setupID: inputKeys.includes('setupID') ? input.setupID : setup.id,
            };
            return requests.post('api/factions', args, defaultHeaders);
        }
    });

    describe('Edit faction', () => {
        let editingFaction;
        let setupID;
        let newName;
        let newDescription;
        beforeEach(() => {
            editingFaction = setup.factions[0];
            setupID = setup.id;
            newName = `${editingFaction.name}2`;
            newDescription = `${editingFaction.description}2`;
        });

        it('Will edit a faction', async() => {
            // given in beforeEach

            const responseObj = await updateFaction();
            const updatedSetup = await setupService.getByID(setupID);
            const newFaction = updatedSetup.factions.find(f => f.id === editingFaction.id);

            expectStatusCode(204, responseObj);
            expect(newFaction.name).to.be.equal(newName);
            expect(newFaction.description).to.be.equal(newDescription);
        });

        it('Will gracefully error on bad name updates', async() => {
            const conflictingName = setup.factions.find(f => f.id !== editingFaction.id).name;

            const responseObj = await updateFaction({ name: conflictingName });

            expectStatusCode(422, responseObj);
            await assertNoUpdates();
        });

        it('Will not allow edits without headers', async() => {
            // given in before each

            const responseObj = await updateFaction({ headers: undefined });

            expectStatusCode(403, responseObj);
            await assertNoUpdates();
        });

        it('Will 404 on bad setup ids', async() => {
            const badSetupID = 'abc';

            const responseObj = await updateFaction({ setupID: badSetupID });

            expectStatusCode(404, responseObj);
            await assertNoUpdates();
        });

        it('Will handle setup IDs that do not exist', async() => {
            const badSetupID = setupID + 1;

            const responseObj = await updateFaction({ setupID: badSetupID });

            expectStatusCode(404, responseObj);
            await assertNoUpdates();
        });

        it('Will 404 on bad faction ids', async() => {
            const badFactionID = 'abc';

            const responseObj = await updateFaction({ factionID: badFactionID });

            expectStatusCode(404, responseObj);
            await assertNoUpdates();
        });

        it('Will handle no names in request', async() => {
            const badName = undefined;

            const responseObj = await updateFaction({ name: badName });

            expectStatusCode(422, responseObj);
            await assertNoUpdates();
        });

        it('Will handle no description in request', async() => {
            const badDescription = undefined;

            const responseObj = await updateFaction({ description: badDescription });

            expectStatusCode(422, responseObj);
            await assertNoUpdates();
        });

        it('Will stop updates if the user does not own the setup', async() => {
            const newUser = await quasiUser.createUser();
            const otherHeaders = authHelpers.fakeTempAuthHeader(newUser.id);

            const responseObj = await updateFaction({ headers: otherHeaders });

            expectStatusCode(403, responseObj);
            await assertNoUpdates();
        });

        it('Will not allow edits to uneditable setups', async() => {
            await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

            const responseObj = await updateFaction();

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
            await assertNoUpdates();
        });

        function updateFaction(args = {}){
            const body = {
                description: 'description' in args ? args.description : newDescription,
                name: 'name' in args ? args.name : newName,
            };
            const requestSetupID = 'setupID' in args ? args.setupID : setupID;
            const requestFactionID = 'factionID' in args ? args.factionID : editingFaction.id;
            const requestHeaders = 'headers' in args ? args.headers : defaultHeaders;
            return requests.put(`api/setups/${requestSetupID}/factions/${requestFactionID}`,
                body, requestHeaders);
        }

        async function assertNoUpdates(){
            const updatedSetup = await setupService.getByID(setupID);
            const newFaction = updatedSetup.factions.find(f => f.id === editingFaction.id);
            expect(newFaction.name).to.be.equal(editingFaction.name);
        }
    });

    describe('Create Faction Ability', () => {
        it('Will add a shared ability', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await addFactionAbility();

            expect(responseObj.statusCode).to.be.equal(200);
            factionAbilitySchema.check(responseObj.json.response);
        });

        it('Will gracefully fail ability adds if the factionID is not a number', async() => {
            // no given

            const responseObj = await addFactionAbility({ factionID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will not add faction abilities to featured setups', async() => {
            await Promise.all([
                quasiGame.create({ setupID: setup.id, hostID: setup.ownerID }),
                setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY),
            ]);

            const responseObj = await addFactionAbility();

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        it('Will gracefully fail ability adds if the factionID does not exist', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionID = setup.factions[0].id + setup.factions.length + 500;

            const responseObj = await addFactionAbility({ factionID });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail ability adds if no ability is supplied', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await addFactionAbility({ ability: undefined });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail ability adds if the ability is not valid', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await addFactionAbility({ ability: 'SuperFakeAbility' });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully allow duplicate ability adds', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const faction = setup.factions.find(f => f.abilities.length);

            const responseObj = await addFactionAbility({
                factionID: faction.id,
                ability: faction.abilities[0].name,
            });
            setup = await setupService.getByID(setup.id);
            const updatedFaction = setup.factions.find(f => f.id === faction.id);

            expect(responseObj.statusCode).to.be.equal(422);
            expect(updatedFaction.abilities.length).to.be.equal(faction.abilities.length);
        });

        function addFactionAbility(input = {}){
            const inputKeys = Object.keys(input);
            const factionID = inputKeys.includes('factionID') ? input.factionID
                : setup.factions[0].id;
            const args = {
                ability: inputKeys.includes('ability') ? input.ability : 'Block',
            };
            return requests.post(`api/factions/${factionID}/abilities`, args, defaultHeaders);
        }
    });

    describe('Delete Faction Ability', () => {
        let factionID;
        beforeEach(() => {
            factionID = setup.factions.find(f => f.abilities.length).id;
        });

        it('Will delete a shared ability', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await deleteFactionAbility();
            const newSetup = await setupService.getByID(setup.id);

            expectStatusCode(204, responseObj);
            expect(newSetup.factions.find(f => f.id === factionID).abilities.length).to.be.equal(0);
        });

        it('Will gracefully fail ability deletes if the factionID is not a number', async() => {
            // no given

            const responseObj = await deleteFactionAbility({ factionID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will gracefully fail ability deletes if the abilityID is not a number', async() => {
            // no given

            const responseObj = await deleteFactionAbility({ abilityID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will not delete faction abilities to featured setups', async() => {
            await Promise.all([
                quasiGame.create({ setupID: setup.id, hostID: setup.ownerID }),
                setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY),
            ]);

            const responseObj = await deleteFactionAbility();

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        it('Will handle no user headers', async() => {
            const badHeaders = undefined;

            const responseObj = await deleteFactionAbility({ headers: badHeaders });

            expectStatusCode(403, responseObj);
        });

        function deleteFactionAbility(input = {}){
            const requestFactionID = 'factionID' in input ? input.factionID : factionID;
            const abilityID = 'abilityID' in input ? input.abilityID
                : setup.factions.find(f => f.id === factionID).abilities[0].id;
            const requestHeaders = 'headers' in input ? input.headers : defaultHeaders;
            const url = `api/factions/${requestFactionID}/abilities/${abilityID}`;
            return requests.delete(url, requestHeaders);
        }
    });

    describe('Enemy creating', () => {
        it('Will add an enemy to a faction', async() => {
            let gameObj = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionID = setup.factions[0].id;
            const enemyID = setup.factions
                .find(f => f.id !== factionID && !f.enemyIDs.includes(factionID)).id;

            const responseObj = await addEnemy({ factionID, enemyID });
            gameObj = await gameService.getByID(gameObj.id);
            const enemyFaction = gameObj.setup.factions.find(f => f.id === enemyID);
            const faction = gameObj.setup.factions.find(f => f.id === factionID);

            expect(responseObj.statusCode).to.be.equal(204);
            expect(faction.enemyIDs).to.include(enemyID);
            expect(enemyFaction.enemyIDs).to.include(factionID);
        });

        it('Will gracefully fail faction enemy adds if the factionID is not a number', async() => {
            // no given

            const responseObj = await addEnemy({ factionID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will gracefully fail faction enemy adds if the enemyID is not a number', async() => {
            // no given

            const responseObj = await addEnemy({ enemyID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will not add enemies to featured setups', async() => {
            await Promise.all([
                quasiGame.create({ setupID: setup.id, hostID: setup.ownerID }),
                setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY),
            ]);

            const responseObj = await addEnemy();

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        it('Will not add enemies if the faction id does not exist', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionID = setup.factions[0].id + setup.factions.length + 100;

            const responseObj = await addEnemy({ factionID });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not add enemies if the enemyID does not exist', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const enemyID = setup.factions[0].id + setup.factions.length + 100;

            const responseObj = await addEnemy({ enemyID });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not add enemies from another setup', async() => {
            const [otherSetup] = await Promise.all([
                quasiSetup.create(),
                quasiGame.create({ setupID: setup.id, hostID: setup.ownerID }),
            ]);

            const responseObj = await addEnemy({ enemyID: otherSetup.factions[0].id });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        function addEnemy(input = {}){
            const factionID = input.factionID || setup.factions[0].id;
            const enemyID = input.enemyID || factionID;
            return requests.post(
                `api/factions/${factionID}/enemies/${enemyID}`, null, defaultHeaders,
            );
        }
    });

    describe('Enemy deleting', () => {
        it('Will delete an enemy to a faction', async() => {
            let gameObj = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            let faction = setup.factions.find(f => f.enemyIDs.length);
            const enemyID = faction.enemyIDs[0];

            const responseObj = await deleteEnemy({ factionID: faction.id, enemyID });
            gameObj = await gameService.getByID(gameObj.id);
            const enemyFaction = gameObj.setup.factions.find(f => f.id === enemyID);
            faction = gameObj.setup.factions.find(f => f.id === faction.id);

            expectStatusCode(204, responseObj);
            expect(faction.enemyIDs).to.not.include(enemyID);
            expect(enemyFaction.enemyIDs).to.not.include(faction.id);
        });

        // eslint-disable-next-line max-len
        it('Will gracefully fail faction enemy delete if the factionID is not a number', async() => {
            // no given

            const responseObj = await deleteEnemy({ factionID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will gracefully fail faction enemy deletes if the enemyID is not a number', async() => {
            // no given

            const responseObj = await deleteEnemy({ enemyID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will not delete enemies to featured setups', async() => {
            await Promise.all([
                quasiGame.create({ setupID: setup.id, hostID: setup.ownerID }),
                setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY),
            ]);

            const responseObj = await deleteEnemy();

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        function deleteEnemy(input = {}){
            const factionID = input.factionID || setup.factions[0].id;
            const enemyID = input.enemyID || factionID;
            const setupID = setup.id;
            const url = `api/setups/${setupID}/factions/${factionID}/enemies/${enemyID}`;
            return requests.delete(url, defaultHeaders);
        }
    });

    describe('Creating sheriff checkable', () => {
        it('Will add a checkable faction for sheriffs', async() => {
            let gameObj = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionID = setup.factions[0].id;
            const checkableID = setup.factions
                .find(f => f.id !== factionID && !f.checkableIDs.includes(factionID)).id;

            const responseObj = await addCheckableForSheriff({ factionID, checkableID });
            gameObj = await gameService.getByID(gameObj.id);
            const faction = gameObj.setup.factions.find(f => f.id === factionID);

            expectStatusCode(204, responseObj);
            expect(faction.checkableIDs).to.include(checkableID);
        });

        it('Will respond ok when adding an already checkable faction for sheriffs', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const faction = setup.factions
                .find(f => f.checkableIDs.length);
            const [checkableID] = faction.checkableIDs;

            const responseObj = await addCheckableForSheriff(
                { factionID: faction.id, checkableID },
            );

            expectStatusCode(204, responseObj);
        });

        it('Will gracefully fail checkable adds if the factionID is not a number', async() => {
        // no given

            const responseObj = await addCheckableForSheriff({ factionID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will gracefully fail faction enemy adds if the enemyID is not a number', async() => {
        // no given

            const responseObj = await addCheckableForSheriff({ checkableID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will not add a checkable faction for sheriffs to featured setups', async() => {
            await Promise.all([
                quasiGame.create({ setupID: setup.id, hostID: setup.ownerID }),
                setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY),
            ]);

            const responseObj = await addCheckableForSheriff();

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        // eslint-disable-next-line max-len
        it('Will not add a checkable faction for sheriffs if the faction id does not exist', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionID = setup.factions[0].id + setup.factions.length + 100;

            const responseObj = await addCheckableForSheriff({ factionID });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        // eslint-disable-next-line max-len
        it('Will not add a detectable faction for sheriffs if the detectable faction id does not exist', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const checkableID = setup.factions[0].id + setup.factions.length + 100;

            const responseObj = await addCheckableForSheriff({ checkableID });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not add checkable factions from another setup', async() => {
            const [otherSetup] = await Promise.all([
                quasiSetup.create(),
                quasiGame.create({ setupID: setup.id, hostID: setup.ownerID }),
            ]);

            const responseObj = await addCheckableForSheriff(
                { checkableID: otherSetup.factions[0].id },
            );

            expect(responseObj.statusCode).to.be.equal(422);
        });

        function addCheckableForSheriff(input = {}){
            const factionID = input.factionID || setup.factions[0].id;
            const checkableID = input.checkableID || factionID;
            return requests.post(
                `api/factions/${factionID}/checkables/${checkableID}`, null, defaultHeaders,
            );
        }
    });

    describe('Delete faction', () => {
        it('Will successfully delete a faction', async() => {
            let gameObject = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const factionLength = gameObject.setup.factions.length;
            const factionID = gameObject.setup.factions[0].id;

            const responseObj = await deleteFaction({ factionID });
            gameObject = await gameService.getByLobbyID(gameObject.joinID);

            expect(responseObj.statusCode).to.be.equal(204);
            expect(gameObject.setup.factions.length).to.be.equal(factionLength - 1);
        });

        it('Will not allow deletes on setups with a game associated to it', async() => {
            setup = await quasiSetup.create({
                setupHiddenCount: 3,
            });
            const gameObject = await quasiGame.create({
                hostID: setup.ownerID,
                isStarted: true,
                playerCount: 3,
                setupID: setup.id,
            });
            const factionID = gameObject.setup.factions[0].id;

            const responseObj = await deleteFaction({ factionID });

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        it('Will not allow deletes on setups that are featured', async() => {
            const gameObject = await quasiGame.create({
                setupID: setup.id,
                hostID: setup.ownerID,
                started: false,
            });
            const factionID = gameObject.setup.factions[0].id;
            await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

            const responseObj = await deleteFaction({ factionID });

            testControllerHelpers.assertUneditableSetupErrors(responseObj);
        });

        // eslint-disable-next-line max-len
        it('Will gracefully fail faction delete attempts if the faction id is not a number', async() => {
        // No given

            const responseObj = await deleteFaction({ factionID: 'ABCD' });

            expect(responseObj.statusCode).to.be.equal(404);
        });

        function deleteFaction(input = {}){
            return requests.delete(`api/factions/${input.factionID}`, defaultHeaders);
        }
    });
});

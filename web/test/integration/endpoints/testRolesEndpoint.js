require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const roleSchema = require('../../schemas/roleSchema');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_KEY, FEATURED_PRIORITY, ROLE_NAME } = require('../../fakeConstants');


describe('Roles endpoint', async() => {
    let user;
    let headers;
    let setup;
    let role;
    beforeEach(async() => {
        user = await quasiUser.createUser();
        headers = authHelpers.fakeTempAuthHeader(user.id);
        setup = await quasiSetup.create({ ownerID: user.id });
        await quasiGame.create({ setupID: setup.id, hostID: user.id });
        [role] = setup.roles;
    });

    it('Will create a role', async() => {
        const roleLength = setup.roles.length;

        const responseObj = await createRole();
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        roleSchema.check(responseObj.json.response);
        expect(setup.roles.length).to.be.equal(roleLength + 1);
    });

    it('Will not add roles if the setup id does not match the game setup id', async() => {
        const setup2 = await quasiSetup.create({ ownerID: user.id });

        const responseObj = await createRole({ setupID: setup2.id });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not add roles to featured setups', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await createRole();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    it('Will not allow create requests with no role name', async() => {
        // given created in beforeEach

        const responseObj = await createRole({ name: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow create requests with duplicate role names', async() => {
        const roleName = setup.roles[0].name;

        const responseObj = await createRole({ name: roleName });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow create requests with no abilities name', async() => {
        // given created in beforeEach

        const responseObj = await createRole({ abilities: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow create requests with no setup id', async() => {
        // given created in beforeEach

        const responseObj = await createRole({ setupID: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow create requests with abilities that aren\'t strings', async() => {
        // given created in beforeEach

        const responseObj = await createRole({ abilities: [{}] });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will allow create requests with empty abilities', async() => {
        // given created in beforeEach

        const responseObj = await createRole({ abilities: [] });

        expect(responseObj.statusCode).to.be.equal(200);
    });

    it('Will not allow create requests with an invalid ability', async() => {
        // given created in beforeEach

        const responseObj = await createRole({ abilities: ['FakeAbilityy'] });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will create roles with more than one ability', async() => {
        // given created in beforeEach

        const responseObj = await createRole({ abilities: ['Sheriff', 'Doctor'] });

        expectStatusCode(200, responseObj);
    });

    it('Will output the database abilityIDs created', async() => {
        // given created in beforeEach

        const responseObj = await createRole();
        const responseRole = responseObj.json.response;
        const updatedSetup = await setupService.getByID(setup.id);

        expectStatusCode(200, responseObj);
        const updatedRole = updatedSetup.roles.find(r => r.id === responseRole.id);
        expect(updatedRole.abilities[0].id).to.equal(responseRole.abilities[0].id);
    });

    it('Will give specific role information', async() => {
        // given setup in beforeEach

        const responseObj = await requests.get(`api/roles/${role.id}`, headers);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.name).to.be.equal(role.name);
    });

    it('Will gracefully fail deletes when the roleID doesn\'t exist', async() => {
        const roleID = role.id + setup.roles.length + 1;

        const responseObj = await requests.get(`api/roles/${roleID}`, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the roleID is not a number', async() => {
        const roleID = 'ABCD';

        const responseObj = await requests.get(`api/roles/${roleID}`, headers);

        expect(responseObj.statusCode).to.be.equal(404);
    });

    it('Will delete a role', async() => {
        // given setup in beforeEach

        const responseObj = await requests.delete(`api/roles/${role.id}`, headers);
        setup = await setupService.getByID(setup.id);
        const deletedRole = setup.roles.find(r => r.id === role.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(deletedRole).to.be.a('undefined');
    });

    it('Will not delete a role that is part of an uneditable setup', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await requests.delete(`api/roles/${role.id}`, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    function createRole(input = {}){
        const inputKeys = Object.keys(input);
        const args = {
            name: inputKeys.includes('name') ? input.name : ROLE_NAME,
            setupID: inputKeys.includes('setupID') ? input.setupID : setup.id,
            abilities: inputKeys.includes('abilities') ? input.abilities : ['Doctor'],
        };
        return requests.post('api/roles', args, headers);
    }
});

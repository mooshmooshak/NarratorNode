require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_KEY, FEATURED_PRIORITY } = require('../../fakeConstants');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Faction Role Ability Modifier Endpoint', () => {
    let gameID;
    let headers;
    let modifierBool;
    let modifierInt;
    let setup;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        gameID = gameObject.id;
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        modifierBool = getFactionRoleAbilityModifierArguments(gameObject, 'boolean');
        modifierBool.newValue = !modifierBool.oldValue;
        modifierInt = getFactionRoleAbilityModifierArguments(gameObject, 'number');
        modifierInt.newValue = modifierInt.oldValue + 2; // need to get past 0 for charges
    });

    it('Will update the faction role ability\'s modifier (boolean)', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierBool.name,
            modifierValue: modifierBool.newValue,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(gameObject, modifierBool.factionRole.id,
            modifierBool.abilityID, modifierBool.name);
        expect(newModifier.value).to.be.equal(modifierBool.newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will insert more than one bool ability modifier if player counts differ', async() => {
        const args = {
            modifierName: modifierBool.name,
            modifierValue: modifierBool.newValue,
        };

        await upsertRequest(args);
        const responseObj = await upsertRequest({
            ...args,
            minPlayerCount: 1,
        });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const factionRole = setup.factions.find(f => f.id === modifierBool.factionRole.factionID)
            .factionRoles.find(fr => fr.id === modifierBool.factionRole.id);
        const ability = factionRole.abilities.find(a => a.id === modifierBool.abilityID);
        const modifiers = ability.modifiers.filter(modifier => modifier.name === args.modifierName);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will update the faction role ability\'s modifier (integer)', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierInt.name,
            modifierValue: modifierInt.newValue,
            abilityID: modifierInt.abilityID,
            factionRoleID: modifierInt.factionRole.id,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(gameObject, modifierInt.factionRole.id,
            modifierInt.abilityID, modifierInt.name);
        expect(newModifier.value).to.be.equal(modifierInt.newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will insert more than one int ability modifier if player counts differ', async() => {
        const args = {
            modifierName: modifierInt.name,
            modifierValue: modifierInt.newValue,
            abilityID: modifierInt.abilityID,
            factionRoleID: modifierInt.factionRole.id,
        };

        await upsertRequest(args);
        const responseObj = await upsertRequest({
            ...args,
            minPlayerCount: 1,
        });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const factionRole = setup.factions.find(f => f.id === modifierBool.factionRole.factionID)
            .factionRoles.find(fr => fr.id === modifierBool.factionRole.id);
        const ability = factionRole.abilities.find(a => a.id === modifierBool.abilityID);
        const modifiers = ability.modifiers.filter(modifier => modifier.name === args.modifierName);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will not allow updates if the user doesn\'t own the setup', async() => {
        const user = await quasiUser.createUser();

        const responseObj = await upsertRequest({
            headers: authHelpers.fakeTempAuthHeader(user.id),
            modifierName: modifierBool.name,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(422);
        const newModifier = getNewModifier(gameObject, modifierBool.factionRole.id,
            modifierBool.abilityID, modifierBool.name);
        expect(newModifier.value).to.be.equal(modifierBool.oldValue);
    });

    it('Will gracefully fail when the factionRoleID doesn\'t exist', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            factionRoleID: modifierBool.factionRole.id * 10,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the abilityID doesn\'t exist', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({ abilityID: modifierBool.abilityID * 10 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierBool.name + modifierBool.name,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierValue: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing minPlayerCount', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            minPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing maxPlayerCount', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            maxPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierInt.name,
            modifierValue: modifierBool.newValue,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierBool.name,
            modifierValue: modifierInt.newValue,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow edits to featured setups', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await upsertRequest();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function upsertRequest(args = {}){
        const inputFactionRoleID = 'factionRoleID' in args
            ? args.factionRoleID
            : modifierBool.factionRole.id;
        const inputAbilityID = 'abilityID' in args ? args.abilityID : modifierBool.abilityID;
        const payload = {
            name: 'modifierName' in args ? args.modifierName : modifierBool.name,
            value: 'modifierValue' in args ? args.modifierValue : modifierBool.newValue,
            minPlayerCount: 'minPlayerCount' in args ? args.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in args ? args.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        return requests.post(
            `api/factionRoles/${inputFactionRoleID}/abilities/${inputAbilityID}/modifiers`,
            payload, args.headers || headers,
        );
    }
});

function getFactionRoleAbilityModifierArguments(gameObject, typeString){
    let returnValue;
    gameObject.setup.factions.forEach(faction => {
        faction.factionRoles.forEach(factionRole => {
            factionRole.abilities.forEach(ability => {
                ability.modifiers.forEach(modifier => {
                    if(typeof(modifier.value) === typeString && !returnValue) // eslint-disable-line valid-typeof
                        returnValue = {
                            factionRole,
                            abilityID: ability.id,
                            name: modifier.name,
                            oldValue: modifier.value,
                        };
                });
            });
        });
    });
    return returnValue;
}

function getNewModifier(gameObject, factionRoleID, abilityID, modifierName){
    let returnValue;
    gameObject.setup.factions.forEach(faction => {
        faction.factionRoles.forEach(factionRole => {
            if(factionRoleID !== factionRole.id)
                return;
            factionRole.abilities.forEach(roleAbility => {
                if(abilityID !== roleAbility.id)
                    return;
                roleAbility.modifiers.forEach(modifier => {
                    if(modifier.name === modifierName)
                        returnValue = modifier;
                });
            });
        });
    });
    return returnValue;
}

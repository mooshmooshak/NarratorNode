require('../../init');

const { expect } = require('chai');

const { GAME_ID } = require('../../fakeConstants');

const requests = require('../../requests');


describe('Channel - Browser Endpoint', () => {
    it('Will fetch active user ids for a bogus game id', async() => {
        const serverResponse = await requests.get(
            `api/channels/browser/activeUserIDs?game_id=${GAME_ID}`,
        );

        expect(serverResponse.statusCode).to.be.equal(200);
        expect(serverResponse.json.response).to.be.a('array');
    });
});

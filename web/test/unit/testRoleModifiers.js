require('../init');
const { expect } = require('chai');

const gameService = require('../../services/gameService');
const roleService = require('../../services/setup/roleService');
const setupService = require('../../services/setup/setupService');

const roleModifierRepo = require('../../repos/roleModifierRepo');

const quasiSetup = require('../quasiModels/quasiSetup');
const quasiGame = require('../quasiModels/quasiGame');

const { MAX_PLAYER_COUNT } = require('../../utils/constants');


describe('Role Modifiers Repo', () => {
    let setup;
    let game;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        game = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
    });

    it('Should delete role modifiers when the role is deleted', async() => {
        const [role] = setup.roles;
        await Promise.all([
            { name: 'AUTO_VEST', value: 50 },
            { name: 'UNCONVERTABLE', value: true }]
            .map(modifier => roleService.createModifier(setup.ownerID, role.id,
                { ...modifier, minPlayerCount: 0, maxPlayerCount: MAX_PLAYER_COUNT })));


        await gameService.deleteGame(game.id);
        await setupService.deleteSetup(setup.id, setup.ownerID);
        const modifiers = await roleModifierRepo.getBySetupID(setup.id);

        expect(modifiers.length).to.be.equal(0);
    });
});

const { expect } = require('chai');

const setHelpers = require('../../utils/setHelpers');


describe('Set Helpers', () => {
    it('Should calculate intersections', () => {
        const set1 = new Set([1, 2]);
        const set2 = new Set([2, 3]);

        const newSet = setHelpers.intersection(set1, set2);

        expect(newSet.size).to.be.equal(1);
        expect(newSet.has(1)).to.be.equal(false);
        expect(newSet.has(2)).to.be.equal(true);
        expect(newSet.has(3)).to.be.equal(false);
    });
});

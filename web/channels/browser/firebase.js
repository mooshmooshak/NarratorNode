const firebase = require('firebase-admin');

const firebaseFile = require('../../../firebase.json');


firebase.initializeApp({
    credential: firebase.credential.cert(firebaseFile),
    databaseURL: 'https://narrator-119be.firebaseio.com',
});

async function getUserInfoByAuthToken(authToken){
    const decodedClaims = await firebase.auth().verifyIdToken(authToken);
    return {
        externalID: decodedClaims.user_id,
        name: 'firebaseUser',
    };
}

module.exports = {
    getUserInfoByAuthToken,
};

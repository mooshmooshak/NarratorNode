function logicToRequest({ setupHiddenID }){
    return {
        event: 'setupHiddenRemove',
        setupHiddenID,
    };
}

module.exports = {
    logicToRequest,
};

function logicToRequest({ endTime }){
    return {
        endTime,
        event: 'timerUpdate',
    };
}

module.exports = {
    logicToRequest,
};

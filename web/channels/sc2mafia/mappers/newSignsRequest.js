function map(newPosts){
    return newPosts
        .filter(newPost => newPost.message === '/sign')
        .map(newPost => ({
            externalID: newPost.externalUserID,
            joinID: newPost.joinID,
            name: newPost.playerName,
        }));
}

module.exports = {
    map,
};

function map(pms, lastCheckTime){
    pms = pms.filter(pm => pm.message === '/host' && lastCheckTime <= pm.createdAt);
    const seenGameRequests = new Set();
    return pms.filter(pm => {
        if(seenGameRequests.has(pm.externalUserID))
            return false;
        seenGameRequests.add(pm.externalUserID);
        return true;
    }).map(pm => {
        pm.gameModifiers = {
            DAY_LENGTH_START: 60 * 60 * 24 * 2,
            NIGHT_LENGTH: 60 * 60 * 24,
        };
        return pm;
    });
}

module.exports = {
    map,
};

function logicToRequest(threadID){
    return {
        threadid: threadID,
        message: 'This game has been abandoned.',
        signature: true,
    };
}

module.exports = {
    logicToRequest,
};

const db = require('../../../db_communicator');


function createPublicChannel({ gameID, channelID }){
    const queryText = ''
        + 'INSERT INTO discord_public_channels '
        + '(game_id, channel_id) '
        + 'VALUES (?, ?);';
    return db.query(queryText, [gameID, channelID]);
}

async function getChannelIDs(gameID){
    const query = ''
        + 'SELECT channel_id FROM discord_public_channels '
        + 'WHERE game_id = ?;';
    const results = await db.query(query, [gameID]);
    const channelIDs = results.map(r => r.channel_id);
    return new Set(channelIDs);
}

async function getGameID(channelID){
    const queryText = ''
        + 'SELECT game_id FROM discord_public_channels '
        + 'WHERE channel_id = ?;';
    const results = await db.query(queryText, [channelID]);
    if(!results.length)
        return null;
    return parseInt(results[0], 10);
}

async function getGameMetadataByChannelID(channelID){
    const queryText = ''
        + 'SELECT replays.id, setup_id, is_started, instance_id FROM replays '
        + 'LEFT JOIN discord_public_channels '
        + 'ON discord_public_channels.game_id = replays.id '
        + 'WHERE channel_id = ?;';
    const results = await db.query(queryText, [channelID]);
    if(!results.length)
        return null;
    const [result] = results;
    return {
        gameID: parseInt(result.id, 10),
        isStarted: !!result.is_started,
        lobbyID: result.instance_id,
        setupID: parseInt(result.setup_id, 10),
    };
}

function deleteByChannelID(channelID){
    const queryText = ''
        + 'DELETE FROM discord_public_channels '
        + 'WHERE channel_id = ?;';
    return db.query(queryText, [channelID]);
}

function deleteAll(){
    return db.query('DELETE FROM discord_public_channels;');
}

module.exports = {
    createPublicChannel,
    getChannelIDs,
    getGameID,
    getGameMetadataByChannelID,
    deleteByChannelID,

    // testOnly
    deleteAll,
};

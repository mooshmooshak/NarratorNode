const db = require('../../../db_communicator');


async function getByGameID(gameID){
    const queryText = ''
        + 'SELECT channel_id, message_id '
        + 'FROM discord_subscription_messages '
        + 'WHERE game_id = ?;';
    const results = await db.query(queryText, [gameID]);
    return results.map(r => ({
        channelID: r.channel_id,
        messageID: r.message_id,
    }));
}

function saveSubscribeMessage({ channelID, gameID, messageID }){
    const queryText = ''
        + 'INSERT INTO discord_subscription_messages '
        + '(game_id, channel_id, message_id) '
        + 'VALUES (?, ?, ?);';
    return db.query(queryText, [gameID, channelID, messageID]);
}

function deleteByGameID(gameID){
    const queryText = ''
        + 'DELETE FROM discord_subscription_messages '
        + 'WHERE game_id = ?;';
    return db.query(queryText, [gameID]);
}


module.exports = {
    getByGameID,
    saveSubscribeMessage,
    deleteByGameID,
};

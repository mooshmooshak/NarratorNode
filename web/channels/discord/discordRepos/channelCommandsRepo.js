const db = require('../../../db_communicator');


async function get(channelID){
    const query = 'SELECT prefix FROM discord_channel_commands '
        + 'WHERE channel_id = ?;';
    const results = await db.query(query, [channelID]);
    if(results.length)
        return results[0].prefix;
    return null;
}

function save(channelID, prefix){
    const query = 'INSERT INTO discord_channel_commands '
        + '(channel_id, prefix) VALUES (?, ?) ON DUPLICATE KEY UPDATE prefix = ?;';
    return db.query(query, [channelID, prefix, prefix]);
}

function deleteAll(){
    const query = 'DELETE FROM discord_channel_commands;';
    return db.query(query);
}

module.exports = {
    get,
    save,
    deleteAll,
};

module.exports = {
    echo: 'echo',
    help: 'help',
    commands: 'commands',
    status: 'status',
    info: 'info',
    invite: 'invite',
    link: 'link',
    changecommand: 'changeCommand',

    host: 'host',
    in: 'in',
    play: 'play',
    join: 'join',
    addbots: 'addBots',

    out: 'out',
    exit: 'exit',
    quit: 'quit',
    leave: 'leave',

    subscribe: 'subscribe',
    unsubscribe: 'unsubscribe',

    autotag: 'autotag',

    name: 'name',
    prefer: 'prefer',
    clearprefers: 'clearPrefers',
    setups: 'setups',
    setup: 'setup',
    moderate: 'moderate',

    autoparity: 'autoParity',
    chatrate: 'chatRate',
    nightlength: 'nightLength',
    daylength: 'dayLength',
    discussionlength: 'discussionLength',
    triallength: 'trialLength',
    nightstart: 'nightStart',
    votesystems: 'voteSystems',
    votesystem: 'voteSystem',

    day: 'day',
    night: 'night',
    daystart: 'dayStart',
    punch: 'punch',
    ita: 'ita',

    start: 'start',

    lw: 'lw',
    votes: 'votecount',
    votecount: 'voteCount',
    vc: 'vc',
    living: 'living',
    alive: 'alive',
    rolecard: 'roleCard',

    stats: 'stats',
    history: 'history',
    leaderboard: 'leaderboard',

    // modcommands
    edittimer: 'editTimer',
    playeractions: 'playerActions',
    playerroles: 'playerRoles',

    skip: 'skip',
    end: 'end',
    usage: 'usage',
};

const { uniq } = require('lodash');
const config = require('../../config');

const gamePhase = require('../../models/enums/gamePhase');
const integrationTypes = require('../../models/enums/integrationTypes');

const channelService = require('./discordServices/channelService');
const channelCommandService = require('./discordServices/channelCommandService');
const gameService = require('./discordServices/gameService');
const messageService = require('./discordServices/messageService');
const userService = require('./discordServices/userService');

const chatService = require('../../services/chatService');
const hanselGameService = require('../../services/gameService');
const timerService = require('../../services/timerService');
const voteService = require('../../services/voteService');

const timeTextUtil = require('./utils/timeTextUtil');

const helpers = require('../../utils/helpers');

const discordJsonClient = require('./discordJsonClient');
const mappers = require('./mappers/mappers');

const greetings = [
    'I\'m back online!',
    'Who missed me?',
    'I was here all along.',
    'Self-awareness attained.',
    'Is it me that was offline, or were you just online?',
    'Ready to host, never to play.',
];

function DiscordWrapper(){
    this.autoTagInfo = {};
}

DiscordWrapper.prototype.connect = async function(){
    await discordJsonClient.connect();

    const rText = greetings[Math.floor(Math.random() * greetings.length)];
    const configJson = require('../../../config');
    return Promise.all([
        discordJsonClient.sendPublicMessage(configJson.discord.reboot_channel_id, rText),
        channelService.deleteAllNightChats(),
    ]);
};

/*
 *  Server methods
 */

async function cleanupPlayer(userID){
    const discordID = await userService.getDiscordID(userID);
    if(!discordID)
        return;

    return userService.cleanupOnGameEnd(discordID);
}

DiscordWrapper.prototype.createIntegration = function(gameID, { channelID }){
    return gameService.createGameChannel({ gameID, channelID });
};

DiscordWrapper.prototype.hasIntegration = async function(gameID){
    const channelIDs = await gameService.getChannelIDs(gameID);
    return !!channelIDs.size;
};

DiscordWrapper.prototype.onPhaseEndBid = function(){};

DiscordWrapper.prototype.onPlayerRemove = function(){};

DiscordWrapper.prototype.onRoleCardUpdate = async function(request){
    const dUserID = await userService.getDiscordID(request.userID);
    if(!dUserID)
        return;

    let output = '**';
    if(request.isDay)
        output += 'Day ';
    else
        output += 'Night ';
    output += `${request.dayNumber}**\n`;
    output += mappers.roleCard(request.profile);
    output += '\nGood luck!';

    await discordJsonClient.sendDirectMessage(dUserID, output);
    timerService.setGameExpiration(request.joinID);
};

DiscordWrapper.prototype.cleanupChannel = function(channelID){
    this.stopAutoTag(channelID);
    return channelService.deletePublicGameChannel(channelID);
};

DiscordWrapper.prototype.stopAutoTag = function(channelID){
    const autoTag = this.autoTagInfo[channelID];
    if(!autoTag)
        return;
    autoTag.stop();
    delete this.autoTagInfo[channelID];
};

async function nightChangingText(nightEnd, message, ogMessage, first, channelID, gameID){
    const remTime = nightEnd - new Date().getTime();

    if(remTime > 0){
        let toWait = 5000;
        if(first)
            toWait -= 1000;
        const game = await hanselGameService.getByID(gameID);
        if(game && game.phase.name === gamePhase.NIGHTTIME)
            setTimeout(() => {
                nightChangingText(nightEnd, message, ogMessage, null, channelID, gameID);
            }, toWait);

        let seconds = Math.floor(remTime / 1000);
        let minutes = Math.floor(seconds / 60);
        const hour = Math.floor(minutes / 60);

        let text = '';
        if(hour > 0){
            text += `${hour}:`;
            minutes %= 60;
            if(minutes < 10)
                minutes = `0${minutes}`;
        }else{
            text += '00:';
        }
        if(minutes < 10)
            text += '0';
        text += `${minutes}:`;
        seconds = (seconds % 60).toString();
        if(seconds < 10)
            seconds = `0${seconds}`;

        text += seconds;
        if(!game || game.phase.name !== gamePhase.NIGHTTIME)
            text = '00:00:00';

        message.edit(`${ogMessage}**${text}**`).catch(helpers.log);
    }
}

async function tryDayMessage(m){
    const text = m.args;
    const [userID, metaData] = await Promise.all([
        userService.getUserID(m.discordID),
        gameService.getGameMetadataByChannelID(m.channelID),
    ]);
    if(!metaData || !userID)
        return;

    const args = { channelID: m.channelID };
    if(!metaData.isStarted)
        return chatService.sendLobbyMessage(metaData.gameID, userID, { text },
            integrationTypes.DISCORD, args);

    try{
        await chatService.say(userID, 'everyone', text, integrationTypes.DISCORD, args);
        return flickerChatPermissions(m, metaData.gameID);
    }catch(err){}
    const users = await userService.getByGameID(metaData.gameID);
    const user = users.find(u => u.userID === userID);
    if(user && user.isModerator)
        return;
    await messageService.deleteMessage(m.raw);
    let message;
    if(user)
        message = 'You shouldn\'t be talking in that channel right now!';
    else
        message = 'You shouldn\'t be talking in that channel.  There\'s a game going on!';
    return discordJsonClient.sendDirectMessage(m.discordID, message);
}

async function flickerChatPermissions({ discordID, channelID }, gameID){
    const messageRate = await gameService.getChatFrequency(gameID);
    if(messageRate === 0)
        return;
    await discordJsonClient.removeChatPermission(discordID, [channelID])
        .catch(helpers.log);
    setTimeout(() => {
        discordJsonClient.addChatPermission(discordID, new Set([channelID]))
            .catch(helpers.log);
    }, 3000);
}

async function getMention(userID, name){
    const discordID = await userService.getDiscordID(userID);
    if(!discordID)
        return name;

    return `<@!${discordID}>`;
}

// approved prototypes

DiscordWrapper.prototype.onActionSubmit = async function(request){
    const discordID = await userService.getDiscordID(request.userID);
    if(!discordID)
        return;
    const message = mappers.onActionSubmitMessage(request, discordID);
    return discordJsonClient.sendMessage(message);
};

// this'll be a future refactor.  let the wrappers dictate what's sent to the clients
DiscordWrapper.prototype.onBroadcast = async function(request){
    const channelIDs = await gameService.getChannelIDs(request.gameID);
    const message = mappers.onBroadcastMessage(request, channelIDs);
    return discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onDayDeath = async function(request){
    const channelIDs = await gameService.getChannelIDs(request.gameID);
    const message = mappers.onDayDeathMessage(request, channelIDs);

    const removals = request.userIDs.map(async userID => {
        const discordID = await userService.getDiscordID(userID);
        return discordJsonClient.removeChatPermission(discordID, channelIDs);
    });

    await Promise.all(removals);
    return discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onDayStart = async function(dayStartEvent){
    dayStartEvent = JSON.parse(JSON.stringify(dayStartEvent));
    const channelIDs = await gameService.getChannelIDs(dayStartEvent.gameID);
    let output = `**Day ${dayStartEvent.dayNumber}**\n`;

    if(dayStartEvent.graveYard){
        let deadPerson;
        for(let i = 0; i < dayStartEvent.graveYard.length; i++){
            deadPerson = dayStartEvent.graveYard[i];

            output += `${await getMention(deadPerson.userID, deadPerson.playerName)} `;

            if(deadPerson.deathType){
                output += 'was ';
                for(let j = 0; j < deadPerson.deathType.length; j++){
                    output += deadPerson.deathType[j].toLowerCase();
                    if(j !== deadPerson.deathType.length - 1)
                        output += ', ';
                }
                output += '. \n';
                output += `They were a **${deadPerson.teamName} ${deadPerson.roleName}**.`;
                if(deadPerson.lastWill)
                    output += `They left a last will:\n*${deadPerson.lastWill}*`;
                output += '\n';
            }else{
                output += 'died!\nWe could not determine what they were.\n';
            }
        }
        this.onPhaseDeath(channelIDs, dayStartEvent.graveYard);
    }else if(!dayStartEvent.firstPhase){
        output += 'No one died!\n';
    }

    if(dayStartEvent.snitchReveals)
        await Promise.all(dayStartEvent.snitchReveals.map(async reveal => {
            // eslint-disable-next-line prefer-template
            const snitchRevealText = ''
                + 'A reliable source has revealed '
                + await getMention(reveal.userID, reveal.name)
                + `to be a **${reveal.teamName} ${reveal.roleName}**.\n`;
            output += snitchRevealText;
        }));


    if(dayStartEvent.discussionLength){
        const minutesLabel = timeTextUtil.getMinutesLabel(dayStartEvent.discussionLength);
        output += `Voting polls will open in ${minutesLabel}.`;
    }else{
        const minutesLabel = timeTextUtil.getMinutesLabel(dayStartEvent.dayLength);
        output += `Voting polls will close in ${minutesLabel}.`;
    }

    const participatingUserIDs = dayStartEvent.players
        .filter(p => p.userID && p.isParticipating)
        .map(p => p.userID);
    dayStartEvent.users.filter(u => u.isModerator).forEach(u => participatingUserIDs.push(u.id));
    await channelService.refreshGameNightChatChannelsPermissions(dayStartEvent);

    uniq(participatingUserIDs).forEach(async userID => {
        const discordID = await userService.getDiscordID(userID);
        if(userID)
            output += `<@!${discordID}>`;
    });

    const daySpeakersUserIDs = getDaySpeakingUserIDs(dayStartEvent);

    // setting channel participants first is for a race condition in the test
    // testSetupOverview.shouldNotShowPlayerRolesToTheModeratorOnPublicInfoRequest
    // info command gets triggered before host is allowed to talk
    await channelService.setChannelParticipants(
        dayStartEvent.gameID, channelIDs, daySpeakersUserIDs,
    );

    discordJsonClient.sendPublicMessage(channelIDs, output);

    return fetchAndSendFeedback(dayStartEvent);
};

function getDaySpeakingUserIDs({ players, users, profiles }){
    const playerIDs = new Set(players.map(({ userID }) => userID));
    const nonParticipatingModerators = users
        .filter(({ id }) => !playerIDs.has(id))
        .map(({ id }) => id);
    const dayPlayerSpeakers = profiles
        .filter(({ flip, isPuppeted, isSilenced }) => !isPuppeted && !isSilenced && !flip)
        .map(({ userID }) => userID);
    return new Set([
        ...nonParticipatingModerators,
        ...dayPlayerSpeakers,
    ]);
}

function fetchAndSendFeedback({ gameID, players, dayNumber }){
    return Promise.all(players
        .filter(p => !p.flip && p.userID)
        .map(async p => {
            const chats = await chatService.getUserChat(gameID, p.userID);
            const text = (chats.message || [])
                .filter(m => m.messageType === 'Feedback' && m.day === dayNumber)
                .map(m => m.simpleText)
                .join('\n');
            const discordID = await userService.getDiscordID(p.userID);
            if(discordID)
                return discordJsonClient.sendDirectMessage(discordID, text);
        }));
}

DiscordWrapper.prototype.onGameChatMessage = async function(request){
    if(!request.chatName.startsWith('Day '))
        return;
    const channelIDs = await gameService.getChannelIDs(request.gameID);
    const filteredChannelIDs = [...channelIDs]
        .filter(channelID => request.args.channelID !== channelID);
    return discordJsonClient.sendWebhook(filteredChannelIDs, request.speakerName, request.text);
};

DiscordWrapper.prototype.onGameEnd = async function({ gameID }){
    const channelIDs = await gameService.getChannelIDs(gameID);
    if(channelIDs.size)
        return hanselGameService.deleteGame(gameID);
};

DiscordWrapper.prototype.onGameStart = async function(jo){
    const channelIDs = await gameService.getChannelIDs(jo.gameID);

    channelIDs.forEach(channelID => this.stopAutoTag(channelID));

    const moderatorUserIDs = jo.users
        .filter(user => user.isModerator)
        .map(user => user.id);
    const moderatorDiscordIDs = Object.values(
        await userService.getUserIDToDiscordIDMap(moderatorUserIDs),
    ).filter(x => x); // removes nulls

    const message = mappers.onGameStartMessage(moderatorDiscordIDs, channelIDs);
    discordJsonClient.sendMessage(message);

    await discordJsonClient.removeEveryoneChatPermission(channelIDs);

    if(jo.dayStart){
        const ps = [...getDaySpeakingUserIDs(jo)]
            .map(async userID => {
                const discordID = await userService.getDiscordID(userID);
                if(discordID)
                    return discordJsonClient.addChatPermission(discordID, channelIDs);
            });
        await Promise.all(ps);
    }
    return messageService.deleteSubscriptionMessages(jo.gameID);
};

DiscordWrapper.prototype.onLobbyChatMessage = async function({
    args, gameID, text, userID,
}){
    const [speakerName, channelIDs] = await Promise.all([
        userService.getNameByID(userID),
        gameService.getChannelIDs(gameID),
    ]);
    const filteredChannelIDs = [...channelIDs].filter(channelID => channelID !== args.channelID);
    return discordJsonClient.sendWebhook(filteredChannelIDs, speakerName, text);
};

DiscordWrapper.prototype.onLobbyDelete = async function(request){
    const channelIDs = await gameService.getChannelIDs(request.gameID);
    await messageService.deleteSubscriptionMessages(request.gameID);

    const users = await userService.getByGameID(request.gameID);
    await Promise.all(users.map(({ userID }) => cleanupPlayer(userID)));

    await Promise.all([...channelIDs].map(
        channelID => this.cleanupChannel(channelID),
    ));

    const message = mappers.onLobbyCloseMessage(channelIDs);
    const promises = Promise.all([
        discordJsonClient.sendMessage(message),
        channelService.deleteGameNightChats(request.gameID),
    ]);
    if(config.isWaitingForEvents())
        await promises;
};

DiscordWrapper.prototype.onNewPlayer = async function(request){
    const channelIDs = await gameService.getChannelIDs(request.gameID);
    const message = mappers.onPlayerJoinMessage(request, channelIDs);
    return discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onNewVote = async function(request){
    const userIDs = request.users.map(user => user.id);
    const [userIDToDiscordIDMap, channelIDs] = await Promise.all([
        userService.getUserIDToDiscordIDMap(userIDs),
        gameService.getChannelIDs(request.gameID),
    ]);
    const message = mappers.onNewVoteMessage(request, channelIDs, userIDToDiscordIDMap);
    return discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onNightStart = async function(request){
    const playerUserIDs = new Set(request.players.map(({ userID }) => userID));
    const canSpeak = request.users
        .reduce((acc, user) => (playerUserIDs.has(user.id) || !user.isModerator
            ? acc
            : [...acc, user.id]),
        []);
    const channelIDs = await gameService.getChannelIDs(request.gameID);
    channelService.setChannelParticipants(request.gameID, channelIDs, new Set(canSpeak));

    channelService.refreshGameNightChatChannelsPermissions(request);
    request.players.filter(p => p.isParticipant && p.userID).forEach(async({ userID }) => {
        const discordID = await userService.getDiscordID(userID);
        discordJsonClient.sendDirectMessage(discordID,
            'Submit your night action!  '
            + 'To see your available night actions use '
            + '**commands**.');
    });

    const votes = await voteService.getVotes(request.gameID);
    const game = await hanselGameService.getByID(request.gameID);
    let message = mappers.onDayEndMessage(votes, game, channelIDs);
    discordJsonClient.sendMessage(message);

    request.graveYard.forEach(dead => {
        message = mappers.onDayEndDeathMessage(dead, channelIDs);
        discordJsonClient.sendMessage(message);
        if(!dead.lastWill)
            return;
        discordJsonClient.sendPublicMessage(channelIDs,
            `${dead.name} left a last will:\n*${dead.lastWill}*`);
    });

    const nightEnd = new Date().getTime() + (request.length * 1000);

    const nighttimeText = 'It is now nighttime.  Day will resume in ';
    const discordMessage = await discordJsonClient.sendPublicMessage(channelIDs, nighttimeText);
    channelIDs.forEach(channelID => nightChangingText(
        nightEnd, discordMessage.find(m => m.channel.id === channelID),
        nighttimeText, true, channelID, request.gameID,
    ));

    this.onPhaseDeath(channelIDs, request.graveYard);
};

DiscordWrapper.prototype.onPhaseDeath = function(channelIDs, graveYard){
    if(!graveYard)
        return;

    const graveyardPromises = graveYard.map(async grave => {
        const discordID = await userService.getDiscordID(grave.userID);
        if(discordID)
            return discordJsonClient.removeChatPermission(discordID, channelIDs);
    });

    return Promise.all(graveyardPromises).catch(helpers.log);
};

DiscordWrapper.prototype.onTimerUpdate = async function({ gameID, seconds }){
    const channelIDs = await gameService.getChannelIDs(gameID);
    const message = mappers.onTimerUpdate(seconds);
    return discordJsonClient.sendMessage({ channelIDs, message });
};

DiscordWrapper.prototype.onTrialStart = async function(request){
    const [game, channelIDs] = await Promise.all([
        hanselGameService.getByID(request.gameID),
        gameService.getChannelIDs(request.gameID),
    ]);
    const userIDs = request.trialedUsers.map(user => user.userID);
    const [userIDToDiscordIDMap, votes] = await Promise.all([
        userService.getUserIDToDiscordIDMap(userIDs),
        voteService.getVotes(request.gameID),
    ]);
    const message = mappers.onTrialStart(request, game, channelIDs, userIDToDiscordIDMap, votes);
    return discordJsonClient.sendMessage(message);
};

DiscordWrapper.prototype.onVotePhaseStart = async function(request){
    const [game, votes, channelIDs, exampleDiscordID] = await Promise.all([
        hanselGameService.getByID(request.gameID),
        voteService.getVotes(request.gameID),
        gameService.getChannelIDs(request.gameID),
        getExampleDiscordID(request),
    ]);
    const channelPrefixMap = await channelCommandService.getPrefixMap(channelIDs);
    const message = mappers.onVotePhaseStart(
        request, votes, game, exampleDiscordID, channelPrefixMap,
    );
    return discordJsonClient.sendMessage(message);
};

async function getExampleDiscordID(request){
    const users = [...request.users];
    users.sort((u1, u2) => u2.isModerator - u1.isModerator);

    for(let i = 0; i < users.length; i++){
        const discordID = await userService.getDiscordID(users[i].id);
        if(discordID)
            return discordID;
    }
    return null;
}

DiscordWrapper.prototype.onVotePhaseReset = async function(request){
    const channelIDs = await gameService.getChannelIDs(request.gameID);
    const game = await hanselGameService.getByID(request.gameID);
    const message = mappers.onVotePhaseReset(request, game, channelIDs);
    return discordJsonClient.sendMessage(message);
};

module.exports = {
    cleanupPlayer,

    tryDayMessage,

    DiscordWrapper,
};

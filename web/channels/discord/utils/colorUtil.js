const teamToColor = {
    '#6495ED': ':blue_car:',
    '#FF0000': ':red_circle:',
    '#FFA500': ':large_orange_diamond:',
    '#FFFF00': ':yellow_heart:',
    '#880BFC': ':purple_heart:',
    '#00FF00': ':green_apple:',
    '#DDA0DD': ':clown:',
    '#D5E68C': ':performing_arts:',
    '#888888': ':performing_arts:',
    '#CD853E': ':knife:',
};

function getSymbolByColor(color){
    return teamToColor[color] || ':grey_question:';
}

module.exports = {
    getSymbolByColor,
};

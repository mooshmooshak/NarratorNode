const stringSimilarity = require('string-similarity');

const config = require('../../../config');
const helpers = require('../../utils/helpers');

const historyRepo = require('../../repos/historyRepo');
const userIntegrationsRepo = require('../../repos/userIntegrationsRepo');

const hanselActionService = require('../../services/actionService');
const hanselGameService = require('../../services/gameService');
const moderatorService = require('../../services/moderatorService');
const phaseService = require('../../services/phaseService');
const playerService = require('../../services/playerService');
const preferService = require('../../services/preferService');
const profileService = require('../../services/profileService');
const setupModifierService = require('../../services/setup/setupModifierService');
const setupService = require('../../services/setup/setupService');
const hanselUserService = require('../../services/userService');
const voteService = require('../../services/voteService');

const gamePhase = require('../../models/enums/gamePhase');
const userTypes = require('../../models/enums/userTypes');

const { MAX_PLAYER_COUNT } = require('../../utils/constants');

const discordJsonClient = require('./discordJsonClient');
const discordClient = require('./discordWrapper');

const actionService = require('./discordServices/actionService');
const channelPrefixService = require('./discordServices/channelCommandService');
const gameService = require('./discordServices/gameService');
const messageService = require('./discordServices/messageService');
const permissionService = require('./discordServices/permissionService');
const userService = require('./discordServices/userService');

const subscribersRepo = require('./discordRepos/subscribersRepo');

const mappers = require('./mappers/mappers');

const userUtil = require('../../utils/userUtil');
const { userError } = require('./utils/utils');


function echo(m){
    if(m.isDM)
        return discordJsonClient.sendDirectMessage(m.discordID, m.args);
    return discordJsonClient.sendPublicMessage(m.channelID, m.args);
}

async function help(m){
    const prefixCharacter = m.channelCommand;
    const prefix = `- ** ${prefixCharacter}`;

    let output = '*This bot uses a web interface as well as discord to host the game. '
        + 'When you join, the bot will DM you a link - click the link after game start, '
        + 'and you can do night actions or chat there instead. '
        + 'You can also DM the bot your night actions in discord '
        + 'if you do not wish to use the web interface.*\n\n';

    output += 'To quickly start a game:\n';
    output += `${prefix}in** to create/join a game\n`;
    output += `${prefix}setup <setupName>** to pick a setup *ex: !setup mash*\n`;
    output += `${prefix}daylength 3** to set the length of the day *!nightlength for night*\n`;
    output += `${prefix}start** to start, duH\n\n`;

    output += 'In game\n';
    output += `${prefix}vote** to vote people\n`;
    output += `${prefix}commands** as a pm *to the bot*`
        + ' to get a list of all your available actions\n\n';

    output += 'Lastly\n';
    output += `${prefix}commands** to see more available commands\n`;
    output += `${prefix}subscribe** in a channel to get notified when a game's starting!`;

    return discordJsonClient.sendPublicMessage(m.originChannelID, output);
}

async function commands(m){
    if(m.args)
        m.args = m.args.toLowerCase();
    const gameID = m.gameMetadata ? m.gameMetadata.gameID : null;
    const playerCommands = await gameService.getGameAbilities(m, gameID);
    const isGameInChannel = !!gameID;
    const game = gameID && await hanselGameService.getByID(gameID);
    const userID = await userService.getUserID(m.discordID);
    const output = mappers.commands(m, playerCommands, isGameInChannel, game, userID);
    return discordJsonClient.sendPublicMessage(m.originChannelID, output);
}

async function info(m){
    if(!m.gameMetadata)
        throw userError('No active games!');

    const game = await hanselGameService.getByID(m.gameMetadata.gameID);
    const userID = m.isDM && await userService.getUserID(m.discordID);
    const overviewJson = mappers.setupOverview(game, userID);

    try{
        if(game.isStarted)
            await discordJsonClient.sendDirectMessage(m.discordID, overviewJson);
        else
            await discordJsonClient.sendPublicMessage(m.channelID, overviewJson);
    }catch(err){
        helpers.log(err);
    }
}

async function invite(m){
    const inviteURL = await discordJsonClient.getInviteURL(config.discord.home_channel_id);
    let output = '**Invite Instructions**\n'
        + 'You can find the link and _much needed setup instructions_ '
        + 'on how to invite me to your discord group here:\n';
    output += 'https://gitlab.com/the-narrator/NarratorNode/-/blob/master/README.md\n';
    output += 'If you find any bugs or have questions, feel free to visit the home channel:\n';
    output += inviteURL;

    return discordJsonClient.sendPublicMessage(m.channelID, output);
}

async function changeCommand(m){
    const command = m.args;
    if(!command)
        throw userError(`You need to specify a prefix to use this! "
            + "For example **${m.channelCommand}changeCommand !** "
            + "or **${m.channelCommand}changeCommand w.in**`);
    if(command.length > 2)
        throw userError('Command prefixes longer than 2 characters are currently unsupported');

    if(!m.channelID)
        throw userError("I'm unsure of which channel you want to change the command for");

    await channelPrefixService.savePrefix(m.channelID, command);
    return discordJsonClient.sendPublicMessage(m.channelID, `Command changed to '${command}'.`);
}

async function host(m){
    if(m.isDM){ // is private message
        const text = "I can't start games here.  Add me to a channel and '!host' there!";
        throw userError(text);
    }

    if(m.gameMetadata){
        const users = await userService.getByGameID(m.gameMetadata.gameID);
        const moderators = users.filter(({ isModerator }) => isModerator);
        if(moderators.find(moderator => moderator.discordID === m.discordID))
            throw userError('You are already hosting a game!');

        const discordModerator = moderators.find(({ discordID }) => discordID);
        if(!discordModerator)
            throw userError('There is already a game going on in this channel.');

        const hostName = await discordJsonClient.getName(discordModerator.discordID, m.channelID);
        throw userError(`${hostName} is already hosting a game.`);
    }

    const userID = await userService.getUserID(m.discordID);
    const tempAuthToken = hanselUserService.addTempAuth(userID);

    const nickname = await discordJsonClient.getName(m.discordID, m.channelID);
    const setupID = await setupService.getDefaultSetupID(userID);

    let game;
    try{
        game = await hanselGameService.create(userID, nickname, setupID);
    }catch(err){
        throw userError(err.errors[0]);
    }
    // triggers game expiration
    m.gameMetadata = {
        gameID: game.id,
        lobbyID: game.lobbyID,
        isStarted: false,
    };

    await Promise.all([
        hanselGameService.updateModifier(userID, { name: 'CHAT_ROLES', value: false }),
        hanselGameService.updateModifier(userID, { name: 'NIGHT_LENGTH', value: 60 }),
        gameService.createGameChannel({ gameID: game.id, channelID: m.channelID }),
    ]);

    discordJsonClient.sendPublicMessage(m.channelID, `A game is being started by ${nickname}.`);
    const message = mappers.newPlayerPmMessage(tempAuthToken, game);
    discordJsonClient.sendDirectMessage(m.discordID, message);

    permissionService.gameCreateChecks(m.channelID);

    const url = await discordJsonClient.getInviteURL(m.channelID);

    const subscribers = (await subscribersRepo.getByGuildID(m.guildID))
        .filter(subscriberUserID => subscriberUserID !== userID); // filters out the host

    const promises = subscribers.map(async subscriberUserID => {
        const discordID = await userIntegrationsRepo.getDiscordID(subscriberUserID);
        if(!discordID)
            return;
        const status = await discordJsonClient.getUserStatus(discordID);
        if(['dnd', 'offline'].includes(status))
            return;
        let output = `${nickname} has started a Narrator Mafia Game!`;
        if(url)
            output += `\n${url}`;
        const cleanupMessage = await discordJsonClient.sendDirectMessage(discordID, output);
        return messageService.saveSubscribeNotification({
            channelID: cleanupMessage.channel.id,
            gameID: game.id,
            messageID: cleanupMessage.id,
        });
    });
    return Promise.all(promises);
}

async function join(m){
    if(!m.gameMetadata)
        return host(m);

    if(m.isDM)
        throw userError(`You can't join games in here.  "
            + "Join a channel where a game is hosted, and then type '${m.channelCommand}join'.`);

    const userID = await userService.getUserID(m.discordID);
    const discordName = await discordJsonClient.getName(m.discordID, m.channelID);
    let gameDict;
    try{
        gameDict = await playerService.create(userID, discordName, m.gameMetadata.lobbyID);
    }catch(errorObj){
        let message = errorObj.errors[0];
        if(message === 'User is already in a game.')
            message = 'You\'re already in a game!';
        throw userError(message);
    }

    const tempAuthToken = hanselUserService.addTempAuth(userID);
    const message = mappers.newPlayerPmMessage(tempAuthToken, gameDict);
    return discordJsonClient.sendDirectMessage(m.discordID, message);
}

async function addBots(m){
    if(config.env === 'prod')
        throw userError('Not allowed');

    const userID = await userService.getUserID(m.discordID);
    const botCount = parseInt(m.args, 10);

    await playerService.addBots(userID, botCount);

    return discordJsonClient.sendMessage({ channelID: m.channelID, message: 'Bots added.' });
}

async function leave(m){
    const userID = await userService.getUserID(m.discordID);
    try{
        await playerService.leave(userID);
    }catch(httpError){
        throw userError(httpError.errors[0]);
    }

    await discordClient.cleanupPlayer(userID);

    const nickname = await discordJsonClient.getName(m.discordID, m.channelID);
    const message = mappers.onPlayerRemove(nickname, m.channelID);
    return discordJsonClient.sendMessage(message);
}

async function subscribe(m){
    if(m.isDM)
        throw userError("I'm unsure of what channel you would like to subscribe to!");

    const userID = await userService.getUserID(m.discordID);
    await subscribersRepo.insert(m.raw.channel.guild.id, userID);

    const message = 'I will let you know if any games get created in this channel! '
        + `If you'd like me to stop, use **${m.channelCommand}unsubscribe**.`;
    return discordJsonClient.sendPublicMessage(m.channelID, message);
}

async function unsubscribe(m){
    if(m.isDM)
        throw userError("I'm unsure of what channel you would like to unsubscribe from!");

    const userID = await userService.getUserID(m.discordID);
    await subscribersRepo.deleteEntry(m.raw.channel.guild.id, userID);

    return discordJsonClient.reply(m,
        'You are unsubscribed from games that pop up in this channel.');
}

// eslint-disable-next-line no-unused-vars
function autotag(m){
    throw userError('This feature is temporarily disabled.');
    // let suffix = m.args;
    // if(!suffix || suffix.toLowerCase() === 'help')
    //     return this.commands(m, 'autotag');

    // suffix = suffix.toLowerCase();

    // if(suffix === 'on'){
    //     if(this.channelStartedGame[m.channelID])
    //         return reply(m, "Can't do that right now.");
    //     if(this.isAutotagging(m))
    //         return reply(m, "I'm already autotagging for you.");

    //     var qString = `SELECT 1 FROM discord_last_tagged WHERE timeStamp > DATE_ADD(NOW(), `
    // `INTERVAL 2 HOUR) AND user_id = '${m.discordID}';`;

    //     return db.query(qString)
    //         .then(results => {
    //             if(results.length)
    //                 return reply(m, "You cannot use the autotag if you're
    // "on the _Do not Disturb_ list.");

    //             reply(m, "I'll begin tagging people you've played with before.");

    //             const channelAutoTag = this.autoTagInfo[channelID];
    //             if(!channelAutoTag)
    //                 this.autoTagInfo[channelID] = new AutoTag(this, channelID,
    // m.discordID, narrRequest);
    //             else
    //                 channelAutoTag.addSearcher(m.discordID);
    //         }).catch(helpers.log);
    // }if(suffix === 'off'){
    //     if(this.channelStartedGame[channelID])
    //         return reply(m, "Can't do that right now.");
    //     if(!this.isAutotagging(m))
    //         return reply(m, "I'm already not autotagging anyone for you.");

    //     reply(m, "I'll stop tagging people you've played with before.");
    //     this.autoTagInfo[channelID].removeSearcher(m.discordID);
    // }else if(suffix === 'dnd'){
    //     if(this.isAutotagging(m))
    //         return reply("You cannot use the autotag if you're on the _Do not Disturb_ list.");
    //     reply(m, "I'll put you on the do not disturb list.");

    // return db.query(`INSERT INTO discord_last_tagged (user_id, timestamp) `
    //     + `VALUES ('${m.discordID}', DATE_ADD(NOW(), INTERVAL 2 MONTH)) `
    //     + `ON DUPLICATE KEY UPDATE timestamp = DATE_ADD(NOW(), INTERVAL 2 MONTH);`)
    //         .catch(helpers.log);
    // }else if(suffix === 'enable'){
    //     reply(m, "I'll remove you from the do not disturb list.");

    //     return db.query(`DELETE FROM discord_last_tagged WHERE user_id = '${m.discordID}';`)
    //         .catch(helpers.log);
    // }else if(suffix === 'ignorehost'){
    //     if(this.isAutotagging(m))
    //         return reply(m, "You can't use this if you're currently autotagging people.");
    //     if(!this.autoTagInfo[channelID] || !this.autoTagInfo[channelID].searchers.length)
    //         return reply(m, 'No one is using autotag right now.');

    //     reply(m, "I'll make sure to never tag you if the current auto taggers are searching.");

    //     var qString = 'INSERT INTO discord_autotag_block (blocked, blocker) VALUES ';
    //     this.autoTagInfo[channelID].searchers.forEach((seeker, i) => {
    //         if(i)
    //             qString += ', ';
    //         qString += `('${seeker}', '${m.discordID}')`;
    //     });
    //     db.query(qString).catch(helpers.log);
    // }else{
    // reply(m, `Unknown autotag command.  Use **${m.channelCommand}autotag help** "
    //     + "to see all available autotag commands`);
    // }
}

// DiscordWrapper.prototype.isAutotagging = function(m){
//     if(!this.autoTagInfo[m.channelID])
//         return false;

//     return this.autoTagInfo[m.channelID].isSearcher(m.discordID);
// };

async function name(m){
    const newName = m.args;
    if(!newName)
        throw userError(`Must specify a name. For example **${m.channelCommand}name IamNotMafia**`);

    const discordName = await discordJsonClient.getName(m.discordID, m.channelID);
    const similarity = stringSimilarity.compareTwoStrings(newName, discordName);
    if(config.discord.word_similarity_threshold > similarity)
        throw userError("That name isn't similar enough to your Discord name.  "
            + "It'll only confuse other people!");


    const userID = await userService.getUserID(m.discordID);

    try{
        await playerService.updatePlayerName(userID, newName);
    }catch(err){
        throw userError(err.errors[0]);
    }
}

async function prefer(m){
    const { setupID } = ensureGameInChannel(m);

    const userID = await userService.getUserID(m.discordID);
    if(!userID)
        throw userError('Must be in a game to prefer anything!');

    const role = m.args;
    if(!role)
        throw userError(`No role selected! Usage : **${m.channelCommand}prefer Doctor**`);

    if(!m.isDM)
        messageService.deleteMessage(m.raw);

    try{
        await preferService.prefer(userID, role, setupID);
        return discordJsonClient.sendDirectMessage(m.discordID, 'Prefer noted.');
    }catch(err){
        throw userError('Unknown role');
    }
}

async function clearPrefers(m){
    const { setupID } = ensureGameInChannel(m);
    const userID = await userService.getUserID(m.discordID);

    if(userID)
        await preferService.deleteBySetupIDUserID(setupID, userID);
    return discordJsonClient.sendMessage({
        channelID: m.channelID,
        message: 'You have cleared all your prefers for this setup.',
    });
}

async function setups(m){
    let setupName = m.args;

    const metadata = m.gameMetadata;
    if(metadata && metadata.isStarted){
        const errorText = setupName
            ? 'How do you expect me to change the setup midgame?'
            : "I'll show you other setups once this game completes.";
        throw userError(errorText);
    }

    if(!setupName){
        const featuredSetups = await setupService.getFeaturedSetups();
        featuredSetups.sort((s1, s2) => s1.priority - s2.priority);
        let output = '**Featured Setups**:\n';
        for(let i = 0; i < featuredSetups.length; i++){
            output += `${featuredSetups[i].priority}. *${featuredSetups[i].key}* - `;
            if(featuredSetups[i].key === 'mash')
                output += `**${featuredSetups[i].slogan}**\n`;
            else
                output += `${featuredSetups[i].slogan}\n`;
        }

        output += 'You may also do **custom** to setup a custom game.  '
            + 'However, you have to use the link the bot sends you to edit the game.';
        discordJsonClient.sendPublicMessage(m.channelID, output);

        output = `**Changing Setups Example**\n${m.channelCommand}setup vanillab\n`
            + `${m.channelCommand}setup 3`;
        return discordJsonClient.sendPublicMessage(m.channelID, output);
    }
    if(!metadata)
        throw userError('You must be part of the game to submit this command.');
    const ogSetupName = setupName;
    setupName = setupName.replace(/ /g, '').toLowerCase();

    const users = await userService.getByGameID(metadata.gameID);
    const moderator = users.find(u => u.isModerator);
    if(moderator && setupName === 'custom'){
        const setupID = await getOrCreateSetup(moderator.userID, metadata.gameID);
        await setupService.setSetup({
            joinID: metadata.lobbyID,
            setupID,
        });
        return discordJsonClient.sendPublicMessage(m.channelID,
            'Setup has been changed to Custom.');
    }

    const featuredSetups = await setupService.getFeaturedSetups();
    const featuredSetup = featuredSetups.find(fs => setupName === fs.key.toLowerCase()
        || parseInt(setupName, 10) === fs.priority);
    if(!featuredSetup)
        return discordJsonClient.sendPublicMessage(m.channelID, `Unknown setup '${ogSetupName}'`);
    await setupService.setSetup({
        setupID: featuredSetup.id,
        joinID: metadata.lobbyID,
    });
    return discordJsonClient.sendPublicMessage(m.channelID,
        `Setup has been changed to ${featuredSetup.name}.`);
}

async function moderate(m){
    const { gameID } = ensureGameInChannel(m);

    let rem = m.args;
    if(!rem)
        rem = 'on';
    rem = rem.toLowerCase();
    if(rem !== 'on' && rem !== 'off')
        throw userError(`Example usage for ${m.channelCommand} moderate**\n"
            + "${m.channelCommand}moderate on/off`);

    const users = await userService.getByGameID(gameID);
    const user = users.find(u => u.isModerator && u.discordID === m.discordID);
    if(!user)
        throw userError('Only hosts can use this command.');

    const { userID } = user;
    const discordName = await discordJsonClient.getName(m.discordID, m.channelID);
    if(rem === 'on'){
        await moderatorService.create(userID);
        throw userError(`${discordName} will be moderating this game.`);
    }else{
        await moderatorService.delete(userID, discordName);
        throw userError(`${discordName} will no longer be moderating this game.`);
    }
}

async function autoParity(m){
    const rem = m.args.toLowerCase();
    if(rem !== 'on' && rem !== 'off')
        throw userError('Usage: _autoParity **on**_ or _autoParity **off**_.');
    await editRules(m, { name: 'AUTO_PARITY', value: rem === 'on' });
    const message = `Auto day parity was turned ${rem}.`;
    return discordJsonClient.sendPublicMessage(m.channelID, message);
}

async function chatRate(m){
    const rem = m.args.toLowerCase();
    const seconds = parseInt(rem, 10);
    if(!Number.isInteger(seconds))
        throw userError('Usage: _autoParity **2**_ to limit someone to one message per 2 seconds.');
    await editRules(m, { name: 'CHAT_RATE', value: seconds });
    const message = `Allowed chat rate was set to 1 per ${seconds} second(s).`;
    return discordJsonClient.sendPublicMessage(m.channelID, message);
}

async function discussionLength(m){
    const min = parseInt(m.args, 10);
    if(!Number.isInteger(min))
        throw userError(`**Example usage for ${m.channelCommand}discussionLength**\n"
            + "${m.channelCommand}discussionLength 14`);

    await editRules(m, { name: 'DISCUSSION_LENGTH', value: min * 60 });

    return discordJsonClient.sendPublicMessage(m.channelID,
        `Discussion length changed to ${min} minutes.`);
}

async function trialLength(m){
    const min = parseInt(m.args, 10);
    if(!Number.isInteger(min))
        throw userError(`**Example usage for ${m.channelCommand}trialLength**\n"
            + "${m.channelCommand}trialLength 14`);

    await editRules(m, { name: 'TRIAL_LENGTH', value: min * 60 });

    return discordJsonClient.sendPublicMessage(m.channelID,
        `Trial length changed to ${min} minutes.`);
}

async function nightLength(m){
    const min = parseInt(m.args, 10);
    if(!Number.isInteger(min))
        throw userError(`**Example usage for ${m.channelCommand}nightLength**\n"
            + "${m.channelCommand}nightLength 14`);

    await editRules(m, { name: 'NIGHT_LENGTH', value: min * 60 });

    return discordJsonClient.sendPublicMessage(m.channelID,
        `Night length changed to ${min} minutes.`);
}

async function dayLength(m){
    const min = parseInt(m.args, 10);
    if(!Number.isInteger(min))
        throw userError(`**Example usage for ${m.channelCommand}dayLength**\n"
            + "${m.channelCommand}dayLength 13`);

    await editRules(m, { name: 'DAY_LENGTH_START', value: min * 60 });

    return discordJsonClient.sendPublicMessage(m.channelID,
        `Day length changed to ${min} minutes.`);
}

function day(m){
    const rem = m.args;
    if(!rem || rem.toLowerCase() !== 'start')
        throw userError(`**Example usage for ${m.channelCommand}day** : "
            + "${m.channelCommand}day start"`);
    return dayStart(m);
}

async function dayStart(m){
    await editRules(m, { name: 'DAY_START', value: true, minPlayerCount: 0 });

    discordJsonClient.sendMessage({
        channelID: m.channelID,
        message: 'Game will now start at daytime.',
    });
}

function night(m){
    const rem = m.args;
    if(!rem || rem.toLowerCase() !== 'start')
        throw userError(`**Example usage for ${m.channelCommand}night** : "
            + "${m.channelCommand}night start`);

    return nightStart(m);
}

async function nightStart(m){
    await editRules(m, { name: 'DAY_START', value: false });

    return discordJsonClient.sendMessage({
        channelID: m.channelID,
        message: 'Game will now start at nighttime.',
    });
}

async function voteSystem(request){
    const value = parseInt(request.args, 10);

    await editRules(request, { name: 'VOTE_SYSTEM', value });
    return discordJsonClient.sendMessage({
        channelID: request.channelID,
        message: 'Vote system changed.',
    });
}

async function voteSystems(request){
    return discordJsonClient.sendMessage(mappers.voteSystems(request));
}

async function punch(m){
    const { isStarted } = ensureGameInChannel(m);
    let rem = m.args;

    if(isStarted)
        return actionService.submitAction(m, `punch ${rem}`);

    if(!rem)
        throw userError(`**Example usage for ${m.channelCommand}punch**\n`
            + `${m.channelCommand}punch on`);

    rem = rem.toLowerCase();
    if(rem !== 'on' && rem !== 'off')
        throw userError("Game hasn't started yet!  Unless you meant to toggle it **on/off**.");

    await editRules(m, { name: 'PUNCH_ALLOWED', value: rem === 'on' });

    const allowedText = 'Day punches are now allowed.';
    const disallowedText = 'Day punches are no longer allowed.';
    const punchReply = rem === 'on' ? allowedText : disallowedText;

    return discordJsonClient.sendMessage({
        channelID: m.channelID,
        message: punchReply,
    });
}

async function start(m){
    const { gameID } = ensureGameInChannel(m);

    const hostUserID = await getHostUserID(m);
    if(!hostUserID)// was deleted
        return;
    try{
        await hanselGameService.start(gameID);
    }catch(responseObj){
        throw userError(responseObj.errors[0]);
    }
}

async function lw(m){
    const { isStarted } = ensureGameInChannel(m);
    let rem = m.args || '';

    if(!isStarted){
        rem = rem.toLowerCase();
        if(rem !== 'on' && rem !== 'off')
            throw userError("Game hasn't started yet!  Unless you meant to toggle it **on/off**.");

        await editRules(m, { name: 'LAST_WILL', value: rem === 'on' });

        let lwReply;
        if(rem === 'on')
            lwReply = 'Last wills are now enabled.';
        else
            lwReply = 'Last wills are now disabled.';
        discordJsonClient.sendMessage({
            channelID: m.channelID,
            message: lwReply,
        });
    }


    if(!rem.length && !m.isDM)
        throw userError('I cannot reveal your last will until you die!');

    const userID = await userService.getUserID(m.discordID);
    if(!userID)
        return;

    if(!rem.length)
        try{
            const profile = await profileService.get(userID);
            const message = profile.lastWill
                ? `Your current last will is:\n${profile.lastWill}`
                : 'No last will.';
            return discordJsonClient.sendDirectMessage(m.discordID, message);
        }catch(err){
            return helpers.log(err);
        }


    try{
        await hanselActionService.submitActionMessage(userID, { message: `lw ${rem}` });
        return discordJsonClient.sendDirectMessage(m.discordID, 'Last will updated.');
    }catch(err){
        throw userError(err.errors[0]);
    }
}

async function roleCard(m){
    ensureGameInChannel(m);
    const userID = await userService.getUserID(m.discordID);
    const profile = userID ? await profileService.get(userID) : {};
    if(!profile.roleCard)
        throw userError('You do not have a role card.');
    return discordJsonClient.sendDirectMessage(m.discordID, mappers.roleCard(profile));
}

async function voteCount(m){
    const { gameID } = ensureGameInChannel(m);
    const game = await hanselGameService.getByID(gameID);
    if(game.phase.name !== gamePhase.VOTES_OPEN)
        throw userError("It isn't daytime");
    const votes = await voteService.getVotes(gameID);

    const userID = await userService.getUserID(m.discordID);
    if(!userID)
        return;

    const isPublic = !m.isDM && !game.players.find(player => player.userID === userID);
    const playerName = (game.players.find(p => p.userID === userID) || {}).name;
    const requestDiscordID = isPublic ? null : m.discordID;
    const message = mappers.voteOverview(
        votes, game, { name: playerName, userID }, new Set([m.channelID]), requestDiscordID,
    );
    return discordJsonClient.sendMessage(message);
}

async function living(m){
    if(!m.gameMetadata)
        throw userError('I can\'t tell you who\'s alive if the game\'s not started.');

    const { gameID } = m.gameMetadata;
    const [game, channelIDs] = await Promise.all([
        hanselGameService.getByID(gameID),
        gameService.getChannelIDs(gameID),
    ]);
    const output = mappers.livingOverview(game);

    try{
        if(game.isStarted && channelIDs.has(m.channelID))
            return discordJsonClient.sendDirectMessage(m.discordID, output);
        return discordJsonClient.sendPublicMessage(m.originChannelID, output);
    }catch(err){
        helpers.log(err);
    }
}

// eslint-disable-next-line no-unused-vars
function stats(m){
    throw userError('This feature is temporarily disabled.');
    // narrRequest('personal_stats', m.discordID)
    //     .then(data => {
    //         const output = {
    //             description: `${m.member.nickname}'s stats`,

    //             fields: [],

    //             footer: {
    //                 text: 'Hosted by The Narrator',
    //             },
    //         };

    //         output.fields.push({
    //             name: 'Total Points',
    //             value: data.points,
    //             inline: true,
    //         });

    //         const values = Object.keys(data.roleStats).map(k => data.roleStats[k]);
    //         let bestRole = '';
    //         let stat = null;
    //         let name = 'Best Role';
    //         let totalGames = 0;
    //         values.forEach(v => {
    //             if(!stat){
    //                 stat = parseInt(v.wins, 10) / v.games.length;
    //                 bestRole += `${v.baseName}\n`;
    //             }else if(stat === parseInt(v.wins, 10) / v.games.length){
    //                 bestRole += `${v.baseName}\n`;
    //                 name = 'Best Roles';
    //             }else if(stat < parseInt(v.wins, 10) / v.games.length){
    //                 stat = parseInt(v.wins, 10) / v.games.length;
    //                 bestRole = `${v.baseName}\n`;
    //                 name = 'Best Role';
    //             }
    //             totalGames += v.games.length;
    //         });

    //         if(bestRole)
    //             output.fields.push({
    //                 name: name,
    //                 value: bestRole,
    //                 inline: true,
    //             });

    //         output.fields.push({
    //             name: 'Total Games Played',
    //             value: totalGames,
    //             inline: true,
    //         });

    //         output.fields.push({
    //             name: 'Win Rate',
    //             value: `${data.winRate}%`,
    //             inline: true,
    //         });

    //         m.channel.send({ embed: output }).catch(helpers.log);
    //     });
}

// eslint-disable-next-line no-unused-vars
function leaderboard(m){
    throw userError('This feature is temporarily disabled.');
    // const p1 = narrRequest('leaderboard', null, { userID: m.discordID, columnName: 'points' });
    // const p2 = narrRequest('leaderboard', null, { userID: m.discordID, columnName: 'winRate' });
    // const p3 = narrRequest('leaderboard', null, {
    //     userID: m.discordID,
    //     columnName: 'gamesPlayed' }
    // );
    // return Promise.all([p1, p2, p3])
    //     .then(data => {
    //         let hadData = false;
    //         const points = data[0];
    //         const winRate = data[1];
    //         const gamesPlayed = data[2];

    //         const titles = ['Most Points', 'Best Winrate', 'Most Games Played'];

    //         const output = {
    //             description: 'Narrator Leaderboard',
    //             fields: [],
    //             footer: {
    //                 text: 'Hosted by The Narrator',
    //             },
    //         };

    //         let board; let value; let
    //             title;
    //         for(let i = 0; i < data.length; i++){
    //             board = data[i];
    //             title = titles[i];
    //             value = '';

    //             board.forEach(rank => {
    //                 value += `${rank.rank}. `;
    //                 if(!rank.name)
    //                     value += `**${m.member.nickname}**`;
    //                 else
    //                     value += rank.name;
    //                 value += `\t **${rank.value}`;
    //                 value += '**\n';
    //             });

    //             hadData = hadData || value;
    //             value = value || 'No Data';

    //             output.fields.push({
    //                 name: title,
    //                 value: value,
    //                 inline: (i % 3 !== 2),
    //             });
    //         }

    //         if(!hadData)
    //             throw userError('No leaderboard data available yet.');

    //         return m.channel.send({ embed: output });
    //     }).catch(helpers.log);
}

async function history(m){
    let limit = 0;

    const rem = m.args;
    const prefix = await channelPrefixService.getPrefix(m.channelID);
    if(rem === '--help'){
        const metadata = m.gameMetadata;

        const text = `${prefix}history <number>** - list _x_ amount of recaps\n`
            + `${prefix}history --all** - list as many recaps as fits in the screen\n`
            + ' *more history commands coming soon!* ';


        if(metadata && metadata.isStarted)
            return discordJsonClient.sendDirectMessage(m.discordID, text);
        return discordJsonClient.sendPublicMessage(m.originChannelID, text);
    }
    if(helpers.isInt(rem)){
        limit = parseInt(rem, 10);
    }else if(!rem){
        limit = 10;
    }else if(rem !== 'all'){
        const replyText = `Unknown history flag. Type ${prefix}history --help `
            + 'to get more options on history commands.';
        throw userError(replyText);
    }

    const user = await userIntegrationsRepo.getByExternalID(m.discordID, userTypes.DISCORD);
    let results;
    if(user)
        results = await historyRepo.get(user.id, limit);
    else
        results = [];

    const discordName = discordJsonClient.getName(m.discordID);
    const game = discordClient.getGameByChannelID(m.originChannelID);
    const message = mappers.historyOverview(results, discordName, game, m.discordID);
    return discordJsonClient.sendMessage(message);
}

function skip(m){
    const { isStarted } = ensureGameInChannel(m);
    if(isStarted)
        return endPhase(m);
    return setSkipRule(m);
}

function end(m){
    if(!m.args)
        throw userError('Unknown command: end.');
    const rem = m.args.toLowerCase();
    if(rem !== 'night' && rem !== 'phase')
        throw userError('Unknown command.  Try "end night" or "end phase".');
    return skip(m);
}

function usage(m){
    if(!m.args)
        throw userError(`To see an example of command usage, "
            + "use ${m.channelCommand}usage *command*\n"
            + "To see all available commands, use **${m.channelCommand}commands**`);

    let commandText = m.args;
    commandText = commandText.toLowerCase().replace(/ /g, '');
    const playerCommands = discordClient.getPlayerCommandsByExternalID(m.discordID);
    if(!playerCommands || !(Object.keys(playerCommands).length))
        throw userError('There are no usage examples for you');

    const command = playerCommands[commandText];
    if(!command || !command.usage)
        throw userError('No usage for this command');

    let message = command.usage.split('\n');
    message = message.map(x => m.channelCommand + x);
    discordJsonClient.sendPublicMessage(m.originChannelID, message.join('\n'));
}

async function editTimer(m){
    const { args } = m;
    const usageText = `**Example usage for ${m.channelCommand}editTimer** : "
            + "${m.channelCommand}editTimer 4`;
    if(!args)
        throw userError(usageText);
    const minutes = parseInt(args, 10);
    if(!Number.isInteger(minutes))
        throw userError(usageText);

    const metadata = m.gameMetadata;
    if(!metadata)
        throw userError('No game found to edit.');
    if(!metadata.isStarted)
        throw userError('Game is not started.');
    const userID = await userService.getUserID(m.discordID);
    const game = await hanselGameService.getByID(metadata.gameID);
    if(!game.users.find(user => user.id === userID && user.isModerator))
        throw userError('Only moderators can use this command.');

    return phaseService.setExpiration(game.joinID, minutes * 60);
}

async function playerActions(m){
    const { gameID, isStarted } = ensureGameInChannel(m);
    if(!isStarted)
        throw userError('Game is not started.');
    const [userID, game] = await Promise.all([
        userService.getUserID(m.discordID),
        hanselGameService.getByID(gameID),
    ]);
    if(!userUtil.isNonParticipatingModerator(game, userID))
        throw userError('Only participating moderators can use this command.');
    const playerUserIds = game.players.map(player => player.userID);
    const userIDtoDiscordIDMap = await userService.getUserIDToDiscordIDMap(playerUserIds);
    const userIDs = game.players
        .reduce((acc, player) => (userIDtoDiscordIDMap[player.userID]
            ? [...acc, player.userID]
            : acc), []);
    const profiles = await Promise.all(userIDs.map(profileService.get));
    const playerRolesMessage = mappers.playerActions(profiles);
    return discordJsonClient.sendDirectMessage(m.discordID, playerRolesMessage);
}

async function playerRoles(m){
    const { isStarted, gameID } = ensureGameInChannel(m);
    if(!isStarted)
        throw userError('Game is not started.');
    const userID = await userService.getUserID(m.discordID);
    const game = await hanselGameService.getByID(gameID);
    if(!userUtil.isNonParticipatingModerator(game, userID))
        throw userError('Only participating moderators can use this command.');
    const playerRolesMessage = mappers.playerRoles(game);
    return discordJsonClient.sendDirectMessage(m.discordID, playerRolesMessage);
}

async function setup(m){
    const metadata = m.gameMetadata;
    if(metadata && metadata.isStarted)
        info(m);
    else
        setups(m);
}

module.exports = {
    echo,
    help,
    commands,
    status: info,
    info,
    invite,
    link: invite,
    changecommand: changeCommand,

    host,
    in: join,
    play: join,
    join,
    addbots: addBots,

    out: leave,
    exit: leave,
    quit: leave,
    leave,

    subscribe,
    unsubscribe,

    autotag,

    name,
    prefer,
    clearprefers: clearPrefers,
    setup,
    setups,
    moderate,

    autoparity: autoParity,
    chatrate: chatRate,
    discussionlength: discussionLength,
    nightlength: nightLength,
    daylength: dayLength,
    triallength: trialLength,
    votesystems: voteSystems,

    day,
    daystart: dayStart,
    night,
    nightstart: nightStart,
    votesystem: voteSystem,
    ita: punch,
    punch,

    start,

    living,
    alive: living,
    lw,
    rolecard: roleCard,
    votes: voteCount,
    vc: voteCount,
    votecount: voteCount,

    // moderator commands
    edittimer: editTimer,
    playeractions: playerActions,
    playerroles: playerRoles,

    stats,
    leaderboard,
    history,

    skip,
    end,
    usage,
};

function ensureGameInChannel(m){
    if(!m.gameMetadata.gameID)
        throw userError('There aren\'t active games in this channel!');
    return m.gameMetadata;
}

async function getHostUserID({ gameMetadata }){
    const users = await userService.getByGameID(gameMetadata.gameID);
    const moderator = users.find(u => u.isModerator);
    if(moderator)
        return moderator.userID;
}

const gameModifierNames = new Set([
    'DISCUSSION_LENGTH', 'NIGHT_LENGTH', 'DAY_LENGTH_START', 'TRIAL_LENGTH', 'VOTE_SYSTEM',
    'AUTO_PARITY', 'CHAT_RATE',
]);

async function editRules(m, modifierChangeRequest){
    const { isStarted } = ensureGameInChannel(m);

    if(isStarted)
        throw userError('No updating modifiers after game start.');

    const hostUserID = await getHostUserID(m);
    if(!hostUserID)// was deleted
        return;
    try{
        if(gameModifierNames
            .has(modifierChangeRequest.name)){
            modifierChangeRequest.id = modifierChangeRequest.name;
            await hanselGameService.updateModifier(hostUserID, modifierChangeRequest);
        }else{
            await setupModifierService.create(hostUserID, {
                ...modifierChangeRequest,
                minPlayerCount: 0,
                maxPlayerCount: MAX_PLAYER_COUNT,
            });
        }
    }catch(err){
        throw userError(err.errors[0]);
    }
}

async function endPhase(m){
    const { gameID } = ensureGameInChannel(m);
    const [game, userID] = await Promise.all([
        hanselGameService.getByID(gameID),
        userService.getUserID(m.discordID),
    ]);
    const message = await getEndPhaseMessage(m, game, userID);
    await hanselActionService.submitActionMessage(userID, { message });
    if(message === 'end night')
        return discordJsonClient.sendDirectMessage(m.discordID, 'You opted to end night early.');
}

async function getEndPhaseMessage(m, { phase, users }, userID){
    const isModerator = users.find(u => u.id === userID && u.isModerator);
    if(m.args.toLowerCase() === 'phase'){
        if(isModerator)
            return 'end phase';
        throw userError(`Unknown command: end ${m.args}.`);
    }
    if(phase.name === gamePhase.NIGHTTIME)
        return 'end night';
    return 'skip day';
}

async function setSkipRule(m){
    let rem = m.args;
    if(!rem)
        rem = 'on';
    rem = rem.toLowerCase();
    if(rem !== 'on' && rem !== 'off')
        throw userError(`Example usage for ${m.channelCommand} skip**\n`
            + `${m.channelCommand}skip on/off`);
    await editRules(m, { name: 'SKIP_VOTE', value: rem === 'on' });

    let message;
    if(rem === 'on')
        message = 'Players may skip the day, avoiding a public execution.';
    else
        message = 'Players must vote someone out to end the day.';
    discordJsonClient.sendPublicMessage(m.channelID, message);
}

async function getOrCreateSetup(hostUserID, gameID){
    const [userSetups, gameObj] = await Promise.all([
        setupService.getUserSetups(hostUserID),
        hanselGameService.getByID(gameID),
    ]);
    const userSetup = userSetups.find(s => s.id !== gameObj.setup.id);
    if(userSetup)
        return userSetup.id;
    const newSetup = await setupService.create(hostUserID, { name: 'Discord Custom Setup' });
    return newSetup.id;
}

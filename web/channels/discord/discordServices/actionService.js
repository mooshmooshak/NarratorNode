const voteSystemTypes = require('../../../models/enums/voteSystemTypes');

const discordJsonClient = require('../discordJsonClient');

const { userError } = require('../utils/utils');


async function submitAction(m, command){
    command = command.split(' ').map(val => {
        if(val.startsWith('<@') && val.endsWith('>')){
            let temp = val.substring(2, val.length - 1);
            if(temp.startsWith('!'))
                temp = temp.substring(1);
            if(temp.match('^[0-9]*$'))
                return temp;
        }
        return val;
    }).join(' ');

    const { isMultiVoteCommand, gameID } = await getIsMultiVoteCommand(command, m);
    if(isMultiVoteCommand){
        const [voteCommand, ...targets] = command.split(' ');
        command = targets.length ? [voteCommand, targets.join('')].join(' ') : voteCommand;
    }

    command = command.split('_').join('');

    const userService = require('./userService');
    const userID = await userService.getUserID(m.discordID);

    const actionService = require('../../../services/actionService');
    try{
        const actionCommand = isMultiVoteCommand
            ? await getMultiVoteCommand(userID, gameID, command)
            : command;
        const response = await actionService.submitActionMessage(userID,
            { message: actionCommand });
        if(response.newActionConfirmText)
            return discordJsonClient.sendPublicMessage(m.originChannelID,
                response.newActionConfirmText);
    }catch(responseObj){
        throw userError(responseObj.errors[0]);
    }
}

module.exports = {
    submitAction,
};

async function getIsMultiVoteCommand(commandText, m){
    const [command] = commandText.split(' ' );
    if(!['vote', 'unvote'].includes(command.toLowerCase()))
        return { isMultiVoteCommand: false, gameID: undefined };

    const gameService = require('./gameService');
    const { gameID } = await gameService.getGameMetadataByMessage(m);
    // eslint-disable-next-line max-len
    const isMultiVoteCommand = await gameService.getVoteType(gameID) === voteSystemTypes.MULTIVOTE_PLURALITY;
    return { isMultiVoteCommand, gameID };
}

async function getMultiVoteCommand(userID, gameID, command){
    const hanselVoteService = require('../../../services/voteService');
    const hanselProfileService = require('../../../services/profileService');
    const [voteInfo, profile] = await Promise.all([
        hanselVoteService.getVotes(gameID),
        hanselProfileService.get(userID),
    ]);

    const alreadyVoting = voteInfo.voterToVotes[profile.name];
    if(command.toLowerCase().startsWith('vote')){
        if(!alreadyVoting.length)
            return command;
        return `${command} ${alreadyVoting.join(' ')}`;
    }

    // gets rid of the 'unvote' which is the first element of command
    const [, ...desiredUnvoteTargets] = command.split(' ').map(t => t.toLowerCase());
    if(!desiredUnvoteTargets.length)
        return 'unvote';
    // unvote case
    const newVotes = alreadyVoting.filter(
        voterName => !desiredUnvoteTargets.includes(voterName.toLowerCase()),
    );
    return `vote ${newVotes.join(' ')}`;
}

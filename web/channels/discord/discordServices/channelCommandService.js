const channelPrefixRepo = require('../discordRepos/channelCommandsRepo');


async function getPrefix(channelID){
    return (await channelPrefixRepo.get(channelID)) || '!';
}

async function getPrefixMap(channelIDs){
    const entries = await Promise.all(
        [...channelIDs].map(async channelID => [channelID, await getPrefix(channelID)]),
    );
    return Object.fromEntries(entries);
}

function savePrefix(channelID, prefix){
    return channelPrefixRepo.save(channelID, prefix);
}

module.exports = {
    getPrefix,
    getPrefixMap,
    savePrefix,
};

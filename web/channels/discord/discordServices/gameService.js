const gameRepo = require('../../../repos/replayRepo');
const gameModifierRepo = require('../../../repos/gameModifierRepo');

const actionService = require('../../../services/actionService');

const publicChannelsRepo = require('../discordRepos/publicChannelsRepo');


async function createGameChannel(args){
    return publicChannelsRepo.createPublicChannel(args);
}

async function getChannelIDs(gameID){
    return publicChannelsRepo.getChannelIDs(gameID);
}

async function getChatFrequency(gameID){
    const modifiers = await gameModifierRepo.getByGameID(gameID);
    return modifiers.CHAT_RATE || 0;
}

async function getGameAbilities({ discordID, channelID }, gameID){
    const [userID, resolvedGameID] = await Promise.all([
        require('./userService').getUserID(discordID),
        gameID || getGameMetadataByMessage({ discordID, channelID })
            .then(game => (game ? game.gameID : undefined)),
    ]);
    if(!userID || !resolvedGameID)
        return [];
    return actionService.getAbilities(userID, resolvedGameID);
}

function getGameID(channelID){
    return publicChannelsRepo.getGameID(channelID);
}

async function getGameMetadataByMessage({ discordID, channelID }){
    const results = await Promise.all([
        getGameMetadataByChannelID(channelID),
        getGameMetadataByDiscordID(discordID),
    ]);
    return results.find(x => x);
}

function getGameMetadataByChannelID(channelID){
    return publicChannelsRepo.getGameMetadataByChannelID(channelID);
}

async function getVoteType(gameID){
    const modifiers = await gameModifierRepo.getByGameID(gameID);
    return modifiers.VOTE_SYSTEM;
}

module.exports = {
    createGameChannel,
    getChannelIDs,
    getChatFrequency,
    getGameAbilities,
    getGameID,
    getGameMetadataByMessage,
    getGameMetadataByChannelID,
    getVoteType,
};

async function getGameMetadataByDiscordID(discordID){
    const userService = require('./userService');
    const userID = await userService.getUserID(discordID);
    if(!userID)
        return;
    const profileService = require('../../../services/profileService');
    const { gameID } = await profileService.get(userID);
    if(!gameID)
        return;
    const game = await gameRepo.getByID(gameID);
    return {
        isStarted: game.isStarted,
        gameID: game.id,
        lobbyID: game.lobbyID,
        setupID: game.setupID,
    };
}

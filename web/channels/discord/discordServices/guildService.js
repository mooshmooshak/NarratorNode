const config = require('../../../../config.json');

const hanselProfileService = require('../../../services/profileService');


async function handleGuildMemberAdd(gMember){
    if(gMember.guild.id !== config.discord.night_chat_server)
        return;

    const discordID = gMember.id;

    const userService = require('./userService');
    const userID = await userService.getUserID(discordID);
    if(!userID)
        return;

    const profile = await hanselProfileService.get(userID);
    if(!profile.gameID)
        return;

    const channelService = require('./channelService');
    return channelService.refreshUserNightChatPerms(userID, discordID, profile.gameID);
}

module.exports = {
    handleGuildMemberAdd,
};

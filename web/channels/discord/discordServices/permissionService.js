const discordJsonClient = require('../discordJsonClient');

const { MANAGE_ROLES, MANAGE_MESSAGES, MANAGE_WEBHOOKS } = require('../enums/permissions');
const permissions = require('../enums/permissions');


const MANAGE_ROLES_ERROR_TEXT = 'Players will not be stopped from talking during nighttime';
// eslint-disable-next-line max-len
const MANAGE_MESSAGES_ERROR_TEXT = 'I will not be able to enforce others from talking in this channel';
const MANAGE_WEBHOOKS_ERROR_TEXT = 'No messages will be forwarded to this channel';

const permissionChecks = {
    [MANAGE_ROLES.value]: MANAGE_ROLES_ERROR_TEXT,
    [MANAGE_MESSAGES.value]: MANAGE_MESSAGES_ERROR_TEXT,
    [MANAGE_WEBHOOKS.value]: MANAGE_WEBHOOKS_ERROR_TEXT,
};

async function gameCreateChecks(channelID){
    const permissionResult = await discordJsonClient.hasChannelPermissions(
        channelID, Object.keys(permissionChecks),
    );

    Object.entries(permissionChecks).forEach(
        ([permission, errorText]) => {
            if(!permissionResult[permission])
                discordJsonClient.sendPublicMessage(channelID, getErrorMsg(permission, errorText));
        },
    );
}

module.exports = {
    gameCreateChecks,

    MANAGE_MESSAGES_ERROR_TEXT,
    MANAGE_ROLES_ERROR_TEXT,
    MANAGE_WEBHOOKS_ERROR_TEXT,
};

function getErrorMsg(permissionValue, errorText){
    return `\`Lacking '${permissions[permissionValue].label}' permission. ${errorText}.\``;
}

const { keyBy } = require('lodash');
const config = require('../../../../config');

const helpers = require('../../../utils/helpers');

const discordJsonClient = require('../discordJsonClient');

const hanselChatService = require('../../../services/chatService');

const publicChannelsRepo = require('../discordRepos/publicChannelsRepo');
const nightChannelsRepo = require('../discordRepos/nightChannelsRepo');


async function getOrCreateNightChat(gameID, chatName){
    const channelID = await getChannelID(gameID, chatName);
    if(channelID)
        return channelID;

    chatName = transformNightChatName(chatName);
    const newChannelID = await discordJsonClient.createChannel(config.discord.night_chat_server,
        chatName, {
            topic: 'For common commands, type: __!commands__',
            type: 'text',
        }, ['VIEW_CHANNEL']);
    await nightChannelsRepo.save(newChannelID, gameID, chatName);
    return newChannelID;
}

async function setChannelParticipants(gameID, channelIDs, allowedUserIDs){
    const userService = require('./userService');
    const users = await userService.getByGameID(gameID);

    const promises = users
        .filter(user => user.discordID)
        .map(async({ userID, discordID }) => {
            if(allowedUserIDs.has(userID))
                return discordJsonClient.addChatPermission(discordID, channelIDs);
            return discordJsonClient.removeChatPermission(discordID, channelIDs);
        });
    try{
        return await Promise.all(promises);
    }catch(err){
        helpers.log(err);
    }
}

function getUserIDsFromChat(chat){
    return Object.keys(chat.members).map(userID => parseInt(userID, 10));
}

async function getDiscordUsersFromChats(chats){
    const userService = require('./userService');
    const usersIDsWithNightChats = [...new Set(chats.map(chat => getUserIDsFromChat(chat)).flat())];
    const discordUsers = await Promise.all(usersIDsWithNightChats.map(async userID => ({
        userID,
        discordID: await userService.getDiscordID(userID),
    })));
    return discordUsers.filter(({ discordID }) => discordID);
}

async function refreshGameNightChatChannelsPermissions({ chats, gameID }){
    chats = await Promise.all(chats.map(async nightChat => ({
        ...nightChat,
        discordChannelID: await getOrCreateNightChat(gameID, nightChat.name),
    })));
    const discordUsers = await getDiscordUsersFromChats(chats);
    const userIDToDiscordID = keyBy(discordUsers, 'userID');

    const usersToChats = discordUsers.reduce((acc, discordUser) => ({
        ...acc,
        [discordUser.userID]: [],
    }), {});
    chats.forEach(chat => {
        const userIDs = getUserIDsFromChat(chat);
        userIDs
            .filter(userID => userIDToDiscordID[userID])
            .forEach(userID => {
                usersToChats[userID] = [...usersToChats[userID], chat];
            });
    });

    return Promise.all([
        setAllowedChatPermissions(usersToChats, userIDToDiscordID),
        removeOldChatPermissions(chats, userIDToDiscordID),
    ]).catch(helpers.log);
}

async function refreshUserNightChatPerms(userID, discordID, gameID){
    const { chatReset } = await hanselChatService.getUserChat(gameID, userID);
    if(!chatReset)
        return;
    const channelIDs = await Promise.all(chatReset.map(
        async({ chatName, chatKey }) => [await getChannelID(gameID, chatName), !!chatKey],
    ));

    return Promise.all(
        channelIDs
            .filter(([cID]) => cID) // removes undefined ids
            .map(([channelID, isActive]) => {
                const nightChatPermission = getNightChatPermission(isActive);
                return discordJsonClient.setChannelPermissions(channelID, discordID,
                    nightChatPermission);
            }),
    );
}

async function deleteGameNightChats(gameID){
    const channelIDs = await nightChannelsRepo.getChannelIDsByGameID(gameID);
    await Promise.all(channelIDs.map(channelID => discordJsonClient.deleteChannel(channelID)));
    return nightChannelsRepo.deleteByGameID(gameID);
}

async function deleteAllNightChats(){
    const channelIDs = await nightChannelsRepo.getAll();
    return Promise.all([
        ...channelIDs.map(channelID => discordJsonClient.deleteChannel(channelID)),
        nightChannelsRepo.deleteAll(),
    ]);
}

function deletePublicGameChannel(channelID){
    return Promise.all([
        discordJsonClient.resetChatPermissions(channelID),
        publicChannelsRepo.deleteByChannelID(channelID),
    ]);
}

module.exports = {
    getOrCreateNightChat,
    setChannelParticipants,
    refreshGameNightChatChannelsPermissions,
    refreshUserNightChatPerms,
    deleteGameNightChats,
    deleteAllNightChats,
    deletePublicGameChannel,
};

async function getChannelID(gameID, chatName){
    chatName = transformNightChatName(chatName);
    return nightChannelsRepo.getChannelID(gameID, chatName);
}

function transformNightChatName(chatName){
    chatName = chatName.replace(/\W+/g, '-');
    if(chatName.endsWith('-'))
        chatName = chatName.substring(0, chatName.length - 1);
    return chatName;
}

function sendNightGuildInvite(channelID, discordID){
    discordJsonClient.getInviteURL(channelID).then(inviteURL => {
        const text = 'You can access '
            + `your secret night chats here: ${inviteURL}`;
        return discordJsonClient.sendDirectMessage(discordID, text);
    });
}

function getNightChatPermission(active, removing){
    return {
        SEND_MESSAGES: !!active,
        READ_MESSAGE_HISTORY: !removing,
        VIEW_CHANNEL: !removing,
    };
}

async function setAllowedChatPermissions(usersToChats, userIDToDiscordID){
    const allowPromises = Object.entries(usersToChats).map(async([userID, userChats]) => {
        const { discordID } = userIDToDiscordID[userID];
        const isInGuild = await discordJsonClient.isInNightChatsGuild(discordID);
        if(!isInGuild)
            return sendNightGuildInvite(userChats[0].discordChannelID, discordID);

        const userPromises = userChats.map(({ active: isActive, discordChannelID: channelID }) => {
            const nightChatPermission = getNightChatPermission(isActive);
            return discordJsonClient.setChannelPermissions(channelID, discordID,
                nightChatPermission);
        });
        return Promise.all(userPromises);
    });

    return Promise.all(allowPromises);
}

function removeOldChatPermissions(chats, userIDToDiscordID){
    const promises = chats.map(async chat => {
        const channelID = chat.discordChannelID;
        // eslint-disable-next-line max-len
        const channelPermissions = await discordJsonClient.getChannelPermissionOverwrites(channelID);
        const allowedDiscordIDs = new Set(getUserIDsFromChat(chat)
            .filter(userID => userIDToDiscordID[userID])
            .map(userID => userIDToDiscordID[userID]));
        return channelPermissions
            .filter(perm => allowedDiscordIDs.has(perm.id))
            .map(perm => Promise.all([
                perm.delete(),
            ]));
    });
    return Promise.all(promises);
}

function getMessage(request, channelIDs, userIDToDiscordIDMap){
    let messages = [];
    if(!request.settings.secretVoting)
        messages.push({
            channelIDs,
            message: request.voteText,
        });
    else if(request.voter && userIDToDiscordIDMap[request.voter.id])
        messages.push({
            discordID: userIDToDiscordIDMap[request.voter.id],
            message: request.voteText,
        });

    const playerUserIDs = new Set(request.playerUserIDs);
    const moderatorPMs = request.users.filter(user => user.isModerator)
        .filter(user => !playerUserIDs.has(user.id))
        .filter(user => userIDToDiscordIDMap[user.id])
        .map(user => ({
            discordID: userIDToDiscordIDMap[user.id],
            message: request.voteText,
        }));
    if(request.settings.secretVoting){
        moderatorPMs.push(messages[0]);
        messages = moderatorPMs;
    }

    return messages;
}

module.exports = {
    getMessage,
};

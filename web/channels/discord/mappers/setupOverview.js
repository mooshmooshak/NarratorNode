const hiddenSpawnUtil = require('../../../utils/hiddenSpawnUtil');
const setupHiddenUtil = require('../../../utils/setupHiddenUtil');

const colorUtils = require('../utils/colorUtil');
const setHelpers = require('../../../utils/setHelpers');

const userUtil = require('../../../utils/userUtil');


function getMessage(game, requesterUserID){
    let description;
    if(game.isStarted)
        description = 'Game in progress';
    else
        description = 'Waiting for game to start';

    const showRoles = game.isStarted
        && userUtil.isNonParticipatingModerator(game, requesterUserID);

    const output = {
        title: `#${game.joinID} Status`,
        description,

        fields: [],

        footer: {
            text: 'Hosted by The Narrator',
        },
    };

    const playerCount = game.players.length;
    const rolesList = game.setup.setupHiddens
        .filter(setupHidden => setupHiddenUtil.isSpawnable(playerCount, setupHidden))
        .map(setupHidden => getSetupHiddenEntry(setupHidden, game.setup, playerCount));
    rolesList.sort();
    const rolesListText = rolesList.length ? rolesList.join('\n') : '**No roles added**';
    output.fields.push({
        name: 'Roles List',
        value: rolesListText,
        inline: true,
    });

    const playerFieldTitle = game.isStarted ? 'Player List' : 'Player Lobby';
    output.fields.push({
        name: playerFieldTitle,
        value: getPlayerList(game, showRoles) + getMinMaxFlourish(game),
        inline: true,
    });

    if(!game.isStarted)
        output.fields.push({
            name: 'Setup',
            value: game.setup.name,
            inline: true,
        });

    return { embed: output };
}

function getMinMaxFlourish(game){
    if(game.isStarted)
        return '';
    const playerCount = game.players.length;
    let min = game.setup.minPlayerCount;
    let max = game.setup.maxPlayerCount;
    if(playerCount < min){
        min -= playerCount;
        return `\n*${min} more to start*`;
    }
    if(playerCount !== max){
        max -= playerCount;
        return `\n-*You may add ${max} more players*`;
    }
    return '';
}

function getPlayerList(game, showRoles){
    const players = game.players.map(player => {
        const p = {
            name: player.name,
        };
        if(showRoles){
            p.color = player.factionRole.team.color;
            p.roleName = player.factionRole.name;
        }else if(isDead(game, player.name)){
            p.color = player.flip.color;
            p.roleName = player.flip.name;
        }
        return p;
    });

    players.sort((x, y) => {
        if(x.color && y.color)
            return x.color.localeCompare(y.color);
        if(x.color)
            return 1;
        if(y.color)
            return -1;
        return x.name.toLowerCase().localeCompare(y.name.toLowerCase());
    });

    let ret = '';
    for(let i = 0; i < players.length; i++){
        const p = players[i];
        if(p.color)
            ret += colorUtils.getSymbolByColor(p.color);
        else
            ret += `**[${i + 1}]**`;
        ret += ` ${p.name}`;
        if(p.roleName)
            ret += ` - *${p.roleName}*`;
        ret += '\n';
    }

    return ret;
}

function isDead(game, name){
    let d;
    for(let i = 0; i < game.graveyard.length; i++){
        d = game.graveyard[i];
        if(d.name === name)
            return true;
    }
    return false;
}

function getSetupHiddenEntry(setupHidden, setup, playerCount){
    const hidden = setup.hiddens.filter(h => h.id === setupHidden.hiddenID)[0];
    const factionRoleIDs = hidden.spawns
        .filter(spawn => hiddenSpawnUtil.isSpawnable(playerCount, spawn))
        .map(spawn => spawn.factionRoleID);
    const hiddenFactionRoleIDSet = new Set(factionRoleIDs);
    const factions = setup.factions
        .filter(faction => {
            const factionFactionRoleIDs = new Set(faction.factionRoles
                .map(factionRole => factionRole.id));
            return setHelpers.intersection(factionFactionRoleIDs, hiddenFactionRoleIDSet).size;
        });
    const colors = factions.map(faction => faction.color);
    const color = colors.length === 1 ? colors[0] : null;
    const symbol = colorUtils.getSymbolByColor(color);
    return `${symbol} ${hidden.name}`;
}

module.exports = {
    getMessage,
};

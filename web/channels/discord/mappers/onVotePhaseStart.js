const timeTextUtil = require('../utils/timeTextUtil');


function getMessage({ dayLength }, votes, game, exampleDiscordID, channelPrefixMap){
    const voteOverviewMapper = require('./voteOverview');
    const channelIDs = Object.keys(channelPrefixMap);
    const m1 = {
        channelIDs,
        message: voteOverviewMapper.getMessage(votes, game, channelIDs).message,
    };
    return [
        m1,
        ...Object.entries(channelPrefixMap)
            .map(entry => getExampleMesasge(entry, dayLength, exampleDiscordID)),
    ];
}

module.exports = {
    getMessage,
};

function getExampleMesasge([channelID, channelCommand], dayLength, exampleDiscordID){
    const timeLabel = timeTextUtil.getMinutesLabel(dayLength);
    let message = `Voting polls are now open and will close in ${timeLabel}.`;
    message += `\nTo vote, type ${channelCommand}vote <mention player>.\n`;
    if(exampleDiscordID)
        message += `For example, !vote <@!${exampleDiscordID}>\n`;
    message += 'Don\'t worry, your vote will be hidden.';
    return {
        channelID,
        message,
    };
}

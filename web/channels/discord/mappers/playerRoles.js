const { keyBy } = require('lodash');

const colorUtils = require('../utils/colorUtil');

const mapperUtils = require('./mapperUtils');


function getMessage(game){
    const factionMap = keyBy(game.setup.factions, 'id');
    const playerRoles = game.players
        .sort((p1, p2) => sortPlayerRoles(p1, p2, factionMap))
        .map(p => {
            const roleName = p.factionRole.name;
            const factionColor = factionMap[p.factionRole.factionID].color;
            const colorIcon = colorUtils.getSymbolByColor(factionColor);
            return `**${colorIcon} ${roleName}** - *${p.name}*`;
        });
    return mapperUtils.wrapEmbed({
        fields: [{
            name: '**Player Roles**',
            value: playerRoles.join('\n'),
            inline: false,
        }],
    });
}

module.exports = {
    getMessage,
};

function sortPlayerRoles(p1, p2, factionMap){
    const fr1Name = factionMap[p1.factionRole.factionID].name;
    const fr2Name = factionMap[p2.factionRole.factionID].name;
    if(fr1Name !== fr2Name)
        return fr1Name.localeCompare(fr2Name);
    return p1.name.localeCompare(p2.name);
}

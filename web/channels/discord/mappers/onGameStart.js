function getMessage(moderatorDiscordIDs, channelIDs){
    if(!moderatorDiscordIDs.length)
        return [];
    return {
        channelIDs,
        message: `<@!${moderatorDiscordIDs[0]}>'s game has begun!`,
    };
}

module.exports = {
    getMessage,
};

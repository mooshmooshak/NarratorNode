function getPlayerIndexMap(players){
    return Object.fromEntries(players.map((p, index) => [p.name, index + 1]));
}

function wrapEmbed(input){
    return { embed: { ...input, footer: { text: 'Hosted by The Narrator' } } };
}

module.exports = {
    getPlayerIndexMap,
    wrapEmbed,
};

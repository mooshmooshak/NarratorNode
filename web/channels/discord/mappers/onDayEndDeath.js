function getMessage(deadPlayer, channelIDs){
    return {
        channelIDs,
        message: `${deadPlayer.name} was ${getDeathString(deadPlayer.deathType.attacks)}`,
    };
}

module.exports = {
    getMessage,
};

function getDeathString(attacks){
    return `${attacks.map(attack => attack.text.toLowerCase()).join(', ')}.`;
}

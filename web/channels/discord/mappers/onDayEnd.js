function getMessage(votes, game, channelIDs){
    const voteOverviewMapper = require('./voteOverview');
    return {
        channelIDs,
        message: voteOverviewMapper.getMessage(votes, game, channelIDs).message,
    };
}

module.exports = {
    getMessage,
};

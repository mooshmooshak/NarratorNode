function getMessage(request, channelIDs){
    return {
        channelIDs,
        message: request.contents,
    };
}

module.exports = {
    getMessage,
};

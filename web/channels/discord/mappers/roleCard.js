function getMessage(profile){
    const { roleCard } = profile;
    let output = `You are a **${roleCard.factionName} ${roleCard.roleName}**.\n`;

    output += getWinConditionText(profile.roleCard);
    output += getAllyText(profile.allies);
    output += [
        '\n',
        ...profile.roleCard.roleModifierDetails,
        ...(profile.roleCard.abilities.map(a => a.details).flat()),
    ].join('\n');

    return output;
}

module.exports = {
    getMessage,
};

function getWinConditionText({ winConditionText, enemyFactions }){
    if(winConditionText)
        return winConditionText;
    if(enemyFactions.length === 1)
        return `You must eliminate **${enemyFactions[0].name}**.`;

    let output = 'You must eliminate the following : ';
    const enemyFactionNames = enemyFactions.map(({ name }) => name);
    enemyFactionNames.sort();
    output += enemyFactionNames.join(', ');
    output += '.';
    return output;
}

function getAllyText(allies){
    let output = '';
    if(allies.length){
        output += '\n';
        if(allies.length === 1)
            output += 'Your ally is ';
        else
            output += 'Your allies are : ';

        allies.forEach((ally, index) => {
            if(index)
                output += ', ';
            output += `**${ally.name}(${ally.roleName})**`;
            // TODO use roleID and lookup.  game logic isn't there yet
        });
        output += '.';
    }
    return output;
}

module.exports = {
    commands: require('./commands').getMessage,

    voteSystems: require('./voteSystems').getOverview,

    newPlayerPmMessage: require('./newPlayerPM').getMessage,

    onActionSubmitMessage: require('./onActionSubmit').getMessage,
    onBroadcastMessage: require('./onBroadcast').getMessage,
    onDayDeathMessage: require('./onDayDeath').getMessage,
    onDayEndDeathMessage: require('./onDayEndDeath').getMessage,
    onDayEndMessage: require('./onDayEnd').getMessage,
    onGameStartMessage: require('./onGameStart').getMessage,
    onLobbyCloseMessage: require('./onLobbyClose').getMessage,
    onPlayerJoinMessage: require('./onPlayerJoin').getMessage,
    onPlayerRemove: require('./onPlayerRemove').getMessage,
    onNewVoteMessage: require('./onNewVote').getMessage,
    onTimerUpdate: require('./onTimerUpdate').getMessage,
    onTrialStart: require('./onTrialStart').getMessage,
    onVotePhaseReset: require('./onVotePhaseReset').getMessage,
    onVotePhaseStart: require('./onVotePhaseStart').getMessage,

    historyOverview: require('./historyOverview').getMessage,
    livingOverview: require('./livingOverview').getMessage,
    roleCard: require('./roleCard').getMessage,
    setupOverview: require('./setupOverview').getMessage,
    voteOverview: require('./voteOverview').getMessage,

    playerActions: require('./playerActions').getMessage,
    playerRoles: require('./playerRoles').getMessage,
};

function getMessage(seconds){
    const minutes = seconds / 60;
    return `Phase will end in ${minutes} minutes.`;
}

module.exports = {
    getMessage,
};

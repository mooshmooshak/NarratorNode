const config = require('../../../../config');

const mapperUtils = require('./mapperUtils');


function getMessage(authToken, game){
    const url = `http://${config.app_url}/?auth_token=${authToken}`;
    const currentRoleCount = Math.max(game.setup.minPlayerCount, game.players.length);
    const playerCount = `${game.players.length}/${currentRoleCount}`;
    return mapperUtils.wrapEmbed({
        title: 'Setup Link',
        description: `[DO NOT SHARE THIS LINK!!!](${url})`,
        fields: [{
            name: 'Lobby ID',
            value: game.lobbyID,
        }, {
            name: 'Setup',
            value: game.setup.name,
        }, {
            name: 'Player Count',
            value: playerCount,
        }],
        url,
    });
}

module.exports = {
    getMessage,
};

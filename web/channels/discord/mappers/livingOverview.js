const mapperUtils = require('./mapperUtils');


function getMessage(game){
    const livingPlayers = game.players.filter(p => !isDead(game, p.name));
    const indexMap = mapperUtils.getPlayerIndexMap(game.players);
    livingPlayers.sort((p1, p2) => p1.name.localeCompare(p2.name));
    const playerOutput = livingPlayers
        .map(p => `**[${indexMap[p.name]}]** ${p.name}\n`)
        .join('');
    const output = {
        fields: [{
            name: '**The Living**',
            value: playerOutput,
            inline: false,
        }],
    };
    return mapperUtils.wrapEmbed(output);
}

module.exports = {
    getMessage,
};

function isDead(game, name){
    for(let i = 0; i < game.graveyard.length; i++){
        const deadPlayer = game.graveyard[i];
        if(deadPlayer.name === name)
            return true;
    }
    return false;
}

package util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import game.abilities.Hidden;
import game.ai.Controller;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.RoleAssigner;
import game.logic.support.RolePackage;
import game.setups.Setup;
import integration.logic.SuperTest;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import services.RoleAbilityModifierService;
import services.SetupHiddenService;
import util.game.HiddenUtil;
import util.game.SetupHiddenUtil;

public class TestUtil {

    public static void modifyAbility(AbilityType abilityType, AbilityModifierName modifierName, boolean value) {
        for(Role role: SuperTest.setup.roles){
            if(role.hasAbility(abilityType))
                RoleAbilityModifierService.upsert(role, abilityType, modifierName, value);
        }
    }

    public static void modifyAbility(AbilityType abilityType, AbilityModifierName modifierName, int i) {
        for(Role role: SuperTest.setup.roles)
            if(role.hasAbility(abilityType))
                modifyAbility(role, abilityType, modifierName, i);
    }

    public static void modifyAbility(Role role, AbilityType abilityType, AbilityModifierName modifierName, int value) {
        RoleAbilityModifierService.upsert(role, abilityType, modifierName, value);
    }

    public static RoleAssigner assignRoles(Game game, Controller[] controllers, FactionRole[] roleTemplates) {

        Hidden hidden;
        SetupHidden setupHidden;
        Map<Player, SetupHidden> playerToSetupHidden = new HashMap<>();
        for(int i = 0; i < controllers.length; i++){
            hidden = HiddenUtil.createHiddenSingle(game.setup, roleTemplates[i]);
            setupHidden = SetupHiddenService.addSetupHidden(hidden, false, false, 0, Setup.MAX_PLAYER_COUNT);
            playerToSetupHidden.put(controllers[i].getPlayer(), setupHidden);
        }

        return new RoleAssigner() {

            @Override
            public void passRolesOut(Game narrator, List<RolePackage> releasedRoles) {
                Map<SetupHidden, RolePackage> map = SetupHiddenUtil.rolePackageToMap(releasedRoles);
                narrator.getRandom().reset();
                SetupHidden setupHidden;
                RolePackage rolePackage;
                for(Player player: playerToSetupHidden.keySet()){
                    setupHidden = playerToSetupHidden.get(player);
                    rolePackage = map.get(setupHidden);
                    rolePackage.assign(player);
                }
            }

        };

    }

    public static void removeAbility(Role role, AbilityType abilityType) {
        SuperTest.sEditor.removeRoleAbility(role, abilityType);
    }

    public static void addAbility(Role role, AbilityType abilityType) {
        SuperTest.sEditor.addRoleAbility(role, abilityType);
    }

    public static String getRandomString(int targetStringLength) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for(int i = 0; i < targetStringLength; i++){
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    public static JSONObject assertNoErrors(JSONObject response) throws NarratorException, JSONException {
        JSONArray errors = response.optJSONArray("errors");
        if(errors != null && errors.length() != 0)
            throw new NarratorException(errors.getString(0));
        return response;
    }

    public static JSONObject assertErrors(JSONObject response) {
        JSONArray errors = response.optJSONArray("errors");
        if(errors == null || errors.length() == 0)
            throw new NarratorException("Errors were not found in JSON response." + errors);
        return response;
    }

}

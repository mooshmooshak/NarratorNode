package util.models;

import game.setups.Setup;
import models.Faction;
import models.schemas.FactionSchema;
import services.FactionService;
import util.Util;

public class FactionTestUtil {

    public static Faction createFaction(Setup setup, String color, String name) {
        return FactionService.createFaction(setup, new FactionSchema(Util.getID(), color, name, ""));
    }
}

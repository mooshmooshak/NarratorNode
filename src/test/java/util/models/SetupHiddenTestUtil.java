package util.models;

import game.abilities.Hidden;
import game.setups.Setup;
import models.SetupHidden;

public class SetupHiddenTestUtil {
    public static SetupHidden findByHidden(Hidden hidden) {
        for(SetupHidden setupHidden: hidden.setup.rolesList.iterable()){
            if(setupHidden.hidden == hidden)
                return setupHidden;
        }
        return null;
    }

    public static SetupHidden findSetupHidden(Setup setup, String hiddenName) {
        for(SetupHidden setupHidden: setup.rolesList.iterable()){
            if(setupHidden.hidden.getName().equals(hiddenName))
                return setupHidden;
        }
        return null;
    }
}

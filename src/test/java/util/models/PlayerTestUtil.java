package util.models;

import java.sql.SQLException;

import game.logic.Player;
import models.schemas.PlayerSchema;
import nnode.Lobby;
import repositories.PlayerRepo;

public class PlayerTestUtil {

    public static PlayerSchema getSchemaByRoleName(Lobby lobby, String roleName) throws SQLException {
        Player player = getByRoleName(lobby, roleName);
        return PlayerRepo.getByID(player.databaseID);
    }

    public static Player getByRoleName(Lobby lobby, String roleName) {
        for(Player player: lobby.game.players){
            if(player.getRoleName().equals(roleName))
                return player;
        }
        throw new Error("Player with that rolename not found.");
    }
}

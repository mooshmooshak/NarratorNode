package util.models;

import game.abilities.GameAbility;
import game.abilities.util.AbilityUtil;
import game.logic.Game;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class AbilityTestUtil {
    @SuppressWarnings("unchecked")
    public static <T extends GameAbility> T getGameAbility(Class<T> abilityClass, Game game) {
        for(AbilityType abilityType: AbilityType.values()){
            if(abilityClass.getSimpleName().equals(abilityType.toString()))
                return (T) AbilityUtil.CREATOR(abilityType, game, new Modifiers<>());
        }
        throw new Error("Bad class");
    }
}

package util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.abilities.Hidden;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import integration.logic.SuperTest;
import models.Role;
import models.idtypes.GameID;
import repositories.PlayerRepo;

public class Assertions {

    public static void assertRoleNotInSetup(Role ghostWithTeamRole) {
        for(Role role: ghostWithTeamRole.setup.roles)
            SuperTest.assertNotSame(ghostWithTeamRole, role);
    }

    public static void assertHiddenNotInSetup(Hidden excludedHidden) {
        for(Hidden hidden: SuperTest.game.setup.hiddens){
            SuperTest.assertNotSame(excludedHidden, hidden);
        }
    }

    public static void notIn(long element, ArrayList<Long> list) {
        SuperTest.assertFalse(list.contains(element));
    }

    public static void assertSetupHiddensAreSame(Game n1, Game n2) {
        List<String> list1 = new LinkedList<>();
        List<String> list2 = new LinkedList<>();

        for(Player player: n1.players)
            list1.add(player.getColor() + "@" + player.getRoleName());

        for(Player player: n2.players)
            list2.add(player.getColor() + "@" + player.getRoleName());

        String key;
        for(Player player: n2.players){
            key = player.getColor() + "@" + player.getRoleName();
            if(!list1.contains(key))
                throw new NarratorException("Couldn't find " + key);
            list1.remove(key);
        }
    }

    public static void assertGamePlayerCount(GameID id) throws SQLException {
        int playerCount = PlayerRepo.getByGameID(id).size();
        SuperTest.assertEquals(3, playerCount);
    }

}

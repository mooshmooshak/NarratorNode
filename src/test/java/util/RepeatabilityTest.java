package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.abilities.AbilityList;
import game.abilities.Executioner;
import game.abilities.GameAbility;
import game.abilities.Vote;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.CommandHandler;
import game.logic.support.RoleAssigner;
import game.logic.support.RolePackage;
import game.logic.support.StoryReader;
import game.logic.templates.NativeStoryPackage;
import integration.logic.SuperTest;
import junit.framework.TestCase;
import models.Command;
import models.FactionRole;
import models.GameModifiers;
import models.GeneratedRole;
import models.SetupHidden;
import models.idtypes.PlayerDBID;
import models.modifiers.Modifiers;
import services.GameService;
import util.game.LookupUtil;
import util.game.SetupHiddenUtil;

public class RepeatabilityTest extends TestCase {

    public void testDummy() {

    }

    public static void runRepeatabilityTest() {
        Game superGame = SuperTest.game;
        Game textGame = new Game(superGame.setup);
        StoryReader storyPackage = new NativeStoryPackage(superGame, false); // don't copy ids
        textGame.gameModifiers = new GameModifiers<>(textGame, new Modifiers<>(storyPackage.getGameModifiers()));
        textGame.setSeed(superGame.getSeed());

        HashMap<String, SetupHidden> playerToSetupHidden = new HashMap<>();
        HashMap<Integer, ArrayList<String>> deathDays = new HashMap<>();
        Set<SetupHidden> assignedSetupHiddens = new HashSet<>();

        Integer key;
        for(Player p: superGame.players){
            playerToSetupHidden.put(p.getID(), p.initialAssignedRole.assignedSetupHidden);
            if(p.isAlive())
                key = null;
            else
                key = p.getDeathDay();
            if(!deathDays.containsKey(key))
                deathDays.put(key, new ArrayList<>());
            deathDays.get(key).add(p.getID());
            assignedSetupHiddens.add(p.initialAssignedRole.assignedSetupHidden);
        }

        RoleAssigner roleAssigner = new RoleAssigner() {

            @Override
            public ArrayList<GeneratedRole> pickRoles(Game game, List<SetupHidden> setupHiddens) {
                GeneratedRole generatedRole;
                FactionRole oldFactionRole;
                FactionRole newFactionRole;
                ArrayList<GeneratedRole> generatedRoles = new ArrayList<>();
                for(SetupHidden setupHidden: setupHiddens){
                    for(Player player: superGame.players){
                        if(player.initialAssignedRole.assignedSetupHidden.id != setupHidden.id)
                            continue;
                        oldFactionRole = player.initialAssignedRole.assignedRole;
                        newFactionRole = textGame.setup.getFactionRole(oldFactionRole.id);
                        generatedRole = new GeneratedRole(setupHidden);
                        generatedRole.factionRoles.add(newFactionRole);
                        generatedRoles.add(generatedRole);
                    }
                    for(RolePackage rolePackage: superGame.unpickedRoles){
                        if(rolePackage.assignedSetupHidden.id != setupHidden.id)
                            continue;
                        oldFactionRole = rolePackage.assignedRole;
                        newFactionRole = LookupUtil.findFactionRole(textGame.setup, oldFactionRole.id);
                        generatedRole = new GeneratedRole(setupHidden);
                        generatedRole.factionRoles.add(newFactionRole);
                        generatedRoles.add(generatedRole);
                    }
                }
                return generatedRoles;
            }

            @Override
            public ArrayList<GeneratedRole> getPassOutRoles(int playerCount, ArrayList<GeneratedRole> generatedRoles) {
                Collection<SetupHidden> values = playerToSetupHidden.values();
                ArrayList<GeneratedRole> passOut = new ArrayList<>();
                for(GeneratedRole role: generatedRoles)
                    if(values.contains(role.setupHidden))
                        passOut.add(role);
                return passOut;
            }

            @Override
            public void passRolesOut(Game narrator, List<RolePackage> releasedRoles) {
                narrator.getRandom().reset();
                Map<SetupHidden, RolePackage> map = SetupHiddenUtil.rolePackageToMap(releasedRoles);

                SetupHidden setupHidden;
                for(Player p: narrator.players.sortByID()){
                    setupHidden = playerToSetupHidden.get(p.getID());
                    map.get(setupHidden).assign(p);
                }
            }
        };

        Player textPlayer;
        for(Player p: SuperTest.game.getAllPlayers().sortByID()){
            textPlayer = textGame.addPlayer(p.getID());
            textPlayer.databaseID = p.databaseID;
            textPlayer.rolePrefer(p.initialAssignedRole.assignedRole.role);
            textPlayer.teamPrefer(p.initialAssignedRole.assignedRole.faction);
        }

        GameService.internalStart(textGame, roleAssigner);

        for(Player basePlayer: SuperTest.game.players){
            textPlayer = textGame.getPlayerByID(basePlayer.getID());
            assertEquals(basePlayer.getInitialColor(), textPlayer.getInitialColor());
            RepeatabilityTest.assertSameInitialAbilities(basePlayer, textPlayer);
            RepeatabilityTest.assertExecutionerTargetEquality(basePlayer, textPlayer);

            if(SuperTest.charges != null)
                RepeatabilityTest.assertChargeConsistency(basePlayer, textPlayer);

        }

        Map<PlayerDBID, Player> databaseMap = textGame.players.getDatabaseMap();
        ArrayList<Command> commands = SuperTest.game.getCommands();
        CommandHandler th = new CommandHandler(textGame);
        Command c;
        Player player;
        boolean modMode = true;
        for(Command command: commands){
            c = command;
            try{
                if(c.playerID.isPresent())
                    player = databaseMap.get(c.playerID.get());
                else
                    player = null;
                th.parseCommand(player, c.text, modMode, c.timeLeft);
            }catch(NarratorException e){
                System.err.println(textGame.getHappenings());
                throw e;
            }
        }

        SuperTest.assertEquals(SuperTest.game.phase, textGame.phase);
        SuperTest.assertEquals(SuperTest.game.getDayNumber(), textGame.getDayNumber());
        SuperTest.assertEquals(SuperTest.game.isInProgress(), textGame.isInProgress());
        if(!SuperTest.game.isInProgress() && SuperTest.game.isStarted()){
            String winMessage = SuperTest.game.getWinMessage().access(Message.PRIVATE);
            String textWinMessage = textGame.getWinMessage().access(Message.PRIVATE);
            assertEquals(winMessage, textWinMessage);
        }
        Player q;
        for(Player p: SuperTest.game.players){
            q = textGame.getPlayerByID(p.getID());
            SuperTest.assertEquals(p.isAlive(), q.isAlive());
            SuperTest.assertEquals(p.isWinner(), q.isWinner());
        }

        SuperTest.game.clearListeners();
        SuperTest.roleAssigner = null;
    }

    public static void assertSameInitialAbilities(Player basePlayer, Player comparePlayer) {
        assertEquals(basePlayer.initialAssignedRole.assignedRole, comparePlayer.initialAssignedRole.assignedRole);
    }

    public static void assertExecutionerTargetEquality(Player p, Player oP) {
        if(p.is(Executioner.abilityType)){
            Executioner e1 = p.getAbility(Executioner.class);
            Executioner e2 = oP.getAbility(Executioner.class);

            assertEquals(e1.getTargets().getFirst().getID(), e2.getTargets().getFirst().getID());
        }
    }

    public static void assertChargeConsistency(Player basePlayer, Player comparePlayer) {
        SuperTest.assertTrue(comparePlayer.game.isFirstPhase()); // just to make sure i'm comparing the right player

        AbilityList basePlayerAbilityList = basePlayer.gameRole._abilities;
        AbilityList comparePlayerAbilityList = comparePlayer.gameRole._abilities;
        if(basePlayerAbilityList.getNonpassive().isEmpty()){
            SuperTest.assertTrue(comparePlayerAbilityList.getNonpassive().isEmpty());
            return;
        }

        HashMap<String, Integer> chargeMap = SuperTest.charges.get(basePlayer.getID());
        for(GameAbility a: comparePlayer.gameRole._abilities.filterNot(Vote.abilityType))
            SuperTest.assertEquals((int) chargeMap.get(a.getCommand()), a.getPerceivedCharges());
    }
}

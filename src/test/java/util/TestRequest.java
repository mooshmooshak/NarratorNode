package util;

import json.JSONException;
import json.JSONObject;
import nnode.LobbyManager;

public class TestRequest {

    public static JSONObject get(String url, JSONObject args) throws JSONException {
        return submitRequest(url, "GET", args);
    }

    public static JSONObject post(String url, JSONObject args) throws JSONException {
        return submitRequest(url, "POST", args);
    }

    public static JSONObject put(String url, JSONObject args) throws JSONException {
        return submitRequest(url, "PUT", args);
    }

    public static JSONObject delete(String url, JSONObject args) throws JSONException {
        return submitRequest(url, "DELETE", args);
    }

    private static JSONObject submitRequest(String url, String method, JSONObject body) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("url", url);
        request.put("method", method);
        request.put("userID", 0);
        request.put("httpRequest", true);
        request.put("requestID", "1");
        request.put("body", body);
        return LobbyManager.handleMessage(request);
    }
}

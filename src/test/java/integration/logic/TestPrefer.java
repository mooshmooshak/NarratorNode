package integration.logic;

import game.abilities.Amnesiac;
import game.abilities.Citizen;
import game.abilities.Goon;
import game.abilities.Hidden;
import game.ai.Controller;
import game.logic.templates.BasicRoles;
import models.SetupHidden;
import services.SetupHiddenService;

public class TestPrefer extends SuperTest {

    public TestPrefer(String name) {
        super(name);
    }

    public void testBasicPrefer() {
        Controller p3 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        p3.rolePrefer(BasicRoles.Goon().role);

        dayStart();

        assertTrue(p3.is(Goon.abilityType));
    }

    public void testSpecificInRandom() {
        Controller player = addPlayer();
        addPlayer();
        addPlayer();

        player.rolePrefer(BasicRoles.Citizen().role);

        addRole(BasicRoles.SerialKiller());
        addRole(BasicRoles.SerialKiller());
        SetupHidden setupHidden = addSetupHidden(Hidden.TownRandom());
        SetupHiddenService.setForcedSpawn(game, setupHidden, BasicRoles.Citizen());

        dayStart();

        assertTrue(Citizen.hasNoAbilities(player.getPlayer()));
    }

    public void testNeutralPrefering() {
        Controller a = addPlayer("A");
        addPlayer("B");
        addPlayer("C");

        addSetupHidden(Hidden.TownRandom());
        addRole(BasicRoles.SerialKiller());
        SetupHidden setupHidden = addSetupHidden(Hidden.NeutralBenignRandom());

        a.getPlayer().teamPrefer(BasicRoles.Amnesiac().faction);

        SetupHiddenService.setForcedSpawn(game, setupHidden, BasicRoles.Amnesiac());

        dayStart();

        assertTrue(a.is(Amnesiac.abilityType));

        skipTearDown = true;
    }
}

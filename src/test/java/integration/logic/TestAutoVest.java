package integration.logic;

import game.abilities.DrugDealer;
import game.abilities.JailExecute;
import game.ai.Controller;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.logic.abilities.TestLookout;
import models.enums.RoleModifierName;

public class TestAutoVest extends SuperTest {

    public TestAutoVest(String s) {
        super(s);
    }

    // tests if sheriff is valid
    public void testSingleSurvive() {
        modifyRole(BasicRoles.Lookout(), RoleModifierName.AUTO_VEST, 1);

        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller sk2 = addPlayer(BasicRoles.SerialKiller());

        setTarget(lookout, lookout);
        setTarget(sk, lookout);
        setTarget(sk2, lookout);
        endNight();

        isAlive(lookout);
        TestLookout.seen(lookout, sk, sk2);
        partialContains(lookout, Constants.AUTO_VEST_USAGE);

        assertPerceivedAutovestCount(0, lookout);
    }

    public void testElectro() {
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());

        modifyRole(BasicRoles.Lookout(), RoleModifierName.AUTO_VEST, 1);

        electrify(electro, baker, fodder1);
        setTarget(baker, lookout);
        nextNight();

        setTarget(lookout, baker);
        setTarget(lookout, fodder1);
        endNight();

        isAlive(lookout);
    }

    public void testJailor() {
        addPlayer(BasicRoles.Architect());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller agent = addPlayer(BasicRoles.Agent());

        modifyRole(BasicRoles.Agent(), RoleModifierName.AUTO_VEST, 1);

        jail(jailor, agent);
        skipDay();

        setTarget(jailor, agent, JailExecute.abilityType);
        endNight();

        isDead(agent);
        assertGameOver();
    }

    public void testDoctorSave() {
        modifyRole(BasicRoles.Lookout(), RoleModifierName.AUTO_VEST, 5);

        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller doc = addPlayer(BasicRoles.Doctor());

        setTarget(lookout, lookout);
        setTarget(sk, lookout);
        setTarget(doc, lookout);
        endNight();

        isAlive(lookout);
        TestLookout.seen(lookout, sk, doc);

        assertPerceivedAutovestCount(5, lookout);
    }

    public void testBodyguardNoUse() {
        modifyRole(BasicRoles.Lookout(), RoleModifierName.AUTO_VEST, 5);

        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Arsonist(), 2);

        setTarget(lookout, lookout);
        setTarget(sk, lookout);
        setTarget(bg, lookout);
        endNight();

        isAlive(lookout);
        TestLookout.seen(lookout, sk, bg);

        assertPerceivedAutovestCount(5, lookout);
    }

    public void testDrugUsage() {
        modifyRole(BasicRoles.Lookout(), RoleModifierName.AUTO_VEST, 1);

        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Arsonist(), 2);

        setTarget(sk, lookout);
        drug(dd, lookout, DrugDealer.WIPE);
        endNight();

        partialExcludes(lookout, Constants.AUTO_VEST_USAGE);
        assertPerceivedAutovestCount(1, lookout);
        assertEquals(0, lookout.getPlayer().getRealAutoVestCount());

        skipDay();
        setTarget(sk, lookout);
        endNight();

        isDead(lookout);
    }

}

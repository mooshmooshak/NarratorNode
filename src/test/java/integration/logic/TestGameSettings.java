package integration.logic;

import game.abilities.Hidden;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import models.enums.GameModifierName;
import services.GameService;
import services.HiddenService;

public class TestGameSettings extends SuperTest {

    public TestGameSettings(String s) {
        super(s);
    }

    public void testChatRoleSpawn() {
        Hidden hidden = HiddenService.createHidden(setup, "rm1", BasicRoles.Ventriloquist(), BasicRoles.Architect(),
                BasicRoles.Disguiser());

        addPlayer(hidden);
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Arsonist());

        GameService.upsertModifier(game, GameModifierName.CHAT_ROLES, false);

        try{
            nightStart();
            fail();
        }catch(NarratorException e){
        }

        assertEquals(3, hidden.size());
        assertTrue(game.setup.hiddens.contains(hidden));

        HiddenService.delete(hidden);

        assertFalse(game.setup.rolesList.contains(hidden, 3));
        assertEquals(2, game.setup.rolesList.size(game));

        addRole(BasicRoles.Ventriloquist());

        try{
            nightStart();
            fail();
        }catch(NarratorException e){
        }

    }
}

package integration.logic;

import game.abilities.CultLeader;
import game.abilities.Executioner;
import game.abilities.Ghost;
import game.abilities.Goon;
import game.abilities.Hidden;
import game.abilities.Jester;
import game.abilities.Mason;
import game.abilities.Sheriff;
import game.logic.Player;
import game.logic.RolesList;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.GameModifierName;
import models.modifiers.Modifiers;
import models.schemas.SetupHiddenSchema;
import services.FactionRoleService;
import services.HiddenService;
import services.HiddenSpawnService;
import services.SetupHiddenService;
import util.game.GameUtil;
import util.game.HiddenUtil;
import util.models.HiddenTestUtil;

public class TestRolesList extends SuperTest {

    private Modifiers<GameModifierName> modifiers = GameUtil.getPermissiveModifiers();

    public TestRolesList(String name) {
        super(name);
    }

    public void testEqualRolesList() {
        RolesList rl1 = new RolesList();
        addHidden(rl1, HiddenUtil.createHiddenSingle(setup, BasicRoles.Citizen()));
        addHidden(rl1, Hidden.TownInvestigative());
        addHidden(rl1, HiddenUtil.createHiddenSingle(setup, BasicRoles.Arsonist()));

        RolesList rl2 = new RolesList();
        addHidden(rl2, Hidden.TownInvestigative());
        addHidden(rl2, HiddenUtil.createHiddenSingle(setup, BasicRoles.Citizen()));
        addHidden(rl2, HiddenUtil.createHiddenSingle(setup, BasicRoles.Arsonist()));

        assertEquals(rl1.hashCode(), rl2.hashCode());
    }

    private void addHidden(RolesList rolesList, Hidden hidden) {
        SetupHiddenSchema schema = new SetupHiddenSchema(hidden.id, hidden.id, false, false, 0, Setup.MAX_PLAYER_COUNT);
        SetupHidden setupHidden = new SetupHidden(hidden, schema);
        rolesList._setupHiddenList.add(setupHidden);
    }

    public void testWillStartWithLessPlayers() {
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Goon());
        addRole(BasicRoles.Goon(Setup.YAKUZA_C));

        dayStart();
    }

    public void testNeedEnoughPlayersToGaraunteeEnemy() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Hidden townMafia = HiddenService.createHidden(setup, "townHidden", BasicRoles.Goon());
        FactionRole factionRole;
        Faction town = setup.getFactionByColor(Setup.TOWN_C);
        for(Role role: game.setup.roles){
            if(role.is(Sheriff.abilityType, CultLeader.abilityType, Executioner.abilityType, Ghost.abilityType,
                    Jester.abilityType))
                continue;
            if(!town.roleMap.containsKey(role))
                factionRole = FactionRoleService.createFactionRole(town, role);
            else
                factionRole = town.roleMap.get(role);
            HiddenTestUtil.addFactionRole(townMafia, factionRole);
        }
        addRole(townMafia, 1);

        dayStart();

        for(Player player: game.players){
            if(player.is(Goon.abilityType))
                return;
        }

        fail();
    }

    public void testNeedEnoughRoles() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        addPlayer();

        try{
            startDay(); // any start really
        }catch(NarratorException e){
            return;
        }
        fail();
    }

    public void testMasonSpawning() {
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        addRole(BasicRoles.Architect());

        dayStart();

        assertTrue(game.players.filter(Mason.abilityType).isEmpty());
    }

    public void testMustSpawnNeutralBenign() {
        addRole(BasicRoles.Jester());
        game.setup.rolesList.get(0).mustSpawn = true;
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen(), 50);

        dayStart();

        assertEquals(3, Setup.getMinPlayerCount(setup, modifiers));
        assertTrue(game.players.filter(Jester.abilityType).isNonempty());
    }

    public void testMustIncreasesMinPlayerCount() {
        addRole(BasicRoles.Jester());
        addRole(BasicRoles.Jester());
        game.setup.rolesList.get(1).mustSpawn = true;
        game.setup.rolesList.get(0).mustSpawn = true;
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());

        try{
            dayStart();
            fail();
        }catch(NarratorException e){
        }
        addPlayer();

        dayStart();

        assertEquals(2, game.players.filter(Jester.abilityType).size());
    }

    public void testNotPossibleToStart() {
        addPlayerCountFactionRole(BasicRoles.Jester(), 0, 0);
        addPlayerCountFactionRole(BasicRoles.Goon(Setup.YAKUZA_C), 3, 3);
        addPlayerCountFactionRole(BasicRoles.Agent(), 4, 4);

        int maxPlayerCount = Setup.getMaxPlayerCount(setup, modifiers);

        assertEquals(3, maxPlayerCount);
    }

    private void addPlayerCountFactionRole(FactionRole factionRole, int hsMin, int shMin) {
        Hidden hidden = HiddenService.createHidden(setup, "jester1");
        HiddenSpawnService.addSpawnableRoles(hidden, factionRole, hsMin, Setup.MAX_PLAYER_COUNT);
        SetupHiddenService.addSetupHidden(hidden, false, false, shMin, Setup.MAX_PLAYER_COUNT);
    }
}

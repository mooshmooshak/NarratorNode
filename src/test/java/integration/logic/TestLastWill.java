package integration.logic;

import game.ai.Controller;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.PhaseException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.enums.FactionModifierName;
import models.enums.SetupModifierName;

public class TestLastWill extends SuperTest {

    public TestLastWill(String name) {
        super(name);
    }

    public void testDisabledLastWill() {
        Controller p = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Baker());
        addPlayer(BasicRoles.Executioner());

        try{
            p.setLastWill("");
            fail();
        }catch(PhaseException e){
        }

        editRule(SetupModifierName.LAST_WILL, false);

        dayStart();

        try{
            p.setLastWill("");
            fail();
        }catch(IllegalActionException e){
        }
    }

    public void testLastWillOnDeath() {
        Controller p = addPlayer(BasicRoles.Agent());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Escort());
        addPlayer(BasicRoles.Consort());

        nightStart();
        String lwText = sk.getName() + " is serial killer";
        p.setLastWill(lwText);

        setTarget(sk, p);
        endNight();

        assertTrue(game.getHappenings().contains(lwText));
    }

    public void testJanitorHide() {
        Controller p = addPlayer(BasicRoles.Agent());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller coroner = addPlayer(BasicRoles.Coroner());
        addPlayer(BasicRoles.Consort());

        nightStart();
        String lwText = sk.getName() + " is serial killer";
        p.setLastWill(lwText);

        setTarget(sk, p);
        setTarget(jan, p);
        endNight();

        assertFalse(game.getHappenings().contains(lwText));

        skipDay();

        setTarget(coroner, p);
        endNight();

        assertNotNull(p.getPlayer().getLastWill(coroner.getPlayer()));
    }

    public void testDeadPlayerSettingLastWill() {
        Controller p = addPlayer(BasicRoles.Agent());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Janitor());
        addPlayer(BasicRoles.Coroner());

        nightStart();

        setTarget(sk, p);
        endNight();

        try{
            p.setLastWill("hi");
            fail();
        }catch(IllegalActionException e){
        }
    }

    public void testTeamLastWillRegularDeath() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller listener = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        setTeamRule(Setup.TOWN_C, FactionModifierName.LAST_WILL, true);

        nightStart();

        try{
            sk.setTeamLastWill("failure");
            fail();
        }catch(IllegalActionException e){
        }

        fodder.setTeamLastWill("help me");
        setTarget(sk, fodder);
        endNight();

        partialContains(listener, "help me");
    }

    public void testTeamLastWillAssassinated() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller listener = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Witch());

        setTeamRule(Setup.TOWN_C, FactionModifierName.LAST_WILL, true);

        dayStart();

        fodder.setTeamLastWill("help me");
        doDayAction(assassin, fodder);

        partialContains(listener, "help me");
    }

    public void testTeamLastWillPoisoned() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller listener = addPlayer(BasicRoles.Citizen());
        Controller pois = addPlayer(BasicRoles.Poisoner());
        addPlayer(BasicRoles.Witch());

        setTeamRule(Setup.TOWN_C, FactionModifierName.LAST_WILL, true);

        nightStart();

        setTarget(pois, fodder);
        endNight();

        fodder.setTeamLastWill("help me");
        skipDay();

        partialContains(listener, "help me");
    }

    public void testTeamLastWillLynched() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller listener = addPlayer(BasicRoles.Citizen());
        Controller pois = addPlayer(BasicRoles.Poisoner());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTeamRule(Setup.TOWN_C, FactionModifierName.LAST_WILL, true);

        dayStart();

        fodder.setTeamLastWill("help me");
        voteOut(fodder, listener, pois, witch);

        partialContains(listener, "help me");
    }
}

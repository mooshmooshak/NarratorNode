package integration.logic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.AbilityList;
import game.abilities.Baker;
import game.abilities.BreadAbility;
import game.abilities.Burn;
import game.abilities.Commuter;
import game.abilities.Douse;
import game.abilities.Driver;
import game.abilities.DrugDealer;
import game.abilities.ElectroManiac;
import game.abilities.FactionKill;
import game.abilities.FactionSend;
import game.abilities.Framer;
import game.abilities.GameAbility;
import game.abilities.Hidden;
import game.abilities.JailCreate;
import game.abilities.Marshall;
import game.abilities.Mayor;
import game.abilities.Operator;
import game.abilities.Punch;
import game.abilities.Puppet;
import game.abilities.Spy;
import game.abilities.Survivor;
import game.abilities.Tailor;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.abilities.Vote;
import game.abilities.Witch;
import game.abilities.support.Gun;
import game.abilities.support.Vest;
import game.abilities.util.AbilityUtil;
import game.ai.Brain;
import game.ai.Computer;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.ChatMessage;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.EventLog;
import game.event.Feedback;
import game.event.Message;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.InvalidSetupHiddenCombination;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.TestException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.listeners.NarratorListener;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.RoleAssigner;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.logic.templates.HTMLDecoder;
import game.logic.templates.NativeControllerGenerator;
import game.logic.templates.NativeSetupController;
import game.logic.templates.PartialListener;
import game.setups.Setup;
import json.JSONObject;
import junit.framework.AssertionFailedError;
import junit.framework.ComparisonFailure;
import junit.framework.TestCase;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.GameModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import nnode.Lobby;
import services.FactionModifierService;
import services.FactionRoleModifierService;
import services.GameService;
import services.SetupHiddenService;
import services.SetupService;
import util.RepeatabilityTest;
import util.TestChatUtil;
import util.Util;
import util.game.HiddenUtil;
import util.game.VoteUtil;
import util.models.RoleTestUtil;

public abstract class SuperTest extends TestCase {

    public static Random random = new Random();

    protected static final AbilityType SEND = FactionSend.abilityType;
    protected static final AbilityType KILL = FactionKill.abilityType;
    protected static final AbilityType GUN = Vigilante.abilityType;
    protected static final AbilityType ARMOR = Survivor.abilityType;

    public SuperTest(String name) {
        super(name);
    }

    public void roleInit() {
    }

    public static Game game;
    public static Setup setup;
    public static RoleAssigner roleAssigner; // used for sleepwalker tests
    public static NativeControllerGenerator generator;
    public static HashMap<Player, Controller> controllerMap = new HashMap<>();

    public static boolean printHappenings = false;
    public static boolean BrainEndGame = true;
    public static boolean checkEnclosingChats = true;

    @Override
    public void runTest() throws Throwable {
        nativeTest();

        SuperTest.checkEnclosingChats = true;
        SuperTest.skipTearDown = false;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        SuperTest.generator = new NativeControllerGenerator();
        newNarrator();
    }

    private void nativeTest() throws Throwable {
        printHappenings = true;
        try{
            roleInit();
            Brain brain = new Brain(game, new Random().setSeed(0));
            super.runTest();
            if(game.isStarted() && BrainEndGame){
                List<NarratorListener> listeners = game.getListeners();
                for(NarratorListener listener: listeners)
                    if(listener instanceof Lobby)
                        game.removeListener(listener);
                brain.endGame();
            }
            BrainEndGame = true;
        }catch(NarratorException e){
            if(printHappenings)
                System.err.println(game.getHappenings());
            throw e;
        }catch(TestException e){
            e.printStackTrace();
            // writeToFile("log1.tx", n);
            throw e;
        }catch(Exception | Error e){
            TestChatUtil.reset();
            e.printStackTrace();
            if(printHappenings)
                System.err.println(game.getHappenings());
            throw e;
        }

        if(!skipTearDown && game.isStarted())
            RepeatabilityTest.runRepeatabilityTest();
    }

    public static HashMap<String, HashMap<String, Integer>> charges;
    public static NativeSetupController sEditor;

    public static void newNarrator() {
        generator.reset();

        sEditor = generator.getSetupController();
        game = generator.getNarrator();
        setup = game.setup;
        if(checkEnclosingChats)
            game.addListener(enclosingChatChecker());
        BasicRoles.setup = setup;
        SetupService.loadDefault(setup);
        TestChatUtil.reset();

        key = 1;

        game.setSeed(0);
        game.setValue(SetupModifierName.CHARGE_VARIABILITY, 2);
        game.setValue(GameModifierName.CHAT_ROLES, true);
        charges = null;
        controllerMap.clear();
        game.addListener(new PartialListener() {
            private boolean ended = false;

            @Override
            public void onGameEnd() {
                if(ended)
                    throw new NarratorException("Game already ended!!");
                ended = true;
            }

            @Override
            public void onGameStart() {
                for(Player player: game.players){
                    TestChatUtil.addPlayer(player);
                }
            }

            @Override
            public void onAnnouncement(Message announcement) {
                for(Player player: TestChatUtil.getPlayers()){
                    onMessageReceive(player, announcement);
                }
            }

            @Override
            public void onMessageReceive(Player player, Message message) {
                TestChatUtil.onMessageReceive(player, message);
            }

            @Override
            public void onWarningReceive(Player player, Message warning) {
                TestChatUtil.onWarningReceive(player, warning);
            }
        });
    }

    public static void checkEventEncapsulation() {
        for(Player p: game.players){
            for(Message playerEvent: p.getEvents()){
                for(String chat: playerEvent.getEnclosingChats(game, p)){
                    if(!mapsToEvent(p, chat) && !chat.equals(Feedback.CHAT_NAME)){
                        System.err.println(playerEvent);
                        System.err.println(Arrays.asList(playerEvent.getEnclosingChats(game, p)));
                        System.err.println(p.getChats());
                        mapsToEvent(p, chat);
                        playerEvent.getEnclosingChats(game, p);
                        p.getChats();
                        p.getEvents();
                        throw new TestException(p.getID());
                    }
                }
            }
        }
    }

    private static boolean mapsToEvent(Player p, String s) {
        for(EventLog el: p.getChats()){
            if(el.getName().equals(s))
                return true;
            if(el.getName().startsWith("Day ") && s.startsWith("Day "))
                return true;
        }
        return false;
    }

    private static PartialListener enclosingChatChecker() {
        return new PartialListener() {
            @Override
            public void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList events) {
                checkEventEncapsulation();
            }

            @Override
            public void onDayPhaseStart(PlayerList newDead) {
                checkEventEncapsulation();
            }
        };
    }

    public static boolean skipTearDown = false;

    @Override
    public void tearDown() throws Exception {
        SuperTest.charges = null;
        if(generator != null)
            generator.cleanup();
        super.tearDown();
    }

    public static int key = 1;

    static String getSlaveName() {
        return Computer.NAME + Computer.toLetter(key++);
    }

    protected Player addPlayer(String name) {
        Controller c = generator.getActionController(name);
        Player player = game.getPlayerByID(name);

        controllerMap.put(player, c);
        return player;
    }

    protected Controller addPlayer() {
        return addPlayer(getSlaveName());
    }

    protected Controller addPlayer(FactionRole factionRole) {
        String name = factionRole.role.getName().replaceAll(" ", "") + Computer.toLetter(key++);
        Controller c = generator.getActionController(name);
        Player p = game.getPlayerByID(name);
        p.rolePrefer(factionRole.role);
        p.teamPrefer(factionRole.faction);
        addSetupHidden(HiddenUtil.createHiddenSingle(setup, factionRole));
        controllerMap.put(p, c);
        return c;
    }

    protected ControllerList addPlayer(FactionRole role, int count) {
        ControllerList pl = new ControllerList();
        for(int i = 0; i < count; i++)
            pl.add(addPlayer(role));
        return pl;
    }

    protected void addPlayer(Hidden rt, int count) {
        for(int i = 0; i < count; i++)
            addPlayer(rt);
    }

    protected Player addPlayer(Hidden hidden) {
        String name = getSlaveName();
        Controller c = generator.getActionController(name);
        Player p = game.getPlayerByID(name);
        addSetupHidden(hidden);
        for(Faction faction: hidden.getFactions())
            p.teamPrefer(faction);

        for(FactionRole factionRole: hidden.getAllFactionRoles())
            p.rolePrefer(factionRole.role);

        controllerMap.put(p, c);
        return p;
    }

    public void setName(Controller c1, String string) {
        c1.setName(string);
    }

    protected void addRole(FactionRole role) {
        Hidden hidden = HiddenUtil.createHiddenSingle(setup, role);
        addSetupHidden(hidden);
    }

    protected SetupHidden addSetupHidden(Hidden r) {
        return SetupHiddenService.addSetupHidden(r, false, false, 0, Setup.MAX_PLAYER_COUNT);
    }

    protected void addRole(Hidden hidden, int quantity) {
        for(int i = 0; i < quantity; i++)
            addSetupHidden(hidden);
    }

    protected void addRole(FactionRole member, int quantity) {
        for(int i = 0; i < quantity; i++)
            addSetupHidden(HiddenUtil.createHiddenSingle(setup, member));
    }

    protected void deleteRole(Role role) {
        sEditor.deleteRole(role);
    }

    protected void deleteHidden(Hidden hidden) {
        sEditor.deleteHidden(hidden);
    }

    protected void removeSetupHidden(Hidden hidden) {
        game.removeSetupHidden(hidden);
    }

    protected void endNight(Controller... p) {
        for(Controller x: p){
            if(!x.getPlayer().endedNight())
                x.endNight(Constants.ALL_TIME_LEFT);
        }
    }

    protected void cancelEndNight(Controller r) {
        r.cancelEndNight(Constants.ALL_TIME_LEFT);
    }

    protected void setTarget(Controller person) {
        if(!game.isStarted())
            nightStart();
        AbilityType ability;
        if(person.is(Burn.abilityType))
            ability = Burn.abilityType;
        else
            ability = person.getPlayer().getRoleAbilities().get(0).getAbilityType();
        setTarget(person, ability, (String) null);
    }

    protected void setTarget(Controller person, Controller target) {
        Player player = person.getPlayer();
        AbilityType abilityType = null;
        if(!game.isStarted())
            nightStart();
        if(person.is(Baker.abilityType))
            abilityType = BreadAbility.abilityType;
        else if(person.is(Douse.abilityType))
            abilityType = Douse.abilityType;
        else{
            String action;
            for(GameAbility ability: person.getPlayer().gameRole._abilities){
                action = ability.getCommand();
                if(action == null)
                    continue;
                abilityType = ability.getAbilityType();
                break;
            }
            if(abilityType == null)
                abilityType = player.gameRole._abilities.get(0).getAbilityType();
            action = person.getPlayer().gameRole._abilities.get(0).getCommand();
        }
        player.setNightTarget(new Action(player, abilityType, target.getPlayer()));
    }

    protected void setTarget(Controller person, Controller target, AbilityType abilityType) {
        Action action = new Action(person.getPlayer(), abilityType, new LinkedList<>(),
                ControllerList.ToPlayerList(game, target));
        setTarget(action);
    }

    protected void setTarget(Controller person, Controller target1, Controller target2) {
        if(!game.isStarted())
            nightStart();
        Player submitter = person.getPlayer();
        AbilityType abilityNumber = submitter.getRoleAbilities().get(0).getAbilityType();
        Action action = new Action(submitter, abilityNumber, new LinkedList<>(),
                ControllerList.ToPlayerList(game, target1, target2));
        setTarget(action);
    }

    protected void setTarget(Controller person, ControllerList targets, AbilityType ability) {
        setTarget(person, ability, null, null, targets.toArray());
    }

    protected void setTarget(Controller person, AbilityType abilityType, String option, Controller... targets) {
        setTarget(person, abilityType, option, null, targets);
    }

    protected void setTarget(Controller person, AbilityType abilityType, String option, String option2,
            ControllerList targets) {
        setTarget(person, abilityType, option, option2, targets.toArray());
    }

    protected void setTarget(Controller person, AbilityType abilityType, String option, String option2,
            Controller... targets) {
        List<String> args = Util.toStringList(option, option2);
        Action action = new Action(person.getPlayer(), abilityType, args, ControllerList.ToPlayerList(game, targets));
        setTarget(action);
    }

    protected static void setTarget(Action action) {
        if(!game.isStarted())
            nightStart();
        Player person = action.owner;
        Player p = person.getPlayer();
        GameAbility ability = p.getAbility(action.abilityType);
        person.setNightTarget(action);

        Action lastAction = p.getActions().getActions().get(p.getActions().size() - 1);
        ability.parseCommand(p, Constants.ALL_TIME_LEFT, ability.getCommandParts(lastAction));
    }

    protected static void cancelAction(Controller person, int index) {
        person.cancelAction(index, Constants.ALL_TIME_LEFT);
    }

    protected void clearTargets(Controller person) {
        person.clearTargets();
    }

    protected void frame(Controller framer, Controller framed, String team) {
        if(!game.isStarted())
            nightStart();
        framer.setNightTarget(new Action(framer.getPlayer(), Framer.abilityType, team, null, framed.getPlayer()));
    }

    protected void spy(Controller spy, String team) {
        if(!game.isStarted())
            nightStart();
        spy.setNightTarget(new Action(spy.getPlayer(), Spy.abilityType, team, (String) null));
    }

    protected void drive(Controller driver, Controller t1, Controller t2) {
        if(!game.isStarted())
            nightStart();
        setTarget(driver, Driver.abilityType, null, t1, t2);
    }

    protected void tele(Controller op, Controller... target) {
        if(!game.isStarted())
            nightStart();
        setTarget(op, Operator.abilityType, null, null, target);
    }

    protected void witch(Controller witch, Controller target, Controller victimTarget) {
        if(!game.isStarted())
            nightStart();
        setTarget(witch, Witch.abilityType, null, target, victimTarget);
    }

    protected void jail(Controller jailor, Controller f1) {
        if(!game.isStarted())
            dayStart();
        doDayAction(jailor, JailCreate.abilityType, f1);
    }

    protected void arch(Controller arc, Controller... targets) {
        if(!game.isStarted())
            dayStart();
        doDayAction(arc, targets);
    }

    protected void drug(Controller dd, Controller arch, String drug) {
        setTarget(dd, DrugDealer.abilityType, drug, arch);
    }

    protected void tailor(Controller tailor, Controller target, Controller template) {
        if(!game.isStarted())
            nightStart();
        setTarget(tailor, Tailor.abilityType, template.getColor(), template.getPlayer().getRoleName(), target);
    }

    protected void vest(Controller p) {
        if(!game.isStarted())
            nightStart();
        p.setNightTarget(new Action(p.getPlayer(), Survivor.abilityType));
    }

    protected void burn(Controller arso) {
        if(!game.isStarted())
            nightStart();
        if(game.isNight()){
            arso.setNightTarget(new Action(arso.getPlayer(), Burn.abilityType));
            return;
        }

        Player player = arso.getPlayer();
        AbilityType abilityType = Burn.abilityType;
        Action action = new Action(player, abilityType, null, null, new PlayerList());
        arso.doDayAction(action);
    }

    protected void commute(Controller commuter) {
        if(!game.isStarted())
            nightStart();
        commuter.setNightTarget(new Action(commuter.getPlayer(), Commuter.abilityType));
    }

    protected void alert(Controller veteran) {
        if(!game.isStarted())
            nightStart();
        veteran.setNightTarget(new Action(veteran.getPlayer(), Veteran.abilityType));
    }

    protected void reveal(Controller mayor) {
        if(!game.isStarted())
            dayStart();

        Player player = mayor.getPlayer();
        AbilityType abilityType = Mayor.abilityType;
        Action action = new Action(player, abilityType, null, null, new PlayerList());
        mayor.doDayAction(action);
    }

    protected void order(Controller marshall) {
        if(!game.isStarted())
            dayStart();

        Player player = marshall.getPlayer();
        AbilityType abilityType = Marshall.abilityType;
        Action action = new Action(player, abilityType, null, null, new PlayerList());
        marshall.doDayAction(action);
    }

    protected void skipDay() {
        for(Player p: game.getAllPlayers()){
            if(!game.isDay())
                return;
            if(!p.isPuppeted() && p.isAlive())
                skipVote(p);
            if(p.hasPuppets()){
                for(Player puppet: p.getPuppets())
                    ventSkipVote(p, puppet);
            }
        }
    }

    protected void lynch(Controller target, ControllerList voters) {
        voteOut(target, voters.toArray());
    }

    protected void voteOut(Controller target, Controller... voters) {
        if(!game.isStarted())
            dayStart();
        for(Controller voter: voters){
            if(game.isDay())
                vote(voter, target);
            else
                break;
        }
    }

    protected void endPhase() {
        game.endPhase(1);
    }

    protected void endNight() {
        for(Player p: game.getNightWaitingOn())
            controllerMap.get(p).endNight(Constants.ALL_TIME_LEFT);
    }

    protected void endDay() {
        game.forceEndDay(1);
    }

    protected void nextNight() {
        endNight();
        skipDay();
    }

    protected void nextDay() {
        skipDay();
        endNight();
    }

    public static List<Feedback> getFeedback(Controller p, int day) {
        return TestChatUtil.getFeedbackMessages(p.getPlayer(), day);
    }

    public static void assertPresent(Optional<?> option) {
        assertTrue(option.isPresent());
    }

    public static void assertEmpty(Optional<?> option) {
        assertFalse(option.isPresent());
    }

    public static void assertEmpty(PlayerList players) {
        assertTrue(players.isEmpty());
    }

    public static void assertEquals(Object s1, Object s2) {
        try{
            junit.framework.TestCase.assertEquals(s1, s2);
        }catch(AssertionFailedError e){
            if(printHappenings)
                System.out.println(game.getHappenings());
            if(s1 == null)
                s1 = "null";
            if(s2 == null)
                s2 = "null";
            throw new ComparisonFailure(Long.toString(game.getSeed()), s1.toString(), s2.toString());
        }
    }

    public static void assertEquals(Game n1, Game n2) {
        try{
            junit.framework.TestCase.assertEquals(n1, n2);
        }catch(AssertionFailedError e){
            writeToFile("input.txt", n1);
            writeToFile("output.txt", n2);
            // System.out.println(n1.getHappenings());
            // System.out.println("#");
            // System.out.println(n2.getPrivateEvents(false));
            throw new ComparisonFailure("", n1.toString(), n2.toString());
        }
    }

    public static void assertTrue(boolean b) {
        try{
            junit.framework.TestCase.assertTrue(b);
        }catch(AssertionFailedError e){
            if(printHappenings)
                System.out.println(game.getHappenings());
            throw new AssertionFailedError();
        }
    }

    @SafeVarargs
    public static <T> void assertContains(Collection<T> list, T... objects) {
        for(Object o: objects){
            try{
                assertTrue(list.contains(o));
            }catch(AssertionFailedError e){
                throw new AssertionFailedError("Missing " + o.toString());
            }
        }
    }

    public static void assertContains(ControllerList list, Controller... players) {
        for(Controller p: players)
            assertTrue(list.contains(p));
    }

    public static void assertContains(PlayerList list, Player... players) {
        for(Player p: players)
            assertTrue(list.contains(p));
    }

    public static void assertFalse(boolean b) {
        try{
            junit.framework.TestCase.assertFalse(b);
        }catch(AssertionFailedError e){
            System.out.print(game.getHappenings());
            throw new AssertionFailedError();
        }
    }

    public static void fail(long t) {
        fail(Long.toString(t));
    }

    public static void fail(String q) {
        if(printHappenings)
            System.out.print(game.getHappenings());
        TestCase.fail(q);
    }

    public static void fail() {
        fail("");
    }

    public static void assertEquals(int expected, int actual) {
        try{
            junit.framework.TestCase.assertEquals(expected, actual);
        }catch(AssertionFailedError e){
            if(printHappenings)
                System.out.println(game.getHappenings());
            TestCase.assertEquals(expected, actual);
        }
    }

    public static void assertEqual(int expected, int actual) {
        assertEquals(expected, actual);
    }

    protected static void assertNotSameAbilities(Controller p, Controller q) {
        AbilityList a1 = p.getPlayer().getRoleAbilities();
        AbilityList a2 = q.getPlayer().getRoleAbilities();
        assertFalse(AbilityUtil.isEqualContent(a1, a2));
    }

    protected static void isWinner(Controller... ps) {
        for(Controller p: ps){
            try{
                assertTrue(p.getPlayer().isWinner());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer().getDescription() + " is a loser!");
            }
        }
    }

    protected static void isLoser(Controller... ps) {
        for(Controller p: ps){
            try{
                assertFalse(p.getPlayer().isWinner());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer().getDescription() + " is a winner!");
            }
        }
    }

    protected static void assertNoAcceptableTargets(Controller c, AbilityType abilityType) {
        PlayerList acceptableTargets = c.getPlayer().getAcceptableTargets(abilityType);
        assertNotNull(acceptableTargets);
        assertTrue(acceptableTargets.isEmpty());
    }

    protected static void isAlive(Controller... ps) {
        for(Controller p: ps){
            try{
                assertTrue(p.getPlayer().isAlive());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer().getDescription() + " came up dead!");
            }
        }
    }

    protected static void isDead(Controller... ps) {
        for(Controller p: ps){
            try{
                assertFalse(p.getPlayer().isAlive());
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer().getDescription() + " came up alive!");
            }
        }
    }

    protected static void isInvuln(Controller c) {
        isInvuln(c, true);
    }

    protected static void isInvuln(Controller c, boolean isInvuln) {
        assertEquals(isInvuln, c.getPlayer().isInvulnerable());
    }

    protected static void assertVoteTarget(Controller voteTargetC, Controller voterC) {
        Player voter = voterC.getPlayer();

        Player voteTarget;
        if(voteTargetC == null)
            voteTarget = null;
        else if(voteTargetC == game.skipper)
            voteTarget = game.skipper;
        else
            voteTarget = voteTargetC.getPlayer();

        assertEquals(voteTarget, game.voteSystem.getVoteTargets(voter).getFirst());
    }

    protected static void assertVoteCount(int count, Controller voted) {
        PlayerList voters = game.voteSystem.getVotersOfPlayer(voted.getPlayer());
        int actualVoteCount = 0;
        for(Player player: voters)
            actualVoteCount += player.getVotePower();
        assertEquals(count, actualVoteCount);
    }

    protected static void assertVotePower(int count, Controller p) {
        assertEquals(count, p.getPlayer().getVotePower());
    }

    protected static void hasSuits(Controller... ps) {
        for(Controller p: ps){
            try{
                assertStatus(p, Tailor.abilityType);
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer().getDescription() + " doesn't have suits!");
            }
        }
    }

    protected static void isCharged(Controller... ps) {
        for(Controller p: ps){
            try{
                assertStatus(p, ElectroManiac.abilityType);
            }catch(AssertionFailedError e){
                throw new AssertionFailedError(p.getPlayer().getDescription() + " isn't charged!");
            }
        }
    }

    protected ArrayList<Message> addAnnouncementListener() {
        AnnouncementListener aListener = new AnnouncementListener();
        game.addListener(aListener);
        return aListener.announcements;
    }

    class AnnouncementListener extends PartialListener {
        public ArrayList<Message> announcements;

        public AnnouncementListener() {
            announcements = new ArrayList<>();
        }

        @Override
        public void onAnnouncement(Message nl) {
            announcements.add(nl);
        }
    }

    protected static void assertGameOver() {
        assertFalse(game.isInProgress());

    }

    protected static void dayStart() {
        sEditor.changeRule(SetupModifierName.DAY_START, Game.DAY_START);
        startGame();
    }

    protected void startDay() {
        dayStart();
    }

    protected void startNight() {
        nightStart();
    }

    protected static void assertBadGameSettings() {
        try{
            GameService.internalStart(game);
            fail();
        }catch(IllegalGameSettingsException | IllegalRoleCombinationException | InvalidSetupHiddenCombination e){
            e.toString();
        }
    }

    protected static void nightStart() {
        sEditor.changeRule(SetupModifierName.DAY_START, Game.NIGHT_START);
        startGame();
    }

    private static void startGame() {
        GameService.internalStart(game);

        collectCharges();
    }

    private static void collectCharges() {
        charges = new HashMap<>();
        HashMap<String, Integer> subChargeMap;
        for(Player p: game.getAllPlayers()){
            subChargeMap = new HashMap<>();
            for(GameAbility a: p.getRoleAbilities()){
                subChargeMap.put(a.getCommand(), a.getPerceivedCharges());
            }
            charges.put(p.getID(), subChargeMap);
        }
    }

    protected void simulationTest(long t) {
        Random r = new Random();
        r.setSeed(t);
        Brain b = new Brain(game, r);
        b.talkingEnabled = false;
        b.setNarrator(game);
        try{
            b.endGame();
        }catch(UnknownTeamException e){
            e.printStackTrace();
            fail(t + "");
        }
    }

    public static void writeToFile(String s, Game n) {
        try{
            FileWriter in = new FileWriter(new File(s));
            // in.write(CodeWriter.getCode(n));
            // in.write(getCommandsList(n));
            in.write(n.getHappenings());
            // in.write(n.getRandom().getLog());
            in.close();
        }catch(IOException | NullPointerException e){
            e.printStackTrace();
        }
    }

    protected void partialContains(String partialFeedback) {
        for(Player p: game.players)
            partialContains(p, partialFeedback, 1);
    }

    protected static void partialContains(Controller p, String partialFeedback) {
        partialContains(p, partialFeedback, 1);
    }

    protected static void partialContains(Controller controller, String partialFeedback, int count) {
        Player player = controller.getPlayer();
        List<Message> messages = TestChatUtil.getMessages(player);
        messages = TestChatUtil.filterByText(messages, player, partialFeedback);
        assertEquals(count, messages.size());
    }

    protected static void partialContains(List<String> comm, String partialFeedback, int count) {
        assertEquals(count, Util.partialContains(comm, partialFeedback));
    }

    protected static void partialIncludes(List<String> list, String partialFeedback) {
        partialContains(list, partialFeedback);
    }

    protected static <T> void assertNotContains(Collection<T> collection, T element) {
        assertFalse(collection.contains(element));
    }

    protected static void partialExcludes(Controller controller, String partialFeedback) {
        Player player = controller.getPlayer();
        List<Message> messages = TestChatUtil.getMessages(player);
        messages = TestChatUtil.filterByText(messages, player, partialFeedback);
        assertTrue(messages.isEmpty());
    }

    protected void partialExcludes(List<String> comm, String partialFeedback) {
        partialContains(comm, partialFeedback, 0);
    }

    protected static void partialContains(List<String> comm, String partialFeedback) {
        partialContains(comm, partialFeedback, 1);
    }

    protected Action getVestAction(Controller p) {
        return new Action(p.getPlayer(), Survivor.abilityType);
    }

    protected void shoot(Controller shooter, Controller target) {
        if(!game.isStarted())
            nightStart();
        Player shooterPlayer = shooter.getPlayer();
        Player targetPlayer = target.getPlayer();
        Action action = new Action(shooterPlayer, Vigilante.abilityType, targetPlayer);
        if(game.isDay())
            shooter.doDayAction(action);
        else
            shooter.setNightTarget(action);
    }

    protected void bread(Controller breader, Controller target) {
        breader.setNightTarget(new Action(breader.getPlayer(), BreadAbility.abilityType, target.getPlayer()));
    }

    public void send(Controller sender, Controller... target) {
        if(!game.isStarted())
            nightStart();
        sender.setNightTarget(new Action(sender.getPlayer(), FactionSend.abilityType,
                ControllerList.ToPlayerList(game, target).getArray()));
    }

    public void mafKill(Controller sender, Controller target) {
        if(!game.isStarted())
            nightStart();
        sender.setNightTarget(new Action(sender.getPlayer(), FactionKill.abilityType, target.getPlayer()));
    }

    protected void electrify(Controller electro, Controller... p) {
        for(Controller target: p){
            setTarget(electro, target);
            nextNight();
        }
    }

    protected void command(Controller p, String... commands) {
        StringBuilder sb = new StringBuilder();
        for(String c: commands){
            sb.append(c);
            sb.append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        new CommandHandler(game).command(p.getPlayer(), sb.toString(), p.getName());
    }

    public void setAllies(FactionRole m1, FactionRole m2) {
        sEditor.setAllies(m1.getColor(), m2.getColor());
    }

    public void setEnemies(FactionRole m1, FactionRole m2) {
        sEditor.setEnemies(m1.getColor(), m2.getColor());
    }

    public void addSheriffDetectable(FactionRole m1, FactionRole m2) {
        this.addSheriffDetectable(m1.getColor(), m2.getColor());
    }

    public void addSheriffDetectable(String t1, String t2) {
        sEditor.addSheriffDetectable(t1, t2);
    }

    public void removeSheriffDetectable(String t1, String t2) {
        sEditor.removeSheriffDetectable(t1, t2);
    }

    public static void editRule(SetupModifierName ruleArr, boolean b) {
        sEditor.changeRule(ruleArr, b);
    }

    public static void editRule(SetupModifierName ruleArr, int i) {
        sEditor.changeRule(ruleArr, i);
    }

    public static void modifyRole(FactionRole factionModifier, RoleModifierName modifierID, int val) {
        sEditor.upsertRoleModifier(factionModifier.role, modifierID, val);
    }

    public static void modifyRole(FactionRole m, AbilityModifierName modifierID, int val) {
        modifyFactionRoleAbility(m, modifierID, RoleTestUtil.getFirstAbilityType(m.role), val);
    }

    public static void modifyRole(FactionRole m, RoleModifierName modifierID, boolean val) {
        sEditor.upsertRoleModifier(m.role, modifierID, val);
    }

    // rename to modifyAbility
    public static void modifyRoleAbility(Role role, AbilityModifierName modifierID, AbilityType abilityType,
            boolean val) {
        sEditor.upsertRoleAbilityModifier(role, modifierID, abilityType, val);
    }

    public static void modifyFactionRoleAbility(FactionRole factionRole, AbilityModifierName modifierID,
            AbilityType abilityType, int val) {
        sEditor.upsertRoleAbilityModifier(factionRole.role, modifierID, abilityType, val);
    }

    public static void modifyAbilitySelfTarget(FactionRole factionRole, boolean val) {
        AbilityType firstClass = RoleTestUtil.getFirstAbilityType(factionRole.role);
        modifyAbilitySelfTarget(factionRole, firstClass, val);
    }

    public static void modifyAbilitySelfTarget(FactionRole factionRole, AbilityType abilityClass, boolean val) {
        FactionRoleModifierService.addModifier(factionRole, AbilityModifierName.SELF_TARGET, abilityClass, val);
    }

    public static void modifyAbilityCharges(FactionRole m, int val) {
        AbilityType abilityType = RoleTestUtil.getFirstAbilityType(m.role);
        sEditor.upsertRoleAbilityModifier(m.role, AbilityModifierName.CHARGES, abilityType, val);
    }

    public static void modifyAbilityCharges(FactionRole factionRole, AbilityType abilityType, int val) {
        sEditor.upsertRoleAbilityModifier(factionRole.role, AbilityModifierName.CHARGES, abilityType, val);
    }

    public static void modifyAbilityCooldown(FactionRole m, int val) {
        modifyFactionRoleAbility(m, AbilityModifierName.COOLDOWN, RoleTestUtil.getFirstAbilityType(m.role), val);
    }

    public static void modifyAbilityCooldown(FactionRole m, AbilityType abilityType, int val) {
        modifyFactionRoleAbility(m, AbilityModifierName.COOLDOWN, abilityType, val);
    }

    public static void modifyRole(Role role, AbilityModifierName modifierID, boolean val) {
        if(role.abilityMap.size() != 1)
            throw new Error();

        modifyRoleAbility(role, modifierID, RoleTestUtil.getFirstAbilityType(role), val);
    }

    public void addTeamAbility(String color, AbilityType abilityType) {
        sEditor.addTeamAbility(color, abilityType);
    }

    public void removeTeamAbility(String color, AbilityType abilityType) {
        sEditor.removeTeamAbility(color, abilityType);
    }

    public void modifyTeamAbility(String color, AbilityModifierName modifier, AbilityType abilityType, boolean b) {
        sEditor.upsertFactionAbilityModifier(color, modifier, abilityType, b);
    }

    public void modifyTeamAbility(String color, AbilityModifierName modifier, AbilityType abilityType, int i) {
        sEditor.upsertFactionAbilityModifier(color, modifier, abilityType, i);
    }

    public static void setTeamRule(String color, FactionModifierName name, int value) {
        Faction faction = setup.getFactionByColor(color);
        FactionModifierService.upsertModifier(faction, name, value);
    }

    public void setTeamRule(Faction faction, FactionModifierName name, int value) {
        FactionModifierService.upsertModifier(faction, name, value);
    }

    public void setTeamRule(Faction faction, FactionModifierName name, boolean value) {
        FactionModifierService.upsertModifier(faction, name, value);
    }

    public static void setTeamRule(String color, FactionModifierName name, boolean value) {
        Faction faction = setup.getFactionByColor(color);
        FactionModifierService.upsertModifier(faction, name, value);
    }

    public void assertIsDay() {
        assertTrue(game.isDay());
    }

    public void assertIsNight() {
        assertTrue(game.isNight());
    }

    public void assertInProgress() {
        assertTrue(game.isInProgress());
    }

    public void assertDayActionSize(int i) {
        assertEquals(i, game.actionStack.size());
    }

    public void vote(Controller voter, Controller... targets) {
        if(!game.isStarted())
            dayStart();
        Player voterPlayer = voter.getPlayer();
        PlayerList targetPlayers = ControllerList.ToPlayerList(game, targets);
        voter.doDayAction(VoteUtil.voteAction(voterPlayer, targetPlayers));
    }

    public void punch(Controller voter, Controller... targets) {
        if(!game.isStarted())
            dayStart();
        Player voterPlayer = voter.getPlayer();
        PlayerList targetPlayers = ControllerList.ToPlayerList(game, targets);
        voter.doDayAction(new Action(voterPlayer, Punch.abilityType, null, (String) null, targetPlayers));
    }

    public void skipVote(Controller... voters) {
        if(!game.isStarted())
            dayStart();
        for(Controller voter: voters)
            voter.doDayAction(VoteUtil.skipAction(voter.getPlayer(), Constants.ALL_TIME_LEFT));
    }

    public void unvote(Controller voter) {
        if(!game.isStarted())
            dayStart();
        voter.cancelAction(0, Constants.ALL_TIME_LEFT);
    }

    public void ventVote(Controller vent, Controller puppet, Controller... targets) {
        if(!game.isStarted())
            dayStart();
        Player ventPlayer = vent.getPlayer();
        PlayerList playerTargets = ControllerList.ToPlayerList(game, targets);
        playerTargets.add(0, puppet.getPlayer());
        AbilityType abilityType = Puppet.abilityType;
        String option = Vote.COMMAND;
        Action action = new Action(ventPlayer, abilityType, option, null, playerTargets);
        vent.doDayAction(action);
    }

    public void ventPunch(Controller vent, Controller puncher, Controller target) {
        if(!game.isStarted())
            dayStart();
        Player ventPlayer = vent.getPlayer();
        PlayerList targets = ControllerList.ToPlayerList(game, puncher, target);
        AbilityType abilityType = Puppet.abilityType;
        String option = Punch.COMMAND;
        Action action = new Action(ventPlayer, abilityType, option, null, targets);
        vent.doDayAction(action);
    }

    public void ventSkipVote(Controller vent, Controller voter) {
        if(!game.isStarted())
            dayStart();
        Player ventPlayer = vent.getPlayer();
        PlayerList targets = ControllerList.ToPlayerList(game, voter, game.skipper);
        AbilityType abilityType = Puppet.abilityType;
        Action action = new Action(ventPlayer, abilityType, Vote.COMMAND, null, targets);
        vent.doDayAction(action);
    }

    public static void ventUnvote(Controller vent, Controller voter) {
        if(!game.isStarted())
            dayStart();
        Player ventPlayer = vent.getPlayer();
        int count = VoteUtil.getVentVoteActionIndex(ventPlayer, voter.getPlayer());
        cancelAction(vent, count);
    }

    public static void hasPuppets(Controller c) {
        hasPuppets(c, true);
    }

    public static void isPuppeted(Controller c) {
        isPuppeted(c, true);
    }

    public static void hasPuppets(Controller c, boolean hasPuppets) {
        if(hasPuppets)
            assertTrue(c.getPlayer().hasPuppets());
        else
            assertFalse(c.getPlayer().hasPuppets());
    }

    public static void isPuppeted(Controller c, boolean isPuppeted) {
        if(isPuppeted)
            assertTrue(c.getPlayer().isPuppeted());
        else
            assertFalse(c.getPlayer().isPuppeted());
    }

    public void doDayAction(Controller p, Controller... targets) {
        if(!game.isStarted())
            dayStart();
        AbilityType abilityType = p.getPlayer().getRoleAbilities().getDayAbilities().get(0).getAbilityType();
        doDayAction(p, abilityType, targets);
    }

    public void doDayAction(Controller p, AbilityType abilityType, Controller... targets) {
        if(!game.isStarted())
            dayStart();
        Player player = p.getPlayer();
        PlayerList playerList = ControllerList.ToPlayerList(game, targets);
        Action action = new Action(player, abilityType, null, null, playerList);
        p.doDayAction(action);
    }

    public void doDayAction(Controller p, AbilityType abilityType, String option, Controller... targets) {
        if(!game.isStarted())
            dayStart();
        Player player = p.getPlayer();
        PlayerList playerList = ControllerList.ToPlayerList(game, targets);
        Action action = new Action(player, abilityType, option, null, playerList);
        p.doDayAction(action);
    }

    public ChatMessage say(Controller c1, String messageText, String key) {
        int preCommandSize = game.getCommands().size();
        ChatMessage message = c1.say(messageText, key, Optional.empty(), new JSONObject());
        assertEquals(preCommandSize + 1, game.getCommands().size());
        return message;
    }

    public void assertFactionController(Controller maf, String c) {
        assertFactionController(maf, game.getFaction(c), FactionKill.abilityType);
    }

    public void assertFactionController(Controller maf, String c, AbilityType abilityType) {
        assertFactionController(maf, game.getFaction(c), abilityType);
    }

    public void assertFactionController(Controller maf, GameFaction team) {
        assertFactionController(maf, team, FactionKill.abilityType);
    }

    public void assertFactionController(Controller maf, GameFaction team, AbilityType abilityType) {
        assertEquals(maf.getPlayer(), team.getCurrentController(abilityType));
    }

    public static void assertStatus(Controller c, AbilityType abilityType) {
        assertStatus(c, abilityType, true);
    }

    public static void assertStatus(Controller c, AbilityType abilityType, boolean hasStatus) {
        Player p = c.getPlayer();
        switch (abilityType) {
        case Poisoner:
            assertEquals(hasStatus, p.isPoisoned());
            return;
        case CultLeader:
            assertEquals(hasStatus, p.isCulted());
            return;
        case Douse:
            assertEquals(hasStatus, p.isDoused());
            return;
        case Blackmailer:
            assertEquals(hasStatus, p.isSilenced());
            assertEquals(hasStatus, p.isDisenfranchised());
            return;
        case Janitor:
            if(p.isDead())
                assertEquals(hasStatus, p.getDeathType().isHidden());
            else
                assertEquals(hasStatus, !p.getCleaners().isEmpty());
            return;
        case JailCreate:
            assertEquals(hasStatus, p.isJailed());
            return;
        case ElectroManiac:
            assertEquals(hasStatus, p.isCharged());
            return;
        case Tailor:
            assertEquals(hasStatus, p.hasSuits());
            return;
        default:
            fail("unknown status");
        }
    }

    public void assertAttackSize(int size, Controller p) {
        assertEquals(size, p.getPlayer().getDeathType().size());
    }

    public void assertActionSize(int size, Controller p) {
        assertEquals(size, p.getPlayer().getActions().size());
    }

    public void assertUseableBreadCount(int breadNumber, Controller breaded) {
        assertUseableBreadCount(breadNumber, breaded, BreadAbility.class);
    }

    public void assertUseableBreadCount(int breadNumber, Controller breaded, Class<? extends GameAbility> abilityType) {
        Player player = breaded.getPlayer();
        assertEquals(breadNumber, BreadAbility.getUseableBread(player));
    }

    public void assertPassableBreadCount(int breadNumber, Controller c) {
        assertEquals(breadNumber, BreadAbility.getPassableBread(c.getPlayer()));
    }

    public void assertFakeGunCount(int assertedGunCount, Controller gunHolder) {
        if(!gunHolder.getPlayer().hasAbility(Vigilante.abilityType))
            assertEquals(0, assertedGunCount);
        else
            assertEquals(assertedGunCount,
                    gunHolder.getPlayer().getAbility(Vigilante.abilityType).charges.getFakeCharges());
    }

    public void assertFakeVestCount(int assertedVestCount, Controller c2) {
        if(!c2.getPlayer().hasAbility(Survivor.abilityType))
            assertEquals(0, assertedVestCount);
        assertEquals(assertedVestCount, c2.getPlayer().getAbility(Survivor.class).getFakeCharges());
    }

    // TODO rename to perceived
    public void assertTotalVestCount(int assertedVestCount, Controller vestHolder) {
        Player vestController = vestHolder.getPlayer();
        if(!vestController.hasAbility(Survivor.abilityType))
            assertEquals(0, assertedVestCount);
        else
            assertEquals(assertedVestCount, vestController.getAbility(Survivor.class).getPerceivedCharges());
    }

    public void assertRealVestCount(int assertedVestCount, Controller vestHolder) {
        if(!vestHolder.getPlayer().hasAbility(Survivor.abilityType))
            assertEquals(0, assertedVestCount);
        assertEquals(assertedVestCount, vestHolder.getPlayer().getAbility(Survivor.class).getRealCharges());
    }

    public void assertRealGunCount(int assertedGunCount, Controller gunHolder) {
        this.assertRealChargeRemaining(assertedGunCount, gunHolder, Vigilante.abilityType);
    }

    public void assertTotalGunCount(int gunNumber, Controller gunned) {
        Vigilante vigiAbility = gunned.getPlayer().getAbility(Vigilante.class);
        if(vigiAbility == null)
            assertEquals(gunNumber, 0);
        else
            assertEquals(gunNumber, vigiAbility.charges.getPerceivedCharges());
    }

    public void assertPerceivedAutovestCount(int count, Controller c) {
        assertEquals(count, c.getPlayer().getPerceivedAutoVestCount());
    }

    public void assertRealChargeRemaining(int val, Controller p) {
        assertRealChargeRemaining(val, p, p.getPlayer().getFirstAbility().getAbilityType());
    }

    public void assertRealChargeRemaining(int val, Controller p, AbilityType abilityType) {
        assertEquals(val, p.getPlayer().getAbility(abilityType).getRealCharges());
    }

    public void assertPerceivedChargeRemaining(int val, Controller c, AbilityType abilityType) {
        assertEquals(val, c.getPlayer().getAbility(abilityType).getPerceivedCharges());
    }

    public void assertPerceivedChargeRemaining(int val, Controller c) {
        assertEquals(val, c.getPlayer().getFirstAbility().getPerceivedCharges());
    }

    public void assertCooldownRemaining(int val, Controller p) {
        assertEquals(val, p.getPlayer().getFirstAbility().getRemainingCooldown());
    }

    public void assertCooldownRemaining(int val, Controller p, AbilityType c) {
        assertEquals(val, p.getPlayer().getAbility(c).getRemainingCooldown());
    }

    public void assertChatKeysSize(int val, Controller p) {
        this.assertChatKeys(val, p);
    }

    public void assertChatKeys(int val, Controller p) {
        assertEquals(val, p.getChatKeys().size());
    }

    public void assertChatLogSize(int val, Controller p) {
        assertEquals(val, p.getPlayer().getChats().size());
    }

    public void assertSuited(Controller dead, Controller looksLike) {
        isDead(dead);
        DeathAnnouncement da;
        String access;
        for(Message m: game.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement){
                da = (DeathAnnouncement) m;
                if(da.dead.contains(dead.getPlayer())){
                    access = m.access(Message.PUBLIC, new HTMLDecoder());
                    assertTrue(access.contains(looksLike.getPlayer().getColor()));
                    assertTrue(access.contains(looksLike.getPlayer().getRoleName()));

                    access = m.access(Message.PRIVATE, new HTMLDecoder());
                    assertTrue(access.contains(dead.getColor()));
                    assertTrue(access.contains(dead.getPlayer().getRoleName()));
                    return;
                }
            }
        }
        fail();
    }

    protected static String gibberish;
    static final int len = 40;

    protected String gibberish() {
        gibberish = Util.getRandomString(len);
        return gibberish;
    }

    public Gun getGun(Controller shooter, int i) {
        return (Gun) shooter.getPlayer().getAbility(Vigilante.class).charges.get(i);
    }

    public Vest getVest(Controller vester, int i) {
        return (Vest) vester.getPlayer().getAbility(Survivor.class).charges.get(i);
    }
}

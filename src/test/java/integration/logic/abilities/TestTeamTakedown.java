package integration.logic.abilities;

import game.abilities.Goon;
import game.abilities.TeamTakedown;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.RoleAbilityModifierService;
import services.RoleService;

public class TestTeamTakedown extends SuperTest {

    public TestTeamTakedown(String name) {
        super(name);
    }

    public void testBasic() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "Egg", Goon.abilityType, TeamTakedown.abilityType);
        FactionRole mashM = FactionRoleService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller agent2 = addPlayer(BasicRoles.Agent());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.MAYOR_VOTE_POWER, 4);

        reveal(mayor);
        voteOut(mash, mayor);

        setTarget(agent, agent2);
        setTarget(agent2, agent);
        endNight();

        assertEquals(2, game.getDeadSize());
        assertTrue(agent.getPlayer().isDead() || agent2.getPlayer().isDead());
        assertFalse(agent.getPlayer().isDead() && agent2.getPlayer().isDead());
    }

    public void testSingle() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "Egg", Goon.abilityType, TeamTakedown.abilityType);
        FactionRole mashM = FactionRoleService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(sk, mash);
        endNight();

        isDead(mash);
        isAlive(sk);
        assertEquals(3, game.getLiveSize());
    }

    public void testDouble() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "Egg", Goon.abilityType, TeamTakedown.abilityType);
        FactionRole mashM = FactionRoleService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        addPlayer(BasicRoles.Agent());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(sk, mash);
        endNight();

        isDead(mash);
        isAlive(sk);
        assertEquals(4, game.getLiveSize());
    }

    public void testLargeTeam() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "Egg", Goon.abilityType, TeamTakedown.abilityType);
        FactionRole mashM = FactionRoleService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        addPlayer(BasicRoles.Agent(), 5);

        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(sk, mash);
        endNight();

        isDead(mash);
        isAlive(sk);
        assertEquals(6, game.getLiveSize());
    }

    public void testLargeTeamDay() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "Egg", Goon.abilityType, TeamTakedown.abilityType);
        FactionRole mashM = FactionRoleService.createFactionRole(mafia, role);

        Controller mash = addPlayer(mashM);
        addPlayer(BasicRoles.Agent(), 6);
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.MAYOR_VOTE_POWER, 6);

        reveal(mayor);
        voteOut(mash, mayor);

        endNight();

        isDead(mash);
        assertEquals(4, game.getDeadSize());
    }

    public void testHealMassSuicide() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "Egg", TeamTakedown.abilityType);
        FactionRole mashM = FactionRoleService.createFactionRole(mafia, role);

        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller cultLeader = addPlayer(mashM);
        Controller cultist1 = addPlayer(BasicRoles.Cultist());
        Controller cultist2 = addPlayer(BasicRoles.Cultist());
        Controller doc2 = addPlayer(BasicRoles.Doctor());

        voteOut(cultLeader, doc1, doc2, cultist1, cultist2);

        setTarget(doc1, cultist1);
        setTarget(doc2, cultist2);
        endNight();

        isAlive(doc1, doc2, cultist1, cultist2);
    }

    public void testNoBackToBackModifier() {
        try{
            Role role = RoleService.createRole(setup, "Egg", TeamTakedown.abilityType);
            RoleAbilityModifierService.upsert(role, TeamTakedown.abilityType, AbilityModifierName.BACK_TO_BACK, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            Role role = RoleService.createRole(setup, "Egg", TeamTakedown.abilityType);
            RoleAbilityModifierService.upsert(role, TeamTakedown.abilityType, AbilityModifierName.ZERO_WEIGHTED, false);
            fail();
        }catch(NarratorException e){
        }
    }
}

package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.Hidden;
import game.abilities.SerialKiller;
import game.abilities.Sheriff;
import game.ai.Controller;
import game.event.Announcement;
import game.event.Message;
import game.logic.GameFaction;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.SetupHidden;
import models.enums.SetupModifierName;
import services.SetupHiddenService;
import util.models.SetupHiddenTestUtil;

public class TestSheriff extends SuperTest {

    public TestSheriff(String s) {
        super(s);
    }

    // tests if sheriff is valid
    public void testSheriffCheck() {
        // serial killer test
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(Hidden.TownRandom());
        removeSheriffDetectable(Setup.TOWN_C, Setup.THREAT_C);

        assertBadGameSettings();

        // cleanup
        removeSetupHidden(Hidden.TownRandom());
        SetupHidden setupHidden = SetupHiddenTestUtil.findSetupHidden(setup, "Serial Killer");
        SetupHiddenService.deleteSetupHidden(game, setupHidden);

        // mafia
        addSetupHidden(Hidden.TownInvestigative());
        addSetupHidden(Hidden.MafiaRandom());
        removeSheriffDetectable(Setup.TOWN_C, Setup.MAFIA_C);

        assertBadGameSettings();
    }

    public void testSheriffNoDifference() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        addSheriffDetectable(BasicRoles.Citizen(), BasicRoles.SerialKiller());
        editRule(SetupModifierName.CHECK_DIFFERENTIATION, false);

        Faction threatFaction = setup.getFactionByColor(Setup.THREAT_C);
        Faction townFaction = setup.getFactionByColor(Setup.TOWN_C);
        assertTrue(townFaction.sheriffCheckables.contains(threatFaction));

        nightStart();

        assertTrue(sheriff.is(Sheriff.abilityType));
        assertTrue(sk.is(SerialKiller.abilityType));
        assertTrue(townFaction.sheriffCheckables.contains(threatFaction));
        Faction sheriffFaction = sheriff.getPlayer().getGameFaction().faction;
        Faction skFaction = sk.getPlayer().getGameFaction().faction;
        assertTrue(sheriffFaction.sheriffCheckables.contains(skFaction));

        setTarget(sheriff, sk);
        endNight();

        partialContains(sheriff, "suspicious");

        skipDay();

        setTarget(sheriff, cit);
        endNight();

        partialContains(sheriff, "not suspicious");
    }

    public static void seen(Controller sheriff, String teamColor) {
        seen(sheriff, game.getFaction(teamColor));
    }

    public static void seen(Controller sheriff, GameFaction team) {
        ArrayList<Object> list = Sheriff.generateFeedback(team, game);

        Message om = new Announcement(game).add(list);

        String message = om.access(sheriff.getPlayer());
        partialContains(sheriff, message);
    }
}

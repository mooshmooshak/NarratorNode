package integration.logic.abilities;

import game.abilities.Bulletproof;
import game.ai.Computer;
import game.ai.Controller;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import util.TestUtil;

public class TestSerialKiller extends SuperTest {
    public TestSerialKiller(String name) {
        super(name);
    }

    public void testImmunity() {
        Controller gf1 = addPlayer(BasicRoles.Godfather());
        Controller gf2 = addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        TestUtil.removeAbility(BasicRoles.SerialKiller().role, Bulletproof.abilityType);
        TestUtil.removeAbility(BasicRoles.Godfather(Setup.YAKUZA_C).role, Bulletproof.abilityType);

        mafKill(gf1, gf2);
        mafKill(gf2, sk);
        setTarget(sk, gf1);

        endNight();

        isDead(sk, gf1, gf2);

        isLoser(sk, gf1, gf2);
    }

    public void testWin() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller sk1 = addPlayer(BasicRoles.SerialKiller());
        Controller sk2 = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.BusDriver());

        setTarget(sk1, sk1);

        endNight();
        for(Message m: game.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement){
                assertTrue(m.isNightToDayAnnouncement());
            }
        }

        skipDay();

        setTarget(sk2, cit);
        nextNight();

        setTarget(sk2, cit2);
        endNight();

        isLoser(sk1);
        isWinner(sk2);
    }

    public void testBDChaos() {
        Controller b = addPlayer(BasicRoles.BusDriver());
        Controller c = addPlayer(BasicRoles.BusDriver());

        Controller d = addPlayer(BasicRoles.Chauffeur());
        Controller e = addPlayer(BasicRoles.Chauffeur());

        Controller f = addPlayer(BasicRoles.Chauffeur2());
        Controller g = addPlayer(BasicRoles.Chauffeur2());
        Controller a = addPlayer(BasicRoles.SerialKiller());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        dayStart();

        voteOut(b, e, c, d, f, sk);

        setTarget(a, c);
        mafKill(f, f);

        endNight();

        isDead(f, c, b);

        voteOut(a, e, g, sk);

        mafKill(e, d);
        mafKill(g, d);

        endNight();

        voteOut(e, g, sk);

        isDead(a, e);

        mafKill(g, g);
        endNight();

        isLoser(a);
    }

    public void testLetters() {

        assertEquals(toLetter(1), "A");
        assertEquals(toLetter(26), "Z");
        assertEquals(toLetter(27), "AA");
        assertEquals(toLetter(28), "AB");

    }

    private static String toLetter(int i) {
        return Computer.toLetter(i);
    }
}

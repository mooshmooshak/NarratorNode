package integration.logic.abilities;

import game.abilities.Bulletproof;
import game.abilities.GraveDigger;
import game.abilities.Gunsmith;
import game.abilities.Hidden;
import game.abilities.Ventriloquist;
import game.abilities.Vigilante;
import game.ai.Controller;
import game.event.DeadChat;
import game.event.VoidChat;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.SetupHiddenService;
import util.TestUtil;
import util.models.SetupHiddenTestUtil;

public class TestGhost extends SuperTest {

    public TestGhost(String name) {
        super(name);
    }

    public void testBasic() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());

        shoot(vig, ghost);
        nextNight();

        assertCause(ghost, vig);
        assertContains(ghost.getCommands(), Ventriloquist.abilityType);
        assertFalse(ghost.getPlayer().getAcceptableTargets(Ventriloquist.abilityType).isEmpty());

        setTarget(ghost, vig, Ventriloquist.abilityType);
        endNight();

        assertIsDay();

        assertTrue(game.getEventManager().dayChat.isActive());
        assertFalse(game.getEventManager().voidChat.isActive());
        assertTrue(ghost.getChatKeys().contains(vig.getName()));
        assertFalse(ghost.getChatKeys().contains(VoidChat.KEY));

        say(ghost, "I'm mafia", vig.getName());
        ventVote(ghost, vig, doc);
        vote(maf, doc);
        vote(sher, doc);

        mafKill(maf, vig);
        shoot(vig, sher);

        endNight();

        isWinner(ghost, maf);
        isLoser(doc, sher, vig);
    }

    public void testEndNight() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(sk, ghost);
        endNight();

        skipDay();

        endNight(ghost);
        cancelEndNight(ghost);

        endNight(cit, cit2, sk);

        assertIsNight();
        assertInProgress();

        endNight(ghost);
    }

    public void testInvalidTargeting() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller ars = addPlayer(BasicRoles.Arsonist());
        Controller maf = addPlayer(BasicRoles.Chauffeur());

        mafKill(maf, maf);
        endNight();

        voteOut(ghost, ars, cit, cit2);

        try{
            setTarget(ghost, maf);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testGraveDigging() {
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller digger = addPlayer(BasicRoles.GraveDigger());
        Controller ghost = addPlayer(BasicRoles.Ghost());

        voteOut(ghost, digger, vet, cit);

        possess(ghost, vet);
        setTarget(GraveDigger.getAction(digger, game, ghost, cit));

        endNight();

        isPuppeted(cit);
        hasPuppets(vet, false);
    }

    public void testVeteranKillingDeadGhost() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(ghost, veteran, doc, witch);

        possess(ghost, veteran);
        alert(veteran);

        endNight();

        assertAttackSize(1, ghost);
    }

    public void testMMKillingDeadGhost() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller mm = addPlayer(BasicRoles.MassMurderer());
        Controller agent = addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.MM_SPREE_DELAY, 4);

        voteOut(ghost, veteran, doc, mm);

        setTarget(agent, mm);
        possess(ghost, mm);
        setTarget(mm, mm);

        endNight();

        assertAttackSize(1, ghost);
        skipDay();

        setTarget(mm, mm);
        setTarget(doc, mm);
        endNight();

        isDead(doc);
    }

    public void testLynchDeath() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller ghost = addPlayer(BasicRoles.Ghost());

        voteOut(ghost, doc, sher, vig);

        assertCause(ghost, vig);

        possess(ghost, vig);
        endNight();

        say(ghost, "I'm mafia", vig.getName());
        ventVote(ghost, vig, doc);
        vote(maf, doc);
        vote(sher, doc);

        mafKill(maf, vig);
        shoot(vig, sher);

        endNight();

        isWinner(ghost, maf);
        isLoser(doc, sher, vig);
    }

    public void testPoisonDeath() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());
        TestUtil.removeAbility(BasicRoles.Poisoner().role, Bulletproof.abilityType);

        setTarget(poisoner, ghost);
        nextNight();

        assertCause(ghost, poisoner);

        possess(ghost, poisoner);
        endNight();

        say(ghost, "I'm mafia", poisoner.getName());
        ventVote(ghost, poisoner, doc);
        vote(maf, doc);
        vote(sher, doc);

        mafKill(maf, poisoner);

        endNight();

        isWinner(ghost, maf);
        isLoser(doc, sher, poisoner);
    }

    public void testElectroDeath() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Chauffeur());

        electrify(electro, ghost, sher);

        setTarget(sher, ghost);
        nextNight();

        isDead(ghost);
        assertCause(ghost, electro);

        possess(ghost, electro);
        endNight();

        isPuppeted(electro);
        hasPuppets(ghost);

        say(ghost, "I'm mafia", electro.getName());
        ventVote(ghost, electro, doc);
        vote(maf, doc);
        vote(cit, doc);

        mafKill(maf, maf);

        endNight();

        isLoser(ghost, maf, doc, sher);
        isWinner(electro);
    }

    public void testGhostLosesIfAlive() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller goon = addPlayer(BasicRoles.Goon());

        voteOut(goon, citizen, ghost);

        assertGameOver();
        isLoser(ghost);
    }

    public void testGhostHammeringGhost1() {
        Controller v1 = addPlayer(BasicRoles.Veteran());
        Controller v2 = addPlayer(BasicRoles.Veteran());
        Controller g2 = addPlayer(BasicRoles.Ghost());
        Controller g1 = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Chauffeur());

        voteOut(g1, v2, maf, g2);

        endNight();

        voteOut(g2, v1, v2, maf);

        mafKill(maf, maf);
        endNight();

        isWinner(v1, v2, g2);
        isLoser(g1, maf);
    }

    public void testGhostHammeringGhost2() {
        Controller v1 = addPlayer(BasicRoles.Veteran());
        Controller v2 = addPlayer(BasicRoles.Veteran());
        Controller g2 = addPlayer(BasicRoles.Ghost());
        Controller g1 = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());

        voteOut(g1, v2, maf, g2);

        endNight();

        voteOut(g2, v1, v2, maf);

        mafKill(maf, v1);
        endNight();

        isLoser(v1, v2, g2);
        isWinner(g1, maf);
    }

    public void testGhostsShootingEachOther() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller g1 = addPlayer(BasicRoles.Ghost());
        Controller g2 = addPlayer(BasicRoles.Ghost());
        Controller agent = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, g1);
        nextNight();

        setTarget(gs, g2);
        nextNight();

        shoot(g1, g2);
        shoot(g2, g1);

        endNight();

        vote(gs, agent);
        endDay();

        endNight();

        isWinner(agent);
        isLoser(g1, g2, gs);
    }

    public void testDeadChatAccess() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller maf = addPlayer(BasicRoles.Goon());

        voteOut(ghost, maf, cit, amn);

        partialExcludes(ghost.getChatKeys(), DeadChat.KEY);
    }

    public void testNoGhostEnemyTeam() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Ghost());
        addPlayer(BasicRoles.Goon());

        setEnemies(BasicRoles.Ghost(), BasicRoles.Citizen());

        Faction benigns = setup.getFactionByColor(BasicRoles.Ghost().getColor());
        assertFalse(benigns.enemies.isEmpty());

        assertBadGameSettings();

        SetupHidden setupHidden = SetupHiddenTestUtil.findSetupHidden(setup, "Ghost");
        SetupHiddenService.deleteSetupHidden(game, setupHidden);

        addSetupHidden(Hidden.NeutralBenignRandom());

        assertBadGameSettings();
    }

    public void testRecruitableGhost() {
        addPlayer(BasicRoles.Citizen());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        nightStart();

        setTarget(cl, ghost);
        endNight();

        assertEquals(BasicRoles.Ghost().getColor(), ghost.getColor());
    }

    public void testBakedGhost() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(baker, ghost);
        endNight();

        voteOut(ghost, baker, sk, maf);

        possess(ghost, sk);
        possess(ghost, maf);

        assertActionSize(1, ghost);
    }

    public void testShootingFakeGun() {
        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, true);

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.GS_DAY_GUNS, true);

        nightStart();

        setTarget(gs, Gunsmith.abilityType, Gunsmith.FAULTY, ghost);
        endNight();

        shoot(ghost, vent);

        isDead(ghost);

        skipDay();

        setTarget(sk, gs);
        endNight();

        isWinner(ghost);
    }

    public void testBDCausingDeath() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, ghost);
        nextNight();

        drive(bd, ghost, maf);
        shoot(ghost, maf);

        endNight();

        isDead(ghost);

        voteOut(bd, gs, maf);

        endNight();

        isWinner(ghost);
    }

    public void testCowardCausingDeath() {
        Role role = BasicRoles.Coward().role;
        Faction town = setup.getFactionByColor(Setup.TOWN_C);

        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller coward = addPlayer(FactionRoleService.createFactionRole(town, role));

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, ghost);
        nextNight();

        setTarget(coward, ghost);
        shoot(ghost, coward);

        endNight();

        isDead(ghost);

        voteOut(coward, gs, maf);

        endNight();

        isWinner(ghost);
    }

    public void testWitchCausingDeath() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, ghost);
        nextNight();

        witch(witch, ghost, ghost);
        shoot(ghost, maf);

        endNight();

        isDead(ghost);

        voteOut(witch, gs, maf);

        endNight();

        isWinner(ghost);
    }

    public void testWitchCausingDeath2() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, ghost);
        nextNight();

        witch(witch, ghost, ghost);
        endNight();

        isDead(ghost);

        voteOut(witch, gs, maf);

        endNight();

        isWinner(ghost);
    }

    public void testGhostFakeShootingGun() {
        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, true);

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        addPlayer(BasicRoles.SerialKiller());

        setTarget(Gunsmith.getFaultyAction(gs, ghost, game));
        nextNight();

        shoot(ghost, gs);
        endNight();

        assertCause(ghost, gs);
        isWinner(ghost);
    }

    public void testShootingBomb() {
        Controller bomb = addPlayer(BasicRoles.Bomb());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        addPlayer(BasicRoles.SerialKiller());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, ghost);
        nextNight();

        shoot(ghost, bomb);
        endNight();

        assertCause(ghost, bomb);

        isLoser(bomb, gs);
        isWinner(ghost);
    }

    public void testJesterDeath() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        addPlayer(BasicRoles.Goon(), 2);

        dayStart();

        voteOut(jester, ghost);
        endDay();
        endNight();

        isLoser(ghost);

        skipDay();
        try{
            setTarget(ghost, cit);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testModKill() {
        addPlayer(BasicRoles.Architect());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        addPlayer(BasicRoles.SerialKiller());

        nightStart();

        ghost.getPlayer().modkill(1);
        endNight();

        isLoser(ghost);
    }

    public void testDeadGhostUsingGun() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller maf = addPlayer(BasicRoles.Goon());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, ghost);
        endNight();

        voteOut(ghost, gs, sk, maf);

        assertFalse(ghost.getCommands().contains(Vigilante.abilityType));

        try{
            shoot(ghost, maf);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testBodyguard() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller maf = addPlayer(BasicRoles.Goon());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, ghost);
        nextNight();

        shoot(ghost, maf);
        setTarget(bg, maf);
        endNight();

        isWinner(ghost);
    }

    public void testMultipleKills() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller ghost = addPlayer(BasicRoles.Ghost());

        setTarget(doc, ghost);
        setTarget(sk, ghost);
        endNight(sk);

        mafKill(maf, ghost);
        endNight(maf);
        shoot(vig, ghost);
        endNight();

        voteOut(maf, vig, doc, sk);

        setTarget(sk, doc);
        endNight();

        isWinner(ghost);
    }

    public void testNoPossessionWhileAlive() {
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller cit = addPlayer(BasicRoles.Citizen());

        witch(witch, ghost, ghost);
        endNight();

        isPuppeted(ghost, false);
        hasPuppets(ghost, false);

        skipDay();

        witch(witch, ghost, cit);
        endNight();

        isPuppeted(ghost, false);
        isPuppeted(witch, false);
        hasPuppets(witch, false);
        hasPuppets(ghost, false);
    }

    public static void assertCause(Controller ghost, Controller cause) {
        assertEquals(cause.getPlayer(), ghost.getPlayer().getDeathType().getCause());
    }

    private void possess(Controller ghost, Controller target) {
        setTarget(ghost, target, Ventriloquist.abilityType);
    }

}

package integration.logic.abilities;

import game.abilities.Driver;
import game.abilities.FactionSend;
import game.ai.Controller;
import game.logic.Player;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import util.TestChatUtil;

public class TestDriver extends SuperTest {

    public TestDriver(String name) {
        super(name);
    }

    public void testSingleDriving() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        drive(bd, cit, maf);
        mafKill(maf, cit);

        endNight();

        isWinner(cit);
    }

    public void testDoubleDrivingFeedback() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller chafu = addPlayer(BasicRoles.Chauffeur());

        nightStart();
        drive(bd, cit, dd);
        drive(chafu, cit, dd);

        endNight();

        // ending night also applies here
        partialContains(dd, Driver.FEEDBACK, 2);
    }

    public void testFailSingleTarget() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());

        nightStart();

        try{
            setTarget(bd, cit);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testSelfTarget() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());

        try{
            setTarget(bd, bd);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testCancelSwitchTargets() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller x = addPlayer(BasicRoles.Citizen());
        Controller y = addPlayer(BasicRoles.Witch());

        drive(bd, x, y);
        int list1 = TestChatUtil.getMessages((Player) bd).size();
        command(bd, Driver.COMMAND + " " + y.getName() + " " + x.getName());

        int list2 = TestChatUtil.getMessages((Player) bd).size();

        // assertTrue(bd.getActions().isEmpty());
        assertEquals(list2, list1 + 1);
    }

    public void testDeadTarget() {
        Controller w = addPlayer(BasicRoles.Witch());
        Controller w2 = addPlayer(BasicRoles.Witch());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller c = addPlayer(BasicRoles.Citizen());

        voteOut(c, bd, w, w2);

        assertFalse(bd.getPlayer().isAcceptableTarget(bd.getPlayer().action(c.getPlayer())));
    }

    public void testDuo() {
        initialSetupDuoTesting();
        setTarget(d, e);
        endNight();

        isDead(f);

        initialSetupDuoTesting();
        setTarget(d, f);
        endNight();

        isDead(c);

        initialSetupDuoTesting();
        setTarget(d, c);
        endNight();

        isDead(e);

        // only one feedback arises from bus driver
        partialContains(e, Driver.FEEDBACK, 1);
    }

    private Controller e, f, c, d;

    private void initialSetupDuoTesting() {
        newNarrator();

        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        c = addPlayer(BasicRoles.Citizen());
        d = addPlayer(BasicRoles.SerialKiller());
        e = addPlayer(BasicRoles.Citizen());
        f = addPlayer(BasicRoles.Citizen());

        nightStart();

        drive(bd, e, f);
        endNight(bd);

        drive(chf, f, c);

    }

    public void testDoubleTarget() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller chf = addPlayer(BasicRoles.Chauffeur());

        nightStart();

        drive(bd, bd, chf);
        assertTrue(bd.getPlayer().getActions().getTargets(FactionSend.abilityType).isEmpty());
        assertTrue(bd.getPlayer().getActions().isTargeting(Player.list(chf.getPlayer(), bd.getPlayer()),
                Driver.abilityType));
        mafKill(chf, bd);
        setTarget(doc, bd);

        endNight();

        assertInProgress();
    }
}

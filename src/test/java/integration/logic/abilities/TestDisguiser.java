package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.Agent;
import game.abilities.Disguiser;
import game.abilities.DrugDealer;
import game.abilities.Executioner;
import game.abilities.Godfather;
import game.abilities.Goon;
import game.abilities.Janitor;
import game.abilities.Visit;
import game.ai.Controller;
import game.event.ChatMessage;
import game.event.DayChat;
import game.event.FactionChat;
import game.event.JailChat;
import game.event.Message;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.logic.templates.HTMLDecoder;
import game.setups.Setup;
import integration.logic.SuperTest;
import junit.framework.AssertionFailedError;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.RoleService;
import util.TestUtil;
import util.game.LookupUtil;
import util.models.ActionTestUtil;

public class TestDisguiser extends SuperTest {

    public TestDisguiser(String s) {
        super(s);
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Doctor());
        Controller disg = addPlayer(BasicRoles.Disguiser());
        addPlayer(BasicRoles.SerialKiller());

        String disg_n = disg.getName();

        saveNames(cit, disg);

        dayStart();
        say(disg, gibberish(), DayChat.KEY);
        ChatMessage cm = (ChatMessage) game.getEventManager().getDayChat().getEvents().getLast();

        ArrayList<Message> deaths = addAnnouncementListener();
        skipDay();

        setTarget(disg, cit);
        endNight();

        isDead(cit);
        isAlive(disg);
        assertStatus(cit, Janitor.abilityType);
        assertSwitched(disg, cit);
        assertTrue(deaths.get(0).access(Message.PUBLIC, new HTMLDecoder()).contains(Constants.JANITOR_DESCRIP));

        partialContains(gibberish);
        assertTrue(cm.access(Message.PUBLIC).contains(disg_n));
        assertTrue(cm.access(Message.PUBLIC, new HTMLDecoder()).contains(disg_n));
    }

    public void testVestNoHit() {
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller godfather = addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Citizen());

        modifyRole(BasicRoles.Godfather(), RoleModifierName.AUTO_VEST, 1);

        setTarget(disguiser, godfather);
        endNight();

        assertNotSame(disguiser.getName(), godfather.getName());
    }

    public void testSheriffSuccessfulChecking() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        addPlayer(BasicRoles.Arsonist());

        setTarget(sheriff, cit);
        setTarget(disguiser, cit);
        endNight();

        TestSheriff.seen(sheriff, disguiser.getColor());
    }

    // make sure bus driver doesn't effect disguiser getting

    public void testLateDriverAffecting() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller disg = addPlayer(BasicRoles.Disguiser());
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        nightStart();

        saveNames(sheriff, disg);
        electrify(em, cit1, bd);

        setTarget(baker, bd);
        nextNight();

        drive(bd, cit1, cit2);
        setTarget(disg, sheriff);
        setTarget(doc, bd);
        drive(bd, agent, sheriff);
        setTarget(doc2, sheriff);

        endNight(doc);

        endNight();

        // here's what happens.
        // disguiser kills sheriff (sheriff hasn't been switched yet))
        // bd gets woken up, and switches sheriff and agent
        // disguiser needs to have sheriff name, not agent, even though its switched

        assertSwitched(disg, sheriff);
    }

    public void testNoDoubleAction() {
        Controller disg = addPlayer(BasicRoles.Disguiser());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller bg = addPlayer(BasicRoles.Bodyguard());

        setTarget(baker, disg);
        nextNight();

        disguise(disg, cit);
        disguise(disg, bg);
        assertActionSize(1, disg);
        endNight();

        isDead(bg);
        isAlive(cit);
    }

    public void testJesterName() {
        addPlayer(BasicRoles.Lookout());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller disg = addPlayer(BasicRoles.Disguiser());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Witch());

        String disg_name = disg.getName();
        String cit_name = cit.getName();

        vote(disg, jester);
        endDay();

        isDead(jester);

        setTarget(sk, cit);
        setTarget(disg, cit);
        ArrayList<Message> deaths = addAnnouncementListener();
        endNight();

        assertEquals(2, deaths.size());

        for(int i = 0; i < 2; i++){
            try{
                printHappenings = false;
                String citSuicidePublic = deaths.get(i).access(Message.PUBLIC);
                assertTrue(citSuicidePublic.contains(disg_name));

                String citSuicidePrivate = deaths.get((i + 1) % 2).access(Message.PRIVATE);
                assertTrue(citSuicidePrivate.contains(cit_name));
            }catch(AssertionFailedError e){
                return;
            }
        }
        fail();
    }

    public void testSuperLateClean() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller disg = addPlayer(BasicRoles.Disguiser());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, amn, agent);

        saveNames(agent, disg);

        setTarget(doc, agent);
        setTarget(disg, agent);
        setTarget(agent, amn);
        endNight();

        assertSwitched(agent, disg);
    }

    public void testDeadDisguiser() {
        Controller graveDigger = addPlayer(BasicRoles.GraveDigger());
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        voteOut(disguiser, doctor, lookout, graveDigger);

        setTarget(ActionTestUtil.getGraveDiggerAction(graveDigger, Visit.getAction(disguiser, lookout, game)));
        setTarget(lookout, lookout);
        endNight();

        isAlive(lookout);
        TestLookout.seen(lookout, disguiser);
    }

    public void testLimitations() {
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Lookout());

        disguiserLimit(1);
        nightStart();

        assertPerceivedChargeRemaining(1, disguiser, Disguiser.abilityType);

        setTarget(disguiser, cit);
        endNight();

        assertFalse(disguiser.getPlayer().isPowerRole());
        assertPerceivedChargeRemaining(0, disguiser, Disguiser.abilityType);

        skipDay();

        try{
            setTarget(disguiser, fodder);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testDoubleDisguising() {
        Controller d1 = addPlayer(BasicRoles.Disguiser());
        Controller d2 = addPlayer(BasicRoles.Disguiser());
        Controller fodder = addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Citizen());

        setTarget(d1, fodder);
        endNight(d1);
        setTarget(d2, fodder);
        endNight();

        assertEquals(d2.getName(), d2.getPlayer().getID());
    }

    public void testRealRoleBlock() {
        Controller disg = addPlayer(BasicRoles.Disguiser());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller escort = addPlayer(BasicRoles.Escort());

        disguiserLimit(1);

        setTarget(escort, disg);
        setTarget(disg, cit);
        endNight();

        assertPerceivedChargeRemaining(1, disg, Disguiser.abilityType);
    }

    public void testFakeRoleBlock() {
        Controller dealer = addPlayer(BasicRoles.DrugDealer());
        Controller disg = addPlayer(BasicRoles.Disguiser());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Escort());

        disguiserLimit(1);

        nightStart();

        drug(dealer, disg, DrugDealer.BLOCKED);
        setTarget(disg, cit);
        endNight();

        assertPerceivedChargeRemaining(0, disg, Disguiser.abilityType);
    }

    public void testAttainingSendAbility() {
        Controller disg = neutDisguiser();
        Controller mafioso = addPlayer(BasicRoles.Goon());
        Controller drugDealer = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Amnesiac());

        editRule(SetupModifierName.DIFFERENTIATED_FACTION_KILLS, true);

        setTarget(disg, mafioso);
        try{
            mafKill(disg, drugDealer);
            fail();
        }catch(PlayerTargetingException e){
        }
        endNight();

        assertTrue(disg.getPlayer().getFactions().contains(drugDealer.getPlayer().getGameFaction()));
        assertNotSame(disg.getColor(), drugDealer.getColor());
        skipDay();

        mafKill(disg, drugDealer);
        mafKill(drugDealer, disg);
        assertFactionController(disg, drugDealer.getPlayer().getGameFaction());
        endNight();

        // disguiser gets priority, because they look like a goon

        isDead(drugDealer);
        isAlive(disg);
        isLoser(mafioso, drugDealer);
        isWinner(disg);

        String[] dt = drugDealer.getPlayer().getDeathType().get(0);
        String flag = dt[0];
        assertTrue(flag.endsWith(drugDealer.getPlayer().getGameFaction().getName()));
    }

    public void testDisguisedAsGodfatherAndLoss() {
        RoleService.delete(BasicRoles.Godfather().role);
        Role role = RoleService.createRole(setup, "Godfather", Godfather.abilityType);
        Faction faction = setup.getFactionByColor(Setup.MAFIA_C);
        FactionRole vulnerableGF = FactionRoleService.createFactionRole(faction, role);

        Controller godfather = addPlayer(vulnerableGF);
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller neutDisguiser = neutDisguiser();
        Controller baker = addPlayer(BasicRoles.Baker());
        addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Arsonist());

        disguiserLimit(2);

        setTarget(neutDisguiser, godfather);
        nextNight();

        isDead(godfather);

        mafKill(maf, neutDisguiser);
        mafKill(neutDisguiser, maf);
        assertActionSize(2, maf);
        assertActionSize(2, neutDisguiser);
        assertEquals(2, neutDisguiser.getPlayer().nightVotePower(maf.getPlayer().getGameFaction().getMembers()));
        assertFactionController(neutDisguiser, maf.getColor());
        endNight();

        isDead(maf);
        isAlive(neutDisguiser);

        skipDay();

        setTarget(neutDisguiser, baker);
        endNight();

        assertFalse(neutDisguiser.getPlayer().getFactions().contains(baker.getPlayer().getGameFaction()));
        assertFalse(neutDisguiser.getPlayer().getFactions().contains(maf.getPlayer().getGameFaction()));
    }

    public void testGainingMasonNightChat() {
        disguiserLimit(2);

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller disg = addPlayer(BasicRoles.Disguiser());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller enforcer = addPlayer(BasicRoles.Enforcer());
        addPlayer(BasicRoles.Goon(), 2);

        nightStart();

        say(ml, gibberish(), ml.getColor());
        ChatMessage cm = (ChatMessage) game.getEventManager().getNightLog(ml.getColor()).getEvents().getLast();
        FactionChat mafChat1 = (FactionChat) game.getEventManager().getNightLog(enforcer.getColor());
        assertFalse(mafChat1.hasAccess(disg.getPlayer().getID()));

        partialExcludes(disg, gibberish);

        setTarget(disg, ml);
        nextNight();

        isDead(ml);
        assertTrue(Disguiser.hasDisguised(disg.getPlayer()));
        assertTrue(disg.getPlayer().getFactions().contains(game.getFaction(cit.getColor())));
        assertTrue(disg.getChatKeys().contains(enforcer.getColor()));

        FactionChat mafChat2 = (FactionChat) game.getEventManager().getNightLog(enforcer.getColor());
        assertTrue(mafChat2.hasAccess(disg.getPlayer().getID()));
        assertTrue(cm.hasAccess(disg, game));
        assertTrue(mafChat1.hasAccess(disg.getPlayer().getID()));
        assertTrue(disg.getPlayer().getEvents().events.contains(cm));

        String gib1 = gibberish;

        say(disg, gibberish(), ml.getColor());
        partialContains(disg, gibberish);
        partialContains(enforcer, gibberish);

        assertTrue(disg.getPlayer().getChats().contains(game.getEventManager().getEventLog(ml.getColor())));

        setTarget(disg, cit);
        nextNight();

        partialExcludes(disg, gib1);
        partialExcludes(disg, gibberish);
        assertFalse(disg.getPlayer().getChats().contains(game.getEventManager().getEventLog(ml.getColor())));
    }

    public void testGainingNightChat() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller disg = neutDisguiser();
        Controller mafioso = addPlayer(BasicRoles.Goon());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Citizen());

        disguiserLimit(2);
        TestUtil.modifyAbility(Disguiser.abilityType, AbilityModifierName.CHARGES, 2);
        nightStart();

        say(mafioso, gibberish(), mafioso.getColor());
        ChatMessage cm = (ChatMessage) game.getEventManager().getNightLog(mafioso.getColor()).getEvents().getLast();
        FactionChat mafChat1 = (FactionChat) game.getEventManager().getNightLog(dd.getColor());
        assertFalse(mafChat1.hasAccess(disg.getPlayer().getID()));

        partialExcludes(disg, gibberish);

        setTarget(disg, mafioso);
        nextNight();

        FactionChat mafChat2 = (FactionChat) game.getEventManager().getNightLog(dd.getColor());
        assertTrue(mafChat2.hasAccess(disg.getPlayer().getID()));
        assertTrue(cm.hasAccess(disg, game));
        assertTrue(mafChat1.hasAccess(disg.getPlayer().getID()));
        assertTrue(disg.getPlayer().getEvents().events.contains(cm));

        String gib1 = gibberish;

        say(disg, gibberish(), mafioso.getColor());
        partialContains(disg, gibberish);
        partialContains(dd, gibberish);

        assertTrue(disg.getPlayer().getChats().contains(game.getEventManager().getEventLog(mafioso.getColor())));

        setTarget(disg, cit);
        nextNight();

        partialExcludes(disg, gib1);
        partialExcludes(disg, gibberish);
        assertFalse(disg.getPlayer().getChats().contains(game.getEventManager().getEventLog(mafioso.getColor())));
    }

    public void testJailedChat() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller disguiser = addPlayer(BasicRoles.Disguiser());

        jail(jailor, cit);
        skipDay();

        String key = JailChat.GetKey(cit, 1);
        String citJailText = gibberish();
        String jailorJailText = gibberish();
        String disguiserJailText = gibberish();
        String jailorJailText2 = gibberish();
        say(cit, citJailText, key);
        say(jailor, jailorJailText, key);

        endNight();

        jail(jailor, disguiser);
        skipDay();

        key = JailChat.GetKey(disguiser, 2);
        say(disguiser, disguiserJailText, key);
        say(jailor, jailorJailText2, key);
        nextNight();

        setTarget(disguiser, cit);
        endNight();

        partialExcludes(disguiser, citJailText);
        partialExcludes(disguiser, jailorJailText);
        partialContains(disguiser, disguiserJailText);
        partialContains(disguiser, jailorJailText2);
    }

    Controller neutDisguiser() {
        Role role = LookupUtil.findRole(setup, Disguiser.class.getSimpleName()).get();
        Faction faction = setup.getFactionByColor(Setup.THREAT_C);
        return addPlayer(FactionRoleService.createFactionRole(faction, role));
    }

    private String disg_name, takenName;

    private void saveNames(Controller p, Controller q) {
        if(p.is(Disguiser.abilityType)){
            disg_name = p.getName();
            takenName = q.getName();
        }else{
            disg_name = q.getName();
            takenName = p.getName();
        }
    }

    public void testKilledTargetNull() {
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Citizen());

        setTarget(doc, cit);
        setTarget(disguiser, cit);
        endNight();

        Disguiser roleCard = disguiser.getPlayer().getAbility(Disguiser.class);
        assertNull(roleCard.killedTarget);
    }

    public void testExecutionerText() {
        Controller executioner = addPlayer(BasicRoles.Executioner());
        Controller disg1 = addPlayer(BasicRoles.Disguiser());
        Controller f1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.ElectroManiac());

        nightStart();
        TestExecutioner.SetExecTarget(executioner, disg1);

        Executioner roleCard = executioner.getPlayer().getAbility(Executioner.class);
        partialContains(roleCard.getProfileRoleCardDetails(executioner.getPlayer()), disg1.getName());

        setTarget(disg1, f1);
        endNight();

        assertTrue(roleCard.getProfileRoleCardDetails(executioner.getPlayer()).contains(Executioner.UNKNOWN_TARGET));
        skipTearDown = true;
    }

    public void testDisguiserDisguisingAsDisguiser() {
        Controller d1 = addPlayer(BasicRoles.Disguiser());
        Controller d2 = addPlayer(BasicRoles.Disguiser());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Architect());

        String d1_n = d1.getName();
        String d2_n = d2.getName();
        String c1_n = cit.getName();

        setTarget(d1, cit);
        endNight(d1);
        setTarget(d2, d1);

        // d1 attacks c1
        // d2 attacks d1
        // d2 should switch names with d1

        ArrayList<Message> deathAnnouncement = addAnnouncementListener();
        endNight();

        isAlive(d2);
        assertEquals(d1_n, d2.getName());

        String c1death = deathAnnouncement.get(0).access(Message.PUBLIC);
        String d2death = deathAnnouncement.get(1).access(Message.PUBLIC);
        if(c1death.contains(d2_n) || d2death.contains(c1_n)){
            String temp = c1death;
            c1death = d2death;
            d2death = temp;
        }
        assertTrue(c1death.contains(c1_n));
        assertTrue(d2death.contains(d2_n));
    }

    public void testDisguiserDisguisingAsDisguiser2() {
        Controller d1 = addPlayer(BasicRoles.Disguiser());
        Controller d2 = addPlayer(BasicRoles.Disguiser());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Architect());

        String d1_n = d1.getName();
        String d2_n = d2.getName();
        String c1_n = cit.getName();

        setTarget(d1, cit);
        setTarget(d2, d1);
        endNight(d2);

        // d2 attacks d1
        // d1 attacks c1
        // d2 should switch names with d1
        // d1 (whos called d2 now) switches names with cit

        ArrayList<Message> deathAnnouncement = addAnnouncementListener();
        endNight();

        isAlive(d2);
        assertEquals(d1_n, d2.getName());

        String d2death = deathAnnouncement.get(0).access(Message.PUBLIC);
        String c1death = deathAnnouncement.get(1).access(Message.PUBLIC);
        if(d2death.contains(c1_n) || c1death.contains(d2_n)){
            String temp = d2death;
            d2death = c1death;
            c1death = temp;
        }

        assertTrue(d2death.contains(d2_n));
        assertTrue(c1death.contains(c1_n));
    }

    public void testDisguiserDetection() {
        Controller detective = addPlayer(BasicRoles.Detective());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        addPlayer(BasicRoles.Poisoner());

        setTarget(disguiser, fodder);
        nextNight();

        setTarget(detective, disguiser);
        mafKill(disguiser, fodder2);
        endNight();

        TestDetective.seen(detective, fodder2);
    }

    public void testDisguisingIntoDeadKnowsTeam() {
        Controller deadYak = addPlayer(BasicRoles.Goon());
        Controller disguisedYak = addPlayer(BasicRoles.Disguiser());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller disguiser = neutDisguiser();
        addPlayer(BasicRoles.Jester());

        setTarget(disguiser, disguisedYak);
        setTarget(sk, deadYak);
        nextNight();

        try{
            mafKill(disguiser, sk);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testDoubleSending() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller yak = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, Agent.abilityType));
        Controller yak2 = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, Agent.abilityType));

        setTarget(disguiser, yak);
        nextNight();

        send(disguiser, disguiser, yak2);
        mafKill(disguiser, yak2);
        setTarget(doc, yak2);
        endNight();

        isAlive(yak2);
    }

    // testing visual what your guy was, what they did

    public void testDisguiserMayorVote() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller disg = addPlayer(BasicRoles.Disguiser());

        editRule(SetupModifierName.MAYOR_VOTE_POWER, 99);

        dayStart();
        reveal(mayor);
        skipDay();

        setTarget(disg, mayor);
        endNight();

        skipVote(disg);

        assertIsNight();

        endNight();

        vote(disg, cit);

        assertIsNight();
        endNight();

        assertGameOver();
    }

    public void testKillingAlly() {
        Controller SlaveBZ = addPlayer(BasicRoles.Goon());
        Controller SlaveCI = addPlayer(BasicRoles.Disguiser());
        Controller SlaveCJ = addPlayer(BasicRoles.Investigator());

        Controller SlaveW = addPlayer(BasicRoles.ElectroManiac());
        Controller SlaveX = addPlayer(BasicRoles.ElectroManiac());

        Controller SlaveAF = addPlayer(BasicRoles.Citizen());
        Controller SlaveAV = addPlayer(BasicRoles.Sheriff());

        FactionRole yakioso = BasicRoles.getMember(Setup.YAKUZA_C, Goon.abilityType);
        Controller SlaveCS = addPlayer(yakioso);
        Controller SlaveD = addPlayer(yakioso);
        Controller SlaveM = addPlayer(yakioso);

        setTarget(SlaveW, SlaveCJ);
        setTarget(SlaveX, SlaveBZ);

        nextNight();
        setTarget(SlaveW, SlaveAV);
        setTarget(SlaveAV, SlaveBZ);
        setTarget(SlaveCI, SlaveD);
        setTarget(SlaveCJ, SlaveAF);
        setTarget(SlaveX, SlaveAF);
        nextNight();

        send(SlaveM, SlaveCS);
    }

    public void testFeedbackDetective() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller det = addPlayer(BasicRoles.Detective());
        Controller disg = addPlayer(BasicRoles.Disguiser());

        setTarget(det, disg);
        String name = disg.getName();
        setTarget(disg, cit);
        endNight();

        partialContains(getFeedback(det, 0).get(0).getExtras(), name);
    }

    public void testDisguisingAsEachOther() {
        Controller SlaveAX = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, Goon.abilityType));
        Controller SlaveCE = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, Disguiser.abilityType));

        Controller SlaveCG = addPlayer(BasicRoles.Goon());
        Controller SlaveAF = addPlayer(BasicRoles.Agent());
        Controller SlaveAY = addPlayer(BasicRoles.Disguiser());

        nightStart();

        setTarget(SlaveAY, SlaveCE);
        setTarget(SlaveCE, SlaveAY);
        endNight();

        isDead(SlaveCE, SlaveAY);
        isAlive(SlaveAX, SlaveAF, SlaveCG);

        skipDay();

        send(SlaveAF, SlaveAF);
    }

    private void disguiserLimit(int charge) {
        modifyRole(BasicRoles.Disguiser(), AbilityModifierName.CHARGES, charge);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
    }

    private void assertSwitched(Controller p, Controller q) {
        if(p.is(Disguiser.abilityType)){
            assertEquals(takenName, p.getName());
            assertEquals(disg_name, q.getName());
        }else{
            assertEquals(takenName, q.getName());
            assertEquals(disg_name, p.getName());
        }
    }

    private void disguise(Controller disguiser, Controller target) {
        setTarget(disguiser, target, Disguiser.abilityType);
    }

}

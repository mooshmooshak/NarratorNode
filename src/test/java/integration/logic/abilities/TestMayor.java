package integration.logic.abilities;

import game.abilities.Hidden;
import game.abilities.Mayor;
import game.abilities.util.AbilityUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Command;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.HiddenService;
import services.RoleService;
import services.SetupHiddenService;
import util.models.SetupHiddenTestUtil;

public class TestMayor extends SuperTest {

    public TestMayor(String name) {
        super(name);
    }

    public void testCommands() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Witch());

        vote(mayor, witch);
        reveal(mayor);

        partialExcludes(Command.getText(game.getCommands()), Constants.CANCEL);
    }

    public void testVotes() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        assertEquals(Mayor.abilityType, AbilityUtil.getAbilityByCommand(Mayor.COMMAND));

        reveal(mayor);

        try{
            reveal(mayor);
            fail();
        }catch(NarratorException e){
        }

        vote(mayor, witch);
        assertGameOver();
    }

    public void testCultConversion() {
        modifyRole(BasicRoles.Mayor(), RoleModifierName.UNCONVERTABLE, true);

        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        try{
            setTarget(mayor);
            fail();
        }catch(NarratorException e){
        }

        String originalColor = mayor.getColor();

        setTarget(cl, mayor);
        endNight();

        assertEquals(mayor.getColor(), originalColor);
    }

    public void testNotEnoughToLynch() {
        ControllerList cits = addPlayer(BasicRoles.Citizen(), 6);
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller witch = addPlayer(BasicRoles.Witch());

        setMayorVotes(3);
        dayStart();

        vote(witch, mayor);
        vote(mayor, witch);
        reveal(mayor);
        vote(cits.get(0), witch);

        assertGameOver();
    }

    private static void setMayorVotes(int vPower) {
        editRule(SetupModifierName.MAYOR_VOTE_POWER, vPower);
    }

    public void testMultiMayor2() {
        addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Mayor());

        assertBadGameSettings();

        SetupHidden setupHidden = SetupHiddenTestUtil.findSetupHidden(setup, "Mayor");
        SetupHiddenService.deleteSetupHidden(game, setupHidden);

        try{
            RoleService.createRole(setup, "Mayor", Mayor.abilityType);
            fail();
        }catch(NamingException e){
        }

        setupHidden = SetupHiddenTestUtil.findSetupHidden(setup, "Mayor");
        SetupHiddenService.deleteSetupHidden(game, setupHidden);
        assertNull(SetupHiddenTestUtil.findSetupHidden(setup, "Mayor"));
    }

    public void testMultiMayor3() {
        addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Mayor());

        assertBadGameSettings();

        SetupHidden setupHidden = SetupHiddenTestUtil.findSetupHidden(setup, "Mayor");
        SetupHiddenService.deleteSetupHidden(game, setupHidden);

        Role role = RoleService.createRole(setup, "Pseudo Mayor", Mayor.abilityType);
        Faction faction = setup.getFactionByColor(Setup.TOWN_C);
        FactionRole factionRole = FactionRoleService.createFactionRole(faction, role);
        Hidden hidden = HiddenService.createHidden(setup, "test", factionRole);
        addSetupHidden(hidden);

        dayStart();

    }

    public void testMultiMayor() {
        addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Mayor());

        assertBadGameSettings();

        SetupHidden setupHidden = SetupHiddenTestUtil.findSetupHidden(setup, "Mayor");
        SetupHiddenService.deleteSetupHidden(game, setupHidden);

        FactionRole m = BasicRoles.Mayor();
        Hidden hidden = HiddenService.createHidden(setup, "test", m);
        addSetupHidden(hidden);

        assertBadGameSettings();

        removeSetupHidden(hidden);
        for(int i = 0; i < 49; i++)
            addPlayer(Hidden.TownRandom());
        // removed a role, need to add one to be equal
        addRole(Hidden.TownRandom(), 1);

        dayStart();

        int mayors = 0;
        for(Controller p: game.getAllPlayers())
            if(p.is(Mayor.abilityType))
                mayors++;
        assertEquals(1, mayors);
    }

    public void testBlackmailedReveal() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller blackmailer = addPlayer(BasicRoles.Blackmailer());
        addPlayer(BasicRoles.ElectroManiac());

        nightStart();

        setTarget(blackmailer, mayor);

        endNight();

        assertNotContains(mayor.getCommands(), Mayor.abilityType);

        try{
            reveal(mayor);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testInnocentChild() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.MAYOR_VOTE_POWER, 0);

        vote(mayor, cit);
        reveal(mayor);

        assertIsDay();

        unvote(mayor);

        vote(mayor, cit);
        assertIsDay();

        assertVoteTarget(cit, mayor);
        assertEquals(1, mayor.getPlayer().getVotePower());
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Mayor(), AbilityModifierName.BACK_TO_BACK,
                    Mayor.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Mayor(), AbilityModifierName.ZERO_WEIGHTED,
                    Mayor.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}

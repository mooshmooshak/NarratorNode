package integration.logic.abilities;

import game.abilities.Amnesiac;
import game.abilities.Hidden;
import game.abilities.Survivor;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.ai.Controller;
import game.logic.GameFaction;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.SetupModifierService;

public class TestAmnesiac extends SuperTest {

    // has github page

    public TestAmnesiac(String name) {
        super(name);
    }

    // tests amnesiac not doing anything, and the basic ability
    public void testBasicAbility() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());

        dayStart();
        voteOut(c2, m1, a1, c1);
        endNight(); // makes sure no null pointer is thrown when amnesiac doesn'tdo anything
        skipDay();

        try{
            setTarget(a1, c1);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(a1, c2);

        endNight();

        assertEquals(a1.getColor(), c1.getColor());
    }

    // previous issue where the amnesiac turned mafia still was a member of the
    // benign team
    public void testMafSend() {
        Controller v = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());

        nightStart();

        GameFaction benign = game.getFaction(BasicRoles.Amnesiac().getColor());

        shoot(v, m2);
        nextNight();

        assertIsNight();
        assertInProgress();

        setTarget(a1, m2);
        endNight();

        assertFalse(benign.hasMember(a1.getPlayer()));
    }

    // amnesiac taking a team that is about to die
    public void testTeamExtinction() {
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());
        Controller v = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        shoot(v, m1);
        endNight();
        skipDay();
        shoot(v, m2);
        setTarget(a1, m1);
        endNight();

        GameFaction mafia = game.getFaction(m1.getColor());
        assertTrue(mafia.isAlive());
        assertInProgress();
    }

    // witch to live people
    public void testWitchToLive() {
        Controller c = addPlayer(BasicRoles.Citizen());
        Controller v = addPlayer(BasicRoles.Vigilante());
        Controller a = addPlayer(BasicRoles.Amnesiac());
        Controller w = addPlayer(BasicRoles.Witch());

        dayStart();
        voteOut(v, w, a, c);

        witch(w, a, w);
        endNight();

        partialContains(a, Amnesiac.LIVE_FEEDBACK);
    }

    // this works because doused is a Controller status
    public void testArsonPersist() {
        Controller c = addPlayer(BasicRoles.Citizen());
        Controller v = addPlayer(BasicRoles.Vigilante());
        Controller amnes = addPlayer(BasicRoles.Amnesiac());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(ars, amnes);
        endNight();
        voteOut(v, c, amnes, ars);

        setTarget(amnes, v);

        endNight();
        skipDay();

        shoot(amnes, c);
        setTarget(ars);
        endNight();

        isDead(amnes);
    }

    public void test1Use() {
        modifyRole(BasicRoles.Survivor(), AbilityModifierName.CHARGES, 1); // going to make sure at least one
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, Constants.UNLIMITED);
        modifyRole(BasicRoles.Veteran(), AbilityModifierName.CHARGES, Constants.UNLIMITED);

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller a_vet = addPlayer(BasicRoles.Amnesiac());
        Controller a_vig = addPlayer(BasicRoles.Amnesiac());
        Controller a_sur = addPlayer(BasicRoles.Amnesiac());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.AMNESIAC_KEEPS_CHARGES, false);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        dayStart();

        voteOut(vet, vigi, cit, surv, witch, a_vig);

        shoot(vigi, surv);

        endNight();
        voteOut(vigi, a_vet, a_vig, a_sur, witch);

        setTarget(a_vet, vet); // a1 taking the vet role
        setTarget(a_vig, vigi);
        setTarget(a_sur, surv);

        endNight();

        assertPerceivedChargeRemaining(Constants.UNLIMITED, a_vig, Vigilante.abilityType);
        assertPerceivedChargeRemaining(1, a_sur, Survivor.abilityType);
        assertPerceivedChargeRemaining(Constants.UNLIMITED, a_vet, Veteran.abilityType);
    }

    public void testVestKeepingPreviousRole() {
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller armorsmith = addPlayer(BasicRoles.Armorsmith());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.AMNESIAC_KEEPS_CHARGES, false);

        modifyRole(BasicRoles.Survivor(), AbilityModifierName.CHARGES, 1);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        setTarget(armorsmith, amn);
        vest(surv);

        endNight();

        voteOut(surv, amn, armorsmith, witch);

        setTarget(amn, surv);

        endNight();

        // get one charge from remembering, and keep armorsmith charge
        assertTotalVestCount(2, amn);
    }

    // amnesiac remembering unique cleaned role

    public void testUniqueTargeting() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        voteOut(mayor, cit, amn, ars);

        try{
            setTarget(amn, mayor);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testGunKeepingPreviousRole() {
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller fodder = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
        editRule(SetupModifierName.AMNESIAC_KEEPS_CHARGES, false);
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        setTarget(gs, amn);
        shoot(vigi, fodder);

        endNight();

        voteOut(vigi, amn, gs, witch);

        setTarget(amn, vigi);

        endNight();

        assertTotalGunCount(2, amn);
    }

    public void testKeepCharges() {
        modifyRole(BasicRoles.Veteran(), AbilityModifierName.CHARGES, Constants.UNLIMITED);
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 10);
        modifyRole(BasicRoles.Survivor(), AbilityModifierName.CHARGES, 1); // going to make sure at least one

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller a1 = addPlayer(BasicRoles.Amnesiac());
        Controller a2 = addPlayer(BasicRoles.Amnesiac());
        Controller a3 = addPlayer(BasicRoles.Amnesiac());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.AMNESIAC_KEEPS_CHARGES, true);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        assertEquals(Constants.UNLIMITED, BasicRoles.Veteran().role.getAbility(Veteran.abilityType).modifiers
                .getInt(AbilityModifierName.CHARGES, game));
        assertEquals(1, BasicRoles.Survivor().role.getAbility(Survivor.abilityType).modifiers
                .getInt(AbilityModifierName.CHARGES, game));
        assertEquals(10, BasicRoles.Vigilante().role.getAbility(Vigilante.abilityType).modifiers
                .getInt(AbilityModifierName.CHARGES, game));

        nightStart();
        vest(surv);
        shoot(vigi, surv);

        endNight();

        voteOut(vet, vigi, cit, surv, witch, a2);

        shoot(vigi, surv);

        endNight();
        voteOut(vigi, a1, a2, a3, witch);

        setTarget(a1, vet); // a1 taking the vet role
        setTarget(a2, vigi);
        setTarget(a3, surv);

        endNight();

        assertTotalVestCount(1, a3);
        assertPerceivedChargeRemaining(Constants.UNLIMITED, a1, Veteran.abilityType);
        assertTotalGunCount(8, a2);
    }

    public void testUniqueRoleTaking() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller godfather1 = addPlayer(BasicRoles.Godfather());
        Controller godfather2 = addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));
        Controller amnesiac = addPlayer(BasicRoles.Amnesiac());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.BusDriver());

        nightStart();

        mafKill(godfather1, godfather1);
        mafKill(godfather2, godfather2);
        setTarget(serialKiller, mayor);

        endNight();
        assertInProgress();
        skipDay();

        try{
            setTarget(amnesiac, godfather1);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(amnesiac, godfather2);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(amnesiac, mayor);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testUniqueFactionRoleTaking() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller amnesiac = addPlayer(BasicRoles.Amnesiac());
        Controller goon = addPlayer(BasicRoles.Goon());

        FactionRoleModifierService.upsertModifier(BasicRoles.Witch(), RoleModifierName.UNIQUE, true);

        nightStart();

        voteOut(witch, goon, amnesiac, citizen);

        try{
            setTarget(amnesiac, witch);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testTownAmnesiac() {
        Role role = BasicRoles.Amnesiac().role;
        Faction faction = setup.getFactionByColor(Setup.TOWN_C);

        Controller amn = addPlayer(FactionRoleService.createFactionRole(faction, role));
        Controller town = addPlayer(Hidden.TownRandom());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller maf = addPlayer(BasicRoles.Agent());
        Controller maf2 = addPlayer(BasicRoles.Chauffeur());

        town.getPlayer().clearPreferences();

        dayStart();

        assertTrue(amn.is(Amnesiac.abilityType));

        voteOut(maf, amn, town, witch);

        Action badAction = new Action(amn.getPlayer(), Amnesiac.abilityType, maf.getPlayer());
        assertFalse(amn.getPlayer().isAcceptableTarget(badAction));
        try{
            setTarget(amn, maf);
            fail();
        }catch(PlayerTargetingException e){
        }

        endNight();

        voteOut(maf2, amn, town, witch);

        setTarget(amn, maf2);
        endNight();

        assertEquals(faction.color, amn.getColor());
    }

    public void testAmnesiacMustRemember() {
        Controller amnesiac = addPlayer(BasicRoles.Amnesiac());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.AMNESIAC_MUST_REMEMBER, true);

        mafKill(maf, cit);
        endNight();

        isLoser(amnesiac);
    }

    public void testAmnesiacNoCharge() {
        try{
            modifyAbilityCharges(BasicRoles.Amnesiac(), Amnesiac.abilityType, 1);
            fail();
        }catch(IllegalGameSettingsException e){
        }

        assertFalse(BasicRoles.Amnesiac().role.getAbility(Amnesiac.abilityType).modifiers
                .hasKey(AbilityModifierName.CHARGES));
    }

    public void testGetInvulnerability() {
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller wit = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller amn = addPlayer(BasicRoles.Amnesiac());

        shoot(vig, sk);
        endNight();

        assertTrue(sk.getPlayer().isInvulnerable());
        voteOut(sk, vig, wit, amn);

        setTarget(amn, sk);
        nextNight();

        shoot(vig, amn);
        endNight();

        isAlive(amn);
    }

    public void testLosingUnlimitedVigiShots() {
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());
        Controller gunsmith = addPlayer(BasicRoles.Gunsmith());
        Controller vigilante = addPlayer(BasicRoles.Vigilante());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        FactionRoleModifierService.addModifier(BasicRoles.Vigilante(), AbilityModifierName.CHARGES,
                Vigilante.abilityType, Constants.UNLIMITED);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CULT_KEEPS_ROLES, false);

        setTarget(gunsmith, amn);
        endNight();

        voteOut(vigilante, amn, cultLeader, gunsmith, fodder);

        setTarget(amn, vigilante);
        nextNight();

        setTarget(cultLeader, amn);
        shoot(amn, fodder);
        nextNight();

        shoot(amn, cultLeader);
        nextNight();

        try{
            shoot(amn, gunsmith);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Amnesiac(), AbilityModifierName.BACK_TO_BACK,
                    Amnesiac.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testCleanedUniqueAttempt() {
        Controller amnesiac = addPlayer(BasicRoles.Amnesiac());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller detective = addPlayer(BasicRoles.Detective());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        Controller janitor = addPlayer(BasicRoles.Janitor());

        nightStart();

        assertTrue(mayor.getPlayer().gameRole.modifiers.getBoolean(RoleModifierName.UNIQUE));

        setTarget(janitor, mayor);
        setTarget(serialKiller, mayor);
        nextNight();

        setTarget(amnesiac, mayor);
        setTarget(detective, amnesiac);
        endNight();

        TestDetective.seen(detective, mayor);
        assertFalse(amnesiac.getPlayer().getGameFaction().getColor().equals(mayor.getColor()));
    }
}

package integration.logic.abilities;

import java.util.ArrayList;
import java.util.LinkedList;

import game.abilities.Doctor;
import game.abilities.Poisoner;
import game.ai.Controller;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.RoleService;
import services.SetupModifierService;
import util.models.FactionRoleTestUtil;

public class TestDoctor extends SuperTest {

    public TestDoctor(String name) {
        super(name);
    }

    public void testDoctorFeedbackEvents() {
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller framer = addPlayer(BasicRoles.Framer());

        setTarget(veteran);
        setTarget(doctor, framer);

        mafKill(framer, veteran);

        nextNight();

        endNight();
    }

    public void testDoctorPoisonerRoleDetails() {
        for(Role role: new LinkedList<>(game.setup.roles)){
            if(role.hasAbility(Poisoner.abilityType))
                RoleService.delete(role);
        }

        SetupModifierService.upsertModifier(setup, SetupModifierName.HEAL_BLOCKS_POISON, true);

        ArrayList<String> roleDetails = getPublicDescription(BasicRoles.Doctor());
        partialExcludes(roleDetails, Doctor.ROLE_DETAIL_POISON_CAN_STOP);
        partialExcludes(roleDetails, Doctor.ROLE_DETAIL_POISON_CANNOT_STOP);

        Role role = RoleService.createRole(setup, "Poisoner", Poisoner.abilityType);
        Faction faction = setup.getFactionByColor(Setup.TOWN_C);
        FactionRole factionRole = FactionRoleService.createFactionRole(faction, role);
        this.addRole(factionRole);
        assertTrue(setup.hasSpawnableFactionRoleWithAbility(game, Poisoner.abilityType));

        roleDetails = getPublicDescription(BasicRoles.Doctor());
        partialContains(roleDetails, Doctor.ROLE_DETAIL_POISON_CAN_STOP);
    }

    private static ArrayList<String> getPublicDescription(FactionRole factionRole) {
        return FactionRoleTestUtil.getPublicDescription(factionRole);
    }

    public void testDoctor() {
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        try{
            setTarget(doc1, doc1);
            fail();
        }catch(PlayerTargetingException e){
        }

        mafKill(maf, doc1);
        setTarget(doc1, doc2);
        setTarget(doc2, doc1);
        setTarget(sk, doc2);

        endNight();
        isDead(doc1, doc2);
    }

    public void testDoctorFeedback() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(doc, cit);
        setTarget(sk, cit);
        endNight();

        partialContains(doc, Doctor.SUCCESFULL_HEAL);
    }

    public void testJesterSuicideSave() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller doc3 = addPlayer(BasicRoles.Doctor());
        Controller jest = addPlayer(BasicRoles.Jester());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(jest, doc, doc2, doc3);

        setTarget(doc, doc2);
        setTarget(doc2, doc3);
        setTarget(doc3, doc);
        endNight();

        voteOut(witch, doc, doc2, doc3);

        assertGameOver();
    }

    public void testDocFeedbackPostSave() {
        // someone getting electrocuted after doctor does the heal should still get the
        // notification
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());

        editRule(SetupModifierName.HEAL_FEEDBACK, true);
        editRule(SetupModifierName.HEAL_SUCCESS_FEEDBACK, true);

        electrify(electro, sheriff, cit);

        setTarget(doctor, sheriff);
        setTarget(sheriff, cit);

        endNight();

        isAlive(sheriff);

        partialContains(doctor, Doctor.SUCCESFULL_HEAL);
        partialContains(sheriff, Doctor.TARGET_FEEDBACK);
    }

    public void testImmuneFeedback() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(doc, gf);
        setTarget(sk, gf);
        setTarget(as, cit);
        endNight();

        partialContains(gf, Doctor.TARGET_FEEDBACK);
        partialContains(doc, Doctor.SUCCESFULL_HEAL);

        skipDay();
        setTarget(sk, cit);
        setTarget(doc2, cit);
        vest(cit);

        endNight();
        partialExcludes(cit, Doctor.TARGET_FEEDBACK);
        partialExcludes(doc2, Doctor.SUCCESFULL_HEAL);
    }

    public void testJailorFeedback() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        jail(jailor, cit);
        skipDay();

        setTarget(doc, cit);
        setTarget(sk, cit);

        endNight();
        partialExcludes(cit, Doctor.TARGET_FEEDBACK);
        partialExcludes(doc, Doctor.SUCCESFULL_HEAL);
    }

    public void testDoctorKnowingVestedTargetAttacked() {
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller fodder = addPlayer(BasicRoles.ElectroManiac());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(as, fodder);
        nextNight();

        vest(fodder);
        setTarget(sk, fodder);
        setTarget(doc1, fodder);
        setTarget(doc2, fodder);

        endNight();
        partialExcludes(doc1, Doctor.SUCCESFULL_HEAL);
        partialExcludes(doc2, Doctor.SUCCESFULL_HEAL);

        partialExcludes(fodder, Doctor.SUCCESFULL_HEAL);
    }
}

package integration.logic.abilities;

import game.abilities.DrugDealer;
import game.abilities.Jester;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.SetupModifierName;

public class TestJester extends SuperTest {

    public TestJester(String name) {
        super(name);
    }

    public void testBenignStart() {
        addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Jester());

        assertBadGameSettings();
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller jester = addPlayer(BasicRoles.Jester());

        dayStart();

        voteOut(jester, cit, maf);

        assertInProgress();
        setTarget(maf, cit, KILL);
        endNight();

        assertTrue(game.getLiveSize() <= 1);

        isDead(cit);

        isLoser(cit);
        isWinner(jester);
    }

    public void testPersistance() {
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        voteOut(jester, cit1, cit2, cit3);

        endNight();

        assertFalse(cit1.getPlayer().getVotedForJester());
        assertFalse(cit2.getPlayer().getVotedForJester());
        assertFalse(cit3.getPlayer().getVotedForJester());
    }

    public void testJesterImmunityKilling() {
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller arson = addPlayer(BasicRoles.Jester());
        Controller pois = addPlayer(BasicRoles.Poisoner());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Citizen());

        voteOut(jester, gf, arson, pois);

        endNight();

        assertEquals(3, game.getLiveSize());
    }

    public void testNoKill() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller arson = addPlayer(BasicRoles.Arsonist());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.JESTER_KILLS, 0);
        editRule(SetupModifierName.JESTER_CAN_ANNOY, false);

        nightStart();

        try{
            setTarget(jester, sk);
            fail();
        }catch(NarratorException e){
        }
        endNight();

        voteOut(jester, arson, sk, cit);

        endNight();

        assertEquals(3, game.getLiveSize());
    }

    public void testCrazyVisiting() {
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller dd = addPlayer(BasicRoles.DrugDealer());

        editRule(SetupModifierName.JESTER_CAN_ANNOY, true);

        drug(dd, jester, DrugDealer.ANNOYED);
        setTarget(jester, dd);
        setTarget(lookout, dd);
        endNight();

        partialContains(dd, Jester.FEEDBACK);
        partialContains(jester, Jester.FEEDBACK);
        TestLookout.seen(lookout, jester);
    }

    public void testAllDying() {
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Citizen());
        Controller p4 = addPlayer(BasicRoles.Citizen());
        Controller p5 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.JESTER_KILLS, 40);

        try{
            editRule(SetupModifierName.JESTER_KILLS, Constants.UNLIMITED);
            fail();
        }catch(NarratorException e){
        }
        assertEquals(40, game.getInt(SetupModifierName.JESTER_KILLS));

        editRule(SetupModifierName.JESTER_KILLS, 100);

        voteOut(jester, sk, p1, p2, p3);

        endDay();
        endNight();

        isDead(jester, sk, p1, p2, p3);
        isAlive(p4, p5);
    }
}

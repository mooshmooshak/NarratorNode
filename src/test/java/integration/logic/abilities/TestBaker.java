package integration.logic.abilities;

import java.util.List;

import game.abilities.Baker;
import game.abilities.BreadAbility;
import game.abilities.Doctor;
import game.abilities.Douse;
import game.abilities.Driver;
import game.abilities.DrugDealer;
import game.abilities.SerialKiller;
import game.abilities.Undouse;
import game.abilities.support.Bread;
import game.ai.Controller;
import game.event.Feedback;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Command;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.SetupModifierService;
import util.models.ActionTestUtil;

public class TestBaker extends SuperTest {

    Controller baker;

    @Override
    public void roleInit() {
        baker = addPlayer(BasicRoles.Baker());
    }

    public TestBaker(String name) {
        super(name);
    }

    private void setAlias() {
        SetupModifierService.upsertModifier(setup, SetupModifierName.BREAD_NAME, "soma");
    }

    // double role
    public void testBasicFunction() {
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller maf1 = addPlayer(BasicRoles.Goon());
        Controller maf2 = addPlayer(BasicRoles.Goon());

        setAlias();

        setTarget(baker, vig);

        nextNight();

        assertPassableBreadCount(1, vig);

        shoot(vig, maf1);
        shoot(vig, maf2);

        assertActionSize(2, vig);

        endNight();

        isDead(maf1, maf2);
    }

    public void testWitchGSBaker() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(baker, witch);
        setTarget(gs, cit);

        nextNight();

        List<Feedback> feedbacks = getFeedback(witch.getPlayer(), 0);
        assertFalse(feedbacks.isEmpty());

        shoot(cit, witch);
        witch(witch, cit, cit);
        witch(witch, gs, witch);

        endNight();

        isAlive(witch);
        isDead(cit);

    }

    public void testBreadCommandHandler() {
        Controller a = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        nightStart();
        command(game.skipper, Constants.MOD_ADDBREAD, Integer.toString(2), a.getPlayer().getID());

        assertPassableBreadCount(2, a);

        assertTrue(
                Command.getText(game.getCommands()).contains(Constants.MOD_ADDBREAD + " 2 " + a.getPlayer().getID()));
    }

    public void testBreadTargeting() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.BREAD_PASSING, false);

        setTarget(baker, doc);

        nextNight();

        try{
            bread(doc, witch);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertFalse(doc.getCommands().contains(BreadAbility.abilityType));
        assertTrue(baker.getCommands().contains(BreadAbility.abilityType));

        assertUseableBreadCount(1, doc);

        setTarget(doc, witch, AbilityType.Doctor);
        assertTrue(doc.isTargeting(Doctor.abilityType, witch));

        setTarget(doc, baker, AbilityType.Doctor);
        assertTrue(doc.isTargeting(Doctor.abilityType, baker));
        assertTrue(doc.isTargeting(Doctor.abilityType, witch));

        partialContains(doc, " and ");

        endNight();
    }

    public void testUnstartedGameSetTarget() {
        Controller bd = addPlayer(BasicRoles.BusDriver());

        try{
            setTarget(baker, bd);
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testDriverCancelationWithBread() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(baker, bd);

        nextNight();

        drive(bd, baker, goon);
        cancelAction(bd, 0);
        assertFalse(bd.isTargeting(Driver.abilityType, baker));
        mafKill(goon, goon);

        endNight();

        isDead(goon);
    }

    public void testBakerMultiBread() {
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.BREAD_PASSING, false);

        setAlias();

        nightStart();

        assertUseableBreadCount(0, baker);
        assertUseableBreadCount(0, baker2);

        setTarget(baker, baker2);

        nextNight();

        partialContains(baker2, Bread.GetFeedback(game));
        assertUseableBreadCount(1, baker2);

        setTarget(baker2, cit);
        setTarget(baker2, witch);
        setTarget(baker, cit);

        nextNight();

        assertUseableBreadCount(2, cit, BreadAbility.class);
        assertUseableBreadCount(0, baker, BreadAbility.class);
        assertUseableBreadCount(1, witch, BreadAbility.class);
        assertUseableBreadCount(0, baker, BreadAbility.class);
    }

    public void testWitchForceBreadPassing() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.BREAD_PASSING, true);

        setTarget(baker, cit);
        nextNight();

        witch(witch, cit, witch);
        nextNight();

        assertPassableBreadCount(1, witch);
    }

    public void testBakerCorpse() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller gd = addPlayer(BasicRoles.GraveDigger());

        voteOut(baker, cit1, cit2, gd);

        setTarget(ActionTestUtil.getGraveDiggerAction(gd, BreadAbility.getAction(baker, gd, game)));
        endNight();

        assertPassableBreadCount(1, gd);
    }

    public void testNonBakerCorpse() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller gd = addPlayer(BasicRoles.GraveDigger());

        voteOut(cit1, baker, cit2, gd);

        setTarget(ActionTestUtil.getGraveDiggerAction(gd, BreadAbility.getAction(cit1, gd, game)));
        endNight();

        assertPassableBreadCount(0, gd);
    }

    public void testAmnesiacKeepingBreads() {
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller dead = addPlayer(BasicRoles.SerialKiller());

        dayStart();

        voteOut(dead, baker, witch, amn);

        for(int i = 0; i < 3; i++){
            setTarget(baker, amn);
            nextNight();
        }

        setTarget(amn, dead);

        nextNight();
        assertUseableBreadCount(3, amn);
    }

    public void newNarratorWitchForceVest() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        witch(witch, baker, baker);

        nextNight();

        // originally baker->baker but i want to get rid of the unused errors
        witch(witch, as, sk);
        // cant really force witch to make someone use a vest
    }

    public void testBDDoubleAction() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(baker, bd);

        nextNight();
        drive(bd, sk, c1);
        drive(bd, vig, bd);
        shoot(vig, bd);
        setTarget(sk, c1);

        endNight();

        isDead(sk, vig);
    }

    public void testBreadedAmnesiac() {
        Controller as2 = addPlayer(BasicRoles.Armorsmith());
        Controller as1 = addPlayer(BasicRoles.Armorsmith());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(baker, amn);
        endNight();

        voteOut(as1, as2, baker, lookout, witch);

        endNight();

        voteOut(as2, baker, lookout, witch);

        setTarget(amn, as1);
        setTarget(amn, as2);
        assertActionSize(1, amn);
        assertTrue(amn.getPlayer().getActions().canAddAnotherAction(BreadAbility.abilityType));
        witch(witch, amn, lookout);
        setTarget(lookout, lookout);

        endNight();

        TestLookout.seen(lookout, amn);
    }

    public void testArsonBoom() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller arson = addPlayer(BasicRoles.Arsonist());

        setTarget(arson, as);
        setTarget(baker, arson);
        setTarget(baker2, arson);

        nextNight();
        setTarget(baker, arson);
        setTarget(baker2, arson);

        nextNight();

        setTarget(arson, as, Undouse.abilityType);
        setTarget(arson);
        setTarget(arson, baker);
        setTarget(arson, baker2);

        assertActionSize(4, arson);

        endNight();

        isAlive(as);
        isDead(baker, baker2);
        assertStatus(as, Douse.abilityType, false);
    }

    public void testQuadrupleKill() {
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller baker3 = addPlayer(BasicRoles.Baker());
        Controller baker4 = addPlayer(BasicRoles.Baker());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();

        setTarget(baker, maf);
        setTarget(baker2, maf);
        setTarget(baker3, maf);
        setTarget(baker4, maf);

        nextNight();

        mafKill(maf, baker);
        mafKill(maf, baker2);
        mafKill(maf, baker3);
        mafKill(maf, baker4);

        endNight();
        isDead(baker, baker2, baker3, baker4);
    }

    public void testVigiWithBread() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller witch1 = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
        editRule(SetupModifierName.BREAD_PASSING, false);

        nightStart();
        assertTotalGunCount(1, vigi);

        setTarget(baker, vigi);

        nextNight();

        shoot(vigi, witch1);
        shoot(vigi, witch2);

        assertActionSize(1, vigi);

        witch(witch1, vigi, baker);
        endNight();
        isDead(baker);
        isAlive(witch1, witch2);

        skipDay();

    }

    public void testVigiWithBread2() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller witch1 = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.BREAD_PASSING, true);

        setTarget(baker, vigi);

        nextNight();

        bread(vigi, witch2);
        witch(witch1, vigi, vigi);
        witch(witch2, vigi, vigi);
        endNight();
    }

    public void testDruggedBread() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        drug(dd, sk, DrugDealer.BREAD);
        nextNight();

        assertUseableBreadCount(1, sk);

        setTarget(sk, baker, SerialKiller.abilityType);
        setTarget(sk, mayor, SerialKiller.abilityType);

        assertActionSize(2, sk);

        endNight();

        isDead(baker);
        isAlive(mayor);
    }

    public void testDruggedChargeUseup() {
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller armorsmith = addPlayer(BasicRoles.Armorsmith());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        Controller drugDealer = addPlayer(BasicRoles.DrugDealer());

        drug(drugDealer, vigi, DrugDealer.BREAD);
        setTarget(armorsmith, vigi);
        nextNight();

        shoot(vigi, armorsmith);
        vest(vigi);
        setTarget(serialKiller, vigi);

        endNight();

        isDead(armorsmith, vigi);
    }

    public void testTryingToDoubleBurn() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Poisoner());

        modifyRole(BasicRoles.Survivor(), AbilityModifierName.CHARGES, 2);

        setTarget(baker, arson);

        nextNight();

        setTarget(arson);
        setTarget(arson);

        assertActionSize(1, arson);
    }

    public void testTryingToDoubleDouse() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());

        modifyRole(BasicRoles.Survivor(), AbilityModifierName.CHARGES, 2);

        setTarget(baker, arson);

        nextNight();

        setTarget(arson, baker);
        setTarget(arson, poisoner);

        assertActionSize(2, arson);
    }

    public void testTryingToUseTwoVests() {
        Controller surv = addPlayer(BasicRoles.Survivor());
        addPlayer(BasicRoles.Poisoner());

        modifyRole(BasicRoles.Survivor(), AbilityModifierName.CHARGES, 2);

        setTarget(baker, surv);

        nextNight();

        vest(surv);
        vest(surv);

        assertActionSize(1, surv);
    }

    public void testBasicBreadPassing() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.BREAD_PASSING, true);

        setTarget(baker, cit);

        // baker bread count only counts initialized breads
        assertUseableBreadCount(0, baker);

        nextNight();

        assertPassableBreadCount(1, cit);

        bread(cit, witch);
        endNight();

        assertPassableBreadCount(0, baker);
        assertPassableBreadCount(0, cit);
        assertPassableBreadCount(1, witch);

        skipDay();

        bread(baker, cit);
        bread(baker, witch);
        assertActionSize(1, baker);
    }

    public void testDruggedBreadPassing() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        editRule(SetupModifierName.BREAD_PASSING, true);

        drug(dd, cit, DrugDealer.BREAD);
        nextNight();

        bread(cit, dd);
        setTarget(lookout, dd);
        endNight();

        assertPassableBreadCount(0, dd);
        assertPassableBreadCount(0, cit);
        TestLookout.seen(lookout, cit);
    }

    public void testForcedBreadPassing() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.BREAD_PASSING, true);

        nightStart();

        assertFalse(cit.getPlayer().getActions().canAddAnotherAction(BreadAbility.abilityType, witch.getPlayer()));
        assertTrue(baker.getPlayer().getActions().canAddAnotherAction(BreadAbility.abilityType, cit.getPlayer()));

        setTarget(baker, cit);
        nextNight();

        assertTrue(cit.getPlayer().getActions().canAddAnotherAction(BreadAbility.abilityType, witch.getPlayer()));
        bread(cit, witch);
        assertFalse(cit.getPlayer().getActions().canAddAnotherAction(BreadAbility.abilityType, witch.getPlayer()));
        cancelAction(cit, 0);

        witch(witch, cit, witch);
        endNight();

        assertPassableBreadCount(0, cit);
        assertPassableBreadCount(1, witch);
    }

    public void testTeammatePassing() {
        Role role = BasicRoles.Baker().role;
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);

        Controller witch = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller baker = addPlayer(FactionRoleService.createFactionRole(mafia, role));

        editRule(SetupModifierName.BREAD_PASSING, true);

        nightStart();
        BreadAbility ba = baker.getPlayer().getAbility(BreadAbility.class);
        Bread bread = (Bread) ba.charges.iterator().next();
        assertFalse(bread.isInitialized());

        try{
            setTarget(baker, maf);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(baker, witch);
        nextNight();

        bread(witch, baker);
        nextNight();

        setTarget(baker, maf);
        try{
            setTarget(baker, maf);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(baker, witch);
    }

    public void testSelfMadeUsage() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.SELF_BREAD_USAGE, true);

        setTarget(gs, baker);
        nextNight();

        setTarget(gs, baker);
        nextNight();

        assertPassableBreadCount(3, baker);
        assertTotalGunCount(2, baker);

        shoot(baker, gs);
        shoot(baker, witch);

        assertActionSize(2, baker);

        endNight();

        isDead(gs, witch);
    }

    public void testNoPassingBreadToSelf() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.BREAD_PASSING, true);

        setTarget(baker, cit);
        nextNight();

        try{
            setTarget(cit, cit, BreadAbility.abilityType);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testPassingBreadToSelf() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Chauffeur());

        editRule(SetupModifierName.BREAD_PASSING, true);

        setTarget(baker, cit);
        nextNight();

        setTarget(cit, cit, BreadAbility.abilityType);
        endNight();

        assertPassableBreadCount(1, cit);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Baker(), AbilityModifierName.BACK_TO_BACK,
                    Baker.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Baker(), AbilityModifierName.ZERO_WEIGHTED,
                    Baker.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}

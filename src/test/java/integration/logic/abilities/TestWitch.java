package integration.logic.abilities;

import java.util.List;

import game.abilities.ElectroManiac;
import game.abilities.FactionSend;
import game.abilities.Block;
import game.abilities.Witch;
import game.ai.Controller;
import game.event.Feedback;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.enums.SetupModifierName;
import util.game.ActionUtil;
import util.models.AbilityTestUtil;

public class TestWitch extends SuperTest {

    public TestWitch(String name) {
        super(name);
    }

    public void testBasicFunction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        try{
            setTarget(ActionUtil.getSimpleAction(witch.getPlayer(), Witch.abilityType));
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(sk, witch);
        witch(witch, sk, cit);
        endNight();

        isWinner(witch);
    }

    public void testWitchSoloKillCult() {
        Controller armorsmith = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller cult = addPlayer(BasicRoles.CultLeader());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(armorsmith, gf);
        setTarget(cult, gf);

        GameFaction gfTeam = game.getFaction(gf.getColor());

        nextNight();

        assertFalse(gfTeam.getMembers().isEmpty());

        mafKill(gf, cit);
        witch(witch, gf, cit);

        endNight();

        isDead(cit);
        assertTotalVestCount(1, gf);
    }

    public void testWitchVestNonSenderKill() {
        Controller armorsmith = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller m1 = addPlayer(BasicRoles.Framer());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(armorsmith, gf);

        nextNight();

        witch(witch, gf, cit);
        mafKill(gf, cit);
        setTarget(m1, m2, FactionSend.abilityType);
        mafKill(m2, armorsmith);

        assertFactionController(m2, gf.getColor());

        endNight();

        isAlive(cit);
        isDead(armorsmith);
        assertTotalVestCount(0, gf);
    }

    public void testCancelationText() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Citizen());

        nightStart();
        witch(witch, doc, cit);
        cancelAction(witch, 0);

        witch(witch, doc, cit);
        witch(witch, doc, doc);
        partialContains(witch, "You decided not to make " + doc.getName() + " target " + cit.getName() + ".");
    }

    public void testAntiTownTeam() {
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Citizen());

        dayStart();
    }

    public void testPriority() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller witch1 = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        /*
         * two things being tested here:
         *
         * sk isn't going to target anyone, but the witches want him to do something
         * witch1 wants both to survive witch2 wants to kill the other witch, cuz she's
         * a bitch
         *
         */

        witch(witch1, witch2, cit);
        endNight(witch1);

        witch(witch2, sk, witch1);
        endNight();

        isDead(cit);

        skipDay();

        witch(witch2, sk, witch1);
        endNight(witch2);

        witch(witch1, witch2, cit2);
        endNight();

        assertIsDay();

        isDead(witch1);

        skipDay();

        setTarget(sk, cit3);

        endNight();

        isDead(cit3);
    }

    public void testMafiaChangeKill() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller chf = addPlayer(BasicRoles.Blackmailer());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        send(bm, maf);
        mafKill(maf, witch);
        witch(witch, maf, cit);
        setTarget(doc, cit);

        endNight();
        isAlive(witch);
        skipDay();

        mafKill(bm, witch);
        witch(witch, bm, cit);
        setTarget(doc, cit);

        endNight();
        isAlive(witch);
        skipDay();

        send(maf, chf);
        mafKill(chf, witch);
        witch(witch, chf, cit);

        endNight();
        isDead(cit);
        skipDay();
    }

    public void testDriverChange() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        witch(witch, chf, cit2);
        drive(chf, cit, cit2);

        endNight();
        isAlive(cit, cit2, cit3, chf, witch);
        skipDay();

        witch(witch, chf, cit2);

        try{
            setTarget(chf, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        mafKill(chf, cit3);

        endNight();
        isDead(cit2);
        skipDay();

        witch(witch, chf, cit3);
        mafKill(chf, cit);

        endNight();
        isDead(cit3);
    }

    public void testForceMafiaKill() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        witch(witch, maf, cit);

        endNight();
        isDead(cit);
        isWinner(witch);
    }

    // tests that the extras on the popup show that you originally checked X
    public void testIntendedTarget() {
        addPlayer(BasicRoles.Citizen());
        Controller det = addPlayer(BasicRoles.Detective());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.WITCH_FEEDBACK, false);

        witch(witch, det, det);
        setTarget(det, witch);
        endNight();

        List<Feedback> fbList = getFeedback(det, 0);
        partialContains(fbList.get(0).getExtras(), witch.getName());
    }

    public void testVetInteraction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller witch = addPlayer(BasicRoles.Witch());

        alert(vet);
        witch(witch, vet, cit);
        endNight();

        isDead(witch);
    }

    public void testPrioritization() {
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        addTeamAbility(Setup.CULT_C, Block.abilityType);
        addTeamAbility(Setup.CULT_C, ElectroManiac.abilityType);

        witch(witch, cult, cit);
        assertTrue(AbilityTestUtil.getGameAbility(ElectroManiac.class, game).getExampleAction(cult.getPlayer()).get()
                .is(Game.KILL_ABILITIES));
        endNight();

        isCharged(cit);
    }
}

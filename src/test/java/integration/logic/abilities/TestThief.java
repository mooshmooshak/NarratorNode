package integration.logic.abilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import game.abilities.Citizen;
import game.abilities.FactionKill;
import game.abilities.GameAbility;
import game.abilities.Goon;
import game.abilities.Thief;
import game.ai.Controller;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.Random;
import game.logic.support.RoleAssigner;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import junit.framework.ComparisonFailure;
import models.Faction;
import models.FactionRole;
import models.GeneratedRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.FactionModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.GameService;
import services.RoleService;
import services.SetupModifierService;

public class TestThief extends SuperTest {

    public TestThief(String name) {
        super(name);
    }

    Controller thief;

    @Override
    public void roleInit() {
        thief = super.addPlayer(Thief.template(game.setup.getFactionByColor(Setup.BENIGN_C)));
        int townPriority = setup.getFactionByColor(BasicRoles.Citizen().getColor()).modifiers
                .getInt(FactionModifierName.WIN_PRIORITY, game);
        setTeamRule(BasicRoles.Goon().getColor(), FactionModifierName.WIN_PRIORITY, townPriority);
    }

    public void testGameStartWithMoreRoles2() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 2);
        addRole(BasicRoles.Jester(), 2);

        dayStart();
    }

    public void testThiefSetupMin3() {
        addRole(BasicRoles.Goon());
        addRole(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        GameService.internalStart(game);
    }

    public void testGameStartWithMoreRoles3() {
        addPlayer(BasicRoles.Citizen(), 6);
        addPlayer(BasicRoles.Goon(), 2);
        addRole(BasicRoles.Jester(), 2);

        dayStart();
    }

    public void testThiefDoesntMatter() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 2);

        try{
            dayStart();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testGameStartWithNotEnoughEnemies() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 1);
        addRole(BasicRoles.Jester());

        try{
            dayStart();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testGameStartWithNotEnoughEnemies2() {
        addPlayer(BasicRoles.Citizen(), 5);
        addPlayer(BasicRoles.Goon(), 2);
        addRole(BasicRoles.Jester());
        addRole(BasicRoles.Jester());

        dayStart();

        assertInProgress();
    }

    public void testUniqueThief() {
        addPlayer(BasicRoles.Citizen(), 4);
        addPlayer(BasicRoles.Goon(), 4);
        addRole(BasicRoles.Jester());
        addRole(BasicRoles.Thief());

        assertBadGameSettings();
    }

    public void testThiefCantHaveEnemies() {
        addPlayer(BasicRoles.Citizen(), 4);
        addPlayer(BasicRoles.Goon(), 4);
        addRole(BasicRoles.Jester());
        RoleService.delete(BasicRoles.Thief().role);
        addRole(Thief.template(game.setup.getFactionByColor(Setup.MAFIA_C)));

        assertBadGameSettings();
    }

    public void testThiefPick() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        nightStart(BasicRoles.Goon(), BasicRoles.Citizen());

        assertEquals(GamePhase.ROLE_PICKING_PHASE, game.phase);
        assertTrue(thief.is(Thief.abilityType));

        pickRole(thief, BasicRoles.Goon());
        pickRole(thief, BasicRoles.Citizen());
        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, game.phase);
        assertTrue(Citizen.hasNoAbilities(thief.getPlayer()));

        String happenings = game.getHappenings();
        String thiefPickText = thief.getName() + " became a Town Citizen";
        assertTrue(happenings.contains(thiefPickText));
    }

    public void testHappeningTextDay() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        dayStart(BasicRoles.Goon(), BasicRoles.Citizen());

        assertEquals(GamePhase.ROLE_PICKING_PHASE, game.phase);
        assertTrue(thief.is(Thief.abilityType));

        pickRole(thief, BasicRoles.Goon());
        pickRole(thief, BasicRoles.Citizen());
        endPhase();

        assertTrue(Citizen.hasNoAbilities(thief.getPlayer()));

        String happenings = game.getHappenings();
        String thiefPickText = thief.getName() + " became a Town Citizen";
        assertTrue(happenings.contains(thiefPickText));
    }

    public void testHappeningTextNight() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        nightStart(BasicRoles.Goon(), BasicRoles.Citizen());

        assertEquals(GamePhase.ROLE_PICKING_PHASE, game.phase);
        assertTrue(thief.is(Thief.abilityType));

        pickRole(thief, BasicRoles.Goon());
        pickRole(thief, BasicRoles.Citizen());
        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, game.phase);
        assertTrue(Citizen.hasNoAbilities(thief.getPlayer()));

        String happenings = game.getHappenings();
        String thiefPickText = thief.getName() + " became a Town Citizen";
        assertTrue(happenings.contains(thiefPickText));
    }

    public void testBadRequests() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        addRole(BasicRoles.Goon());
        addRole(BasicRoles.Citizen());

        nightStart(BasicRoles.Goon(), BasicRoles.Citizen());

        assertEquals(GamePhase.ROLE_PICKING_PHASE, game.phase);
        assertTrue(thief.is(Thief.abilityType));

        try{
            pickRole(thief, BasicRoles.Thief());
            fail();
        }catch(NarratorException e){
        }
        try{
            thief.setNightTarget(new Action(thief.getPlayer(), Thief.abilityType, "Mayor", (String) null));
            fail();
        }catch(NarratorException e){
        }
        try{
            setTarget(thief, c1);
            fail();
        }catch(NarratorException e){
        }
        try{
            setTarget(thief);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testPickingMafiaTeam() {
        String mafiaColor = BasicRoles.Goon().getColor();
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        nightStart(BasicRoles.Goon(), BasicRoles.Citizen());

        assertEquals(GamePhase.ROLE_PICKING_PHASE, game.phase);
        assertTrue(thief.is(Thief.abilityType));

        pickRole(thief, BasicRoles.Goon());
        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, game.phase);
        assertTrue(thief.is(Goon.abilityType));
        // this will ensure when executioners targets are being picked, the former thief
        // is eligible to be a target
        assertEquals(2, game.getFaction(mafiaColor)._startingSize);

        setTarget(thief, cit1, FactionKill.abilityType);
        say(thief, gibberish(), mafiaColor);
        partialContains(thief, gibberish);
        partialContains(goon, gibberish);
    }

    public void testThiefNotPicking() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        nightStart(BasicRoles.Goon(), BasicRoles.Citizen());

        assertEquals(GamePhase.ROLE_PICKING_PHASE, game.phase);
        assertTrue(thief.is(Thief.abilityType));

        endPhase();

        assertEquals(GamePhase.NIGHT_ACTION_SUBMISSION, game.phase);
        assertFalse(thief.is(Thief.abilityType));
    }

    public void testOtherThief() {
        Faction faction = setup.getFactionByColor(Setup.BENIGN_C);
        Role thief2 = RoleService.createRole(setup, "Thiefy", Thief.abilityType);
        FactionRole thiefy = FactionRoleService.createFactionRole(faction, thief2);

        addPlayer(thiefy);
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        addRole(BasicRoles.Goon(), 2);
        addRole(BasicRoles.Citizen(), 2);

        assertBadGameSettings();
    }

    public void testForcedChoice() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        int townPriority = setup.getFactionByColor(BasicRoles.Citizen().getColor()).modifiers
                .getInt(FactionModifierName.WIN_PRIORITY, game);
        setTeamRule(BasicRoles.Goon().getColor(), FactionModifierName.WIN_PRIORITY, townPriority + 1);

        nightStart(BasicRoles.Goon(), BasicRoles.Citizen());

        try{
            pickRole(thief, BasicRoles.Citizen());
            fail();
        }catch(NarratorException e){
            assertEquals(
                    "Roles with lower priority are not allowed to be picked.  'Mafia' or 'Goon' are the only allowed choices.",
                    e.getMessage());
        }

        Player thiefPlayer = thief.getPlayer();
        GameAbility thiefAbility = thiefPlayer.getAbility(Thief.class);

        Action exampleAction = thiefAbility.getExampleAction(thiefPlayer).get();
        assertEquals(0, exampleAction.getTargets().size());
        assertNotNull(exampleAction.getArg1());

        String usage = thiefAbility.getUsage(thiefPlayer, new Random());
        try{
            assertEquals(usage, Thief.COMMAND + " Citizen");
        }catch(ComparisonFailure e){
            assertEquals(usage, Thief.COMMAND + " Goon");
        }
    }

    public void testAllRolesUnique() {
        // make all roles in roles list unique
        // thief should be able to get it
    }

    private void dayStart(FactionRole... factionRoleArray) {
        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.DAY_START);
        start(factionRoleArray);
    }

    private void nightStart(FactionRole... factionRoleArray) {
        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.NIGHT_START);
        start(factionRoleArray);
    }

    private void start(FactionRole... factionRoleArray) {
        for(FactionRole factionRole: factionRoleArray)
            this.addRole(factionRole);
        GameService.internalStart(game, new RoleAssigner() {
            @Override
            public ArrayList<GeneratedRole> getPassOutRoles(int playerCount, ArrayList<GeneratedRole> generatedRoles) {
                List<FactionRole> factionRoles = new LinkedList<>(Arrays.asList(factionRoleArray));
                ArrayList<GeneratedRole> passOut = new ArrayList<>();
                for(GeneratedRole role: generatedRoles){
                    if(factionRoles.contains(role.getFactionRole()))
                        factionRoles.remove(role.getFactionRole());
                    else
                        passOut.add(role);
                }
                return passOut;
            }
        });
    }

    private void pickRole(Controller c, FactionRole role) {
        c.setNightTarget(new Action(c.getPlayer(), Thief.abilityType, role.getName(), (String) null));
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Thief(), AbilityModifierName.BACK_TO_BACK,
                    Thief.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Thief(), AbilityModifierName.ZERO_WEIGHTED,
                    Thief.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}

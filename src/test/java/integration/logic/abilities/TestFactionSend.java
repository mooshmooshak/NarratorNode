package integration.logic.abilities;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import game.abilities.Blackmailer;
import game.abilities.ElectroManiac;
import game.abilities.FactionSend;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.condorcet.Condorcet;
import game.logic.support.condorcet.CondorcetGraph;
import game.logic.support.condorcet.Pairing;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;

public class TestFactionSend extends SuperTest {

    public TestFactionSend(String s) {
        super(s);
    }

    Controller goon;

    @Override
    public void roleInit() {
        goon = addPlayer(BasicRoles.Goon());
    }

    public void testBadSends() {
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());

        badSend(goon, goon2, goon2);
    }

    public void testBasic() {
        Controller goon2 = addPlayer(BasicRoles.Goon());
        Controller goon3 = addPlayer(BasicRoles.Goon());
        Controller goon4 = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());

        mafKill(goon, cit);
        mafKill(goon2, cit2);
        mafKill(goon3, detect);
        mafKill(goon4, cit2);
        send(goon, goon, goon3, goon2, goon4);
        send(goon2, goon2, goon3, goon, goon4);
        send(goon3, goon3, goon, goon2, goon4);
        send(goon4, goon3, goon, goon4, goon2);

        assertNotNull(goon.getPlayer().getAction(FactionSend.abilityType));

        PlayerList mafias = goon.getPlayer().getGameFaction().getMembers();
        List<List<Set<Player>>> input = FactionSend.CondorcetOrdering(mafias);

        List<Pairing<Player>> rankings = Condorcet.getPairings(mafias.toSet(), input);
        Collections.sort(rankings);
        CondorcetGraph<Player> acceptedGraph = new CondorcetGraph<Player>(rankings);

        assertNull(acceptedGraph.get(goon3.getPlayer()).winners);

        List<Set<Player>> cList = Condorcet.getOrderings(mafias.toSet(), input);
        assertEquals(1, cList.get(0).size());
        assertEquals(goon3, cList.get(0).iterator().next());

        endNight();

        isDead(detect);
    }

    public void testLotOfPeople() {
        addPlayer(BasicRoles.Goon(), 3);
        Controller cit = addPlayer(BasicRoles.Citizen());

        mafKill(goon, cit);
        endNight();
        isDead(cit);
    }

    public void testGoonPrioritization() {
        Controller goon2 = addPlayer(BasicRoles.Blackmailer());
        Controller goon3 = addPlayer(BasicRoles.Blackmailer());
        Controller goon4 = addPlayer(BasicRoles.Blackmailer());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());

        mafKill(goon, cit);
        mafKill(goon2, cit2);
        mafKill(goon3, detect);
        mafKill(goon4, cit2);
        send(goon, goon, goon3, goon2, goon4);
        send(goon2, goon, goon3, goon2, goon4);
        send(goon3, goon3, goon, goon2, goon4);
        send(goon4, goon3, goon, goon4, goon2);

        endNight();

        assertEquals(1, goon3.getPlayer().getPrevNightTarget().size());

        isDead(cit);
    }

    public void testStalemate() {
        Controller goon2 = addPlayer(BasicRoles.Blackmailer());
        Controller goon3 = addPlayer(BasicRoles.Blackmailer());
        Controller goon4 = addPlayer(BasicRoles.Blackmailer());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());

        nightStart();
        assertEquals(1, goon.getPlayer().getFactions().size());

        mafKill(goon, cit);
        mafKill(goon2, cit2);
        mafKill(goon3, detect);
        mafKill(goon4, cit2);
        send(goon, goon2, goon3, goon4, goon);
        send(goon2, goon, goon3, goon4, goon2);
        send(goon3, goon2, goon, goon4, goon3);
        send(goon4, goon, goon2, goon3, goon4);

        PlayerList factionMembers = goon.getPlayer().getGameFaction().getMembers();
        List<Set<Player>> cList = Condorcet.getOrderings(factionMembers.toSet(),
                FactionSend.CondorcetOrdering(factionMembers));
        assertEquals(3, cList.size());
        assertContains(cList.get(0), goon.getPlayer(), goon2.getPlayer());
    }

    public void testBlackmailed() {
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller goon = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.BusDriver());

        setTarget(bm, goon);
        nextNight();

        badSend(goon, bm, goon);
        badSend(goon, bm);
        send(goon, goon);
    }

    public void testSendMemory() {
        Controller lynched = addPlayer(BasicRoles.Chauffeur());
        Controller goony = addPlayer(BasicRoles.Goon());
        Controller fodder1 = addPlayer(BasicRoles.Detective());
        Controller detect = addPlayer(BasicRoles.Detective());

        send(goon, lynched);
        mafKill(goon, fodder1);
        PlayerList sendOrder = goon.getPlayer().getAction(FactionSend.abilityType).getTargets();
        assertEquals(2, sendOrder.size());
        assertEquals(goon, sendOrder.getFirst());
        assertEquals(lynched, sendOrder.getLast());

        endNight();
        assertEquals(2, goon.getPlayer().getPrevNightTarget().size());

        voteOut(lynched, goony, goon, detect);
        assertActionSize(1, goon);
        endNight();

        voteOut(goony, goon, detect);
        assertActionSize(0, goon);
    }

    public void testBlackmailedHelper() {
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        addPlayer(BasicRoles.Operator());

        setTarget(bm, goon);
        send(goon, bm, goon);
        nextNight();

        assertFalse(goon.getPlayer().getPrevNightTarget(0).isEmpty());

        assertActionSize(0, goon);
        send(goon, goon);
        nextNight();

        assertActionSize(1, goon);
        assertEquals(bm, goon.getPlayer().getAction(FactionSend.abilityType).getTargets().getFirst());
    }

    public void testStaleMate() {
        Controller goon2 = addPlayer(BasicRoles.Goon());
        Controller fodder = addPlayer(BasicRoles.Detective());

        mafKill(goon, fodder);
        mafKill(goon2, fodder);
        endNight();

        isDead(fodder);
    }

    public void testStaleMate2() {
        Controller goon2 = addPlayer(BasicRoles.Goon());
        Controller fodder = addPlayer(BasicRoles.Citizen());

        mafKill(goon, fodder);
        mafKill(goon2, fodder);
        endNight();

        isDead(fodder);
    }

    public void testNoOneWantsIt() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());
        Controller goon2 = addPlayer(BasicRoles.Goon());
        ControllerList fodder = addPlayer(BasicRoles.Citizen(), 3);

        witch(witch, goon2, fodder.get(0));
        endNight();

        isDead(fodder.get(0));
        skipDay();

        witch(witch, goon, fodder.get(1));
        endNight(witch);
        witch(witch2, goon2, witch);
        endNight();

        isDead(fodder.get(1));
        isAlive(witch);
    }

    public void testUsingDifferentActions() {
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());

        addTeamAbility(Setup.MAFIA_C, Blackmailer.abilityType);
        addTeamAbility(Setup.MAFIA_C, ElectroManiac.abilityType);

        setTarget(goon1, cit, Blackmailer.abilityType);
        setTarget(goon2, cit, ElectroManiac.abilityType);

        assertFactionController(goon1, goon1.getColor(), Blackmailer.abilityType);
        assertFactionController(goon2, goon1.getColor(), ElectroManiac.abilityType);
        endNight();

        isCharged(cit);
        assertStatus(cit, Blackmailer.abilityType);
    }

    // blackmailed, front end doesn't send all targets

    private void badSend(Controller sender, Controller... players) {
        try{
            send(sender, players);
            fail();
        }catch(PlayerTargetingException e){
        }
    }
}
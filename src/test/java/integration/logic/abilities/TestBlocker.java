package integration.logic.abilities;

import game.abilities.Block;
import game.ai.Controller;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.RoleModifierName;

public class TestBlocker extends SuperTest {

    public TestBlocker(String name) {
        super(name);
    }

    public void testBasicBlockTest() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller esc = addPlayer(BasicRoles.Escort());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();

        assertTrue(esc.is(Block.abilityType));
        assertFalse(esc.getPlayer().getAcceptableTargets(Block.abilityType).isEmpty());

        setTarget(esc, maf);
        mafKill(maf, cit);

        endNight();

        isAlive(cit);
    }

    public void setBlockable() {
        modifyRole(BasicRoles.Escort(), RoleModifierName.UNBLOCKABLE, false);
        modifyRole(BasicRoles.Consort(), RoleModifierName.UNBLOCKABLE, false);
    }

    public void testSelfBlock() {
        addPlayer(BasicRoles.Citizen());
        Controller esc = addPlayer(BasicRoles.Escort());
        Controller witch = addPlayer(BasicRoles.Witch());

        setBlockable();

        witch(witch, esc, esc);

        endNight();
    }

    public void testBlockersCanBeRoleBlocked() {
        Controller esc = addPlayer(BasicRoles.Escort());
        Controller con = addPlayer(BasicRoles.Consort());
        Controller maf = addPlayer(BasicRoles.Chauffeur());

        setBlockable();

        nightStart();

        setTarget(con, esc);
        send(con, maf);
        endNight(con);

        setTarget(esc, maf);

        mafKill(maf, esc);

        endNight();

        isWinner(con);
    }

    public void setBlockImmune() {
        modifyRole(BasicRoles.Consort(), RoleModifierName.UNBLOCKABLE, true);
        modifyRole(BasicRoles.Escort(), RoleModifierName.UNBLOCKABLE, true);
    }

    public void testBlockersCantBeRoleBlocked() {
        setBlockImmune();

        Controller esc = addPlayer(BasicRoles.Escort());
        Controller con = addPlayer(BasicRoles.Consort());
        Controller maf = addPlayer(BasicRoles.Chauffeur());

        setTarget(con, esc);
        setTarget(con, maf, SEND);
        endNight(con);
        setTarget(esc, maf);
        mafKill(maf, esc);

        endNight();

        assertInProgress();
    }

    public void testBlockingVeteran() {
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller cons = addPlayer(BasicRoles.Consort());
        Controller gs = addPlayer(BasicRoles.Gunsmith());

        setTarget(gs, vet);
        nextNight();

        shoot(vet, cons);
        setTarget(cons, vet);

        endNight();

        assertInProgress();
        isAlive(cons);
    }
}

package integration.logic.abilities;

import game.abilities.Marshall;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.GameService;

public class TestMarshall extends SuperTest {

    public TestMarshall(String name) {
        super(name);
    }

    Controller marshall;

    @Override
    public void roleInit() {
        marshall = addPlayer(BasicRoles.Marshall());
    }

    private void order() {
        order(marshall);
    }

    private void setExtraLynches(int i) {
        editRule(SetupModifierName.MARSHALL_EXECUTIONS, i);
    }

    public void testBasics() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller agent = addPlayer(BasicRoles.Agent());

        order();

        try{
            order();
            fail();
        }catch(NarratorException e){
        }

        voteOut(c1, marshall, c2, agent);

        try{
            vote(c1, c2);
            fail();
        }catch(NarratorException e){
        }

        assertIsDay();
        isDead(c1);
        assertVoteTarget(null, marshall);

        voteOut(c2, marshall, agent);

        assertIsNight();
        isDead(c2);
    }

    public void testFewerLynchesThanLiving() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller agent = addPlayer(BasicRoles.Agent());

        setExtraLynches(1);

        order();

        voteOut(c1, c2, c3, agent);
        voteOut(c2, c3, agent, marshall);

        assertIsNight();
    }

    public void testParity() {
        GameService.upsertModifier(game, GameModifierName.AUTO_PARITY, true);
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        setExtraLynches(3);

        order();

        voteOut(cit1, cit2, maf1, marshall);
        assertIsNight();
    }

    public void testEndGame() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());

        setExtraLynches(3);

        order();

        voteOut(maf1, cit1, marshall);

        assertGameOver();
        try{
            vote(cit1, marshall);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testOrderingTodayTomorrow() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        modifyAbilityCharges(BasicRoles.Marshall(), 2);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        dayStart();
        assertRealChargeRemaining(2, marshall, Marshall.abilityType);

        order();
        assertRealChargeRemaining(1, marshall, Marshall.abilityType);

        skipDay();
        endNight();

        order();
        assertRealChargeRemaining(0, marshall, Marshall.abilityType);

        skipDay();
        endNight();

        try{
            order();
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Marshall(), AbilityModifierName.BACK_TO_BACK,
                    Marshall.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Marshall(), AbilityModifierName.ZERO_WEIGHTED,
                    Marshall.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    // visual checks
    // no graveyard showing up, no announcement
    // no nonpositive numbers when manipulating marshall lynches

}

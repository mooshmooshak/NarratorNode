package integration.logic.abilities;

import java.util.List;

import game.abilities.Architect;
import game.abilities.Burn;
import game.abilities.DrugDealer;
import game.abilities.JailCreate;
import game.abilities.JailExecute;
import game.ai.Controller;
import game.event.ArchitectChat;
import game.event.ChatMessage;
import game.event.Message;
import game.logic.Player;
import game.logic.exceptions.ChatException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Command;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.RoleService;

public class TestArchitect extends SuperTest {

    public TestArchitect(String name) {
        super(name);
    }

    Controller arch;

    @Override
    public void roleInit() {
        arch = addPlayer(BasicRoles.Architect());
    }

    public void testBasicFunction() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        arch(arch, cit1, witch);
        skipDay();

        assertChatKeysSize(2, cit1);
        partialExcludes(cit1.getChatKeys(), "Jail");

        assertChatKeysSize(1, arch);
        partialExcludes(arch.getChatKeys(), "Jail");

        endNight();

        // night chat, day chat, arch chat
        assertChatLogSize(3, arch);

        assertTrue(arch.getCommands().contains(Architect.abilityType));

        assertFalse(arch.getPlayer().getAcceptableTargets(Architect.abilityType).isEmpty());
    }

    public void testNoSubmittingDeadActions() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        mafKill(goon, arch);
        endNight();

        try{
            arch(arch, cit, goon);
            fail();
        }catch(NarratorException e){
        }

        partialExcludes(Command.getText(game.getCommands()), Architect.COMMAND);
    }

    public void cancelingArchTarget() {
        Controller arch = addPlayer(BasicRoles.Architect());
        Controller p1 = addPlayer(BasicRoles.Agent());
        Controller p2 = addPlayer(BasicRoles.SerialKiller());

        arch(arch, p1, p2);
        cancelAction(arch, 0);

        skipDay();

        noArchChat(p1, p2);
    }

    private void archAction(Controller... targets) {
        super.arch(arch, targets);
    }

    public void testRemoveFromChats() {
        Controller masonLeader = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller kidnapper = addPlayer(BasicRoles.Kidnapper());
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.ARCH_QUARANTINE, true);

        archAction(masonLeader, kidnapper);
        skipDay();

        assertFalse(kidnapper.getChatKeys().contains(kidnapper.getColor()));
        assertFalse(masonLeader.getChatKeys().contains(masonLeader.getColor()));
        assertFalse(mason.getChatKeys().contains(mason.getColor()));

        ChatMessage message = say(goon, gibberish(), Setup.MAFIA_C);
        assertFalse(game.getEventManager().getNightLog(Setup.MAFIA_C).hasAccess(kidnapper.getName()));

        List<Message> messages = kidnapper.getPlayer().getEvents().events;
        assertFalse(messages.contains(message));
    }

    public void testCompetingQuarintine() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller arch2 = addPlayer(BasicRoles.Architect());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.ARCH_QUARANTINE, true);

        arch(arch2, cit, ml);
        arch(arch, mason, cit);
        skipDay();

        assertChatKeysSize(1, mason);
    }

    public void testIllegalActions() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller town = addPlayer(BasicRoles.BusDriver());
        Controller arc = addPlayer(BasicRoles.Architect());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller witch = addPlayer(BasicRoles.Witch());

        mafKill(maf, maf);
        endNight();

        assertTrue(arch.getCommands().contains(Architect.abilityType));

        try{
            archAction(maf, town);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            archAction(cit1);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            archAction(town, town);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            archAction(town, cit1, witch);
            fail();
        }catch(PlayerTargetingException e){
        }

        skipDay();

        assertFalse(arc.getPlayer().getAcceptableTargets(Architect.abilityType).isEmpty());
    }

    protected static void selfTarget(boolean b) {
        SuperTest.modifyAbilitySelfTarget(BasicRoles.Architect(), b);
    }

    public void testSelfTarget() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Poisoner());

        selfTarget(true);

        arch(arch, arch, cit2);
    }

    public void testNoSelfTarget() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Poisoner());

        selfTarget(false);

        try{
            arch(arch, arch, cit2);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testSpeakingToClosedChat() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller arc = arch;

        arch(arc, c1, c2);
        skipDay();

        String key = ArchitectChat.GetKey(c1, c2, game.getDayNumber());

        ArchitectChat ac = (ArchitectChat) game.getEventManager().getNightLog(key);

        String gib1 = gibberish(), gib2 = gibberish();
        say(c1, gib1, key);
        say(c2, gib2, key);
        try{
            say(witch, "this won't work", key);
            fail();
        }catch(ChatException e){
        }
        try{
            say(arc, "this won't work YET", key);
            fail();
        }catch(ChatException e){
        }

        partialContains(c1, gib1);
        partialContains(c1, gib2);
        partialContains(c2, gib1);
        partialContains(c2, gib2);
        partialExcludes(arc, gib1);
        partialExcludes(arc, gib2);
        partialExcludes(witch, gib1);
        partialExcludes(witch, gib2);

        endNight();

        assertIsDay();
        assertFalse(ac.isActive());
        assertTrue(ac.hasAccess(arc.getPlayer().getID()));

        try{
            say(c1, "throwback", key);
            fail();
        }catch(ChatException e){
        }

        skipDay();

        try{
            say(c1, "throwback2", key);
            fail();
        }catch(ChatException e){
        }
    }

    public void testTwoArchitects() {
        Controller p1 = addPlayer(BasicRoles.Poisoner());
        Controller p2 = addPlayer(BasicRoles.ElectroManiac());
        Controller arc1 = addPlayer(BasicRoles.Architect());
        Controller arc2 = addPlayer(BasicRoles.Architect());

        arch(arc1, p1, p2);
        arch(arc2, p1, p2);
        skipDay();

        assertChatKeysSize(2, p1);
        assertChatLogSize(3, p2);
    }

    public void testBreadedArchitect() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller p1 = addPlayer(BasicRoles.Poisoner());
        Controller p3 = addPlayer(BasicRoles.ElectroManiac());
        Controller p4 = addPlayer(BasicRoles.Amnesiac());

        setTarget(baker, arch);
        endNight();

        arch(arch, p1, baker);
        arch(arch, p3, p4);

        skipDay();
        endNight();

        assertChatKeysSize(1, arch);
        // arc1,2 chat, day chat, night chat
        assertChatLogSize(4, arch);
    }

    public void testDeadArchParticipants() {
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        Controller arch = addPlayer(BasicRoles.Architect());
        Controller arch2 = addPlayer(BasicRoles.Architect());
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller fodder2 = addPlayer(BasicRoles.Citizen());

        setTarget(poisoner, fodder1);
        endNight();

        arch(arch, fodder1, fodder2);
        skipDay();

        // fodder2 should be dead
        assertEquals(1, fodder2.getChatKeys().size());

        setTarget(poisoner, arch);
        endNight();

        arch(arch, fodder2, arch2);
        skipDay();

        assertEquals(1, fodder2.getChatKeys().size());

        setTarget(poisoner, fodder2);
        endNight();

        arch(arch2, poisoner, fodder2);
        skipDay();

        // void and dead
        assertEquals(2, fodder2.getChatKeys().size());
    }

    public void testJailorActingFirst() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller f1 = addPlayer(BasicRoles.Amnesiac());
        Controller f2 = addPlayer(BasicRoles.Witch());

        // jailing arch
        jail(jailor, arch);
        arch(arch, f1, f2);
        skipDay();

        noArchChat(f1, f2);
        endNight();

        arch(arch, f1, f2);
        jail(jailor, arch);
        skipDay();

        noArchChat(f1, f2);
        endNight();

        // jailing arch participant
        jail(jailor, f1);
        arch(arch, f1, f2);
        skipDay();

        noArchChat(f1, f2);
        endNight();

        arch(arch, f1, f2);
        jail(jailor, f1);
        skipDay();

        noArchChat(f1, f2);
        endNight();
    }

    public void testFakeBread() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller arch = addPlayer(BasicRoles.Architect());
        Controller fod1 = addPlayer(BasicRoles.Citizen());
        Controller fod2 = addPlayer(BasicRoles.Citizen());

        drug(dd, arch, DrugDealer.BREAD);
        endNight();

        arch(arch, dd, baker);
        arch(arch, fod1, fod2);

        assertDayActionSize(2);

        skipDay();

        isArchChat(dd, baker);
        noArchChat(fod1, fod2);
    }

    public void testArchChatKeys() {
        Controller pois = addPlayer(BasicRoles.Poisoner());
        Controller elec = addPlayer(BasicRoles.ElectroManiac());

        arch(arch, pois, elec);
        skipDay();

        assertIsNight();

        assertChatKeys(2, pois);
        isArchChat(pois, elec);

        endNight();
        assertIsDay();

        arch(arch, pois, elec);
        assertDayActionSize(1);
        skipDay();
        assertIsNight();

        isArchChat(pois, elec);

        assertChatKeys(2, pois);
        assertChatLogSize(4, pois);

        endNight();

        assertChatLogSize(4, pois);
    }

    public void testArchitectCharges() {
        modifyAbilityCharges(BasicRoles.Architect(), Architect.abilityType, 2);
        modifyAbilityCooldown(BasicRoles.Architect(), Architect.abilityType, 1);

        Controller arch = addPlayer(BasicRoles.Architect());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
        jail(jailor, fodder);
        arch(arch, jailor, fodder);

        assertPerceivedChargeRemaining(2, arch, Architect.abilityType);

        skipDay();

        assertPerceivedChargeRemaining(2, arch, Architect.abilityType);
        assertFalse(fodder.getPlayer().inArchChat());
        assertFalse(jailor.getPlayer().inArchChat());
        endNight();

        arch(arch, jailor, fodder);
        skipDay();

        assertPerceivedChargeRemaining(1, arch, Architect.abilityType);
        endNight();

        // on cooldown, but still has charges
        assertFalse(arch.getCommands().contains(Architect.abilityType));
        skipDay();
        endNight();

        assertTrue(arch.getCommands().contains(Architect.abilityType));
        arch(arch, jailor, fodder);
        skipDay();

        assertPerceivedChargeRemaining(0, arch, Architect.abilityType);
        endNight();
        skipDay();
        endNight();

        assertFalse(arch.getCommands().contains(Architect.abilityType));
    }

    public void testReplace() {
        Role role = RoleService.createRole(setup, "ArchiJailor", Architect.abilityType, JailExecute.abilityType);
        Faction faction = setup.getFactionByColor(Setup.TOWN_C);
        Player architect = addPlayer(FactionRoleService.createFactionRole(faction, role)).getPlayer();
        Player baker = addPlayer(BasicRoles.Baker()).getPlayer();
        Player mafioso = addPlayer(BasicRoles.Goon()).getPlayer();

        arch(architect, baker, mafioso);

        assertEquals(4, architect.gameRole._abilities.size());

        Action oldAction = architect.getAction(Architect.abilityType);
        Action newAction = new Action(architect, Architect.abilityType, architect, baker);
        architect.getActions().replace(oldAction, newAction);

        assertTrue(game.actionStack.contains(newAction));
        assertFalse(game.actionStack.contains(oldAction));

        cancelAction(architect, 0);

        assertTrue(game.actionStack.isEmpty());

        arch(architect, baker, mafioso);
        oldAction = architect.getAction(Architect.abilityType);
        newAction = new Action(architect, JailCreate.abilityType, baker);

        architect.getActions().replace(oldAction, newAction);
        assertTrue(game.actionStack.contains(newAction));
        assertFalse(game.actionStack.contains(oldAction));
    }

    public void testBreadedArchitect2() {
        Controller architect = addPlayer(BasicRoles.Architect());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller mafioso = addPlayer(BasicRoles.Goon());
        Controller witch1 = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Witch());

        setTarget(baker, architect);
        endNight();

        arch(architect, baker, mafioso);

        assertTrue(architect.isTargeting(Architect.abilityType, baker, mafioso));
        arch(architect, baker, witch1);

        skipDay();

        assertPassableBreadCount(0, architect);
    }

    private Controller fireInArchChat(boolean archChatSpread) {
        Controller arso = addPlayer(BasicRoles.Arsonist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.ARCH_BURN, archChatSpread);

        setTarget(arso, cit);
        endNight();

        arch(arch, cit, cit2);
        skipDay();

        burn(arso);
        endNight();
        return cit2;
    }

    public void testFireInArchChatOff() {
        Controller cit = fireInArchChat(false);
        isAlive(cit);
    }

    public void testFireInArchChat() {
        Controller cit = fireInArchChat(true);
        isDead(cit);
    }

    public void testFireInArchChatInvul() {
        Controller arso = addPlayer(BasicRoles.Arsonist());
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.ARCH_BURN, true);

        setTarget(arso, em);
        endNight();

        arch(arch, em, cit2);
        skipDay();

        burn(arso);
        endNight();

        isDead(cit2);
    }

    public void testFireInArchChatBoth() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller arson = addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.ARCH_BURN, true);

        nightStart();

        List<String> arsonRoleSpecs = ((Player) arson).getRoleCardDetails();
        partialIncludes(arsonRoleSpecs, Burn.ARCHITECT_ROOM_SPREAD_TEXT);
        partialExcludes(arsonRoleSpecs, Burn.ARCHITECT_ROOM_NO_SPREAD_TEXT);

        setTarget(arson, cit);
        nextNight();
        setTarget(arson, cit2);
        endNight();

        arch(arch, cit, cit2);
        skipDay();

        burn(arson);
        endNight();

        isDead(cit2, cit);

        assertAttackSize(1, cit);
        assertAttackSize(1, cit2);
    }

    public void testBackToBackModifier() {
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon());

        FactionRoleModifierService.addModifier(BasicRoles.Architect(), AbilityModifierName.BACK_TO_BACK,
                Architect.abilityType, false);

        arch(arch, goon1, goon2);
        skipDay();
        endNight();

        try{
            arch(arch, goon2, goon1);
            fail();
        }catch(NarratorException e){
        }

        skipDay();
        endNight();

        arch(arch, goon1, goon2);
    }

    private void noArchChat(Controller p1, Controller p2) {
        String key = ArchitectChat.GetKey(p1, p2, game.getDayNumber());
        assertFalse(p1.getChatKeys().contains(key));
        assertFalse(p2.getChatKeys().contains(key));
    }

    private void isArchChat(Controller p1, Controller p2) {
        String key = ArchitectChat.GetKey(p1, p2, game.getDayNumber());
        assertTrue(p1.getChatKeys().contains(key));
        assertTrue(p2.getChatKeys().contains(key));
    }

}

package integration.logic.abilities;

import game.abilities.Block;
import game.abilities.CultLeader;
import game.abilities.FactionKill;
import game.abilities.GraveDigger;
import game.abilities.ProxyKill;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.RoleService;

public class TestProxyKill extends SuperTest {

    public TestProxyKill(String name) {
        super(name);
    }

    public void testCLKillMashSend() {
        Faction cultFaction = setup.getFactionByColor(Setup.CULT_C);
        Role role = RoleService.createRole(setup, "SavageLeader", CultLeader.abilityType, ProxyKill.abilityType);
        FactionRole mashedRole = FactionRoleService.createFactionRole(cultFaction, role);

        Controller cl = addPlayer(mashedRole);
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        Controller strip = addPlayer(BasicRoles.Escort());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.CULT_PROMOTION, true);
        editRule(SetupModifierName.CONVERT_REFUSABLE, false);
        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);
        modifyAbilityCooldown(BasicRoles.CultLeader(), CultLeader.abilityType, 0);

        setTarget(cl, cit, CultLeader.abilityType);
        nextNight();

        assertTrue(cl.getPlayer().hasAbility(ProxyKill.abilityType));
        assertFalse(cit.getPlayer().hasAbility(ProxyKill.abilityType));
        assertEquals(2, cl.getCommands().size());
        assertTrue(cit.getCommands().isEmpty());

        proxySend(cl, cl, fodder);
        nextNight();

        isDead(fodder);

        TestGhost.assertCause(fodder, cl);

        proxySend(cl, cit, fodder2);
        setTarget(lookout, fodder2);
        endNight();

        isDead(fodder2);
        TestLookout.seen(lookout, cit);

        skipDay();

        setTarget(strip, cit);
        proxySend(cl, cit, strip);
        endNight();

        isAlive(strip);
        partialExcludes(cl, Block.FEEDBACK);
        partialContains(cit, Block.FEEDBACK);

        skipDay();

        setTarget(strip, cl);
        setTarget(baker, cl);
        proxySend(cl, cit, strip);
        endNight();

        isDead(strip);

        skipDay();

        proxySend(cl, cit, lookout);
        proxySend(cl, cit, baker);
        assertActionSize(2, cl);

        endNight();

        isDead(baker, lookout);
        assertPassableBreadCount(0, cl);
    }

    private void proxySend(Controller owner, Controller killer, Controller target) {
        setTarget(owner, ControllerList.list(killer, target), ProxyKill.abilityType);
    }

    private static FactionRole ProxyKill() {
        Faction cult = setup.getFactionByColor(Setup.CULT_C);
        Role role = RoleService.createRole(setup, "ProxyKill", ProxyKill.abilityType);
        return FactionRoleService.createFactionRole(cult, role);
    }

    public void testBadTargeting1() {
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller px = addPlayer(ProxyKill());
        addPlayer(BasicRoles.Goon());

        try{
            proxySend(px, fodder1, fodder1);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(px, ControllerList.list(px), ProxyKill.abilityType);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            setTarget(px, ControllerList.list(px, px, px, px), ProxyKill.abilityType);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testElectroProxyKill() {
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        Controller px = addPlayer(ProxyKill());
        Controller maf = addPlayer(BasicRoles.Cultist());
        Controller cit = addPlayer(BasicRoles.Citizen());

        electrify(em, maf, cit);
        proxySend(px, maf, cit);
        endNight();

        isDead(maf);
        isAlive(px, em);
    }

    public void testWitchProxyKill() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller px = addPlayer(ProxyKill());
        Controller maf = addPlayer(BasicRoles.Cultist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller lookout2 = addPlayer(BasicRoles.Lookout());

        witch(witch, px, cit);
        setTarget(lookout, cit);
        proxySend(px, maf, cit2);
        setTarget(lookout2, cit2);
        endNight();

        isDead(cit2);
        isAlive(cit);
        TestLookout.seen(lookout, px);
        TestLookout.seen(lookout2, maf);

        skipDay();

        witch(witch, maf, cit);
        proxySend(px, maf, witch);
        endNight();

        isAlive(witch);
        isDead(cit);
    }

    public void testGainingNightKill() {
        Controller prox = addPlayer(ProxyKill());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifierName.PROXY_UPGRADE, true);

        reveal(mayor);
        vote(cult, prox);
        vote(mayor, prox);

        mafKill(cult, mayor);
        endNight();

        isDead(mayor);
        skipDay();

        assertCooldownRemaining(0, cult, FactionKill.abilityType);
    }

    public void testNoGainingNightKill() {
        Controller prox = addPlayer(ProxyKill());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifierName.PROXY_UPGRADE, false);

        reveal(mayor);
        vote(cult, prox);
        vote(mayor, prox);

        try{
            mafKill(cult, mayor);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testCorpseProxy() {
        Controller prox = addPlayer(ProxyKill());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller gd = addPlayer(BasicRoles.GraveDigger());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifierName.PROXY_UPGRADE, true);

        reveal(mayor);
        vote(cult, prox);
        vote(gd, prox);
        vote(mayor, prox);

        mafKill(cult, mayor);
        setTarget(gd, GraveDigger.abilityType, null, prox, cult);
        endNight();

        isDead(mayor, cult, prox);
    }
}

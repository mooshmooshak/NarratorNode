package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.Citizen;
import game.abilities.Godfather;
import game.abilities.Snitch;
import game.abilities.Vigilante;
import game.ai.Controller;
import game.event.Message;
import game.event.SnitchAnnouncement;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;

public class TestSnitch extends SuperTest {

    public TestSnitch(String s) {
        super(s);
    }

    public void testSnitchBasic() {
        ArrayList<Message> announcements = addAnnouncementListener();

        Controller consort = addPlayer(BasicRoles.Consort());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Citizen());

        nightStart();
        setTarget(snitch, poisoner);
        shoot(vig, snitch);
        setTarget(consort, snitch);

        endNight();
        assertEquals(2, announcements.size());
    }

    public void testSnitchBread() {
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller snitch2 = addPlayer(BasicRoles.Snitch());
        Controller consort = addPlayer(BasicRoles.Consort());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit4 = addPlayer(BasicRoles.Citizen());

        ArrayList<Message> announcements = addAnnouncementListener();

        nightStart();

        setTarget(baker, snitch);
        setTarget(baker2, snitch2);

        nextNight();

        mafKill(consort, snitch);
        reveal(snitch, doctor);
        reveal(snitch, cit2);
        reveal(snitch2, cit3);
        reveal(snitch2, cit4);

        endNight();

        assertEquals(5, announcements.size());

        skipDay();

        mafKill(consort, snitch2);
        endNight();

        assertEquals(8, announcements.size());
    }

    public void testSnitchWitch() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller vigi = addPlayer(BasicRoles.Vigilante());

        nightStart();

        ArrayList<Message> announcements = addAnnouncementListener();

        witch(witch, snitch, citizen);
        setTarget(snitch, vigi);
        shoot(vigi, snitch);

        endNight();

        String message;
        for(Message a: announcements){
            message = a.access(Constants.PUBLIC);
            if(message.contains(Citizen.class.getSimpleName()))
                return;
            if(message.contains(Vigilante.class.getSimpleName()))
                fail();
        }
        fail();
    }

    public void testSnitchOnGodfather() {
        Controller godfather = addPlayer(BasicRoles.Godfather());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        addPlayer(BasicRoles.Poisoner());
        addPlayer(BasicRoles.Citizen());

        setTarget(snitch, godfather);
        mafKill(godfather, snitch);

        ArrayList<Message> announcements = addAnnouncementListener();

        endNight();

        SnitchAnnouncement(announcements, Citizen.class.getSimpleName(), Godfather.class.getSimpleName());

        for(Message m: game.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof SnitchAnnouncement){
                assertTrue(m.isNightToDayAnnouncement());
            }
        }
    }

    public static void SnitchAnnouncement(ArrayList<Message> announcements, String shouldBe, String cantBe) {
        String message;
        for(Message a: announcements){
            if(!(a instanceof SnitchAnnouncement))
                continue;
            message = a.access(Constants.PUBLIC);
            message = message.substring(message.indexOf("to be a"));
            if(message.contains(shouldBe))
                return;
            if(message.contains(cantBe))
                fail();
        }
        fail();
    }

    // godfather, but no citizens possible

    public void testSnitchArsonGameOver() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.ARSON_DAY_IGNITES, 1);

        setTarget(arson, snitch);
        setTarget(snitch, arson);

        endNight();

        ArrayList<Message> announcements = addAnnouncementListener();
        burn(arson);

        assertEquals(2, announcements.size());
        for(Message m: game.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof SnitchAnnouncement){
                assertFalse(m.isNightToDayAnnouncement());
            }
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Snitch(), AbilityModifierName.BACK_TO_BACK,
                    Snitch.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    private void reveal(Controller snitch, Controller target) {
        setTarget(snitch, target, Snitch.abilityType);
    }
}

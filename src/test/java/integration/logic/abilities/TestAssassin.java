package integration.logic.abilities;

import game.abilities.Assassin;
import game.abilities.FactionKill;
import game.abilities.FactionSend;
import game.ai.Controller;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.GameModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.GameService;
import util.TestUtil;

public class TestAssassin extends SuperTest {

    public TestAssassin(String s) {
        super(s);
    }

    public void testBasicAbility() {
        addPlayer(BasicRoles.Citizen());
        Controller voteFodder = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());

        dayStart();

        assertTrue(assassin.getPlayer().hasDayAction(Assassin.abilityType));

        try{
            doDayAction(assassin, assassin);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            this.doDayAction(assassin, Assassin.abilityType, c3, voteFodder);
            fail();
        }catch(NarratorException e){

        }

        doDayAction(assassin, c3);

        isDead(c3);

        vote(voteFodder, assassin);
        assertTrue(game.getCommands().get(0).text.toLowerCase().contains(Assassin.COMMAND.toLowerCase()));
    }

    public void testNoDeadTargeting() {
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);

        mafKill(assassin, citizen);
        endNight();

        isDead(citizen);

        try{
            doDayAction(assassin, citizen);
            fail();
        }catch(NarratorException e){
        }

    }

    public void testRemoveVotes() {
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);

        vote(assassin, citizen);
        doDayAction(assassin, citizen);

        assertEmpty(game.voteSystem.getVoteTargets(assassin.getPlayer()));
    }

    public void testAssassinNightKillUnselect() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Lookout());
        Controller assass = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Assassin());

        mafKill(assass, c1);

        cancelAction(assass, 1);

        assertFalse(assass.getPlayer().getActions().isTargeting(assass.getPlayer(), FactionSend.abilityType));

        // used to throw index out of bounds exception
        Action oldA = assass.getPlayer().getAction(FactionKill.abilityType);
        Action newA = new Action(assass.getPlayer(), FactionKill.abilityType, c2.getPlayer());
        assass.getPlayer().getActions().replace(oldA, newA);
    }

    public void testReverseParse() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller assass = addPlayer(BasicRoles.Assassin());

        mafKill(assass, c1);
        assertActionSize(1, assass);

        endNight();

        isDead(c1);
    }

    public void testCommand() {
        Controller c3 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        Controller ass = addPlayer(BasicRoles.Assassin());

        dayStart();

        command(ass, Assassin.COMMAND + " " + c3.getName());

        isDead(c3);
    }

    public void testEndDay() {
        addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller ass = addPlayer(BasicRoles.Assassin());

        dayStart();

        doDayAction(ass, c3);

        isDead(c3);

        isWinner(ass);
    }

    public void testGameEnd() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Goon(), 5);

        dayStart();
        doDayAction(assassin, cit);

        assertGameOver();
    }

    public void testAssassinTargeting() {
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Citizen());

        try{
            doDayAction(assassin, goon);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testEndGameNightlessAssassin() {
        addPlayer(BasicRoles.Citizen());
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Assassin());

        GameService.upsertModifier(game, GameModifierName.NIGHT_LENGTH, 0);
        removeTeamAbility(Setup.MAFIA_C, FactionKill.abilityType);
        assertFalse(setup.getFactionByColor(Setup.MAFIA_C).hasSharedAbilities());

        doDayAction(p3, p1);

        assertGameOver();
    }

    public void testAssassinTargetingEnemyHasAssassin() {
        Role assassinRole = BasicRoles.Assassin().role;
        Faction town = setup.getFactionByColor(Setup.TOWN_C);
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller sin2 = addPlayer(FactionRoleService.createFactionRole(town, assassinRole));
        Controller cit = addPlayer(BasicRoles.Citizen());

        doDayAction(assassin, goon);
        doDayAction(sin2, cit);
    }

    public void testSecretAssassinKill() {
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        secretKill();

        doDayAction(assassin, cit);

        for(Message m: game.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement){
                assertFalse(m.access(Message.PUBLIC).contains(assassin.getName()));
                return;
            }
        }
        throw new Error("Bad assassin exception.");
    }

    public void testNoZeroWeightkModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Assassin(), AbilityModifierName.ZERO_WEIGHTED,
                    Assassin.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    private void secretKill() {
        TestUtil.modifyAbility(Assassin.abilityType, AbilityModifierName.SECRET_KILL, true);
    }
}

package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.DrugDealer;
import game.abilities.Joker;
import game.abilities.Vigilante;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Message;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.RoleService;

public class TestJoker extends SuperTest {

    public TestJoker(String s) {
        super(s);
    }

    ArrayList<Message> announcements;
    Controller joker;

    @Override
    public void roleInit() {
        joker = addPlayer(BasicRoles.Joker());
        announcements = addAnnouncementListener();
    }

    public void testBasic() {
        Controller citB = addPlayer(BasicRoles.Citizen());
        Controller citC = addPlayer(BasicRoles.Citizen());
        Controller citD = addPlayer(BasicRoles.Citizen());
        Controller citE = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 1);
        try{
            editRule(SetupModifierName.BOUNTY_INCUBATION, 0);
            fail();
        }catch(NarratorException e){
        }
        assertEquals(1, game.getInt(SetupModifierName.BOUNTY_INCUBATION));
        editRule(SetupModifierName.BOUNTY_INCUBATION, 2);

        bounty(citE);
        endNight();

        isBountied(citE, 2);
        skipDay();
        isBountied(citE);
        endNight();

        isBountied(citE);
        skipDay();

        assertBountyExpired(citE);

        bounty(citD, citB, citC);
        endNight();

        isDead(citB, citC);
        isBountied(citD);
        isNotBountied(citE);
    }

    public void testBadAttempts() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller arc = addPlayer(BasicRoles.Architect());

        try{
            onlyKill(cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        bounty(cit);

        nextNight();

        try{
            bounty(arc);
            fail();
        }catch(PlayerTargetingException e){
        }

        nextNight();
        try{
            bounty(arc, arc);
            fail();
        }catch(PlayerTargetingException e){
        }
        bounty(arc);
    }

    public void test1DayLimit() {
        Controller cit = addPlayer(BasicRoles.Architect());
        Controller arc = addPlayer(BasicRoles.Architect());
        addPlayer(BasicRoles.Architect());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 1);

        bounty(cit);
        nextNight();
        bounty(cit, arc);
        endNight();

        isDead(arc);
        isBountied(cit);
    }

    public void testManipulation() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 1);

        bounty(witch);
        witch(witch, joker, bd);
        endNight();

        isNotBountied(bd);
        isBountied(witch);
        skipDay();

        // successful bounty here

        bounty(witch);
        drive(bd, witch, bd);
        endNight();

        isBountied(witch);
        isNotBountied(bd);

        skipDay();
        // 2 successful bounties
        assertEquals(2 * game.getInt(SetupModifierName.BOUNTY_FAIL_REWARD), getRoleCard().bounty_kills);

        onlyKill(witch);
        witch(witch, joker, cit);
        endNight();

        // 2 sucess, 1 kill used

        isDead(cit);
        skipDay();

        bounty(witch, bd);
        witch(witch, joker, witch);
        endNight();

        isDead(witch);
        // 2 success, 2 kills used
        assertJokerKills(2, 2);
    }

    public void testWitchAddingBounty() {
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 1);

        witch(witch, joker, witch);
        endNight();

        isBountied(witch);
        skipDay();

        assertJokerKills(1, 0);
        witch(witch, joker, witch);
        endNight();

        isDead(witch);
    }

    private void assertJokerKills(int bounties, int usedKills) {
        assertEquals((bounties * game.getInt(SetupModifierName.BOUNTY_FAIL_REWARD)) - usedKills,
                getRoleCard().bounty_kills);
    }

    public void testVisitation() {
        Controller detective = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Citizen());

        setTarget(detective, joker);
        bounty(detective);
        endNight();

        TestDetective.seen(detective, detective);
    }

    public void testDeadJoker() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.MAYOR_VOTE_POWER, 3);

        bounty(mayor);
        endNight();

        reveal(mayor);
        vote(mayor, joker);
        isDead(joker);

        endNight();
        isBountied(mayor, game.getInt(SetupModifierName.BOUNTY_INCUBATION) - 1);
        skipDay();

        assertBountyExpired(mayor);
    }

    public void testGhostJoker() {
        Controller ghost = addPlayer(BasicRoles.Ghost());
        ControllerList cits = addPlayer(BasicRoles.Citizen(), 3);

        bounty(ghost);
        endNight();

        lynch(ghost, cits);
        endNight();

        lynch(joker, cits);
        TestGhost.assertCause(ghost, joker);
        isWinner(ghost);
    }

    public void testMarshallGhostBounty() {
        addPlayer(BasicRoles.Arsonist());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller marshall = addPlayer(BasicRoles.Marshall());
        ControllerList cits = addPlayer(BasicRoles.Citizen(), 4);

        editRule(SetupModifierName.MARSHALL_EXECUTIONS, 1);

        nightStart();
        assertEquals(1, game.getInt(SetupModifierName.MARSHALL_EXECUTIONS));

        bounty(ghost);
        endNight();

        order(marshall);
        assertEquals(2, game._lynches);
        lynch(joker, cits);
        vote(marshall, joker);
        assertEquals(1, game._lynches);
        isDead(joker);

        lynch(ghost, cits);
        isDead(ghost);

        assertIsNight();
        TestGhost.assertCause(ghost, cits.getLast());
    }

    public void testHasBountyNotCauseGhost() {
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.GS_DAY_GUNS, true);

        setTarget(gs, cit);
        bounty(ghost);
        endNight();

        shoot(cit, ghost);

        voteOut(gs, joker, cit);
        TestGhost.assertCause(ghost, cit);

    }

    public void testElectroAllActions() {
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 1);

        Role role = RoleService.createRole(setup, "WeakJoker", Joker.abilityType);
        Faction faction = setup.getFactionByColor(Setup.THREAT_C);
        joker = addPlayer(FactionRoleService.createFactionRole(faction, role));

        bounty(cit);
        isInvuln(joker, false);
        isInvuln(electro);
        endNight(joker);
        electrify(electro, joker, cit);

        bounty(electro, cit);
        endNight();

        isBountied(electro);
        isDead(cit);
    }

    public void testCorpseGhostGraveDigger() {
        Controller gd = addPlayer(BasicRoles.GraveDigger());
        Controller ghost = addPlayer(BasicRoles.Ghost());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller dit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 2);

        voteOut(joker, gd, ghost, cit);

        setTarget(gd, joker, ghost);
        endNight();

        isBountied(ghost, game.getInt(SetupModifierName.BOUNTY_INCUBATION));
        voteOut(ghost, gd, cit, dit);
        assertIsNight();

        TestGhost.assertCause(ghost, gd);

        setTarget(gd, joker, gd);
        endNight();

        isBountied(gd);

        skipDay();
        setTarget(gd, joker, cit);
        endNight();

        isNotBountied(cit);
    }

    public void testBakerJoker() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 2);
        editRule(SetupModifierName.BREAD_PASSING, false);

        setTarget(baker, joker);
        setTarget(gs, joker);
        bounty(baker); // main bounty
        nextNight();

        bounty(gs); // side bounty, jokerBread = 1
        setTarget(baker, joker);
        assertFalse(joker.getPlayer().getActions().canAddAnotherAction(joker.getPlayer().action(cit.getPlayer())));
        endNight();

        assertPassableBreadCount(0, joker);
        assertUseableBreadCount(1, joker);
        isBountied(baker, 1);
        isBountied(gs, 2);
        skipDay(); // main bounty ends

        bounty(cit); // main bounty
        // assertEquals(1, joker.getActionWeight());
        assertTrue(joker.getPlayer().getActions().canAddAnotherAction(Vigilante.abilityType));
        shoot(joker, cit);
        assertActionSize(2, joker);
        cancelAction(joker, 1);
        nextNight();

        shoot(joker, cit);

        assertNotNull(getRoleCard().activeBounty);
        assertUseableBreadCount(1, joker);

        Action a = joker.getPlayer().action(cit.getPlayer(), Joker.abilityType);
        assertTrue(joker.getPlayer().getActions().canAddAnotherAction(a));
        assertFalse(a.getAbility().canSubmitWithoutBread(a));
    }

    public void testFakeRoleblockable() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Escort());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
        modifyAbilityCharges(BasicRoles.Joker(), Joker.abilityType, 1);

        drug(dd, joker, DrugDealer.BLOCKED);
        bounty(dd);
        endNight();

        isBountied(dd);
        assertRealChargeRemaining(0, joker, Joker.abilityType);
    }

    public void testFakeBread() {
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller baker = addPlayer(BasicRoles.Baker());

        drug(dd, joker, DrugDealer.BREAD);
        bounty(dd);
        endNight();
        skipDay();

        bounty(baker);
        endNight();

        isNotBountied(baker);
        assertPassableBreadCount(0, joker);
    }

    public void testCommuter() {
        Controller commuter = addPlayer(BasicRoles.Commuter());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller fodder = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.BOUNTY_INCUBATION, 1);

        commute(commuter);
        bounty(commuter);
        endNight();

        isNotBountied(commuter);
        skipDay();

        bounty(fodder);
        nextNight();

        bounty(cit, commuter);
        commute(commuter);
        endNight();

        isAlive(commuter);
        isBountied(cit);
        skipDay();

        bounty(commuter, cit);
        commute(commuter);
        endNight();

        isDead(cit);
        isNotBountied(commuter);
        skipDay();
    }

    public void testSimple() {
        Controller bakerA = addPlayer(BasicRoles.Baker());
        Controller bakerB = addPlayer(BasicRoles.Baker());

        dayStart();
        skipDay();

        setTarget(bakerA, joker);
        setTarget(bakerB, joker);
        bounty(bakerB);
        endNight();
        skipDay();

        Action a = new Action(joker.getPlayer(), Joker.abilityType, bakerA.getPlayer());
        assertTrue(joker.getPlayer().getActions().canAddAnotherAction(a));
        setTarget(joker, bakerA);
        endNight();
    }

    private void bounty(Controller... bounties) {
        setTarget(joker, ControllerList.list(bounties), Joker.abilityType);
    }

    private void onlyKill(Controller... bounties) {
        setTarget(joker, Joker.abilityType, Joker.NO_BOUNTY, bounties);
    }

    private void isBountied(Controller p, int days) {
        isBountied(p);
        String text = Joker.DayStartAnnouncementCreator(p.getPlayer(), days).access(Message.PUBLIC);
        for(Message a: announcements){
            if(a.access(Message.PUBLIC).equals(text) && a.getDay() == game.getDayNumber())
                return;
        }
        fail();
    }

    private void assertBountyExpired(Controller p) {
        isNotBountied(p);
        String text = Joker.DayEndAnnouncementCreator(p.getPlayer()).access(Message.PUBLIC);
        for(Message a: announcements){
            if(a.access(Message.PUBLIC).equals(text) && a.getDay() == game.getDayNumber())
                return;
        }
        fail();

    }

    private static void isBountied(Controller p) {
        assertTrue(p.getPlayer().isBountied());
    }

    private static void isNotBountied(Controller citA) {
        assertFalse(citA.getPlayer().isBountied());
    }

    private Joker getRoleCard() {
        return joker.getPlayer().getAbility(Joker.class);
    }
}

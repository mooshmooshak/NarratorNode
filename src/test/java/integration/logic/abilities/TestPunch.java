package integration.logic.abilities;

import game.abilities.FactionKill;
import game.abilities.Punch;
import game.abilities.Ventriloquist;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.enums.FactionModifierName;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import services.GameService;

public class TestPunch extends SuperTest {

    public TestPunch(String name) {
        super(name);
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.SerialKiller());

        setPunchHit(Setup.TOWN_C, 100);
        setPunchHit(Setup.BENIGN_C, 0);

        dayStart();

        assertFalse(jester.getPlayer().getDayAbilities().get(0).getAcceptableTargets(jester.getPlayer()).isEmpty());

        assertTrue(jester.getPlayer().hasDayAction(Punch.abilityType));

        punch(jester, cit);
        isAlive(cit);

        punch(cit, cit2);
        isDead(cit2);
    }

    public void testUnallowed() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());

        setPunchHit(Setup.TOWN_C, 100);
        setPunchHit(Setup.BENIGN_C, 0);
        setPunchHit(Setup.MAFIA_C, 0);

        try{
            setPunchHit(Setup.TOWN_C, 101);
            fail();
        }catch(NarratorException e){
        }
        try{
            setPunchHit(Setup.BENIGN_C, -1);
            fail();
        }catch(NarratorException e){
        }

        assertEquals(100,
                setup.getFactionByColor(Setup.TOWN_C).modifiers.getInt(FactionModifierName.PUNCH_SUCCESS, game));
        assertEquals(0,
                setup.getFactionByColor(Setup.BENIGN_C).modifiers.getInt(FactionModifierName.PUNCH_SUCCESS, game));

        mafKill(maf, cit2);
        badPunch(cit2, maf);// nighttime targeting
        setTarget(bm, sk);
        setTarget(vent, bm, Ventriloquist.abilityType);
        endNight();

        badPunch(cit, (Controller) null);
        badPunch(null, cit);
        badPunch(cit2, cit);
        badPunch(cit, cit2);
        badPunch(cit, cit2, maf);
        badPunch(cit, game.skipper);

        // double
        punch(maf, cit);
        badPunch(maf, cit);

        badPunch(sk, cit);// bmed
        badPunch(bm, vent);// puppetted
    }

    public void testNoPunch() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.PUNCH_ALLOWED, false);
        dayStart();

        badPunch(maf, cit);
        badPunch(cit, maf);
    }

    public void testElectro() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        setPunchHit(Setup.TOWN_C, 100);

        electrify(em, cit, cit2);
        endNight();

        punch(cit, cit2);

        isDead(cit, cit2);
        isWinner(em);
        assertGameOver();
    }

    public void testVotesDisappearOnDeath() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Arsonist(), 2);

        setPunchHit(Setup.TOWN_C, 100);

        vote(cit, cit2);

        punch(cit, cit2);

        assertEmpty(game.voteSystem.getVoteTargets(cit.getPlayer()));
    }

    public void testPunchPuppet() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        addPlayer(BasicRoles.Ventriloquist());

        setPunchHit(Setup.TOWN_C, 100);

        assertFalse(setup.getFactionByColor(Setup.TOWN_C).hasAbility(FactionKill.abilityType));

        setTarget(vent, cit, Ventriloquist.abilityType);
        endNight();

        ventPunch(vent, cit, cit2);
        isDead(cit2);
    }

    public void testParity() {
        GameService.upsertModifier(game, GameModifierName.AUTO_PARITY, true);
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());
        Controller maf2 = addPlayer(BasicRoles.Goon());
        Controller maf3 = addPlayer(BasicRoles.Goon());

        setPunchHit(Setup.MAFIA_C, 100);

        dayStart();
        assertIsDay();

        punch(cit2, cit3);
        punch(cit3, cit2);

        assertContains(cit1.getCommands(), Punch.abilityType);
        assertNotContains(cit2.getCommands(), Punch.abilityType);

        punch(maf3, cit1);
        punch(maf1, maf2);
        assertIsNight();
    }

    public void testCanPunchOnDay2() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller maf1 = addPlayer(BasicRoles.Goon());

        setPunchHit(Setup.MAFIA_C, 0);

        punch(maf1, cit2);

        skipDay();
        endNight();

        punch(maf1, cit1);
    }

    // puppeted punching

    private void setPunchHit(String townC, int i) {
        editRule(SetupModifierName.PUNCH_ALLOWED, true);
        setTeamRule(townC, FactionModifierName.PUNCH_SUCCESS, i);
    }

    private void badPunch(Controller p1, Controller... p2) {
        try{
            punch(p1, p2);
            fail();
        }catch(NarratorException | NullPointerException e){
        }
    }

    // discord on/off
    //
}

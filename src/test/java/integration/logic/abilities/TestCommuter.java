package integration.logic.abilities;

import game.abilities.BreadAbility;
import game.abilities.Commuter;
import game.abilities.Detective;
import game.abilities.Douse;
import game.abilities.GraveDigger;
import game.abilities.Block;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;

public class TestCommuter extends SuperTest {

    public TestCommuter(String name) {
        super(name);
    }

    Controller commuter;

    @Override
    public void roleInit() {
        commuter = addPlayer(BasicRoles.Commuter());
    }

    private void commute() {
        commute(commuter);
    }

    public void testBasic() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller agent = addPlayer(BasicRoles.Agent());

        commute();
        mafKill(agent, commuter);
        endNight();

        isAlive(commuter);
    }

    public void testNoDoubleUsage() {
        Controller baker = addPlayer(BasicRoles.Baker());
        addPlayer(BasicRoles.Agent());

        setTarget(baker, commuter);
        nextNight();

        commute();
        commute();

        assertActionSize(1, commuter);
    }

    public void testNoArsonKill() {
        addPlayer(BasicRoles.Citizen());
        Controller arson = addPlayer(BasicRoles.Arsonist());

        setTarget(arson, commuter, Douse.abilityType);
        nextNight();

        commute();
        burn(arson);
        endNight();

        isAlive(commuter);
    }

    public void testNoElectroSheriff() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());

        electrify(electro, sheriff, commuter);

        commute();
        setTarget(sheriff, commuter);
        endNight();

        isAlive(commuter, sheriff);
    }

    public void testCommuterKillingFromAfar() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());

        editRule(SetupModifierName.BREAD_PASSING, true);

        setTarget(baker, commuter);
        electrify(electro, baker, commuter);

        commute();
        setTarget(commuter, baker, BreadAbility.abilityType);
        endNight();

        assertGameOver();
        isDead(baker, commuter);
    }

    public void testNoBlockFeedback() {
        Controller strip = addPlayer(BasicRoles.Escort());
        addPlayer(BasicRoles.Agent());

        setTarget(strip, commuter);
        commute();
        endNight();

        partialExcludes(commuter, Block.FEEDBACK);
    }

    public void testDeadCommuting() {
        addPlayer(BasicRoles.Citizen());
        Controller coro = addPlayer(BasicRoles.Coroner());
        Controller dete = addPlayer(BasicRoles.Detective());
        Controller gd = addPlayer(BasicRoles.GraveDigger());

        voteOut(commuter, coro, dete, gd);

        setTarget(gd, GraveDigger.abilityType, Commuter.COMMAND, commuter);
        setTarget(coro, commuter);
        setTarget(dete, coro);
        endNight();

        partialContains(dete, Detective.NO_VISIT);
    }

    public void testMMInteraction() {
        Controller detective = addPlayer(BasicRoles.Detective());
        Controller mm = addPlayer(BasicRoles.MassMurderer());

        setTarget(detective, commuter);
        setTarget(mm, commuter);
        commute();
        endNight();

        isAlive(detective, commuter);
    }

    public void testDriverInteraction() {
        Controller driver = addPlayer(BasicRoles.BusDriver());
        Controller detect = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Agent());

        setTarget(detect, driver);
        drive(driver, commuter, detect);
        commute();
        endNight();

        TestDetective.seen(detect, detect);
    }

    public void testCowardInteraction() {
        Controller cow = addPlayer(BasicRoles.Coward());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        commute();
        setTarget(sk, cow);
        setTarget(cow, commuter);
        endNight();

        isDead(cow);
        isAlive(commuter);
    }

    public void testOperatorInteraction() {
        Controller operator = addPlayer(BasicRoles.Operator());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        tele(operator, sk, commuter);
        setTarget(sk, operator);
        commute();
        endNight();

        isDead(operator);
    }

    public void testWitchIneraction() {
        modifyAbilityCharges(BasicRoles.Commuter(), 1);

        Controller witch = addPlayer(BasicRoles.Witch());
        Controller detect = addPlayer(BasicRoles.Detective());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
        nightStart();

        assertRealChargeRemaining(1, commuter, Commuter.abilityType);

        witch(witch, commuter, detect);
        endNight();

        assertRealChargeRemaining(0, commuter, Commuter.abilityType);
    }

    public void testElectroSavedWitch() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller detect = addPlayer(BasicRoles.Detective());

        setTarget(baker, witch);
        electrify(electro, baker, witch);

        witch(witch, baker, witch);
        witch(witch, commuter, doctor);
        setTarget(detect, commuter);
        setTarget(doctor, witch);
        endNight();

        partialExcludes(detect, Detective.NO_VISIT);
        partialExcludes(detect, Detective.FEEDBACK);

        isAlive(detect, witch);
    }

    public void testNoCharges() {
        modifyAbilityCharges(BasicRoles.Commuter(), 1);

        Controller witch = addPlayer(BasicRoles.Witch());
        Controller detect = addPlayer(BasicRoles.Detective());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        commute();
        nextNight();

        witch(witch, commuter, detect);
        setTarget(detect, commuter);
        endNight();

        TestDetective.seen(detect, detect);
    }

    public void testElectrCommuterInitiated() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());

        setTarget(baker, commuter);
        setTarget(gs, commuter);
        electrify(electro, gs, commuter);

        commute();
        shoot(commuter, gs);
        endNight();

        isDead(commuter);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Commuter(), AbilityModifierName.BACK_TO_BACK,
                    Commuter.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}

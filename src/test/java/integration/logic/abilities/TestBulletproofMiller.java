package integration.logic.abilities;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import game.abilities.Bulletproof;
import game.abilities.Miller;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;

public class TestBulletproofMiller extends SuperTest {

    public TestBulletproofMiller(String s) {
        super(s);
    }

    public void testBulletProofBasic() {
        Controller bp = addPlayer(BasicRoles.Bulletproof());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller gf = addPlayer(BasicRoles.Godfather());

        setTarget(sk, bp);
        mafKill(gf, bp);

        endNight();

        isAlive(bp, sk, gf);
    }

    public void testBpElectric() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller bp = addPlayer(BasicRoles.Bulletproof());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, bp, sheriff);

        setTarget(doc, sheriff);
        setTarget(sheriff, bp);

        endNight();

        isAlive(bp, sheriff, doc, em);
    }

    public void testBPAssassination() {
        Controller bp = addPlayer(BasicRoles.Bulletproof());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.SerialKiller());

        dayStart();

        doDayAction(assassin, bp);

        isDead(bp);
    }

    public void testMillerSheriffCheck() {
        Controller miller = addPlayer(BasicRoles.Miller());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller agent = addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.CHECK_DIFFERENTIATION, true);

        setTarget(sheriff, miller);

        assertEquals(agent.getColor(), miller.getPlayer().getFrameStatus().getColor());

        endNight();

        partialContains(sheriff, agent.getPlayer().getGameFaction().getName());
        // the bullet points are coming up as evil to sheriffs (indicating miller), and
        // what they will turn up as to sheriff checks
        List<String> publicDescription = miller.getPlayer().getAbility(Miller.class).getPublicDescription(
                Optional.of(game), BasicRoles.Miller().getName(), Optional.empty(), new HashSet<>());
        assertEquals(2, publicDescription.size());
    }

    public void testDeathSuited() {
        Controller miller = addPlayer(BasicRoles.Miller());
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.MILLER_SUITED, true);

        voteOut(miller, goon, cit1, cit2);

        assertSuited(miller, goon);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Miller(), AbilityModifierName.BACK_TO_BACK,
                    Miller.abilityType, false);
            fail();
        }catch(NarratorException e){
        }

        try{
            FactionRoleModifierService.addModifier(BasicRoles.Bulletproof(), AbilityModifierName.BACK_TO_BACK,
                    Bulletproof.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Miller(), AbilityModifierName.ZERO_WEIGHTED,
                    Miller.abilityType, false);
            fail();
        }catch(NarratorException e){
        }

        try{
            FactionRoleModifierService.addModifier(BasicRoles.Bulletproof(), AbilityModifierName.ZERO_WEIGHTED,
                    Bulletproof.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}

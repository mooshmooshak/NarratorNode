package integration.logic.abilities;

import java.util.List;
import java.util.Optional;

import game.abilities.FactionKill;
import game.ai.Controller;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class TestFactionKill extends SuperTest {

    public TestFactionKill(String s) {
        super(s);
    }

    public void testRuleTexts() {
        Faction faction = setup.getFactionByColor(Setup.MAFIA_C);

        for(Ability ability: faction.abilities){
            List<String> ruleTexts = ability.getPublicDescription(Optional.of(game), Optional.of(Setup.MAFIA_C),
                    faction.getName(), faction.abilities, new Modifiers<>());
            for(String text: ruleTexts)
                assertFalse(text.isEmpty());
        }
    }

    public void testFactionKillingRole() {
        Role killerRole = RoleService.createRole(setup, "Killer", FactionKill.abilityType);
        Faction faction = setup.getFactionByColor(Setup.CULT_C);
        FactionRole factionRole = FactionRoleService.createFactionRole(faction, killerRole);
        Controller killer = addPlayer(factionRole);
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        setTarget(killer, cit, FactionKill.abilityType);
        endNight();

        isDead(cit);
    }
}

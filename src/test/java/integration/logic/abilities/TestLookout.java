package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.FactionKill;
import game.abilities.Lookout;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Feedback;
import game.logic.Player;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.RoleModifierName;

public class TestLookout extends SuperTest {

    public TestLookout(String name) {
        super(name);
    }

    public void testBasicFunction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller maf = addPlayer(BasicRoles.Goon());

        setTarget(lookout, cit);
        setTarget(doc, cit);
        mafKill(maf, cit);

        endNight();

        seen(lookout, doc, maf);

        skipDay();

        setTarget(lookout, cit2);
        mafKill(maf, cit2);

        endNight();

        seen(lookout, maf);
    }

    public void testDoubleLookout() {
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller lookout2 = addPlayer(BasicRoles.Lookout());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        mafKill(maf, cit);
        setTarget(lookout2, cit);
        setTarget(lookout, cit);

        endNight();

        vote(cit2, maf);
        seen(lookout, maf, lookout2);
        seen(lookout2, maf, lookout);

        skipDay();
        assertFalse(game.getFaction(maf.getColor()).getAbility(FactionKill.abilityType).isOnCooldown());
        mafKill(maf, lookout2);
        setTarget(lookout, lookout2);

        endNight();

        seen(lookout, maf);
    }

    public void testNonVisitingPlayers() {
        modifyRole(BasicRoles.Godfather(), RoleModifierName.UNDETECTABLE, false);

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller jester = addPlayer(BasicRoles.Jester());

        Controller witch1 = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());
        Controller witch3 = addPlayer(BasicRoles.Witch());
        Controller witch4 = addPlayer(BasicRoles.Witch());

        // witch 1 makes cit target mayor
        // witch 2 makes mayor target mayor
        // witch 3 makes jester target mayor
        // witch 4 makes gf target mayor
        // mafs vote maf1 to be the sender
        // if gf gets the kill, maf1 doens't die, but kills the mayor
        // maf1 kills cit 2
        witch(witch1, cit, mayor);
        assertActionSize(1, witch1);

        witch(witch2, mayor, mayor);
        witch(witch3, jester, mayor);
        witch(witch4, gf, mayor);

        mafKill(maf, bd);
        setTarget(gf, maf, KILL);
        setTarget(gf, maf, SEND);

        assertFactionController(maf, game.getFaction(maf.getColor()));

        setTarget(lookout, mayor);

        endNight();
        isDead(bd);
        seen(lookout, cit, mayor, jester, gf, witch2);
    }

    public static void seen(Controller lookout, Controller... players) {
        ControllerList targets = ControllerList.list(players).sortByName();
        seen(lookout, targets);
    }

    public static void seen(Controller lookout_c, ControllerList visitors) {
        Player lookout = lookout_c.getPlayer();
        ArrayList<Object> parts = Lookout.FeedbackGenerator(visitors.toPlayerList(game));
        Feedback f = new Feedback(game.skipper);
        f.add(parts);
        f.setVisibility(lookout);
        String feedback = f.access(lookout);
        f.removeVisiblity(lookout);
        partialContains(lookout, feedback);
    }
}

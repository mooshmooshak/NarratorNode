package integration.logic.abilities;

import java.time.Instant;
import java.util.ArrayList;

import game.abilities.Amnesiac;
import game.abilities.CultLeader;
import game.abilities.Enforcer;
import game.abilities.Hidden;
import game.abilities.Mason;
import game.abilities.MasonLeader;
import game.abilities.Vigilante;
import game.ai.Controller;
import game.event.Feedback;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.FactionRole;
import models.ModifierValue;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.modifiers.RoleModifier;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.RoleModifierService;
import services.RoleService;
import util.RepeatabilityTest;
import util.game.LookupUtil;

public class TestMason extends SuperTest {

    public TestMason(String name) {
        super(name);
    }

    public void testBasic() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Arsonist());

        setTarget(ml, cit);
        endNight();

        isMason(cit);

        skipDay();
        setTarget(ml, doc);
        endNight();

        assertNotMason(doc);
    }

    public void testNoFactionRoleMason() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        FactionRole masonLeaderFactionRole = BasicRoles.MasonLeader();
        addPlayer(masonLeaderFactionRole);
        for(FactionRole factionRole: setup.getFactionByColor(masonLeaderFactionRole.getColor())
                .getFactionRolesWithAbility(Mason.abilityType)){
            FactionRoleService.deleteFactionRole(factionRole);
        }

        assertBadGameSettings();
    }

    public void testNoMasons() {
        for(Role role: LookupUtil.findRolesWithAbility(setup, Mason.abilityType))
            RoleService.delete(role);

        addPlayer(BasicRoles.MasonLeader());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        assertBadGameSettings();
    }

    public void testCultKilling() {
        addPlayer(BasicRoles.Citizen());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cult = addPlayer(BasicRoles.CultLeader());

        setTarget(cult, maf);
        nextNight();

        assertStatus(maf, CultLeader.abilityType);

        setTarget(ml, maf);
        endNight();

        isDead(maf);
    }

    // i actually want this to start off in a mafia parity control
    public void testMasonPromotion() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.MASON_PROMOTION, true);

        setTarget(ml, cit);
        nextNight();

        assertTrue(cit.is(Mason.abilityType));

        mafKill(maf, ml);
        endNight();

        assertTrue(cit.is(MasonLeader.abilityType));
    }

    public void testMasonLeaderPromotionDay() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Assassin());

        setTarget(ml, cit);
        endNight();

        assertTrue(cit.is(Mason.abilityType));
        doDayAction(assassin, ml);

        isDead(ml);
        assertTrue(cit.is(MasonLeader.abilityType));
        skipDay();

        assertTrue(cit.is(MasonLeader.abilityType));
    }

    private void promotionOff() {
        editRule(SetupModifierName.MASON_PROMOTION, false);
    }

    private void promotionOn() {
        editRule(SetupModifierName.MASON_PROMOTION, true);
    }

    public void testNoPromotion() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        promotionOff();

        setTarget(sk, ml);
        endNight();

        assertTrue(mason.is(Mason.abilityType));
    }

    public void testMasonChat() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        setTarget(ml, cit1);
        assertEquals(1, ml.getChatKeys().size());
        assertEquals(1, maf.getChatKeys().size());

        nextNight();
        assertEquals(2, ml.getChatKeys().size());
        assertEquals(2, cit1.getChatKeys().size());
        assertEquals(1, cit2.getChatKeys().size());
        assertTrue(cit1.getChatKeys().contains(cit1.getColor()));

        say(cit1, gibberish(), cit1.getColor());
        partialContains(ml, gibberish);
        partialContains(cit1, gibberish);
        partialExcludes(cit2, gibberish);

        setTarget(ml, cit2);
        nextNight();
    }

    public void testEnclosingChatError() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Mason());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();
        say(ml, gibberish(), Setup.TOWN_C);

        mafKill(maf, ml);
        nextNight();
    }

    public void testJailStopMasonChatForming() {
        Controller kid = addPlayer(BasicRoles.Kidnapper());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());

        jail(kid, ml);
        skipDay();

        assertEquals(1, mason.getChatKeys().size());
        nextNight();

        assertEquals(2, ml.getChatKeys().size());
        assertEquals(2, mason.getChatKeys().size());
    }

    public void testNoRecruitPowerlessRoles() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller gunsmith = addPlayer(BasicRoles.Gunsmith());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.MASON_NON_CIT_RECRUIT, false);
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        setTarget(gunsmith, vigi);
        shoot(vigi, fodder);
        nextNight();

        setTarget(ml, vigi);
        endNight();

        assertTrue(vigi.is(Vigilante.abilityType));
    }

    public void testRecruitPowerlessRoles() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.MASON_NON_CIT_RECRUIT, true);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        shoot(vigi, fodder);
        nextNight();

        assertFalse(vigi.getPlayer().isPowerRole());

        setTarget(ml, vigi);
        endNight();

        assertTrue(vigi.is(Mason.abilityType));
    }

    public void testNonFactionCitizens() {
        Role role = BasicRoles.Citizen().role;
        Faction faction = setup.getFactionByColor(Setup.BENIGN_C);
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(FactionRoleService.createFactionRole(faction, role));
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.MASON_NON_CIT_RECRUIT, true);

        setTarget(ml, cit);
        nextNight();

        assertFalse(cit.is(Mason.abilityType));

        setTarget(ml, sheriff);
        endNight();

        assertFalse(sheriff.is(Mason.abilityType));
    }

    public void testAutoMasonPromotionNight() {
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Arsonist());

        promotionOn();

        assertBadGameSettings();
    }

    public void testAutoMasonPromotionDay() {
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Arsonist());

        promotionOn();

        assertBadGameSettings();
    }

    public void testSingleMasonFail() {
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Arsonist());

        promotionOff();

        assertBadGameSettings();
    }

    public void testSingleRandomNoMasons() {
        for(int i = 0; i < 1000; i++){
            newNarrator();
//            long seed = Long.parseLong("-8435271749782363605");
            long seed = random.nextLong();
            game.setSeed(seed);
            addPlayer(Hidden.TownGovernment());
            addPlayer(Hidden.TownGovernment());
            addPlayer(BasicRoles.Poisoner());
            addPlayer(BasicRoles.Poisoner());

            promotionOff();

            dayStart();

            int singleMasonSize = game.getAllPlayers().filter(Mason.abilityType).size();
            int masonSize = getMasonTypeSize();
            if(singleMasonSize == 1 && masonSize == 1)
                fail(Long.toString(seed));

            RepeatabilityTest.runRepeatabilityTest();
            BrainEndGame = false;
        }
    }

    private int getMasonTypeSize() {
        int counter = 0;
        for(Player player: game.players){
            if(Mason.IsMasonType(player))
                counter++;
        }
        return counter;
    }

    public void testCultVsMason() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        setTarget(ml, cit);
        setTarget(cl, cit);
        endNight();

        isAlive(cit);
        assertEquals(ml.getColor(), cit.getColor());
        assertTrue(cit.is(Mason.abilityType));
    }

    public void testUnrecruitableMasons() {
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        setTarget(cl, ml);
        nextNight();

        assertEquals(mason.getColor(), ml.getColor());

        setTarget(cl, mason);
        nextNight();

        assertEquals(mason.getColor(), ml.getColor());

    }

    public void testNoOtherMasonTarget() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Arsonist());

        nightStart();

        try{
            setTarget(ml, mason);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertTrue(mason.getPlayer().getAcceptableTargets(MasonLeader.abilityType).isEmpty());
    }

    public void testAmnesiacIn() {
        Controller ml = addPlayer(BasicRoles.MasonLeader());
        Controller mason = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller amn2 = addPlayer(BasicRoles.Amnesiac());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.MASON_PROMOTION, true);

        setTarget(ml, mason);
        endNight();

        voteOut(mason, ml, cit2, amn, ars);
        endNight();

        voteOut(ml, ars, amn, cit2);

        try{
            setTarget(amn, ml);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(amn, mason);
        setTarget(amn2, mason);
        endNight();

        assertTrue(amn.is(MasonLeader.abilityType) || amn.is(Mason.abilityType));
        assertTrue(amn2.is(MasonLeader.abilityType) || amn.is(Mason.abilityType));

        assertNotSameAbilities(amn, amn2);
    }

    public void testClubber() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller clubber = addPlayer(BasicRoles.Clubber());
        Controller mason = addPlayer(BasicRoles.Mason());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller cLeader = addPlayer(BasicRoles.CultLeader());

        promotionOn();

        nightStart();

        assertTrue(mason.getPlayer().initialAssignedRole.assignedRole.role.hasAbility(Mason.abilityType));
        assertNotMason(mason);
        assertStatus(cult, CultLeader.abilityType);

        setTarget(clubber, cult);
        setTarget(cLeader, cit);
        nextNight();

        assertStatus(cit, CultLeader.abilityType);
        isDead(cult);

        setTarget(clubber, cit);
        say(clubber, gibberish(), clubber.getColor());

        partialContains(mason, gibberish);
        nextNight();

        isDead(cit);

        setTarget(clubber, cLeader);
        endNight();

        assertGameOver();
        isDead(cLeader, cit, cult);
        isAlive(clubber, mason);
    }

    public void testEnforcer() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mL = addPlayer(BasicRoles.MasonLeader());
        Controller enforcer = addPlayer(BasicRoles.Enforcer());
        Controller cLeader = addPlayer(BasicRoles.CultLeader());

        promotionOn();

        nightStart();

        setTarget(enforcer, cit);
        setTarget(cLeader, cit);
        say(enforcer, gibberish(), enforcer.getColor());

        partialContains(mL, gibberish);
        nextNight();

        assertStatus(cit, CultLeader.abilityType, false);

        setTarget(enforcer, cit);
        setTarget(mL, cit);
        endNight();

        assertFalse(cit.is(Mason.abilityType));

        skipDay();
        setTarget(cLeader, cit);
        endNight();

        assertStatus(cit, CultLeader.abilityType);
    }

    public void testClubberAutoVest() {
        Controller clubber = addPlayer(BasicRoles.Clubber());
        Player cultLeader = addPlayer(BasicRoles.CultLeader()).getPlayer();
        addPlayer(BasicRoles.Citizen());
        RoleModifierService.upsertModifier(BasicRoles.CultLeader().role, new RoleModifier(RoleModifierName.AUTO_VEST,
                new ModifierValue(1), 0, Setup.MAX_PLAYER_COUNT, Instant.now()));

        nightStart();
        assertEquals(1, cultLeader.getRealAutoVestCount());

        setTarget(clubber, cultLeader);
        endNight();

        isAlive(cultLeader);
    }

    public void testAmnesiacEnforcer() {
        Controller enforcer = addPlayer(BasicRoles.Enforcer());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller fodder = addPlayer(BasicRoles.Witch());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(fodder, witch, amn, enforcer);
        setTarget(enforcer, amn);
        setTarget(amn, fodder);
        endNight();

        assertTrue(amn.is(Amnesiac.abilityType));
    }

    public void testEnforcerLearning() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller enforcer = addPlayer(BasicRoles.Enforcer());
        Controller cLeader = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifierName.ENFORCER_LEARNS_NAMES, true);

        setTarget(enforcer, cit);
        setTarget(cLeader, cit);
        endNight();

        seen(enforcer, cLeader);
    }

    public static void seen(Controller enforcer_c, Controller recruiter_c) {
        Player recruiter = recruiter_c.getPlayer();
        Player enforcer = enforcer_c.getPlayer();
        ArrayList<Object> parts = Enforcer.FeedbackGenerator(recruiter);
        Feedback f = new Feedback(enforcer.getSkipper());
        f.add(parts);
        f.setVisibility(enforcer);
        String feedback = f.access(enforcer);
        f.removeVisiblity(enforcer);
        partialContains(enforcer, feedback);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Mason(), AbilityModifierName.BACK_TO_BACK,
                    Mason.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Mason(), AbilityModifierName.ZERO_WEIGHTED,
                    Mason.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    private void isMason(Controller p) {
        assertTrue(p.is(Mason.abilityType));
    }

    private void assertNotMason(Controller p) {
        assertFalse(p.is(Mason.abilityType));
    }

}

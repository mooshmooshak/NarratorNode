package integration.logic.abilities;

import java.util.List;

import game.abilities.DrugDealer;
import game.abilities.ParityCheck;
import game.ai.Controller;
import game.event.Feedback;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;

public class TestParityCop extends SuperTest {

    public TestParityCop(String s) {
        super(s);
    }

    Controller parityCop;

    @Override
    public void roleInit() {
        parityCop = addPlayer(BasicRoles.ParityCop());
    }

    public void testNoFeedbackFirstNight() {
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());

        setTarget(parityCop, goon);
        endNight();

        assertGottaWaitFeedback();
    }

    public void testDifferentFeedbackNight() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(parityCop, goon);
        nextNight();

        setTarget(parityCop, citizen);
        endNight();

        assertDifferentParityFeedback();
    }

    public void testSameFeedbackNight() {
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());

        setTarget(parityCop, goon);
        nextNight();

        setTarget(parityCop, goon);
        endNight();

        assertSameParityFeedback();
    }

    public void testNoDeadChecking() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        doDayAction(mayor);
        vote(mayor, cit);

        try{
            setTarget(parityCop, cit);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testComparing2With1Previous() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(baker, parityCop);
        parityCheck(parityCop, baker);
        nextNight();

        parityCheck(parityCop, baker);
        parityCheck(parityCop, goon);
        endNight();

        assertSameParityFeedback();
        assertDifferentParityFeedback();
    }

    public void testComparing1With2Previous() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(baker, parityCop);
        setTarget(baker2, parityCop);
        nextNight();

        parityCheck(parityCop, baker);
        parityCheck(parityCop, baker2);
        parityCheck(parityCop, goon);
        nextNight();

        parityCheck(parityCop, goon);
        endNight();

        assertSomeParityFeedback(1);
    }

    public void testSkippingAnight() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(parityCop, goon);
        nextNight();
        nextNight();

        setTarget(parityCop, citizen);
        endNight();

        assertDifferentParityFeedback();
    }

    public void testRealBlock() {
        Controller blocker = addPlayer(BasicRoles.Escort());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(parityCop, goon);
        nextNight();

        setTarget(parityCop, blocker);
        setTarget(blocker, parityCop);
        nextNight();

        setTarget(parityCop, goon);
        endNight();

        assertSameParityFeedback();
    }

    public void testWitchedFeedback() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(parityCop, cit);
        nextNight();

        setTarget(witch, parityCop, witch);
        endNight();

        assertDifferentParityFeedback();
        List<Feedback> feedbacks = super.getFeedback(parityCop, game.getDayNumber() - 1);
        for(Feedback feedback: feedbacks){
            // makes sure that we're not giving extra information about who the witch made
            // the cop target
            assertTrue(feedback.getExtras() == null || feedback.getExtras().isEmpty());
        }
    }

    public void testDrugDealerWiping() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller drugDealer = addPlayer(BasicRoles.DrugDealer());

        setTarget(parityCop, citizen);
        nextNight();

        drug(drugDealer, parityCop, DrugDealer.WIPE);
        setTarget(parityCop, drugDealer);
        endNight();

        assertNoFeedback();

        skipDay();

        setTarget(parityCop, drugDealer);
        endNight();

        assertGottaWaitFeedback();
    }

    public void testDrugDealerWipingNoAction() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller drugDealer = addPlayer(BasicRoles.DrugDealer());

        setTarget(parityCop, citizen);
        nextNight();

        drug(drugDealer, parityCop, DrugDealer.WIPE);
        endNight();

        assertNoFeedback();

        skipDay();

        setTarget(parityCop, drugDealer);
        endNight();

        assertDifferentParityFeedback();
    }

    private void assertGottaWaitFeedback() {
        List<Feedback> feedbacks = SuperTest.getFeedback(parityCop, game.getDayNumber() - 1);
        Player player = parityCop.getPlayer();
        for(Feedback feedback: feedbacks)
            if(feedback.access(player).contains(ParityCheck.GOTTA_WAIT_FEEDBACK))
                return;
        fail("Gotta wait feedback not found");
    }

    private void assertSomeParityFeedback(int count) {
        partialContains(parityCop, ParityCheck.getFeedback(count));
    }

    private void assertSameParityFeedback() {
        partialContains(parityCop, ParityCheck.FEEDBACK_SAME_ALIGNMENT);
    }

    private void assertDifferentParityFeedback() {
        partialContains(parityCop, ParityCheck.FEEDBACK_ALL_DIFFERENT_ALIGNMENT);
    }

    private void assertNoFeedback() {
        List<Feedback> feedbacks = SuperTest.getFeedback(parityCop, game.getDayNumber() - 1);
        Player player = parityCop.getPlayer();
        String string;
        for(Feedback feedback: feedbacks){
            string = feedback.access(player);
            assertFalse(string.contains(ParityCheck.GOTTA_WAIT_FEEDBACK));
            assertFalse(string.contains(ParityCheck.FEEDBACK_SAME_ALIGNMENT));
            assertFalse(string.contains(ParityCheck.FEEDBACK_ALL_DIFFERENT_ALIGNMENT));
        }
    }

    private void parityCheck(Controller parityCop, Controller target) {
        setTarget(parityCop, target, ParityCheck.abilityType);
    }

}

package integration.logic.abilities;

import game.abilities.Hidden;
import game.abilities.Veteran;
import game.ai.Controller;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.HiddenService;
import services.HiddenSpawnService;

public class TestVeteran extends SuperTest {

    public TestVeteran(String s) {
        super(s);
    }

    public void testBGSave() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Witch());

        nightStart();

        assertNull(vet.getPlayer().getAcceptableTargets(Veteran.abilityType));

        setTarget(vet);
        setTarget(vig, vet, GUN);
        setTarget(bg, vig);

        endNight();

        isAlive(vig);
        assertTrue(vet.is(Veteran.abilityType));
        isAlive(vet);
        isDead(bg);
    }

    public void testCommandTest() {
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        setTarget(sk, vet);
        command(vet, Veteran.COMMAND);
        endNight();

        isDead(sk);
    }

    public void testLimits() {
        modifyRole(BasicRoles.Veteran(), AbilityModifierName.CHARGES, 1);

        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vet2 = addPlayer(BasicRoles.Veteran());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Lookout());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        nightStart();

        setTarget(vet);
        endNight();

        skipDay();

        // out of shots
        try{
            setTarget(vet, vet);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(vet2);
        witch(witch, vet, vet2);

        endNight();

        isAlive(witch);
        isDead(vet);
    }

    public void testImmune() {
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller mm = addPlayer(BasicRoles.MassMurderer());

        nightStart();

        setTarget(vet);
        setTarget(mm, vet);
        setTarget(sk, vet);

        endNight();

        isWinner(vet);
        isLoser(sk, mm);
    }

    public void testLimitations() {
        modifyRole(BasicRoles.Veteran(), AbilityModifierName.CHARGES, 1);

        Controller detective = addPlayer(BasicRoles.Detective());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        nightStart();

        alert(vet);

        endNight();
        skipDay();

        try{
            alert(vet);
            fail();
        }catch(PlayerTargetingException e){
        }

        witch(witch, vet, detective);

        setTarget(detective, vet);

        endNight();

        TestDetective.seen(detective, detective);

        assertEquals(3, game.getLiveSize());
    }

    public void testBasicFunction() {
        PlayerList players = new PlayerList();

        Hidden hidden = HiddenService.createHidden(setup, "Single Targetables");
        HiddenSpawnService.addSpawnableRoles(hidden, BasicRoles.Armorsmith());
        HiddenSpawnService.addSpawnableRoles(hidden, BasicRoles.Detective());

        assertTrue(hidden.size() != 0);

        for(int i = 0; i < 10; i++)
            addPlayer(hidden);

        players.add(game.getAllPlayers());

        Role citizenRole = BasicRoles.Citizen().role;
        Role veteranRole = BasicRoles.Veteran().role;

        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Controller cit = addPlayer(FactionRoleService.createFactionRole(mafia, citizenRole));
        Controller vet = addPlayer(FactionRoleService.createFactionRole(mafia, veteranRole));

        nightStart();

        for(Controller p: players)
            setTarget(p, vet);
        try{
            setTarget(vet, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(vet);

        endNight();

        assertGameOver();
        isWinner(vet);
    }

    public void testDocSave() {

        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(doc, vig);
        setTarget(vet);
        setTarget(vig, vet, GUN);

        endNight();

        isAlive(vet);
        assertEquals(4, game.getLiveSize());
    }

    public void testBDTwiceVisits() {
        Controller bd1 = addPlayer(BasicRoles.BusDriver());
        Controller bd2 = addPlayer(BasicRoles.BusDriver());
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller doc3 = addPlayer(BasicRoles.Doctor());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller vet2 = addPlayer(BasicRoles.Veteran());
        addPlayer(BasicRoles.Witch());

        setTarget(doc1, bd1);
        setTarget(doc2, bd2);
        setTarget(doc3, bd2);
        setTarget(vet);
        setTarget(vet2);
        drive(bd1, vet, vet2);
        drive(bd2, vet2, vet);
        endNight();

        isAlive(bd2);
        isDead(bd1);
    }

    public void testNotImmuneAnymore() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Agent(), 2);

        alert(veteran);
        nextNight();

        setTarget(sk, veteran);
        endNight();

        isDead(veteran);
    }

    public void testImmunityOnGameEnd() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Agent(), 1);

        alert(veteran);

        setTarget(sk, veteran);
        endNight();

        isInvuln(veteran, false);
    }

    public void testVeteranCulted() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller mafia = addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);

        setTarget(doc, cl);
        setTarget(cl, veteran);
        mafKill(mafia, veteran);
        alert(veteran);
        endNight();

        isInvuln(veteran, false);
    }

    public void testVeteranCultedJesterKilled() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Witch());

        modifyRole(BasicRoles.CultLeader(), RoleModifierName.AUTO_VEST, 1);

        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);

        vote(vet, jester);
        endDay();

        isAlive(vet);

        alert(vet);
        setTarget(cl, vet);
        endNight();
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Veteran(), AbilityModifierName.BACK_TO_BACK,
                    Veteran.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}

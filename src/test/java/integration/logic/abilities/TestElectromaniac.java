package integration.logic.abilities;

import java.util.LinkedList;

import game.abilities.Blackmailer;
import game.abilities.Bulletproof;
import game.abilities.Detective;
import game.abilities.DrugDealer;
import game.abilities.ElectroManiac;
import game.abilities.Vigilante;
import game.abilities.Witch;
import game.ai.Controller;
import game.logic.Player;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import util.TestUtil;
import util.models.ActionTestUtil;

public class TestElectromaniac extends SuperTest {

    public TestElectromaniac(String name) {
        super(name);
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        try{
            setTarget(em, em);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(em, cit);

        nextNight();

        setTarget(em, sheriff);
        setTarget(sheriff, cit);

        endNight();

        assertGameOver();
        isDead(sheriff, cit);
    }

    public void testBasicVisitingEachother() {
        Controller sheriff2 = addPlayer(BasicRoles.Sheriff());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        setTarget(em, sheriff2);
        nextNight();

        setTarget(em, sheriff);
        setTarget(sheriff, sheriff2);
        setTarget(sheriff2, sheriff);

        endNight();

        assertGameOver();
        isDead(sheriff, sheriff2);

        assertAttackSize(1, sheriff);
        assertAttackSize(1, sheriff2);
    }

    public void testNoDeathGettingChargedAfter() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller consort = addPlayer(BasicRoles.Consort());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, cit);

        setTarget(em, consort);
        setTarget(consort, cit);

        endNight();

        assertStatus(consort, ElectroManiac.abilityType);
        isAlive(consort);
    }

    public void testSingleAttackBDVisit() {
        Controller driver = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, cit, doc);

        setTarget(em, driver);
        drive(driver, cit, doc);

        endNight();

        isAlive(doc, cit, driver);

        skipDay();

        drive(driver, cit, doc);
        endNight();

        isDead(cit, driver, doc);

        assertAttackSize(1, driver);
    }

    public void testRevistCharged() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller electro1 = addPlayer(BasicRoles.ElectroManiac());
        Controller electro2 = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.Cultist());

        setTarget(electro1, cit);
        setTarget(electro2, cit);

        endNight();

        isDead(cit);
    }

    public void testSwapSuceeding() {
        Controller driver = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.Agent());

        electrify(em, driver, cit, doc);

        setTarget(jan, cit);
        drive(driver, cit, doc);
        endNight();

        assertStatus(doc, ElectroManiac.abilityType);

        isDead(doc, cit, driver);

    }

    public void testDocSave() {
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller driver = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.Agent());

        electrify(em, driver, cit);

        setTarget(jan, cit);
        setTarget(doc, driver);
        drive(driver, cit, jan);
        endNight();

        isDead(cit);
        isAlive(driver, doc);
    }

    public void testMafiosoGettingReinsertedIntoOoO() {
        Controller driver = addPlayer(BasicRoles.BusDriver());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, driver, maf);

        drive(driver, maf, cit);
        mafKill(maf, doc);
        setTarget(doc, cit);

        endNight();
        isDead(driver, doc);
        isAlive(maf);
    }

    public void testElectroImmunityToChargeDeath() {
        addPlayer(BasicRoles.Citizen());
        Controller em3 = addPlayer(BasicRoles.ElectroManiac());
        Controller em2 = addPlayer(BasicRoles.ElectroManiac());
        Controller em1 = addPlayer(BasicRoles.ElectroManiac());
        TestUtil.removeAbility(BasicRoles.ElectroManiac().role, Bulletproof.abilityType);

        electrify(em1, em3, em2);

        setTarget(em3, em2);

        endNight();

        isAlive(em3, em2);
    }

    public void testDeadKeepsCharged() {
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller digger = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        editRule(SetupModifierName.CORONER_EXHUMES, false);

        electrify(em, coroner, cit, digger, witch);
        endNight();

        voteOut(witch, digger, coroner, cit);

        setTarget(coroner, witch);
        setTarget(ActionTestUtil.getGraveDiggerAction(digger,
                new Action(witch.getPlayer(), Witch.abilityType, cit.getPlayer())));

        endNight();

        isDead(digger, coroner, witch);
    }

    public void testBGCantSaveElectro() {
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, coroner, cit);
        endNight();

        voteOut(cit, coroner, bg, witch);

        setTarget(coroner, cit);
        setTarget(bg, coroner);
        endNight();

        isAlive(bg);
        isDead(coroner, cit);
    }

    public void testElectroDoubleTarget1() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller electro2 = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.ElectroManiac());

        doubleCharge(electro, cit1, cit1);
        doubleCharge(electro, cit2, cit2);
        setTarget(baker, electro2);

        nextNight();

        isDead(cit2);
        doubleCharge(electro2, baker, baker);
        doubleCharge(electro2, baker, electro);
        assertActionSize(1, electro2);

        try{
            doubleCharge(electro, cit2, cit2);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testElectroDoubleTarget2() {
        addPlayer(BasicRoles.Citizen());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller witch = addPlayer(BasicRoles.Witch());

        electrify(electro, cit1);

        doubleCharge(electro, cit1, witch);

        endNight();

        isDead(cit1);
        isAlive(witch);

        assertStatus(witch, ElectroManiac.abilityType);
    }

    private void doubleCharge(Controller electro, Controller p1, Controller p2) {
        setTarget(electro, ElectroManiac.abilityType, null, p1, p2);
    }

    public void testElectroDoubleTargetDrugDealer() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller esc = addPlayer(BasicRoles.Escort());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller dd = addPlayer(BasicRoles.DrugDealer());

        drug(dd, electro, DrugDealer.BLOCKED);
        doubleCharge(electro, cit, cit);

        nextNight();
        isDead(cit);

        drug(dd, electro, DrugDealer.BLOCKED);
        doubleCharge(electro, esc, esc);

        nextNight();
        isAlive(esc);

        doubleCharge(electro, dd, dd);
        endNight();

        isAlive(dd);
    }

    public void testChargedVisitingElectrocutioner() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller elect = addPlayer(BasicRoles.ElectroManiac());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());

        setTarget(elect, sheriff);
        setTarget(sheriff, elect);

        endNight();

        isDead(sheriff);
    }

    public void testBreadChargesUsedUp() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, sheriff, cit);

        setTarget(baker, sheriff);

        nextNight();

        setTarget(sheriff, baker);
        setTarget(sheriff, cit);
        setTarget(doc, sheriff);

        endNight();

        isDead(cit);
        isAlive(sheriff, doc, baker);

        assertPassableBreadCount(0, sheriff);
    }

    public void testElectroBreadedVigiRegainsGuns() {
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, Constants.UNLIMITED);

        setTarget(baker, vig);
        setTarget(em, vig);

        nextNight();

        electrify(em, baker);

        shoot(vig, baker);
        shoot(vig, witch);
        assertActionSize(2, vig);
        setTarget(doc, vig);
        endNight();

        assertTotalGunCount(Constants.UNLIMITED, vig);

        isDead(baker, witch);
        isAlive(doc, vig);
    }

    public void testElectroBreadedReviveCompleteAction() {
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, Constants.UNLIMITED);

        setTarget(baker, bm);

        nextNight();

        electrify(em, bm, baker);

        setTarget(bm, baker);
        setTarget(bm, doc);
        setTarget(doc, bm);

        endNight();

        assertStatus(baker, Blackmailer.abilityType);
        assertStatus(doc, Blackmailer.abilityType);
    }

    public void testElectroNoRedoingAction() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller doc3 = addPlayer(BasicRoles.Doctor());
        Controller doc4 = addPlayer(BasicRoles.Doctor());
        Controller doc5 = addPlayer(BasicRoles.Doctor());
        Controller doc6 = addPlayer(BasicRoles.Doctor());
        Controller doc7 = addPlayer(BasicRoles.Doctor());
        Controller doc8 = addPlayer(BasicRoles.Doctor());
        Controller doc9 = addPlayer(BasicRoles.Doctor());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        TestUtil.removeAbility(BasicRoles.ElectroManiac().role, Bulletproof.abilityType);

        electrify(em, sk, maf, bd);

        setTarget(doc1, maf);// bd visiting
        setTarget(doc2, sk);// bd visiting
        setTarget(doc3, bd);// bd visiting

        setTarget(doc4, bd);// maf kill
        setTarget(doc5, bd);// electro maf visit

        setTarget(doc6, bd);// sk kill
        setTarget(doc7, bd);// sk electro visit

        setTarget(doc8, maf);// electro from visiting charged bd
        setTarget(doc9, sk);// electro from visiting charged bd

        drive(bd, sk, maf);
        setTarget(sk, bd);
        mafKill(maf, bd);

        endNight();

        isAlive(bd, sk, maf);
    }

    public void testElectroAssassinationsNotImmune() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller arso = addPlayer(BasicRoles.Arsonist());
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        TestUtil.removeAbility(BasicRoles.Arsonist().role, Bulletproof.abilityType);

        editRule(SetupModifierName.GS_DAY_GUNS, true);
        editRule(SetupModifierName.ARSON_DAY_IGNITES, 1);
        editRule(SetupModifierName.LAST_WILL, true);

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        witch(witch, gs, gs);
        setTarget(gs, witch);
        setTarget(arso, vet);
        assassin.setLastWill("assassin last will");
        cit.setLastWill("cit last will");

        nextNight();

        electrify(em, assassin, cit, arso, witch, gs);
        endNight();

        doDayAction(assassin, cit);

        isDead(cit, assassin);

        assertAttackSize(2, cit);

        burn(arso);

        isAlive(arso);
        isDead(vet);

        doDayAction(gs, Vigilante.abilityType, witch);

        isDead(witch, gs);
    }

    public void testElectroAssassinationsImmune() {
        modifyRole(BasicRoles.Assassin(), RoleModifierName.UNDETECTABLE, true);

        addPlayer(BasicRoles.Citizen(), 2);
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.ElectroManiac());
        TestUtil.addAbility(BasicRoles.Assassin().role, Bulletproof.abilityType);

        nightStart();
        isInvuln(assassin);

        electrify(em, cit, assassin);
        endNight();

        doDayAction(assassin, cit);

        isDead(cit);
        isAlive(assassin);

        assertAttackSize(2, cit);
    }

    public void testElectroVisitation() {
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, detect, cit, lookout);

        setTarget(doc2, detect);
        setTarget(doc, lookout);
        setTarget(lookout, cit);
        setTarget(detect, cit);

        endNight();

        TestLookout.seen(lookout, detect);
        assertAttackSize(2, cit);
    }

    public void testCoronerElectrocution() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, detect, coroner, amn);
        endNight();

        voteOut(detect, cit, em, coroner, witch);

        setTarget(coroner, detect);
        setTarget(amn, detect);

        endNight();

        assertAttackSize(1, detect);
        assertTrue(amn.is(Detective.abilityType));

        String happenings = game.getHappenings();
        assertTrue(happenings.contains(
                amn.getName() + "(Amnesiac) will remember they were like " + detect.getName() + "(Detective)."));
        assertTrue(
                happenings.contains(amn.getName() + "(Amnesiac) has become like " + detect.getName() + "(Detective)."));

    }

    public void testFrameVisitationNotTriggered() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller framed = addPlayer(BasicRoles.Citizen());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller killer = addPlayer(BasicRoles.Goon());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        nightStart();

        electrify(em, fodder, framed);

        frame(framer, framed, killer.getColor());
        mafKill(killer, fodder);

        endNight();

        isAlive(framed);
        assertAttackSize(1, fodder);
    }

    public void testReceivingItemsButDrugged() {
        Controller gunsmith = addPlayer(BasicRoles.Gunsmith());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller armorsmith = addPlayer(BasicRoles.Armorsmith());
        Controller receiver = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller drugDealer = addPlayer(BasicRoles.DrugDealer());
        Controller em = addPlayer(BasicRoles.ElectroManiac());

        electrify(em, drugDealer, receiver);

        setTarget(doc, receiver);
        setTarget(doc2, drugDealer);
        setTarget(gunsmith, receiver);
        setTarget(baker, receiver);
        setTarget(armorsmith, receiver);
        drug(drugDealer, receiver, DrugDealer.WIPE);

        endNight();

        assertTotalGunCount(0, receiver);
        assertPassableBreadCount(0, receiver);
        assertTotalVestCount(0, receiver);
    }

    public void testElectroReplaceAction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();
        doubleCharge(electro, cit, cit);
        Action newAction = new Action(electro.getPlayer(), ElectroManiac.abilityType, new LinkedList<>(),
                Player.list(witch.getPlayer(), cit.getPlayer()));
        electro.getPlayer().getActions().replace(electro.getPlayer().getAction(ElectroManiac.abilityType), newAction);
    }

}

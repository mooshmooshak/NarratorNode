package integration.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game.abilities.Doctor;
import game.abilities.Hidden;
import game.abilities.Sheriff;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.RoleModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.HiddenService;
import services.HiddenSpawnService;
import services.SetupHiddenService;
import util.models.RoleTestUtil;

public class TestHidden extends SuperTest {

    public TestHidden(String name) {
        super(name);
    }

    public void testSetRole() {
        Hidden rm = Hidden.TownRandom();
        SetupHidden sh1 = SetupHiddenService.addSetupHidden(rm, false, false, 0, Setup.MAX_PLAYER_COUNT);
        SetupHidden sh2 = SetupHiddenService.addSetupHidden(rm, false, false, 0, Setup.MAX_PLAYER_COUNT);
        SetupHiddenService.setForcedSpawn(game, sh1, BasicRoles.Citizen());
        SetupHiddenService.setForcedSpawn(game, sh2, BasicRoles.Sheriff());

        Controller c1 = addPlayer();
        Controller c2 = addPlayer();
        addPlayer(BasicRoles.SerialKiller());

        nightStart();

        assertNotSame(c1.is(Sheriff.abilityType), c2.is(Sheriff.abilityType));
    }

    public void testUniqueDouble() {
        Hidden rm = Hidden.TownRandom();
        SetupHidden sh1 = SetupHiddenService.addSetupHidden(rm, false, false, 0, Setup.MAX_PLAYER_COUNT);
        SetupHidden sh2 = SetupHiddenService.addSetupHidden(rm, false, false, 0, Setup.MAX_PLAYER_COUNT);

        SetupHiddenService.setForcedSpawn(game, sh1, BasicRoles.Mayor());

        try{
            SetupHiddenService.setForcedSpawn(game, sh2, BasicRoles.Mayor());
            fail();
        }catch(NarratorException e){
        }
    }

    public void testSettingUniqueSetupHiddens() {
        addRole(BasicRoles.Doctor());
        addRole(BasicRoles.Doctor());

        assertBadGameSettings();
    }

    public void testNewModifiers1() {
        addPlayer(BasicRoles.Citizen());
        Controller host = addPlayer(BasicRoles.Goon());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        mafKill(host, sk);

        endNight();
        assertInProgress();

    }

    public void testNewModifiers2() {
        addPlayer(BasicRoles.Citizen());
        Controller host = addPlayer(BasicRoles.Goon());
        Controller nk = addPlayer(Hidden.NeutralKillingRandom());

        mafKill(host, nk);

        endNight();
        assertInProgress();
    }

    // i have a list of unique roles
    // one setup hidden can spawn all but one of these roles (3 of these)
    // another setup hidden can spawn all of these roles (1 of these)
    //
    public void testUniqueOrderings() {
        addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Witch());
        String[] teamColors = new String[] { Setup.BENIGN_C, Setup.CULT_C, Setup.MAFIA_C, Setup.YAKUZA_C };
        Role doctorRole = RoleTestUtil.getRole(setup, "Doctor");
        List<FactionRole> factionRoles = new ArrayList<>();
        FactionRole factionRole;
        String teamColor;
        for(int i = 0; i < teamColors.length; i++){
            teamColor = teamColors[i];
            factionRole = FactionRoleService.createFactionRole(setup.getFactionByColor(teamColor), doctorRole);
            FactionRoleModifierService.upsertModifier(factionRole, RoleModifierName.UNIQUE, i != 0);
            factionRoles.add(factionRole);
        }

        Hidden hidden1 = HiddenService.createHidden(setup, "ColorCit3");
        Hidden hidden2 = HiddenService.createHidden(setup, "ColorCit2");
        for(int i = 0; i < factionRoles.size(); i++){
            if(i != 0)
                HiddenSpawnService.addSpawnableRoles(hidden1, factionRoles.get(i));
            HiddenSpawnService.addSpawnableRoles(hidden2, factionRoles.get(i));
        }

        for(int i = 0; i < 3; i++)
            SetupHiddenService.addSetupHidden(hidden1, false, false, 0, Setup.MAX_PLAYER_COUNT);
        SetupHiddenService.addSetupHidden(hidden2, false, false, 0, Setup.MAX_PLAYER_COUNT);

        addPlayer();
        addPlayer();
        addPlayer();
        addPlayer();

        dayStart();
        Set<String> colors = new HashSet<>();
        for(Controller p: game.players){
            if(!p.is(Doctor.abilityType))
                continue;
            if(colors.contains(p.getColor()))
                fail();
            colors.add(p.getColor());
        }
        assertEquals(teamColors.length, colors.size());
    }
}

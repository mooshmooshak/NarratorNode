package integration.logic;

import java.util.ArrayList;

import integration.logic.abilities.TestAmnesiac;
import integration.logic.abilities.TestArchitect;
import integration.logic.abilities.TestArmorsmith;
import integration.logic.abilities.TestArmsDetector;
import integration.logic.abilities.TestArsonist;
import integration.logic.abilities.TestAssassin;
import integration.logic.abilities.TestBaker;
import integration.logic.abilities.TestBlackmailer;
import integration.logic.abilities.TestBlacksmith;
import integration.logic.abilities.TestBlocker;
import integration.logic.abilities.TestBodyguard;
import integration.logic.abilities.TestBomb;
import integration.logic.abilities.TestBulletproofMiller;
import integration.logic.abilities.TestCommuter;
import integration.logic.abilities.TestCoroner;
import integration.logic.abilities.TestCoward;
import integration.logic.abilities.TestCult;
import integration.logic.abilities.TestDetective;
import integration.logic.abilities.TestDisfranchise;
import integration.logic.abilities.TestDisguiser;
import integration.logic.abilities.TestDoctor;
import integration.logic.abilities.TestDriver;
import integration.logic.abilities.TestDrugDealer;
import integration.logic.abilities.TestElector;
import integration.logic.abilities.TestElectromaniac;
import integration.logic.abilities.TestExecutioner;
import integration.logic.abilities.TestFactionKill;
import integration.logic.abilities.TestFactionSend;
import integration.logic.abilities.TestFramer;
import integration.logic.abilities.TestGhost;
import integration.logic.abilities.TestGodfather;
import integration.logic.abilities.TestGraveDigger;
import integration.logic.abilities.TestGunsmith;
import integration.logic.abilities.TestInfiltrator;
import integration.logic.abilities.TestInterceptor;
import integration.logic.abilities.TestInvestigator;
import integration.logic.abilities.TestJailor;
import integration.logic.abilities.TestJanitor;
import integration.logic.abilities.TestJester;
import integration.logic.abilities.TestJoker;
import integration.logic.abilities.TestLookout;
import integration.logic.abilities.TestMarshall;
import integration.logic.abilities.TestMason;
import integration.logic.abilities.TestMassMurderer;
import integration.logic.abilities.TestMayor;
import integration.logic.abilities.TestOperator;
import integration.logic.abilities.TestParity;
import integration.logic.abilities.TestParityCop;
import integration.logic.abilities.TestPoisoner;
import integration.logic.abilities.TestProxyKill;
import integration.logic.abilities.TestPunch;
import integration.logic.abilities.TestScout;
import integration.logic.abilities.TestSerialKiller;
import integration.logic.abilities.TestSheriff;
import integration.logic.abilities.TestSilencer;
import integration.logic.abilities.TestSleepwalker;
import integration.logic.abilities.TestSnitch;
import integration.logic.abilities.TestSpy;
import integration.logic.abilities.TestSurvivor;
import integration.logic.abilities.TestTailor;
import integration.logic.abilities.TestTeamTakedown;
import integration.logic.abilities.TestThief;
import integration.logic.abilities.TestVentriloquist;
import integration.logic.abilities.TestVeteran;
import integration.logic.abilities.TestVigilante;
import integration.logic.abilities.TestWitch;
import integration.unit.TestCommandModel;
import integration.unit.TestCondorcet;
import integration.unit.TestRoleAssignmentService;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LogicTests {

    public static ArrayList<Class<? extends TestCase>> NarratorTests() {
        ArrayList<Class<? extends TestCase>> tests = new ArrayList<>();

        tests.add(TestSheriff.class);
        tests.add(TestInvestigator.class);
        tests.add(TestDetective.class);
        tests.add(TestLookout.class);
        tests.add(TestSpy.class);
        tests.add(TestCoroner.class);
        tests.add(TestArmsDetector.class);
        tests.add(TestParityCop.class);

        tests.add(TestDoctor.class);
        tests.add(TestBlocker.class);
        tests.add(TestBodyguard.class);
        tests.add(TestArmorsmith.class);

        tests.add(TestDriver.class);
        tests.add(TestOperator.class);
        tests.add(TestCoward.class);

        tests.add(TestVeteran.class);
        tests.add(TestJailor.class);
        tests.add(TestGunsmith.class);
        tests.add(TestVigilante.class);

        tests.add(TestSnitch.class);
        tests.add(TestMayor.class);
        tests.add(TestMarshall.class);
        tests.add(TestMason.class);
        tests.add(TestArchitect.class);
        tests.add(TestBaker.class);
        tests.add(TestBlacksmith.class);
        tests.add(TestCommuter.class);

        tests.add(TestPunch.class);
        tests.add(TestBomb.class);
        tests.add(TestBulletproofMiller.class);
        tests.add(TestSleepwalker.class);

        tests.add(TestFactionKill.class);
        tests.add(TestFactionSend.class);
        tests.add(TestMafiaTeam.class);
        tests.add(TestTeamTakedown.class);
        tests.add(TestProxyKill.class);

        tests.add(TestBlackmailer.class);
        tests.add(TestSilencer.class);
        tests.add(TestDisfranchise.class);
        tests.add(TestJanitor.class);
        tests.add(TestGodfather.class);
        tests.add(TestFramer.class);
        tests.add(TestDisguiser.class);
        tests.add(TestAssassin.class);
        tests.add(TestDrugDealer.class);
        tests.add(TestTailor.class);
        tests.add(TestInfiltrator.class);

        tests.add(TestThief.class);
        tests.add(TestGhost.class);
        tests.add(TestJester.class);
        tests.add(TestExecutioner.class);
        tests.add(TestAmnesiac.class);
        tests.add(TestSurvivor.class);
        tests.add(TestAutoVest.class);

        tests.add(TestWitch.class);
        tests.add(TestGraveDigger.class);
        tests.add(TestVentriloquist.class);
        tests.add(TestElector.class);

        tests.add(TestCult.class);
        tests.add(TestScout.class);

        tests.add(TestMassMurderer.class);
        tests.add(TestInterceptor.class);
        tests.add(TestArsonist.class);
        tests.add(TestSerialKiller.class);
        tests.add(TestElectromaniac.class);
        tests.add(TestPoisoner.class);
        tests.add(TestJoker.class);

        tests.add(TestTalking.class);
        tests.add(TestLastWill.class);
        tests.add(TestEvent.class);
        tests.add(TestPluralityVoteSystem.class);
        tests.add(TestMultiVotePluralitySystem.class);
        tests.add(TestDiminishingPoolVoteSystem.class);
        tests.add(TestNoEliminationVoteSystem.class);
        tests.add(TestStructure.class);
        tests.add(TestGameSettings.class);
        tests.add(TestPrefer.class);
        tests.add(TestParity.class);
        tests.add(TeamTests.class);
        tests.add(TestHidden.class);

        tests.add(TestRolesList.class);
        tests.add(TestHiddenSpawns.class);

        tests.add(TestCommandModel.class);
        tests.add(TestCondorcet.class);
        tests.add(TestRoleAssignmentService.class);

        tests.add(TestMashRole.class);
        tests.add(TestModifiers.class);
        tests.add(TestModifierText.class);
        tests.add(TestModifierFactionSize.class);
        tests.add(TestModifierPlayerBounds.class);

        tests.add(TestBrain.class);

        return tests;
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(LogicTests.class.getName());

        for(Class<? extends TestCase> testCase: NarratorTests())
            suite.addTestSuite(testCase);

        return suite;
    }

}

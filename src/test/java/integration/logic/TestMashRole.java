package integration.logic;

import java.util.LinkedList;
import java.util.List;

import game.abilities.AbilityList;
import game.abilities.Amnesiac;
import game.abilities.Architect;
import game.abilities.Baker;
import game.abilities.BreadAbility;
import game.abilities.Doctor;
import game.abilities.DrugDealer;
import game.abilities.GameAbility;
import game.abilities.Ghost;
import game.abilities.Hidden;
import game.abilities.JailCreate;
import game.abilities.JailExecute;
import game.abilities.Lookout;
import game.abilities.SerialKiller;
import game.abilities.Sheriff;
import game.abilities.Ventriloquist;
import game.abilities.util.AbilityUtil;
import game.ai.Controller;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.abilities.TestLookout;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.HiddenService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.Assertions;
import util.game.HiddenUtil;

public class TestMashRole extends SuperTest {

    public TestMashRole(String s) {
        super(s);
    }

    public void testBakerLookout() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role mashedRole = RoleService.createRole(setup, "LookoutBaker", Lookout.abilityType, Baker.abilityType);

        Controller mash = addPlayer(FactionRoleService.createFactionRole(mafia, mashedRole));
        Controller jester1 = addPlayer(BasicRoles.Jester());
        Controller jester2 = addPlayer(BasicRoles.Jester());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.SELF_BREAD_USAGE, true);

        nightStart();

        assertPassableBreadCount(1, mash);
        setTarget(mash, cit1, Lookout.abilityType);
        setTarget(mash, cit2, Lookout.abilityType);
        assertActionSize(2, mash);
        cancelAction(mash, 1);
        assertActionSize(1, mash);
        setTarget(mash, cit2, Lookout.abilityType);
        setTarget(mash, cit2, Lookout.abilityType);
        setTarget(jester1, cit1);
        setTarget(jester2, cit2);

        endNight();

        TestLookout.seen(mash, jester1);
        TestLookout.seen(mash, jester2);
    }

    public void testDifferentCharges() {
        Faction mafiaFaction = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "LookoutDoctor", Doctor.abilityType, Lookout.abilityType);
        RoleAbilityModifierService.upsert(role, Doctor.abilityType, AbilityModifierName.CHARGES, 1);

        Controller mash = addPlayer(FactionRoleService.createFactionRole(mafiaFaction, role));
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 3);

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        nightStart();

        assertRealChargeRemaining(Constants.UNLIMITED, mash, Lookout.abilityType);
        assertRealChargeRemaining(1, mash, Doctor.abilityType);

        setTarget(mash, cit1, Doctor.abilityType);
        endNight();

        assertRealChargeRemaining(Constants.UNLIMITED, mash, Lookout.abilityType);
        assertRealChargeRemaining(0, mash, Doctor.abilityType);

    }

    public void testBadAbilities() {
        badAbilitiyList(Amnesiac.abilityType, Sheriff.abilityType);
    }

    @SafeVarargs
    final private void badAbilitiyList(AbilityType... abilityTypes) {
        try{
            List<GameAbility> abilities = new LinkedList<>();
            for(AbilityType abilityType: abilityTypes)
                abilities.add(AbilityUtil.CREATOR(abilityType, game, new Modifiers<>()));

            new AbilityList(abilities);
            fail();
        }catch(IllegalRoleCombinationException e){
        }
    }

    // no using other abilities while dead
    public void testGhostAbilities() {
        Role ghostWithTeamRole = RoleService.createRole(setup, "GhostDoctor", Doctor.abilityType, Ghost.abilityType);
        FactionRole ghostWithTeam = FactionRoleService.createFactionRole(setup.getFactionByColor(Setup.MAFIA_C),
                ghostWithTeamRole);
        HiddenUtil.createHiddenSingle(setup, ghostWithTeam);

        Controller ghosty = addPlayer(ghostWithTeam);
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        assertBadGameSettings();

        deleteRole(ghostWithTeamRole);
        Assertions.assertRoleNotInSetup(ghostWithTeamRole);
        assertEquals(3, game.setup.rolesList.size(game));

        Controller ars = addPlayer(BasicRoles.Arsonist());
        Role ghostDoctorRole = RoleService.createRole(setup, "ghost doctor", Ghost.abilityType, Doctor.abilityType,
                Ventriloquist.abilityType);
        Faction benigns = setup.getFactionByColor(Setup.BENIGN_C);
        addSetupHidden(
                HiddenUtil.createHiddenSingle(setup, FactionRoleService.createFactionRole(benigns, ghostDoctorRole)));

        for(Hidden hidden: new LinkedList<>(setup.hiddens)){
            if(hidden.size() == 0)
                HiddenService.delete(hidden);
        }

        nightStart();
        try{
            setTarget(ghosty, lookout, Ventriloquist.abilityType);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(ghosty, lookout, Doctor.abilityType);
        setTarget(lookout, lookout);
        endNight();

        TestLookout.seen(lookout, ghosty);

        voteOut(ghosty, ars, lookout, cit2);

        try{
            setTarget(ghosty, lookout, Doctor.abilityType);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(ghosty, lookout, Ventriloquist.abilityType);

        endNight();

        isPuppeted(lookout);
        hasPuppets(ghosty);
    }

    public void testRoleNamesUnique() {
        Controller p = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Doctor());

        try{
            game.addPlayer(BasicRoles.Agent().getName());
            fail();
        }catch(NamingException e){
        }

        try{
            RoleService.createRole(setup, DrugDealer.WIPE, Doctor.abilityType, SerialKiller.abilityType);
            fail();
        }catch(NamingException e){
        }

        RoleService.createRole(setup, p.getName(), Doctor.abilityType, SerialKiller.abilityType);
    }

    public void testMashAbilityCooldown() {
        Role mashedRole = RoleService.createRole(setup, "LookoutBaker", Lookout.abilityType, Baker.abilityType);
        RoleAbilityModifierService.upsert(mashedRole, Baker.abilityType, AbilityModifierName.COOLDOWN, 2);
        FactionRole mashed = FactionRoleService.createFactionRole(setup.getFactionByColor(Setup.MAFIA_C), mashedRole);

        Controller mash = addPlayer(mashed);
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifierName.SELF_BREAD_USAGE, true);

        nightStart();

        // n0
        assertPassableBreadCount(1, mash);
        nextNight();

        // n1
        assertPassableBreadCount(1, mash);
        nextNight();

        // n2
        assertPassableBreadCount(1, mash);
        nextNight();

        assertPassableBreadCount(2, mash);
    }

    public void testMashAbilityCooldownNoSelfBread() {
        Role mashedRole = RoleService.createRole(setup, "LookoutBaker", Lookout.abilityType, Baker.abilityType);
        RoleAbilityModifierService.upsert(mashedRole, Baker.abilityType, AbilityModifierName.COOLDOWN, 2);
        Faction faction = setup.getFactionByColor(Setup.MAFIA_C);
        FactionRole mashed = FactionRoleService.createFactionRole(faction, mashedRole);

        Controller mash = addPlayer(mashed);
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);

        editRule(SetupModifierName.SELF_BREAD_USAGE, false);

        nightStart();

        // n0
        assertPassableBreadCount(1, mash);
        assertUseableBreadCount(0, mash);

        nextNight();

        // n1
        assertPassableBreadCount(1, mash);
        assertUseableBreadCount(0, mash);

        setTarget(mash, cit, BreadAbility.abilityType);
        nextNight();

        // n2
        assertPassableBreadCount(1, cit);
        assertPassableBreadCount(0, mash);
        assertCooldownRemaining(2, mash, Baker.abilityType);
        try{
            setTarget(BreadAbility.getAction(mash, cit, game));
            fail();
        }catch(PlayerTargetingException e){
        }
        nextNight();

        // n3
        assertPassableBreadCount(0, mash);
        try{
            setTarget(BreadAbility.getAction(mash, cit, game));
            fail();
        }catch(PlayerTargetingException e){
        }
        nextNight();

        // n4
        assertPassableBreadCount(1, mash);
        // assertEquals(1, mash.getBread().size());
        setTarget(mash, cit, BreadAbility.abilityType);
        assertUseableBreadCount(0, mash);
        nextNight();

        // n5
        assertPassableBreadCount(2, cit);
    }

    public void testArchitectJailorCanceling() {
        Role mashedRole = RoleService.createRole(setup, "JailorArchitect", Architect.abilityType,
                JailExecute.abilityType);
        FactionRole mashed = FactionRoleService.createFactionRole(setup.getFactionByColor(Setup.TOWN_C), mashedRole);

        Controller jarch = addPlayer(mashed);
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(baker, jarch);
        endNight();

        jail(jarch, baker);
        arch(jarch, baker, goon);
        cancelAction(jarch, 0);
        assertNotNull(jarch.getPlayer().getAction(Architect.abilityType));
        assertNull(jarch.getPlayer().getAction(JailCreate.abilityType));
    }

    public void testPassingAndSubmittingBread() {
        Role mashedRole = RoleService.createRole(setup, "LoookoutBaker", Lookout.abilityType, Baker.abilityType);
        FactionRole mashed = FactionRoleService.createFactionRole(setup.getFactionByColor(Setup.MAFIA_C), mashedRole);

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller mash = addPlayer(mashed);
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit4 = addPlayer(BasicRoles.Citizen());

        game.setValue(SetupModifierName.BREAD_PASSING, true);
        game.setValue(SetupModifierName.SELF_BREAD_USAGE, true);

        nightStart();
        assertPassableBreadCount(1, mash);

        setTarget(mash, cit, Lookout.abilityType);
        setTarget(mash, cit3, BreadAbility.abilityType);
        assertActionSize(2, mash);
        setTarget(mash, cit4, Lookout.abilityType);
        setTarget(mash, cit2, BreadAbility.abilityType);
        assertActionSize(2, mash);

        // try replacing the action as well

        endNight();
    }

}

package integration.logic;

import java.util.Set;

import game.abilities.Hidden;
import game.ai.Brain;
import game.ai.Claim;
import game.ai.Computer;
import game.ai.Controller;
import game.logic.support.Random;
import game.logic.templates.BasicRoles;
import models.FactionRole;
import services.HiddenService;
import util.RepeatabilityTest;

public class TestBrain extends SuperTest {

    public TestBrain(String name) {
        super(name);
    }

    public void testBrain() {
        SuperTest.printHappenings = false;
        SuperTest.checkEnclosingChats = false;
        Long prevSeed = null;
        for(int j = 0; j < 3; j++){

            newNarrator();

            long t = new Random().nextLong();
//             t = Long.parseLong("-7997947417764629821");

            if(prevSeed != null && t == prevSeed)
                break;
            prevSeed = t;

            System.out.println("Brain" + (j + 1) + ": " + t);
            game.setSeed(t);

            addPlayer(Hidden.TownRandom(), 10);
            addPlayer(Hidden.MafiaRandom(), 4);
            addPlayer(Hidden.YakuzaRandom(), 4);
            addPlayer(Hidden.NeutralRandom(), 2);

            dayStart();

            try{
                simulationTest(t);
            }catch(Exception e){
                writeToFile("log1.tx", game);
                e.printStackTrace();
                fail(t);
            }

            RepeatabilityTest.runRepeatabilityTest();
        }
        skipTearDown = true;
    }

    public void testSafeClaim() {
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller m3 = addPlayer(BasicRoles.Goon());
        Controller m2 = addPlayer(BasicRoles.Goon());
        Controller m1 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        Brain b = new Brain(game, game.getRandom());
        assertEquals(7, b.publicRoles.size());

        Set<String> safeColors = b.getSafeColors(game);
        assertEquals(1, safeColors.size());
        assertTrue(safeColors.contains(c2.getColor()));

        voteOut(c2, c1, m1, m2, m3);
        assertEquals(6, b.publicRoles.size());
        mafKill(m1, c1);
        endNight();

        isDead(c1);
        safeColors = b.getSafeColors(game);
        assertEquals(1, safeColors.size());
        assertEquals(5, b.publicRoles.size());
        assertTrue(safeColors.contains(m1.getColor()));
    }

    public void roleClaim() {

        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        Brain b = new Brain(game, game.getRandom());
        Claim c = b.computers.get(c2).roleClaim();
        assertTrue(c.believable(b.computers.get(c1)));
    }

    public void testAvailable() {
        FactionRole a = BasicRoles.Citizen();
        FactionRole b = BasicRoles.Doctor();
        FactionRole c = BasicRoles.Sheriff();
        FactionRole d = BasicRoles.Sheriff();
        Hidden r1 = HiddenService.createHidden(setup, "ABCD", a, b, c, d);
        Hidden r2 = HiddenService.createHidden(setup, "ABC", a, b, c);
        Hidden r3 = HiddenService.createHidden(setup, "AB", a, b);

        addPlayer(r1);
        addPlayer(r2);
        addPlayer(r3);
        addPlayer(BasicRoles.Goon());

        dayStart();

        Brain brain = new Brain(game, new Random());
        assertEquals(3, brain.count(a));
    }

    public void testCrossOff() {
        FactionRole a = BasicRoles.Citizen();
        FactionRole b = BasicRoles.Doctor();
        FactionRole c = BasicRoles.Sheriff();
        FactionRole d = BasicRoles.Sheriff();
        Hidden r1 = HiddenService.createHidden(setup, "ABCD", a, b, c, d);
        Hidden r2 = HiddenService.createHidden(setup, "ABC", a, b, c);
        Hidden r3 = HiddenService.createHidden(setup, "AB", a, b);

        addPlayer(r1);
        addPlayer(r2);
        addPlayer(r3);
        addPlayer(BasicRoles.Goon());

        dayStart();

        Brain brain = new Brain(game, new Random());
        brain.publicRoles.crossOff(a);
        assertEquals(3, brain.publicRoles.size());
    }

    public void testCompetingClaims() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Doctor());
        Controller p4 = addPlayer(BasicRoles.Goon());

        dayStart();
        Brain brain = new Brain(game, new Random());

        Computer c1 = brain.computers.get(p1);
        Computer c2 = brain.computers.get(p2);
        Computer c3 = brain.computers.get(p3);
        Computer c4 = brain.computers.get(p4);

        c4.roleClaim();
        c1.roleClaim();
        c2.roleClaim();
        c3.roleClaim();

        assertEquals(4, brain.getClaims().size());
    }

    public void testBrainMassBlackmailer() {
        for(int j = 0; j < 2; j++){
            newNarrator();
            long seed = new Random().nextLong();
            // seed = Long.parseLong("-6474779550190453190");
            System.out.println("Mass Blackmailer: " + seed);
            game.setSeed(seed);

            addPlayer(BasicRoles.Detective(), 5);
            addPlayer(BasicRoles.Blackmailer(), 16);

            dayStart();

            simulationTest(seed);
            RepeatabilityTest.runRepeatabilityTest();
        }
        skipTearDown = true;
    }

    public void testRandom() {
        Random r = new Random();
        long l = r.nextLong();
        r.setSeed(l);

        r.reset();
        l = r.nextLong();

        r.reset();
        assertEquals(l, r.nextLong());

        r.reset();
        assertEquals(l, r.nextLong());

    }
}

package integration.logic;

import game.setups.Setup;
import models.enums.SetupModifierName;
import services.SetupModifierService;

public class TestModifierPlayerBounds extends SuperTest {

    public TestModifierPlayerBounds(String name) {
        super(name);
    }

    public void testSetupModifierValueBoundsOverwriting() throws InterruptedException {
        for(int i = 0; i < 5; i++)
            addPlayer();

        setModifier(false, 0, Setup.MAX_PLAYER_COUNT);

        assertSkipVote(false);

        setModifier(true, 5, 5);

        assertSkipVote(true);

        setModifier(false, 6, 8);

        assertSkipVote(true);

        setModifier(false, 3, 9);

        assertSkipVote(false);

        setModifier(true, 0, Setup.MAX_PLAYER_COUNT);

        assertSkipVote(true);

        setModifier(false, 0, Setup.MAX_PLAYER_COUNT);

        assertSkipVote(false);

    }

    private static void setModifier(boolean value, int minPlayerCount, int maxPlayerCount) throws InterruptedException {
        SetupModifierService.upsertModifier(setup, SetupModifierName.SKIP_VOTE, value, minPlayerCount, maxPlayerCount);
        Thread.sleep(1);
    }

    private static void assertSkipVote(boolean b) {
        assertEquals(b, game.setup.modifiers.getBoolean(SetupModifierName.SKIP_VOTE, game));
    }

}

package integration.logic;

import game.abilities.CultLeader;
import game.abilities.Disguiser;
import game.abilities.FactionKill;
import game.abilities.Infiltrator;
import game.abilities.SerialKiller;
import game.abilities.Block;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import services.FactionAbilityModifierService;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.RoleAbilityModifierService;
import services.RoleService;

public class TestModifierFactionSize extends SuperTest {
    public TestModifierFactionSize(String name) {
        super(name);
    }

    public void testMaxFactionSizeUsageForFactionAbility() {
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon());
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        Controller citizen3 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        Faction mafia = BasicRoles.Goon().faction;
        FactionAbilityModifierService.upsertModifier(mafia, FactionKill.abilityType,
                AbilityModifierName.FACTION_COUNT_MAX, 1);

        try{
            mafKill(goon2, citizen2);
            fail();
        }catch(NarratorException e){
        }
        assertTrue(goon2.getPlayer().getAcceptableTargets(FactionKill.abilityType).isEmpty());

        endNight();

        voteOut(goon1, citizen2, goon2, citizen3, citizen1);

        mafKill(goon2, citizen1);
        endNight();

        isDead(citizen1);
    }

    public void testMaxFactionSizeUsageForRoleAbility() {
        Controller blocker = addPlayer(BasicRoles.Consort());
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        Controller citizen3 = addPlayer(BasicRoles.Citizen());

        FactionRoleModifierService.addModifier(BasicRoles.Consort(), AbilityModifierName.FACTION_COUNT_MAX,
                Block.abilityType, 1);

        try{
            setTarget(blocker, citizen2, Block.abilityType);
            fail();
        }catch(NarratorException e){
        }
        assertTrue(blocker.getPlayer().getAcceptableTargets(Block.abilityType).isEmpty());

        endNight();

        voteOut(goon1, citizen2, blocker, citizen3);

        assertIsNight();
        isDead(goon1);

        setTarget(blocker, citizen1);
    }

    public void testMinFactionSizeUsageForFactionAbility() {
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon());
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        Controller citizen3 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        Faction mafia = BasicRoles.Goon().faction;
        FactionAbilityModifierService.upsertModifier(mafia, FactionKill.abilityType,
                AbilityModifierName.FACTION_COUNT_MIN, 2);

        mafKill(goon1, citizen1);
        endNight();

        voteOut(goon1, citizen2, goon2, citizen3);

        try{
            mafKill(goon2, citizen2);
            fail();
        }catch(NarratorException e){
        }
        assertTrue(goon2.getPlayer().getAcceptableTargets(FactionKill.abilityType).isEmpty());
    }

    public void testMinFactionSizeUsageForRoleAbility() {
        Controller blocker = addPlayer(BasicRoles.Consort());
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        Controller citizen3 = addPlayer(BasicRoles.Citizen());

        FactionRoleModifierService.addModifier(BasicRoles.Consort(), AbilityModifierName.FACTION_COUNT_MIN,
                Block.abilityType, 2);

        setTarget(blocker, citizen1);
        endNight();

        voteOut(goon1, citizen2, blocker, citizen3);

        assertIsNight();
        isDead(goon1);
        try{
            setTarget(blocker, citizen2, Block.abilityType);
            fail();
        }catch(NarratorException e){
        }
        assertTrue(blocker.getPlayer().getAcceptableTargets(Block.abilityType).isEmpty());
    }

    public void testRecruitedInfiltratorEnablesRole() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role infilRole = RoleService.createRole(setup, "Infiltrator", Infiltrator.abilityType);
        Faction cult = setup.getFactionByColor(Setup.CULT_C);
        Role cultRole = RoleService.createRole(setup, "Rebel Leader", CultLeader.abilityType, SerialKiller.abilityType);
        RoleAbilityModifierService.upsert(cultRole, SerialKiller.abilityType, AbilityModifierName.FACTION_COUNT_MIN, 2);

        Controller infil = addPlayer(FactionRoleService.createFactionRole(mafia, infilRole));
        Controller cultLeader = addPlayer(FactionRoleService.createFactionRole(cult, cultRole));
        Controller citizen = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 3);

        setTarget(cultLeader, infil, CultLeader.abilityType);
        nextNight();

        setTarget(cultLeader, citizen, SerialKiller.abilityType);
        endNight();

        isDead(citizen);
    }

    public void testDisguiserKeepsUpMin() {
        Faction cult = setup.getFactionByColor(Setup.CULT_C);
        Role cultRole = RoleService.createRole(setup, "Rebel Leader", CultLeader.abilityType, SerialKiller.abilityType);
        RoleAbilityModifierService.upsert(cultRole, SerialKiller.abilityType, AbilityModifierName.FACTION_COUNT_MIN, 2);

        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller cultLeader = addPlayer(FactionRoleService.createFactionRole(cult, cultRole));
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);

        setTarget(cultLeader, citizen, CultLeader.abilityType);
        nextNight();

        setTarget(disguiser, citizen, Disguiser.abilityType);
        nextNight();

        setTarget(cultLeader, citizen2, SerialKiller.abilityType);
        endNight();

        isDead(citizen);
    }

    // check infiltrator counting for this
    // check disguiser counting for this
}

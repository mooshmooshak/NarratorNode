package integration.controller;

import java.sql.SQLException;

import integration.ControllerTestCase;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import nnode.Lobby;
import util.TestChatUtil;
import util.TestRequest;
import util.models.GameTestUtil;
import util.models.PlayerTestUtil;

public class TestChatController extends ControllerTestCase {

    public void testGetPreviousDeadChatMessages() throws JSONException, SQLException {
        Lobby game = GameTestUtil.createStartedGame();
        long deadID = PlayerTestUtil.getSchemaByRoleName(game, "Doctor").userID.get();
        GameTestUtil.voteOutByRoleName(game, "Citizen");
        GameTestUtil.speakDeadByRoleName(game, "Citizen");
        GameTestUtil.stabByRoleNameDoctor(game, "Doctor");

        JSONObject serverResponse = sendChatRequest(deadID, game.gameID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousMasonChatMessages() throws JSONException, SQLException {
        Lobby game = GameTestUtil.createStartedGame();
        long citizenID = PlayerTestUtil.getSchemaByRoleName(game, "Citizen").userID.get();
        GameTestUtil.skipToNight(game);
        GameTestUtil.speakFactionByRoleName(game, "Mason");
        GameTestUtil.recruitByRoleName(game, "Citizen");

        JSONObject serverResponse = sendChatRequest(citizenID, game.gameID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousArchitectChatMessages() throws JSONException, SQLException {
        Lobby game = GameTestUtil.createStartedGame();
        long architectID = PlayerTestUtil.getSchemaByRoleName(game, "Architect").userID.get();
        GameTestUtil.buildRoom(game);
        GameTestUtil.speakRoom(game);
        GameTestUtil.skipToDay(game);

        JSONObject serverResponse = sendChatRequest(architectID, game.gameID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousFactionMessagesFromDisguise() throws JSONException, SQLException {
        Lobby game = GameTestUtil.createStartedGame();
        long disguiserID = PlayerTestUtil.getSchemaByRoleName(game, "Disguiser").userID.get();
        GameTestUtil.skipToNight(game);
        GameTestUtil.speakFactionByRoleName(game, "Goon");
        GameTestUtil.disguiseByRoleName(game, "Goon");

        JSONObject serverResponse = sendChatRequest(disguiserID, game.gameID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousMasonMessagesFromDisguise() throws JSONException, SQLException {
        Lobby game = GameTestUtil.createStartedGame();
        long disguiserID = PlayerTestUtil.getSchemaByRoleName(game, "Disguiser").userID.get();
        GameTestUtil.skipToNight(game);
        GameTestUtil.speakFactionByRoleName(game, "Mason");
        GameTestUtil.disguiseByRoleName(game, "Mason");

        JSONObject serverResponse = sendChatRequest(disguiserID, game.gameID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    public void testGetPreviousFactionMessagesFromInfiltrator() throws JSONException, SQLException {
        Lobby game = GameTestUtil.createStartedGame();
        long infiltratorID = PlayerTestUtil.getSchemaByRoleName(game, "Infiltrator").userID.get();
        GameTestUtil.skipToNight(game);
        GameTestUtil.speakFactionByRoleName(game, "Mason");
        GameTestUtil.recruitByRoleName(game, "Infiltrator");

        JSONObject serverResponse = sendChatRequest(infiltratorID, game.gameID);

        TestChatUtil.assertMessageTextInChatMessages(serverResponse);
    }

    private static JSONObject sendChatRequest(long userID, GameID gameID) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("gameID", gameID);
        request.put("userID", userID);
        return TestRequest.get("chats", request);
    }
}

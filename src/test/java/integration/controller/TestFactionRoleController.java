package integration.controller;

import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.abilities.Blacksmith;
import game.setups.Setup;
import integration.ControllerTestCase;
import integration.server.UserWrapper;
import json.JSONException;
import json.JSONObject;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.modifiers.Modifiers;
import models.requests.FactionRoleAbilityModifierRequest;
import models.schemas.AbilityModifierSchema;
import models.schemas.RoleModifierSchema;
import repositories.FactionRoleAbilityModifierRepo;
import repositories.FactionRoleModifierRepo;
import util.FakeConstants;
import util.TestRequest;
import util.TestUtil;

public class TestFactionRoleController extends ControllerTestCase {
    public void testFactionRoleAbilityModifierPutTrue() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Setup setup = host.getSetup();
        JSONObject request = new JSONObject();
        Faction benigns = setup.getFactionByColor(Setup.OUTCAST_C);
        FactionRole blacksmith = null;
        for(FactionRole factionRole: benigns.roleMap.values()){
            if(factionRole.role.hasAbility(Blacksmith.abilityType))
                blacksmith = factionRole;
        }
        request.put("userID", host.id);
        request.put("factionRoleID", blacksmith.id);
        request.put("abilityID", blacksmith.role.getAbility(Blacksmith.abilityType).id);
        request.put("value", false);
        request.put("minPlayerCount", 0);
        request.put("maxPlayerCount", Setup.MAX_PLAYER_COUNT);
        request.put("name", AbilityModifierName.AS_FAKE_VESTS.toString());

        JSONObject response = TestRequest.post("factionRoleAbilityModifiers", request);
        TestUtil.assertNoErrors(response);
        Ability ability = blacksmith.role.getAbility(Blacksmith.abilityType);
        JSONObject modifierResponse = response.getJSONObject("response").getJSONObject("modifier");
        Map<Long, Map<Long, Set<AbilityModifierSchema>>> map = FactionRoleAbilityModifierRepo
                .getAbilityModifiersSync(setup.id);
        Set<AbilityModifierSchema> modifiers = map.get(blacksmith.id)
                .get(blacksmith.role.getAbility(Blacksmith.abilityType).id);

        assertFalse(ability.modifiers.getBoolean(AbilityModifierName.AS_FAKE_VESTS, Optional.empty()));
        assertFalse(modifierResponse.getBoolean("value"));
        for(AbilityModifierSchema modifierSchema: modifiers){
            if(modifierSchema.name != AbilityModifierName.AS_FAKE_VESTS)
                continue;
            if(modifierSchema.value.equals(Boolean.FALSE))
                return;
        }
        fail("Did not find saved modifier in database");
    }

    public void testFactionRoleAbilityModifierPutFalse() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Setup setup = host.getSetup();
        FactionRole blacksmith = setup.getFactionByColor(Setup.OUTCAST_C)
                .getFactionRolesWithAbility(Blacksmith.abilityType).iterator().next();

        long abilityID = blacksmith.role.getAbility(Blacksmith.abilityType).id;
        FactionRoleAbilityModifierRequest request = new FactionRoleAbilityModifierRequest(abilityID, blacksmith.id,
                Setup.MAX_PLAYER_COUNT, 0, AbilityModifierName.AS_FAKE_VESTS, new ModifierValue(true), host.id);

        JSONObject response = TestRequest.post("factionRoleAbilityModifiers", request.toJson());

        TestUtil.assertNoErrors(response);
        JSONObject modifierResponse = response.getJSONObject("response").getJSONObject("modifier");

        assertTrue(modifierResponse.getBoolean("value"));
        blacksmith = host.getSetup().getFactionByColor(Setup.OUTCAST_C)
                .getFactionRolesWithAbility(Blacksmith.abilityType).iterator().next();
        Modifiers<AbilityModifierName> abilityModifiers = blacksmith.abilityModifiers.get(Blacksmith.abilityType);
        assertTrue(abilityModifiers.getBoolean(AbilityModifierName.AS_FAKE_VESTS, Optional.empty()));
    }

    public void testFactionRoleModifier() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Setup setup = host.getSetup();
        FactionRole factionRole = setup.factions.values().iterator().next().roleMap.values().iterator().next();
        boolean value = !factionRole.roleModifiers.getBoolean(RoleModifierName.UNBLOCKABLE, Optional.empty());
        JSONObject request = new JSONObject();
        request.put("userID", host.id);
        request.put("factionRoleID", factionRole.id);
        request.put("name", RoleModifierName.UNBLOCKABLE.toString());
        request.put("value", value);
        request.put("minPlayerCount", 0);
        request.put("maxPlayerCount", Setup.MAX_PLAYER_COUNT);

        JSONObject response = TestRequest.post("factionRoleModifiers", request);
        TestUtil.assertNoErrors(response);
        Map<Long, Set<RoleModifierSchema>> map = FactionRoleModifierRepo.getRoleModifiersSync(setup.id);
        Set<RoleModifierSchema> modifiers = map.get(factionRole.id);

        for(RoleModifierSchema modifierSchema: modifiers){
            if(modifierSchema.name != RoleModifierName.UNBLOCKABLE)
                continue;
            if(modifierSchema.value.equals(Boolean.valueOf(value)))
                return;
        }
        fail("Did not find saved modifier in database");
    }
}

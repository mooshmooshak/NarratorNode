package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;

import game.abilities.Hidden;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.idtypes.GameID;
import models.schemas.SetupHiddenSchema;
import nnode.DBStoryPackage;
import nnode.StoryWriter;
import repositories.BaseRepo;
import repositories.Connection;
import repositories.GameRepo;
import repositories.SetupHiddenRepo;
import repositories.UserRepo;
import services.GameService;
import services.SetupService;

public class TestCases extends SuperTest {

    public TestCases(String name) {
        super(name);
    }

    @Override
    public void setUp() throws Exception {
        BaseRepo.connection = new Connection();
        super.setUp();
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        BaseRepo.connection.close();
    }

    private static final String INSTANCE_ID = "$$$$";
    private static final boolean IS_PRIVATE = true;

    public void testRandomMemberCopyMultiEntry() throws SQLException {
        addPlayer(Hidden.TownRandom());
        addPlayer(Hidden.TownRandom());
        addPlayer(BasicRoles.Assassin());

        GameService.internalStart(game);

        game.setup.ownerID = UserRepo.create("forTestCase");
        SetupService.insert(setup);
        GameID gameID = GameRepo.create(INSTANCE_ID, IS_PRIVATE, game.getSeed(), game.setup.id);

        StoryWriter sw = new StoryWriter(game.getCommands());
        sw.setGameID(gameID, game.players.getDatabaseMap());

        ArrayList<SetupHiddenSchema> setupHiddens = SetupHiddenRepo
                .getBySetupIDSync(GameRepo.getSetupID(sw.getGameID()));
        assertEquals(3, setupHiddens.size());

        DBStoryPackage.deserialize(sw.getGameID());
        GameRepo.deleteByID(sw.getGameID());

        SuperTest.skipTearDown = true;
    }

}

package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import json.JSONException;
import json.JSONObject;
import nnode.Lobby;
import nnode.LobbyManager;
import nnode.LobbyManager.SwitchListener;
import nnode.StateObject;
import repositories.GameUserRepo;
import repositories.SetupRepo;

public class SwitchWrapper {

    static ArrayList<JSONObject> stack;
    static List<JSONObject> serverStack;
    static ArrayList<Long> idCleanup;
    static HashMap<Long, UserWrapper> phoneBook;

    public static void init() {
        Lobby.runningTests = true;
        idCleanup = new ArrayList<Long>();
        phoneBook = new HashMap<>();
        stack = new ArrayList<>();
        serverStack = new ArrayList<>();

        LobbyManager.switchListener = new SwitchListener() {
            @Override
            public void onSwitchMessage(JSONObject jo) {
                try{
                    if(jo.has("event")){
                        serverStack.add(jo);
                        return;
                    }
                    Long to_id;
                    if(jo.has(StateObject.userID)){
                        to_id = jo.getLong(StateObject.userID);
                    }else{
                        to_id = Long.valueOf(jo.getString("requestID"));
                        jo.put(StateObject.userID, to_id);
                    }
                    UserWrapper nc = phoneBook.get(to_id);
                    if(nc == null)
                        stack.add(jo);
                    else
                        phoneBook.get(to_id).receiveMessage(jo);
                }catch(JSONException | SQLException e){
                    e.printStackTrace();
                }

            }
        };
    }

    public static void cleanUp() throws SQLException {
        killDBConnections();
        if(idCleanup.isEmpty())
            return;
        for(Long setupID: idCleanup)
            SetupRepo.deleteByID(setupID);
    }

    public static JSONObject send(JSONObject jo) throws JSONException {
        return LobbyManager.handleMessage(jo);
    }

    public static void killDBConnections() {
        LobbyManager.killDBConnections();
    }

    public static ArrayList<UserWrapper> getUserWrappers(UserWrapper userWrapper) throws SQLException {
        ArrayList<UserWrapper> userWrappers = new ArrayList<>();

        Set<Long> userIDs = GameUserRepo.getIDs(userWrapper.getGameID());
        for(long userID: userIDs)
            userWrappers.add(SwitchWrapper.phoneBook.get(userID));

        return userWrappers;
    }

}

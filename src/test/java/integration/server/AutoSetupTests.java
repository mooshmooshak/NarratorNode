package integration.server;

import java.sql.SQLException;
import java.util.List;

import game.abilities.Hidden;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import json.JSONException;
import models.SetupHidden;
import nnode.Lobby;
import nnode.LobbyManager;
import util.UserFactory;

public class AutoSetupTests extends InstanceTests {

    public AutoSetupTests(String name) {
        super(name);
    }

    public void testExposedRandom() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addBots(2);

        Setup setup = host.getSetup();
        Hidden tk = Hidden.TownKilling();
        host.addSetupHidden(tk);
        host.addSetupHidden(BasicRoles.Agent());
        host.addSetupHidden(BasicRoles.Agent());

        setup = host.getSetup();
        List<SetupHidden> rolesList = setup.rolesList._setupHiddenList;
        for(SetupHidden setupHidden: rolesList)
            host.setExposed(setupHidden.id);

        setup = host.getSetup();
        rolesList = setup.rolesList.getSpawningSetupHiddens(3);
        for(SetupHidden setupHidden: rolesList){
            assertNotSame(0, setupHidden.id);
            if(setupHidden.hidden.getName().equals(tk.getName()))
                assertTrue(setupHidden.isExposed);
        }

        host.startGame();

        for(SetupHidden setupHidden: rolesList){
            if(setupHidden.hidden.getName().equals(tk.getName()))
                assertTrue(setupHidden.isExposed);
        }

        SwitchWrapper.killDBConnections();

        reloadInstances();
        for(Lobby reloadedLobby: LobbyManager.idToLobby.values()){
            assertNotNull(reloadedLobby.game);
            for(SetupHidden setupHidden: reloadedLobby.game.setup.rolesList.iterable(game))
                assertTrue(setupHidden.isExposed);
        }
    }
}

package integration.server;

import integration.controller.TestActionController;
import integration.controller.TestChatController;
import integration.controller.TestFactionController;
import integration.controller.TestFactionRoleController;
import integration.controller.TestProfileController;
import integration.controller.TestRoleController;
import integration.controller.TestSetupController;
import integration.logic.SuperTest;
import junit.framework.Test;
import junit.framework.TestSuite;

public class NodeTests {

    public static Test suite() {

        SuperTest.BrainEndGame = false;
        TestSuite suite = new TestSuite(NodeTests.class.getName());
        // $JUnit-BEGIN$

        suite.addTestSuite(TestActionController.class);
        suite.addTestSuite(TestChatController.class);
        suite.addTestSuite(TestFactionController.class);
        suite.addTestSuite(TestFactionRoleController.class);
        suite.addTestSuite(TestRoleController.class);
        suite.addTestSuite(TestSetupController.class);
        suite.addTestSuite(TestProfileController.class);

        suite.addTestSuite(PlayerLobbyTests.class);
        suite.addTestSuite(PlayerLeaveTests.class);
        suite.addTestSuite(RuleManipulationTests.class);
        suite.addTestSuite(WarningTests.class);
        suite.addTestSuite(MiscellaneousInstanceTests.class);
        suite.addTestSuite(TestCases.class);
        suite.addTestSuite(AutoSetupTests.class);
        suite.addTestSuite(InstanceSetupTests.class);
        suite.addTestSuite(DatabaseTests.class);

        return suite;
    }

}

package integration.server;

import java.sql.SQLException;

import game.logic.Game;
import game.logic.templates.BasicRoles;
import json.JSONException;
import json.JSONObject;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import util.UserFactory;

public class RuleManipulationTests extends InstanceTests {

    public RuleManipulationTests(String name) {
        super(name);
    }

    public void testDayManipulation() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.updateSetupModifier(SetupModifierName.DAY_START, Game.DAY_START);

        Modifiers<SetupModifierName> modifiers = host.getSetup().modifiers;
        assertEquals(Game.DAY_START, modifiers.getBoolean(SetupModifierName.DAY_START, 0));

        host.updateSetupModifier(SetupModifierName.DAY_START, Game.NIGHT_START);
        assertEquals(Game.NIGHT_START, game.getBool(SetupModifierName.DAY_START));
    }

    public void testPlayerLimitBounds() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        JSONObject response;
        for(int i = 0; i < 16; i++){
            response = host.addSetupHidden(BasicRoles.Agent());
            assertEquals(0, response.getJSONArray("errors").length());
        }
        response = host.addSetupHidden(BasicRoles.Agent());
        assertEquals(1, response.getJSONArray("errors").length());
    }

    public void testPlayerModLimitBounds() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(UserFactory.PUBLIC, UserFactory.MOD);

        JSONObject response;
        for(int i = 0; i < 20; i++){
            response = host.addSetupHidden(BasicRoles.Agent());
            assertEquals(0, response.getJSONArray("errors").length());
        }
    }
}

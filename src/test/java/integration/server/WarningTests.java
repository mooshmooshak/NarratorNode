package integration.server;

import java.sql.SQLException;

import game.abilities.FactionKill;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import json.JSONException;
import util.UserFactory;

public class WarningTests extends InstanceTests {

    public WarningTests(String name) {
        super(name);
    }

    public void testMafWarning() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper p1 = host.addUser();
        UserWrapper p2 = host.addUser();

        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.SerialKiller());

        p1.prefer(BasicRoles.Goon());
        p2.prefer(BasicRoles.Goon());

        host.startGame(Game.NIGHT_START);

        isRole(p1, BasicRoles.Goon());
        isRole(p2, BasicRoles.Goon());

        Player hostPlayer = host.getPlayer();
        p1.setNightTarget(new Action(p1.getPlayer(), FactionKill.abilityType, hostPlayer));
        p2.setNightTarget(new Action(p2.getPlayer(), FactionKill.abilityType, hostPlayer));
    }
}

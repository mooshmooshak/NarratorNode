package integration.server;

import java.sql.SQLException;
import java.util.Iterator;

import game.abilities.Hidden;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.json_output.FactionRoleJson;
import services.SetupService;
import util.TestUtil;
import util.UserFactory;
import util.game.LookupUtil;

public class InstanceSetupTests extends InstanceTests {

    public InstanceSetupTests(String name) {
        super(name);
    }

    String newTeamColor = Constants.ORANGE;

    public void testCreatedFactionRoles() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", newTeamColor);
        host.createFactionRole("Citizen", newTeamColor);
        host.createFactionRole("Sheriff", newTeamColor);

        Setup setup = SetupService.getSetup(BasicRoles.setup.id);
        assertEquals(2, setup.getFactionByColor(newTeamColor).roleMap.size());
    }

    // making roles available, adding the random role, and then making the roles
    // unavailable should delete those random roles
    public void testBlacklistingToOblivion() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", newTeamColor);
        Setup setup = SetupService.getSetup(host.getSetupID());
        assertPresent(LookupUtil.findFactionByColor(setup, newTeamColor));

        long frID1 = host.createFactionRole("Citizen", newTeamColor).getJSONObject("response").getLong("id");
        JSONObject response = host.createFactionRole("Sheriff", newTeamColor).getJSONObject("response");
        long frID2 = FactionRoleJson.getID(response);

        long hiddenID = host.createHidden("citsher").getJSONObject("response").getLong("id");
        host.addHiddenSpawn(hiddenID, frID1, frID2);

        Hidden hidden = LookupUtil.findHidden(host.getSetup(), hiddenID);
        assertNotNull(hidden);

        host.addSetupHidden(hidden);
        host.addSetupHidden(hidden);

        Iterator<FactionRole> iterator = hidden.getAllFactionRoles().iterator();
        host.deleteFactionRole(iterator.next());
        host.deleteFactionRole(iterator.next());

        hidden = LookupUtil.findHidden(host.getSetup(), hiddenID);
        assertEquals(0, hidden.size());
    }

    public void testBlacklistingRandomInfoRemoval() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        TestUtil.assertNoErrors(host.createTeam("Starky", newTeamColor));
        host.createFactionRole("Citizen", newTeamColor);
        host.createFactionRole("Sheriff", newTeamColor);
        JSONObject response = host.createFactionRole("Doctor", newTeamColor);
        long factionRoleID = response.getJSONObject("response").getLong("id");

        // throws error if does not exist. it's a wanted assertion here
        FactionRole fr = host.getSetup().getFactionRole(factionRoleID);

        host.deleteFactionRole(fr);
        assertEquals(2, host.getSetup().getFactionByColor(newTeamColor).roleMap.size());
    }

    public void testBlackListSpaceNames() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();

        host.createTeam("Starky", newTeamColor);
        host.createFactionRole("Drug Dealer", newTeamColor);
        host.createFactionRole("Mass Murderer", newTeamColor);
        JSONObject response = host.createFactionRole("Serial Killer", newTeamColor);
        long factionRoleID = response.getJSONObject("response").getLong("id");

        FactionRole fr = LookupUtil.findFactionRole(host.getSetup(), factionRoleID);

        host.deleteFactionRole(fr);
        assertEquals(2, host.getSetup().getFactionByColor(newTeamColor).roleMap.size());
    }
}

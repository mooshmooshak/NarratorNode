package integration.unit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game.logic.support.condorcet.Condorcet;
import junit.framework.TestCase;

public class TestCondorcet extends TestCase {

    public static String A = "A";
    public static String B = "B";
    public static String C = "C";
    public static String D = "D";
    public static String E = "E";
    private static String[] names = {
            A, B, C, E, D
    };

    public void testCase1() {
        List<List<Set<String>>> input = new ArrayList<>();
        for(String voter: names)
            if(!voter.equals(A) && !voter.contentEquals(E))
                input.add(getAnyoneButVoter(voter));
        List<Set<String>> ret = new ArrayList<>();
        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();
        Set<String> set3 = new HashSet<>();
        Set<String> set4 = new HashSet<>();
        Set<String> set5 = new HashSet<>();
        ret.add(set1);
        ret.add(set2);
        ret.add(set3);
        ret.add(set4);
        ret.add(set5);
        set1.add(B);
        set3.add(C);
        set4.add(D);
        set4.add(E);
        set5.add(A);
        input.add(ret);

        List<Set<String>> output = Condorcet.getOrderings(input);
        Set<String> lastSet = output.get(output.size() - 1);

        assertEquals(2, lastSet.size());
        assertEquals(A, lastSet.iterator().next());
    }

    private static List<Set<String>> getAnyoneButVoter(String voter) {
        List<Set<String>> ret = new ArrayList<>();
        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();
        for(String name: names){
            if(name.equals(voter))
                continue;
            set1.add(name);
        }
        ret.add(set1);
        set2.add(voter);
        ret.add(set2);
        return ret;
    }
}

package integration;

import integration.server.InstanceTests;
import integration.server.SwitchWrapper;
import junit.framework.TestCase;
import repositories.BaseRepo;
import repositories.Connection;

public abstract class ControllerTestCase extends TestCase {

    @Override
    protected void setUp() throws Exception {
        BaseRepo.connection = new Connection();
        InstanceTests.wipeTables();
        super.setUp();
    }

    @Override
    public void runTest() throws Throwable {
        SwitchWrapper.init();
        super.runTest();
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        BaseRepo.connection.close();
    }
}

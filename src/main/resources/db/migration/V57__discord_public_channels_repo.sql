DROP TABLE discord_edited_channels;

CREATE TABLE discord_public_channels(
    game_id BIGINT UNSIGNED NOT NULL,
    channel_id VARCHAR(36) NOT NULL,
    UNIQUE KEY `discord_public_channels` (channel_id)
);

CREATE TABLE discord_subscription_messages(
    game_id BIGINT UNSIGNED NOT NULL,
    channel_id VARCHAR(36) NOT NULL,
    message_id VARCHAR(36),
    UNIQUE KEY `discord_cleanup_messages` (message_id)
)

ALTER TABLE commands ADD COLUMN time_left DOUBLE AFTER command;
UPDATE commands SET time_left = 0;
ALTER TABLE commands CHANGE COLUMN time_left time_left DOUBLE NOT NULL;

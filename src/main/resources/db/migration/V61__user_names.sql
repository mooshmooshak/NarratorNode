ALTER TABLE users DROP COLUMN is_guest;
ALTER TABLE users ADD COLUMN `name` VARCHAR(40);

UPDATE users u
INNER JOIN players p ON p.user_id = u.id
SET u.name = p.name WHERE p.name IS NOT NULL;

UPDATE users SET name = '' WHERE name IS NULL;

ALTER TABLE users CHANGE COLUMN name name VARCHAR(40) NOT NULL;

CREATE TABLE lobby_chat(
   id SERIAL PRIMARY KEY,
   game_id BIGINT UNSIGNED NOT NULL,
   user_id BIGINT UNSIGNED NOT NULL,
   text VARCHAR(5000) NOT NULL,
   created_at TIMESTAMP NOT NULL,
   CONSTRAINT `lobby_chat_user_ref` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
   CONSTRAINT `lobby_chat_game_ref` FOREIGN KEY (`game_id`) REFERENCES `replays` (`id`) ON DELETE CASCADE
);

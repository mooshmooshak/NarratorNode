ALTER TABLE setups ADD COLUMN is_editable BOOLEAN DEFAULT true AFTER description;

UPDATE setups SET is_editable = false WHERE id IN (SELECT replays.setup_id FROM replays);

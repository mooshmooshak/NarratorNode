DROP TABLE role_stats;
DROP TABLE player_input;
DELETE FROM ability_makeup_saved_games
WHERE game_id NOT IN (SELECT a.game_id
                        FROM saved_games a);
DELETE FROM roles_list_saved_games
WHERE game_id NOT IN (SELECT a.game_id
                        FROM saved_games a);
DELETE FROM roles_list_composition_saved_games
WHERE game_id NOT IN (SELECT a.game_id
                        FROM saved_games a);


CREATE TABLE setups(
	id SERIAL PRIMARY KEY,
	owner_id BIGINT UNSIGNED NOT NULL
);

ALTER TABLE users DROP COLUMN android_id;
ALTER TABLE users ADD CONSTRAINT token UNIQUE(token);

INSERT IGNORE INTO users (token, points, winrate, accountType)
SELECT DISTINCT hostToken, 0, 0, 'guest' FROM saved_games;

ALTER TABLE users ADD COLUMN id SERIAL FIRST;
ALTER TABLE users DROP PRIMARY KEY, ADD PRIMARY KEY(ID);

INSERT INTO setups (owner_id, id)
SELECT users.id, saved_games.game_id FROM saved_games
LEFT JOIN users
ON users.token=saved_games.hostToken;


-- MORPH saved_games INTO replays
RENAME TABLE saved_games TO replays;
ALTER TABLE replays ADD COLUMN created_at TIMESTAMP;
UPDATE replays SET created_at=`date`;
-- ALTER TABLE replays DROP COLUMN `date`;

ALTER TABLE replays DROP COLUMN isFinished;
ALTER TABLE replays DROP COLUMN hostName;
ALTER TABLE replays CHANGE COLUMN game_id game_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE replays DROP PRIMARY KEY;
ALTER TABLE replays CHANGE COLUMN game_id id SERIAL PRIMARY KEY FIRST;
ALTER TABLE replays ADD COLUMN setup_id BIGINT UNSIGNED AFTER id;
UPDATE replays a SET a.setup_id=a.id;
ALTER TABLE replays CHANGE COLUMN created_at created_at TIMESTAMP NOT NULL AFTER game_seed;
ALTER TABLE replays CHANGE setup_id setup_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE replays CHANGE COLUMN game_seed seed BIGINT NOT NULL AFTER setup_id;

ALTER TABLE replays ADD COLUMN host_id BIGINT UNSIGNED AFTER created_at;
UPDATE replays a INNER JOIN users b ON a.hostToken=b.token
SET host_id=b.id;
ALTER TABLE replays CHANGE COLUMN host_id host_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE replays DROP COLUMN hostToken;
ALTER TABLE replays CHANGE COLUMN isPrivate is_private BOOLEAN DEFAULT true NOT NULL;

CREATE TABLE setup_int_modifiers(
	setup_id BIGINT UNSIGNED NOT NULL,
	modifier_name VARCHAR(40) NOT NULL,
	value MEDIUMINT NOT NULL
);

CREATE TABLE setup_bool_modifiers(
	setup_id BIGINT UNSIGNED NOT NULL,
	modifier_name VARCHAR(40) NOT NULL,
	value bool NOT NULL
);

ALTER TABLE replays ADD COLUMN IF NOT EXISTS jester_kills INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'jester_kills', jester_kills FROM replays;
ALTER TABLE replays DROP COLUMN jester_kills;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS cit_ratio INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'cit_ratio', cit_ratio FROM replays;
ALTER TABLE replays DROP COLUMN cit_ratio;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS customizability INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'customizability', customizability FROM replays;
ALTER TABLE replays DROP COLUMN customizability;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS mayor_votes INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'mayor_vote_power', mayor_votes FROM replays;
ALTER TABLE replays DROP COLUMN mayor_votes;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS Day_Length INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'day_length', Day_Length FROM replays;
ALTER TABLE replays DROP COLUMN Day_Length;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS arson_day_ignite INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'arson_day_ignites', arson_day_ignite FROM replays;
ALTER TABLE replays DROP COLUMN arson_day_ignite;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS charge_sdv INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'charge_variability', charge_sdv FROM replays;
ALTER TABLE replays DROP COLUMN charge_sdv;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS cult_pr_cooldown INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'cult_power_role_cd', cult_pr_cooldown FROM replays;
ALTER TABLE replays DROP COLUMN cult_pr_cooldown;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS Night_Length INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'night_length', Night_Length FROM replays;
ALTER TABLE replays DROP COLUMN Night_Length;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS Player_Limit INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'player_limit', Player_Limit FROM replays;
ALTER TABLE replays DROP COLUMN Player_Limit;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS joker_inc INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'bounty_incubation', joker_inc FROM replays;
ALTER TABLE replays DROP COLUMN joker_inc;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS joker_kil INTEGER;
INSERT INTO setup_int_modifiers (setup_id, modifier_name, value)
SELECT id, 'bounty_fail_reward', joker_kil FROM replays;
ALTER TABLE replays DROP COLUMN joker_kil;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS amne_must_remember BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'amnesiac_must_remember', amne_must_remember FROM replays;
ALTER TABLE replays DROP COLUMN amne_must_remember;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS fkill_diff BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'differentiated_faction_kills', fkill_diff FROM replays;
ALTER TABLE replays DROP COLUMN fkill_diff;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS gd_reanimate BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'gd_reanimate', gd_reanimate FROM replays;
ALTER TABLE replays DROP COLUMN gd_reanimate;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS bg_douse_redir BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'guard_redirects_douse', bg_douse_redir FROM replays;
ALTER TABLE replays DROP COLUMN bg_douse_redir;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS bg_cult_redir BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'guard_redirects_convert', bg_cult_redir FROM replays;
ALTER TABLE replays DROP COLUMN bg_cult_redir;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS bg_pois_redir BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'guard_redirects_poison', bg_pois_redir FROM replays;
ALTER TABLE replays DROP COLUMN bg_pois_redir;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS coro_imprint BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'coroner_learns_roles', coro_imprint FROM replays;
ALTER TABLE replays DROP COLUMN coro_imprint;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS det_pierce_imm BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'follow_pierces_immunity', det_pierce_imm FROM replays;
ALTER TABLE replays DROP COLUMN det_pierce_imm;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS dead_knows BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'omniscient_dead', dead_knows FROM replays;
ALTER TABLE replays DROP COLUMN dead_knows;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS tailor_notif BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'tailor_feedback', tailor_notif FROM replays;
ALTER TABLE replays DROP COLUMN tailor_notif;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS cult_power_up BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'cult_promotion', cult_power_up FROM replays;
ALTER TABLE replays DROP COLUMN cult_power_up;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS punch_allowed BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'punch_allowed', punch_allowed FROM replays;
ALTER TABLE replays DROP COLUMN punch_allowed;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS miller_suited BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'miller_suited', miller_suited FROM replays;
ALTER TABLE replays DROP COLUMN miller_suited;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS jan_get_roles BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'janitor_gets_roles', jan_get_roles FROM replays;
ALTER TABLE replays DROP COLUMN jan_get_roles;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS jester_annoy BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'jester_can_annoy', jester_annoy FROM replays;
ALTER TABLE replays DROP COLUMN jester_annoy;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS marsh_q_reveal BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'marshall_quick_reveal', marsh_q_reveal FROM replays;
ALTER TABLE replays DROP COLUMN marsh_q_reveal;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS sher_pre_peek BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'sheriff_prepeek', sher_pre_peek FROM replays;
ALTER TABLE replays DROP COLUMN sher_pre_peek;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS arch_burn BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'arch_burn', arch_burn FROM replays;
ALTER TABLE replays DROP COLUMN arch_burn;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS self_vote BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'self_vote', self_vote FROM replays;
ALTER TABLE replays DROP COLUMN self_vote;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS sntchPiersSuit BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'snitch_pierces_suit', sntchPiersSuit FROM replays;
ALTER TABLE replays DROP COLUMN sntchPiersSuit;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS exec_town BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'exec_town', exec_town FROM replays;
ALTER TABLE replays DROP COLUMN exec_town;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS doc_block_pois BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'heal_blocks_poison', doc_block_pois FROM replays;
ALTER TABLE replays DROP COLUMN doc_block_pois;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS chat_roles BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'chat_roles', chat_roles FROM replays;
ALTER TABLE replays DROP COLUMN chat_roles;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS arch_quarintine BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'arch_quarantine', arch_quarintine FROM replays;
ALTER TABLE replays DROP COLUMN arch_quarintine;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS amnesiac_keeps_charge BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'amnesiac_keeps_charges', amnesiac_keeps_charge FROM replays;
ALTER TABLE replays DROP COLUMN amnesiac_keeps_charge;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS enforcer_learns_names BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'enforcer_learns_names', enforcer_learns_names FROM replays;
ALTER TABLE replays DROP COLUMN enforcer_learns_names;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS spy_targets_enemies BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'spy_targets_enemies', spy_targets_enemies FROM replays;
ALTER TABLE replays DROP COLUMN spy_targets_enemies;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS host_voting BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'host_voting', host_voting FROM replays;
ALTER TABLE replays DROP COLUMN host_voting;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS arsonist_informs BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'douse_feedback', arsonist_informs FROM replays;
ALTER TABLE replays DROP COLUMN arsonist_informs;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS stripper_fb BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'block_feedback', stripper_fb FROM replays;
ALTER TABLE replays DROP COLUMN stripper_fb;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS doc_target_notif BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'heal_feedback', doc_target_notif FROM replays;
ALTER TABLE replays DROP COLUMN doc_target_notif;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS cult_refusable BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'convert_refusable', cult_refusable FROM replays;
ALTER TABLE replays DROP COLUMN cult_refusable;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS ghost_back_to_back BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'ghost_back_to_back', ghost_back_to_back FROM replays;
ALTER TABLE replays DROP COLUMN ghost_back_to_back;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS vent_back_to_back BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'vent_back_to_back', vent_back_to_back FROM replays;
ALTER TABLE replays DROP COLUMN vent_back_to_back;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS mason_noncit_recruit BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'mason_non_cit_recruit', mason_noncit_recruit FROM replays;
ALTER TABLE replays DROP COLUMN mason_noncit_recruit;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS mason_promotion BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'mason_promotion', mason_promotion FROM replays;
ALTER TABLE replays DROP COLUMN mason_promotion;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS witch_feedback BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'witch_feedback', witch_feedback FROM replays;
ALTER TABLE replays DROP COLUMN witch_feedback;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS bomb_pierce BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'bomb_pierce', bomb_pierce FROM replays;
ALTER TABLE replays DROP COLUMN bomb_pierce;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS exec_to_jest BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'executioner_to_jester', exec_to_jest FROM replays;
ALTER TABLE replays DROP COLUMN exec_to_jest;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS Day_Start BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'day_start', Day_Start FROM replays;
ALTER TABLE replays DROP COLUMN Day_Start;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS sheriff_tells_diff BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'check_differentiation', sheriff_tells_diff FROM replays;
ALTER TABLE replays DROP COLUMN sheriff_tells_diff;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS bread_passing BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'bread_passing', bread_passing FROM replays;
ALTER TABLE replays DROP COLUMN bread_passing;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS self_bread_usage BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'self_bread_usage', self_bread_usage FROM replays;
ALTER TABLE replays DROP COLUMN self_bread_usage;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS last_wills BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'last_will', last_wills FROM replays;
ALTER TABLE replays DROP COLUMN last_wills;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS proxy_upgrade BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'proxy_upgrade', proxy_upgrade FROM replays;
ALTER TABLE replays DROP COLUMN proxy_upgrade;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS exec_win_immune BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'executioner_win_immune', exec_win_immune FROM replays;
ALTER TABLE replays DROP COLUMN exec_win_immune;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS coroner_exhumes BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'coroner_exhumes', coroner_exhumes FROM replays;
ALTER TABLE replays DROP COLUMN coroner_exhumes;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS gs_day_guns BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'gs_day_guns', gs_day_guns FROM replays;
ALTER TABLE replays DROP COLUMN gs_day_guns;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS doc_notif BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'heal_success_feedback', doc_notif FROM replays;
ALTER TABLE replays DROP COLUMN doc_notif;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS cult_keeps_roles BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'cult_keeps_roles', cult_keeps_roles FROM replays;
ALTER TABLE replays DROP COLUMN cult_keeps_roles;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS spy_targets_allies BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'spy_targets_allies', spy_targets_allies FROM replays;
ALTER TABLE replays DROP COLUMN spy_targets_allies;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS detct_all_targs BOOLEAN;
INSERT INTO setup_bool_modifiers (setup_id, modifier_name, value)
SELECT id, 'follow_gets_all', detct_all_targs FROM replays;
ALTER TABLE replays DROP COLUMN detct_all_targs;

ALTER TABLE replays ADD COLUMN IF NOT EXISTS detct_all_targs BOOLEAN;
ALTER TABLE replays DROP COLUMN detct_all_targs;
ALTER TABLE replays ADD COLUMN IF NOT EXISTS tailor_self_target BOOLEAN;
ALTER TABLE replays DROP COLUMN tailor_self_target;
ALTER TABLE replays ADD COLUMN IF NOT EXISTS arch_self_target BOOLEAN;
ALTER TABLE replays DROP COLUMN arch_self_target;
ALTER TABLE replays ADD COLUMN IF NOT EXISTS cult_conversion_cooldown BOOLEAN;
ALTER TABLE replays DROP COLUMN cult_conversion_cooldown;
ALTER TABLE replays ADD COLUMN IF NOT EXISTS mm_cd BOOLEAN;
ALTER TABLE replays DROP COLUMN mm_cd;
ALTER TABLE replays ADD COLUMN IF NOT EXISTS marshall_execs BOOLEAN;
ALTER TABLE replays DROP COLUMN marshall_execs;

ALTER TABLE setup_int_modifiers CHANGE COLUMN modifier_name name VARCHAR(40) NOT NULL;
ALTER TABLE setup_bool_modifiers CHANGE COLUMN modifier_name name VARCHAR(40) NOT NULL;


-- MORPH comands_saved_games INTO commands
RENAME TABLE commands_saved_games TO commands;
ALTER TABLE commands CHANGE COLUMN counter counter BIGINT UNSIGNED NOT NULL FIRST;
ALTER TABLE commands CHANGE COLUMN game_id replay_id BIGINT UNSIGNED NOT NULL AFTER command;
ALTER TABLE commands DROP COLUMN command_id;


-- MORPH players_saved_games INTO players
INSERT IGNORE INTO users (token, points, winrate, accountType)
SELECT DISTINCT token, 0, 0, 'guest' FROM player_saved_games WHERE token IS NOT NULL;

RENAME TABLE player_saved_games TO players;
ALTER TABLE players DROP COLUMN color;
ALTER TABLE players DROP COLUMN role;
ALTER TABLE players DROP COLUMN baseName;
ALTER TABLE players DROP COLUMN player_id;
ALTER TABLE players ADD COLUMN id SERIAL PRIMARY KEY FIRST;
ALTER TABLE players CHANGE COLUMN deathday death_day SMALLINT;
ALTER TABLE players CHANGE COLUMN winner is_winner BOOLEAN;
ALTER TABLE players CHANGE COLUMN isExited is_exited BOOLEAN;
ALTER TABLE players CHANGE COLUMN game_id replay_id BIGINT UNSIGNED NOT NULL;

ALTER TABLE players ADD COLUMN user_id BIGINT UNSIGNED AFTER replay_id;
UPDATE players a
INNER JOIN users b ON a.token = b.token
SET user_id = b.id WHERE a.token IS NOT NULL;
ALTER TABLE players DROP COLUMN token;


-- MORPH faction_saved_games INTO factions
RENAME TABLE faction_saved_games TO factions;
ALTER TABLE factions DROP COLUMN team_id;
ALTER TABLE factions ADD COLUMN IF NOT EXISTS killcd BOOLEAN;
ALTER TABLE factions DROP COLUMN killcd;
ALTER TABLE factions ADD COLUMN id SERIAL PRIMARY KEY FIRST;
ALTER TABLE factions CHANGE COLUMN game_id setup_id BIGINT UNSIGNED NOT NULL AFTER id;
ALTER TABLE factions CHANGE COLUMN descrip description VARCHAR(500) NOT NULL AFTER name;


-- CREATE FACTION MODIFIERS
CREATE TABLE faction_int_modifiers(
	faction_id BIGINT UNSIGNED NOT NULL,
	modifier_id VARCHAR(20) NOT NULL,
	value MEDIUMINT NOT NULL
);

CREATE TABLE faction_bool_modifiers(
	faction_id BIGINT UNSIGNED NOT NULL,
	modifier_id VARCHAR(20) NOT NULL,
	value bool NOT NULL
);

ALTER TABLE factions ADD COLUMN IF NOT EXISTS priority BOOLEAN;
INSERT INTO faction_int_modifiers (faction_id, modifier_id, value)
SELECT id, 'win_priority', priority FROM factions;
ALTER TABLE factions DROP COLUMN priority;

ALTER TABLE factions ADD COLUMN IF NOT EXISTS punchSucc BOOLEAN;
INSERT INTO faction_int_modifiers (faction_id, modifier_id, value)
SELECT id, 'punch_success', punchSucc FROM factions;
ALTER TABLE factions DROP COLUMN punchSucc;

ALTER TABLE factions ADD COLUMN IF NOT EXISTS identity BOOLEAN;
INSERT INTO faction_bool_modifiers (faction_id, modifier_id, value)
SELECT id, 'knows_allies', identity FROM factions;
ALTER TABLE factions DROP COLUMN identity;

ALTER TABLE factions ADD COLUMN IF NOT EXISTS liveToWin BOOLEAN;
INSERT INTO faction_bool_modifiers (faction_id, modifier_id, value)
SELECT id, 'live_to_win', liveToWin FROM factions;
ALTER TABLE factions DROP COLUMN liveToWin;

ALTER TABLE factions ADD COLUMN IF NOT EXISTS lastWill BOOLEAN;
INSERT INTO faction_bool_modifiers (faction_id, modifier_id, value)
SELECT id, 'last_will', lastWill FROM factions;
ALTER TABLE factions DROP COLUMN lastWill;

ALTER TABLE factions ADD COLUMN IF NOT EXISTS nightChat BOOLEAN;
INSERT INTO faction_bool_modifiers (faction_id, modifier_id, value)
SELECT id, 'has_night_chat', nightChat FROM factions;
ALTER TABLE factions DROP COLUMN nightChat;

ALTER TABLE factions ADD COLUMN IF NOT EXISTS recruitable BOOLEAN;
INSERT INTO faction_bool_modifiers (faction_id, modifier_id, value)
SELECT id, 'is_recruitable', recruitable FROM factions;
ALTER TABLE factions DROP COLUMN recruitable;

ALTER TABLE faction_bool_modifiers CHANGE COLUMN modifier_id name VARCHAR(20);
ALTER TABLE faction_int_modifiers CHANGE COLUMN modifier_id name VARCHAR(20);

ALTER TABLE factions DROP COLUMN IF EXISTS godfather;
ALTER TABLE factions DROP COLUMN IF EXISTS untraceable;
ALTER TABLE factions DROP COLUMN IF EXISTS ironwill;


-- MORPH team_abilities_saved_games INTO faction_abilities
RENAME TABLE team_abilities_saved_games TO faction_abilities;
ALTER TABLE faction_abilities CHANGE COLUMN abilityName ability_name VARCHAR(20) NOT NULL;
ALTER TABLE faction_abilities ADD COLUMN faction_id BIGINT UNSIGNED FIRST;
UPDATE faction_abilities a
INNER JOIN factions b
ON b.color = a.color
AND b.setup_id = a.game_id
SET faction_id = b.id;
ALTER TABLE faction_abilities CHANGE COLUMN faction_id faction_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE faction_abilities DROP COLUMN color;
ALTER TABLE faction_abilities DROP COLUMN game_id;
ALTER TABLE faction_abilities ADD COLUMN id SERIAL PRIMARY KEY FIRST;

RENAME TABLE team_modifiers_saved_games TO faction_ability_int_modifiers;
ALTER TABLE faction_ability_int_modifiers CHANGE COLUMN modifierName modifier_name VARCHAR(40) NOT NULL;
ALTER TABLE faction_ability_int_modifiers ADD COLUMN faction_ability_id BIGINT UNSIGNED NOT NULL AFTER color;
ALTER TABLE faction_ability_int_modifiers ADD COLUMN faction_id BIGINT UNSIGNED AFTER color;
UPDATE faction_ability_int_modifiers a
INNER JOIN factions b
ON b.color = a.color
AND b.setup_id = a.game_id
SET faction_id = b.id;

UPDATE faction_ability_int_modifiers a
INNER JOIN faction_abilities b
ON a.faction_id=b.faction_id
AND RIGHT(a.modifier_name, 11) = b.ability_name
SET a.faction_ability_id = b.id;
ALTER TABLE faction_abilities CHANGE COLUMN ability_name name VARCHAR(20) NOT NULL;

UPDATE faction_ability_int_modifiers
SET modifier_name = LEFT(modifier_name,length(modifier_name)-11);
ALTER TABLE faction_ability_int_modifiers CHANGE COLUMN modifier_name modifier_id VARCHAR(20) NOT NULL;

ALTER TABLE faction_ability_int_modifiers DROP COLUMN color;
ALTER TABLE faction_ability_int_modifiers DROP COLUMN game_id;
ALTER TABLE faction_ability_int_modifiers DROP COLUMN faction_id;

CREATE TABLE IF NOT EXISTS faction_ability_bool_modifiers
SELECT * FROM faction_ability_int_modifiers;

DELETE FROM faction_ability_int_modifiers WHERE modifierIValue IS NULL;
DELETE FROM faction_ability_bool_modifiers WHERE modifierBValue IS NULL;
ALTER TABLE faction_ability_int_modifiers CHANGE COLUMN modifierIValue value MEDIUMINT NOT NULL;
ALTER TABLE faction_ability_bool_modifiers CHANGE COLUMN modifierBValue value BOOLEAN NOT NULL;
ALTER TABLE faction_ability_int_modifiers DROP COLUMN modifierBValue;
ALTER TABLE faction_ability_bool_modifiers DROP COLUMN modifierIValue;
ALTER TABLE faction_ability_bool_modifiers CHANGE COLUMN modifier_id name VARCHAR(20) NOT NULL;
ALTER TABLE faction_ability_int_modifiers CHANGE COLUMN modifier_id name VARCHAR(20) NOT NULL;

UPDATE faction_ability_int_modifiers SET name = 'zero_weighted' WHERE name = 'ability_weight';

CREATE TABLE hiddens(
	id SERIAL PRIMARY KEY,
	setup_id BIGINT UNSIGNED NOT NULL,
	name VARCHAR(40) NOT NULL,
	color CHAR(7) NOT NULL,
	is_exposed BOOLEAN NOT NULL,
    is_single BOOLEAN NOT NULL,
	rt_id BIGINT UNSIGNED,
    internal_name VARCHAR(40)
);


CREATE TABLE hidden_roles(
    hidden_id BIGINT UNSIGNED NOT NULL,
    role_id BIGINT UNSIGNED NOT NULL
);
ALTER TABLE hidden_roles ADD CONSTRAINT hidden_id_role_id UNIQUE(hidden_id, role_id);


CREATE TABLE role_ability_modifiers (
  role_ability_id BIGINT UNSIGNED NOT NULL,
  modifier_id VARCHAR(20) NOT NULL,
  int_value MEDIUMINT DEFAULT NULL,
  bool_value BOOLEAN DEFAULT NULL
);

-- MORPH ability_makeup_saved_games INTO role_abilities
RENAME TABLE ability_makeup_saved_games TO role_abilities;
ALTER TABLE role_abilities CHANGE COLUMN game_id setup_id  BIGINT UNSIGNED NOT NULL;
ALTER TABLE role_abilities CHANGE COLUMN abilityName name VARCHAR(20) NOT NULL;
ALTER TABLE role_abilities ADD COLUMN roles_list_id BIGINT UNSIGNED AFTER setup_id;
ALTER TABLE role_abilities ADD COLUMN role_id BIGINT UNSIGNED AFTER setup_id;
ALTER TABLE role_abilities ADD COLUMN id SERIAL PRIMARY KEY FIRST;

UPDATE role_abilities
SET roles_list_id = rt_id
WHERE randomMemberComposition = false;
UPDATE role_abilities
SET role_id = rt_id
WHERE randomMemberComposition = true;
ALTER TABLE role_abilities DROP COLUMN randomMemberComposition;
ALTER TABLE role_abilities DROP COLUMN rt_id;

RENAME TABLE role_modifiers_saved_games TO role_modifiers;
ALTER TABLE role_modifiers CHANGE COLUMN game_id setup_id  BIGINT UNSIGNED NOT NULL;
ALTER TABLE role_modifiers ADD COLUMN roles_list_id BIGINT UNSIGNED AFTER setup_id;
ALTER TABLE role_modifiers ADD COLUMN role_id BIGINT UNSIGNED AFTER setup_id;

UPDATE role_modifiers
SET roles_list_id = rt_id
WHERE randomMemberComposition = false;
UPDATE role_modifiers
SET role_id = rt_id
WHERE randomMemberComposition = true;
ALTER TABLE role_modifiers DROP COLUMN randomMemberComposition;
ALTER TABLE role_modifiers DROP COLUMN rt_id;


-- MORPH roles_list_composition_saved_games INTO roles
RENAME TABLE roles_list_composition_saved_games TO roles;

ALTER TABLE roles CHANGE COLUMN rr_id id BIGINT UNSIGNED NOT NULL;
ALTER TABLE roles DROP PRIMARY KEY;
ALTER TABLE roles CHANGE COLUMN id id SERIAL PRIMARY KEY FIRST;
ALTER TABLE roles CHANGE COLUMN internalName internal_name VARCHAR(40);


-- MORPH roles_list_saved_games INTO roles_list
RENAME TABLE roles_list_saved_games TO roles_list;
ALTER TABLE roles_list CHANGE COLUMN rt_internal_id id BIGINT UNSIGNED NOT NULL;
ALTER TABLE roles_list DROP PRIMARY KEY;
ALTER TABLE roles_list CHANGE COLUMN id id SERIAL PRIMARY KEY;
ALTER TABLE roles_list CHANGE COLUMN game_id setup_id BIGINT UNSIGNED NOT NULL AFTER id;
ALTER TABLE roles_list CHANGE COLUMN roleName name VARCHAR(40) AFTER id;
ALTER TABLE roles_list CHANGE COLUMN exposed is_exposed BOOLEAN NOT NULL AFTER internalName;
ALTER TABLE roles_list ADD COLUMN hidden_id BIGINT UNSIGNED AFTER id;

ALTER TABLE roles_list ADD COLUMN spawn_color CHAR(7) AFTER spawn;
ALTER TABLE roles_list ADD COLUMN spawn_role CHAR(40) AFTER rt_id;
UPDATE roles_list
SET spawn_color=CONCAT("#", SUBSTRING_INDEX(spawn, '#', -1))
WHERE spawn IS NOT NULL;
UPDATE roles_list
SET spawn_role=SUBSTRING_INDEX(spawn, '#', 1)
WHERE spawn IS NOT NULL;
ALTER TABLE roles_list DROP COLUMN spawn;



-- inserts true hiddens from roles list
INSERT INTO hiddens (setup_id, name, color, is_exposed, is_single, rt_id, internal_name)
SELECT DISTINCT setup_id, name, color, is_exposed, false, rt_id, internalName FROM roles_list
WHERE rt_id IS NOT NULL;

-- creates single container random templates
INSERT INTO hiddens (setup_id, name, color, is_exposed, is_single, rt_id, internal_name)
SELECT DISTINCT setup_id, name, color, true, true, rt_id, internalName FROM roles_list
WHERE rt_id IS NULL;
ALTER TABLE roles_list DROP COLUMN internalName;

-- connects hiddens to roles list
UPDATE roles_list a
LEFT JOIN hiddens b
ON a.setup_id=b.setup_id
AND a.name=b.name
AND a.color=b.color
SET hidden_id=b.id;
ALTER TABLE roles_list DROP COLUMN rt_id;

ALTER TABLE role_abilities ADD COLUMN hidden_id BIGINT UNSIGNED AFTER roles_list_id;
UPDATE role_abilities a
LEFT JOIN roles_list b
ON b.id = a.roles_list_id
SET a.hidden_id = b.hidden_id
WHERE roles_list_id IS NOT NULL;
ALTER TABLE role_abilities DROP COLUMN roles_list_id;

ALTER TABLE role_modifiers ADD COLUMN hidden_id BIGINT UNSIGNED AFTER roles_list_id;
UPDATE role_modifiers a
LEFT JOIN roles_list b
ON b.id = a.roles_list_id
SET a.hidden_id = b.hidden_id
WHERE roles_list_id IS NOT NULL;
ALTER TABLE role_modifiers DROP COLUMN roles_list_id;

-- connects roles that weren't in the direct roles list to 'hidden_roles'
INSERT INTO hidden_roles (hidden_id, role_id)
SELECT DISTINCT hiddens.id, roles.id
FROM roles
INNER JOIN hiddens
ON roles.rt_id=hiddens.rt_id
AND roles.game_id=hiddens.setup_id;

ALTER TABLE roles CHANGE COLUMN roleName name VARCHAR(40) NOT NULL;
ALTER TABLE roles CHANGE COLUMN game_id setup_id BIGINT UNSIGNED NOT NULL after id;
ALTER TABLE roles ADD COLUMN is_submember BOOLEAN DEFAULT true;

INSERT INTO roles (setup_id, name, color, internal_name, is_submember, rt_id)
SELECT DISTINCT hiddens.setup_id, hiddens.name, hiddens.color, hiddens.internal_name, false, -1
FROM hiddens
WHERE is_single = true;

INSERT INTO hidden_roles (role_id, hidden_id)
SELECT DISTINCT roles.id, hiddens.id
FROM roles
LEFT JOIN hiddens
ON hiddens.setup_id = roles.setup_id
AND hiddens.name = roles.name
AND hiddens.color = roles.color
WHERE roles.is_submember = false
AND roles.rt_id = -1;

INSERT INTO role_abilities (setup_id, role_id, hidden_id, name)
SELECT ra.setup_id, hr.role_id, NULL, ra.name
FROM hidden_roles hr
LEFT JOIN role_abilities ra
ON hr.hidden_id = ra.hidden_id
WHERE ra.hidden_id IS NOT NULL;
ALTER TABLE role_abilities DROP COLUMN hidden_id;
DELETE FROM role_abilities WHERE role_id IS NULL;

DELETE t1 FROM role_abilities t1
INNER JOIN
role_abilities t2
WHERE
t1.id < t2.id
AND t1.setup_id = t2.setup_id
AND t1.role_id = t2.role_id
AND t1.name = t2.name;

INSERT INTO role_modifiers (setup_id, role_id, hidden_id, modifierName, modifierIValue, modifierBValue)
SELECT rm.setup_id, hr.role_id, NULL, rm.modifierName, rm.modifierIValue, rm.modifierBValue
FROM hidden_roles hr
LEFT JOIN role_modifiers rm
ON hr.hidden_id = rm.hidden_id
WHERE rm.hidden_id IS NOT NULL;
ALTER TABLE role_modifiers DROP COLUMN hidden_id;
DELETE FROM role_modifiers WHERE role_id IS NULL;


ALTER TABLE role_modifiers CHANGE COLUMN modifierName name VARCHAR(40) NOT NULL;
ALTER TABLE role_modifiers DROP COLUMN setup_id;
ALTER TABLE role_modifiers CHANGE COLUMN role_id role_id BIGINT UNSIGNED NOT NULL;

RENAME TABLE role_modifiers TO role_modifiers_old;
CREATE TABLE role_modifiers (
  role_id BIGINT UNSIGNED NOT NULL,
  name VARCHAR(40) NOT NULL,
  modifierIValue MEDIUMINT DEFAULT NULL,
  modifierBValue BOOLEAN DEFAULT NULL
);

INSERT INTO role_modifiers (role_id, name, modifierIValue, modifierBValue)
SELECT * from role_modifiers_old
WHERE name = 'unique' OR name = 'undetectable' OR name = 'auto_vest' OR name = 'unconvertable' OR name = 'unblockable';

DELETE FROM role_modifiers_old
WHERE name = 'unique' OR name = 'undetectable' OR name = 'auto_vest' OR name = 'unconvertable' OR name = 'unblockable';

ALTER TABLE role_modifiers_old ADD COLUMN ability_name VARCHAR(20) AFTER name;
ALTER TABLE role_modifiers_old ADD COLUMN ability_id BIGINT UNSIGNED AFTER ability_name;


UPDATE role_modifiers_old
SET ability_name = RIGHT(name, CHAR_LENGTH(name) - CHAR_LENGTH('cooldown'))
WHERE name LIKE 'cooldown%';

UPDATE role_modifiers_old m
LEFT JOIN role_abilities a
ON m.ability_name = a.name
AND m.role_id = a.role_id
SET m.ability_id = a.id
WHERE m.ability_name IS NOT NULL;

INSERT INTO role_ability_modifiers (role_ability_id, modifier_id, int_value, bool_value)
SELECT ability_id, 'cooldown', modifierIValue, NULL
FROM role_modifiers_old
WHERE ability_id IS NOT NULL;

DELETE FROM role_modifiers_old WHERE ability_id IS NOT NULL;

UPDATE role_modifiers_old
SET ability_name = RIGHT(name, CHAR_LENGTH(name) - CHAR_LENGTH('charges'))
WHERE name LIKE 'charges%';

UPDATE role_modifiers_old m
LEFT JOIN role_abilities a
ON m.ability_name = a.name
AND m.role_id = a.role_id
SET m.ability_id = a.id
WHERE m.ability_name IS NOT NULL;

INSERT INTO role_ability_modifiers (role_ability_id, modifier_id, int_value, bool_value)
SELECT ability_id, 'charges', modifierIValue, NULL
FROM role_modifiers_old
WHERE ability_id IS NOT NULL;

DELETE FROM role_modifiers_old WHERE ability_id IS NOT NULL;

UPDATE role_modifiers_old
SET ability_name = RIGHT(name, CHAR_LENGTH(name) - CHAR_LENGTH('hidden'))
WHERE name LIKE 'hidden%';

UPDATE role_modifiers_old m
LEFT JOIN role_abilities a
ON m.ability_name = a.name
AND m.role_id = a.role_id
SET m.ability_id = a.id
WHERE m.ability_name IS NOT NULL;

INSERT INTO role_ability_modifiers (role_ability_id, modifier_id, int_value, bool_value)
SELECT ability_id, 'hidden', NULL, modifierBValue
FROM role_modifiers_old
WHERE ability_id IS NOT NULL;

DELETE FROM role_modifiers_old WHERE ability_id IS NOT NULL;

UPDATE role_modifiers_old
SET ability_name = 'Blacksmith'
WHERE name = 'gs_faulty_guns';

UPDATE role_modifiers_old m
LEFT JOIN role_abilities a
ON m.ability_name = a.name
AND m.role_id = a.role_id
SET m.ability_id = a.id
WHERE m.ability_name IS NOT NULL;

INSERT INTO role_ability_modifiers (role_ability_id, modifier_id, int_value, bool_value)
SELECT ability_id, 'gs_faulty_guns', NULL, modifierBValue
FROM role_modifiers_old
WHERE ability_id IS NOT NULL;

DELETE FROM role_modifiers_old WHERE ability_id IS NOT NULL;

UPDATE role_modifiers_old
SET ability_name = 'Gunsmith'
WHERE name = 'gs_faulty_guns';

UPDATE role_modifiers_old m
LEFT JOIN role_abilities a
ON m.ability_name = a.name
AND m.role_id = a.role_id
SET m.ability_id = a.id
WHERE m.ability_name IS NOT NULL;

INSERT INTO role_ability_modifiers (role_ability_id, modifier_id, int_value, bool_value)
SELECT ability_id, 'gs_faulty_guns', NULL, modifierBValue
FROM role_modifiers_old
WHERE ability_id IS NOT NULL;

DELETE FROM role_modifiers_old WHERE ability_id IS NOT NULL;

UPDATE role_modifiers_old
SET ability_name = 'Armorsmith'
WHERE name = 'vs_fake_vest';

UPDATE role_modifiers_old m
LEFT JOIN role_abilities a
ON m.ability_name = a.name
AND m.role_id = a.role_id
SET m.ability_id = a.id
WHERE m.ability_name IS NOT NULL;

INSERT INTO role_ability_modifiers (role_ability_id, modifier_id, int_value, bool_value)
SELECT ability_id, 'vs_fake_vest', NULL, modifierBValue
FROM role_modifiers_old
WHERE ability_id IS NOT NULL;

DELETE FROM role_modifiers_old WHERE ability_id IS NOT NULL;

UPDATE role_modifiers_old
SET ability_name = 'Blacksmith'
WHERE name = 'vs_fake_vest';

UPDATE role_modifiers_old m
LEFT JOIN role_abilities a
ON m.ability_name = a.name
AND m.role_id = a.role_id
SET m.ability_id = a.id
WHERE m.ability_name IS NOT NULL;

INSERT INTO role_ability_modifiers (role_ability_id, modifier_id, int_value, bool_value)
SELECT ability_id, 'vs_fake_vest', NULL, modifierBValue
FROM role_modifiers_old
WHERE ability_id IS NOT NULL;

DELETE FROM role_modifiers_old WHERE ability_id IS NOT NULL;
DROP TABLE role_modifiers_old;

-- split role_modifiers
CREATE TABLE role_int_modifiers (
  role_id BIGINT UNSIGNED NOT NULL,
  name VARCHAR(40) NOT NULL,
  value MEDIUMINT NOT NULL
);

INSERT INTO role_int_modifiers (role_id, name, value)
SELECT role_id, name, modifierIValue
FROM role_modifiers
WHERE modifierIValue IS NOT NULL;

RENAME TABLE role_modifiers TO role_bool_modifiers;
ALTER TABLE role_bool_modifiers DROP COLUMN modifierIValue;
DELETE FROM role_bool_modifiers WHERE modifierBValue IS NULL;
ALTER TABLE role_bool_modifiers CHANGE COLUMN modifierBValue value BOOLEAN NOT NULL;

-- split role_ability_modifiers
CREATE TABLE role_ability_int_modifiers (
  role_ability_id BIGINT UNSIGNED NOT NULL,
  name VARCHAR(40) NOT NULL,
  value MEDIUMINT NOT NULL
);

INSERT INTO role_ability_int_modifiers (role_ability_id, name, value)
SELECT role_ability_id, modifier_id, int_value
FROM role_ability_modifiers
WHERE int_value IS NOT NULL;

RENAME TABLE role_ability_modifiers TO role_ability_bool_modifiers;
ALTER TABLE role_ability_bool_modifiers DROP COLUMN int_value;
DELETE FROM role_ability_bool_modifiers WHERE bool_value IS NULL;
ALTER TABLE role_ability_bool_modifiers CHANGE COLUMN bool_value value BOOLEAN NOT NULL;
ALTER TABLE role_ability_bool_modifiers CHANGE COLUMN modifier_id name VARCHAR(20) NOT NULL;


ALTER TABLE roles_list ADD COLUMN spawned_role_id BIGINT UNSIGNED AFTER setup_id;
UPDATE roles_list r
LEFT JOIN hidden_roles h
ON r.hidden_id = h.hidden_id
SET r.spawned_role_id = h.role_id
WHERE r.spawn_role is null;

ALTER TABLE role_abilities DROP COLUMN setup_id;
ALTER TABLE role_abilities CHANGE COLUMN role_id role_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE roles DROP COLUMN rt_id;
ALTER TABLE roles DROP COLUMN is_submember;
ALTER TABLE roles ADD COLUMN faction_id BIGINT UNSIGNED AFTER color;

UPDATE roles r
LEFT JOIN factions f
ON r.color = f.color
AND r.setup_id = f.setup_id
SET r.faction_id = f.id;

ALTER TABLE roles CHANGE COLUMN faction_id faction_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE roles DROP COLUMN color;
ALTER TABLE roles_list DROP COLUMN name;
ALTER TABLE roles_list DROP COLUMN color;
ALTER TABLE roles_list DROP COLUMN is_exposed;
ALTER TABLE hiddens DROP COLUMN is_single;
ALTER TABLE hiddens DROP COLUMN rt_id;
ALTER TABLE hiddens DROP COLUMN internal_name;
ALTER TABLE players CHANGE COLUMN is_exited is_exited BOOLEAN NOT NULL;

ALTER TABLE players ADD COLUMN roles_list_id BIGINT UNSIGNED NOT NULL AFTER name;
ALTER TABLE players ADD COLUMN role_id BIGINT UNSIGNED NOT NULL AFTER name;
ALTER TABLE players ADD COLUMN hidden_id BIGINT UNSIGNED NOT NULL AFTER name;

UPDATE players p
LEFT JOIN roles_list r
ON p.name = r.playerAssigned
AND r.setup_id = p.replay_id
SET p.roles_list_id = r.id;

UPDATE players p
LEFT JOIN roles_list r
ON p.name = r.playerAssigned
AND r.setup_id = p.replay_id
SET p.roles_list_id = r.id;

UPDATE players p
LEFT JOIN roles_list r
ON p.name = r.playerAssigned
AND r.setup_id = p.replay_id
SET p.hidden_id = r.hidden_id;

ALTER TABLE roles_list DROP COLUMN playerAssigned;
ALTER TABLE roles_list ADD COLUMN spawned_faction_id BIGINT UNSIGNED;

UPDATE roles_list r
LEFT JOIN factions f
ON r.spawn_color = f.color
AND r.setup_id = f.setup_id
SET r.spawned_faction_id = f.id
WHERE r.spawn_color IS NOT NULL;

ALTER TABLE roles_list DROP COLUMN spawn_color;

UPDATE roles_list rl
LEFT JOIN roles r
ON rl.setup_id = r.setup_id
AND rl.spawn_role = r.name
AND rl.spawned_faction_id = r.faction_id
SET rl.spawned_role_id = r.id
WHERE rl.spawn_role IS NOT NULL;

ALTER TABLE roles_list DROP COLUMN spawned_faction_id;
ALTER TABLE roles_list DROP COLUMN spawn_role;

UPDATE players p
LEFT JOIN roles_list rl
ON p.roles_list_id = rl.id
AND p.replay_id = rl.setup_id
SET p.role_id = rl.spawned_role_id;
ALTER TABLE roles_list DROP COLUMN spawned_role_id;

RENAME TABLE sheriff_detectables_saved_games TO sheriff_checkables;
ALTER TABLE sheriff_checkables CHANGE COLUMN game_id setup_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE sheriff_checkables ADD COLUMN sheriff_faction_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE sheriff_checkables ADD COLUMN detectable_faction_id BIGINT UNSIGNED NOT NULL;

UPDATE sheriff_checkables s
LEFT JOIN factions f
ON s.setup_id = f.setup_id
AND s.teamColor = f.color
SET s.sheriff_faction_id = f.id;
ALTER TABLE sheriff_checkables DROP COLUMN teamColor;

UPDATE sheriff_checkables s
LEFT JOIN factions f
ON s.setup_id = f.setup_id
AND s.detectedColor = f.color
SET s.detectable_faction_id = f.id;
ALTER TABLE sheriff_checkables DROP COLUMN detectedColor;
ALTER TABLE sheriff_checkables DROP COLUMN team_id;
ALTER TABLE sheriff_checkables DROP COLUMN setup_id;

RENAME TABLE enemies_saved_games TO faction_enemies;
ALTER TABLE faction_enemies DROP COLUMN enemies_id;
ALTER TABLE faction_enemies ADD COLUMN enemy_a BIGINT UNSIGNED NOT NULL;
ALTER TABLE faction_enemies ADD COLUMN enemy_b BIGINT UNSIGNED NOT NULL;

UPDATE faction_enemies e
LEFT JOIN factions f
ON e.game_id = f.setup_id
AND e.teamColor1 = f.color
SET e.enemy_a = f.id;
ALTER TABLE faction_enemies DROP COLUMN teamColor1;

UPDATE faction_enemies e
LEFT JOIN factions f
ON e.game_id = f.setup_id
AND e.teamColor2 = f.color
SET e.enemy_b = f.id;
ALTER TABLE faction_enemies DROP COLUMN teamColor2;
ALTER TABLE faction_enemies DROP COLUMN game_id;


CREATE TABLE discord_users(
    user_id BIGINT UNSIGNED NOT NULL,
    discord_id VARCHAR(36) NOT NULL
);
ALTER TABLE discord_users ADD CONSTRAINT user_id_discord_id UNIQUE(user_id, discord_id);

ALTER TABLE users CHANGE COLUMN accountType user_type VARCHAR(10) NOT NULL;

INSERT INTO discord_users (user_id, discord_id)
SELECT id, token FROM users WHERE user_type = 'discord';

RENAME TABLE permissions TO user_permissions;
ALTER TABLE user_permissions ADD COLUMN user_id BIGINT UNSIGNED NOT NULL FIRST;
ALTER TABLE user_permissions CHANGE COLUMN permission permission_type SMALLINT NOT NULL;
UPDATE user_permissions p
LEFT JOIN users u
ON u.token = p.user
SET p.user_id = u.id;
ALTER TABLE user_permissions DROP COLUMN `user`;
ALTER TABLE user_permissions ADD CONSTRAINT user_id_permission_type UNIQUE(user_id, permission_type);


INSERT INTO users (token, points, winrate, user_type)
SELECT DISTINCT name, 0, 0, "computer" FROM players WHERE user_id IS NULL;
UPDATE players p
LEFT JOIN users u
ON p.name = u.token
SET p.user_id = u.id
WHERE p.user_id IS NULL;

ALTER TABLE players CHANGE COLUMN user_id user_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE players DROP COLUMN roles_list_id;

RENAME TABLE roles_list TO setup_hiddens;

ALTER TABLE player_deaths CHANGE COLUMN game_id player_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE player_deaths DROP COLUMN baseName;
ALTER TABLE player_deaths DROP COLUMN token;
ALTER TABLE users CHANGE COLUMN winRate win_rate DOUBLE(5, 2) DEFAULT 0.0 NOT NULL;
ALTER TABLE users CHANGE COLUMN user_type type VARCHAR(10) NOT NULL;


UPDATE role_ability_bool_modifiers SET name = 'as_fake_vests' WHERE name = 'vs_fake_vest';


-- cascade deletes
ALTER TABLE user_permissions ADD CONSTRAINT user_permissions_on_user_delete FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;

ALTER TABLE sheriff_checkables ADD CONSTRAINT sheriff_checkables_on_sheriff_delete FOREIGN KEY (sheriff_faction_id) REFERENCES factions(id) ON DELETE CASCADE;
ALTER TABLE sheriff_checkables ADD CONSTRAINT sheriff_checkables_on_detectable_delete FOREIGN KEY (detectable_faction_id) REFERENCES factions(id) ON DELETE CASCADE;

ALTER TABLE setup_int_modifiers ADD CONSTRAINT setup_int_modifiers_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;

ALTER TABLE setup_hiddens ADD CONSTRAINT setup_hiddens_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;
ALTER TABLE setup_hiddens ADD CONSTRAINT setup_hiddens_on_hidden_delete FOREIGN KEY (hidden_id) REFERENCES hiddens(id) ON DELETE CASCADE;

ALTER TABLE setup_bool_modifiers ADD CONSTRAINT setup_bool_modifiers_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;

ALTER TABLE roles ADD CONSTRAINT roles_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;
ALTER TABLE roles ADD CONSTRAINT roles_on_faction_delete FOREIGN KEY (faction_id) REFERENCES factions(id) ON DELETE CASCADE;

ALTER TABLE role_int_modifiers ADD CONSTRAINT role_int_modifiers_on_role_delete FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;

ALTER TABLE role_bool_modifiers ADD CONSTRAINT role_bool_modifiers_on_role_delete FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;

ALTER TABLE role_ability_int_modifiers ADD CONSTRAINT role_ability_int_modifiers_on_role_ability_delete FOREIGN KEY (role_ability_id) REFERENCES role_abilities(id) ON DELETE CASCADE;

ALTER TABLE role_ability_bool_modifiers ADD CONSTRAINT role_ability_bool_modifiers_on_role_ability_delete FOREIGN KEY (role_ability_id) REFERENCES role_abilities(id) ON DELETE CASCADE;

ALTER TABLE role_abilities ADD CONSTRAINT role_abilities_on_role_delete FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;

ALTER TABLE replays ADD CONSTRAINT replays_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;

ALTER TABLE players ADD CONSTRAINT players_on_replay_delete FOREIGN KEY (replay_id) REFERENCES replays(id) ON DELETE CASCADE;
ALTER TABLE players ADD CONSTRAINT players_on_role_delete FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;
ALTER TABLE players ADD CONSTRAINT players_on_hidden_delete FOREIGN KEY (hidden_id) REFERENCES hiddens(id) ON DELETE CASCADE;

ALTER TABLE player_deaths ADD CONSTRAINT player_deaths_on_player_delete FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE;

ALTER TABLE hiddens ADD CONSTRAINT hiddens_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;

ALTER TABLE hidden_roles ADD CONSTRAINT hidden_roles_on_hidden_delete FOREIGN KEY (hidden_id) REFERENCES hiddens(id) ON DELETE CASCADE;
ALTER TABLE hidden_roles ADD CONSTRAINT hidden_roles_on_role_delete FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;

ALTER TABLE factions ADD CONSTRAINT factions_on_setup_delete FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;

ALTER TABLE faction_int_modifiers ADD CONSTRAINT faction_int_modifiers_on_faction_delete FOREIGN KEY (faction_id) REFERENCES factions(id) ON DELETE CASCADE;

ALTER TABLE faction_enemies ADD CONSTRAINT faction_enemies_on_faction1_delete FOREIGN KEY (enemy_a) REFERENCES factions(id) ON DELETE CASCADE;
ALTER TABLE faction_enemies ADD CONSTRAINT faction_enemies_on_faction2_delete FOREIGN KEY (enemy_a) REFERENCES factions(id) ON DELETE CASCADE;

ALTER TABLE faction_bool_modifiers ADD CONSTRAINT faction_bool_modifiers_on_faction_delete FOREIGN KEY (faction_id) REFERENCES factions(id) ON DELETE CASCADE;

ALTER TABLE faction_ability_int_modifiers ADD CONSTRAINT faction_ability_int_modifiers_on_faction_ability_delete FOREIGN KEY (faction_ability_id) REFERENCES faction_abilities(id) ON DELETE CASCADE;

ALTER TABLE faction_ability_bool_modifiers ADD CONSTRAINT faction_ability_bool_modifiers_on_faction_ability_delete FOREIGN KEY (faction_ability_id) REFERENCES faction_abilities(id) ON DELETE CASCADE;

ALTER TABLE faction_abilities ADD CONSTRAINT faction_abilities_on_faction_delete FOREIGN KEY (faction_id) REFERENCES factions(id) ON DELETE CASCADE;

ALTER TABLE discord_users ADD CONSTRAINT discord_users_on_user_delete FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;

ALTER TABLE commands ADD CONSTRAINT commands_on_setup_delete FOREIGN KEY (replay_id) REFERENCES replays(id) ON DELETE CASCADE;

DELETE FROM replays WHERE id = 508;
DELETE FROM replays WHERE id = 341;
DELETE FROM replays WHERE id = 349;
DELETE FROM replays WHERE id = 553;

-- story fixes
UPDATE players SET role_id = 8629 WHERE id = 1701;
UPDATE players SET role_id = 8657 WHERE id = 1703;
UPDATE players SET role_id = 8632 WHERE id = 1708;
UPDATE players SET role_id = 9078 WHERE id = 1775;
UPDATE players SET role_id = 9111 WHERE id = 1778;
UPDATE players SET role_id = 9082 WHERE id = 1792;

UPDATE players SET role_id = 601 WHERE role_id = 606;
UPDATE IGNORE hidden_roles SET role_id = 601 WHERE role_id = 606;
DELETE FROM roles WHERE id = 606;
UPDATE players SET role_id = 603 WHERE role_id = 608;
UPDATE IGNORE hidden_roles SET role_id = 603 WHERE role_id = 608;
DELETE FROM roles WHERE id = 608;
UPDATE players SET role_id = 604 WHERE role_id = 609;
UPDATE IGNORE hidden_roles SET role_id = 604 WHERE role_id = 609;
DELETE FROM roles WHERE id = 609;
UPDATE players SET role_id = 605 WHERE role_id = 610;
UPDATE IGNORE hidden_roles SET role_id = 605 WHERE role_id = 610;
DELETE FROM roles WHERE id = 610;
UPDATE players SET role_id = 686 WHERE role_id = 15402;
UPDATE IGNORE hidden_roles SET role_id = 686 WHERE role_id = 15402;
DELETE FROM roles WHERE id = 15402;
UPDATE players SET role_id = 708 WHERE role_id = 15403;
UPDATE IGNORE hidden_roles SET role_id = 708 WHERE role_id = 15403;
DELETE FROM roles WHERE id = 15403;
UPDATE players SET role_id = 734 WHERE role_id = 15406;
UPDATE IGNORE hidden_roles SET role_id = 734 WHERE role_id = 15406;
DELETE FROM roles WHERE id = 15406;
UPDATE players SET role_id = 895 WHERE role_id = 910;
UPDATE IGNORE hidden_roles SET role_id = 895 WHERE role_id = 910;
DELETE FROM roles WHERE id = 910;
UPDATE players SET role_id = 899 WHERE role_id = 911;
UPDATE IGNORE hidden_roles SET role_id = 899 WHERE role_id = 911;
DELETE FROM roles WHERE id = 911;
UPDATE players SET role_id = 897 WHERE role_id = 912;
UPDATE IGNORE hidden_roles SET role_id = 897 WHERE role_id = 912;
DELETE FROM roles WHERE id = 912;
UPDATE players SET role_id = 901 WHERE role_id = 913;
UPDATE IGNORE hidden_roles SET role_id = 901 WHERE role_id = 913;
DELETE FROM roles WHERE id = 913;
UPDATE players SET role_id = 898 WHERE role_id = 914;
UPDATE IGNORE hidden_roles SET role_id = 898 WHERE role_id = 914;
DELETE FROM roles WHERE id = 914;
UPDATE players SET role_id = 902 WHERE role_id = 915;
UPDATE IGNORE hidden_roles SET role_id = 902 WHERE role_id = 915;
DELETE FROM roles WHERE id = 915;
UPDATE players SET role_id = 903 WHERE role_id = 916;
UPDATE IGNORE hidden_roles SET role_id = 903 WHERE role_id = 916;
DELETE FROM roles WHERE id = 916;
UPDATE players SET role_id = 900 WHERE role_id = 918;
UPDATE IGNORE hidden_roles SET role_id = 900 WHERE role_id = 918;
DELETE FROM roles WHERE id = 918;
UPDATE players SET role_id = 904 WHERE role_id = 919;
UPDATE IGNORE hidden_roles SET role_id = 904 WHERE role_id = 919;
DELETE FROM roles WHERE id = 919;
UPDATE players SET role_id = 907 WHERE role_id = 920;
UPDATE IGNORE hidden_roles SET role_id = 907 WHERE role_id = 920;
DELETE FROM roles WHERE id = 920;
UPDATE players SET role_id = 906 WHERE role_id = 921;
UPDATE IGNORE hidden_roles SET role_id = 906 WHERE role_id = 921;
DELETE FROM roles WHERE id = 921;
UPDATE players SET role_id = 896 WHERE role_id = 922;
UPDATE IGNORE hidden_roles SET role_id = 896 WHERE role_id = 922;
DELETE FROM roles WHERE id = 922;
UPDATE players SET role_id = 908 WHERE role_id = 923;
UPDATE IGNORE hidden_roles SET role_id = 908 WHERE role_id = 923;
DELETE FROM roles WHERE id = 923;
UPDATE players SET role_id = 909 WHERE role_id = 15419;
UPDATE IGNORE hidden_roles SET role_id = 909 WHERE role_id = 15419;
DELETE FROM roles WHERE id = 15419;
UPDATE players SET role_id = 1067 WHERE role_id = 1079;
UPDATE IGNORE hidden_roles SET role_id = 1067 WHERE role_id = 1079;
DELETE FROM roles WHERE id = 1079;
UPDATE players SET role_id = 1068 WHERE role_id = 1084;
UPDATE IGNORE hidden_roles SET role_id = 1068 WHERE role_id = 1084;
DELETE FROM roles WHERE id = 1084;
UPDATE players SET role_id = 1070 WHERE role_id = 1086;
UPDATE IGNORE hidden_roles SET role_id = 1070 WHERE role_id = 1086;
DELETE FROM roles WHERE id = 1086;
UPDATE players SET role_id = 1069 WHERE role_id = 1087;
UPDATE IGNORE hidden_roles SET role_id = 1069 WHERE role_id = 1087;
DELETE FROM roles WHERE id = 1087;
UPDATE players SET role_id = 1074 WHERE role_id = 1088;
UPDATE IGNORE hidden_roles SET role_id = 1074 WHERE role_id = 1088;
DELETE FROM roles WHERE id = 1088;
UPDATE players SET role_id = 1081 WHERE role_id = 1089;
UPDATE IGNORE hidden_roles SET role_id = 1081 WHERE role_id = 1089;
DELETE FROM roles WHERE id = 1089;
UPDATE players SET role_id = 1082 WHERE role_id = 1090;
UPDATE IGNORE hidden_roles SET role_id = 1082 WHERE role_id = 1090;
DELETE FROM roles WHERE id = 1090;
UPDATE players SET role_id = 1083 WHERE role_id = 1091;
UPDATE IGNORE hidden_roles SET role_id = 1083 WHERE role_id = 1091;
DELETE FROM roles WHERE id = 1091;
UPDATE players SET role_id = 1100 WHERE role_id = 15422;
UPDATE IGNORE hidden_roles SET role_id = 1100 WHERE role_id = 15422;
DELETE FROM roles WHERE id = 15422;
UPDATE players SET role_id = 1223 WHERE role_id = 1235;
UPDATE IGNORE hidden_roles SET role_id = 1223 WHERE role_id = 1235;
DELETE FROM roles WHERE id = 1235;
UPDATE players SET role_id = 1224 WHERE role_id = 1240;
UPDATE IGNORE hidden_roles SET role_id = 1224 WHERE role_id = 1240;
DELETE FROM roles WHERE id = 1240;
UPDATE players SET role_id = 1226 WHERE role_id = 1242;
UPDATE IGNORE hidden_roles SET role_id = 1226 WHERE role_id = 1242;
DELETE FROM roles WHERE id = 1242;
UPDATE players SET role_id = 1225 WHERE role_id = 1243;
UPDATE IGNORE hidden_roles SET role_id = 1225 WHERE role_id = 1243;
DELETE FROM roles WHERE id = 1243;
UPDATE players SET role_id = 1230 WHERE role_id = 1244;
UPDATE IGNORE hidden_roles SET role_id = 1230 WHERE role_id = 1244;
DELETE FROM roles WHERE id = 1244;
UPDATE players SET role_id = 1237 WHERE role_id = 1245;
UPDATE IGNORE hidden_roles SET role_id = 1237 WHERE role_id = 1245;
DELETE FROM roles WHERE id = 1245;
UPDATE players SET role_id = 1238 WHERE role_id = 1246;
UPDATE IGNORE hidden_roles SET role_id = 1238 WHERE role_id = 1246;
DELETE FROM roles WHERE id = 1246;
UPDATE players SET role_id = 1239 WHERE role_id = 1247;
UPDATE IGNORE hidden_roles SET role_id = 1239 WHERE role_id = 1247;
DELETE FROM roles WHERE id = 1247;
UPDATE players SET role_id = 1256 WHERE role_id = 15429;
UPDATE IGNORE hidden_roles SET role_id = 1256 WHERE role_id = 15429;
DELETE FROM roles WHERE id = 15429;
UPDATE players SET role_id = 1348 WHERE role_id = 1360;
UPDATE IGNORE hidden_roles SET role_id = 1348 WHERE role_id = 1360;
DELETE FROM roles WHERE id = 1360;
UPDATE players SET role_id = 1349 WHERE role_id = 1365;
UPDATE IGNORE hidden_roles SET role_id = 1349 WHERE role_id = 1365;
DELETE FROM roles WHERE id = 1365;
UPDATE players SET role_id = 1351 WHERE role_id = 1367;
UPDATE IGNORE hidden_roles SET role_id = 1351 WHERE role_id = 1367;
DELETE FROM roles WHERE id = 1367;
UPDATE players SET role_id = 1350 WHERE role_id = 1368;
UPDATE IGNORE hidden_roles SET role_id = 1350 WHERE role_id = 1368;
DELETE FROM roles WHERE id = 1368;
UPDATE players SET role_id = 1355 WHERE role_id = 1369;
UPDATE IGNORE hidden_roles SET role_id = 1355 WHERE role_id = 1369;
DELETE FROM roles WHERE id = 1369;
UPDATE players SET role_id = 1362 WHERE role_id = 1370;
UPDATE IGNORE hidden_roles SET role_id = 1362 WHERE role_id = 1370;
DELETE FROM roles WHERE id = 1370;
UPDATE players SET role_id = 1363 WHERE role_id = 1371;
UPDATE IGNORE hidden_roles SET role_id = 1363 WHERE role_id = 1371;
DELETE FROM roles WHERE id = 1371;
UPDATE players SET role_id = 1364 WHERE role_id = 1372;
UPDATE IGNORE hidden_roles SET role_id = 1364 WHERE role_id = 1372;
DELETE FROM roles WHERE id = 1372;
UPDATE players SET role_id = 1377 WHERE role_id = 15437;
UPDATE IGNORE hidden_roles SET role_id = 1377 WHERE role_id = 15437;
DELETE FROM roles WHERE id = 15437;
UPDATE players SET role_id = 1418 WHERE role_id = 1430;
UPDATE IGNORE hidden_roles SET role_id = 1418 WHERE role_id = 1430;
DELETE FROM roles WHERE id = 1430;
UPDATE players SET role_id = 1419 WHERE role_id = 1435;
UPDATE IGNORE hidden_roles SET role_id = 1419 WHERE role_id = 1435;
DELETE FROM roles WHERE id = 1435;
UPDATE players SET role_id = 1421 WHERE role_id = 1437;
UPDATE IGNORE hidden_roles SET role_id = 1421 WHERE role_id = 1437;
DELETE FROM roles WHERE id = 1437;
UPDATE players SET role_id = 1420 WHERE role_id = 1438;
UPDATE IGNORE hidden_roles SET role_id = 1420 WHERE role_id = 1438;
DELETE FROM roles WHERE id = 1438;
UPDATE players SET role_id = 1425 WHERE role_id = 1439;
UPDATE IGNORE hidden_roles SET role_id = 1425 WHERE role_id = 1439;
DELETE FROM roles WHERE id = 1439;
UPDATE players SET role_id = 1432 WHERE role_id = 1440;
UPDATE IGNORE hidden_roles SET role_id = 1432 WHERE role_id = 1440;
DELETE FROM roles WHERE id = 1440;
UPDATE players SET role_id = 1433 WHERE role_id = 1441;
UPDATE IGNORE hidden_roles SET role_id = 1433 WHERE role_id = 1441;
DELETE FROM roles WHERE id = 1441;
UPDATE players SET role_id = 1434 WHERE role_id = 1442;
UPDATE IGNORE hidden_roles SET role_id = 1434 WHERE role_id = 1442;
DELETE FROM roles WHERE id = 1442;
UPDATE players SET role_id = 1451 WHERE role_id = 15438;
UPDATE IGNORE hidden_roles SET role_id = 1451 WHERE role_id = 15438;
DELETE FROM roles WHERE id = 15438;
UPDATE players SET role_id = 1473 WHERE role_id = 1485;
UPDATE IGNORE hidden_roles SET role_id = 1473 WHERE role_id = 1485;
DELETE FROM roles WHERE id = 1485;
UPDATE players SET role_id = 1474 WHERE role_id = 1490;
UPDATE IGNORE hidden_roles SET role_id = 1474 WHERE role_id = 1490;
DELETE FROM roles WHERE id = 1490;
UPDATE players SET role_id = 1476 WHERE role_id = 1492;
UPDATE IGNORE hidden_roles SET role_id = 1476 WHERE role_id = 1492;
DELETE FROM roles WHERE id = 1492;
UPDATE players SET role_id = 1475 WHERE role_id = 1493;
UPDATE IGNORE hidden_roles SET role_id = 1475 WHERE role_id = 1493;
DELETE FROM roles WHERE id = 1493;
UPDATE players SET role_id = 1480 WHERE role_id = 1494;
UPDATE IGNORE hidden_roles SET role_id = 1480 WHERE role_id = 1494;
DELETE FROM roles WHERE id = 1494;
UPDATE players SET role_id = 1487 WHERE role_id = 1495;
UPDATE IGNORE hidden_roles SET role_id = 1487 WHERE role_id = 1495;
DELETE FROM roles WHERE id = 1495;
UPDATE players SET role_id = 1488 WHERE role_id = 1496;
UPDATE IGNORE hidden_roles SET role_id = 1488 WHERE role_id = 1496;
DELETE FROM roles WHERE id = 1496;
UPDATE players SET role_id = 1489 WHERE role_id = 1497;
UPDATE IGNORE hidden_roles SET role_id = 1489 WHERE role_id = 1497;
DELETE FROM roles WHERE id = 1497;
UPDATE players SET role_id = 1502 WHERE role_id = 15449;
UPDATE IGNORE hidden_roles SET role_id = 1502 WHERE role_id = 15449;
DELETE FROM roles WHERE id = 15449;
UPDATE players SET role_id = 1545 WHERE role_id = 1557;
UPDATE IGNORE hidden_roles SET role_id = 1545 WHERE role_id = 1557;
DELETE FROM roles WHERE id = 1557;
UPDATE players SET role_id = 1546 WHERE role_id = 1562;
UPDATE IGNORE hidden_roles SET role_id = 1546 WHERE role_id = 1562;
DELETE FROM roles WHERE id = 1562;
UPDATE players SET role_id = 1548 WHERE role_id = 1564;
UPDATE IGNORE hidden_roles SET role_id = 1548 WHERE role_id = 1564;
DELETE FROM roles WHERE id = 1564;
UPDATE players SET role_id = 1547 WHERE role_id = 1565;
UPDATE IGNORE hidden_roles SET role_id = 1547 WHERE role_id = 1565;
DELETE FROM roles WHERE id = 1565;
UPDATE players SET role_id = 1552 WHERE role_id = 1566;
UPDATE IGNORE hidden_roles SET role_id = 1552 WHERE role_id = 1566;
DELETE FROM roles WHERE id = 1566;
UPDATE players SET role_id = 1559 WHERE role_id = 1567;
UPDATE IGNORE hidden_roles SET role_id = 1559 WHERE role_id = 1567;
DELETE FROM roles WHERE id = 1567;
UPDATE players SET role_id = 1560 WHERE role_id = 1568;
UPDATE IGNORE hidden_roles SET role_id = 1560 WHERE role_id = 1568;
DELETE FROM roles WHERE id = 1568;
UPDATE players SET role_id = 1561 WHERE role_id = 1569;
UPDATE IGNORE hidden_roles SET role_id = 1561 WHERE role_id = 1569;
DELETE FROM roles WHERE id = 1569;
UPDATE players SET role_id = 1578 WHERE role_id = 15458;
UPDATE IGNORE hidden_roles SET role_id = 1578 WHERE role_id = 15458;
DELETE FROM roles WHERE id = 15458;
UPDATE players SET role_id = 1584 WHERE role_id = 1596;
UPDATE IGNORE hidden_roles SET role_id = 1584 WHERE role_id = 1596;
DELETE FROM roles WHERE id = 1596;
UPDATE players SET role_id = 1585 WHERE role_id = 1601;
UPDATE IGNORE hidden_roles SET role_id = 1585 WHERE role_id = 1601;
DELETE FROM roles WHERE id = 1601;
UPDATE players SET role_id = 1587 WHERE role_id = 1603;
UPDATE IGNORE hidden_roles SET role_id = 1587 WHERE role_id = 1603;
DELETE FROM roles WHERE id = 1603;
UPDATE players SET role_id = 1586 WHERE role_id = 1604;
UPDATE IGNORE hidden_roles SET role_id = 1586 WHERE role_id = 1604;
DELETE FROM roles WHERE id = 1604;
UPDATE players SET role_id = 1591 WHERE role_id = 1605;
UPDATE IGNORE hidden_roles SET role_id = 1591 WHERE role_id = 1605;
DELETE FROM roles WHERE id = 1605;
UPDATE players SET role_id = 1598 WHERE role_id = 1606;
UPDATE IGNORE hidden_roles SET role_id = 1598 WHERE role_id = 1606;
DELETE FROM roles WHERE id = 1606;
UPDATE players SET role_id = 1599 WHERE role_id = 1607;
UPDATE IGNORE hidden_roles SET role_id = 1599 WHERE role_id = 1607;
DELETE FROM roles WHERE id = 1607;
UPDATE players SET role_id = 1600 WHERE role_id = 1608;
UPDATE IGNORE hidden_roles SET role_id = 1600 WHERE role_id = 1608;
DELETE FROM roles WHERE id = 1608;
UPDATE players SET role_id = 1613 WHERE role_id = 15459;
UPDATE IGNORE hidden_roles SET role_id = 1613 WHERE role_id = 15459;
DELETE FROM roles WHERE id = 15459;
UPDATE players SET role_id = 4573 WHERE role_id = 4585;
UPDATE IGNORE hidden_roles SET role_id = 4573 WHERE role_id = 4585;
DELETE FROM roles WHERE id = 4585;
UPDATE players SET role_id = 4570 WHERE role_id = 4586;
UPDATE IGNORE hidden_roles SET role_id = 4570 WHERE role_id = 4586;
DELETE FROM roles WHERE id = 4586;
UPDATE players SET role_id = 4575 WHERE role_id = 4587;
UPDATE IGNORE hidden_roles SET role_id = 4575 WHERE role_id = 4587;
DELETE FROM roles WHERE id = 4587;
UPDATE players SET role_id = 4571 WHERE role_id = 4588;
UPDATE IGNORE hidden_roles SET role_id = 4571 WHERE role_id = 4588;
DELETE FROM roles WHERE id = 4588;
UPDATE players SET role_id = 4576 WHERE role_id = 4589;
UPDATE IGNORE hidden_roles SET role_id = 4576 WHERE role_id = 4589;
DELETE FROM roles WHERE id = 4589;
UPDATE players SET role_id = 4577 WHERE role_id = 4590;
UPDATE IGNORE hidden_roles SET role_id = 4577 WHERE role_id = 4590;
DELETE FROM roles WHERE id = 4590;
UPDATE players SET role_id = 4572 WHERE role_id = 4592;
UPDATE IGNORE hidden_roles SET role_id = 4572 WHERE role_id = 4592;
DELETE FROM roles WHERE id = 4592;
UPDATE players SET role_id = 4574 WHERE role_id = 4593;
UPDATE IGNORE hidden_roles SET role_id = 4574 WHERE role_id = 4593;
DELETE FROM roles WHERE id = 4593;
UPDATE players SET role_id = 4578 WHERE role_id = 4594;
UPDATE IGNORE hidden_roles SET role_id = 4578 WHERE role_id = 4594;
DELETE FROM roles WHERE id = 4594;
UPDATE players SET role_id = 4581 WHERE role_id = 4595;
UPDATE IGNORE hidden_roles SET role_id = 4581 WHERE role_id = 4595;
DELETE FROM roles WHERE id = 4595;
UPDATE players SET role_id = 4580 WHERE role_id = 4596;
UPDATE IGNORE hidden_roles SET role_id = 4580 WHERE role_id = 4596;
DELETE FROM roles WHERE id = 4596;
UPDATE players SET role_id = 4569 WHERE role_id = 4597;
UPDATE IGNORE hidden_roles SET role_id = 4569 WHERE role_id = 4597;
DELETE FROM roles WHERE id = 4597;
UPDATE players SET role_id = 4582 WHERE role_id = 4598;
UPDATE IGNORE hidden_roles SET role_id = 4582 WHERE role_id = 4598;
DELETE FROM roles WHERE id = 4598;
UPDATE players SET role_id = 4584 WHERE role_id = 4599;
UPDATE IGNORE hidden_roles SET role_id = 4584 WHERE role_id = 4599;
DELETE FROM roles WHERE id = 4599;
UPDATE players SET role_id = 4583 WHERE role_id = 15498;
UPDATE IGNORE hidden_roles SET role_id = 4583 WHERE role_id = 15498;
DELETE FROM roles WHERE id = 15498;
UPDATE players SET role_id = 4637 WHERE role_id = 15502;
UPDATE IGNORE hidden_roles SET role_id = 4637 WHERE role_id = 15502;
DELETE FROM roles WHERE id = 15502;
UPDATE players SET role_id = 4660 WHERE role_id = 15508;
UPDATE IGNORE hidden_roles SET role_id = 4660 WHERE role_id = 15508;
DELETE FROM roles WHERE id = 15508;
UPDATE players SET role_id = 4923 WHERE role_id = 15537;
UPDATE IGNORE hidden_roles SET role_id = 4923 WHERE role_id = 15537;
DELETE FROM roles WHERE id = 15537;
UPDATE players SET role_id = 4946 WHERE role_id = 15543;
UPDATE IGNORE hidden_roles SET role_id = 4946 WHERE role_id = 15543;
DELETE FROM roles WHERE id = 15543;
UPDATE players SET role_id = 4969 WHERE role_id = 15549;
UPDATE IGNORE hidden_roles SET role_id = 4969 WHERE role_id = 15549;
DELETE FROM roles WHERE id = 15549;
UPDATE players SET role_id = 5276 WHERE role_id = 15725;
UPDATE IGNORE hidden_roles SET role_id = 5276 WHERE role_id = 15725;
DELETE FROM roles WHERE id = 15725;
UPDATE players SET role_id = 5284 WHERE role_id = 5290;
UPDATE IGNORE hidden_roles SET role_id = 5284 WHERE role_id = 5290;
DELETE FROM roles WHERE id = 5290;
UPDATE players SET role_id = 5283 WHERE role_id = 5306;
UPDATE IGNORE hidden_roles SET role_id = 5283 WHERE role_id = 5306;
DELETE FROM roles WHERE id = 5306;
UPDATE players SET role_id = 5285 WHERE role_id = 5310;
UPDATE IGNORE hidden_roles SET role_id = 5285 WHERE role_id = 5310;
DELETE FROM roles WHERE id = 5310;
UPDATE players SET role_id = 5286 WHERE role_id = 5311;
UPDATE IGNORE hidden_roles SET role_id = 5286 WHERE role_id = 5311;
DELETE FROM roles WHERE id = 5311;
UPDATE players SET role_id = 5287 WHERE role_id = 5312;
UPDATE IGNORE hidden_roles SET role_id = 5287 WHERE role_id = 5312;
DELETE FROM roles WHERE id = 5312;
UPDATE players SET role_id = 5289 WHERE role_id = 5316;
UPDATE IGNORE hidden_roles SET role_id = 5289 WHERE role_id = 5316;
DELETE FROM roles WHERE id = 5316;
UPDATE players SET role_id = 5299 WHERE role_id = 5317;
UPDATE IGNORE hidden_roles SET role_id = 5299 WHERE role_id = 5317;
DELETE FROM roles WHERE id = 5317;
UPDATE players SET role_id = 5291 WHERE role_id = 5318;
UPDATE IGNORE hidden_roles SET role_id = 5291 WHERE role_id = 5318;
DELETE FROM roles WHERE id = 5318;
UPDATE players SET role_id = 5308 WHERE role_id = 5319;
UPDATE IGNORE hidden_roles SET role_id = 5308 WHERE role_id = 5319;
DELETE FROM roles WHERE id = 5319;
UPDATE players SET role_id = 5309 WHERE role_id = 5320;
UPDATE IGNORE hidden_roles SET role_id = 5309 WHERE role_id = 5320;
DELETE FROM roles WHERE id = 5320;
UPDATE players SET role_id = 5302 WHERE role_id = 5321;
UPDATE IGNORE hidden_roles SET role_id = 5302 WHERE role_id = 5321;
DELETE FROM roles WHERE id = 5321;
UPDATE players SET role_id = 5305 WHERE role_id = 15726;
UPDATE IGNORE hidden_roles SET role_id = 5305 WHERE role_id = 15726;
DELETE FROM roles WHERE id = 15726;
UPDATE players SET role_id = 5323 WHERE role_id = 5329;
UPDATE IGNORE hidden_roles SET role_id = 5323 WHERE role_id = 5329;
DELETE FROM roles WHERE id = 5329;
UPDATE players SET role_id = 5322 WHERE role_id = 5345;
UPDATE IGNORE hidden_roles SET role_id = 5322 WHERE role_id = 5345;
DELETE FROM roles WHERE id = 5345;
UPDATE players SET role_id = 5324 WHERE role_id = 5349;
UPDATE IGNORE hidden_roles SET role_id = 5324 WHERE role_id = 5349;
DELETE FROM roles WHERE id = 5349;
UPDATE players SET role_id = 5325 WHERE role_id = 5350;
UPDATE IGNORE hidden_roles SET role_id = 5325 WHERE role_id = 5350;
DELETE FROM roles WHERE id = 5350;
UPDATE players SET role_id = 5326 WHERE role_id = 5351;
UPDATE IGNORE hidden_roles SET role_id = 5326 WHERE role_id = 5351;
DELETE FROM roles WHERE id = 5351;
UPDATE players SET role_id = 5328 WHERE role_id = 5355;
UPDATE IGNORE hidden_roles SET role_id = 5328 WHERE role_id = 5355;
DELETE FROM roles WHERE id = 5355;
UPDATE players SET role_id = 5338 WHERE role_id = 5356;
UPDATE IGNORE hidden_roles SET role_id = 5338 WHERE role_id = 5356;
DELETE FROM roles WHERE id = 5356;
UPDATE players SET role_id = 5330 WHERE role_id = 5357;
UPDATE IGNORE hidden_roles SET role_id = 5330 WHERE role_id = 5357;
DELETE FROM roles WHERE id = 5357;
UPDATE players SET role_id = 5347 WHERE role_id = 5358;
UPDATE IGNORE hidden_roles SET role_id = 5347 WHERE role_id = 5358;
DELETE FROM roles WHERE id = 5358;
UPDATE players SET role_id = 5348 WHERE role_id = 5359;
UPDATE IGNORE hidden_roles SET role_id = 5348 WHERE role_id = 5359;
DELETE FROM roles WHERE id = 5359;
UPDATE players SET role_id = 5341 WHERE role_id = 5360;
UPDATE IGNORE hidden_roles SET role_id = 5341 WHERE role_id = 5360;
DELETE FROM roles WHERE id = 5360;
UPDATE players SET role_id = 5344 WHERE role_id = 15729;
UPDATE IGNORE hidden_roles SET role_id = 5344 WHERE role_id = 15729;
DELETE FROM roles WHERE id = 15729;
UPDATE players SET role_id = 5362 WHERE role_id = 5368;
UPDATE IGNORE hidden_roles SET role_id = 5362 WHERE role_id = 5368;
DELETE FROM roles WHERE id = 5368;
UPDATE players SET role_id = 5361 WHERE role_id = 5384;
UPDATE IGNORE hidden_roles SET role_id = 5361 WHERE role_id = 5384;
DELETE FROM roles WHERE id = 5384;
UPDATE players SET role_id = 5363 WHERE role_id = 5388;
UPDATE IGNORE hidden_roles SET role_id = 5363 WHERE role_id = 5388;
DELETE FROM roles WHERE id = 5388;
UPDATE players SET role_id = 5364 WHERE role_id = 5389;
UPDATE IGNORE hidden_roles SET role_id = 5364 WHERE role_id = 5389;
DELETE FROM roles WHERE id = 5389;
UPDATE players SET role_id = 5365 WHERE role_id = 5390;
UPDATE IGNORE hidden_roles SET role_id = 5365 WHERE role_id = 5390;
DELETE FROM roles WHERE id = 5390;
UPDATE players SET role_id = 5367 WHERE role_id = 5394;
UPDATE IGNORE hidden_roles SET role_id = 5367 WHERE role_id = 5394;
DELETE FROM roles WHERE id = 5394;
UPDATE players SET role_id = 5377 WHERE role_id = 5395;
UPDATE IGNORE hidden_roles SET role_id = 5377 WHERE role_id = 5395;
DELETE FROM roles WHERE id = 5395;
UPDATE players SET role_id = 5369 WHERE role_id = 5396;
UPDATE IGNORE hidden_roles SET role_id = 5369 WHERE role_id = 5396;
DELETE FROM roles WHERE id = 5396;
UPDATE players SET role_id = 5386 WHERE role_id = 5397;
UPDATE IGNORE hidden_roles SET role_id = 5386 WHERE role_id = 5397;
DELETE FROM roles WHERE id = 5397;
UPDATE players SET role_id = 5387 WHERE role_id = 5398;
UPDATE IGNORE hidden_roles SET role_id = 5387 WHERE role_id = 5398;
DELETE FROM roles WHERE id = 5398;
UPDATE players SET role_id = 5380 WHERE role_id = 5399;
UPDATE IGNORE hidden_roles SET role_id = 5380 WHERE role_id = 5399;
DELETE FROM roles WHERE id = 5399;
UPDATE players SET role_id = 5439 WHERE role_id = 5450;
UPDATE IGNORE hidden_roles SET role_id = 5439 WHERE role_id = 5450;
DELETE FROM roles WHERE id = 5450;
UPDATE players SET role_id = 5440 WHERE role_id = 5455;
UPDATE IGNORE hidden_roles SET role_id = 5440 WHERE role_id = 5455;
DELETE FROM roles WHERE id = 5455;
UPDATE players SET role_id = 5442 WHERE role_id = 5457;
UPDATE IGNORE hidden_roles SET role_id = 5442 WHERE role_id = 5457;
DELETE FROM roles WHERE id = 5457;
UPDATE players SET role_id = 5441 WHERE role_id = 5458;
UPDATE IGNORE hidden_roles SET role_id = 5441 WHERE role_id = 5458;
DELETE FROM roles WHERE id = 5458;
UPDATE players SET role_id = 5445 WHERE role_id = 5460;
UPDATE IGNORE hidden_roles SET role_id = 5445 WHERE role_id = 5460;
DELETE FROM roles WHERE id = 5460;
UPDATE players SET role_id = 5452 WHERE role_id = 5461;
UPDATE IGNORE hidden_roles SET role_id = 5452 WHERE role_id = 5461;
DELETE FROM roles WHERE id = 5461;
UPDATE players SET role_id = 5453 WHERE role_id = 5462;
UPDATE IGNORE hidden_roles SET role_id = 5453 WHERE role_id = 5462;
DELETE FROM roles WHERE id = 5462;
UPDATE players SET role_id = 5454 WHERE role_id = 5463;
UPDATE IGNORE hidden_roles SET role_id = 5454 WHERE role_id = 5463;
DELETE FROM roles WHERE id = 5463;
UPDATE players SET role_id = 5472 WHERE role_id = 15754;
UPDATE IGNORE hidden_roles SET role_id = 5472 WHERE role_id = 15754;
DELETE FROM roles WHERE id = 15754;
UPDATE players SET role_id = 5514 WHERE role_id = 5525;
UPDATE IGNORE hidden_roles SET role_id = 5514 WHERE role_id = 5525;
DELETE FROM roles WHERE id = 5525;
UPDATE players SET role_id = 5515 WHERE role_id = 5530;
UPDATE IGNORE hidden_roles SET role_id = 5515 WHERE role_id = 5530;
DELETE FROM roles WHERE id = 5530;
UPDATE players SET role_id = 5517 WHERE role_id = 5532;
UPDATE IGNORE hidden_roles SET role_id = 5517 WHERE role_id = 5532;
DELETE FROM roles WHERE id = 5532;
UPDATE players SET role_id = 5516 WHERE role_id = 5533;
UPDATE IGNORE hidden_roles SET role_id = 5516 WHERE role_id = 5533;
DELETE FROM roles WHERE id = 5533;
UPDATE players SET role_id = 5520 WHERE role_id = 5535;
UPDATE IGNORE hidden_roles SET role_id = 5520 WHERE role_id = 5535;
DELETE FROM roles WHERE id = 5535;
UPDATE players SET role_id = 5527 WHERE role_id = 5536;
UPDATE IGNORE hidden_roles SET role_id = 5527 WHERE role_id = 5536;
DELETE FROM roles WHERE id = 5536;
UPDATE players SET role_id = 5528 WHERE role_id = 5537;
UPDATE IGNORE hidden_roles SET role_id = 5528 WHERE role_id = 5537;
DELETE FROM roles WHERE id = 5537;
UPDATE players SET role_id = 5529 WHERE role_id = 5538;
UPDATE IGNORE hidden_roles SET role_id = 5529 WHERE role_id = 5538;
DELETE FROM roles WHERE id = 5538;
UPDATE players SET role_id = 5543 WHERE role_id = 15770;
UPDATE IGNORE hidden_roles SET role_id = 5543 WHERE role_id = 15770;
DELETE FROM roles WHERE id = 15770;
UPDATE players SET role_id = 5621 WHERE role_id = 5632;
UPDATE IGNORE hidden_roles SET role_id = 5621 WHERE role_id = 5632;
DELETE FROM roles WHERE id = 5632;
UPDATE players SET role_id = 5622 WHERE role_id = 5637;
UPDATE IGNORE hidden_roles SET role_id = 5622 WHERE role_id = 5637;
DELETE FROM roles WHERE id = 5637;
UPDATE players SET role_id = 5624 WHERE role_id = 5639;
UPDATE IGNORE hidden_roles SET role_id = 5624 WHERE role_id = 5639;
DELETE FROM roles WHERE id = 5639;
UPDATE players SET role_id = 5623 WHERE role_id = 5640;
UPDATE IGNORE hidden_roles SET role_id = 5623 WHERE role_id = 5640;
DELETE FROM roles WHERE id = 5640;
UPDATE players SET role_id = 5627 WHERE role_id = 5642;
UPDATE IGNORE hidden_roles SET role_id = 5627 WHERE role_id = 5642;
DELETE FROM roles WHERE id = 5642;
UPDATE players SET role_id = 5634 WHERE role_id = 5643;
UPDATE IGNORE hidden_roles SET role_id = 5634 WHERE role_id = 5643;
DELETE FROM roles WHERE id = 5643;
UPDATE players SET role_id = 5635 WHERE role_id = 5644;
UPDATE IGNORE hidden_roles SET role_id = 5635 WHERE role_id = 5644;
DELETE FROM roles WHERE id = 5644;
UPDATE players SET role_id = 5636 WHERE role_id = 5645;
UPDATE IGNORE hidden_roles SET role_id = 5636 WHERE role_id = 5645;
DELETE FROM roles WHERE id = 5645;
UPDATE players SET role_id = 5650 WHERE role_id = 15825;
UPDATE IGNORE hidden_roles SET role_id = 5650 WHERE role_id = 15825;
DELETE FROM roles WHERE id = 15825;
UPDATE players SET role_id = 10790 WHERE role_id = 10797;
UPDATE IGNORE hidden_roles SET role_id = 10790 WHERE role_id = 10797;
DELETE FROM roles WHERE id = 10797;
UPDATE players SET role_id = 10794 WHERE role_id = 10804;
UPDATE IGNORE hidden_roles SET role_id = 10794 WHERE role_id = 10804;
DELETE FROM roles WHERE id = 10804;
UPDATE players SET role_id = 10796 WHERE role_id = 10805;
UPDATE IGNORE hidden_roles SET role_id = 10796 WHERE role_id = 10805;
DELETE FROM roles WHERE id = 10805;
UPDATE players SET role_id = 10792 WHERE role_id = 10811;
UPDATE IGNORE hidden_roles SET role_id = 10792 WHERE role_id = 10811;
DELETE FROM roles WHERE id = 10811;
UPDATE players SET role_id = 10789 WHERE role_id = 10813;
UPDATE IGNORE hidden_roles SET role_id = 10789 WHERE role_id = 10813;
DELETE FROM roles WHERE id = 10813;
UPDATE players SET role_id = 10791 WHERE role_id = 10816;
UPDATE IGNORE hidden_roles SET role_id = 10791 WHERE role_id = 10816;
DELETE FROM roles WHERE id = 10816;
UPDATE players SET role_id = 10793 WHERE role_id = 10822;
UPDATE IGNORE hidden_roles SET role_id = 10793 WHERE role_id = 10822;
DELETE FROM roles WHERE id = 10822;
UPDATE players SET role_id = 10795 WHERE role_id = 10823;
UPDATE IGNORE hidden_roles SET role_id = 10795 WHERE role_id = 10823;
DELETE FROM roles WHERE id = 10823;
UPDATE players SET role_id = 11120 WHERE role_id = 15955;
UPDATE IGNORE hidden_roles SET role_id = 11120 WHERE role_id = 15955;
DELETE FROM roles WHERE id = 15955;
UPDATE players SET role_id = 11996 WHERE role_id = 12008;
UPDATE IGNORE hidden_roles SET role_id = 11996 WHERE role_id = 12008;
DELETE FROM roles WHERE id = 12008;
UPDATE players SET role_id = 11997 WHERE role_id = 12013;
UPDATE IGNORE hidden_roles SET role_id = 11997 WHERE role_id = 12013;
DELETE FROM roles WHERE id = 12013;
UPDATE players SET role_id = 11999 WHERE role_id = 12015;
UPDATE IGNORE hidden_roles SET role_id = 11999 WHERE role_id = 12015;
DELETE FROM roles WHERE id = 12015;
UPDATE players SET role_id = 11998 WHERE role_id = 12016;
UPDATE IGNORE hidden_roles SET role_id = 11998 WHERE role_id = 12016;
DELETE FROM roles WHERE id = 12016;
UPDATE players SET role_id = 12003 WHERE role_id = 12018;
UPDATE IGNORE hidden_roles SET role_id = 12003 WHERE role_id = 12018;
DELETE FROM roles WHERE id = 12018;
UPDATE players SET role_id = 12010 WHERE role_id = 12019;
UPDATE IGNORE hidden_roles SET role_id = 12010 WHERE role_id = 12019;
DELETE FROM roles WHERE id = 12019;
UPDATE players SET role_id = 12011 WHERE role_id = 12020;
UPDATE IGNORE hidden_roles SET role_id = 12011 WHERE role_id = 12020;
DELETE FROM roles WHERE id = 12020;
UPDATE players SET role_id = 12012 WHERE role_id = 12021;
UPDATE IGNORE hidden_roles SET role_id = 12012 WHERE role_id = 12021;
DELETE FROM roles WHERE id = 12021;
UPDATE players SET role_id = 12026 WHERE role_id = 15962;
UPDATE IGNORE hidden_roles SET role_id = 12026 WHERE role_id = 15962;
DELETE FROM roles WHERE id = 15962;
UPDATE players SET role_id = 12301 WHERE role_id = 15970;
UPDATE IGNORE hidden_roles SET role_id = 12301 WHERE role_id = 15970;
DELETE FROM roles WHERE id = 15970;
UPDATE players SET role_id = 12329 WHERE role_id = 15972;
UPDATE IGNORE hidden_roles SET role_id = 12329 WHERE role_id = 15972;
DELETE FROM roles WHERE id = 15972;
UPDATE players SET role_id = 13756 WHERE role_id = 13763;
UPDATE IGNORE hidden_roles SET role_id = 13756 WHERE role_id = 13763;
DELETE FROM roles WHERE id = 13763;
UPDATE players SET role_id = 14274 WHERE role_id = 16027;
UPDATE IGNORE hidden_roles SET role_id = 14274 WHERE role_id = 16027;
DELETE FROM roles WHERE id = 16027;
UPDATE players SET role_id = 905 WHERE role_id = 917;
UPDATE IGNORE hidden_roles SET role_id = 905 WHERE role_id = 917;
DELETE FROM roles WHERE id = 917;
UPDATE players SET role_id = 4579 WHERE role_id = 4591;
UPDATE IGNORE hidden_roles SET role_id = 4579 WHERE role_id = 4591;
DELETE FROM roles WHERE id = 4591;

UPDATE hidden_roles hr
LEFT JOIN hiddens h
ON h.id = hr.hidden_id
LEFT JOIN roles r
ON r.id = hr.role_id
SET r.name = CONCAT(r.name, ' Leader')
WHERE h.setup_id = 352
AND h.name LIKE '% Leader';

UPDATE hidden_roles hr
LEFT JOIN hiddens h
ON h.id = hr.hidden_id
LEFT JOIN roles r
ON r.id = hr.role_id
SET r.name = CONCAT(r.name, ' Leader')
WHERE h.setup_id = 367
AND h.name LIKE '% Leader';
UPDATE players SET role_id = 15942 WHERE role_id = 10901;
UPDATE IGNORE hidden_roles SET role_id = 15942 WHERE role_id = 10901;
DELETE FROM roles WHERE id = 10901;
UPDATE players SET role_id = 11085 WHERE role_id = 15950;
UPDATE IGNORE hidden_roles SET role_id = 11085 WHERE role_id = 15950;
DELETE FROM roles WHERE id = 15950;
DELETE FROM commands WHERE replay_id = 416 AND counter = 61;

ALTER TABLE players ADD CONSTRAINT players_name_replay_id UNIQUE(name, replay_id);

UPDATE roles SET name = TRIM(name);
UPDATE commands SET command = REPLACE(command, "  ", " ") WHERE replay_id = 75;

ALTER TABLE users ADD COLUMN is_guest BOOLEAN NOT NULL;
UPDATE users SET is_guest = true WHERE type = 'guest';
UPDATE users SET is_guest = false WHERE type != 'guest';
UPDATE users SET type = 'firebase' WHERE type = 'native' OR type = 'guest';

CREATE TABLE user_integrations(
    user_id BIGINT UNSIGNED NOT NULL,
    external_id VARCHAR(128) NOT NULL,
    integration_type VARCHAR(10) NOT NULL
);

INSERT INTO user_integrations (user_id, external_id, integration_type)
SELECT id, token, type FROM users;
ALTER TABLE users DROP COLUMN token;
ALTER TABLE users DROP COLUMN type;
ALTER TABLE user_integrations ADD CONSTRAINT external_id_integeration_type UNIQUE (external_id, integration_type);
ALTER TABLE user_integrations ADD CONSTRAINT user_integrations_on_user_delete FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;

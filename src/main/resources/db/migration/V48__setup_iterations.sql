CREATE TABLE setup_iterations(
    id SERIAL PRIMARY KEY,
    setup_id BIGINT UNSIGNED NOT NULL,
    player_count TINYINT UNSIGNED NOT NULL,
    CONSTRAINT `setup_iterations_on_setup_delete` FOREIGN KEY (`setup_id`) REFERENCES `setups` (`id`) ON DELETE CASCADE
);

CREATE TABLE setup_iteration_spawns(
    setup_iteration_id BIGINT UNSIGNED NOT NULL,
    setup_hidden_id BIGINT UNSIGNED NOT NULL,
    faction_role_id BIGINT UNSIGNED NOT NULL,
    CONSTRAINT `setup_iteration_spawns_on_setup_iteration_delete` FOREIGN KEY (`setup_iteration_id`) REFERENCES `setup_iterations` (`id`) ON DELETE CASCADE,
    CONSTRAINT `setup_iteration_spawns_setup_hidden_delete` FOREIGN KEY (`setup_hidden_id`) REFERENCES `setup_hiddens` (`id`)  ON DELETE CASCADE,
    CONSTRAINT `setup_iteration_spawns_faction_role_delete` FOREIGN KEY (`faction_role_id`) REFERENCES `faction_roles` (`id`)  ON DELETE CASCADE
);


ALTER TABLE discord_subscribers CHANGE COLUMN user_id discord_id VARCHAR(64) NOT NULL;
ALTER TABLE discord_subscribers ADD COLUMN user_id BIGINT UNSIGNED;

UPDATE discord_subscribers ds 
INNER JOIN user_integrations ui
ON ds.discord_id = ui.external_id
SET ds.user_id = ui.user_id;

DELETE FROM discord_subscribers WHERE user_id is null;
ALTER TABLE discord_subscribers CHANGE COLUMN user_id user_id BIGINT UNSIGNED NOT NULL;

ALTER TABLE discord_subscribers DROP COLUMN discord_id;

CREATE TABLE discord_subscribers2 (
  guild_id VARCHAR(64) NOT NULL,
  user_id BIGINT UNSIGNED NOT NULL,
  KEY discord_subscribers_on_user_delete (user_id),
  CONSTRAINT discord_subscribers_on_user_delete FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
ALTER IGNORE TABLE discord_subscribers2
ADD UNIQUE INDEX dsubscriber (user_id, guild_id);

INSERT INTO discord_subscribers2 (guild_id, user_id)
SELECT DISTINCT guild_id, user_id FROM discord_subscribers;

DROP TABLE discord_subscribers;
RENAME TABLE discord_subscribers2 TO discord_subscribers;
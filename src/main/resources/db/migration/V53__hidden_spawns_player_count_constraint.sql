ALTER TABLE hidden_spawns DROP FOREIGN KEY hidden_roles_on_hidden_delete;
ALTER TABLE hidden_spawns DROP FOREIGN KEY hidden_faction_roles_on_hidden_delete;
ALTER TABLE hidden_spawns DROP FOREIGN KEY hidden_faction_roles_on_faction_role_delete;

ALTER TABLE hidden_spawns DROP CONSTRAINT IF EXISTS hidden_id_faction_role_id;


ALTER TABLE hidden_spawns ADD CONSTRAINT hidden_spawns_on_hidden_delete FOREIGN KEY (hidden_id) REFERENCES hiddens(id) ON DELETE CASCADE;
ALTER TABLE hidden_spawns ADD CONSTRAINT hidden_spawns_on_faction_role_delete FOREIGN KEY (faction_role_id) REFERENCES faction_roles(id) ON DELETE CASCADE;
ALTER TABLE hidden_spawns ADD CONSTRAINT hidden_spawns_unique UNIQUE (hidden_id, faction_role_id, min_player_count, max_player_count);

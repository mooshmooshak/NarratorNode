ALTER TABLE players CHANGE COLUMN user_id user_id BIGINT UNSIGNED;

CREATE TABLE game_users(
    game_id BIGINT UNSIGNED NOT NULL,
    user_id BIGINT UNSIGNED NOT NULL,
    is_moderator BOOLEAN NOT NULL,
    is_exited BOOLEAN NOT NULL,
    UNIQUE KEY `game_users_unique` (`game_id`,`user_id`),
    CONSTRAINT `game_users_on_game_delete` FOREIGN KEY (`game_id`) REFERENCES `replays` (`id`) ON DELETE CASCADE
);

INSERT INTO game_users
    SELECT id, host_id, true, true FROM replays;
ALTER TABLE replays DROP COLUMN host_id;
ALTER TABLE replays ADD COLUMN is_finished BOOLEAN DEFAULT false;
UPDATE replays SET is_finished = true WHERE instance_id IS NULL;

INSERT IGNORE INTO game_users
    SELECT replay_id, user_id, false, true FROM players;
ALTER TABLE players DROP COLUMN is_exited;

DELETE FROM commands WHERE command LIKE 'alias %';

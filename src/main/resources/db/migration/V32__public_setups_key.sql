ALTER TABLE featured_setups ADD COLUMN `key` VARCHAR(10) NOT NULL;

ALTER TABLE featured_setups ADD CONSTRAINT `key` UNIQUE(`key`);

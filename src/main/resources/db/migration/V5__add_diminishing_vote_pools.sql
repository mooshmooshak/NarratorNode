INSERT INTO setup_int_modifiers (setup_id, name, value)
SELECT id, 'VOTE_SYSTEM', 1 from setups;

INSERT INTO setup_int_modifiers (setup_id, name, value)
SELECT id, 'TRIAL_LENGTH', 0 from setups;

INSERT INTO setup_bool_modifiers (setup_id, name, value)
SELECT id, 'SECRET_VOTES', 0 from setups;

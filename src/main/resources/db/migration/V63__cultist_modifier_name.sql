DELETE FROM faction_ability_bool_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_ability_int_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_ability_string_modifiers WHERE lower(name) = 'cultist_name';

DELETE FROM faction_bool_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_int_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_string_modifiers WHERE lower(name) = 'cultist_name';

DELETE FROM faction_role_ability_bool_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_role_ability_int_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_role_ability_string_modifiers WHERE lower(name) = 'cultist_name';

DELETE FROM faction_role_role_bool_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_role_role_int_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM faction_role_role_string_modifiers WHERE lower(name) = 'cultist_name';

DELETE FROM role_ability_bool_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM role_ability_int_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM role_ability_string_modifiers WHERE lower(name) = 'cultist_name';

DELETE FROM role_bool_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM role_int_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM role_string_modifiers WHERE lower(name) = 'cultist_name';

DELETE FROM setup_bool_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM setup_int_modifiers WHERE lower(name) = 'cultist_name';
DELETE FROM setup_string_modifiers WHERE lower(name) = 'cultist_name';

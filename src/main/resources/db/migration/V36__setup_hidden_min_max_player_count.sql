ALTER TABLE setup_hiddens ADD COLUMN min_player_count TINYINT UNSIGNED;
UPDATE setup_hiddens SET min_player_count = 0;
ALTER TABLE setup_hiddens CHANGE COLUMN min_player_count min_player_count TINYINT UNSIGNED NOT NULL;

ALTER TABLE setup_hiddens ADD COLUMN max_player_count TINYINT UNSIGNED;
UPDATE setup_hiddens SET max_player_count = 255;
ALTER TABLE setup_hiddens CHANGE COLUMN max_player_count max_player_count TINYINT UNSIGNED NOT NULL;

ALTER DATABASE narrator CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
ALTER TABLE lobby_chat CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE lobby_chat MODIFY text TEXT CHARSET utf8mb4;
ALTER TABLE lobby_chat CHANGE text text VARCHAR(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;

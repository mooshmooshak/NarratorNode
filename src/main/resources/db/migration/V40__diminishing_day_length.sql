UPDATE game_int_modifiers SET name = 'DAY_LENGTH_START' WHERE name = 'DAY_LENGTH';

INSERT INTO game_int_modifiers (game_id, name, value)
SELECT id, 'DAY_LENGTH_DECREASE', 0 FROM replays;

INSERT INTO game_int_modifiers (game_id, name, value)
SELECT game_id, 'DAY_LENGTH_MIN', value FROM game_int_modifiers
WHERE name = 'DAY_LENGTH_START';

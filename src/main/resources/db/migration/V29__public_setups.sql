DELETE FROM setups WHERE owner_id NOT IN (SELECT id FROM users) AND id NOT IN (SELECT id FROM replays);
UPDATE setups SET OWNER_ID = 2205 WHERE owner_id NOT IN (SELECT id FROM users);

CREATE TABLE featured_setups(
    setup_id BIGINT UNSIGNED NOT NULL,
    priority TINYINT UNSIGNED NOT NULL,
    CONSTRAINT `featured_setups_on_setup_delete`
        FOREIGN KEY (setup_id)
        REFERENCES setups (id)
        ON DELETE CASCADE
);

ALTER TABLE featured_setups ADD CONSTRAINT setup_id UNIQUE(setup_id);

ALTER TABLE setups ADD COLUMN slogan VARCHAR(45) AFTER name;
ALTER TABLE setups ADD COLUMN description VARCHAR(512) AFTER slogan;
ALTER TABLE setups ADD CONSTRAINT setups_owner_id FOREIGN KEY (owner_id) REFERENCES users(id);

DELETE FROM faction_enemies WHERE enemy_a NOT IN (SELECT id FROM factions);
DELETE FROM faction_enemies WHERE enemy_b NOT IN (SELECT id FROM factions);

ALTER TABLE faction_enemies ADD CONSTRAINT faction_enemies_enemy_a FOREIGN KEY (enemy_a) REFERENCES factions(id) ON DELETE CASCADE;
ALTER TABLE faction_enemies ADD CONSTRAINT faction_enemies_enemy_b FOREIGN KEY (enemy_b) REFERENCES factions(id) ON DELETE CASCADE;

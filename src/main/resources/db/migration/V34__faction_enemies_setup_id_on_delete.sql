UPDATE faction_enemies fe
INNER JOIN factions f ON fe.enemy_a = f.id
SET fe.setup_id = f.setup_id;

DELETE FROM faction_enemies WHERE setup_id NOT IN (
    SELECT id as setup_id FROM setups
);

ALTER TABLE faction_enemies CHANGE COLUMN setup_id setup_id BIGINT UNSIGNED NOT NULL;

ALTER TABLE faction_enemies
    ADD CONSTRAINT faction_enemies_setup_ref
        FOREIGN KEY (setup_id) REFERENCES setups(id) ON DELETE CASCADE;

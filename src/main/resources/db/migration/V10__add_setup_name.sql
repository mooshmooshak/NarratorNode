ALTER TABLE setups ADD COLUMN name VARCHAR(25);
UPDATE setups SET name = 'Legacy Setup';
ALTER TABLE setups CHANGE COLUMN name name VARCHAR(25) NOT NULL;

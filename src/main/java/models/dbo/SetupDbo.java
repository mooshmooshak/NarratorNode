package models.dbo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import models.enums.SetupModifierName;
import models.modifiers.Modifier;
import models.schemas.AbilityModifierSchema;
import models.schemas.AbilitySchema;
import models.schemas.FactionAbilityModifierSchema;
import models.schemas.FactionModifierSchema;
import models.schemas.FactionRoleSchema;
import models.schemas.FactionSchema;
import models.schemas.HiddenSchema;
import models.schemas.HiddenSpawnSchema;
import models.schemas.RoleModifierSchema;
import models.schemas.RoleSchema;
import models.schemas.SetupHiddenSchema;
import models.schemas.SetupSchema;
import models.schemas.SheriffCheckableSchema;

public class SetupDbo {

    public long id;
    public long ownerID;
    public String name;
    public String description;
    public String slogan;
    public boolean isEditable;

    public Collection<Modifier<SetupModifierName>> modifiers;

    public Set<FactionSchema> factionDbos;
    public Map<Long, Set<FactionModifierSchema>> factionModifiers;
    public Map<Long, Set<AbilitySchema>> factionAbilities;
    public Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> factionAbilityModifiers;
    public ArrayList<SheriffCheckableSchema> sheriffCheckables;
    public ArrayList<String[]> factionEnemies;

    public Set<RoleSchema> roleDbos;
    public Map<Long, Set<RoleModifierSchema>> roleModifiers;
    public Map<Long, LinkedHashMap<Long, AbilitySchema>> roleAbilities;
    public Map<Long, Map<Long, Set<AbilityModifierSchema>>> roleAbilityModifiers;

    public Set<FactionRoleSchema> factionRoleDbos;
    public Map<Long, Set<RoleModifierSchema>> factionRoleModifiers;
    public Map<Long, Map<Long, Set<AbilityModifierSchema>>> factionRoleAbilityModifiers;

    public Set<HiddenSchema> hiddenDbos;
    public Set<HiddenSpawnSchema> hiddenSpawns;

    public ArrayList<SetupHiddenSchema> setupHiddenDbos;

    public SetupDbo(long id, long ownerID, String name, String description, String slogan, boolean isEditable) {
        this.id = id;
        this.ownerID = ownerID;
        this.name = name;
        this.description = description;
        this.slogan = slogan;
        this.isEditable = isEditable;
    }

    public SetupDbo(SetupSchema schema) {
        this(schema.id, schema.ownerID, schema.name, schema.description, schema.slogan, schema.isEditable);
    }
}

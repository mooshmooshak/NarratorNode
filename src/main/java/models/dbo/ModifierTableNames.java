package models.dbo;

import java.util.Set;

import util.CollectionUtil;

public class ModifierTableNames {

    public String tableIntName;
    public String tableBoolName;
    public String tableStringName;

    public ModifierTableNames(String tableIntName, String tableBoolName, String tableStringName) {
        this.tableIntName = tableIntName;
        this.tableBoolName = tableBoolName;
        this.tableStringName = tableStringName;
    }

    public Set<String> getNames() {
        return CollectionUtil.toSet(this.tableBoolName, this.tableIntName, this.tableStringName);
    }
}

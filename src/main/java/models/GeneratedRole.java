package models;

import java.util.ArrayList;

public class GeneratedRole {
    public ArrayList<FactionRole> factionRoles = new ArrayList<>();
    public int index = 0;
    public SetupHidden setupHidden;

    public GeneratedRole(SetupHidden setupHidden) {
        this.setupHidden = setupHidden;
    }

    public boolean isAtEndOfOptions() {
        return index == factionRoles.size() - 1;
    }

    public FactionRole getFactionRole() {
        return factionRoles.get(index);
    }

    @Override
    public String toString() {
        return "\n" + getFactionRole().toString() + "(" + index + ")";
    }
}

package models;

import java.util.Set;

import game.logic.Game;
import models.idtypes.GameID;
import models.schemas.GameUserSchema;
import models.schemas.PlayerSchema;

public class OpenLobby {

    public Game game;
    public GameID gameID;
    public String lobbyID;
    public Set<PlayerSchema> players;
    public Set<GameUserSchema> users;

    public OpenLobby(GameID id, String lobbyID, Game game, Set<PlayerSchema> players, Set<GameUserSchema> users) {
        this.game = game;
        this.gameID = id;
        this.lobbyID = lobbyID;
        this.players = players;
        this.users = users;
    }
}

package models;

import java.util.Comparator;
import java.util.Set;

import game.abilities.Hidden;
import game.setups.Setup;
import models.schemas.SetupHiddenSchema;

public class SetupHidden {

    public Setup setup;
    public Hidden hidden;
    public FactionRole spawn;
    public long id;
    public boolean isExposed = false;
    public boolean mustSpawn = false;
    public int minPlayerCount;
    public int maxPlayerCount;

    public SetupHidden(Hidden hidden, SetupHiddenSchema schema) {
        this.id = schema.id;
        this.hidden = hidden;
        this.setup = hidden.setup;
        this.minPlayerCount = schema.minPlayerCount;
        this.maxPlayerCount = schema.maxPlayerCount;
        this.isExposed = schema.isExposed;
        this.mustSpawn = schema.mustSpawn;
    }

    public static Comparator<? super SetupHidden> RandomComparator() {
        return new Comparator<SetupHidden>() {
            @Override
            public int compare(SetupHidden r1, SetupHidden r2) {
                Set<String> color1 = r1.hidden.getColors();
                Set<String> color2 = r2.hidden.getColors();

                if(color1.size() != color2.size())
                    return color1.size() - color2.size();
                if(!color1.equals(color2))
                    return color1.iterator().next().compareTo(color2.iterator().next());

                boolean l1 = r1.hidden.size() < 1;
                boolean l2 = r2.hidden.size() < 1;
                if(l1 && !l2)
                    return 1;
                if(!l1 && l2)
                    return -1;

                return r1.hidden.getName().compareTo(r2.hidden.getName());
            }
        };
    }

    public static int difference(int i, int j) {
        if(i < j)
            return -1;
        if(i > j)
            return 1;

        return 0;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(this.id);
    }

    @Override
    public boolean equals(Object o) {
        if(o == this)
            return true;
        if(o == null)
            return false;
        if(o instanceof SetupHidden)
            return ((SetupHidden) o).id == this.id;
        return false;
    }

    @Override
    public String toString() {
        return hidden.toString();
    }
}

package models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import models.idtypes.GameID;
import nnode.Lobby;
import util.PlayerUtil;

public class GameLobby {
    public GameID id;
    public String lobbyID;
    public Game game;
    public Map<Long, GameUser> userMap = new HashMap<>();
    public PlayerUserIDMap playerUserIDMap;

    public GameLobby(Lobby lobby, long hostUserID, Player player) {
        this(lobby, hostUserID, Optional.of(player));
    }

    public GameLobby(Lobby lobby, long hostUserID, Optional<Player> player) {
        this.game = lobby.game;
        this.id = lobby.gameID;
        this.lobbyID = lobby.id;
        userMap.put(hostUserID, new GameUser(hostUserID, true, PlayerUtil.getDBID(player)));

        Map<Player, Optional<Long>> playerUserMap = new HashMap<>();
        if(player.isPresent())
            playerUserMap.put(player.get(), Optional.of(hostUserID));
        this.playerUserIDMap = new PlayerUserIDMap(playerUserMap);
    }

    public GameLobby(Lobby lobby, Set<Long> moderatorIDs, Map<Long, Optional<Player>> userIDPlayerMap) {
        this.game = lobby.game;
        this.id = lobby.gameID;
        this.lobbyID = lobby.id;

        Map<Player, Optional<Long>> playerUserIDMap = new HashMap<>();

        Set<Long> nonParticipatingModerators = new HashSet<>(moderatorIDs);
        Optional<Player> optPlayer;
        for(long userID: userIDPlayerMap.keySet()){
            nonParticipatingModerators.remove(userID);
            optPlayer = userIDPlayerMap.get(userID);
            userMap.put(userID, new GameUser(userID, moderatorIDs, PlayerUtil.getDBID(optPlayer)));

            if(optPlayer.isPresent())
                playerUserIDMap.put(optPlayer.get(), Optional.of(userID));
        }

        for(long moderatorID: nonParticipatingModerators)
            userMap.put(moderatorID, new GameUser(moderatorID, true, Optional.empty()));
        for(Player player: game.players){
            if(!playerUserIDMap.containsKey(player))
                playerUserIDMap.put(player, Optional.empty());
        }
        this.playerUserIDMap = new PlayerUserIDMap(playerUserIDMap);
    }
}

package models.controllerResponse;

import java.util.Set;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import models.serviceResponse.BotAddResponse;
import models.view.Jsonifiable;

public class BotAddResponseJson extends Jsonifiable {
    public Set<PlayerJson> players;
    public SetupJson setup;

    public BotAddResponseJson(BotAddResponse response) {
        this.players = PlayerJson.getSet(response.players);
        this.setup = new SetupJson(response.game);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject response = new JSONObject();
        response.put("setup", this.setup);
        response.put("players", this.players);
        return response;
    }

    public static Set<String> getPlayerNames(JSONObject json) throws JSONException {
        JSONArray players = json.getJSONArray("players");
        return PlayerJson.getPlayerNames(players);
    }
}

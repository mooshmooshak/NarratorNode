package models.controllerResponse;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.json_output.SetupJson;
import models.view.Jsonifiable;

public class PlayerDeleteResponseJson extends Jsonifiable {

    public SetupJson setup;

    public PlayerDeleteResponseJson(Game game) {
        this.setup = new SetupJson(game);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject response = new JSONObject();
        response.put("setup", setup.toJson());
        return response;
    }
}

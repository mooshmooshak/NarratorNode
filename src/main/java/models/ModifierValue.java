package models;

import game.logic.exceptions.NarratorException;

public class ModifierValue {
    public Object internalValue;

    public ModifierValue(int value) {
        this.internalValue = value;
    }

    public ModifierValue(boolean value) {
        this.internalValue = value;
    }

    public ModifierValue(Object value) {
        if(!(value instanceof Integer) && !(value instanceof Boolean) && !(value instanceof String))
            throw new NarratorException("Unsupported modifier value");
        this.internalValue = value;
    }

    public int getIntValue() {
        return (int) internalValue;
    }

    public boolean getBoolValue() {
        return (boolean) internalValue;
    }

    public String getStringValue() {
        return internalValue.toString();
    }

    @Override
    public String toString() {
        return internalValue.toString();
    }
}

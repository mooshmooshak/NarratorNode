package models.enums;

public enum FlavorAlias {
    GUN_ALIAS(SetupModifierName.GUN_NAME), VEST_ALIAS(SetupModifierName.VEST_NAME);

    public SetupModifierName modifierName;

    FlavorAlias(SetupModifierName name) {
        this.modifierName = name;
    }
}

package models.enums;

public enum VoteSystemTypes {

    PLURALITY(1, "plurality"), DIMINISHING_POOL(2, "diminishing_pool"), MULTI_VOTE_PLURALITY(3, "multi_plurality"),
    NO_ELIMINATION(4, "");

    public final int value;
    public final String name;

    VoteSystemTypes(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static VoteSystemTypes getByValue(int value) {
        switch (value) {
        case 1:
            return PLURALITY;
        case 2:
            return DIMINISHING_POOL;
        case 3:
            return MULTI_VOTE_PLURALITY;
        case 4:
            return NO_ELIMINATION;
        default:
            return null;
        }
    }
}

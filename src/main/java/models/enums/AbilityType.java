package models.enums;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import game.logic.exceptions.NarratorException;

public enum AbilityType {
    // Enums need to not be all caps, because the names match the class names
    // Agent here, matches Agent.class

    Agent("Stalk", "Agent"), Amnesiac("Remember", "Amnesiac"), Architect("Build", "Architect"),
    Armorsmith("Armor", "Armorsmith"), ArmsDetector("Detect", "ArmsDetector"), Assassin("Assassinate", "Assassin"),
    Blackmailer("Blackmail", "Blackmailer"), Blacksmith("Craft", "Blacksmith"), Block("Block", "Stripper"),
    Bodyguard("Guard", "Bodyguard"), BreadAbility("Bread", "BreadAbility"), Burn("Burn", "Burn"),
    Clubber("Purge", "Clubber"), Commuter("Commute", "Commuter"), Coroner("Autopsy", "Coroner"),
    Coward("Hide", "Coward"), CultLeader("Convert", "CultLeader"), Detective("Follow", "Detective"),
    Disfranchise("Disfranchise", "Disfranchise"), Disguiser("Disguise", "Disguiser"), Doctor("Heal", "Doctor"),
    Douse("Douse", "Douse"), Driver("Swap", "Driver"), DrugDealer("Drug", "DrugDealer"),
    Elector("Votesteal", "Elector"), ElectroManiac("Charge", "ElectroManiac"), Enforcer("Enforce", "Enforcer"),
    FactionKill("Kill", "FactionKill"), Framer("Frame", "Framer"), GraveDigger("Dig", "GraveDigger"),
    Gunsmith("Gun", "Gunsmith"), Interceptor("Intercept", "Interceptor"), Investigator("Investigate", "Investigator"),
    JailExecute("Execute", "Jailor"), Janitor("Clean", "Janitor"), Jester("Annoy", "Jester"), Joker("Bounty", "Joker"),
    Lookout("Watch", "Lookout"), Marshall("MarshallLaw", "Marshall"), MasonLeader("Recruit", "MasonLeader"),
    MassMurderer("Spree", "MassMurderer"), Mayor("Reveal", "Mayor"), Operator("Switch", "Operator"),
    ParityCheck("Compare", "ParityCheck"), Poisoner("Poison", "Poisoner"), ProxyKill("Murder", "ProxyKill"),
    Scout("Peek", "Scout"), SerialKiller("Stab", "SerialKiller"), Sheriff("Check", "Sheriff"),
    Silence("Silence", "Silence"), Snitch("Plan", "Snitch"), Spy("SpyAbility", "Spy"), Survivor("Vest", "Survivor"),
    Tailor("Suit", "Tailor"), Thief("pick", "Thief"), Undouse("Undouse", "Undouse"),
    Ventriloquist("Puppet", "Ventriloquist"), Veteran("Alert", "Veteran"), Vigilante("Shoot", "Vigilante"),
    Vote("Vote", "Vote"), Visit("Visit", "Visit"), Witch("Control", "Witch"),

    CultInvitation("invitation", null), FactionSend("Send", null), JailCreate("Jail", null), Punch("Punch", null),
    Puppet("DayPuppet", null),

    Baker("Baker"), Bomb("Bomb"), Bulletproof("Bulletproof"), Cultist("Cultist"), Executioner("Executioner"),
    Ghost("Ghost"), Godfather("Godfather"), Goon("Goon"), Infiltrator("Infiltrator"), Mason("Mason"), Miller("Miller"),
    Sleepwalker("Sleepwalker"), TeamTakedown("TeamTakedown");

    public final String command;
    public final String dbName;

    private AbilityType(String dbName) {
        this.command = null;
        this.dbName = dbName;
    }

    private AbilityType(String command, String dbName) {
        this.command = command;
        this.dbName = dbName;
    }

    public static Set<String> toCommandStringSet(Collection<AbilityType> abilityTypes) {
        Set<String> abilitySet = new HashSet<>();
        for(AbilityType abilityType: abilityTypes){
            abilitySet.add(abilityType.command);
        }
        return abilitySet;
    }

    public static AbilityType narratorParse(String string) {
        try{
            return valueOf(string);
        }catch(IllegalArgumentException e){
            throw new NarratorException("No ability with that name.");
        }
    }
}

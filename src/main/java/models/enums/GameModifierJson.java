package models.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import json.JSONException;
import json.JSONObject;
import models.GameModifiers;
import models.ModifierValue;
import models.modifiers.GameModifier;
import models.modifiers.Modifier;
import models.view.Jsonifiable;

public class GameModifierJson extends Jsonifiable {

    public GameModifierName name;
    public ModifierValue value;

    public GameModifierJson(GameModifierName name, ModifierValue value) {
        this.name = name;
        this.value = value;
    }

    public GameModifierJson(Modifier<GameModifierName> modifier) {
        this(modifier.name, modifier.value);
    }

    public static Map<String, GameModifierJson> getMap(GameModifiers<GameModifierName> modifiers) {
        Map<String, GameModifierJson> modifierViews = new HashMap<>();
        Object value;
        ModifierValue modifierValue;
        for(GameModifierName modifierName: GameModifierName.values()){
            value = modifiers.get(modifierName);
            modifierValue = new ModifierValue(value);
            modifierViews.put(modifierName.toString(), new GameModifierJson(modifierName, modifierValue));
        }
        return modifierViews;
    }

    public static Set<GameModifierJson> getSet(Set<GameModifier> gameModifiers) {
        Map<GameModifierName, GameModifier> map = new HashMap<>();
        for(GameModifier gameModifier: gameModifiers)
            map.put(gameModifier.name, gameModifier);

        Set<GameModifierJson> modifierViews = new HashSet<>();
        GameModifier gameModifier;
        ModifierValue value;
        for(GameModifierName name: GameModifierName.values()){
            gameModifier = map.get(name);
            if(gameModifier == null)
                value = new ModifierValue(name.defaultValue);
            else
                value = gameModifier.value;
            modifierViews.add(new GameModifierJson(name, value));
        }
        return modifierViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jSetupModifier = new JSONObject();
        jSetupModifier.put("name", this.name.toString());
        jSetupModifier.put("value", this.value.internalValue);
        return jSetupModifier;

    }
}

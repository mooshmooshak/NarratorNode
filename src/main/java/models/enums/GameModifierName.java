package models.enums;

import game.logic.support.rules.ModifierName;

public enum GameModifierName implements ModifierName {

    CHAT_ROLES(true),

    AUTO_PARITY(false), CHAT_RATE(0),

    DAY_LENGTH_START(240), DAY_LENGTH_DECREASE(0), DAY_LENGTH_MIN(240),

    NIGHT_LENGTH(180), TRIAL_LENGTH(0), DISCUSSION_LENGTH(0), ROLE_PICKING_LENGTH(30),

    VOTE_SYSTEM(VoteSystemTypes.PLURALITY.value), OMNISCIENT_DEAD(true);

    public final Object defaultValue;

    GameModifierName(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String getActiveFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getInactiveFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getUnlimitedFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getLimitedFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getUnsureFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return this.defaultValue;
    }

    @Override
    public String getLabelFormat() {
        // TODO Auto-generated method stub
        return null;
    }
}

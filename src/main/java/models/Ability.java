package models;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.GameAbility;
import game.abilities.util.AbilityUtil;
import game.logic.Game;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import models.schemas.AbilitySchema;

public class Ability {
    public AbilityType type;
    public Modifiers<AbilityModifierName> modifiers;
    public long id;
    public Setup setup;

    public Ability(Setup setup, AbilitySchema schema) {
        this.setup = setup;
        this.id = schema.id;
        this.type = schema.type;
        this.modifiers = new Modifiers<>();
    }

    public GameAbility getGameAbility() {
        return getGameAbility(new Modifiers<>());
    }

    public GameAbility getGameAbility(Modifiers<AbilityModifierName> modifiers) {
        return AbilityUtil.CREATOR(this.type, setup, modifiers);
    }

    public GameAbility getGameAbility(Game game, Modifiers<AbilityModifierName> modifiers) {
        return AbilityUtil.CREATOR(this.type, game, modifiers);
    }

    public GameAbility getGameAbility(Optional<Game> game, Modifiers<AbilityModifierName> modifiers) {
        if(game.isPresent())
            return AbilityUtil.CREATOR(this.type, game.get(), modifiers);
        return AbilityUtil.CREATOR(this.type, setup, modifiers);
    }

    public boolean isFactionUniqueAbility() {
        return getGameAbility().isFactionUniqueAbility();
    }

    public boolean isRoleUniqueAbility() {
        return getGameAbility().isRoleUniqueAbility();
    }

    public String getAbilityDescription(Optional<Game> game, Modifiers<AbilityModifierName> modifiers) {
        return getGameAbility(game, modifiers).getAbilityDescription(game, setup);
    }

    public List<String> getPublicDescription(Optional<Game> game, Optional<String> color, String roleName,
            Set<Ability> abilities, Modifiers<AbilityModifierName> modifiers) {
        return getGameAbility(game, modifiers).getPublicDescription(game, roleName, color, abilities);
    }

    public List<AbilityModifierName> getAbilityModifiers() {
        return getGameAbility().getAbilityModifiers();
    }
}

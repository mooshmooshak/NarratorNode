package models.events;

import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.PlayerUserIDMap;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;

public class GameEndEvent {

    public static JSONObject request(GameLobby gameLobby) throws JSONException {
        Set<Long> playerUserIDs = gameLobby.playerUserIDMap.getUserIDs();
        JSONObject request = new JSONObject();
        request.put("event", "gameEnd");
        request.put("gameID", gameLobby.id);
        request.put("joinID", gameLobby.lobbyID);
        request.put("phase", new PhaseJson(gameLobby.game));
        request.put("userIDs", playerUserIDs);
        request.put("winners", getWinners(gameLobby.game, gameLobby.playerUserIDMap));
        request.put("winnerUserIDs", getWinnerIDs(gameLobby.playerUserIDMap));
        return request;
    }

    private static JSONArray getWinnerIDs(PlayerUserIDMap playerUserMap) {
        JSONArray winnerIDs = new JSONArray();

        for(Player player: playerUserMap.getPlayers()){
            if(player.isWinner() && playerUserMap.hasUserID(player))
                winnerIDs.put(playerUserMap.getUserID(player).get());
        }
        return winnerIDs;
    }

    private static JSONArray getWinners(Game game, PlayerUserIDMap playerUserMap) throws JSONException {
        JSONArray winners = new JSONArray();
        for(Player player: game.players){
            if(playerUserMap.hasUserID(player) && player.isWinner())
                winners.put(new PlayerJson(player, playerUserMap.getUserID(player)));
        }
        return winners;
    }
}

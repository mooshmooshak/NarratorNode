package models.events;

import java.util.Set;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.idtypes.PlayerDBID;
import models.json_output.SetupJson;
import models.view.Jsonifiable;

public class PlayerRemovedEvent extends Jsonifiable {

    private Set<Long> moderatorIDs;
    private String lobbyID;
    private PlayerDBID playerID;
    private String playerName;
    private SetupJson setup;
    private long userID;

    public PlayerRemovedEvent(long userID, PlayerDBID playerID, String playerName, Game game, String lobbyID,
            Set<Long> moderatorIDs) {
        this.lobbyID = lobbyID;
        this.moderatorIDs = moderatorIDs;
        this.playerName = playerName;
        this.playerID = playerID;
        this.setup = new SetupJson(game);
        this.userID = userID;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "playerRemove");
        request.put("joinID", this.lobbyID);
        request.put("moderatorIDs", this.moderatorIDs);
        request.put("playerID", this.playerID);
        request.put("playerName", this.playerName);
        request.put("setup", this.setup);
        request.put("userID", userID);
        return request;
    }
}

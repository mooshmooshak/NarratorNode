package models.events;

import game.event.SelectionMessage;
import game.logic.Player;
import json.JSONException;
import json.JSONObject;

public class ActionSubmitEvent {

    public static JSONObject request(Player player, String joinID, long userID, SelectionMessage selectionMessage)
            throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "actionSubmit");
        request.put("joinID", joinID);
        request.put("userID", userID);
        request.put("feedbackText", selectionMessage.access(player));
        return request;
    }

}

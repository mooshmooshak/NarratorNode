package models.events;

import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.view.Jsonifiable;

public class LobbyDeleteEvent extends Jsonifiable {

    private String lobbyID;
    private GameID gameID;

    public LobbyDeleteEvent(String lobbyID, GameID gameID) {
        this.lobbyID = lobbyID;
        this.gameID = gameID;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "lobbyDelete");
        request.put("joinID", this.lobbyID);
        request.put("gameID", this.gameID);
        request.put("replayID", this.gameID);
        return request;
    }
}

package models.events;

import game.abilities.Executioner;
import game.abilities.Jester;
import game.logic.GameFaction;
import game.logic.Player;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.User;
import models.json_output.profile.ProfileAllyJson;
import models.json_output.profile.ProfileJson;
import nnode.Lobby;
import nnode.StateObject;

public class RoleCardUpdate {
    public static JSONObject request(Lobby game, User user, Player player) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("allies", ProfileAllyJson.getSet(player));
        request.put("event", "roleCardUpdate");
        request.put("joinID", game.id);
        request.put("profile", new ProfileJson(game.gameID, user, player));
        request.put("userID", user.id);
        request.put(StateObject.isDay, player.game.isDay());
        request.put(StateObject.dayNumber, player.game.getDayNumber());

        request.put(StateObject.roleName, player.gameRole.factionRole.getPlayerVisibleFactionRole().getName());
        request.put(StateObject.teamName, player.getGameFaction().getName());
        request.put(StateObject.color, player.getGameFaction().getColor());

        if(player.getGameFaction().hasEnemies()){
            JSONArray enemies = new JSONArray();
            for(GameFaction gameFaction: player.getGameFaction().getEnemies()){
                enemies.put(gameFaction.getName());
            }
            request.put(StateObject.enemy, enemies);
        }else if(player.hasAbility(Jester.abilityType)){
            request.put(StateObject.enemy, "To win, you must get yourself publicly executed.");
        }else if(player.hasAbility(Executioner.abilityType)){
            request.put(StateObject.enemy, Executioner.GetTarget(player).getName());
        }else{
            request.put(StateObject.enemy, "You must survive until the end of the game.");
        }
        return request;
    }
}

package models.events;

import java.util.ArrayList;
import java.util.Map;

import game.event.SnitchAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.PlayerUserIDMap;
import models.enums.GameModifierName;
import models.idtypes.GameID;
import models.json_output.ChatsJson;
import models.json_output.GameUserJson;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import models.json_output.profile.ProfileJson;
import nnode.StateObject;
import services.VoteService;
import util.game.PhaseUtil;

public class DayStartEvent {
    public static JSONObject request(GameLobby gameLobby, PlayerList newDead,
            ArrayList<SnitchAnnouncement> sAnnoucements, Map<Long, String> userIDtoNameMap) throws JSONException {
        Game game = gameLobby.game;
        GameID gameID = gameLobby.id;
        PlayerUserIDMap playerUserMap = gameLobby.playerUserIDMap;

        JSONObject request = new JSONObject();
        request.put("event", "dayStart");
        request.put("joinID", gameLobby.lobbyID);
        request.put("gameID", gameLobby.id);
        request.put(StateObject.dayNumber, game.getDayNumber());
        request.put(StateObject.isDay, game.isDay());
        request.put("discussionLength", game.getInt(GameModifierName.DISCUSSION_LENGTH));
        request.put("dayLength", PhaseUtil.getDayLength(game));
        request.put("firstPhase", game.isFirstPhase());
        request.put("gameID", gameID);
        request.put("replayID", gameID);
        request.put("chats", ChatsJson.toJson(game, playerUserMap));
        request.put("phase", new PhaseJson(game));
        request.put("players", PlayerJson.getSet(game.players, playerUserMap));
        request.put("profiles", ProfileJson.toSet(gameID, playerUserMap, userIDtoNameMap));
        request.put("setup", new SetupJson(game));
        request.put("users", GameUserJson.getSet(gameLobby.userMap.values()));
        request.put("voteInfo", VoteService.getVotes(game));

        request.put("snitchReveals", addSnitchAnnouncements(gameLobby, sAnnoucements));
        if(newDead != null && !newDead.isEmpty()){
            JSONArray jDeadList = new JSONArray();
            JSONObject jDead;
            JSONArray deathType;
            for(Player dead: newDead){
                jDead = new JSONObject();
                jDead.put(StateObject.playerName, dead.getName());
                if(gameLobby.playerUserIDMap.hasUserID(dead))
                    jDead.put(StateObject.userID, gameLobby.playerUserIDMap.getUserID(dead).get());
                if(!dead.getDeathType().isHidden()){
                    deathType = new JSONArray();
                    for(String[] dType: dead.getDeathType().attacks){
                        deathType.put(dType[1]);
                    }
                    jDead.put("deathType", deathType);
                    jDead.put(StateObject.roleName, dead.getGraveyardRoleName());
                    jDead.put(StateObject.teamName, dead.getGraveyardTeamName());
                    jDead.put(StateObject.color, dead.getGraveyardColor());
                    jDead.put("lastWill", dead.getLastWill(null));
                }
                jDeadList.put(jDead);
            }
            request.put(StateObject.graveYard, jDeadList);
        }

        return request;
    }

    private static JSONArray addSnitchAnnouncements(GameLobby game, ArrayList<SnitchAnnouncement> sAnnoucements)
            throws JSONException {
        JSONArray snitchAnnouncements = new JSONArray();
        JSONObject jo;
        for(SnitchAnnouncement sn: sAnnoucements){
            jo = new JSONObject();
            if(game.playerUserIDMap.hasUserID(sn.revealed))
                jo.put("userID", game.playerUserIDMap.getUserID(sn.revealed).get());
            jo.put(StateObject.teamName, sn.teamName);
            jo.put(StateObject.roleName, sn.roleName);
            snitchAnnouncements.put(jo);
        }
        return snitchAnnouncements;
    }
}

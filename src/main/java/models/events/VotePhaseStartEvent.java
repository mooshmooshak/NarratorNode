package models.events;

import java.util.Map;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.json_output.GameUserJson;
import models.json_output.PhaseJson;
import models.json_output.profile.ProfileJson;
import services.VoteService;
import util.game.PhaseUtil;

public class VotePhaseStartEvent {
    public static JSONObject request(GameLobby gameLobby, Map<Long, String> userIDtoNameMap) throws JSONException {
        Game game = gameLobby.game;
        JSONObject request = new JSONObject();
        request.put("dayLength", PhaseUtil.getDayLength(gameLobby.game));
        request.put("event", "votePhaseStart");
        request.put("gameID", gameLobby.id);
        request.put("isFirstPhase", gameLobby.game.isFirstPhase());
        request.put("joinID", gameLobby.lobbyID);
        request.put("phase", new PhaseJson(game));
        request.put("profiles", ProfileJson.toSet(gameLobby.id, gameLobby.playerUserIDMap, userIDtoNameMap));
        request.put("voteInfo", VoteService.getVotes(game));
        request.put("users", GameUserJson.getSet(gameLobby.userMap.values()));

        return request;
    }
}

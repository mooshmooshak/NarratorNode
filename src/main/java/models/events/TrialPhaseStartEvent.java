package models.events;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.enums.GameModifierName;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;

public class TrialPhaseStartEvent {

    public static JSONObject request(GameLobby gameLobby, Player trialedPerson) throws JSONException {
        Game game = gameLobby.game;
        PlayerList playersOnTrial = game.voteSystem.getPlayersOnTrial();
        JSONObject request = new JSONObject();
        request.put("event", "trialStart");
        request.put("gameID", gameLobby.id);
        request.put("joinID", gameLobby.lobbyID);
        request.put("phase", new PhaseJson(game));
        request.put("trialLength", game.getInt(GameModifierName.TRIAL_LENGTH));
        request.put("trialedUser", new PlayerJson(trialedPerson, gameLobby.playerUserIDMap.getUserID(trialedPerson)));
        request.put("trialedUsers", PlayerJson.getSet(playersOnTrial, gameLobby.playerUserIDMap));
        return request;
    }

}

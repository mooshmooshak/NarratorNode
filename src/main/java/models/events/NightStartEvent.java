package models.events;

import java.util.Map;

import game.logic.Game;
import game.logic.PlayerList;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.enums.GameModifierName;
import models.json_output.ChatsJson;
import models.json_output.DeadPlayerJson;
import models.json_output.GameUserJson;
import models.json_output.PhaseJson;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import models.json_output.profile.ProfileJson;
import nnode.StateObject;
import services.VoteService;

public class NightStartEvent {
    public static JSONObject request(GameLobby gameLobby, PlayerList votedOutPlayers, PlayerList poisonedPlayers,
            Map<Long, String> userIDtoNameMap) throws JSONException {
        Game game = gameLobby.game;
        PlayerList deadPlayers = new PlayerList();
        if(votedOutPlayers != null)
            deadPlayers.add(votedOutPlayers);
        if(poisonedPlayers != null)
            deadPlayers.add(poisonedPlayers);

        JSONObject request = new JSONObject();

        request.put("chats", ChatsJson.toJson(game, gameLobby.playerUserIDMap));
        request.put("event", "nightStart");
        request.put("gameID", gameLobby.id);
        request.put("joinID", gameLobby.lobbyID);
        request.put("length", gameLobby.game.getInt(GameModifierName.NIGHT_LENGTH));
        request.put("nightStart", 1);
        request.put("players", PlayerJson.getSet(gameLobby.game.players, gameLobby.playerUserIDMap));
        request.put("phase", new PhaseJson(game));
        request.put("profiles", ProfileJson.toSet(gameLobby.id, gameLobby.playerUserIDMap, userIDtoNameMap));
        request.put("replayID", gameLobby.id);
        request.put("setup", new SetupJson(gameLobby.game));
        request.put("users", GameUserJson.getSet(gameLobby.userMap.values()));
        request.put("voteInfo", VoteService.getVotes(game));
        request.put(StateObject.graveYard, DeadPlayerJson.getSet(deadPlayers, gameLobby.playerUserIDMap));

        return request;
    }
}

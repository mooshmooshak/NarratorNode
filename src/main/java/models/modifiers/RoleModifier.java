package models.modifiers;

import java.time.Instant;

import models.ModifierValue;
import models.enums.RoleModifierName;
import models.schemas.RoleModifierSchema;

public class RoleModifier extends Modifier<RoleModifierName> {

    public RoleModifier(RoleModifierName name, ModifierValue value, int minPlayerCount, int maxPlayerCount,
            Instant upsertedAt) {
        super(name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public RoleModifier(RoleModifierName name, Object internalValue, int minPlayerCount, int maxPlayerCount,
            Instant upsertedAt) {
        super(name, new ModifierValue(internalValue), minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public RoleModifier(RoleModifierSchema roleModifier) {
        super(roleModifier);
    }

    public RoleModifier(Modifier<RoleModifierName> modifier) {
        super(modifier);
    }

}

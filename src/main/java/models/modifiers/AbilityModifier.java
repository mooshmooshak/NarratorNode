package models.modifiers;

import java.time.Instant;

import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.schemas.AbilityModifierSchema;
import models.schemas.FactionAbilityModifierSchema;

public class AbilityModifier extends Modifier<AbilityModifierName> {

    public AbilityModifier(AbilityModifierName modifierName, ModifierValue modifierValue, int minPlayerCount,
            int maxPlayerCount, Instant upsertedAt) {
        super(modifierName, modifierValue, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public AbilityModifier(AbilityModifierName modifierName, Object internalValue, int minPlayerCount,
            int maxPlayerCount, Instant upsertedAt) {
        this(modifierName, new ModifierValue(internalValue), minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public AbilityModifier(AbilityModifierSchema abilityModifier) {
        super(abilityModifier);
    }

    public AbilityModifier(FactionAbilityModifierSchema modifier) {
        super(modifier);
    }

}

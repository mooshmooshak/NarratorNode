package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import game.abilities.GameAbility;
import game.logic.Game;
import game.logic.GameRole;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import util.game.GameUtil;
import util.game.ModifierUtil;

public class FactionRole {

    public Modifiers<RoleModifierName> roleModifiers;
    public Map<AbilityType, Modifiers<AbilityModifierName>> abilityModifiers;
    public Faction faction;
    public Role role;
    public long id;
    public FactionRole receivedRole;

    public FactionRole(long id, Faction faction, Role role) {
        this.id = id;
        this.faction = faction;
        this.role = role;
        this.roleModifiers = new Modifiers<>();
        this.abilityModifiers = new HashMap<>();
    }

    @Override
    public String toString() {
        return faction.getName() + " " + role.getName();
    }

    public String getColor() {
        return faction.getColor();
    }

    public String getName() {
        return role.getName();
    }

    public boolean isRoleUnique(int playerCount) {
        return role.isRoleUnique(playerCount);
    }

    public boolean isFactionUnique(int playerCount) {
        return roleModifiers.getBoolean(RoleModifierName.UNIQUE, playerCount);
    }

    public boolean isUnique(int playerCount) {
        return isFactionUnique(playerCount) || isRoleUnique(playerCount);
    }

    // is this used?
    public ArrayList<String> getPublicDescription(Optional<Game> game) {
        ArrayList<String> ruleTexts = new ArrayList<>();

        Modifiers<RoleModifierName> roleModifiers = ModifierUtil.mergeModifiers(this.role.modifiers,
                this.roleModifiers);
        Map<AbilityType, Modifiers<AbilityModifierName>> abilityModifiersMap = ModifierUtil
                .mergeAbilityModifiers(this.role.abilityMap, this.abilityModifiers);

        GameAbility gameAbility;
        Modifiers<AbilityModifierName> abilityModifiers;
        ArrayList<String> publicDescription;
        for(Ability a: role.abilityMap.values()){
            abilityModifiers = abilityModifiersMap.getOrDefault(a.type, new Modifiers<>());
            gameAbility = a.getGameAbility(game, abilityModifiers);
            publicDescription = gameAbility.getPublicDescription(game, this.getName(), Optional.of(this.getColor()),
                    this.role.getAbilities());
            ruleTexts.addAll(publicDescription);
        }

        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        ruleTexts.addAll(GameRole.GetModifierRuleTexts(roleModifiers, playerCount));

        return ruleTexts;
    }

    public boolean getModifierValue(Game game, AbilityType abilityType, AbilityModifierName modifierName,
            boolean defaultValue) {
        Optional<Modifier<AbilityModifierName>> modifier = getModifier(game, abilityType, modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getBoolValue();
        return defaultValue;
    }

    public int getModifierValue(Game game, AbilityType abilityType, AbilityModifierName modifierName,
            int defaultValue) {
        Optional<Modifier<AbilityModifierName>> modifier = getModifier(game, abilityType, modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getIntValue();
        return defaultValue;
    }

    private Optional<Modifier<AbilityModifierName>> getModifier(Game game, AbilityType abilitytype,
            AbilityModifierName modifier) {
        int playerCount = game.players.size();
        Modifiers<AbilityModifierName> modifiers = this.abilityModifiers.get(abilitytype);
        if(modifiers.hasKey(modifier))
            return modifiers.getModifier(modifier, game.players.size());

        if(!role.hasAbility(abilitytype))
            return Optional.empty();

        Ability ability = role.getAbility(abilitytype);
        return ability.modifiers.getModifier(modifier, playerCount);
    }

    public FactionRole getPlayerVisibleFactionRole() {
        if(this.receivedRole == null)
            return this;
        return this.receivedRole;
    }

    public boolean allowsForFriendlyFire(Game game) {
        Modifiers<AbilityModifierName> modifiers;
        for(Ability ability: this.role.abilityMap.values()){
            modifiers = getAbilityModifiers(ability.type, game.players.size());
            if(ability.getGameAbility(game, modifiers).allowsForFriendlyFire())
                return true;
        }
        return false;
    }

    public boolean isConvertable(Game game) {
        for(Ability ability: this.role.abilityMap.values()){
            if(!ability.getGameAbility(game, this.getAbilityModifiers(ability.type)).isRecruitable())
                return false;
        }
        Modifiers<RoleModifierName> roleModifiers = ModifierUtil.mergeModifiers(this.role.modifiers,
                this.roleModifiers);
        return !roleModifiers.getBoolean(RoleModifierName.UNCONVERTABLE, game);
    }

    public boolean hasMembersThatAffectSending() {
        for(Ability ability: this.role.abilityMap.values()){
            // doesn't need game reference
            if(ability.getGameAbility(getAbilityModifiers(ability.type)).affectsSending())
                return true;
        }
        return false;
    }

    public Modifiers<AbilityModifierName> getAbilityModifiers(AbilityType abilityType, int playerCount) {
        return ModifierUtil.mergeModifiers(playerCount, role.getAbility(abilityType).modifiers,
                this.abilityModifiers.get(abilityType));
    }

    public Modifiers<AbilityModifierName> getAbilityModifiers(AbilityType abilityType) {
        return ModifierUtil.mergeModifiers(role.getAbility(abilityType).modifiers,
                this.abilityModifiers.get(abilityType));
    }
}

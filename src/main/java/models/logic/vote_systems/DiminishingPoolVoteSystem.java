package models.logic.vote_systems;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import game.event.DeathAnnouncement;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Random;
import game.logic.support.action.Action;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import nnode.Config;
import util.Util;

public class DiminishingPoolVoteSystem extends VoteSystem {

    private PlayerList playersOnTrial;
    private PlayerList alreadyTrialedPlayers = new PlayerList();
    private PlayerList eligibileTargets;
    private int trialIndex = -1;
    private int deadlockCount = 3;

    public DiminishingPoolVoteSystem(Game narrator) {
        super(narrator);
        reset();
    }

    @Override
    public Player getPlayerOnTrial() {
        if(playersOnTrial == null || playersOnTrial.isEmpty())
            return null;
        return playersOnTrial.get(trialIndex);
    }

    @Override
    public PlayerList getPlayersOnTrial() {
        return playersOnTrial.copy();
    }

    @Override
    public void endDiscussion() {
        super.endDiscussion();
        this.clearAllVotes();
    }

    @Override
    public void endPhase(double timeLeft) {
        if(game.phase == GamePhase.DISCUSSION_PHASE || game.phase == GamePhase.TRIAL_PHASE){
            trialIndex++;
            if(trialIndex == 0){
                this.endDiscussion();
            }else if(trialIndex == playersOnTrial.size()){
                this.endDiscussion();
                this.alreadyTrialedPlayers.add(playersOnTrial);
                if(alreadyTrialedPlayers.size() == 1 && playersOnTrial.size() == 1){
                    playersOnTrial = game.getLivePlayers();
                }else{
                    playersOnTrial = alreadyTrialedPlayers.copy();
                }
                if(game.getBool(SetupModifierName.SKIP_VOTE))
                    playersOnTrial.add(game.skipper);
                this.setElligibleVoteTargets(playersOnTrial);
                playersOnTrial = null;
            }else{
                startTrial(playersOnTrial.get(trialIndex), timeLeft);
            }
        }else{
            playersOnTrial = new PlayerList();
            PlayerList voters;
            for(Player voted: this.eligibileTargets){
                voters = this.getVotersOfPlayer(voted);
                if(voted != game.skipper && voters != null && voters.size() >= 2
                        && !alreadyTrialedPlayers.contains(voted)){
                    playersOnTrial.add(voted);
                }
            }

            if(playersOnTrial.isEmpty() || game.getInt(GameModifierName.TRIAL_LENGTH) == 0){
                killHighestVotedPlayer();
            }else{
                if(playersOnTrial.size() > 1)
                    playersOnTrial.shuffle(new Random(), "Shuffling trialed players");
                this.trialIndex = 0;
                startTrial(playersOnTrial.get(0), timeLeft);
            }
        }
    }

    private void endGameInDraw() {
        game.modkill(game.getLivePlayers(), 0);
    }

    private boolean closingVotePool() {
        HashMap<Player, Integer> voteToCount = new HashMap<>();
        ArrayList<Player> votedPlayers = new ArrayList<>();
        int totalSubmittedVotes = 0, count;
        for(Player voted: this.eligibileTargets){
            count = getVoteCountOf(voted);
            totalSubmittedVotes += count;
            voteToCount.put(voted, count);
            if(count != 0)
                votedPlayers.add(voted);
        }

        votedPlayers.sort(new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return voteToCount.get(o2) - voteToCount.get(o1);
            }
        });
        if(votedPlayers.size() < 2)
            return false;
        Player topVoted = votedPlayers.get(0);
        Player leastVoted = votedPlayers.get(votedPlayers.size() - 1);
        int topVotedVoteCount = voteToCount.get(topVoted);
        if(topVotedVoteCount * 2 > totalSubmittedVotes)
            return false;
        int minVote = voteToCount.get(leastVoted);
        while (voteToCount.get(leastVoted) == minVote){
            votedPlayers.remove(leastVoted);
            if(votedPlayers.size() < 2)
                return false;
            leastVoted = votedPlayers.get(votedPlayers.size() - 1);
        }
        if(votedPlayers.size() < 2)
            return false;

        this.setElligibleVoteTargets(new PlayerList(votedPlayers));
        this.clearAllVotes();
        return true;
    }

    private void removeNotVotedPlayers() {
        PlayerList votedPlayers = new PlayerList();
        int count;
        for(Player voted: this.eligibileTargets){
            count = getVoteCountOf(voted);
            if(count != 0)
                votedPlayers.add(voted);
        }
        if(votedPlayers.size() > 1)
            this.setElligibleVoteTargets(votedPlayers);
    }

    private void killHighestVotedPlayer() {
        if(closingVotePool())
            return;
        Player highestVoted = getHighestVoted();
        if(highestVoted == null){
            deadlockCount--;
            if(deadlockCount == 0)
                endGameInDraw();
            else
                pushVoteResetEvent();
            removeNotVotedPlayers();
            this.clearAllVotes();
            return;
        }
        removeNotVotedPlayers();
        boolean dayEnding = false;
        if(highestVoted != game.skipper)
            highestVoted.setLynchDeath(this.getVotersOfPlayer(highestVoted));
        game._lynches--;
        game.checkProgress();
        if(game._lynches == 0 || game.getLiveSize() < 3){
            dayEnding = true;
        }

        if(!dayEnding && game._lynches != 1){
            boolean nightDeath = false;
            DeathAnnouncement da = new DeathAnnouncement(highestVoted, nightDeath);
            if(game._lynches != 2)
                da.add("There are " + (game._lynches - 1) + " more day executions planned for today.");
            else
                da.add("There is 1 more day execution planned for today.");
            game.announcement(da);
        }

        if(dayEnding)
            game.endDay(game.parityReached());
    }

    private Player getHighestVoted() {
        Player target = null;
        int maxCount = 0, count, totalVotes = 0;
        for(Player voted: this.eligibileTargets){
            if(voted == game.skipper && !game.getBool(SetupModifierName.SKIP_VOTE))
                continue;
            count = getVoteCountOf(voted);
            totalVotes += count;
            if(maxCount < count){
                maxCount = count;
                target = voted;
            }else if(maxCount == count)
                target = null;
        }

        if(maxCount == 0 && game.getBool(SetupModifierName.SKIP_VOTE))
            return game.skipper;
        if(target == null && this.alreadyTrialedPlayers.size() == 1)
            return this.alreadyTrialedPlayers.getFirst();
        if(maxCount * 2 <= totalVotes)
            target = null;

        return target;
    }

    @Override
    public void reset() {
        PlayerList allowedPlayers = game.getLivePlayers();
        if(game.getBool(SetupModifierName.SKIP_VOTE))
            allowedPlayers.add(game.skipper);
        setElligibleVoteTargets(allowedPlayers);

        playersOnTrial = null;
        alreadyTrialedPlayers.clear();
        if(game.getInt(GameModifierName.DISCUSSION_LENGTH) == 0)
            trialIndex = 0;
        else
            trialIndex = -1;
        deadlockCount = 3;
    }

    private void setElligibleVoteTargets(PlayerList allowedVoteTargets) {
        this.eligibileTargets = allowedVoteTargets.copy();
    }

    @Override
    public void vote(Action infoAction, Action parentAction) {
        Player voter = infoAction.owner;
        PlayerList prevTargets = getVoteTargets(voter);

        this.game.addActionStack(parentAction);
        this.recalculateVotes();

        VoteAnnouncement voteAnnouncement = getVoteAnnouncement(voter, infoAction.getTargets(), prevTargets);

        voteAnnouncement.finalize();
        checkVote(parentAction.timeLeft);
        if(game.phase == GamePhase.VOTE_PHASE){
            List<NarratorListener> listeners = game.getListeners();
            for(NarratorListener listener: listeners){
                try{
                    listener.onVote(voter, voteAnnouncement, parentAction.timeLeft);
                }catch(Throwable t){
                    if(Config.getBoolean("test_mode"))
                        throw t;
                    Util.log(t);
                }
            }
        }

    }

    @Override
    public void checkVote(double timeLeft) {
        if(!game.isInProgress() || game.getLiveSize() > 2)
            return;
        game.endDay(game.parityReached());
    }

    @Override
    public void isValidVote(Action action) {
        super.isValidVote(action);
        if(!eligibileTargets.contains(action.getTarget()))
            throw new VotingException("This person may not be voted anymore.");
    }

    private void pushVoteResetEvent() {
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onVotePhaseReset(deadlockCount);
    }

    @Override
    public PlayerList getElligibleTargets() {
        return eligibileTargets.copy();
    }

    @Override
    protected boolean canVoteDuringTrial() {
        return false;
    }

}

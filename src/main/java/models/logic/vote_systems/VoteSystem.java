package models.logic.vote_systems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import game.abilities.Puppet;
import game.abilities.Vote;
import game.abilities.util.AbilityValidationUtil;
import game.event.Announcement;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.event.SelectionMessage;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import util.Util;
import util.game.VoteUtil;

public abstract class VoteSystem {

    private HashMap<Player, PlayerList> voterToVotesMap;
    private HashMap<Player, PlayerList> votedToVotersMap;
    public double dayTimeLeft;

    protected Game game;

    public VoteSystem(Game narrator) {
        this.game = narrator;
        this.voterToVotesMap = new HashMap<>();
        this.votedToVotersMap = new HashMap<>();
    }

    public abstract Player getPlayerOnTrial();

    public abstract PlayerList getPlayersOnTrial();

    public abstract void endPhase(double timeLeft);

    public abstract void reset();

    public abstract void vote(Action infoAction, Action parentAction);

    public abstract PlayerList getElligibleTargets();

    public abstract void checkVote(double timeLeft);

    public final void forceCheckVote(double timeLeft) {
        SelectionMessage e = new SelectionMessage(game.skipper, false);
        game.events.addCommand(game.skipper, timeLeft, Constants.CHECK_VOTE);
        e.dontShowPrivate();
        this.checkVote(timeLeft);
    }

    public void targetSizeCheck(Action action) {
        AbilityValidationUtil.defaultTargetSizeCheck(1, action);
    }

    public void clearAllVotes() {
        Action action;
        for(int i = 0; i < game.actionStack.size();){
            action = game.actionStack.get(i);
            if(action.is(Vote.abilityType))
                game.actionStack.remove(i);
            else
                i++;
        }
        this.voterToVotesMap.clear();
        this.votedToVotersMap.clear();
    }

    protected boolean canSkipVoting() {
        return game.getBool(SetupModifierName.SKIP_VOTE) && game.getInt(GameModifierName.NIGHT_LENGTH) != 0;
    }

    protected final void unvoteHelper(Action newAction) {
        Action action;
        for(int i = 0; i < game.actionStack.size();){
            action = game.actionStack.get(i);
            if(action.owner != newAction.owner || newAction.abilityType != action.abilityType
                    || Util.notEqual(newAction.getArg1(), action.getArg1())){
                i++;
                continue;
            }
            if(action.is(Puppet.abilityType) && action.getTargets().getFirst() != newAction.getTargets().getFirst()){
                i++;
                continue;
            }
            game.actionStack.remove(i);
            break;
        }
    }

    public final PlayerList getVoteTargets(Player voter) {
        if(this.voterToVotesMap.containsKey(voter))
            return this.voterToVotesMap.get(voter).copy();
        return new PlayerList();
    }

    public PlayerList getVotersOfPlayer(Player p) {
        if(this.votedToVotersMap.containsKey(p))
            return this.votedToVotersMap.get(p).copy();
        return new PlayerList();
    }

    public final int getVoteCountOf(Player voted) {
        int count = 0;
        PlayerList voters = this.getVotersOfPlayer(voted).getLivePlayers();
        if(voters == null)
            return 0;
        for(Player voter: voters)
            count += voter.getVotePower();
        return count;
    }

    public VoteAnnouncement getUnvoteMessage(Action action) {
        return VoteUtil.getUnvoteAnnouncement(action);
    }

    public ArrayList<Message> publicizeVoteEnding(GameFaction parity) {
        ArrayList<Message> messages = new ArrayList<>();
        Player Skipper = game.skipper;
        int dayNumber = game.getDayNumber();
        PlayerList lynchedTargets = game.getTodaysLynchedTargets();
        PlayerList votersOf;
        DeathAnnouncement e;
        if(lynchedTargets.contains(Skipper)){
            e = new DeathAnnouncement(new PlayerList(), game, !DeathAnnouncement.NIGHT_DEATH);
            votersOf = getVotersOfPlayer(Skipper);
            if(votersOf.isEmpty()){
                if(parity == null)
                    e.add("Day " + dayNumber + " ended due to inactivity.");
                else
                    e.add("Day " + dayNumber + " has ended because ", new HTString(parity), " controlled the votes.");
            }else{
                e.add(votersOf, " opted to skip day executions today.");
            }
            e.setDayEndDeath();
            messages.add(e);
        }else{

            for(Player lynched: lynchedTargets){
                StringChoice lyn = new StringChoice(lynched);
                lyn.add(lynched, "You");

                if(lynched.isPoisoned()){
                    e = new DeathAnnouncement(lynched);
                    e.setPicture("poisoner");
                    lyn.add(lynched, "you");

                    StringChoice they = new StringChoice("they");
                    they.add(lynched, "you");

                    e.add(getVotersOfPlayer(lynched), " attempted to publicly execute ", lyn, " but ", they,
                            " were already dead from poison.");
                }else{
                    lyn.add(lynched, "You");
                    e = new DeathAnnouncement(lynched);
                    e.setPicture("executioner");
                    e.add(lyn, " ");
                    StringChoice was = new StringChoice("was");
                    was.add(lynched, "were");
                    e.add(was, " publicly executed by ", lynched.getDeathType().getLynchers(), ".");
                }
                e.setDayEndDeath();
                messages.add(e);
            }
        }
        return messages;
    }

    protected abstract boolean canVoteDuringTrial();

    public void isValidVote(Action action) {
        Player voter = action.owner;
        PlayerList targets = action.getTargets();

        if(!game.isInProgress())
            throw new VotingException("You cannot vote right now.");
        if(game.phase != GamePhase.VOTE_PHASE && (game.phase != GamePhase.TRIAL_PHASE || !canVoteDuringTrial()))
            throw new VotingException("You cannot vote right now.");

        if(voter == null)
            throw new NullPointerException("Someone has to be a voter.");
        if(voter.in(targets) && !game.getBool(SetupModifierName.SELF_VOTE)){
            throw new VotingException("Cannot vote for one's self.");
        }

        Player skipper = game.skipper;
        if(voter == skipper)
            throw new VotingException("Skipper cannot vote.");

        if(voter.isDead())
            throw new VotingException("Dead players cannot vote.  " + voter.getDescription() + " is dead.");
        if(targets.getDeadPlayers().isNonempty())
            throw new VotingException("Dead players cannot be voted.");

        if(targets.contains(skipper)){
            if(!game.getBool(SetupModifierName.SKIP_VOTE))
                throw new VotingException("Skipping is not allowed.");
            if(game.getInt(GameModifierName.NIGHT_LENGTH) == 0)
                throw new VotingException("Day cannot be skipped in nightless game");

        }

        if(voter.isDisenfranchised() && (targets.size() != 1 || !action.getTargets().contains(skipper)))
            throw new VotingException(
                    voter + ", who is blackmailed, is trying to vote " + targets.getStringName() + ".");

        if(targets.toSet().size() != targets.size())
            throw new VotingException("No duplicate targets in votes.");
    }

    protected void endDiscussion() {
        startVotePhase();
    }

    protected void startVotePhase() {
        game.phase = GamePhase.VOTE_PHASE;
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onVotePhaseStart();
    }

    public void onCancelVote(Action action, double timeLeft, VoteAnnouncement voteAnnouncement) {
        game.timeLeft = timeLeft;
        recalculateVotes();
        checkVote(timeLeft);
        voteAnnouncement.finalize();
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onVoteCancel(action.owner, voteAnnouncement);
    }

    protected VoteAnnouncement getVoteAnnouncement(Player voter, PlayerList newTargets, PlayerList oldTargets) {
        return VoteUtil.getVoteAnnouncement(voter, newTargets, oldTargets);
    }

    protected final List<VoteAnnouncement> recalculateVotes() {
        List<VoteAnnouncement> announcements = new LinkedList<>();
        HashMap<Player, PlayerList> oldVoterToVotesMap = this.voterToVotesMap;
        this.voterToVotesMap = new HashMap<>();
        votedToVotersMap.clear();

        PlayerList oldVotes, newVotes;
        for(Action action: game.actionStack){
            if(action.abilityType == Puppet.abilityType)
                action = Puppet.getSubAction(action);

            if(action.abilityType != Vote.abilityType)
                continue;
            newVotes = action.getTargets();
            voterToVotesMap.put(action.owner, newVotes);
            for(Player voted: newVotes){
                if(!votedToVotersMap.containsKey(voted))
                    votedToVotersMap.put(voted, new PlayerList());
                votedToVotersMap.get(voted).add(action.owner);
            }

            oldVotes = oldVoterToVotesMap.getOrDefault(action.owner, new PlayerList());
            if(!oldVotes.toSet().equals(newVotes.toSet()))
                announcements.add(getVoteAnnouncement(action.owner, action.getTargets(), oldVotes));
            oldVoterToVotesMap.remove(action.owner);
        }

        for(Player player: oldVoterToVotesMap.keySet())
            announcements.add(getVoteAnnouncement(player, new PlayerList(), oldVoterToVotesMap.get(player)));

        for(VoteAnnouncement voteAnnouncement: announcements)
            voteAnnouncement.finalize();

        return announcements;
    }

    public final void removeVotersOf(PlayerList players) {
        PlayerList playersLeft;
        for(Action action: new ArrayList<>(this.game.actionStack)){
            playersLeft = action.getTargets().remove(players);
            if(playersLeft.isEmpty()){
                this.game.actionStack.remove(action);
            }else{
                action._targets = playersLeft;
            }
        }
        this.recalculateVotes();
    }

    protected void startTrial(Player playerOnTrial, double timeLeft) {
        this.game.phase = GamePhase.TRIAL_PHASE;
        this.dayTimeLeft = timeLeft;
        this.game.timeLeft = 1;
        Announcement e = new Announcement(game);
        StringChoice sc = new StringChoice("");
        sc.add(playerOnTrial, "Your trial has begun.");
        e.add(sc);

        sc = new StringChoice("The trial of ");
        sc.add(playerOnTrial, "");
        e.add(sc);

        sc = new StringChoice(playerOnTrial);
        sc.add(playerOnTrial, "");
        e.add(sc);

        sc = new StringChoice(" has begun.");
        sc.add(playerOnTrial, "");
        e.add(sc);

        e.finalize();
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onTrialStart(playerOnTrial, e);
    }
}

package models.logic.vote_systems;

import java.util.List;

import game.abilities.Executioner;
import game.abilities.Jester;
import game.abilities.Puppet;
import game.abilities.Vote;
import game.event.DeathAnnouncement;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.listeners.NarratorListener;
import game.logic.support.action.Action;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import nnode.Config;
import util.Util;
import util.game.VoteUtil;

public class PluralityVoteSystem extends VoteSystem {

    public PluralityVoteSystem(Game narrator) {
        super(narrator);
        reset();
    }

    private Player playerOnTrial;

    @Override
    public Player getPlayerOnTrial() {
        return playerOnTrial;
    }

    @Override
    public PlayerList getPlayersOnTrial() {
        if(playerOnTrial != null)
            return Player.list(playerOnTrial);
        return null;
    }

    @Override
    public void endPhase(double timeLeft) {
        if(game.phase == GamePhase.DISCUSSION_PHASE){
            endDiscussion();
            return;
        }
        if(sizeCheck())
            return;

        if(game.phase == GamePhase.TRIAL_PHASE){
            endTrial(timeLeft);
        }else{
            Player highestVoted = this.getHighestVoted();
            if(highestVoted == null){
                if(noOneBeingVoted() && game.getBool(SetupModifierName.SKIP_VOTE)){
                    game.endDay(null);
                    return;
                }
                this.isTied = true;
                return;
            }
            shouldDayEnd(timeLeft, true);
        }
    }

    @Override
    protected void endDiscussion() {
        if(game.phase != GamePhase.DISCUSSION_PHASE)
            throw new NarratorException("Game is not in discussion phase.");

        super.endDiscussion();
    }

    @Override
    public VoteAnnouncement getVoteAnnouncement(Player voter, PlayerList targets, PlayerList prevTargets) {
        VoteAnnouncement voteAnnouncement = VoteUtil.getVoteAnnouncement(voter, targets, prevTargets);
        int toLynch = getMinAutoLynchVote(null, 0) - getVoteCountOf(targets.getFirst());
        voteAnnouncement.add(" ", numberOfVotesNeeded(toLynch));
        return voteAnnouncement;
    }

    private PlayerList getPrivateSubmittedVoteTargets(Player voter) {
        Action action = voter.getAction(Vote.abilityType);
        if(action == null)
            return new PlayerList();
        return action.getTargets();
    }

    @Override
    public void vote(Action infoAction, Action parentAction) {
        Player voter = infoAction.owner;
        PlayerList prevTargets = getPrivateSubmittedVoteTargets(voter);
        if(infoAction.getTargets().equals(prevTargets))
            return;

        this.unvoteHelper(parentAction);
        this.game.addActionStack(parentAction);
        parentAction.owner.getActions().addFreeAction(parentAction);
        if(game.phase == GamePhase.TRIAL_PHASE)
            return;

        if(game.phase == GamePhase.VOTE_PHASE)
            game.timeLeft = parentAction.timeLeft;
        VoteAnnouncement voteAnnouncement = this.recalculateVotes().get(0);
        checkVote(parentAction.timeLeft);
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners){
            try{
                listener.onVote(voter, voteAnnouncement, parentAction.timeLeft);
            }catch(Throwable t){
                if(Config.getBoolean("test_mode"))
                    throw t;
                Util.log(t);
            }
        }
    }

    @Override
    public void onCancelVote(Action action, double timeLeft, VoteAnnouncement voteAnnouncement) {
        if(game.phase == GamePhase.TRIAL_PHASE){
            super.unvoteHelper(action);
        }else{
            super.onCancelVote(action, timeLeft, voteAnnouncement);
        }
    }

    /**
     * Formatter for number of players needed to hammer the person. This is attached
     * to all events that have the format: Voss has voted Joe (L-3)
     *
     * @param difference the number of players to hammer
     * @return the string to be attached to all vote and unvote events
     */
    public static String numberOfVotesNeeded(int difference) {
        if(difference <= 0)
            return "(Hammer)";
        return "  (L - " + difference + ")";
    }

    @Override
    public void reset() {
        clearAllVotes();
        this.isTied = false;
    }

    @Override
    public VoteAnnouncement getUnvoteMessage(Action action) {
        int difference = getMinAutoLynchVote(null, action.timeLeft) - getVoteCountOf(action.getTarget());

        VoteAnnouncement e = super.getUnvoteMessage(action);
        e.add(" ", numberOfVotesNeeded(difference));
        return e;
    }

    protected int getMinAutoLynchVote(Player player, double timeLeft) {
        int minVote = game.getLivePlayers().size();// accounts for the extra person out of skip
        minVote /= 2;
        minVote++;
        return minVote;
    }

    @Override
    public void checkVote(double timeLeft) {
        if(!sizeCheck())
            shouldDayEnd(timeLeft, false);
    }

    protected boolean isTied = false;

    private Player getHighestVoted() {
        Player target = null;
        int maxCount = 0, count;
        for(Player voted: this.getElligibleTargets()){
            count = getVoteCountOf(voted);
            if(maxCount < count){
                maxCount = count;
                target = voted;
            }else if(maxCount == count)
                target = null;
        }

        return target;
    }

    private boolean noOneBeingVoted() {
        for(Action action: game.actionStack){
            if(action.is(Vote.abilityType))
                return false;
            if(action.is(Puppet.abilityType) && Vote.COMMAND.equals(action.getArg1()))
                return false;
        }
        return true;
    }

    protected int getMinTrialCount(Player player, double timeLeft) {
        return Integer.MAX_VALUE;
    }

    private boolean isTrialStarted(Player highestVoted, double timeLeft) {
        if(game.getInt(GameModifierName.TRIAL_LENGTH) <= 0)
            return false;
        if(highestVoted == null)
            return false;
        if(highestVoted == game.skipper)
            return false;

        int minTrialCount = getMinTrialCount(highestVoted, timeLeft);
        int voteCount = this.getVoteCountOf(highestVoted);

        if(minTrialCount > voteCount)
            return false;
        startTrial(highestVoted, timeLeft);

        return true;
    }

    private boolean sizeCheck() {
        if(game.getLiveSize() <= 2){
            game.endDay(null);
            return true;
        }
        return false;
    }

    private void endTrial(double timeLeft) {
        Player oldTrialed = this.playerOnTrial;

        // this announces vote changes after trial ends
        for(VoteAnnouncement voteAnnouncement: this.recalculateVotes()){
            List<NarratorListener> listeners = game.getListeners();
            for(NarratorListener listener: listeners){
                if(voteAnnouncement.voteTarget == null)
                    listener.onVoteCancel(voteAnnouncement.voter, voteAnnouncement);
                else
                    listener.onVote(voteAnnouncement.voter, voteAnnouncement, timeLeft);
            }
        }
        Player highestVoted = this.getHighestVoted();

        boolean trialStarted = isTrialStarted(highestVoted, timeLeft);
        if(trialStarted){
            Player newTrialed = this.playerOnTrial;
            if(oldTrialed == newTrialed)
                this.eliminatePlayer(newTrialed, false);
            return;
        }
        playerOnTrial = null;
        this.game.timeLeft = this.dayTimeLeft;
        super.startVotePhase();
    }

    private void shouldDayEnd(double timeLeft, boolean dayForceEnding) {
        // check if target has enough votes to close the booths

        Player highestVoted = getHighestVoted();

        if(isTrialStarted(highestVoted, timeLeft))
            return;

        int minLynchVote = getMinAutoLynchVote(highestVoted, timeLeft);
        if(getVoteCountOf(highestVoted) < minLynchVote && !isTied && !dayForceEnding){
            GameFaction t = game.parityReached();
            if(t != null)
                game.endDay(t);
            return;
        }

        if(highestVoted == null)
            return;

        eliminatePlayer(highestVoted, dayForceEnding);
    }

    private void eliminatePlayer(Player highestVoted, boolean dayForceEnding) {
        Player Skipper = game.skipper;
        PlayerList voters = this.getVotersOfPlayer(highestVoted);
        boolean dayEnding = false;
        if(Skipper == highestVoted)
            dayEnding = true;
        else{
            highestVoted.setLynchDeath(voters);
            giveExecutionerWin(highestVoted);
            game._lynches--;
            game.checkProgress();
            if(game.isFinished())
                return;
            dayEnding = game._lynches == 0 || game.getLiveSize() < 3;
            if(!dayEnding)
                reset();

            if(!dayForceEnding && !dayEnding && game._lynches != 1){
                DeathAnnouncement da = new DeathAnnouncement(highestVoted, !DeathAnnouncement.NIGHT_DEATH);
                if(game._lynches != 2)
                    da.add("There are " + (game._lynches - 1) + " more day executions planned for today.");
                else
                    da.add("There is 1 more day execution planned for today.");
                game.announcement(da);
            }
        }

        // jester check
        if(highestVoted.is(Jester.abilityType)){
            for(Player p: voters){
                p.votedForJester(true);
            }
        }

        GameFaction t = game.parityReached();
        if(t != null || dayEnding)
            game.endDay(t);
    }

    private void giveExecutionerWin(Player target) {
        for(Player exec: game.players.filter(Executioner.abilityType).getLivePlayers()){
            Executioner r = exec.getAbility(Executioner.class);
            if(r.getTarget().equals(target))
                r.setWon();
        }
    }

    @Override
    protected void startTrial(Player trialed, double timeLeft) {
        this.playerOnTrial = trialed;
        super.startTrial(trialed, timeLeft);
    }

    @Override
    public PlayerList getElligibleTargets() {
        PlayerList living = game.getLivePlayers();
        if(canSkipVoting())
            living.add(game.skipper);
        return living;
    }

    @Override
    protected boolean canVoteDuringTrial() {
        return true;
    }
}

package models.logic.vote_systems;

import game.abilities.util.AbilityValidationUtil;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import util.game.VoteUtil;

public class MultiVotePluralitySystem extends PluralityVoteSystem {

    public MultiVotePluralitySystem(Game narrator) {
        super(narrator);
    }

    @Override
    public void targetSizeCheck(Action action) {
        if(action.getTargets().isEmpty())
            AbilityValidationUtil.Exception("You must select at least one vote target.");
    }

    @Override
    public void reset() {
        this.isTied = false;
        game.timeLeft = 1;
    }

    @Override
    public VoteAnnouncement getUnvoteMessage(Action action) {
        return VoteUtil.getUnvoteAnnouncement(action);
    }

    @Override
    public VoteAnnouncement getVoteAnnouncement(Player voter, PlayerList targets, PlayerList prevTargets) {
        return VoteUtil.getVoteAnnouncement(voter, targets, prevTargets);
    }

    @Override
    public int getMinAutoLynchVote(Player player, double timeLeft) {
        if(this.isTied)
            return 1;
        if(player == game.skipper || this.getPlayerOnTrial() != null)
            return getMinTrialCount(player, timeLeft);
        return Integer.MAX_VALUE;
    }

    @Override
    protected int getMinTrialCount(Player player, double timeLeft) {
        int playerCount = game.getLivePlayers().size();
        if(player != game.skipper)
            --playerCount;
        double minTrialCount = timeLeft * playerCount;
        return (int) Math.floor(minTrialCount) + 1;
    }
}

package models;

import java.util.Optional;

import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.logic.support.rules.ModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;

public class GameModifiers<T extends ModifierName> {
    public Game game;

    public Modifiers<T> modifiers;

    public GameModifiers(Game game, Modifiers<T> modifiers) {
        this.game = game;
        this.modifiers = modifiers;
    }

    private Optional<Modifier<T>> getModifier(T modifierName) {
        return modifiers.getModifier(modifierName, game.players.size());
    }

    public Object get(T modifierName) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.internalValue;
        return modifierName.getDefaultValue();
    }

    public boolean getBoolean(T modifierName) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getBoolValue();
        return (boolean) modifierName.getDefaultValue();
    }

    public int getInt(T modifierName) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getIntValue();
        return (int) modifierName.getDefaultValue();
    }

    public String getString(T modifierName) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getStringValue();
        throw new NarratorException(modifierName.toString() + " modifier not found");
    }

    public int getOrDefault(T modifierName, int defaultValue) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getIntValue();
        return defaultValue;
    }

    public boolean getOrDefault(T modifierName, boolean defaultValue) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getBoolValue();
        return defaultValue;
    }

    public String getOrDefault(T modifierName, String defaultValue) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.getStringValue();
        return defaultValue;
    }

    public Object getOrDefault(T modifierName, Object defaultValue) {
        Optional<Modifier<T>> modifier = getModifier(modifierName);
        if(modifier.isPresent())
            return modifier.get().value.internalValue;
        return defaultValue;
    }

    public boolean hasKey(T modifierName) {
        return getModifier(modifierName) != null;
    }
}

package models.schemas;

public class SetupHiddenSpawnSchema {
    public long setupHiddenID;
    public long factionRoleID;

    public SetupHiddenSpawnSchema(long setupHiddenID, long factionRoleID) {
        this.setupHiddenID = setupHiddenID;
        this.factionRoleID = factionRoleID;
    }
}

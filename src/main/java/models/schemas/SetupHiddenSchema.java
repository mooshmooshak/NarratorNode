package models.schemas;

public class SetupHiddenSchema {
    public long hiddenID;
    public long id;
    public boolean isExposed;
    public boolean mustSpawn;
    public int minPlayerCount;
    public int maxPlayerCount;

    public SetupHiddenSchema(long id, long hiddenID, boolean isExposed, boolean mustSpawn, int minPlayerCount,
            int maxPlayerCount) {
        this.hiddenID = hiddenID;
        this.id = id;
        this.isExposed = isExposed;
        this.mustSpawn = mustSpawn;
        this.minPlayerCount = minPlayerCount;
        this.maxPlayerCount = maxPlayerCount;
    }
}

package models.schemas;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import models.idtypes.GameID;
import models.idtypes.PlayerDBID;

public class PlayerSchema {

    public PlayerDBID id;
    public GameID gameID;
    public String name;
    public Optional<Long> userID;

    public PlayerSchema(PlayerDBID id, GameID gameID, String name, Optional<Long> userID) {
        this.id = id;
        this.gameID = gameID;
        this.name = name;
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "PlayerSchema " + name + "{" + id + "}";
    }

    public static Map<String, PlayerSchema> mapByName(Set<PlayerSchema> players) {
        Map<String, PlayerSchema> map = new HashMap<>();
        for(PlayerSchema player: players)
            map.put(player.name, player);
        return map;
    }
}

package models.schemas;

import java.time.Instant;

import models.enums.RoleModifierName;
import models.modifiers.Modifier;

public class RoleModifierSchema extends ModifierSchema<RoleModifierName> {

    public RoleModifierSchema(long id, RoleModifierName name, Object value, int minPlayerCount, int maxPlayerCount,
            Instant upsertedAt) {
        super(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public RoleModifierSchema(Modifier<RoleModifierName> modifier) {
        super(modifier);
    }

}

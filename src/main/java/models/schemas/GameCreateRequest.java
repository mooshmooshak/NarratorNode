package models.schemas;

public class GameCreateRequest {
    public String hostName;
    public long setupID;
    public boolean isPublic;

    public GameCreateRequest(String hostName, long setupID, boolean isPublic) {
        this.hostName = hostName;
        this.setupID = setupID;
        this.isPublic = isPublic;
    }
}

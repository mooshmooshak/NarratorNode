package models.schemas;

import java.util.ArrayList;
import java.util.List;

import game.abilities.Hidden;
import models.HiddenSpawn;
import models.requests.HiddenSpawnCreateRequest;

public class HiddenSpawnSchema {
    public long factionRoleID;
    public long hiddenID;
    public long id;
    public int maxPlayerCount;
    public int minPlayerCount;

    public HiddenSpawnSchema(long id, long hiddenID, long factionRoleID, int minPlayerCount, int maxPlayerCount) {
        this.id = id;
        this.hiddenID = hiddenID;
        this.factionRoleID = factionRoleID;
        this.minPlayerCount = minPlayerCount;
        this.maxPlayerCount = maxPlayerCount;
    }

    public HiddenSpawnSchema(HiddenSpawnSchema hiddenSpawnSchema) {
        this.id = hiddenSpawnSchema.id;
        this.factionRoleID = hiddenSpawnSchema.factionRoleID;
        this.hiddenID = hiddenSpawnSchema.hiddenID;
        this.minPlayerCount = hiddenSpawnSchema.minPlayerCount;
        this.maxPlayerCount = hiddenSpawnSchema.maxPlayerCount;
    }

    public HiddenSpawnSchema(Hidden hidden, HiddenSpawn spawn) {
        this.id = spawn.id;
        this.factionRoleID = spawn.factionRole.id;
        this.minPlayerCount = spawn.minPlayerCount;
        this.maxPlayerCount = spawn.maxPlayerCount;
        this.hiddenID = hidden.id;
    }

    public HiddenSpawnSchema(long spawnID, HiddenSpawnCreateRequest request) {
        this.factionRoleID = request.factionRoleID;
        this.hiddenID = request.hiddenID;
        this.id = spawnID;
        this.maxPlayerCount = request.maxPlayerCount;
        this.minPlayerCount = request.minPlayerCount;
    }

    public static ArrayList<HiddenSpawnSchema> from(List<HiddenSpawnCreateRequest> schemas, List<Long> spawnIDs) {
        ArrayList<HiddenSpawnSchema> newList = new ArrayList<>();
        long spawnID;
        HiddenSpawnCreateRequest request;
        for(int i = 0; i < schemas.size() || i < spawnIDs.size(); i++){
            request = schemas.get(i);
            spawnID = spawnIDs.get(i);
            newList.add(new HiddenSpawnSchema(spawnID, request));
        }
        return newList;
    }
}

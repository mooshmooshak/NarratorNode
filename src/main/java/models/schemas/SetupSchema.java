package models.schemas;

public class SetupSchema {
    public String description, slogan, name;
    public long id, ownerID;
    public boolean isEditable;
}

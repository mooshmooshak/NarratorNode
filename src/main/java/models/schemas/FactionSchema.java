package models.schemas;

import models.requests.FactionCreateRequest;

public class FactionSchema {
    public long id;
    public String color;
    public String name;
    public String description;

    public FactionSchema(long id, String color, String name, String description) {
        this.id = id;
        this.color = color;
        this.name = name;
        this.description = description;
    }

    public FactionSchema(long factionID, FactionCreateRequest request) {
        this(factionID, request.color, request.name, request.description);
    }
}

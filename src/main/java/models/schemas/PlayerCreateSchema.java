package models.schemas;

import java.util.Optional;

public class PlayerCreateSchema {

    public String name;
    public Optional<Long> userID;

    public PlayerCreateSchema(String name, Optional<Long> userID) {
        this.name = name;
        this.userID = userID;
    }

    public PlayerCreateSchema(String name) {
        this(name, Optional.empty());
    }

    @Override
    public String toString() {
        return "PlayerSchema " + name;
    }
}

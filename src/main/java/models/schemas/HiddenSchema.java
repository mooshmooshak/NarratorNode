package models.schemas;

public class HiddenSchema {
    public long id;
    public String name;

    public HiddenSchema(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

package models.schemas;

import java.util.Optional;

public class FactionRoleSchema {

    public long factionID, roleID, id;
    public Optional<Long> receivedFactionRoleID;
}

package models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.AbilityList;
import game.abilities.GameAbility;
import game.logic.Game;
import game.logic.GameRole;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.modifiers.Modifiers;
import models.schemas.RoleSchema;
import util.game.GameUtil;

public class Role {
    public Setup setup;

    public long id;
    public String name;

    // I want to preserve the order of inserted abilities
    public LinkedHashMap<AbilityType, Ability> abilityMap;

    public Modifiers<RoleModifierName> modifiers;

    public Role(Setup setup, RoleSchema schema) {
        this.setup = setup;

        this.id = schema.id;
        this.name = schema.name;

        this.abilityMap = new LinkedHashMap<>();
        this.modifiers = new Modifiers<>();
    }

    public String getName() {
        return name;
    }

    final public boolean hasAbility(AbilityType... abilityTypes) {
        for(AbilityType abilityType: abilityTypes)
            if(abilityMap.containsKey(abilityType))
                return true;
        return false;
    }

    public Ability getAbility(AbilityType abilityType) {
        Ability ability = abilityMap.get(abilityType);
        if(ability == null)
            throw new NarratorException("Ability not found");
        return ability;
    }

    public Ability getAbility(long abilityID) {
        for(Ability ability: abilityMap.values()){
            if(ability.id == abilityID)
                return ability;
        }
        throw new NarratorException("Ability not found.");
    }

    @SafeVarargs
    public final boolean is(AbilityType... abilityTypes) {
        for(Ability ability: getAbilities()){
            for(AbilityType abilityType: abilityTypes)
                if(ability.type.equals(abilityType))
                    return true;
        }
        return false;
    }

    // linked bc first ability will be shown first to user
    public LinkedHashSet<Ability> getAbilities() {
        return new LinkedHashSet<>(abilityMap.values());
    }

    public ArrayList<String> getPublicDescription(Optional<Game> game) {
        ArrayList<String> ruleTexts = new ArrayList<>();

        GameAbility gameAbility;
        ArrayList<String> publicDescription;
        for(Ability a: this.abilityMap.values()){
            gameAbility = a.getGameAbility(game, a.modifiers);
            publicDescription = gameAbility.getPublicDescription(game, this.getName(), Optional.empty(),
                    this.getAbilities());
            ruleTexts.addAll(publicDescription);
        }

        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        ruleTexts.addAll(GameRole.GetModifierRuleTexts(this.modifiers, playerCount));

        return ruleTexts;
    }

    public boolean isRoleUnique(int playerCount) {
        return modifiers.getBoolean(RoleModifierName.UNIQUE, playerCount);
    }

    public boolean isChatRole() {
        for(Ability ability: this.getAbilities()){
            if(ability.getGameAbility().isChatRole())
                return true;
        }
        return false;
    }

    public String getDatabaseName() {
        List<GameAbility> gameAbilities = new LinkedList<>();
        for(Ability ability: this.getAbilities())
            gameAbilities.add(ability.getGameAbility());

        return new AbilityList(gameAbilities).getDatabaseName();
    }

    @Override
    public String toString() {
        return this.name + " [" + this.id + "]";
    }
}

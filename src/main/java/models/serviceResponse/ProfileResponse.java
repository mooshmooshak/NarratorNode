package models.serviceResponse;

import java.util.Optional;

import game.logic.Player;
import models.User;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;

public class ProfileResponse {
    public Optional<GameID> gameID;
    public Optional<Player> player;
    public Optional<PlayerDBID> playerID;
    public Optional<String> playerName;
    public User user;

    public ProfileResponse(Optional<GameID> gameID, User user, Optional<PlayerDBID> playerID,
            Optional<String> playerName, Optional<Player> player) {
        this.gameID = gameID;
        this.player = player;
        this.playerID = playerID;
        this.playerName = playerName;
        this.user = user;
    }
}

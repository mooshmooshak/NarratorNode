package models.serviceResponse;

import game.abilities.Hidden;
import game.logic.Game;

public class HiddenCreateResponse {

    public Hidden hidden;
    public Game game;

    public HiddenCreateResponse(Hidden hidden, Game game) {
        this.hidden = hidden;
        this.game = game;
    }
}

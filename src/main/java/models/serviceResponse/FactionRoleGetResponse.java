package models.serviceResponse;

import java.util.Optional;

import game.logic.Game;
import models.FactionRole;

public class FactionRoleGetResponse {

    public FactionRole factionRole;
    public Optional<Game> game;
    public int playerCount;

    public FactionRoleGetResponse(FactionRole factionRole, Optional<Game> optGame, int playerCount) {
        this.factionRole = factionRole;
        this.game = optGame;
        this.playerCount = playerCount;
    }

}

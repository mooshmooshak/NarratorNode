package models.serviceResponse;

import game.logic.support.CommandHandler;

public class AbilityMetadata {
    public String command;
    public String description;
    public String usage;
    public boolean publicCommand;

    public AbilityMetadata(String command, String description, String usage, boolean publicCommand) {
        this.command = command;
        this.description = description;
        this.usage = usage;
        this.publicCommand = publicCommand;
    }

    public static AbilityMetadata getEndNight() {
        return new AbilityMetadata(CommandHandler.END_NIGHT, "skip night, making morning arrive sooner",
                CommandHandler.END_NIGHT, true);
    }

    public static AbilityMetadata getModkill(String exampleName) {
        return new AbilityMetadata(CommandHandler.MODKILL, "prematurely stop a player from playing",
                CommandHandler.MODKILL + " " + exampleName, true);
    }

    public static AbilityMetadata endPhase() {
        return new AbilityMetadata(CommandHandler.END_PHASED_SPACED, "ends the current phase, and starts the next one",
                CommandHandler.END_PHASED_SPACED, true);
    }
}

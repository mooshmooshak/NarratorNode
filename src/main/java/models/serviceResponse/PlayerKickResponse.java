package models.serviceResponse;

import java.util.Set;

import game.logic.Game;

public class PlayerKickResponse {
    public Set<Long> moderatorIDs;
    public Game game;

    public PlayerKickResponse(Set<Long> moderatorIDs, Game game) {
        this.moderatorIDs = moderatorIDs;
        this.game = game;
    }
}

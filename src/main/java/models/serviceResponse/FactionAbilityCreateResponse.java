package models.serviceResponse;

import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import models.Ability;

public class FactionAbilityCreateResponse {

    public Ability ability;
    public String color;
    public String factionName;
    public Optional<Game> game;
    public Set<Ability> otherAbilities;

    public FactionAbilityCreateResponse(Ability ability, String color, String factionName, Optional<Game> game,
            Set<Ability> otherAbilities) {
        this.ability = ability;
        this.color = color;
        this.factionName = factionName;
        this.game = game;
        this.otherAbilities = otherAbilities;
    }

}

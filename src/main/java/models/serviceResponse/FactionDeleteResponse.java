package models.serviceResponse;

public class FactionDeleteResponse {

    public String joinID;
    public long setupID;

    public FactionDeleteResponse(long setupID, String joinID) {
        this.joinID = joinID;
        this.setupID = setupID;
    }

}

package models.serviceResponse;

import json.JSONException;
import json.JSONObject;
import models.idtypes.PlayerDBID;
import models.view.Jsonifiable;

public class PlayerNameUpdateResponse extends Jsonifiable {
    private String lobbyID;
    private PlayerDBID playerID;

    public PlayerNameUpdateResponse(String lobbyID, PlayerDBID playerID) {
        this.lobbyID = lobbyID;
        this.playerID = playerID;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject response = new JSONObject();
        response.put("joinID", this.lobbyID);
        response.put("playerID", this.playerID);
        return response;
    }
}

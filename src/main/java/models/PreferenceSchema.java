package models;

import models.enums.PreferType;

public class PreferenceSchema {

    public long entityID;
    public PreferType preferenceType;

    public PreferenceSchema(long entityID, PreferType preferType) {
        this.entityID = entityID;
        this.preferenceType = preferType;
    }
}

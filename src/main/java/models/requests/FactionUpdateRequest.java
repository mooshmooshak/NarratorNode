package models.requests;

import json.JSONException;
import json.JSONObject;

public class FactionUpdateRequest {
    public String name;
    public String description;

    public FactionUpdateRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public FactionUpdateRequest(JSONObject args) throws JSONException {
        this(args.getString("name"), args.getString("description"));
    }
}

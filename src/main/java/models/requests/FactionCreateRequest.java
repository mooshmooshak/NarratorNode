package models.requests;

import java.util.ArrayList;

import models.Faction;

public class FactionCreateRequest {
    public String name;
    public String color;
    public String description;

    public FactionCreateRequest(String name, String color, String description) {
        this.name = name;
        this.color = color;
        this.description = description;
    }

    public FactionCreateRequest(Faction faction) {
        this(faction.name, faction.color, faction.description);
    }

    public static ArrayList<FactionCreateRequest> fromFactions(ArrayList<Faction> factions) {
        ArrayList<FactionCreateRequest> requests = new ArrayList<>();
        for(Faction faction: factions)
            requests.add(new FactionCreateRequest(faction));
        return requests;
    }
}

package models.requests;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.view.Jsonifiable;

public class FactionRoleAbilityModifierRequest extends Jsonifiable {

    public long abilityID;
    public long factionRoleID;
    public int maxPlayerCount;
    public int minPlayerCount;
    public AbilityModifierName name;
    public ModifierValue value;
    public long userID;

    public FactionRoleAbilityModifierRequest(long abilityID, long factionRoleID, int maxPlayerCount, int minPlayerCount,
            AbilityModifierName name, ModifierValue value, long userID) {
        this.abilityID = abilityID;
        this.factionRoleID = factionRoleID;
        this.maxPlayerCount = maxPlayerCount;
        this.minPlayerCount = minPlayerCount;
        this.name = name;
        this.value = value;
        this.userID = userID;
    }

    public static FactionRoleAbilityModifierRequest fromJson(JSONObject object) throws JSONException {
        long userID = object.getLong("userID");
        long factionRoleID = object.getLong("factionRoleID");
        long abilityID = object.getLong("abilityID");
        ModifierValue value = new ModifierValue(object.get("value"));
        int minPlayerCount = object.getInt("minPlayerCount");
        int maxPlayerCount = object.getInt("maxPlayerCount");

        String modifierName = object.getString("name");
        AbilityModifierName name;
        try{
            name = AbilityModifierName.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown role ability modifier name: " + modifierName);
        }

        return new FactionRoleAbilityModifierRequest(abilityID, factionRoleID, maxPlayerCount, minPlayerCount, name,
                value, userID);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("userID", this.userID);
        request.put("factionRoleID", this.factionRoleID);
        request.put("abilityID", this.abilityID);
        request.put("value", this.value.internalValue);
        request.put("minPlayerCount", this.minPlayerCount);
        request.put("maxPlayerCount", this.maxPlayerCount);
        request.put("name", this.name.toString());
        return request;
    }

}

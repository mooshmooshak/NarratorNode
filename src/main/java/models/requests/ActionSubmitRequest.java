package models.requests;

import java.util.List;

import game.logic.support.Constants;
import game.logic.support.action.Action;
import json.JSONException;
import json.JSONObject;
import models.enums.AbilityType;
import models.view.Jsonifiable;
import nnode.StateObject;

public class ActionSubmitRequest extends Jsonifiable {

    private AbilityType abilityType;
    private String option;
    private String option2;
    private String option3;
    private List<String> targets;
    private double timeLeft;
    private long userID;

    public ActionSubmitRequest(Action action, long userID) {
        this.abilityType = action.abilityType;
        this.option = action.getArg1();
        this.option2 = action.getArg2();
        this.option3 = action.getArg3();
        this.targets = action.getTargets().getNamesToStringList();
        this.timeLeft = action.timeLeft;
        this.userID = userID;
    }

    public ActionSubmitRequest(AbilityType abilityType, List<String> targets, long userID) {
        this.abilityType = abilityType;
        this.targets = targets;
        this.timeLeft = Constants.ALL_TIME_LEFT;
        this.userID = userID;
    }

    public ActionSubmitRequest(AbilityType abilityType, String opt1, List<String> targets, long userID) {
        this.abilityType = abilityType;
        this.option = opt1;
        this.targets = targets;
        this.timeLeft = Constants.ALL_TIME_LEFT;
        this.userID = userID;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.command, this.abilityType.command);
        jo.put(StateObject.option, this.option);
        jo.put(StateObject.option2, this.option2);
        jo.put(StateObject.option3, this.option3);
        jo.put(StateObject.targets, this.targets);
        jo.put("timeLeft", this.timeLeft);
        jo.put("userID", this.userID);
        return jo;
    }

}

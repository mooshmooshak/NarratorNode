package models.idtypes;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class PlayerDBID extends IDType {

    public PlayerDBID(long id) {
        super(id);
    }

    public static Set<Long> toPrimitiveSet(Collection<PlayerDBID> ids) {
        Set<Long> idList = new HashSet<>();
        for(PlayerDBID id: ids)
            idList.add(id.getValue());
        return idList;
    }
}

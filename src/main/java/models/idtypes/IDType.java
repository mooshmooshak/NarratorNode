package models.idtypes;

public abstract class IDType {

    private long id;

    public IDType(long id) {
        this.id = id;
    }

    public long getValue() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(this.getClass().equals(o.getClass())){
            IDType otherType = (IDType) o;
            return otherType.id == this.id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(id);
    }

    @Override
    public String toString() {
        return Long.toString(id);
    }
}

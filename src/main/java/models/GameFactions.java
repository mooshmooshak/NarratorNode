package models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import game.abilities.GameAbility;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import models.enums.AbilityType;

public class GameFactions implements Iterable<GameFaction> {
    public Set<GameFaction> factions;

    public GameFactions(Set<GameFaction> factions) {
        this.factions = factions;
    }

    public Set<GameAbility> getAbilities() {
        Set<GameAbility> gameAbilities = new HashSet<>();
        for(GameFaction gameFaction: this.factions)
            for(GameAbility ability: gameFaction.getAbilities())
                gameAbilities.add(ability);

        return gameAbilities;
    }

    public HashMap<GameFaction, Player> getAbilityControllers(AbilityType abilityType) {
        HashMap<GameFaction, Player> abilityControllers = new HashMap<>();
        for(GameFaction faction: this.factions){
            if(faction.size() < 2)
                continue;
            abilityControllers.put(faction, faction.getCurrentController(abilityType));
        }
        return abilityControllers;
    }

    @Override
    public Iterator<GameFaction> iterator() {
        return factions.iterator();
    }

    public PlayerList getFactionMates() {
        PlayerList players = new PlayerList();
        for(GameFaction gameFaction: factions)
            for(Player player: gameFaction.getMembers())
                if(!players.contains(player))
                    players.add(player);

        return players;
    }

    public int size() {
        return factions.size();
    }

    public GameFactions filterKnowsTeam() {
        Set<GameFaction> knownFactions = new HashSet<>();
        for(GameFaction faction: factions)
            if(faction.knowsTeam())
                knownFactions.add(faction);
        return new GameFactions(knownFactions);
    }

    public boolean hasAbility(AbilityType abilityType) {
        for(GameFaction faction: factions){
            if(faction.faction.hasAbility(abilityType))
                return true;
        }
        return false;
    }

    public boolean contains(GameFaction gameFaction) {
        return factions.contains(gameFaction);
    }

    public Optional<GameFaction> getFactionWithAbility(AbilityType abilityType) {
        GameAbility a;
        for(GameFaction t: factions){
            a = t.getAbility(abilityType);
            if(a != null)
                return Optional.of(t);
        }
        return Optional.empty();
    }
}

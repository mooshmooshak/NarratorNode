package models.json_output;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.Ability;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import models.serviceResponse.FactionAbilityCreateResponse;
import models.view.Jsonifiable;
import util.game.GameUtil;
import util.game.ModifierUtil;

public class AbilityJson extends Jsonifiable {

    private long id;
    private String description;
    private List<String> details;
    private Set<AbilityModifierJson> modifiers;
    private String name;
    private List<String> setupModifierNames;

    private AbilityJson(Ability ability, Set<Ability> otherAbilities, Optional<String> color, String name,
            Optional<Game> game, Map<AbilityType, Modifiers<AbilityModifierName>> factionRoleAbilityModifiers) {

        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        Modifiers<AbilityModifierName> modifiers = ModifierUtil.mergeModifiers(playerCount, ability.modifiers,
                factionRoleAbilityModifiers.get(ability.type));

        this.id = ability.id;
        this.description = ability.getAbilityDescription(game, modifiers);
        this.details = ability.getPublicDescription(game, color, name, otherAbilities, modifiers);
        this.modifiers = AbilityModifierJson.getSet(ability, modifiers, game, otherAbilities.size() == 1);
        this.name = ability.type.toString();
        this.setupModifierNames = new LinkedList<>();
        for(SetupModifierName modifierName: ability.getGameAbility(game, modifiers).getSetupModifierNames())
            this.setupModifierNames.add(modifierName.toString());
    }

    public AbilityJson(FactionAbilityCreateResponse response) {
        this(response.ability, response.otherAbilities, Optional.of(response.color), response.factionName,
                response.game, new HashMap<>());
    }

    // preserves ordering
    public static LinkedHashSet<AbilityJson> getSet(LinkedHashSet<Ability> abilities, Optional<String> color,
            String name, Optional<Game> game,
            Map<AbilityType, Modifiers<AbilityModifierName>> factionRoleAbilityModifiers) {
        LinkedHashSet<AbilityJson> abilityViews = new LinkedHashSet<>();

        AbilityJson abilityView;
        for(Ability ability: abilities){
            abilityView = new AbilityJson(ability, abilities, color, name, game, factionRoleAbilityModifiers);
            abilityViews.add(abilityView);
        }

        return abilityViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject abilityJson = new JSONObject();
        abilityJson.put("id", this.id);
        abilityJson.put("description", this.description);
        abilityJson.put("details", this.details);
        abilityJson.put("modifiers", toJson(this.modifiers));
        abilityJson.put("name", this.name);
        abilityJson.put("setupModifierNames", this.setupModifierNames);
        return abilityJson;
    }
}

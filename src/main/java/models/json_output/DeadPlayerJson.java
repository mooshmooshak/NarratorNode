package models.json_output;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.logic.Player;
import game.logic.PlayerList;
import json.JSONException;
import json.JSONObject;
import models.PlayerUserIDMap;
import models.view.Jsonifiable;

public class DeadPlayerJson extends Jsonifiable {

    private DeathTypeJson deathType;
    private Optional<String> lastWill;
    private String name;
    private GraveJson role;
    private Optional<Long> userID;

    public static Set<DeadPlayerJson> getSet(PlayerList players, PlayerUserIDMap playerUserMap) {
        Set<DeadPlayerJson> deadPlayerViews = new HashSet<>();
        for(Player p: players)
            deadPlayerViews.add(new DeadPlayerJson(p, playerUserMap.getUserID(p)));

        return deadPlayerViews;
    }

    public DeadPlayerJson(Player player, Optional<Long> userID) {
        this.name = player.getName();
        this.role = new GraveJson(player);
        this.userID = userID;
        this.deathType = new DeathTypeJson(player.getDeathType());
        this.lastWill = Optional.ofNullable(player.getLastWill(null));
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject playerJson = new JSONObject();
        playerJson.put("name", this.name);
        playerJson.put("role", this.role);
        playerJson.put("userID", this.userID);
        playerJson.put("deathType", this.deathType);
        playerJson.put("lastWill", this.lastWill);
        return playerJson;
    }

}

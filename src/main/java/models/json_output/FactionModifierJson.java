package models.json_output;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import json.JSONException;
import json.JSONObject;
import models.Faction;
import models.ModifierValue;
import models.enums.FactionModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.view.Jsonifiable;
import util.game.ModifierUtil;

public class FactionModifierJson extends Jsonifiable {

    private boolean isOverride;
    private String label;
    private FactionModifierName name;
    private ModifierValue value;

    public FactionModifierJson(Modifier<FactionModifierName> modifier, boolean isOverride,
            Modifiers<SetupModifierName> setupModifiers, Optional<Integer> playerCount) {
        this(modifier.name, modifier.value, isOverride, setupModifiers, playerCount);
    }

    public FactionModifierJson(FactionModifierName name, ModifierValue value, boolean isOverride,
            Modifiers<SetupModifierName> setupModifiers, Optional<Integer> playerCount) {
        this.isOverride = isOverride;
        this.label = name.getLabel(playerCount, setupModifiers);
        this.name = name;
        this.value = value;
    }

    public static Set<FactionModifierJson> getSet(Faction faction, Optional<Integer> playerCount) {
        Set<FactionModifierJson> factionViews = new HashSet<>();

        Modifiers<SetupModifierName> setupModifiers = faction.setup.modifiers;
        FactionModifierJson factionView;
        Set<FactionModifierName> seenModifierNames = new HashSet<>();

        for(Modifier<FactionModifierName> modifier: faction.modifiers){
            factionView = new FactionModifierJson(modifier, true, setupModifiers, playerCount);
            factionViews.add(factionView);
            if(ModifierUtil.isDefault(modifier))
                seenModifierNames.add(modifier.name);
        }
        for(FactionModifierName modifierName: FactionModifierName.values()){
            if(seenModifierNames.contains(modifierName))
                continue;
            factionView = new FactionModifierJson(modifierName, new ModifierValue(modifierName.defaultValue), false,
                    setupModifiers, playerCount);
            factionViews.add(factionView);
        }

        return factionViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {

        JSONObject object = new JSONObject();

        object.put("name", this.name.toString());
        object.put("isOverride", this.isOverride);
        object.put("value", this.value.internalValue);
        object.put("label", this.label);

        return object;

    }

}

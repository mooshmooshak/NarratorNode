
package models.json_output;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.OpenLobby;
import models.enums.GameModifierJson;
import models.idtypes.GameID;
import models.serviceResponse.GameLobbyGetResponse;
import models.view.Jsonifiable;

public class GameJson extends Jsonifiable {

    private int dayNumber; // TODO remove
    private Set<PlayerJson> graveyard; // deprecated
    private GameID gameID;
    private boolean isStarted;
    private String lobbyID;
    private Map<String, GameModifierJson> modifiers;
    private PhaseJson phase;
    private Set<PlayerJson> players;
    private SetupJson setup;
    private VoteData voteInfo;
    private Set<GameUserJson> users;

    public GameJson(OpenLobby lobby) {
        this.dayNumber = 0;
        this.graveyard = new HashSet<>();
        this.gameID = lobby.gameID;
        this.isStarted = false;
        this.lobbyID = lobby.lobbyID;
        this.modifiers = GameModifierJson.getMap(lobby.game.gameModifiers);
        this.phase = new PhaseJson(lobby);
        this.players = PlayerJson.getSet(lobby.players);
        this.setup = new SetupJson(lobby.game);
        this.voteInfo = new VoteData(lobby);
        this.users = GameUserJson.fromSchemas(lobby.users);
    }

    public GameJson(GameLobby gameLobby) {
        Game game = gameLobby.game;
        this.dayNumber = game.getDayNumber();
        this.graveyard = PlayerJson.getSet(game.getDeadPlayers(), gameLobby.playerUserIDMap);
        this.gameID = gameLobby.id;
        this.isStarted = game.isStarted();
        this.lobbyID = gameLobby.lobbyID;
        this.modifiers = GameModifierJson.getMap(game.gameModifiers);
        this.phase = new PhaseJson(game);
        this.players = PlayerJson.getSet(game.players, gameLobby.playerUserIDMap);
        this.setup = new SetupJson(game);
        this.voteInfo = new VoteData(game);
        this.users = GameUserJson.getSet(gameLobby.userMap.values());
    }

    public static GameJson from(GameLobbyGetResponse response) {
        if(response.gameLobby.isPresent())
            return new GameJson(response.gameLobby.get());
        return new GameJson(response.openLobby.get());
    }

    public static Set<GameJson> from(Set<OpenLobby> games) {
        Set<GameJson> gameViews = new HashSet<>();
        for(OpenLobby openLobby: games)
            gameViews.add(new GameJson(openLobby));
        return gameViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject gameJson = new JSONObject();
        gameJson.put("dayNumber", this.dayNumber); // TODO remove
        gameJson.put("graveyard", this.graveyard); // deprecated
        gameJson.put("id", this.gameID);
        gameJson.put("isStarted", this.isStarted);
        gameJson.put("joinID", this.lobbyID); // deprecated
        gameJson.put("lobbyID", this.lobbyID);
        gameJson.put("modifiers", this.modifiers);
        gameJson.put("phase", this.phase);
        gameJson.put("players", this.players);
        gameJson.put("setup", this.setup);
        gameJson.put("voteInfo", this.voteInfo);
        gameJson.put("users", this.users);
        return gameJson;
    }

    public static String getLobbyID(JSONObject gameJson) throws JSONException {
        return gameJson.getString("joinID");
    }
}

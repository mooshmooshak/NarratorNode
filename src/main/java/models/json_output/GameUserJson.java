package models.json_output;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import json.JSONException;
import json.JSONObject;
import models.GameUser;
import models.schemas.GameUserSchema;
import models.view.Jsonifiable;

public class GameUserJson extends Jsonifiable {

    private boolean isModerator;
    private long userID;

    public GameUserJson(long userID, boolean isModerator) {
        this.isModerator = isModerator;
        this.userID = userID;
    }

    public GameUserJson(long userID, Set<Long> moderatorIDs) {
        this(userID, moderatorIDs.contains(userID));
    }

    public GameUserJson(long userID, long hostID) {
        this(userID, hostID == userID);
    }

    public GameUserJson(GameUser user) {
        this(user.id, user.isModerator);
    }

    public static Set<GameUserJson> getSet(Collection<Long> users, Set<Long> moderatorIDs) {
        Set<GameUserJson> usersJson = new HashSet<>();
        for(long userID: users)
            usersJson.add(new GameUserJson(userID, moderatorIDs));
        return usersJson;
    }

    public static Set<GameUserJson> getSet(Collection<Long> users, long hostID) {
        Set<GameUserJson> usersViews = new HashSet<>();
        for(long userID: users)
            usersViews.add(new GameUserJson(userID, hostID));
        return usersViews;
    }

    public static Set<GameUserJson> getSet(Collection<GameUser> users) {
        Set<GameUserJson> userViews = new HashSet<>();
        for(GameUser user: users)
            userViews.add(new GameUserJson(user));
        return userViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject userJSON = new JSONObject();
        userJSON.put("id", this.userID);
        userJSON.put("isModerator", this.isModerator);
        return userJSON;
    }

    public static Set<GameUserJson> fromSchemas(Set<GameUserSchema> schemas) {
        Set<GameUserJson> views = new HashSet<>();
        for(GameUserSchema schema: schemas)
            views.add(new GameUserJson(schema.id, schema.isModerator));

        return views;
    }

}

package models.json_output;

import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.setups.Setup;
import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;
import util.game.GameUtil;

public class SetupJson extends Jsonifiable {

    private Set<FactionJson> factions;
    private Set<HiddenJson> hiddens;
    private boolean isEditable;
    private int maxPlayerCount;
    private int minPlayerCount;
    private String name;
    private long ownerID;
    private Set<RoleJson> roles;
    private long setupID;
    private Set<SetupHiddenJson> setupHiddens;
    private Set<SetupModifierJson> setupModifiers;

    public SetupJson(Setup setup, Optional<Game> game) {
        this.factions = FactionJson.getSet(setup.getFactions(), game);
        this.hiddens = HiddenJson.getSet(setup.hiddens, game);
        this.isEditable = setup.isEditable;
        this.maxPlayerCount = Setup.getMaxPlayerCount(setup, game);
        this.minPlayerCount = Setup.getMinPlayerCount(setup, game);
        this.name = setup.name;
        this.ownerID = setup.ownerID;
        this.roles = RoleJson.getSet(setup.roles, game);
        this.setupID = setup.id;
        this.setupHiddens = SetupHiddenJson.getSet(setup.rolesList, game);
        this.setupModifiers = SetupModifierJson.getSet(setup, GameUtil.getPlayerCount(game));
    }

    public SetupJson(Game game) {
        this(game.setup, Optional.of(game));
    }

    public SetupJson(Setup setup) {
        this(setup, Optional.empty());
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject setupJson = new JSONObject();
        setupJson.put("factions", this.factions);
        setupJson.put("hiddens", this.hiddens);
        setupJson.put("id", this.setupID);
        setupJson.put("isEditable", this.isEditable);
        setupJson.put("maxPlayerCount", this.maxPlayerCount);
        setupJson.put("minPlayerCount", this.minPlayerCount);
        setupJson.put("name", this.name);
        setupJson.put("ownerID", this.ownerID);
        setupJson.put("roles", this.roles);
        setupJson.put("setupHiddens", toJson(this.setupHiddens));
        setupJson.put("setupModifiers", this.setupModifiers);
        return setupJson;
    }
}

package models.json_output;

import java.util.HashSet;
import java.util.Set;

import json.JSONException;
import json.JSONObject;
import models.BaseAbility;
import models.view.Jsonifiable;

public class BaseAbilityView extends Jsonifiable {

    public boolean isFactionAllowed;
    public String type;

    public BaseAbilityView(BaseAbility baseAbility) {
        this.isFactionAllowed = baseAbility.isFactionAllowed;
        this.type = baseAbility.type.toString();
    }

    public static Set<BaseAbilityView> getSet(Set<BaseAbility> baseAbilities) {
        Set<BaseAbilityView> views = new HashSet<>();
        for(BaseAbility baseAbility: baseAbilities)
            views.add(new BaseAbilityView(baseAbility));
        return views;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject response = new JSONObject();
        response.put("isFactionAllowed", this.isFactionAllowed);
        response.put("type", this.type);
        return response;
    }
}

package models.json_output;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.logic.support.rules.ModifierName;
import game.setups.Setup;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.view.Jsonifiable;
import util.game.ModifierUtil;

public class ModifierJson<T extends ModifierName> extends Jsonifiable {

    private boolean isOverride;
    private String label;
    private String name;
    private int maxPlayerCount;
    private int minPlayerCount;
    private ModifierValue value;

    public ModifierJson(Modifier<T> modifier, Optional<Integer> playerCount,
            Modifiers<SetupModifierName> setupModifiers) {
        this.isOverride = true;
        this.label = getLabel(playerCount, setupModifiers, modifier.name);
        this.maxPlayerCount = modifier.maxPlayerCount;
        this.minPlayerCount = modifier.minPlayerCount;
        this.name = modifier.name.toString();
        this.value = modifier.value;
    }

    public ModifierJson(T name, Optional<Integer> playerCount, Modifiers<SetupModifierName> setupModifiers) {
        this.isOverride = false;
        this.label = getLabel(playerCount, setupModifiers, name);
        this.maxPlayerCount = Setup.MAX_PLAYER_COUNT;
        this.minPlayerCount = 0;
        this.name = name.toString();
        this.value = new ModifierValue(ModifierUtil.IsIntModifier(name) ? 0 : false);
    }

    private static <T extends ModifierName> String getLabel(Optional<Integer> playerCount,
            Modifiers<SetupModifierName> setupModifiers, T name) {
        return ModifierUtil.replaceFormats(playerCount, setupModifiers, name.getLabelFormat());
    }

    public static <T extends ModifierName> Set<ModifierJson<T>> getSet(Optional<Integer> playerCount,
            Modifiers<SetupModifierName> setupModifiers, Modifiers<T> modifiers, T[] modifierNames) {
        Set<ModifierJson<T>> modifierViews = new HashSet<>();
        Set<T> seenModifierNames = new HashSet<>();
        for(Modifier<T> modifier: modifiers){
            modifierViews.add(new ModifierJson<T>(modifier, playerCount, setupModifiers));
            seenModifierNames.add(modifier.name);
        }
        for(T modifierName: modifierNames){
            if(seenModifierNames.contains(modifierName))
                continue;
            modifierViews.add(new ModifierJson<T>(modifierName, playerCount, setupModifiers));
        }
        return modifierViews;
    }

    public static Set<ModifierJson<RoleModifierName>> getRoleModifiers(Optional<Integer> playerCount,
            Modifiers<SetupModifierName> setupModifiers, Modifiers<RoleModifierName> modifiers) {
        return getSet(playerCount, setupModifiers, modifiers, RoleModifierName.values());
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jModifier = new JSONObject();
        jModifier.put("label", this.label);
        jModifier.put("name", this.name);
        jModifier.put("isOverride", this.isOverride);
        jModifier.put("minPlayerCount", this.minPlayerCount);
        jModifier.put("maxPlayerCount", this.maxPlayerCount);
        jModifier.put("value", this.value.internalValue);
        return jModifier;
    }
}

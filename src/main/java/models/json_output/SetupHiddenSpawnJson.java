package models.json_output;

import java.util.List;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GeneratedRole;

public class SetupHiddenSpawnJson {
    public static JSONArray toJson(List<GeneratedRole> iterations) throws JSONException {
        JSONArray array = new JSONArray();
        for(GeneratedRole setupHiddenSpawn: iterations)
            array.put(toJson(setupHiddenSpawn));
        return array;
    }

    public static JSONObject toJson(GeneratedRole schema) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("setupHiddenID", schema.setupHidden.id);
        object.put("factionRoleID", schema.getFactionRole().id);
        return object;
    }
}

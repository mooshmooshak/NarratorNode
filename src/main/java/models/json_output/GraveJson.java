package models.json_output;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.logic.DeathType;
import game.logic.Player;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.view.Jsonifiable;

public class GraveJson extends Jsonifiable {

    private Optional<String> color;
    private int dayNumber;
    private Set<String> deathTypes;
    private Optional<Long> factionID;
    private boolean isDayDeath;
    private Optional<String> roleName;

    public GraveJson(Player player) {
        DeathType dt = player.getDeathType();

        Set<String> deathTypes = new HashSet<>();
        for(String[] s: dt.getList())
            deathTypes.add(s[1]);// 0 is the id, 1 is the description

        this.color = player.getGraveyardColor();
        this.dayNumber = dt.getDeathDay();
        this.deathTypes = deathTypes;
        this.factionID = getGraveyardFactionID(player);
        this.isDayDeath = dt.isDayDeath();
        this.roleName = player.getGraveyardRoleName();
    }

    private static Optional<Long> getGraveyardFactionID(Player player) {
        Optional<FactionRole> factionRole = player.getGraveyardFactionRole();
        if(factionRole.isPresent())
            return Optional.of(factionRole.get().faction.id);
        return Optional.empty();
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject graveyardRole = new JSONObject();
        graveyardRole.put("color", this.color);
        graveyardRole.put("dayNumber", this.dayNumber);
        graveyardRole.put("deathTypes", this.deathTypes);
        graveyardRole.put("factionID", this.factionID);
        graveyardRole.put("isDayDeath", this.isDayDeath);
        graveyardRole.put("name", this.roleName);
        graveyardRole.put("roleName", this.roleName);
        return graveyardRole;
    }

}

package models.json_output;

import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;

public class SlowGretelSqlQueryJson extends Jsonifiable {

    private String sqlQuery;
    private long length;

    public SlowGretelSqlQueryJson(long length, String sqlQuery) {
        this.length = length;
        this.sqlQuery = sqlQuery;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("name", "Gretel db query");
        obj.put("sqlQuery", sqlQuery);
        obj.put("length", length);
        obj.put("logMessage", true);
        return obj;
    }

}

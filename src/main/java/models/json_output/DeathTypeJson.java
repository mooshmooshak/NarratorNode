package models.json_output;

import java.util.LinkedList;
import java.util.List;

import game.logic.DeathType;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;

public class DeathTypeJson extends Jsonifiable {

    private int dayNumber;
    private List<String> attacks;

    public DeathTypeJson(DeathType deathType) {
        this.attacks = new LinkedList<>();
        this.dayNumber = deathType.getDeathDay();

        for(String[] attacks: deathType.attacks)
            this.attacks.add(attacks[1]);
    }

    @Override
    public JSONObject toJson() throws JSONException {

        JSONArray attacksJson = new JSONArray();
        JSONObject attackObj;
        for(String attack: this.attacks){
            attackObj = new JSONObject();
            attackObj.put("text", attack);
            attacksJson.put(attackObj);
        }

        JSONObject request = new JSONObject();
        request.put("attacks", attacksJson);
        request.put("dayNumber", this.dayNumber);
        return request;
    }

}

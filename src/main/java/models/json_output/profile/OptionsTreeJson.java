package models.json_output.profile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import game.abilities.GameAbility;
import game.logic.Player;
import game.logic.support.Option;
import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;

public class OptionsTreeJson extends Jsonifiable {

    Map<String, OptionJson> backer = new HashMap<>();

    public OptionsTreeJson(Player player, GameAbility ability) {
        this(player, ability, new ArrayList<>(), OptionJson.getNextOptions(player, ability, new ArrayList<>()));
    }

    OptionsTreeJson(Player player, GameAbility ability, ArrayList<String> path, ArrayList<Option> oList) {
        for(Option option: oList)
            this.backer.put(option.getValue(), new OptionJson(player, ability, option, path));
    }

    @Override
    public JSONObject toJson() throws JSONException {
        return new JSONObject(this.backer);
    }
}

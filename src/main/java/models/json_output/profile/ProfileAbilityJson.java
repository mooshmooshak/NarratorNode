package models.json_output.profile;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import game.abilities.GameAbility;
import game.logic.Player;
import game.logic.PlayerList;
import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;

public class ProfileAbilityJson extends Jsonifiable {

    private boolean canAddAction;
    public String command; // for ease of testing
    private List<String> details;
    private OptionsTreeJson options;
    private Set<String> targets;

    private ProfileAbilityJson(Player player, GameAbility ability) {
        this.canAddAction = player.getActions().canAddAnotherAction(ability.getAbilityType());
        this.command = ability.getCommand();
        this.details = ability.getProfileRoleCardDetails(player);
        this.options = new OptionsTreeJson(player, ability);
        this.targets = getTargets(player, ability);
    }

    public static LinkedHashSet<ProfileAbilityJson> getSet(Player player) {
        LinkedHashSet<ProfileAbilityJson> abilities = new LinkedHashSet<>();
        for(GameAbility ability: player.getAbilities())
            abilities.add(new ProfileAbilityJson(player, ability));

        return abilities;
    }

    private static Set<String> getTargets(Player player, GameAbility ability) {
        PlayerList allowedTargets = player.getAcceptableTargets(ability.getAbilityType());
        if(allowedTargets == null)
            return new HashSet<>();
        return new HashSet<>(allowedTargets.getNamesToStringList());
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject abilityJson = new JSONObject();
        abilityJson = new JSONObject();
        abilityJson.put("canAddAction", this.canAddAction);
        abilityJson.put("command", this.command);
        abilityJson.put("details", this.details);
        abilityJson.put("options", this.options);
        abilityJson.put("targets", this.targets);
        return abilityJson;
    }
}

package models.json_output.profile;

import java.util.Optional;

import game.abilities.Bulletproof;
import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.enums.RoleModifierName;
import models.view.Jsonifiable;

public class ProfileAllyModifiersJson extends Jsonifiable {

    private Optional<Boolean> nightKillImmune;
    public Optional<Boolean> undetectable;
    private Optional<Boolean> unblockable;
    private Optional<Integer> autoVestCount;

    public ProfileAllyModifiersJson(Game game, FactionRole factionRole) {
        int autoVestCount = factionRole.roleModifiers.getInt(RoleModifierName.AUTO_VEST, game);
        this.autoVestCount = autoVestCount > 0 ? Optional.of(autoVestCount) : Optional.empty();

        this.nightKillImmune = factionRole.role.hasAbility(Bulletproof.abilityType) ? Optional.of(true)
                : Optional.empty();
        this.unblockable = factionRole.roleModifiers.getBoolean(RoleModifierName.UNBLOCKABLE, game) ? Optional.of(true)
                : Optional.empty();
        this.undetectable = factionRole.roleModifiers.getBoolean(RoleModifierName.UNDETECTABLE, game)
                ? Optional.of(true)
                : Optional.empty();
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject modifier = new JSONObject();
        modifier.put("night_kill_immune", this.nightKillImmune);
        modifier.put("undetectable", this.undetectable);
        modifier.put("unblockable", this.unblockable);
        modifier.put("autoVestCount", this.autoVestCount);
        return modifier;
    }
}

package models.json_output.profile;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.Executioner;
import game.abilities.Ghost;
import game.logic.GameFaction;
import game.logic.Player;
import json.JSONException;
import json.JSONObject;
import models.Faction;
import models.json_output.ActionJson;
import models.json_output.FactionOverviewJson;
import models.view.Jsonifiable;

public class ProfileRoleCardJson extends Jsonifiable {

    // these are public for ease of testing purposes
    public LinkedHashSet<ProfileAbilityJson> abilities; // order is important. faction abilities last
    private Set<ActionJson> actions;
    public List<String> details; // deprecated
    private Set<Long> enemyFactionIDs;
    private Set<FactionOverviewJson> enemyFactions;
    private long factionID;
    private String factionName; // so discord doesn't have to do a call for getting the faction name
    private long factionRoleID;
    private List<String> roleModifierDetails;
    private String roleName;
    private Optional<String> winConditionText;

    public ProfileRoleCardJson(Player player) {
        Set<Long> enemyFactionIDs = getEnemyFactionIDs(player);

        this.abilities = ProfileAbilityJson.getSet(player);
        this.actions = ActionJson.getSet(player);
        this.details = player.getRoleCardDetails();
        this.enemyFactionIDs = enemyFactionIDs;
        this.enemyFactions = getEnemyFactions(player);
        this.factionID = player.gameRole.factionRole.faction.id;
        this.factionName = player.getGameFaction().getName();
        this.factionRoleID = player.gameRole.factionRole.id;
        this.roleModifierDetails = player.getRoleModifierDetails();
        this.roleName = player.getRoleName();
        this.winConditionText = getWinConditionText(player, enemyFactionIDs);
    }

    private static Set<Long> getEnemyFactionIDs(Player player) {
        Set<Long> enemyFactionIDs = new HashSet<>();
        if(player.is(Ghost.abilityType)){
            if(player.isAlive())
                return enemyFactionIDs;
            Player cause = player.getDeathType().getCause();
            if(cause == player && player.getCause() != null)
                cause = player.getCause();
            if(cause == null || cause == player)
                return enemyFactionIDs;
            GameFaction faction = cause.getGameFaction();
            if(faction.isEnemy(faction))
                return enemyFactionIDs;
            if(faction.knowsTeam() || faction._startingSize > 0)
                enemyFactionIDs.add(faction.faction.id);
        }else{
            Faction faction = player.getGameFaction().faction;
            for(Faction enemyFaction: faction.enemies)
                enemyFactionIDs.add(enemyFaction.id);
        }
        return enemyFactionIDs;
    }

    private static Set<FactionOverviewJson> getEnemyFactions(Player player) {
        Set<FactionOverviewJson> enemyFactions = new HashSet<>();
        Faction faction = player.getGameFaction().faction;
        for(Faction enemyFaction: faction.enemies)
            enemyFactions.add(new FactionOverviewJson(enemyFaction));
        return enemyFactions;
    }

    private static Optional<String> getWinConditionText(Player player, Set<Long> enemyFactionIDs) {
        if(player.is(Ghost.abilityType)){
            if(player.isAlive())
                return Optional.of("The first step to winning is to die.");
            Player cause = player.getDeathType().getCause();
            if(cause == player && player.getCause() != null)
                cause = player.getCause();
            if(cause == null || cause == player)
                return Optional.of("You've lost the game.");
            GameFaction faction = cause.getGameFaction();
            if(faction.isEnemy(faction) || (!faction.knowsTeam() && (faction._startingSize <= 0)))
                return Optional.of("You must assure the defeat of " + cause.getName() + " to win.");
        }else if(player.is(Executioner.abilityType)){
            String targetName = Executioner.GetTarget(player).getName();
            return Optional.of("To win, you must get " + targetName + " voted out.");
        }else if(enemyFactionIDs.isEmpty())
            return Optional.of("You don't care about who wins.");
        return Optional.empty();
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject response = new JSONObject();
        response.put("abilities", this.abilities);
        response.put("actions", this.actions);
        response.put("details", this.details);
        response.put("enemyFactions", this.enemyFactions);
        response.put("enemyFactionIDs", this.enemyFactionIDs);
        response.put("factionID", this.factionID);
        response.put("factionName", this.factionName);
        response.put("factionRoleID", this.factionRoleID);
        response.put("roleModifierDetails", this.roleModifierDetails);
        response.put("roleName", this.roleName);
        response.put("winConditionText", this.winConditionText);
        return response;
    }
}

package models.json_output.profile;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.abilities.Cultist;
import game.abilities.Disguiser;
import game.abilities.Infiltrator;
import game.abilities.Mason;
import game.logic.GameFaction;
import game.logic.GameRole;
import game.logic.Player;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.view.Jsonifiable;

public class ProfileAllyJson extends Jsonifiable {

    public long factionID; // deprecated
    public long factionRoleID;
    public ProfileAllyModifiersJson modifiers;
    public String name;
    public String roleName; // deprecated

    private ProfileAllyJson(Player player, AllyVisibility allyVisibility) {
        Player ally = allyVisibility.player;
        FactionRole factionRole = allyVisibility.factionRole;

        this.factionID = factionRole.faction.id;
        this.factionRoleID = factionRole.id;
        this.modifiers = new ProfileAllyModifiersJson(player.game, factionRole);
        this.name = ally.getName();
        this.roleName = factionRole.getName();
    }

    public static Set<ProfileAllyJson> getSet(Player player) {
        Set<ProfileAllyJson> allies = new HashSet<>();
        Optional<AllyVisibility> allyVisibility;
        for(Player ally: player.game.getLivePlayers()){
            allyVisibility = shouldShowPlayer(player, ally);
            if(allyVisibility.isPresent())
                allies.add(new ProfileAllyJson(player, allyVisibility.get()));
        }
        return allies;
    }

    public static Optional<AllyVisibility> shouldShowPlayer(Player p, Player q) {
        if(p == q || p.isDead())
            return Optional.empty();
        if(p.getGameFaction().equals(q.getGameFaction()) && p.getGameFaction().knowsTeam())
            return Optional.of(new AllyVisibility(q, q.gameRole));
        for(GameFaction t: p.getFactions())
            if(t.knowsTeam() && t.getMembers().contains(q) && q.getGameFaction() == t)
                return Optional.of(new AllyVisibility(q, q.gameRole));

        if(Mason.IsMasonType(p) && q.getFactions().contains(p.getGameFaction()) && Mason.IsMasonType(q))
            return Optional.of(new AllyVisibility(q, q.gameRole));

        if(q.isDead())
            return Optional.empty();

        if(Disguiser.hasDisguised(q)){
            Disguiser dg = q.getAbility(Disguiser.class);
            if(shouldShowPlayer(p, dg.disguised) != null)
                return Optional.of(new AllyVisibility(q, dg.disguised.gameRole));
        }
        if(q.is(Infiltrator.abilityType) && q.in(p.getGameFaction().getMembers())){
            Set<FactionRole> cultists = p.getGameFaction().faction.getFactionRolesWithAbility(Cultist.abilityType);
            if(!cultists.isEmpty())
                return Optional.of(new AllyVisibility(p, cultists.iterator().next()));
        }
        return Optional.empty();
    }

    private static class AllyVisibility {
        Player player;
        FactionRole factionRole;

        public AllyVisibility(Player player, FactionRole factioRole) {
            this.player = player;
            this.factionRole = factioRole;
        }

        public AllyVisibility(Player player, GameRole gameFactionRole) {
            this(player, gameFactionRole.factionRole);
        }
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jAlly = new JSONObject();
        jAlly.put("factionID", this.factionID);
        jAlly.put("factionRoleID", this.factionRoleID);
        jAlly.put("modifiers", this.modifiers);
        jAlly.put("name", this.name);
        jAlly.put("roleName", this.roleName);
        return jAlly;
    }
}

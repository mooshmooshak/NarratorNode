package models.json_output;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import game.logic.Player;
import game.logic.PlayerList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.PlayerUserIDMap;
import models.idtypes.PlayerDBID;
import models.schemas.PlayerSchema;
import models.view.Jsonifiable;

public class PlayerJson extends Jsonifiable {

    private PlayerDBID id;
    private boolean endedNight;
    private boolean isParticipating;
    private Optional<FactionRoleJson> factionRole;
    private Optional<GraveJson> flip;
    private String name;
    private Optional<Long> setupHiddenID;
    private Optional<Long> userID;
    private Optional<Integer> votePower;

    public PlayerJson(PlayerSchema playerSchema) {
        this(playerSchema.id, playerSchema.name, playerSchema.userID);
    }

    public PlayerJson(PlayerDBID id, String name, long userID) {
        this(id, name, Optional.of(userID));
    }

    public PlayerJson(PlayerDBID id, String name, Optional<Long> userID) {
        this.endedNight = false;
        this.factionRole = Optional.empty();
        this.flip = Optional.empty();
        this.id = id;
        this.isParticipating = userID.isPresent();
        this.name = name;
        this.setupHiddenID = Optional.empty();
        this.userID = userID;
        this.votePower = Optional.empty();
    }

    public PlayerJson(Player player, Optional<Long> playerUserID) {
        this.id = player.databaseID;
        this.endedNight = player.endedNight();
        this.factionRole = getFactionRoleJson(player);

        if(player.isDead())
            this.flip = Optional.of(new GraveJson(player));
        else
            this.flip = Optional.empty();
        this.isParticipating = player.isParticipating();

        this.name = player.getName();
        this.setupHiddenID = Optional.of(player.initialAssignedRole.assignedSetupHidden.id);
        this.userID = playerUserID;
        this.votePower = Optional.of(player.getVotePower());
    }

    public static Set<PlayerJson> getSet(Set<PlayerSchema> players) {
        Set<PlayerJson> playersJson = new HashSet<>();
        for(PlayerSchema player: players)
            playersJson.add(new PlayerJson(player));
        return playersJson;
    }

    // linked hash set to preserve order
    // needed for things like trialed players, where order tells trial order
    public static LinkedHashSet<PlayerJson> getSet(PlayerList players, PlayerUserIDMap playerUserMap) {
        LinkedHashSet<PlayerJson> playerViews = new LinkedHashSet<>();

        for(Player player: players)
            playerViews.add(new PlayerJson(player, playerUserMap.getUserID(player)));
        return playerViews;
    }

    private static Optional<FactionRoleJson> getFactionRoleJson(Player player) {
        FactionRoleJson view;
        if(player.gameRole.factionRole.receivedRole == null)
            view = new FactionRoleJson(player.gameRole.factionRole, player.game);
        else
            view = new FactionRoleJson(player.gameRole.factionRole.receivedRole, player.game);
        return Optional.of(view);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject playerJson = new JSONObject();

        playerJson.put("endedNight", this.endedNight);
        playerJson.put("factionRole", this.factionRole);
        playerJson.put("flip", this.flip);
        playerJson.put("id", this.id);
        playerJson.put("isComputer", !this.userID.isPresent()); // deprecated
        playerJson.put("isParticipating", this.isParticipating);
        playerJson.put("name", this.name);
        playerJson.put("setupHiddenID", this.setupHiddenID);
        playerJson.put("userID", this.userID);
        playerJson.put("votePower", this.votePower);

        return playerJson;
    }

    public static String getPlayerName(JSONObject object) throws JSONException {
        return object.getString("name");
    }

    public static Set<String> getPlayerNames(JSONArray players) throws JSONException {
        Set<String> botNames = new HashSet<>();
        String botName;
        for(int i = 0; i < players.length(); i++){
            botName = getPlayerName(players.getJSONObject(i));
            botNames.add(botName);
        }
        return botNames;
    }
}

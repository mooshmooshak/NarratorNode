package models.view;

import java.util.Set;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;

// this can easily move to an interface if I want to move out the jsonArray helper
public abstract class Jsonifiable {
    public abstract JSONObject toJson() throws JSONException;

    public static <T extends Jsonifiable> JSONArray toJson(Set<T> models) throws JSONException {
        JSONArray output = new JSONArray();
        for(T model: models)
            output.put(model.toJson());
        return output;
    }
}

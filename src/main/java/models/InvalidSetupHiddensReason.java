package models;

import models.enums.InvalidSetupHiddensCode;

public class InvalidSetupHiddensReason {
    public InvalidSetupHiddensCode code;
    public Integer resetIndex;

    public InvalidSetupHiddensReason(InvalidSetupHiddensCode code) {
        this.code = code;
    }
}

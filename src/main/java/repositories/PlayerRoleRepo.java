package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import models.idtypes.GameID;
import models.idtypes.PlayerDBID;

public class PlayerRoleRepo extends BaseRepo {

    public static void insertAssignments(GameID id, Map<PlayerDBID, Long> assignments) throws SQLException {
        if(assignments.isEmpty())
            return;
        List<Object> queryArgs = new LinkedList<>();
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO player_roles ");
        query.append("(game_id, setup_hidden_id, player_id) ");
        query.append("VALUES ");

        Iterator<PlayerDBID> iterator = assignments.keySet().iterator();
        PlayerDBID playerID;
        for(int i = 0; i < assignments.size(); i++){
            if(i != 0)
                query.append(", ");
            query.append("(?, ?, ?)");
            playerID = iterator.next();
            queryArgs.add(id);
            queryArgs.add(assignments.get(playerID));
            queryArgs.add(playerID);
        }
        query.append(";");
        executeInsertQuery(query.toString(), queryArgs);
    }

    public static Map<PlayerDBID, Long> getByGameID(GameID replayID) throws SQLException {
        Map<PlayerDBID, Long> assignments = new HashMap<>();
        String query = "SELECT setup_hidden_id, player_id FROM player_roles WHERE game_id = ?;";
        ResultSet results = executeQuery(query, replayID);
        PlayerDBID playerID;
        long setupHiddenID;
        while (results.next()){
            playerID = new PlayerDBID(results.getLong("player_id"));
            setupHiddenID = results.getLong("setup_hidden_id");
            assignments.put(playerID, setupHiddenID);
        }
        results.close();
        return assignments;
    }

    public static void deleteByGameID(Connection connection, long gameID) throws SQLException {
        String query = "DELETE FROM player_roles WHERE game_id = ?;";
        execute(query, gameID);
    }

}

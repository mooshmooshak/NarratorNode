package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.dbo.ModifierTableNames;
import models.enums.AbilityModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.AbilityModifierSchema;
import util.game.ModifierUtil;

public class FactionRoleAbilityModifierRepo extends BaseRepo {

    private static ModifierTableNames tableNames = new ModifierTableNames("faction_role_ability_int_modifiers",
            "faction_role_ability_bool_modifiers", "faction_role_ability_string_modifiers");

    private static String getBySetupIDQuery(String tableName) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT " + tableName
                + ".id, faction_role_id, role_ability_id, name, value, min_player_count, max_player_count, upserted_at  ");
        query.append("FROM " + tableName + " ");
        query.append("LEFT JOIN faction_roles ");
        query.append("  ON " + tableName + ".faction_role_id = faction_roles.id ");
        query.append("WHERE setup_id = ?;");
        return query.toString();
    }

    public static Future<Map<Long, Map<Long, Set<AbilityModifierSchema>>>> getAbilityModifiers(long setupID) {
        return executorService.submit(new Callable<Map<Long, Map<Long, Set<AbilityModifierSchema>>>>() {
            @Override
            public Map<Long, Map<Long, Set<AbilityModifierSchema>>> call() throws Exception {
                return getAbilityModifiersSync(setupID);
            }
        });
    }

    public static Map<Long, Map<Long, Set<AbilityModifierSchema>>> getAbilityModifiersSync(long setupID)
            throws SQLException {
        Map<Long, Map<Long, Set<AbilityModifierSchema>>> resultsMap = new HashMap<>();

        ResultSet results;
        String query;
        for(String tableName: tableNames.getNames()){
            query = getBySetupIDQuery(tableName);
            results = executeQuery(query, setupID);
            extractAbilityModifierResults(resultsMap, results);
            results.close();
        }

        return resultsMap;
    }

    private static void extractAbilityModifierResults(Map<Long, Map<Long, Set<AbilityModifierSchema>>> resultsMap,
            ResultSet results) throws SQLException {
        Object value;
        long factionRoleID;
        long roleAbilityID;
        long id;
        int minPlayerCount;
        int maxPlayerCount;
        Instant upsertedAt;
        AbilityModifierName modifier;
        AbilityModifierSchema modifierSchema;
        Map<Long, Set<AbilityModifierSchema>> abilityMap;
        while (results.next()){
            id = results.getLong("id");
            modifier = AbilityModifierName.valueOf(results.getString("name"));
            value = results.getObject("value");
            factionRoleID = results.getLong("faction_role_id");
            roleAbilityID = results.getLong("role_ability_id");
            minPlayerCount = results.getInt("min_player_count");
            maxPlayerCount = results.getInt("max_player_count");
            upsertedAt = results.getTimestamp("upserted_at").toInstant();
            modifierSchema = new AbilityModifierSchema(id, modifier, value, minPlayerCount, maxPlayerCount, upsertedAt);

            if(!resultsMap.containsKey(factionRoleID))
                resultsMap.put(factionRoleID, new HashMap<>());
            abilityMap = resultsMap.get(factionRoleID);
            if(abilityMap.containsKey(roleAbilityID))
                abilityMap.get(roleAbilityID).add(modifierSchema);
            else
                abilityMap.put(roleAbilityID, new HashSet<>(Arrays.asList(modifierSchema)));
        }
    }

    public static void upsertAbilityModifier(long factionRoleID, long roleAbilityID,
            Modifier<AbilityModifierName> modifier) throws SQLException {
        Object value = modifier.value.internalValue;
        String tableName = getTableName(modifier.name);
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO " + tableName + " ");
        query.append(
                "(faction_role_id, role_ability_id, name, value, min_player_count, max_player_count, upserted_at) ");
        query.append("VALUES (?, ?, ?, ?, ?, ?, ?) ");
        query.append("ON DUPLICATE KEY UPDATE value = ?, upserted_at = ?;");
        execute(query.toString(), factionRoleID, roleAbilityID, modifier.name.toString(), value,
                modifier.minPlayerCount, modifier.maxPlayerCount, modifier.upsertedAt, value, modifier.upsertedAt);
    }

    private static String getTableName(AbilityModifierName name) {
        if(ModifierUtil.IsIntModifier(name))
            return tableNames.tableIntName;
        if(ModifierUtil.IsBoolModifier(name))
            return tableNames.tableBoolName;
        return tableNames.tableStringName;
    }

    private static StringBuilder getInsertQuery(String tableName) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO " + tableName + " ");
        query.append(
                "(faction_role_id, role_ability_id, name, value, min_player_count, max_player_count, upserted_at) VALUES ");
        return query;
    }

    public static void create(ArrayList<Long> factionRoleIDs, ArrayList<Long> roleAbilityIDs,
            ArrayList<Modifiers<AbilityModifierName>> modifiersList) throws SQLException {
        StringBuilder intQuery = getInsertQuery(tableNames.tableIntName);
        StringBuilder booleanQuery = getInsertQuery(tableNames.tableBoolName);
        StringBuilder stringQuery = getInsertQuery(tableNames.tableStringName);

        List<Object> boolModifierArgs = new LinkedList<>();
        List<Object> intModifierArgs = new LinkedList<>();
        List<Object> stringModifierArgs = new LinkedList<>();

        StringBuilder query;
        List<Object> modifierArgs;
        Modifiers<AbilityModifierName> modifiers;
        for(int i = 0; i < modifiersList.size(); i++){
            modifiers = modifiersList.get(i);
            for(Modifier<AbilityModifierName> modifier: modifiers){
                query = ModifierUtil.IsBoolModifier(modifier.name) ? booleanQuery : intQuery;
                query.append("(?, ?, ?, ?, ?, ?, ?), ");

                modifierArgs = ModifierUtil.IsStringModifier(modifier.name) ? stringModifierArgs
                        : ModifierUtil.IsBoolModifier(modifier.name) ? boolModifierArgs : intModifierArgs;
                modifierArgs.add(factionRoleIDs.get(i));
                modifierArgs.add(roleAbilityIDs.get(i));
                modifierArgs.add(modifier.name.toString());
                modifierArgs.add(modifier.value.internalValue);
                modifierArgs.add(modifier.minPlayerCount);
                modifierArgs.add(modifier.maxPlayerCount);
                modifierArgs.add(modifier.upsertedAt);
            }
        }

        ModifierRepo.executeIfArgs(intQuery, intModifierArgs);
        ModifierRepo.executeIfArgs(stringQuery, stringModifierArgs);
        ModifierRepo.executeIfArgs(booleanQuery, boolModifierArgs);
    }

}

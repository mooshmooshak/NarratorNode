package repositories;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;

import models.idtypes.IDType;
import nnode.Config;
import util.Util;

public class Connection {

    private java.sql.Connection internalConnection;
    private static Object connectionLock = new Object();

    public Connection() throws SQLException {
        this.internalConnection = getConnection();
    }

    public void close() {
        try{
            internalConnection.close();
        }catch(SQLException e){
            Util.log(e, "Failed to close connection");
        }

    }

    public void attemptRestart() throws SQLException {
        synchronized (connectionLock){
            internalConnection.close();
            internalConnection = getConnection();
        }
    }

    private static java.sql.Connection getConnection() throws SQLException {
        final String dbUsername = Config.getString("db_username");
        final String dbPassword = Config.getString("db_password");
        final String dbHostname = Config.getString("db_hostname");
        final String dbPort = Config.getString("db_port");
        final String dbName = Config.getString("db_name");
        final String dbURLTemplate = "jdbc:mariadb://%s:%s/%s?autoReconnect=true&allowPublicKeyRetrieval=true&useSSL=false";
        String url = String.format(dbURLTemplate, dbHostname, dbPort, dbName);

        return DriverManager.getConnection(url, dbUsername, dbPassword);
    }

    public PreparedStatement prepareStatement(String query, Object... parameters) throws SQLException {
        PreparedStatement ps = this.internalConnection.prepareStatement(query);
        setParameters(ps, parameters);
        return ps;
    }

    public PreparedStatement prepareInsertStatement(String query, Object... parameters) throws SQLException {
        PreparedStatement ps = this.internalConnection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        setParameters(ps, parameters);
        return ps;
    }

    private static void setParameters(PreparedStatement ps, Object... parameters) throws SQLException {
        for(int i = 0; i < parameters.length; i++)
            setParameter(ps, i, parameters[i]);
    }

    private static void setParameter(PreparedStatement ps, int i, Object parameter) throws SQLException {
        if(parameter instanceof Integer)
            ps.setInt(i + 1, (int) parameter);
        else if(parameter instanceof String)
            ps.setString(i + 1, (String) parameter);
        else if(parameter instanceof Long)
            ps.setLong(i + 1, (Long) parameter);
        else if(parameter instanceof Boolean)
            ps.setBoolean(i + 1, (Boolean) parameter);
        else if(parameter instanceof Short)
            ps.setShort(i + 1, (Short) parameter);
        else if(parameter instanceof Double)
            ps.setDouble(i + 1, (Double) parameter);
        else if(parameter instanceof Date)
            ps.setDate(i + 1, (Date) parameter);
        else if(parameter instanceof Instant)
            ps.setTimestamp(i + 1, Timestamp.from((Instant) parameter));
        else if(parameter instanceof IDType)
            ps.setLong(i + 1, ((IDType) parameter).getValue());
        else
            throw new SQLException("Unknown type" + parameter.toString());
    }
}

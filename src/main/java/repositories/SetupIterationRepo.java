package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class SetupIterationRepo extends BaseRepo {
    public static Optional<Long> getSetupIterationID(long setupID, int playerCount)
            throws SQLException {
        ResultSet rs = executeQuery("SELECT id FROM setup_iterations WHERE setup_id = ? AND player_count = ? ORDER BY RAND() LIMIT 1",
                setupID,
                playerCount);
        Optional<Long> setupIterationID = Optional.empty();

        while (rs.next()){
            setupIterationID = Optional.of(rs.getLong("id"));
            break;
        }
        rs.close();
        return setupIterationID;
    }
}

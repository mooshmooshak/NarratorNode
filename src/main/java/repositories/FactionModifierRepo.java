package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.Faction;
import models.dbo.ModifierTableNames;
import models.enums.FactionModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.FactionModifierSchema;
import util.game.ModifierUtil;

public class FactionModifierRepo extends ModifierRepo {

    private static ModifierTableNames tableNames = new ModifierTableNames("faction_int_modifiers",
            "faction_bool_modifiers", "faction_string_modifiers");

    private static String getBySetupID(String tableName) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT fim.id, faction_id, fim.name, value, min_player_count, max_player_count, upserted_at ");
        query.append("FROM " + tableName + " AS fim ");
        query.append("LEFT JOIN factions ON factions.id = fim.faction_id ");
        query.append("WHERE setup_id=?;");
        return query.toString();
    }

    private static FactionModifierSchema getFromResult(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        FactionModifierName name = FactionModifierName.valueOf(rs.getString("name").toUpperCase());
        Object value = rs.getObject("value");
        Instant upsertedAt = rs.getTimestamp("upserted_at").toInstant();
        int minPlayerCount = rs.getInt("min_player_count");
        int maxPlayerCount = rs.getInt("max_player_count");
        return new FactionModifierSchema(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public static Future<Map<Long, Set<FactionModifierSchema>>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Map<Long, Set<FactionModifierSchema>>>() {
            @Override
            public Map<Long, Set<FactionModifierSchema>> call() throws Exception {
                Map<Long, Set<FactionModifierSchema>> factionsModifiers = new HashMap<>();

                ResultSet rs;
                long factionID;
                for(String tableName: tableNames.getNames()){
                    rs = executeQuery(getBySetupID(tableName), setupID);
                    while (rs.next()){
                        factionID = rs.getInt("faction_id");
                        if(!factionsModifiers.containsKey(factionID))
                            factionsModifiers.put(factionID, new HashSet<>());
                        factionsModifiers.get(factionID).add(getFromResult(rs));
                    }
                    rs.close();
                }

                return factionsModifiers;
            }
        });
    }

    public static void create(List<Faction> factions) throws SQLException {
        ArrayList<Modifiers<FactionModifierName>> modifiersList = new ArrayList<>();
        ArrayList<Long> factionIDs = new ArrayList<>();
        for(Faction faction: factions){
            modifiersList.add(faction.modifiers);
            factionIDs.add(faction.id);
        }
        insertModifiers(tableNames, "faction_id", factionIDs, modifiersList);
    }

    public static void upsertModifier(Modifier<FactionModifierName> modifier, long factionID) throws SQLException {
        String tableName = getTableName(modifier.name);
        Object value = modifier.value.internalValue;
        String query = "INSERT INTO " + tableName
                + "(faction_id, name, value, min_player_count, max_player_count, upserted_at) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE value = ?, upserted_at = ?;";
        execute(query, factionID, modifier.name.toString(), value, modifier.minPlayerCount, modifier.maxPlayerCount,
                modifier.upsertedAt, value, modifier.upsertedAt);
    }

    private static String getTableName(FactionModifierName name) {
        if(ModifierUtil.IsBoolModifier(name))
            return tableNames.tableBoolName;
        if(ModifierUtil.IsStringModifier(name))
            return tableNames.tableStringName;
        return tableNames.tableIntName;
    }

}

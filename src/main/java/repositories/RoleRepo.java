package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.schemas.RoleSchema;

public class RoleRepo extends BaseRepo {

    public static Future<Set<RoleSchema>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Set<RoleSchema>>() {
            @Override
            public Set<RoleSchema> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Set<RoleSchema> getBySetupIDSync(long setupID) throws SQLException {
        Set<RoleSchema> roles = new HashSet<>();
        String queryText = "SELECT id, name FROM roles WHERE setup_id = ?;";
        ResultSet rs = executeQuery(queryText, setupID);
        RoleSchema roleSchema;
        while (rs.next()){
            roleSchema = new RoleSchema(rs.getLong("id"), rs.getString("name"));
            roleSchema.id = rs.getLong("id");
            roleSchema.name = rs.getString("name");
            roles.add(roleSchema);
        }
        rs.close();
        return roles;
    }

    public static long create(long setupID, String roleName) throws SQLException {
        return create(setupID, new ArrayList<>(Arrays.asList(roleName))).iterator().next();
    }

    public static ArrayList<Long> create(long setupID, ArrayList<String> roleNames) throws SQLException {
        if(roleNames.isEmpty())
            return new ArrayList<>();
        Object[] params = new Object[roleNames.size()];
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO roles (setup_id, name) VALUES");

        String roleName;
        for(int i = 0; i < roleNames.size(); i++){
            roleName = roleNames.get(i);
            if(i != 0)
                query.append(", ");
            query.append("(");
            query.append(setupID);
            query.append(", ?)");
            params[i] = roleName;
        }
        query.append(";");
        return executeInsertQuery(query.toString(), params);
    }

    public static void deleteByIDs(long setupID, long roleID) throws SQLException {
        deleteByIDs(setupID, new ArrayList<>(Arrays.asList(roleID)));
    }

    public static void deleteByIDs(long setupID, ArrayList<Long> roleIDs) throws SQLException {
        if(roleIDs.isEmpty())
            return;
        StringBuilder query = new StringBuilder("");
        query.append("DELETE FROM roles WHERE setup_id = ? AND (");

        long roleID;
        for(int i = 0; i < roleIDs.size(); i++){
            if(i != 0)
                query.append(" OR ");
            roleID = roleIDs.get(i);
            query.append("id = ");
            query.append(roleID);
        }

        query.append(");");
        execute(query.toString(), setupID);
    }
}

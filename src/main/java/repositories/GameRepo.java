package repositories;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.logic.exceptions.NarratorException;
import models.idtypes.GameID;
import models.schemas.GameOverviewSchema;

public class GameRepo extends BaseRepo {

    public static GameOverviewSchema getOverview(GameID gameID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT setup_id, is_finished, seed, instance_id, is_private, is_started FROM replays ");
        query.append("WHERE id = ?;");
        ResultSet rs = executeQuery(query.toString(), gameID);
        if(!rs.next()){
            rs.close();
            throw new NarratorException("No game with that ID found.");
        }

        GameOverviewSchema gameOverview = getFromResult(gameID, rs);

        rs.close();
        return gameOverview;
    }

    private static GameOverviewSchema getFromResult(ResultSet rs) throws SQLException {
        return getFromResult(new GameID(rs.getLong("id")), rs);
    }

    private static GameOverviewSchema getFromResult(GameID gameID, ResultSet rs) throws SQLException {
        return new GameOverviewSchema(gameID, rs.getLong("setup_id"), rs.getString("instance_id"),
                rs.getBoolean("is_started"), rs.getBoolean("is_finished"), rs.getBoolean("is_private"),
                rs.getLong("seed"));
    }

    public static Optional<GameOverviewSchema> getByLobbyID(String lobbyID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT setup_id, is_finished, seed, id, is_private, is_started FROM replays ");
        query.append("WHERE instance_id = ?;");
        ResultSet rs = executeQuery(query.toString(), lobbyID);

        Optional<GameOverviewSchema> game;
        if(rs.next())
            game = Optional.of(new GameOverviewSchema(new GameID(rs.getLong("id")), rs.getLong("setup_id"), lobbyID,
                    rs.getBoolean("is_started"), rs.getBoolean("is_finished"), rs.getBoolean("is_private"),
                    rs.getLong("seed")));
        else
            game = Optional.empty();
        rs.close();
        return game;
    }

    public static long getSetupID(GameID unfinishedReplayID) throws SQLException {
        ResultSet rs = executeQuery("SELECT setup_id FROM replays WHERE id = ?;", unfinishedReplayID);
        rs.next();
        long setupID = rs.getLong("setup_id");
        rs.close();
        return setupID;
    }

    public static Set<GameOverviewSchema> getUnfinished() throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT id, setup_id, is_finished, is_started, seed, is_private, instance_id ");
        query.append("FROM replays WHERE is_finished = false;");
        ResultSet rs = executeQuery(query.toString());
        Set<GameOverviewSchema> games = new HashSet<>();
        while (rs.next())
            games.add(getFromResult(rs));

        rs.close();
        return games;
    }

    public static Set<GameOverviewSchema> getInProgress() throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT id, setup_id, is_finished, is_started, seed, is_private, instance_id ");
        query.append("FROM replays WHERE is_finished = false AND is_started = true;");
        ResultSet rs = executeQuery(query.toString());
        Set<GameOverviewSchema> games = new HashSet<>();
        while (rs.next())
            games.add(getFromResult(rs));

        rs.close();
        return games;
    }

    public static Set<GameID> getFinishedIDs() throws SQLException {
        Set<GameID> ids = new HashSet<>();
        String queryString = "SELECT id FROM replays WHERE is_finished = true;";

        ResultSet rs = executeQuery(queryString);
        while (rs.next())
            ids.add(new GameID(rs.getLong("id")));
        rs.close();

        return ids;
    }

    public static GameID create(String instanceID, boolean isPrivate, long seed, long setupID) throws SQLException {
        Date created_at = new Date(System.currentTimeMillis());
        long gameID = executeInsertQuery(
                "INSERT INTO replays (setup_id, seed, created_at, is_private, instance_id, is_started) "
                        + "VALUES (?, ?, ?, ?, ?, false);",
                setupID, seed, created_at, isPrivate, instanceID).get(0);
        return new GameID(gameID);
    }

    public static ArrayList<GameID> getAllIDs() throws SQLException {
        ResultSet rs = executeQuery("SELECT id FROM replays;");
        ArrayList<Long> gameIDs = new ArrayList<>();
        while (rs.next()){
            gameIDs.add(rs.getLong("id"));
        }
        rs.close();
        return GameID.toIDList(gameIDs);
    }

    public static void setSetupID(GameID gameID, long setupID) throws SQLException {
        execute("UPDATE replays SET setup_id = ? WHERE id = ?;", setupID, gameID);
    }

    public static void setFinished(GameID gameID) throws SQLException {
        execute("UPDATE replays SET is_finished = true WHERE id = ?;", gameID);
    }

    public static void removeLobbyID(GameID gameID) throws SQLException {
        execute("UPDATE replays SET instance_id = NULL WHERE id = ?;", gameID);
    }

    public static void deleteByID(GameID gameID) throws SQLException {
        execute("DELETE FROM replays WHERE id = ?;", gameID);
    }

    public static void deleteAll(Connection c) throws SQLException {
        execute("DELETE FROM replays;");
    }

    public static void setStarted(GameID id) throws SQLException {
        execute("UPDATE replays SET is_started = true where id = ?;", id);
    }

    public static Set<String> getOpenLobbyIDs() throws SQLException {
        ResultSet rs = executeQuery("SELECT instance_id FROM replays WHERE instance_id IS NOT NULL;");
        Set<String> lobbyIDs = new HashSet<>();
        while (rs.next())
            lobbyIDs.add(rs.getString("instance_id"));
        rs.close();
        return lobbyIDs;
    }

    public static Set<GameOverviewSchema> getOpenGames() throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT id, setup_id, is_finished, seed, instance_id, is_private, is_started FROM replays ");
        query.append("WHERE instance_id IS NOT NULL;");
        ResultSet rs = executeQuery(query.toString());
        Set<GameOverviewSchema> games = new HashSet<>();
        while (rs.next())
            games.add(new GameOverviewSchema(new GameID(rs.getLong("id")), rs.getLong("setup_id"),
                    rs.getString("instance_id"), rs.getBoolean("is_started"), rs.getBoolean("is_finished"),
                    rs.getBoolean("is_private"), rs.getLong("seed")));
        rs.close();
        return games;

    }
}

package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.logic.exceptions.NarratorException;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.schemas.PlayerCreateSchema;
import models.schemas.PlayerSchema;
import repositories.util.QueryHelpers;

public class PlayerRepo extends BaseRepo {

    public static Set<PlayerSchema> getByGameID(GameID gameID) throws SQLException {
        ResultSet rs = executeQuery("SELECT id, name, user_id FROM players WHERE replay_id = ?;", gameID);
        Set<PlayerSchema> players = new HashSet<>();
        PlayerSchema player;
        long id;
        String name;

        while (rs.next()){
            id = rs.getLong("id");
            name = rs.getString("name");
            player = new PlayerSchema(new PlayerDBID(id), gameID, name, getUserID(rs));
            players.add(player);
        }
        rs.close();
        return players;
    }

    public static void updateDeathDayAndIsWinner(GameID gameID2, String name, short deathDay, boolean isWinner)
            throws SQLException {
        execute("UPDATE players SET is_winner = ?, death_day = ? WHERE replay_id = ? AND name = ?;", isWinner, deathDay,
                gameID2, name);
    }

    public static Optional<Long> getUserID(GameID gameID, String name) throws SQLException {
        ResultSet rs = executeQuery("SELECT user_id FROM players WHERE replay_id = ? and NAME = ?;", gameID, name);
        if(!rs.next()){
            rs.close();
            return Optional.empty();
        }
        Optional<Long> userID = getUserID(rs);
        rs.close();
        return userID;
    }

    public static PlayerDBID create(GameID gameID, long userID, String name) throws SQLException {
        PlayerCreateSchema playerSchema = new PlayerCreateSchema(name, Optional.of(userID));
        ArrayList<PlayerCreateSchema> set = new ArrayList<>();
        set.add(playerSchema);
        return create(gameID, set).iterator().next();
    }

    public static ArrayList<PlayerDBID> create(GameID gameID, ArrayList<PlayerCreateSchema> players)
            throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO players (replay_id, user_id, name) VALUES");
        List<Object> queryArgs = new LinkedList<>();

        Iterator<PlayerCreateSchema> iterator = players.iterator();
        PlayerCreateSchema player;
        for(int i = 0; i < players.size(); i++){
            if(i != 0)
                query.append(", ");
            player = iterator.next();
            queryArgs.add(gameID);
            query.append("(?, ");

            if(player.userID.isPresent()){
                queryArgs.add(player.userID.get());
                query.append("?");
            }else
                query.append("NULL");

            queryArgs.add(player.name);
            query.append(", ?)");
        }
        query.append(";");
        ArrayList<Long> dbIDs = executeInsertQuery(query.toString(), queryArgs);
        ArrayList<PlayerDBID> playerDBIDs = new ArrayList<>();
        for(long dbID: dbIDs)
            playerDBIDs.add(new PlayerDBID(dbID));
        return playerDBIDs;
    }

    public static void deleteByReplayID(Connection connection, long replayID) throws SQLException {
        execute("DELETE FROM players WHERE replay_id = ?;", replayID);
    }

    public static void clearIsWinnerAndDeathDay() throws SQLException {
        execute("UPDATE players SET is_winner = null, death_day = null;");
    }

    // only used by tests
    public static PlayerSchema getByGameIDAndUserID(GameID gameID, long userID) throws SQLException {
        ResultSet rs = executeQuery("SELECT id, name FROM players WHERE replay_id = ? AND user_id = ?;", gameID,
                userID);
        rs.next();
        PlayerDBID id = new PlayerDBID(rs.getLong("id"));
        PlayerSchema playerSchema = new PlayerSchema(id, gameID, rs.getString("name"), Optional.of(userID));
        rs.close();
        return playerSchema;
    }

    public static void deleteByUserAndGame(long leaverID, GameID gameID) throws SQLException {
        execute("DELETE FROM players WHERE replay_id = ? AND user_id = ?;", gameID, leaverID);
    }

    public static Optional<PlayerSchema> getByNameAndGameID(String playerName, GameID gameID) throws SQLException {
        ResultSet rs = executeQuery("SELECT id, user_id FROM players WHERE replay_id = ? AND name = ?;", gameID,
                playerName);
        if(!rs.next()){
            rs.close();
            return Optional.empty();
        }
        Optional<Long> userID = getUserID(rs);
        long id = rs.getLong("id");
        rs.close();
        return Optional.of(new PlayerSchema(new PlayerDBID(id), gameID, playerName, userID));
    }

    public static Optional<PlayerSchema> getByUserIDGameID(long userID, GameID gameID) throws SQLException {
        ResultSet rs = executeQuery("SELECT id, name FROM players WHERE replay_id = ? AND user_id = ?;", gameID,
                userID);
        if(!rs.next()){
            rs.close();
            return Optional.empty();
        }

        String name = rs.getString("name");
        PlayerDBID id = new PlayerDBID(rs.getLong("id"));
        rs.close();

        return Optional.of(new PlayerSchema(id, gameID, name, Optional.of(userID)));
    }

    public static Set<Long> getActiveUserIDs(Set<PlayerDBID> playerIDs) throws SQLException {
        Set<Long> userIDs = new HashSet<>();
        if(playerIDs.isEmpty())
            return userIDs;
        StringBuilder query = new StringBuilder();
        query.append("SELECT user_id FROM players ");
        query.append("WHERE ");
        query.append(QueryHelpers.ToInClause("players.id", PlayerDBID.toPrimitiveSet(playerIDs)));

        long userID;
        ResultSet rs = executeQuery(query.toString());
        while (rs.next()){
            userID = rs.getLong("user_id");
            if(!rs.wasNull())
                userIDs.add(userID);
        }
        rs.close();
        return userIDs;
    }

    public static Optional<PlayerSchema> getActiveByUserID(long userID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT players.id, players.name, replay_id FROM players ");
        query.append("LEFT JOIN game_users ");
        query.append("ON game_users.user_id = players.user_id AND game_users.game_id = players.replay_id ");
        query.append("LEFT JOIN replays ON players.replay_id = replays.id ");
        query.append("WHERE players.user_id = ? ");
        query.append("AND (game_users.is_exited = false AND !replays.is_finished);");
        ResultSet rs = executeQuery(query.toString(), userID);
        if(!rs.next()){
            rs.close();
            return Optional.empty();
        }

        long id = rs.getLong("id");
        String name = rs.getString("name");
        GameID gameID = new GameID(rs.getLong("replay_id"));

        PlayerSchema player = new PlayerSchema(new PlayerDBID(id), gameID, name, Optional.of(userID));
        rs.close();
        return Optional.of(player);
    }

    public static PlayerSchema getByID(PlayerDBID id) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT replay_id AS game_id, user_id, name FROM players ");
        query.append("WHERE id = ?;");
        ResultSet rs = executeQuery(query.toString(), id);
        if(!rs.next()){
            rs.close();
            throw new NarratorException("Player with that id not found.");
        }
        GameID gameID = new GameID(rs.getLong("game_id"));
        String name = rs.getString("name");
        PlayerSchema player = new PlayerSchema(id, gameID, name, getUserID(rs));
        rs.close();
        return player;
    }

    private static Optional<Long> getUserID(ResultSet rs) throws SQLException {
        long userID = rs.getLong("user_id");
        if(rs.wasNull())
            return Optional.empty();
        return Optional.of(userID);
    }

    public static void updateName(PlayerDBID databaseID, String name) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("UPDATE players SET name = ? ");
        query.append("WHERE id = ?;");
        execute(query.toString(), name, databaseID);
    }

    public static void deleteByID(GameID gameID, PlayerDBID dbID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("DELETE FROM players ");
        query.append("WHERE id = ? AND replay_id = ?;");
        execute(query.toString(), dbID, gameID);
    }
}

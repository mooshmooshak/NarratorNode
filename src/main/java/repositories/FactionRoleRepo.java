package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.FactionRole;
import models.schemas.FactionRoleSchema;

public class FactionRoleRepo extends BaseRepo {

    public static long create(long roleID, long factionID, long setupID) throws SQLException {
        return executeInsertQuery("INSERT INTO faction_roles (faction_id, role_id, setup_id) VALUES (?, ?, ?);",
                factionID, roleID, setupID).get(0);
    }

    public static ArrayList<Long> create(long setupID, List<FactionRole> factionRoles) throws SQLException {
        if(factionRoles.isEmpty())
            return new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO faction_roles (faction_id, role_id, setup_id) VALUES ");
        for(FactionRole factionRole: factionRoles){
            query.append("(");
            query.append(factionRole.faction.id);
            query.append(", ");
            query.append(factionRole.role.id);
            query.append(", ");
            query.append(setupID);
            query.append("), ");
        }
        query.replace(query.length() - 2, query.length(), ";");
        return executeInsertQuery(query.toString());
    }

    public static Future<Set<FactionRoleSchema>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Set<FactionRoleSchema>>() {
            @Override
            public Set<FactionRoleSchema> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Set<FactionRoleSchema> getBySetupIDSync(long setupID) throws SQLException {
        ResultSet results = executeQuery(
                "SELECT faction_id, role_id, id, received_faction_role_id FROM faction_roles WHERE setup_id = ?;",
                setupID);
        Set<FactionRoleSchema> factionRoles = new HashSet<>();
        FactionRoleSchema factionRole;
        while (results.next()){
            factionRole = new FactionRoleSchema();
            factionRole.factionID = results.getLong("faction_id");
            factionRole.roleID = results.getLong("role_id");
            factionRole.id = results.getLong("id");
            factionRole.receivedFactionRoleID = Optional.of(results.getLong("received_faction_role_id"));
            if(results.wasNull())
                factionRole.receivedFactionRoleID = Optional.empty();
            factionRoles.add(factionRole);
        }
        return factionRoles;
    }

    public static void updateRoleReceived(long setupID, long factionRoleId, long receivedFactionRoleID)
            throws SQLException {
        execute("UPDATE faction_roles SET received_faction_role_id = ? WHERE setup_id = ? AND id = ? ;",
                receivedFactionRoleID, setupID, factionRoleId);
    }

    public static void removeRoleReceived(long setupID, long factionRoleId) throws SQLException {
        execute("UPDATE faction_roles SET received_faction_role_id = NULL WHERE setup_id = ? AND id = ? ;", setupID,
                factionRoleId);
    }

    public static void deleteSetRoleReceive(long factionRoleID, long setupID) throws SQLException {
        execute("DELETE FROM faction_roles WHERE setup_id = ? AND received_faction_role_id = ?", setupID,
                factionRoleID);
    }

    public static void delete(long setupID, long factionRoleID) throws SQLException {
        deleteByIDs(setupID, new ArrayList<>(Arrays.asList(factionRoleID)));
    }

    public static void deleteByIDs(long setupID, ArrayList<Long> factionRoleIDs) throws SQLException {
        if(factionRoleIDs.isEmpty())
            return;
        StringBuilder query = new StringBuilder("");
        query.append("DELETE FROM faction_roles WHERE setup_id = ? AND (");

        long factionRoleID;
        for(int i = 0; i < factionRoleIDs.size(); i++){
            if(i != 0)
                query.append(" OR ");
            factionRoleID = factionRoleIDs.get(i);
            query.append("id = ");
            query.append(factionRoleID);
        }

        query.append(");");
        execute(query.toString(), setupID);
    }

}

package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import models.ModifierValue;
import models.enums.GameModifierName;
import models.idtypes.GameID;
import models.modifiers.GameModifier;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import util.game.ModifierUtil;

public class GameModifierRepo extends ModifierRepo {

    public static void insert(GameID gameID, Modifiers<GameModifierName> gameModifiers) throws SQLException {
        StringBuilder boolQuery = new StringBuilder();
        StringBuilder intQuery = new StringBuilder();
        boolQuery.append("INSERT INTO game_bool_modifiers (game_id, name, value) VALUES");
        intQuery.append("INSERT INTO game_int_modifiers (game_id, name, value) VALUES");
        List<Modifier<GameModifierName>> boolModifiers = new LinkedList<>();
        List<Modifier<GameModifierName>> intModifiers = new LinkedList<>();
        for(Modifier<GameModifierName> modifier: gameModifiers){
            if(ModifierUtil.IsIntModifier(modifier.name))
                intModifiers.add(modifier);
            else if(ModifierUtil.IsBoolModifier(modifier.name))
                boolModifiers.add(modifier);
        }

        Modifier<GameModifierName> modifier;
        Iterator<Modifier<GameModifierName>> iterator = intModifiers.iterator();
        List<Object> intArgs = new LinkedList<>();
        for(int i = 0; i < intModifiers.size(); i++){
            if(i != 0)
                intQuery.append(", ");
            intQuery.append("(?, ?, ?)");
            modifier = iterator.next();
            intArgs.add(gameID);
            intArgs.add(modifier.name.toString());
            intArgs.add(modifier.value.internalValue);
        }
        intQuery.append(";");
        executeIfArgs(intQuery, intArgs);

        iterator = boolModifiers.iterator();
        List<Object> boolArgs = new LinkedList<>();
        for(int i = 0; i < boolModifiers.size(); i++){
            if(i != 0)
                boolQuery.append(", ");
            boolQuery.append("(?, ?, ?)");
            modifier = iterator.next();
            boolArgs.add(gameID);
            boolArgs.add(modifier.name.toString());
            boolArgs.add(modifier.value.internalValue);
        }
        boolQuery.append(";");
        executeIfArgs(boolQuery, boolArgs);
    }

    public static Set<Modifier<GameModifierName>> getByGameID(GameID gameID) throws SQLException {
        Set<Modifier<GameModifierName>> modifiers = new HashSet<>();

        ResultSet rs = executeQuery("SELECT name, value FROM game_bool_modifiers WHERE game_id = ?;", gameID);

        GameModifierName name;
        ModifierValue value;
        while (rs.next()){
            name = GameModifierName.valueOf(rs.getString("name").toUpperCase());
            value = new ModifierValue(rs.getBoolean("value"));
            modifiers.add(new GameModifier(name, value));
        }

        rs.close();
        rs = executeQuery("SELECT name, value FROM game_int_modifiers WHERE game_id = ?;", gameID);

        while (rs.next()){
            name = GameModifierName.valueOf(rs.getString("name").toUpperCase());
            value = new ModifierValue(rs.getInt("value"));
            modifiers.add(new GameModifier(name, value));
        }

        rs.close();

        return modifiers;
    }

    public static void upsertModifier(Modifier<GameModifierName> modifier, GameID gameID) throws SQLException {
        Object value = modifier.value.internalValue;
        String tableName = getTableName(modifier.name);
        String query = "INSERT INTO " + tableName
                + " (game_id, name, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value = ?;";
        execute(query, gameID, modifier.name.toString(), value, value);
    }

    private static String getTableName(GameModifierName name) {
        if(ModifierUtil.IsBoolModifier(name))
            return "game_bool_modifiers";
        return "game_int_modifiers";
    }
}

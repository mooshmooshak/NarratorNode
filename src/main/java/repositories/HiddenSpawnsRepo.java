package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import game.logic.exceptions.NarratorException;
import models.enums.SqlErrorCodes;
import models.requests.HiddenSpawnCreateRequest;
import models.schemas.HiddenSpawnSchema;

public class HiddenSpawnsRepo extends BaseRepo {

    public static Future<Set<HiddenSpawnSchema>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Set<HiddenSpawnSchema>>() {
            @Override
            public Set<HiddenSpawnSchema> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Set<HiddenSpawnSchema> getBySetupIDSync(long setupID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT id, hidden_id, faction_role_id, min_player_count, max_player_count FROM hidden_spawns ");
        query.append("WHERE setup_id = ?;");
        ResultSet rs = executeQuery(query.toString(), setupID);
        Set<HiddenSpawnSchema> spawns = new HashSet<>();

        long hiddenID;
        HiddenSpawnSchema spawnSchema;
        while (rs.next()){
            hiddenID = rs.getLong("hidden_id");
            spawnSchema = new HiddenSpawnSchema(rs.getLong("id"), hiddenID, rs.getLong("faction_role_id"),
                    rs.getInt("min_player_count"), rs.getInt("max_player_count"));
            spawns.add(spawnSchema);
        }
        rs.close();
        return spawns;
    }

    public static ArrayList<Long> create(long setupID, List<HiddenSpawnCreateRequest> hiddenSpawns)
            throws SQLException {
        if(hiddenSpawns.isEmpty())
            return new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append(
                "INSERT INTO hidden_spawns (hidden_id, faction_role_id, min_player_count, max_player_count, setup_id) VALUES ");

        for(HiddenSpawnCreateRequest hiddenSpawn: hiddenSpawns){
            query.append("(");
            query.append(hiddenSpawn.hiddenID);
            query.append(", ");
            query.append(hiddenSpawn.factionRoleID);
            query.append(", ");
            query.append(hiddenSpawn.minPlayerCount);
            query.append(", ");
            query.append(hiddenSpawn.maxPlayerCount);
            query.append(", ");
            query.append(setupID);
            query.append("), ");
        }

        query.replace(query.length() - 2, query.length(), ";");
        try{
            return executeInsertQuery(query.toString());
        }catch(SQLIntegrityConstraintViolationException e){
            if(e.getErrorCode() == SqlErrorCodes.DUPLICATE_ENTRY)
                throw new NarratorException("Hidden spawn definition already exists!");
            throw e;
        }
    }

    public static void delete(long setupID, long hiddenSpawnID) throws SQLException {
        execute("DELETE FROM hidden_spawns WHERE setup_id = ? AND id = ?;", setupID, hiddenSpawnID);
    }

}

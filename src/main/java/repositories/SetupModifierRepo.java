package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.ModifierValue;
import models.dbo.ModifierTableNames;
import models.enums.SetupModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.modifiers.SetupModifier;
import util.game.ModifierUtil;

public class SetupModifierRepo extends ModifierRepo {

    private static ModifierTableNames tableNames = new ModifierTableNames("setup_int_modifiers", "setup_bool_modifiers",
            "setup_string_modifiers");

    public static void insert(long setupID, Modifiers<SetupModifierName> setupModifiers) throws SQLException {
        ArrayList<Long> setupIDList = new ArrayList<>();
        setupIDList.add(setupID);
        ArrayList<Modifiers<SetupModifierName>> modifiers = new ArrayList<>();
        modifiers.add(setupModifiers);
        insertModifiers(tableNames, "setup_id", setupIDList, modifiers);
    }

    public static Future<Collection<Modifier<SetupModifierName>>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Collection<Modifier<SetupModifierName>>>() {
            @Override
            public Collection<Modifier<SetupModifierName>> call() throws Exception {
                List<Modifier<SetupModifierName>> modifiers = new LinkedList<>();

                for(String tableName: tableNames.getNames()){
                    ResultSet rs = executeQuery(getQuery(tableName), setupID);
                    while (rs.next())
                        modifiers.add(getFromResult(rs));

                    rs.close();
                }

                return modifiers;
            }
        });

    }

    private static String getQuery(String tableName) {
        return "SELECT id, name, value, min_player_count, max_player_count, upserted_at FROM " + tableName
                + " WHERE setup_id = ?;";
    }

    private static SetupModifier getFromResult(ResultSet rs) throws SQLException {
        SetupModifierName name = SetupModifierName.valueOf(rs.getString("name").toUpperCase());
        ModifierValue value = new ModifierValue(rs.getObject("value"));
        long id = rs.getLong("id");
        int minPlayerCount = rs.getInt("min_player_count");
        int maxPlayerCount = rs.getInt("max_player_count");
        Instant upsertedAt = rs.getTimestamp("upserted_at").toInstant();
        return new SetupModifier(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public static void upsertSetupModifier(Modifier<SetupModifierName> modifier, long setupID) throws SQLException {
        String tableName = getTableName(modifier.name);
        Object value = modifier.value.internalValue;
        execute("INSERT INTO " + tableName
                + " (setup_id, name, value, min_player_count, max_player_count, upserted_at) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE value = ?, upserted_at = ?;",
                setupID, modifier.name.toString(), value, modifier.minPlayerCount, modifier.maxPlayerCount,
                modifier.upsertedAt, value, modifier.upsertedAt);
    }

    private static String getTableName(SetupModifierName name) {
        if(ModifierUtil.IsBoolModifier(name))
            return "setup_bool_modifiers";
        if(ModifierUtil.IsStringModifier(name))
            return "setup_string_modifiers";
        return "setup_int_modifiers";
    }

}

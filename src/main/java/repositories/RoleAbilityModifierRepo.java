package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.dbo.ModifierTableNames;
import models.enums.AbilityModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.AbilityModifierSchema;
import util.game.ModifierUtil;

public class RoleAbilityModifierRepo extends ModifierRepo {

    private static ModifierTableNames tableNames = new ModifierTableNames("role_ability_int_modifiers",
            "role_ability_bool_modifiers", "role_ability_string_modifiers");

    private static AbilityModifierSchema getResult(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        AbilityModifierName name = AbilityModifierName.valueOf(rs.getString("name").toUpperCase());
        int minPlayerCount = rs.getInt("min_player_count");
        int maxPlayerCount = rs.getInt("max_player_count");
        Instant upsertedAt = rs.getTimestamp("upserted_at").toInstant();
        Object value = rs.getObject("value");
        return new AbilityModifierSchema(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    private static String getByRoleAbilityQuery(String tableName) {
        return "SELECT id, name, value, min_player_count, upserted_at, max_player_count FROM " + tableName
                + " WHERE role_ability_id=?;";
    }

    public static Set<AbilityModifierSchema> getByRoleAbilityID(long roleAbilityID) throws SQLException {
        Set<AbilityModifierSchema> modifiers = new HashSet<>();

        ResultSet rs;
        for(String tableName: tableNames.getNames()){
            rs = executeQuery(getByRoleAbilityQuery(tableName), roleAbilityID);
            while (rs.next())
                modifiers.add(getResult(rs));
            rs.close();
        }

        return modifiers;
    }

    private static String getBySetupIDQuery(String tableName) {
        StringBuilder query = new StringBuilder();
        query.append(
                "SELECT raim.id, role_id, role_ability_id, raim.name, value, min_player_count, max_player_count, upserted_at  ");
        query.append("FROM " + tableName + " AS raim ");
        query.append("INNER JOIN role_abilities ");
        query.append("ON role_abilities.id = raim.role_ability_id ");
        query.append("INNER JOIN roles ");
        query.append("ON roles.id = role_abilities.role_id ");
        query.append("WHERE setup_id = ?;");
        return query.toString();
    }

    public static Future<Map<Long, Map<Long, Set<AbilityModifierSchema>>>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Map<Long, Map<Long, Set<AbilityModifierSchema>>>>() {
            @Override
            public Map<Long, Map<Long, Set<AbilityModifierSchema>>> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Map<Long, Map<Long, Set<AbilityModifierSchema>>> getBySetupIDSync(long setupID) throws SQLException {
        Map<Long, Map<Long, Set<AbilityModifierSchema>>> modifiers = new HashMap<>();

        long roleID, roleAbilityID;
        ResultSet rs;
        for(String tableName: tableNames.getNames()){
            rs = executeQuery(getBySetupIDQuery(tableName), setupID);
            while (rs.next()){
                roleID = rs.getLong("role_id");
                roleAbilityID = rs.getLong("role_ability_id");
                makeSpaceInMap(modifiers, roleID, roleAbilityID);
                modifiers.get(roleID).get(roleAbilityID).add(getResult(rs));
            }
            rs.close();
        }

        return modifiers;
    }

    private static void makeSpaceInMap(Map<Long, Map<Long, Set<AbilityModifierSchema>>> modifiers, long roleID,
            long roleAbilityID) {
        if(!modifiers.containsKey(roleID))
            modifiers.put(roleID, new HashMap<>());
        Map<Long, Set<AbilityModifierSchema>> factionAbilities = modifiers.get(roleID);
        if(!factionAbilities.containsKey(roleAbilityID))
            factionAbilities.put(roleAbilityID, new HashSet<>());
    }

    public static void create(ArrayList<Long> roleAbilityIDs, ArrayList<Modifiers<AbilityModifierName>> modifiersList)
            throws SQLException {
        insertModifiers(tableNames, "role_ability_id", roleAbilityIDs, modifiersList);
    }

    public static void upsert(long roleAbilityID, Modifier<AbilityModifierName> modifier) throws SQLException {
        String tableName = getTableName(modifier.name);
        Object value = modifier.value.internalValue;
        execute("INSERT INTO " + tableName
                + " (role_ability_id, name, value, min_player_count, max_player_count, upserted_at) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE value = ?, upserted_at = ?;",
                roleAbilityID, modifier.name.toString(), value, modifier.minPlayerCount, modifier.maxPlayerCount,
                modifier.upsertedAt, value, modifier.upsertedAt);
    }

    private static String getTableName(AbilityModifierName name) {
        if(ModifierUtil.IsIntModifier(name))
            return tableNames.tableIntName;
        if(ModifierUtil.IsStringModifier(name))
            return tableNames.tableStringName;
        return tableNames.tableBoolName;
    }
}

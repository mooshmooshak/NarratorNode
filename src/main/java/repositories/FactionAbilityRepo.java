package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import game.abilities.util.AbilityUtil;
import game.logic.exceptions.NarratorException;
import models.enums.SqlErrorCodes;
import models.requests.AbilityCreateRequest;
import models.schemas.AbilitySchema;

public class FactionAbilityRepo extends AbilityRepo {

    public static Future<Map<Long, Set<AbilitySchema>>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Map<Long, Set<AbilitySchema>>>() {
            @Override
            public Map<Long, Set<AbilitySchema>> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    private static Map<Long, Set<AbilitySchema>> getBySetupIDSync(long setupID) throws SQLException {
        Map<Long, Set<AbilitySchema>> abilities = new HashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT faction_id, fa.id, fa.name FROM faction_abilities AS fa ");
        query.append("LEFT JOIN factions ON factions.id = fa.faction_id ");
        query.append("WHERE setup_id=?;");
        ResultSet rs = executeQuery(query.toString(), setupID);

        long factionID;
        AbilitySchema ability;
        while (rs.next()){
            factionID = rs.getLong("faction_id");
            ability = new AbilitySchema(rs.getLong("id"), AbilityUtil.getAbilityTypeByDbName(rs.getString("name")));
            if(!abilities.containsKey(factionID))
                abilities.put(factionID, new HashSet<>());
            abilities.get(factionID).add(ability);
        }
        rs.close();

        return abilities;
    }

    public static long create(long factionID, AbilityCreateRequest ability) throws SQLException {
        return create(new ArrayList<>(Arrays.asList(factionID)), new ArrayList<>(Arrays.asList(ability))).get(0);
    }

    public static ArrayList<Long> create(ArrayList<Long> factionAbilityFactionIDs,
            ArrayList<AbilityCreateRequest> factionAbilities) throws SQLException {
        try{
            return insertAbilities("faction_abilities", "faction_id", factionAbilityFactionIDs, factionAbilities);
        }catch(SQLException e){
            if(e.getErrorCode() != SqlErrorCodes.DUPLICATE_ENTRY)
                throw e;
            throw new NarratorException("This faction ability already exists!");
        }
    }

    public static void delete(long factionID, long abilityID) throws SQLException {
        execute("DELETE FROM faction_abilities WHERE id = ? AND faction_id = ?;", abilityID, factionID);
    }

}

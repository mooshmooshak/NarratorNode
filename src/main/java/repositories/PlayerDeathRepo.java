package repositories;

import java.sql.SQLException;

import models.idtypes.GameID;

public class PlayerDeathRepo extends BaseRepo {

    public static void createDeath(GameID gameID, String name, String deathType) throws SQLException {
        execute("INSERT INTO player_deaths (player_id, death_type) SELECT id, ? FROM players WHERE replay_id = ? AND name = ?;",
                deathType, gameID, name);

    }

    public static void deleteAll() throws SQLException {
        execute("DELETE FROM player_deaths;");
    }

}

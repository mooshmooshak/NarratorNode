package util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import game.logic.support.Random;

public class Util {

    static Random random = new Random();

    public static boolean notEqual(Object o, Object p) {
        if(o == null){
            if(p != null){
                return true;
            }
        }else{
            if(!o.equals(p))
                return true;
        }
        return false;
    }

    public static void mash(ArrayList<String> ret, String[] strings) {
        for(String s: strings)
            ret.add(s);
    }

    public static String[] ToStringArray(String s) {
        return new String[] {
                s
        };
    }

    public static String[] toStringArray(List<String> ret) {
        return ret.toArray(new String[ret.size()]);
    }

    public static Object[] toObjectArray(List<Object> ret) {
        return ret.toArray(new Object[ret.size()]);
    }

    public static List<String> toStringList(String... ts) {
        List<String> list = new LinkedList<>();
        for(String t: ts){
            list.add(t);
        }
        return list;
    }

    public static String TitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for(char c: input.toCharArray()){
            if(Character.isSpaceChar(c)){
                nextTitleCase = true;
            }else if(nextTitleCase){
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    public static boolean isInt(String s) {
        if(s == null)
            return false;
        final int RADIX = 10;
        if(s.isEmpty())
            return false;
        for(int i = 0; i < s.length(); i++){
            if(i == 0 && s.charAt(i) == '-'){
                if(s.length() == 1)
                    return false;
                continue;
            }
            if(Character.digit(s.charAt(i), RADIX) < 0)
                return false;
        }
        return true;
    }

    public static void printStackTrace() {
        System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        for(StackTraceElement se: Thread.currentThread().getStackTrace()){
            System.err.println(se);
        }
    }

    public static boolean vowelStart(String role) {
        char c = role.charAt(0);
        c = Character.toLowerCase(c);
        return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');

    }

    public static String plural(String string, int days) {
        if(days == 1)
            return string;
        return string + "s";
    }

    public static boolean hasNonAlphaNumericString(String name) {
        Pattern p = Pattern.compile("[^a-zA-Z0-9 ]");
        return p.matcher(name).find();
    }

    public static String stripNonAlphaNumericCharacters(String name) {
        return stripNonAlphaNumericCharacters(name, false);
    }

    public static String stripNonAlphaNumericCharacters(String name, boolean spaceOkay) {
        if(spaceOkay)
            return name.replaceAll("[^A-Za-z0-9 ]", "");
        return name.replaceAll("[^A-Za-z0-9]", "");
    }

    public static String SpacedString(String... list) {
        if(list.length == 0)
            return "";
        if(list.length == 1)
            return list[0];
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < list.length; i++){
            if(i != 0)
                sb.append(", ");
            sb.append(list[i]);
        }
        return sb.toString();
    }

    public static String SpacedString(List<String> list) {
        if(list.isEmpty())
            return "";
        if(list.size() == 1)
            return list.get(0);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < list.size(); i++){
            if(i != 0)
                sb.append(", ");
            sb.append(list.get(i));
        }
        return sb.toString();
    }

    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) {
        if(c != null && string != null){
            try{
                return Enum.valueOf(c, string.trim().toUpperCase());
            }catch(IllegalArgumentException ex){
            }
        }
        return null;
    }

    public static int partialContains(List<String> list, String message) {
        int count = 0;
        for(String s: list){
            if(s.contains(message))
                count++;
        }
        return count;
    }

    public static void log(String message) {
        System.err.println(message);
    }

    public static void log(Throwable e, String... string) {
        e.printStackTrace();
    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static SecureRandom rnd = new SecureRandom();

    public static String getRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for(int i = 0; i < length; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static long getID() {
        return Math.abs(random.nextLong()) * -1;
    }
}

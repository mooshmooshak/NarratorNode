package util.game;

import game.logic.Game;
import models.enums.GameModifierName;

public class PhaseUtil {
    public static int getDayLength(Game narrator) {
        int lostTime = (narrator.getDayNumber() - 1) * narrator.getInt(GameModifierName.DAY_LENGTH_DECREASE);
        int dayLength = narrator.getInt(GameModifierName.DAY_LENGTH_START) - lostTime;
        return Math.max(narrator.getInt(GameModifierName.DAY_LENGTH_MIN), dayLength);
    }
}

package util.game;

import java.util.Iterator;
import java.util.Set;

import game.logic.support.Random;

public class RandomUtil {
    public static <T> T getElem(Random random, Set<T> set) {
        Iterator<T> iterator = set.iterator();
        int pops = random.nextInt(set.size());
        for(; pops > 0; --pops)
            iterator.next();
        return iterator.next();
    }

    public static long getLong() {
        return new Random().nextLong();
    }
}

package util.game;

import java.util.ArrayList;
import java.util.LinkedList;

import game.abilities.Puppet;
import game.abilities.Vote;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;

public class VoteUtil {
    public static VoteAnnouncement getVoteAnnouncement(Player voter, PlayerList targets, PlayerList previousTargets) {
        VoteAnnouncement e = new VoteAnnouncement(previousTargets, voter, targets);

        PlayerList addedVoters = targets.compliment(previousTargets);
        PlayerList removedVoters = previousTargets.compliment(targets);

        return addText(e, addedVoters, removedVoters);
    }

    public static VoteAnnouncement getUnvoteAnnouncement(Action action) {
        Player voter = action.owner;
        PlayerList prevTargets = action.getTargets();
        VoteAnnouncement e = new VoteAnnouncement(prevTargets, voter);

        return addText(e, new PlayerList(), prevTargets);
    }

    private static VoteAnnouncement addText(VoteAnnouncement e, PlayerList addedVoters, PlayerList removedVoters) {
        StringChoice sc = new StringChoice(e.voter);
        sc.add(e.voter, "You");
        e.add(sc);

        ArrayList<Object> added = addedVoters.isNonempty() ? StringChoice.YouYourself("you", addedVoters) : null;
        ArrayList<Object> removed = removedVoters.isNonempty() ? StringChoice.YouYourself("you", removedVoters) : null;

        if(addedVoters.isNonempty() && removedVoters.isNonempty()){
            StringChoice theirYour = new StringChoice("their").add(e.voter, "your");
            e.add(" changed ", theirYour, " vote from ", removed, " to ", added);
        }

        else if(addedVoters.isNonempty())
            e.add(" voted for ", added);

        else if(removedVoters.isNonempty())
            e.add(" unvoted ", removed);

        e.add(".");
        return e;
    }

    public static Action skipAction(Player player, double timeLeft) {
        return new Action(player, Vote.abilityType, timeLeft, new LinkedList<>(), new PlayerList(player.getSkipper()));
    }

    public static Action voteAction(Player voter, PlayerList targets) {
        return new Action(voter, Vote.abilityType, new LinkedList<>(), targets);
    }

    public static int getVentVoteActionIndex(Player ventPlayer, Player puppet) {
        Game narrator = ventPlayer.game;
        int count = -1;
        Action action;
        for(Action element: narrator.actionStack){
            action = element;
            if(action.owner != ventPlayer)
                continue;
            count++;
            if(action.abilityType == Puppet.abilityType && Vote.COMMAND.equals(action.getArg1())
                    && action.getTarget() == puppet)
                break;
        }
        return count;
    }
}

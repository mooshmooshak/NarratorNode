package util.game;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.ai.Computer;
import game.logic.Game;
import game.setups.Setup;
import models.ModifierValue;
import models.enums.GameModifierName;
import models.idtypes.GameID;
import models.modifiers.GameModifier;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.GameOverviewSchema;

public class GameUtil {
    public static Optional<Integer> getPlayerCount(Optional<Game> game) {
        if(game.isPresent())
            return Optional.of(game.get().players.size());
        return Optional.empty();
    }

    public static Game getGeneric(Setup setup, int playerCount, Modifiers<GameModifierName> modifiers) {
        Game game = new Game(setup);

        String name;
        for(int i = 1; i <= playerCount; i++){
            name = Computer.toLetter(i);
            game.addPlayer(name);
        }

        for(Modifier<GameModifierName> modifier: modifiers)
            game.gameModifiers.modifiers.add(modifier);

        return game;
    }

    public static Modifiers<GameModifierName> getPermissiveModifiers() {
        Modifiers<GameModifierName> modifiers = new Modifiers<>();
        modifiers.add(new GameModifier(GameModifierName.CHAT_ROLES, new ModifierValue(true)));
        return modifiers;
    }

    public static Set<GameID> getGameIDs(Set<GameOverviewSchema> games) {
        Set<GameID> gameIDs = new HashSet<>();
        for(GameOverviewSchema game: games)
            gameIDs.add(game.id);
        return gameIDs;
    }
}

package util.game;

import game.logic.Game;
import game.logic.support.Constants;
import models.enums.SetupModifierName;

public class ChargeUtil {

    public static int AddNoise(int charge, Game n) {
        if(charge == Constants.UNLIMITED)
            return Constants.UNLIMITED;
        int variablity = n.getInt(SetupModifierName.CHARGE_VARIABILITY);
        if(variablity == 0)
            return charge;
        int oldCharge = charge;
        charge = n.getRandom().nextGaussian() * variablity + charge;
        if(charge == 0 && n.getRandom().nextBoolean()){
            if(n.getRandom().nextDouble() * oldCharge < 1)
                charge = Constants.UNLIMITED;
            else
                charge = 1;
        }else if(charge < 0)
            charge = Constants.UNLIMITED;
        else
            charge++;
        return charge;
    
    }

}

package util.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game.logic.Game;
import models.FactionRole;
import models.enums.AbilityType;
import models.enums.GameModifierName;

public class FactionRoleUtil {
    public static List<FactionRole> getList(FactionRole... memberArray) {
        ArrayList<FactionRole> memberList = new ArrayList<>();
        for(FactionRole m: memberArray)
            memberList.add(m);

        return memberList;
    }

    public static Set<Long> getRoleIDs(Set<FactionRole> factionRoles) {
        Set<Long> roleIDs = new HashSet<>();
        for(FactionRole factionRole: factionRoles)
            roleIDs.add(factionRole.role.id);
        return roleIDs;
    }

    public static boolean hasFactionRoleWithAbility(Set<FactionRole> factionRoles, AbilityType abilityType) {
        for(FactionRole factionRole: factionRoles)
            if(factionRole.role.hasAbility(abilityType))
                return true;
        return false;
    }

    public static Set<FactionRole> filterChatRoles(Set<FactionRole> allFactionRoles, Game game) {
        if(game.gameModifiers.getBoolean(GameModifierName.CHAT_ROLES))
            return new HashSet<>(allFactionRoles);
        Set<FactionRole> nonChatFactionRoles = new HashSet<>();
        for(FactionRole factionRole: allFactionRoles)
            if(!factionRole.role.isChatRole())
                nonChatFactionRoles.add(factionRole);
        return nonChatFactionRoles;
    }
}

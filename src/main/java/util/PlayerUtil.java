package util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.logic.Player;
import models.idtypes.PlayerDBID;
import models.schemas.PlayerSchema;

public class PlayerUtil {
    public static Optional<PlayerDBID> getDBID(Optional<Player> player) {
        if(player.isPresent())
            return Optional.of(player.get().databaseID);
        return Optional.empty();
    }

    public static Optional<PlayerDBID> getDBIDFromSchema(Optional<PlayerSchema> schema) {
        if(schema.isPresent())
            return Optional.of(schema.get().id);
        return Optional.empty();
    }

    public static Optional<String> getName(Optional<PlayerSchema> playerSchema) {
        if(playerSchema.isPresent())
            return Optional.of(playerSchema.get().name);
        return Optional.empty();
    }

    public static Set<String> getNames(Collection<PlayerSchema> players) {
        HashSet<String> names = new HashSet<>();
        for(PlayerSchema player: players)
            names.add(player.name);
        return names;
    }

    public static Set<Long> getUserIDs(Set<PlayerSchema> players) {
        Set<Long> userIDs = new HashSet<>();
        for(PlayerSchema player: players){
            if(player.userID.isPresent())
                userIDs.add(player.userID.get());
        }
        return userIDs;
    }
}

package nnode;

import java.util.HashMap;
import java.util.Set;

import json.JSONException;
import json.JSONObject;

public class UnreadManager {

    String logName;
    public HashMap<Long, Integer> unreadCount;

    public UnreadManager(String name, Set<Long> userIDs) {
        unreadCount = new HashMap<>();
        for(long userID: userIDs)
            unreadCount.put(userID, 0);
        this.logName = name;
    }

    public Set<Long> getUserIDs() {
        return unreadCount.keySet();
    }

    public void pushExcept(Set<Long> toExclude) {
        for(long userID: unreadCount.keySet()){
            if(toExclude.contains(userID))
                continue;
            increment(userID);
        }
        for(long userID: getUserIDs()){
            if(!toExclude.contains(userID))
                sendUnread(userID);
        }
    }

    public void moreUnread(long userID) {
        if(!unreadCount.containsKey(userID))
            unreadCount.put(userID, 0);
        increment(userID);
        sendUnread(userID);
    }

    private void increment(long userID) {
        int former = unreadCount.get(userID);
        unreadCount.put(userID, former + 1);
    }

    public void sendUnread(long userID) {
        try{
            JSONObject jo = new JSONObject();
            jo.put(StateObject.unreadUpdate, logName);
            jo.put(StateObject.count, unreadCount.get(userID));
            WebCommunicator.pushToUser(jo, userID);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public void setRead(long userID) {
        if(unreadCount.containsKey(userID))
            unreadCount.put(userID, 0);
    }

    public int unreadCount(long userID) {
        if(unreadCount.containsKey(userID))
            return unreadCount.get(userID);
        return 0;
    }

    public void addUser(long userID) {
        unreadCount.put(userID, 0);
    }

    public void removeUser(long userID) {
        unreadCount.remove(userID);
    }

    public int remove(long userID) {
        if(!unreadCount.containsKey(userID))
            return 0;
        return unreadCount.remove(userID);
    }

    public void add(long userID, int unreads) {
        unreadCount.put(userID, unreads);
    }

}

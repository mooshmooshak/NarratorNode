package nnode;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

import controllers.HttpController;
import game.event.EventDecoder;
import game.logic.support.AlternateLookup;
import game.logic.support.StoryPackage;
import game.logic.templates.HTMLDecoder;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.schemas.GameOverviewSchema;
import repositories.Connection;
import repositories.GameRepo;
import repositories.GameUserRepo;
import util.Util;

public class LobbyManager {
    public static HashMap<String, Lobby> idToLobby = new HashMap<>(); // these are in progress
    public static SwitchListener switchListener;

    public static final String[] BAD_WORDS = { "ANUS", "ARSE", "ASSS", "CLIT", "COCK", "COON", "CUNT", "DAGO", "DAMN",
            "DICK", "DIKE", "DYKE", "FUCK", "GOOK", "HEEB", "HELL", "HOMO", "JIZZ", "KIKE", "KUNT", "KYKE", "MICK",
            "MUFF", "PAKI", "PISS", "POON", "PUTO", "SHIT", "SHIZ", "SLUT", "SMEG", "SPIC", "TARD", "TITS", "TWAT",
            "WANK", "NIGG", "GIZZ", "SMDK" };

    public interface NodeConnectionRefresh {
        void onNewConnection(Connection c);
    }

    public void start() {
        Scanner scan = new Scanner(System.in);

        while (scan.hasNextLine()){

            String message = scan.nextLine().replace("\n", "");
            try{
                handleMessage(message);
            }catch(JSONException e){
                System.err.println("[ " + message + " ] was invalid");
                e.printStackTrace();
            }catch(NullPointerException f){
                f.printStackTrace();
            }catch(Throwable t){
                t.printStackTrace();
                break;
            }

        }
        System.err.println("java ending");

        scan.close();
    }

    public static JSONObject handleMessage(JSONObject nodeRequest) throws JSONException {
        if(nodeRequest.has(StateObject.httpRequest) && nodeRequest.getBoolean(StateObject.httpRequest))
            return HttpController.handleHTTPRequest(nodeRequest);

        handlePlayerMessage(nodeRequest);

        return null;
    }

    public static synchronized void handleMessage(String message) throws JSONException, SQLException {
        JSONObject nodeRequest = new JSONObject(message);
        handleMessage(nodeRequest);
    }

    // could be optimized by not selecting 'bad numbers' first

    public static String registerInstanceID(String id, Lobby i) {
        idToLobby.put(id, i);
        return id;
    }

    public static void handlePlayerMessage(JSONObject jo) throws JSONException {
        long userID = jo.getLong(StateObject.userID);

        try{
            Optional<GameID> gameID = GameUserRepo.getActiveGameIDByUserID(userID);
            if(gameID.isPresent())
                Lobby.handlePlayerMessage(userID, gameID.get(), jo);
        }catch(SQLException e){
            WebCommunicator.pushWarningToUser(e.getMessage(), userID);
            LobbyManager.internalError(e);
        }
        return;
    }

    public interface SwitchListener {
        void onSwitchMessage(JSONObject jo);
    }

    public static EventDecoder htmlDecoder = new HTMLDecoder();

    public static void reloadInstances() throws SQLException {
        LobbyManager.idToLobby.clear();
        Set<GameOverviewSchema> unfinishedReplayIDs = GameRepo.getInProgress();

        StoryPackage sp;
        for(GameOverviewSchema unfinishedGame: unfinishedReplayIDs){
            try{
                sp = DBStoryPackage.deserialize(unfinishedGame.id);
            }catch(Exception | Error e){
                e.printStackTrace();
                Util.log(e, "Couln't unpack replay " + unfinishedGame.id + ".");
                continue;
            }
            if(sp == null)
                continue;

            if(sp.onlyComputers())
                GameRepo.removeLobbyID(unfinishedGame.id);
            else
                loadInstance(sp);
        }
    }

    public static void loadInstance(StoryPackage sp) throws SQLException {
        new Lobby(sp); // registering is taken care of inside
    }

    private static int internalErrors = 0;
    public static AlternateLookup nodeSwitchLookup;

    public static void internalError(Throwable e) {
        LobbyManager.internalErrors++;
    }

    public static void resetInternalErrors() {
        internalErrors = 0;
    }

    public static boolean hasInternalErrors() {
        return internalErrors != 0;
    }

    public static void killDBConnections() {
        for(Lobby lobby: idToLobby.values())
            lobby.killDBConnection();
    }
}

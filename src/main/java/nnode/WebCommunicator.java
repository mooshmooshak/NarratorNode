package nnode;

import java.util.Optional;
import java.util.Set;

import game.event.Feedback;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Player;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.enums.GamePhase;
import models.view.Jsonifiable;

public class WebCommunicator {

    public static void pushOut(Jsonifiable view) {
        try{
            pushOut(view.toJson());
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public static void pushOut(JSONObject jo) {
        if(LobbyManager.switchListener != null)
            LobbyManager.switchListener.onSwitchMessage(jo);
    }

    public static void pushWarningToUser(Message message, long userID, Optional<Player> optPlayer, boolean isNight) {
        if(message instanceof SelectionMessage){
            if(optPlayer.isPresent() && optPlayer.get() == ((SelectionMessage) message).owner)
                return;
        }
        if(message instanceof Feedback && isNight){
            // these are cop and executioner game start
            if(message.getPhase() == GamePhase.NIGHT_ACTION_SUBMISSION || message.getDay() != 0)
                return;
        }
        if(!optPlayer.isPresent())
            return;

        Player player = optPlayer.get();
        try{
            JSONObject j = new JSONObject();

            JSONArray chats = new JSONArray();
            Lobby.AppendMessage(chats, message, player, player.getName(), Optional.of(userID));
            j.put(StateObject.message, chats);

            WebCommunicator.pushToUser(j, userID);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public static void pushWarningToUser(String warningText, Optional<Long> userID) {
        if(userID.isPresent())
            pushWarningToUser(warningText, userID.get());
    }

    public static void pushWarningToUser(String warningText, Set<Long> userIDs) {
        // the node switch could be smarter so as to not send so many messages
        for(long userID: userIDs)
            pushWarningToUser(warningText, userID);
    }

    public static void pushWarningToUser(String warningText, long userID) {
        try{
            JSONObject jo = new JSONObject();
            jo.put(StateObject.message, StateObject.warning);
            jo.put(StateObject.warning, warningText);
            jo.put("userID", userID);
            pushOut(jo);
        }catch(JSONException e1){
            e1.printStackTrace();
        }
    }

    public static void pushToUsers(JSONObject jo, Set<Long> userIDs) throws JSONException {
        for(long userID: userIDs){
            jo.put("userID", userID);
            pushOut(jo);
        }
    }

    public static void pushToUser(JSONObject jo, long userID) throws JSONException {
        jo.put("userID", userID);
        pushOut(jo);
    }

    public static void pushMessageTextToUser(String s, long userID) {
        JSONObject jo = new JSONObject();
        try{
            jo.put("message", s);
            pushToUser(jo, userID);
        }catch(JSONException e){
        }
    }
}

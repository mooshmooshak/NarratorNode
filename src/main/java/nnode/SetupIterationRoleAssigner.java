package nnode;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import game.logic.Game;
import game.logic.support.RoleAssigner;
import game.setups.Setup;
import models.FactionRole;
import models.GeneratedRole;
import models.SetupHidden;
import models.schemas.SetupHiddenSpawnSchema;
import repositories.SetupIterationRepo;
import repositories.SetupIterationSpawnsRepo;
import util.game.LookupUtil;

public class SetupIterationRoleAssigner extends RoleAssigner {

    Lobby lobby;

    public SetupIterationRoleAssigner(Lobby lobby) {
        this.lobby = lobby;
    }

    @Override
    public ArrayList<GeneratedRole> pickRoles(Game game, List<SetupHidden> setupHiddens) {
        try{
            Optional<Long> setupIterationID = SetupIterationRepo.getSetupIterationID(lobby.game.setup.id,
                    lobby.game.players.size());
            if(!setupIterationID.isPresent())
                return super.pickRoles(game, setupHiddens);

            List<SetupHiddenSpawnSchema> spawns = SetupIterationSpawnsRepo.getByIterationID(setupIterationID.get());

            ArrayList<GeneratedRole> pickedRoles = new ArrayList<>();
            GeneratedRole setupHiddenSpawn;
            SetupHidden setupHidden;
            FactionRole factionRole;
            Setup setup = lobby.game.setup;
            for(SetupHiddenSpawnSchema schema: spawns){
                setupHidden = LookupUtil.findSetupHidden(lobby.game, schema.setupHiddenID);
                factionRole = setup.getFactionRole(schema.factionRoleID);
                setupHiddenSpawn = new GeneratedRole(setupHidden);
                setupHiddenSpawn.factionRoles.add(factionRole);
                pickedRoles.add(setupHiddenSpawn);
            }
            return pickedRoles;
        }catch(Exception e){
            e.printStackTrace();
            return super.pickRoles(game, setupHiddens);
        }
    }
}

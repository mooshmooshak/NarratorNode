package nnode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

import game.ai.Brain;
import game.ai.Computer;
import game.event.DeathAnnouncement;
import game.event.EventDecoder;
import game.event.Message;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.RoleAssigner;
import game.logic.support.StoryPackage;
import game.logic.templates.HTMLDecoder;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.idtypes.GameID;
import repositories.BaseRepo;
import services.GameService;

public class HappeningGenerator {

    public static final String[] NAMES = { "Alpha", "Bravo", "Charlie", "Deltaa", "Echo", "Foxtrot", "Golf", "Hotel",
            "India", "Juliett", "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra",
            "Tango", "Uniform", "Victor", "Whiskey", "Xray", "Yankee", "Zulu" };

    public static void main(String args[]) throws JSONException, ClassNotFoundException, SQLException {
        // args = new
        // String[]{"{\"size\":30,\"seed\":29,\"genre\":\"brave\",\"setup\":\"brave\"}"};
        if(args.length < 1){
            throw new IllegalArgumentException("Need a json encoded string as an argument");
        }

        BaseRepo.StartDriver();

        Game game = null;
        int size;

        DatabaseDeath dd = new DatabaseDeath(0);
        JSONObject jo = new JSONObject(args[0]);
        String genre = null, setupName;
        long seed;
        StoryPackage sp = null;

        if(jo.has("genre")){
            genre = jo.getString("genre").toLowerCase().replaceAll(" ", "");
            dd.setGenre(genre);
        }
        boolean addPlayers = true;
        RoleAssigner roleAssigner;

        if(jo.has("storyID")){
            GameID storyID = new GameID(jo.getLong("storyID"));
            sp = DBStoryPackage.deserialize(storyID);
            if(sp == null)
                throw new NullPointerException("Game with id " + storyID + " not found.");
            size = sp.phoneBook.size();
            seed = sp.narrator.getSeed();
            game = sp.narrator;
            roleAssigner = StoryPackage.GetRoleAssigner(sp);
            addPlayers = false;
        }else{
            setupName = jo.getString("setup");
            size = 15;
            seed = 0;
//            Setup setup = SetupService.getSetup(setupID);
            size = jo.getInt("size");
            seed = jo.getLong("seed");
//            if(size > Setup.getMaxPlayerCount(game.setup))
//                size = Setup.getMaxPlayerCount(game.setup);
//            else if(size < Setup.getMaxPlayerCount(game.setup))
//                size = Setup.getMinPlayerCount(game.setup);
            roleAssigner = new RoleAssigner();
//            n = setup.narrator;
            game.setSeed(seed);
        }

        String name;
        for(int i = 0; i < size && addPlayers; i++){
            if(i < NAMES.length)
                name = NAMES[i];
            else
                name = Computer.toLetter(i + 1);
            game.addPlayer(name).setComputer();
        }

        game.setDeathDescriber(dd);
        GameService.internalStart(game, roleAssigner);

        if(sp != null){
            sp.runCommands();
        }else
            Brain.EndGame(game, seed);

        dd.cleanup();

        JSONObject retObject = new JSONObject();
        JSONArray jStory = new JSONArray();
        JSONArray deads = new JSONArray();
        JSONArray deaths = new JSONArray();

        String access;
        HTMLDecoder decoder = new HTMLDecoder();
        for(Message m: game.getEventManager().getEvents(Message.PRIVATE)){
            if(m instanceof VoteAnnouncement)
                continue;

            if(sp == null){
                if(m instanceof DeathAnnouncement && ((DeathAnnouncement) m).dead != null)
                    deaths.put(jDescript((DeathAnnouncement) m, decoder));
            }
            access = m.access(Message.PRIVATE, decoder);
            if(access.length() > 0)
                jStory.put(access + "<br>");
        }
        retObject.put("story", jStory);

        ArrayList<String> seen = new ArrayList<>();

        JSONObject dead;
        for(FactionRole m: game.setup.getFactionRoles()){
            if(seen.contains(m.toString()))
                continue;
            seen.add(m.toString());
            dead = new JSONObject();
            dead.put("name", m.getName());
            dead.put("color", m.getColor());
            dead.put("teamVal", DatabaseDeath.getFactionID(Optional.of(game.getFaction(m.getColor()).getName()), game));
            dead.put("roleVal", m.role.getDatabaseName());
            dead.put("teamName", game.getFaction(m.getColor()).getName());
            deads.put(dead);
        }
        retObject.put("roles", deads);
        retObject.put("deaths", deaths);

        seen.clear();

        System.out.println(retObject);
    }

    private static JSONObject jDescript(DeathAnnouncement m, EventDecoder decoder) throws JSONException {
        JSONObject jo = new JSONObject(), jKill;
        String access = m.access(Message.PUBLIC, decoder);
        if(access.length() > 0)
            jo.put("text", access);
        JSONArray kills = new JSONArray();

        for(Player p: m.dead){
            for(String[] deathType: p.getDeathType().getList()){

                jKill = new JSONObject();
                jKill.put("name", deathType[1]);
                jKill.put("killID", deathType[0]);
                kills.put(jKill);

            }
        }

        jo.put("attacks", kills);

        return jo;
    }

}

package nnode;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NarratorThreadPool {
    public static final ExecutorService narratorThreadPool = Executors.newFixedThreadPool(getThreadPoolCount());

    private static int getThreadPoolCount() {
        int waitTime = Config.getInt("thread_wait_time"); //
        int serviceTime = Config.getInt("thread_service_time"); // how long setup takes to put everything together
        int coreCount = Config.getInt("thread_core_count");
        double utlization = Config.getDouble("thread_utilization_percentage");
        int threadPoolCount = (int) ((coreCount * (1 + waitTime / serviceTime)) * utlization);
        int limitedTPC = Math.max(1, Math.min(40, threadPoolCount));
        System.err.println("Using " + limitedTPC + " theads for game");
        return limitedTPC;
    }
}

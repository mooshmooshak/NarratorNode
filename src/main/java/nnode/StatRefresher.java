package nnode;

import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import game.logic.support.StoryPackage;
import models.idtypes.GameID;
import repositories.BaseRepo;
import repositories.Connection;
import repositories.GameRepo;
import repositories.PlayerDeathRepo;
import repositories.PlayerRepo;
import repositories.UserRepo;

public class StatRefresher {

    public static void main(String args[]) throws SQLException {
        BaseRepo.connection = new Connection();
        try{
            runMain();
        }catch(Throwable t){
            t.printStackTrace();
        }
        BaseRepo.connection.close();
    }

    public static void runMain() throws SQLException {
        Set<GameID> finishedGameIDs = GameRepo.getFinishedIDs();

        clearTables();

        List<GameID> errorGameIDs = new LinkedList<>();
        Collections.sort(errorGameIDs);

        int index = 0;
        StoryPackage sp;
        for(GameID finID: finishedGameIDs){
            System.err.println("#" + finID + "\t(" + ++index + " / " + finishedGameIDs.size() + ")");
            sp = DBStoryPackage.deserialize(finID);

            try{
                sp.runCommands();
            }catch(NullPointerException e){
                e.printStackTrace();
                continue;
            }

            if(sp.narrator.isInProgress()){
                errorGameIDs.add(sp.replayID);
                continue;
            }

            StoryWriter.UpdateStats(sp.narrator, sp.replayID);
        }

        System.err.println(errorGameIDs);

        BaseRepo.connection.close();
    }

    private static void clearTables() throws SQLException {
        PlayerRepo.clearIsWinnerAndDeathDay();
        PlayerDeathRepo.deleteAll();
        UserRepo.clearPointsAndWinRate();
    }
}

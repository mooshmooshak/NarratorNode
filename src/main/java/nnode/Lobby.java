package nnode;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.ai.Brain;
import game.ai.Computer;
import game.event.Announcement;
import game.event.ChatMessage;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.EventLog;
import game.event.Feedback;
import game.event.Header;
import game.event.JailChat;
import game.event.Message;
import game.event.SelectionMessage;
import game.event.SnitchAnnouncement;
import game.event.VoidChat;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.listeners.NarratorListener;
import game.logic.support.AlternateLookup;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.DeathDescriber;
import game.logic.support.Random;
import game.logic.support.StoryPackage;
import game.logic.support.action.Action;
import game.logic.templates.HTMLDecoder;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.PlayerUserIDMap;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import models.events.ActionSubmitEvent;
import models.events.BroadcastEvent;
import models.events.ChatMessageSendEvent;
import models.events.DayDeathEvent;
import models.events.DayStartEvent;
import models.events.GameEndEvent;
import models.events.GameStartEvent;
import models.events.NightStartEvent;
import models.events.PhaseEndBidEvent;
import models.events.PhaseResetEvent;
import models.events.PlayerUpdateEvent;
import models.events.RoleCardUpdate;
import models.events.RolePickingPhaseStartEvent;
import models.events.TrialPhaseStartEvent;
import models.events.VotePhaseStartEvent;
import models.events.VoteResetEvent;
import models.events.VoteUpdateEvent;
import models.idtypes.GameID;
import models.schemas.GameOverviewSchema;
import models.schemas.GameUserSchema;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.SetupRepo;
import repositories.UserPermissionRepo;
import services.ActionService;
import services.ChatService;
import services.GameService;
import services.GameSpawnsService;
import services.LobbyService;
import services.ModeratorService;
import services.PhaseService;
import services.PlayerService;
import services.UserService;
import util.Util;

public class Lobby implements NarratorListener {

    public static boolean runningTests = false;

    public Game game;
    public CommandHandler ch;

    public String id;

    private StoryWriter sw;

    private boolean isPrivate;

    public GameID gameID;

    private ArrayList<SnitchAnnouncement> snitchAnnouncements;

    public AlternateLookup nodeSwitchLookup = new AlternateLookup() {
        @Override
        public Player lookup(String name) {
            try{
                long userID = Long.parseLong(name);
                Optional<Player> player = PlayerService.getByUserID(userID);
                if(player.isPresent())
                    return player.get();
            }catch(NumberFormatException | SQLException e){
            }
            return null;
        }
    };

    private void init() {
        snitchAnnouncements = new ArrayList<>();
    }

    public Lobby(StoryPackage sp) throws SQLException {
        init();
        GameService.gamesMap.put(sp.replayID, sp.narrator);
        LobbyManager.idToLobby.put(sp.instanceID, this);

        this.gameID = sp.replayID;
        this.id = sp.instanceID;
        this.game = sp.narrator;

        ch = new CommandHandler(game);

        LobbyManager.registerInstanceID(sp.instanceID, this);
        this.id = sp.instanceID;
        this.isPrivate = sp.isPrivate;

        addDeathDescriptions();

        Throwable error = sp.runCommands();
        if(error != null)
            LobbyManager.internalError(error);

        sw = new StoryWriter(new ArrayList<>());
        sw.setGameID(sp.replayID, this.game.players.getDatabaseMap());
        sw.counter = sp.commands.size();
        game.addListener(sw);
        game.addListener(this);

        setBrain();

        PhaseService.setPhaseTimeout(this);
        try{
            WebCommunicator.pushOut(PhaseResetEvent.request(this));
        }catch(JSONException e){
            Util.log(e, "Failed to send close lobby event.");
        }
    }

    public Lobby(Game game, GameOverviewSchema schema) {
        init();
        this.game = game;
        this.gameID = schema.id;
        this.id = schema.lobbyID;
        game.addListener(this);
        ch = new CommandHandler(game);

        this.isPrivate = schema.isPrivate;
    }

    public void resetSetupHiddenSpawns() {
        game.setup.rolesList.clearSpawns();
    }

    public PlayerList addComputers(int size) {
        PlayerList newPlayers = new PlayerList();
        Player player;
        int currentComputerSize = 1;
        while (size > 0){

            // Gets valid computer name
            while (game.getPlayerByID(
                    Computer.toLetter(currentComputerSize) + Computer.toLetter(currentComputerSize)) != null)
                currentComputerSize++;

            player = game.addPlayer(Computer.toLetter(currentComputerSize) + Computer.toLetter(currentComputerSize))
                    .setComputer();
            newPlayers.add(player);
            size--;
        }
        return newPlayers;
    }

    DeathDescriber dd;

    private void addDeathDescriptions() throws SQLException {
        if(dd != null)
            dd.cleanup();
        dd = new DatabaseDeath(game.getSeed());
        if(dd == null){
            dd = new DeathDescriber() {
                @Override
                public void populateDeath(Player dead, Announcement e) {
                    try{
                        String deaths = readFile("deaths", StandardCharsets.UTF_8);
                        JSONArray jArray = new JSONArray(deaths);
                        Random r = new Random();

                        String death = jArray.getString(r.nextInt(jArray.length()));
                        death = death.replaceAll("%s", dead.getName());

                        if(!dead.getDeathType().isHidden()){
                            death += dead.getName() + " was a " + dead.getGameFaction().getName() + " "
                                    + dead.getRoleName() + ".";
                        }else{
                            death += "We could not determine what " + dead.getName() + " was.";
                        }

                        e.add(death);
                    }catch(IOException | JSONException e1){
                        e1.printStackTrace();
                    }
                }

                @Override
                public boolean hasDeath(Player dead) {
                    return true;
                }

                @Override
                public void setGenre(String s) {
                }

                @Override
                public void cleanup() {
                }
            };
        }else{
            ((DatabaseDeath) dd).setProductionMode(true);
        }
        game.setDeathDescriber(dd);

    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static JSONObject GetGUIObject() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.guiUpdate, true);
        jo.put(StateObject.type, new JSONArray());
        return jo;
    }

    public void startGame(boolean isPrivate) {
        // th is initialized first because if the game is started, a message will be
        // sent out
        // instance will not quite be set yet, and throw null pointers

        /*
         * String errorMessage = _n.willStart(); if(errorMessage != null){
         * host.warn(errorMessage); return; }
         */

        game.players.sortByName();

        try{
            ArrayList<NarratorListener> nls = new ArrayList<>();
            nls.addAll(game.getListeners());
            game.getListeners().addAll(nls);
            // after this call is when the game state is pushed out to the clients
            GameService.internalStart(game, new SetupIterationRoleAssigner(this));
        }catch(NarratorException e){
            throw e;
        }catch(Throwable t){
            t.printStackTrace();
            // if unable to get moderatorIDs, don't send to anyone
            if(t.getMessage() != null)
                WebCommunicator.pushWarningToUser(t.getMessage(), new HashSet<>());
            return;
        }

        try{
            SetupRepo.setNotEditable(this.game.setup.id);
            GameRepo.setStarted(this.gameID);
        }catch(SQLException e2){
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }

        try{
            addDeathDescriptions();
        }catch(SQLException e1){
            Util.log(e1, "Failed to add death descriptions.");
        }

        this.isPrivate = isPrivate;

        sw = new StoryWriter(game.getCommands());
        try{
            sw.setGameID(this.gameID, this.game.players.getDatabaseMap());
            GameSpawnsService.saveSpawns(this.gameID, this.game);
            PlayerService.savePlayersToGame(this.gameID, this.game);
        }catch(SQLException | NarratorException e1){
            Util.log(e1);
        }

        game.addListener(sw);
        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            Map<Long, String> userIDtoNamesMap = UserService.getNamesMap(gameLobby.userMap.keySet());
            WebCommunicator.pushOut(GameStartEvent.request(gameLobby, userIDtoNamesMap));
            Optional<Long> userID;
            for(Player player: game.players){
                userID = gameLobby.playerUserIDMap.getUserID(player);
                if(!userID.isPresent())
                    continue;
                WebCommunicator.pushOut(RoleCardUpdate.request(this, UserService.getUser(userID.get()), player));
            }
        }catch(JSONException | SQLException e){
            Util.log(e, "Failed to send game start event.");
        }

        if(game.isDay())
            speakAnnouncement("Game has started.  It is daytime.");
        else
            speakAnnouncement("Game has started.  It is nighttime.");

        setBrain();
        sendGameState();
    }

    @Override
    public void onRolepickingPhaseStart() {
        PhaseService.setPhaseTimeout(this);
        try{
            WebCommunicator.pushOut(RolePickingPhaseStartEvent.request(this.game, this.id));
        }catch(JSONException e){
            Util.log(e, "Failed to send role picking phase start event.");
        }
    }

    private long getSeed() {
//        Random
//        if(this.setup.getName().equalsIgnoreCase(Default.KEY) || this.narrator.hiddens.isEmpty())
//            return narrator.getRandom().nextLong();
//        String query = "SELECT SUM(1/variability) AS total FROM seed_variability WHERE setup = ? AND playerCount = ?;";
//        PreparedStatement ps = connection.prepareStatement(query);
//        ps.setString(1, setup.key);
//        ps.setInt(2, narrator.players.size());
//        ResultSet rs = ps.executeQuery();
//
//        if(!rs.next() || rs.getDouble(1) <= 0){
//            rs.close();
//            ps.close();
//            return narrator.getRandom().nextLong();
//        }
//        double vari = narrator.getRandom().nextDouble() * rs.getDouble(1);
//        rs.close();
//        ps.close();
//
//        ps = connection.prepareStatement(
//                "SELECT seed, 1/variability FROM seed_variability WHERE playerCount = ? AND setup = ? AND variability > 0 ORDER BY RAND();");
//        ps.setInt(1, narrator.players.size());
//        ps.setString(2, setup.key);
//        rs = ps.executeQuery();
//
//        while (rs.next()){
//            vari -= rs.getDouble(2);
//            if(vari <= 0)
//                return rs.getLong(1);
//        }

        return game.getRandom().nextLong();
    }

    Brain brain;

    private void setBrain() {
        if(game.players.hasComputers()){
            brain = new Brain(game, game.players.getComputers(), new Random().setSeed(game.getSeed()));
        }
    }

    public void cancelBrain() {
        brain = null;
    }

    public JSONObject sendGameState(long userID) throws JSONException, SQLException {
        return sendGameState(this, userID);
    }

    public static StateObject getGameState(Lobby game, Optional<Player> player, Set<Long> moderatorIDs) {
        Game narrator = game.game;

        StateObject gameState = new StateObject(game);
        gameState.addState(StateObject.PLAYERLISTS);
        gameState.addState(StateObject.GRAVEYARD);
        gameState.addState(StateObject.ROLEINFO);
        gameState.addState("setup");

        gameState.addKey(StateObject.gameStart, narrator.isStarted());
        gameState.addKey(StateObject.joinID, game.id);
        gameState.addKey("joinID", game.id);
        gameState.addState(StateObject.FACTIONS);
        gameState.addKey("moderatorIDs", moderatorIDs);

        if(narrator.isStarted()){
            gameState.addState(StateObject.DAYLABEL);
            gameState.addState(StateObject.ACTIONS);
            gameState.addKey(StateObject.isDay, narrator.isDay());
            if(narrator.isInProgress()){
                if(player.isPresent() && player.get()._lastWill != null && !player.get()._lastWill.isEmpty()){
                    // I will not continually send this upon last will updates. the client should
                    // have this
                    gameState.addKey("lastWill", player.get()._lastWill);
                }
                if(player.isPresent() && player.get()._secretLastWill != null
                        && !player.get()._secretLastWill.isEmpty()){
                    // I will not continually send this upon last will updates. the client should
                    // have this
                    gameState.addKey(StateObject.teamLastWill, player.get()._secretLastWill);
                }
            }
            if(narrator.isDay()){
                if(player.isPresent())
                    gameState.addKey(StateObject.getDayActions,
                            new JSONArray(AbilityType.toCommandStringSet(player.get().getDayCommands())));
                else
                    gameState.addKey(StateObject.getDayActions, new JSONArray());
            }
        }else{
            gameState.addKey(StateObject.isPrivateInstance, game.isPrivate());
            if(player.isPresent())
                gameState.addKey(StateObject.playerName, player.get().getName());
        }
        return gameState;
    }

    public static JSONObject sendGameState(Lobby lobby, long userID) throws SQLException {
        Set<Long> hostID = ModeratorService.getModeratorIDs(lobby.gameID);
        Optional<Player> player = PlayerService.getByUserID(userID, lobby);
        StateObject iObject = getGameState(lobby, player, hostID);
        return iObject.constructAndSend(userID, player, lobby);
    }

    public void sendPlayerLists() throws SQLException {
        StateObject io = new StateObject(this);
        io.addState(StateObject.PLAYERLISTS);

        Map<Long, Optional<Player>> userPlayerMap = UserService.getUserPlayerMap(this.gameID);
        Optional<Player> player;
        for(Long userID: userPlayerMap.keySet()){
            player = userPlayerMap.get(userID);
            if(player.isPresent())
                io.addKey(StateObject.playerName, player.get().getName());
            io.constructAndSend(userID, player, this);
        }
    }

    public void sendGameState() {
        try{
            for(long userID: UserService.getUserIDs(this.gameID))
                sendGameState(this, userID);
        }catch(SQLException f){
            LobbyManager.internalError(f);
            throw new Error(f);
        }catch(Exception | Error e){
            LobbyManager.internalError(e);
            throw e;
        }
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public boolean hasPermission(long userID, int permission) {
        try{
            return UserPermissionRepo.hasPermission(userID, permission);
        }catch(SQLException e){
            WebCommunicator.pushMessageTextToUser("Error with database, unable to process request.", userID);
            Util.log(e, "Failed to retrieve user permission.");
            return false;
        }
    }

    private static Lobby getLobby(long userID) throws SQLException {
        Optional<GameUserSchema> gameOpt = GameUserRepo.getActiveByUserID(userID);
        if(!gameOpt.isPresent())
            return null;
        GameOverviewSchema gameOverview = GameRepo.getOverview(gameOpt.get().gameID);
        Lobby lobby = LobbyManager.idToLobby.get(gameOverview.lobbyID);
        if(lobby != null)
            return lobby;

        return LobbyService.getByGame(gameOverview);
    }

    public static synchronized void handlePlayerMessage(long userID, GameID gameID, JSONObject jo)
            throws JSONException, SQLException {
        String message = jo.getString(StateObject.message);
        if(message.length() == 0)
            return;

        if(message.equals(StateObject.requestChat)){
            ChatService.pushAllChats(userID);
            return;
        }

        Lobby lobby = getLobby(userID);
        if(message.equals(StateObject.requestUnreads)){
            ChatService.pushUnreads(userID);
            return;
        }

        if(message.equals(StateObject.requestGameState)){
            lobby.sendGameState(userID);
            return;
        }

        if(message.equals(StateObject.setReadChat)){
            if(jo.has(StateObject.setReadChat)){
                try{
                    String chatReadName = jo.getString(StateObject.setReadChat);
                    ChatService.setRead(lobby.gameID, userID, chatReadName);
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
            return;
        }

        Game game = lobby.game;
        // peek peekroleslist
        if(message.equals("getRolesList")){
            if(lobby.isPrivate() || lobby.hasPermission(userID, Permission.GAME_EDITING)){
                if(!game.isStarted()){
                    WebCommunicator.pushMessageTextToUser("Roles aren't assigned yet.", userID);
                    return;
                }
                for(Player p: game.getAllPlayers().sortByDeath()){
                    WebCommunicator.pushMessageTextToUser(
                            Message.accessHelper(p, Message.PRIVATE, game.getDayNumber(), LobbyManager.htmlDecoder),
                            userID);
                }
            }else{
                WebCommunicator.pushMessageTextToUser("You don't have this permissions!", userID);
            }
            return;
        }

        Optional<Player> optPlayer = PlayerService.getByUserID(userID, lobby);
        if(!optPlayer.isPresent()){
            Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(lobby.gameID);
            if(moderatorIDs.contains(userID)){
                WebCommunicator.pushMessageTextToUser("Unable to find find you in the game.  Action not submitted.",
                        userID);
                return;
            }
        }

        if(message.toLowerCase().equalsIgnoreCase(Constants.MOD_ADDBREAD)){
            if(!lobby.hasPermission(userID, Permission.GAME_EDITING)){
                WebCommunicator.pushMessageTextToUser("You don't have this permissions!", userID);
                return;
            }
            if(!game.isStarted){
                WebCommunicator.pushWarningToUser("Game hasn't started, no bread to give", userID);
                return;
            }

            int bread = jo.getInt("bread");
            String player_s = jo.getString("eater");
            Player player = game.players.getByName(player_s);
            if(player == null){
                WebCommunicator.pushWarningToUser("Player with that name not found", userID);
                return;
            }
            player.addModBread(bread, Constants.ALL_TIME_LEFT);
            sendGameState(lobby, userID);

            return;
        }

        if(message.equals(StateObject.teamLastWill)){
            if(optPlayer.isPresent() && game.isInProgress() && optPlayer.get().getGameFaction().hasLastWill()
                    && optPlayer.get().isAlive()){
                optPlayer.get().setTeamLastWill(jo.getString(StateObject.willText));
            }
            return;
        }

        try{
            ActionService.submitAction(userID, message, Constants.ALL_TIME_LEFT);
        }catch(NarratorException e){
            WebCommunicator.pushWarningToUser(e.getMessage(), userID);
        }catch(Throwable t){
            LobbyManager.internalError(t);
            if(runningTests)
                throw t;
            t.printStackTrace();
            String warningMessage;
            if(t.getMessage() != null)
                warningMessage = t.getMessage();
            else
                warningMessage = "Server Error : This message has been logged.";
            WebCommunicator.pushWarningToUser(warningMessage, userID);
        }
    }

    public void resetChat(long userID) {
        try{
            ChatService.pushAllChats(userID, this.gameID);
        }catch(JSONException | SQLException e1){
            e1.printStackTrace();
        }
    }

    public static void AppendMessage(JSONArray chatLog, Message e, Player p, String key, Optional<Long> speakingUserID)
            throws JSONException {
        JSONObject jo = new JSONObject();
        if(e instanceof Header)
            return;
        else if(e instanceof SelectionMessage && ((SelectionMessage) e).owner == p)
            return;
        else if(e instanceof ChatMessage){
            ChatMessage cm = (ChatMessage) e;
            if(cm.getSender() == null)
                return;
            jo.put("text", cm.message);
            jo.put("sender", Message.accessHelper(cm.getSender(), key, e.getDay(), new HTMLDecoder(), cm.n));
            if(speakingUserID.isPresent() && !p.game.isInProgress())
                jo.put("userID", speakingUserID.get());
            else
                jo.put("speakerName", cm.speakerName);
        }else{
            String text = e.access(key, new HTMLDecoder()) + "\n";
            if(text.contains(Player.END_NIGHT_TEXT) || text.contains(Player.CANCEL_END_NIGHT_TEXT))
                return;
            if(text.length() <= 1)
                return;
            jo.put("text", text);
            jo.put("simpleText", e.access(key));

            if(e.getID() != null){
                jo.put("mID", e.getID());
                jo.put("pic", e.getPicture());
                jo.put("extras", e.getExtras());
            }
        }
        jo.put("messageType", e.getClass().getSimpleName());
        jo.put("day", e.getDay());
        jo.put("isDay", e.getPhase());
        String[] chatNames = Util.toStringArray(e.getEnclosingChats(e.n, p));
        jo.put("chat", new JSONArray(chatNames));

        if(isPlayerOwnerSelection(e, p)){
            jo.put(StateObject.chatClue, "myNightTarget");
        }
        if(e instanceof Feedback){
            jo.put(StateObject.chatClue, "feedback");
        }
        if(e instanceof DeathAnnouncement){
            jo.put(StateObject.chatClue, "deathAnnouncement");
        }
        if(e.isNightToDayAnnouncement())
            jo.put("nightDeath", true);
        chatLog.put(jo);
    }

    private static boolean isPlayerOwnerSelection(Message e, Player p) {
        if(e instanceof SelectionMessage){
            SelectionMessage sm = (SelectionMessage) e;
            return sm.owner == p;

            // i'd like to check for a finality here too.
        }
        return false;
    }

    private void resetChat() {
        try{
            for(long userID: UserService.getUserIDs(this.gameID))
                resetChat(userID);
        }catch(SQLException e){
            Util.log(e);
        }
    }

    @Override
    public void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList e) {
        sendGameState();
        resetChat();

        PhaseService.setPhaseTimeout(this);
        try{
            setNightChatsUnread();
            GameLobby game = LobbyService.getGameLobby(this);
            Map<Long, String> userIDstoNamesMap = UserService.getNamesMap(game.userMap.keySet());
            WebCommunicator.pushOut(NightStartEvent.request(game, lynched, poisoned, userIDstoNamesMap));
        }catch(JSONException | SQLException f){
            Util.log(f, "Failed to send day start message.");
        }
    }

    public void setNightChatsUnread() throws SQLException {
        setUnread(game.getEventManager().getNightLog(VoidChat.KEY));
        String jailChatKey;
        EventLog jc;

        PlayerUserIDMap playerUserIDMap = UserService.getPlayerUserMap(this.game);
        for(Player player: game.players){
            if(!player.isJailed() || !playerUserIDMap.hasUserID(player))
                continue;
            jailChatKey = JailChat.GetKey(player, game.getDayNumber());
            jc = game.getEventManager().getNightLog(jailChatKey);
            setSingleUnread(jc, playerUserIDMap.getUserID(player).get());
        }
    }

    @Override
    public void onDayPhaseStart(PlayerList newDead) {
        sendGameState();

        resetChat();
        if(game.getInt(GameModifierName.DISCUSSION_LENGTH) != 0)
            PhaseService.setPhaseTimeout(this);

        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            Map<Long, String> userIDtoNameMap = UserService.getNamesMap(gameLobby.userMap.keySet());
            WebCommunicator
                    .pushOut(DayStartEvent.request(gameLobby, newDead, this.snitchAnnouncements, userIDtoNameMap));
        }catch(JSONException | SQLException e){
            Util.log(e, "Failed to send day start message.");
        }

        this.snitchAnnouncements.clear();

        setUnread(game.getEventManager().getDayChat());
    }

    @Override
    public void onVotePhaseStart() {
        PhaseService.setPhaseTimeout(this);
        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            Map<Long, String> userIDtoNameMap = UserService.getNamesMap(gameLobby.userMap.keySet());
            WebCommunicator.pushOut(VotePhaseStartEvent.request(gameLobby, userIDtoNameMap));
        }catch(JSONException | SQLException e){
            Util.log(e, "Failed to send vote phase start message.");
        }
    }

    @Override
    public void onVotePhaseReset(int resetsRemaining) {
        PhaseService.setPhaseTimeout(this);
        try{
            WebCommunicator.pushOut(VoteResetEvent.request(this, resetsRemaining));
        }catch(JSONException e){
            Util.log(e, "Failed to send out vote phase reset message.");
        }
    }

    @Override
    public void onTrialStart(Player trialedPerson, Announcement trialStartEvent) {
        PhaseService.setPhaseTimeout(this);
        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            WebCommunicator.pushOut(TrialPhaseStartEvent.request(gameLobby, trialedPerson));
        }catch(JSONException | SQLException e){
            Util.log(e, "Failed to send trial phase start message.");
        }
        broadcast(trialStartEvent, false);
    }

    @Override
    public void onGameStart() {

    }

    @Override
    public void onGameEnd() {
        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            WebCommunicator.pushOut(GameEndEvent.request(gameLobby));
        }catch(JSONException | SQLException e){
            Util.log(e, "Failed to send trial phase start message.");
        }
        sendGameState();
        resetChat();
        broadcast(game.getWinMessage(), true);

        try{
            sw.endGame(game);
        }catch(SQLException e){
            Util.log(e, "SQL Exception on end game");
            LobbyManager.internalError(e);
            e.printStackTrace();
        }
        game.removeListener(sw);
        killDBConnection();
        ModeratorService.resetRepickers(this.gameID);
        ChatService.deleteUnreads(this.gameID);
    }

    public void killDBConnection() {
        if(dd != null){
            game.setDeathDescriber(null);
            dd.cleanup();
        }
        if(sw == null)
            return;
        game.removeListener(sw);
        // _n.removeListener(this); //so when the brain ends the game, the update stats
        // isn't triggered.
    }

    // im only going to call this when something new happens in the specified chat
    // toExclude are people that I dont want to notify that there's a new 'unread
    // message in the chat'
    // nor do I want to remove them from the 'read'
    public void setUnread(EventLog el, Set<Long> toExclude) {
        String logName = el.getName();

        try{
            UnreadManager toSend = ChatService.getUnreadManager(this.getGameID(), logName, el.getMembers());
            toSend.pushExcept(toExclude);
        }catch(SQLException e){
            Util.log(e);
        }
    }

    public void setUnread(EventLog el) {
        setUnread(el, new HashSet<>());
    }

    public void setUnread(EventLog el, long userID) {
        Set<Long> userIDs = new HashSet<>();
        userIDs.add(userID);
        setUnread(el, userIDs);
    }

    public void setUnread(EventLog el, Optional<Long> userID) {
        Set<Long> userIDs = new HashSet<>();
        if(userID.isPresent())
            userIDs.add(userID.get());
        setUnread(el, userIDs);
    }

    public void setSingleUnread(EventLog el, long userID) {
        String key = el.getName();
        try{
            UnreadManager alreadyRead = ChatService.getUnreadManager(this.gameID, key, el.getMembers());
            alreadyRead.moreUnread(userID);
        }catch(SQLException e){
            Util.log(e);
            return;
        }
    }

    @Override
    public void onDayActionSubmit(Player player, Action a) {
        Optional<Long> optUser;
        try{
            optUser = UserService.getUserID(player);
        }catch(SQLException e){
            e.printStackTrace();
            return;
        }
        if(!optUser.isPresent())
            return;
        StateObject io = new StateObject(this);
        io.addState(StateObject.ACTIONS);
        io.addState(StateObject.PLAYERLISTS);
        io.constructAndSend(optUser.get(), Optional.of(player), this);
    }

    @Override
    public void onRoleReveal(Player revealer, Message e) {
        speakAnnouncement(e.access(Message.PUBLIC));
        broadcast(e, false);

        pushToDiscordUsers(this.id, this.gameID, e.access(Message.PUBLIC), e);

        Optional<Long> optUserID;
        try{
            optUserID = UserService.getUserID(revealer);
        }catch(SQLException e2){
            return;
        }
        if(!optUserID.isPresent())
            return;

        long userID = optUserID.get();
        setUnread(game.getEventManager().getDayChat(), userID);
        try{
            sendGameState(userID);
        }catch(JSONException | SQLException e1){
            e1.printStackTrace();
        }
        WebCommunicator.pushOut(new PlayerUpdateEvent(revealer, this.id, userID));
    }

    @Override
    public void onElectroExplosion(PlayerList deadPeople, DeathAnnouncement announcement) {
        resetChat();
        sendGameState();

        setUnread(game.getEventManager().getDayChat());

        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            WebCommunicator.pushOut(DayDeathEvent.request(gameLobby, announcement));
        }catch(JSONException | SQLException e1){
            Util.log(e1, "Failed to execute Day Death event.");
        }
    }

    @Override
    public void onDayBurn(Player arson, PlayerList burned, DeathAnnouncement e) {
        resetChat();
        sendGameState();

        setUnread(game.getEventManager().getDayChat());

        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            WebCommunicator.pushOut(DayDeathEvent.request(gameLobby, e));
        }catch(JSONException | SQLException e1){
            Util.log(e1, "Failed to execute Day Death event.");
        }
    }

    @Override
    public void onAssassination(Player assassin, Player target, DeathAnnouncement e) {
        resetChat();
        sendGameState();

        setUnread(game.getEventManager().getDayChat());

        try{
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            WebCommunicator.pushOut(DayDeathEvent.request(gameLobby, e));
        }catch(JSONException | SQLException e1){
            Util.log(e1, "Failed to execute Day Death event.");
        }
    }

    private void pushVoteUpdate(VoteAnnouncement event) {
        try{
            GameLobby game = LobbyService.getGameLobby(this);
            WebCommunicator.pushOut(VoteUpdateEvent.request(game, event));
        }catch(JSONException | SQLException e1){
            Util.log(e1, "Failed to push vote event.");
        }

        if(!this.game.getBool(SetupModifierName.SECRET_VOTES))
            broadcast(event, false);

        try{
            Optional<Long> user = UserService.getUserID(event.voter);
            setUnread(game.getEventManager().getDayChat(), user);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onVote(Player voter, VoteAnnouncement e, double timeLeft) {
        if(!voter.isComputer() && brain != null){
            if(e.voteTarget.contains(Constants.SKIP_VOTE_TITLE))
                Brain.SkipDay(brain, game, timeLeft);
            else
                Brain.Vote(brain, game.getPlayerByName(e.voteTarget.get(0)), null, timeLeft);
        }
        pushVoteUpdate(e);
    }

    @Override
    public void onVoteCancel(Player voter, VoteAnnouncement e) {
        pushVoteUpdate(e);
    }

    @Override
    public void onTargetSelection(Player owner, SelectionMessage selectionMessage) {
        if(brain != null && !owner.isComputer())
            brain.nightAction();
        // feedback already being sent and stored
        StateObject so = new StateObject(this).addState(StateObject.PLAYERLISTS).addState(StateObject.ACTIONS);
        if(game.isDay())
            so.addState(StateObject.ROLEINFO);

        Optional<Long> userIDOpt;
        try{
            userIDOpt = UserService.getUserID(owner);
        }catch(SQLException e){
            e.printStackTrace();
            return;
        }
        if(!userIDOpt.isPresent())
            return;

        long userID = userIDOpt.get();
        so.constructAndSend(userID, Optional.of(owner), this);
        try{
            WebCommunicator.pushOut(ActionSubmitEvent.request(owner, this.id, userID, selectionMessage));
        }catch(JSONException e1){
            Util.log(e1, "Failed to push action submit event.");
        }
    }

    @Override
    public void onTargetRemove(Player owner, AbilityType abilityType, PlayerList prev) {
        Optional<Long> optUserID;
        try{
            optUserID = UserService.getUserID(owner);
        }catch(SQLException e){
            e.printStackTrace();
            return;
        }
        if(!optUserID.isPresent())
            return;

        StateObject so = new StateObject(this).addState(StateObject.PLAYERLISTS).addState(StateObject.ACTIONS);
        so.constructAndSend(optUserID.get(), Optional.of(owner), this);
    }

    @Override
    public void onEndNight(Player player, boolean forced) {
        if(player.isComputer())
            return;
        try{
            PlayerUserIDMap playerUserIDMap = UserService.getPlayerUserMap(this.game);
            WebCommunicator.pushOut(PhaseEndBidEvent.request(this, playerUserIDMap));
        }catch(JSONException | SQLException e1){
            Util.log(e1, "Failed to push end phase bid event.");
        }
        for(Player c: game.players){
            if(c.isEliminated())
                continue;
            if(c == player)
                continue;
            if(c.endedNight())
                continue;
            if(!c.isComputer()){
                return;
            }
        }
        game.forceEndNight(Constants.NO_TIME_LEFT);
    }

    @Override
    public void onCancelEndNight(Player player) {
        try{
            PlayerUserIDMap playerUserIDMap = UserService.getPlayerUserMap(this.game);
            WebCommunicator.pushOut(PhaseEndBidEvent.request(this, playerUserIDMap));
        }catch(JSONException | SQLException e1){
            Util.log(e1, "Failed to push end phase bid event.");
        }
    }

    @Override
    public void onMessageReceive(Player receiver, Message e) {
        Optional<Long> optUserID;
        try{
            optUserID = PlayerRepo.getUserID(this.gameID, receiver.getID());
        }catch(SQLException f){
            Util.log(f);
            return;
        }
        if(!optUserID.isPresent())
            return;

        long userID = optUserID.get();
        WebCommunicator.pushWarningToUser(e, userID, Optional.of(receiver), game.isNight());

        if(!(e instanceof ChatMessage))
            return;
        ChatMessage cm = (ChatMessage) e;
        if(cm.eventLog == null)
            return;

        if(cm.getSender().getPlayer() != receiver){
            setSingleUnread(cm.eventLog, userID);
            return;
        }

        ChatService.setRead(this.gameID, userID, cm.eventLog.getName());
    }

    @Override
    public void onChatMessageSend(ChatMessage message, Optional<String> source, JSONObject args) {
        WebCommunicator.pushOut(new ChatMessageSendEvent(message, source, this.gameID, args));
    }

    @Override
    public void onWarningReceive(Player player, Message m) {
        try{
            Optional<Long> userID = PlayerRepo.getUserID(this.gameID, player.getID());
            WebCommunicator.pushWarningToUser(m.access(player), userID);
        }catch(SQLException e){
            Util.log(e);
        }
    }

    @Override
    public void onModKill(PlayerList bad) {
        sendGameState();
        resetChat();

        if(game.isDay())
            setUnread(game.getEventManager().getDayChat());
        else if(game.isNight())
            setUnread(game.getEventManager().getNightLog(VoidChat.KEY));

        try{
            String message = bad.getStringName() + " has/have inexplicably died.";
            GameLobby gameLobby = LobbyService.getGameLobby(this);
            WebCommunicator.pushOut(DayDeathEvent.request(gameLobby, bad, message));
        }catch(JSONException | SQLException e1){
            Util.log(e1, "Failed to execute modkill event.");
        }
    }

    private void broadcast(Message m, boolean toSlack) {
        for(Player p: game.players)
            p.sendMessage(m);

        if(toSlack)
            pushToDiscordUsers(this.id, this.gameID, m.access(Message.PUBLIC), m);
    }

    public static void pushToSlackUsers(String lobbyID, GameID gameID, String s) {
        pushToDiscordUsers(lobbyID, gameID, s, null);
    }

    public static void pushToDiscordUsers(String lobbyID, GameID gameID, String s, Message e) {
        try{
            WebCommunicator.pushOut(BroadcastEvent.request(lobbyID, gameID, s, e));
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    @Override
    public void onNightEnding() {
        // saveCommands();
    }

    public void speakAnnouncement(String s) {
        try{
            Set<Long> userIDs = GameUserRepo.getIDs(this.gameID);
            JSONObject jo = new JSONObject();
            jo.put(StateObject.speechContent, s);
            WebCommunicator.pushToUsers(jo, userIDs);
        }catch(JSONException | SQLException e){
            Util.log(e);
        }
    }

    @Override
    public void onAnnouncement(Message m) {
        if(game.isNight() && m instanceof SnitchAnnouncement)
            this.snitchAnnouncements.add((SnitchAnnouncement) m);

        String message = m.access(Message.PUBLIC);
        speakAnnouncement(message);

        // why am i skipping this?
        if((m instanceof DeathAnnouncement || m instanceof SnitchAnnouncement) && game.isNight())
            return;
        broadcast(m, false);// don't announce, because my slackswitch will broadcast the messages there
    }

    public GameID getGameID() {
        return this.gameID;
    }
}

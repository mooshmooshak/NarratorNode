package nnode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import json.JSONException;
import json.JSONObject;

public class Config {

    static HashMap<String, Object> config = new HashMap<>();

    public static void loadConfig() {
        if(!config.isEmpty())
            return;

        config.put("db_enabled", true);

        StringBuffer sb = new StringBuffer();
        File directory = new File(System.getenv("NARRATOR_CONFIG_LOCATION"));
        BufferedReader reader;
        try{
            reader = new BufferedReader(new FileReader(directory));
            for(String line; (line = reader.readLine()) != null;){
                sb.append(line);
            }
            JSONObject jo = new JSONObject(sb.toString());
            Iterator<?> keyIterator = jo.keys();
            while (keyIterator.hasNext()){
                String key = keyIterator.next().toString();
                config.put(key, jo.get(key));
            }
        }catch(JSONException | IOException e1){
            e1.printStackTrace();
        }
    }

    public static String getString(String key) {
        loadConfig();
        return config.get(key).toString();
    }

    static String getNestedString(String nested, String key) {
        loadConfig();
        String jString = config.get(nested).toString();
        JSONObject jo;
        try{
            jo = new JSONObject(jString);
            return jo.getString(key);
        }catch(JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getBoolean(String key) {
        loadConfig();
        return Boolean.parseBoolean(config.get(key).toString());
    }

    public static long getLong(String key) {
        loadConfig();
        return Integer.parseInt(config.get(key).toString());
    }

    public static int getInt(String key) {
        loadConfig();
        return Integer.parseInt(config.get(key).toString());
    }

    public static double getDouble(String key) {
        loadConfig();
        return Double.parseDouble(config.get(key).toString());
    }

}

package nnode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import game.logic.exceptions.StoryReaderError;
import game.logic.support.StoryPackage;
import game.logic.support.StoryReader;
import game.setups.Setup;
import models.Command;
import models.enums.GameModifierName;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.modifiers.Modifier;
import models.schemas.GameOverviewSchema;
import models.schemas.PlayerSchema;
import repositories.CommandRepo;
import repositories.GameModifierRepo;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import services.GameSpawnsService;
import services.PlayerRoleService;
import services.SetupService;
import util.Util;

public class DBStoryPackage implements StoryReader {

    public long setupID;

    GameID gameID;

    public static StoryPackage deserialize(GameID unfinishedGameID) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(unfinishedGameID);
        Setup setup = SetupService.getSetup(game.setupID);
        return StoryPackage.deserialize(new DBStoryPackage(unfinishedGameID), setup, unfinishedGameID);
    }

    public DBStoryPackage(long setupID) {
        this.setupID = setupID;
    }

    public DBStoryPackage(GameID unfinishedReplayID) {
        this.gameID = unfinishedReplayID;
        try{
            this.setupID = GameRepo.getSetupID(unfinishedReplayID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    public DBStoryPackage() {
    }

    @Override
    public Collection<Modifier<GameModifierName>> getGameModifiers() {
        try{
            return GameModifierRepo.getByGameID(gameID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public GameOverviewSchema getGameOverview() {
        try{
            return GameRepo.getOverview(gameID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public Set<Long> getModeratorIDs() {
        try{
            return GameUserRepo.getModeratorIDs(gameID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public Set<PlayerSchema> getPlayers() {
        try{
            return PlayerRepo.getByGameID(gameID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    private StoryReaderError getStoryReaderError(SQLException e) {
        String message = "Failed to execute DBStoryPackage query.";
        Util.log(e, message, e.getMessage());
        return new StoryReaderError(message);
    }

    @Override
    public ArrayList<Command> getCommands() {
        try{
            return CommandRepo.getByReplayID(gameID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public Map<Long, Long> getSetupHiddenSpawns() {
        try{
            return GameSpawnsService.getByGameID(gameID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }

    @Override
    public Map<PlayerDBID, Long> getPlayerRoles() {
        try{
            return PlayerRoleService.getByGameID(gameID);
        }catch(SQLException e){
            throw getStoryReaderError(e);
        }
    }
}

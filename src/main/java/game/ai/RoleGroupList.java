package game.ai;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import game.logic.FactionList;
import game.logic.Player;
import models.Faction;
import models.FactionRole;
import models.enums.AbilityType;

public class RoleGroupList implements Iterable<RoleGroup> {

    private Set<RoleGroup> set;
    private HashMap<FactionRole, Boolean> memberMap;
    private HashMap<AbilityType, Boolean> abilityMap;
    private HashMap<String, Boolean> colorMap;

    public RoleGroupList() {
        this.set = new HashSet<>();
        memberMap = new HashMap<>();
        colorMap = new HashMap<>();
        abilityMap = new HashMap<>();
    }

    public void add(RoleGroup roleGroup) {
        this.set.add(roleGroup);
        this.cacheClear();
    }

    public void remove(RoleGroup roleGroup) {
        this.set.remove(roleGroup);
        this.cacheClear();
    }

    public boolean contains(FactionRole role) {
        if(memberMap.containsKey(role))
            return memberMap.get(role);
        for(RoleGroup roleGroup: this)
            if(roleGroup.contains(role)){
                this.memberMap.put(role, true);
                return true;
            }
        this.memberMap.put(role, false);
        return false;
    }

    public boolean contains(AbilityType ability) {
        if(this.abilityMap.containsKey(ability))
            return abilityMap.containsKey(ability);
        for(RoleGroup roleGroup: this){
            if(roleGroup.contains(ability)){
                this.abilityMap.put(ability, true);
                return true;
            }
        }
        this.abilityMap.put(ability, false);
        return false;
    }

    public boolean contains(String color) {
        if(this.colorMap.containsKey(color))
            return colorMap.containsKey(color);
        for(RoleGroup roleGroup: this){
            if(roleGroup.contains(color)){
                this.colorMap.put(color, true);
                return true;
            }
        }
        this.colorMap.put(color, false);
        return false;
    }

    public boolean hasTeam(FactionList factions) {
        for(Faction faction: factions){
            if(contains(faction.getColor()))
                return true;
        }
        return false;
    }

    public int size() {
        return this.set.size();
    }

    public boolean crossOff(FactionRole a) {
        RoleGroup r1 = null, r2 = null;
        for(RoleGroup r: this.set){
            if(!r.contains(a))
                continue;
            if(r1 == null)
                r1 = r;
            else
                r2 = r;
        }

        if(r1 == null)
            // i guess this could be some sort of exception
            return false;
        remove(r1);

        if(r2 != null)
            r2.add(a);
        cacheClear();
        return true;
    }

    public boolean crossOff(Player p) {
        Optional<FactionRole> optFactionRole = p.getGraveyardFactionRole();
        if(!optFactionRole.isPresent())
            return false;
        String roleName = optFactionRole.get().getName();
        String color = optFactionRole.get().getColor();
        RoleGroup r1 = null, r2 = null;
        for(RoleGroup r: this.set){
            for(FactionRole role: r){
                if(!role.getColor().equals(color))
                    continue;
                if(!role.getName().equals(roleName))
                    continue;
                if(r1 == null)
                    r1 = r;
                else
                    r2 = r;
            }
        }

        if(r1 == null)
            // i guess this could be some sort of exception
            return false;
        remove(r1);

        if(r2 != null)
            r2.add(r1);
        cacheClear();
        return true;
    }

    private void cacheClear() {
        this.memberMap.clear();
        this.abilityMap.clear();
    }

    @Override
    public Iterator<RoleGroup> iterator() {
        return set.iterator();
    }

    public RoleGroupList copy() {
        RoleGroupList roleGroupList = new RoleGroupList();
        for(RoleGroup roleGroup: this)
            roleGroupList.add(roleGroup.copy());
        return roleGroupList;
    }

}

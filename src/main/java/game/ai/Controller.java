package game.ai;

import java.util.ArrayList;
import java.util.Optional;

import game.event.ChatMessage;
import game.logic.Player;
import game.logic.support.action.Action;
import json.JSONObject;
import models.Role;
import models.enums.AbilityType;

public interface Controller {

    void log(String string);

    void endNight(double timeLeft);

    void cancelEndNight(double timeLeft);

    void setNightTarget(Action action);

    void clearTargets();

    // 0 indexed
    void cancelAction(int actionIndex, double timeLeft);

    ChatMessage say(String message, String key, Optional<String> source, JSONObject args);

    void doDayAction(Action action);

    void rolePrefer(Role role);

    boolean isTargeting(AbilityType abilityType, Controller... targets);

    Controller setName(String string);

    String getName();

    String getColor();

    String getRoleName();

    ArrayList<String> getChatKeys(); // I want to make sure the tests for this are gone //TODO remove

    ArrayList<AbilityType> getCommands();

    Player getPlayer();

    void setLastWill(String string);

    void setTeamLastWill(String string);

    boolean is(AbilityType class1);

}

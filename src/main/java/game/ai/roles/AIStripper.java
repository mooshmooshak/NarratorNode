package game.ai.roles;

import game.abilities.Block;
import game.ai.Computer;
import game.ai.RoleClaim;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;

public class AIStripper {

    public static void doRoleNightAction(Computer c) {
        Player slave = c.player;

        PlayerList selections = c.brain.slaves.getLivePlayers().remove(c.player);// slave.getAcceptableTargets(Role.MAIN_ABILITY);
        PlayerList teamMates = c.getFactionmates();

        // don't need to handle the case where you're enemies with your team
        // because you don't know your teammates if you're enemies with them
        selections.remove(teamMates);

        // roleblock known enemies
        // roleblock someone that is voting enemy aligned

        PlayerList knownEnemies = new PlayerList();

        for(Player p: selections){
            RoleClaim rClaim = c.brain.getRoleClaim(p);
            if(rClaim == null || rClaim.getClaimer().isDead())
                continue;
            if(rClaim.believable(c) && slave.getGameFaction().faction.enemies.contains(rClaim.faction)){
                knownEnemies.add(p);
            }

        }
        if(!knownEnemies.isEmpty())
            selections = knownEnemies;

        for(PlayerList suspList: c.brain.getSuspiciousPeople(slave)){
            selections.softRemove(suspList);
        }

        Player choice;
        while (!selections.isEmpty()){
            choice = c.brain.random.getPlayer(selections);
            try{
                c.controller.setNightTarget(new Action(slave, Block.abilityType, choice));
                break;
            }catch(PlayerTargetingException e){
                selections.remove(choice);
            }
        }
    }
}

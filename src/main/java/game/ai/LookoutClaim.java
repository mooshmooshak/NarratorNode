package game.ai;

import java.util.ArrayList;

import game.abilities.Lookout;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import models.Faction;

public class LookoutClaim extends Claim {

    private ArrayList<Faction> teams;
    private PlayerList possibleKillers;

    public LookoutClaim(Player prosecutor, PlayerList possibleKillers, ArrayList<Faction> teams, Brain b) {
        super(prosecutor, b);
        this.teams = teams;
        this.possibleKillers = possibleKillers;
    }

    @Override
    public PlayerList getAccused() {
        return possibleKillers;
    }

    @Override
    public boolean believable(Computer c) {
        if(possibleKillers.getLivePlayers().isEmpty())
            return false;
        if(c.player == getClaimer())
            return true;
        for(Player p: possibleKillers.getDeadPlayers()){
            for(Faction t: teams){
                if(p.getColor().equals(t.getColor()))
                    return false;
            }
        }

        return true;
    }

    @Override
    public boolean outlandish(Computer slave) {
        PlayerList deadAccused = getAccused().getDeadPlayers();
        if(deadAccused.isEmpty())
            return false;

        for(Player acc: getAccused().getDeadPlayers()){
            if(acc.getDeathType().isHidden())
                return false;
            if(teams.contains(acc.getGameFaction().faction))
                return false;
        }
        // i did not find a dead person whos team was a team that the prosecutor claimed
        return true;
    }

    @Override
    public boolean valid(RoleGroupList rolesList, Computer c) {
        if(c.player.equals(getClaimer()))
            return true;

        if(!rolesList.contains(Lookout.abilityType))
            return false;
        return true;
    }

    @Override
    public void talkAboutBadClaim(Computer c) {
        StringBuilder sb = new StringBuilder();
        sb.append("I don't believe " + getClaimer().getName() + "'s claim that " + possibleKillers.getStringName()
                + " is part of the ");
        if(teams.size() == 1)
            sb.append(teams.get(0).getName());
        else{
            Faction lastTeam = teams.get(teams.size() - 1);
            for(Faction t: teams){
                if(t == lastTeam){
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append(" or ");
                }
                sb.append(t.getName());
                sb.append(", ");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append('.');
        }
        c.say(sb.toString(), Constants.DAY_CHAT);

    }

    @Override
    public PlayerList getPlayersToActOn(Computer computer) {
        if(outlandish(computer))
            return new PlayerList(getClaimer());
        return getAccused();
    }

    @Override
    public PlayerList getSafePlayers(Computer computer) {
        // if(!believable(computer))
        return new PlayerList();

    }
}

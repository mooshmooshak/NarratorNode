package game.ai;

import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import models.Faction;
import models.FactionRole;

public class RoleClaim extends Claim {

    FactionRole role;
    public Faction faction;

    public RoleClaim(Player claimer, FactionRole role, Brain brain) {
        super(claimer, brain);
        this.role = role;
        this.faction = role.faction;
    }

    @Override
    public PlayerList getAccused() {
        return new PlayerList();
    }

    @Override
    public boolean believable(Computer c) {
        return c.rolesList.contains(role);
    }

    @Override
    public boolean outlandish(Computer c) {
        return c.rolesList.contains(role);
    }

    @Override
    public String toString() {
        return role.toString();
    }

    public void feedToComputer(Computer comp) {
        if(!comp.rolesList.crossOff(role)){ // can't cross it off because it's not legit
            comp.badClaims.add(this);
            comp.say("I don't believe that claim", Constants.DAY_CHAT);
        }
    }

    @Override
    public boolean valid(RoleGroupList rolesList, Computer c) {
        if(c.player.equals(this.getClaimer()))
            return true;
        if(getClaimer().isDead())
            return true;
        return rolesList.crossOff(role);
    }

    @Override
    public void talkAboutBadClaim(Computer c) {
        if(c.player.isSilenced() || c.player.isPuppeted())
            return;
        String teamName = faction.name;
        StringBuilder sb = new StringBuilder();
        sb.append("I don't believe that " + getClaimer().getName() + " is a " + teamName + " " + role.getName());

        c.say(sb.toString(), Constants.DAY_CHAT);
    }

    @Override
    public PlayerList getPlayersToActOn(Computer computer) {
        if(outlandish(computer))
            return new PlayerList(getClaimer());
        return new PlayerList();
    }

    @Override
    public PlayerList getSafePlayers(Computer computer) {
        if(!believable(computer))
            return new PlayerList();

        if(computer.player.getGameFaction().faction.enemies.contains(faction))
            return new PlayerList();

        return new PlayerList(getClaimer());

    }
}

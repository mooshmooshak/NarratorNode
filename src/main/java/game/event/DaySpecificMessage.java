package game.event;

public interface DaySpecificMessage {
    public String getName();
}

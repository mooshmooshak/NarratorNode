package game.event;

import java.util.ArrayList;

import game.logic.Game;
import game.logic.Player;
import models.enums.GamePhase;
import models.enums.SetupModifierName;

public class Happening extends Message {

    public Happening(Game narrator) {
        super(narrator);
        setPrivate();

        if(!n.isStarted())
            n.prehappenings.add(this);
        else if(narrator.phase == GamePhase.ROLE_PICKING_PHASE){
            if(narrator.getBool(SetupModifierName.DAY_START))
                n.getEventManager().getDayChat().add(this);
            else
                n.getEventManager().voidChat.add(this);
        }else if(narrator.phase == GamePhase.NIGHT_ACTION_SUBMISSION)
            n.getEventManager().voidChat.add(this);
        else
            n.getEventManager().getDayChat().add(this);
    }

    @Override
    public ArrayList<String> getEnclosingChats(Game n, Player unused) {
        ArrayList<String> chats = new ArrayList<>();
        EventManager em = n.getEventManager();
        if(n.isInProgress() && unused.isAlive()){
            chats.add(Feedback.CHAT_NAME);
        }
        if(getPhase() == GamePhase.NIGHT_ACTION_SUBMISSION)
            chats.add(em.getNightLog(VoidChat.KEY).getName());
        else if(getDay() != 0)
            chats.add(getSpecificDayChat());
        return chats;
    }

}

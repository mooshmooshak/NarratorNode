package game.event;

import java.util.ArrayList;
import java.util.List;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;

public class VoteAnnouncement extends Announcement implements DaySpecificMessage {

    /*
     * System Messages are dead people announcements on the start of the day
     */

    // unvoting
    public VoteAnnouncement(PlayerList previous, Player voter) {
        super(voter.game);
        this.voter = voter;
        voteCount = voter.getVotePower();
        if(previous != null)
            this.prev = previous.getNamesToStringList();
    }

    public Player voter;
    public List<String> voteTarget, prev;
    public int voteCount;

    public VoteAnnouncement(PlayerList prev, Player voter, PlayerList voted) {
        this(prev, voter);
        this.voteTarget = voted.getNamesToStringList();
    }

    @Override
    public String getName() {
        if(dc != null)
            return dc.getName();
        return null;
    }

    EventLog dc;

    // any changes i do here should be reflected in the super
    @Override
    public void finalize() {
        EventManager em = n.getEventManager();
        dc = em.getDayChat().add(this);
    }

    @Override
    public ArrayList<String> getEnclosingChats(Game n, Player p) {
        return addHeaders(getSpecificDayChat());
    }

    @Override
    public Integer getID() {
        return null;
    }
}

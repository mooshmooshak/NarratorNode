package game.event;

public interface AnnouncementCheckCondition {

    boolean isAnnounceable();
}

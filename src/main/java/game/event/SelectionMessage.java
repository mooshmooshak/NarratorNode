package game.event;

import java.util.ArrayList;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import models.GameFactions;

public class SelectionMessage extends Message {

    private EventLog el;
    private PlayerList toSend;
    private boolean isNightMessage;
    public Player owner; // need this in case I don't want to send out the selection message to the chat

    public SelectionMessage(Player owner, boolean teamSend) {
        this(owner, teamSend, owner.game.isNight());
    }

    public SelectionMessage(Player owner, boolean teamSend, boolean addToLog) {
        super(owner.game);
        this.owner = owner;
        this.isNightMessage = owner.game.isNight();
        toSend = new PlayerList();

        Game game = owner.game;
        if(addToLog && teamSend && game.isNight()){
            GameFactions factionsWithKnownAllies = owner.getFactions().filterKnowsTeam();
            toSend = factionsWithKnownAllies.getFactionMates().filterUnquarantined();
            if(!toSend.contains(owner))
                toSend.add(owner);
            setVisibility(toSend);
            el = game.getEventManager().getNightLog(VoidChat.KEY);
        }else{
            if(game.isDay())
                el = game.getEventManager().getDayChat();
            else
                el = game.getEventManager().getNightLog(VoidChat.KEY);
            if(!teamSend){
                setPrivate();
                dontShowPrivate();
            }
            setVisibility(owner);
            toSend.add(owner);
        }
        if(addToLog)
            el.add(this);
    }

    public void recordCommand(Player p, double timeLeft, String... command) {
        p.game.getEventManager().addCommand(p, timeLeft, command);
    }

    public void pushOut() {
        toSend.sendMessage(this);
    }

    public void addToPushOut(PlayerList notSentYet) {
        for(Player p: notSentYet){
            if(!p.in(toSend))
                toSend.add(p);
        }
    }

    @Override
    public ArrayList<String> getEnclosingChats(Game n, Player p) {
        EventManager em = n.getEventManager();
        ArrayList<String> ret = new ArrayList<>();
        if(p != null && p.isDead())
            return new ArrayList<>();
        else if(!n.isInProgress()){// game over
            if(isNightMessage)
                addHeaders(ret, em.getNightLog(VoidChat.KEY).getName());
            else
                addHeaders(ret, getSpecificDayChat());
        }else{
            if(p != owner){ // handles faction chats
                if(!isNightMessage)
                    addHeaders(ret, getSpecificDayChat());
                else if(el instanceof FactionChat && ((FactionChat) el).getMembers().size() == 1)
                    addHeaders(ret, n.getEventManager().getNightLog(VoidChat.KEY).getName());
                else if(p != null && p.isQuarintined())
                    return ret;
                addHeaders(ret, el.getName());
            }else if(!isNightMessage){
                addHeaders(ret, getSpecificDayChat());
            }else if(!(el instanceof VoidChat)){
                if(p.isQuarintined())
                    addHeaders(ret, Feedback.CHAT_NAME, em.getNightLog(VoidChat.KEY).getName());
                else
                    addHeaders(ret, Feedback.CHAT_NAME, el.getName(), em.getNightLog(VoidChat.KEY).getName());
            }else{
                addHeaders(ret, Feedback.CHAT_NAME, el.getName());
            }
        }
        return ret;
    }

    @Override
    public Integer getID() {
        return null;
    }

}

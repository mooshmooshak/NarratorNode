package game.event;

import java.util.ArrayList;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;

public class OGIMessage extends Message {

    public OGIMessage(Player p, String message) {
        this();
        add(message);
        p.sendMessage(this);
    }

    public OGIMessage(PlayerList texters, String message) {
        this();
        for(Player p: texters)
            new OGIMessage(p, message);
    }

    public OGIMessage(Player p, Object... objects) {
        this(Player.list(p), objects);
    }

    public OGIMessage(PlayerList allPlayers, Object... objects) {
        this();
        for(Object o: objects)
            add(o);
        setVisibility(allPlayers);
        for(Player p: allPlayers)
            p.sendMessage(this);
    }

    public OGIMessage() {
        super(null);
    }

    public OGIMessage(Game n) {
        super(n);
    }

    public ArrayList<String> getEnclosingChats(Game n, Player p) {
        return new ArrayList<>();
    }

}

package game.event;

import java.util.ArrayList;

import game.logic.Game;
import game.logic.Player;
import models.enums.GamePhase;
import models.enums.SetupModifierName;

public class Feedback extends Message implements Comparable<Feedback> {

    public static final String CHAT_NAME = "Clues";

    public static final String TYPE = "fas fa-search";

    public static final boolean PREGAME_FEEDBACK = true;

    public boolean hideableFeedback = true;

    public Player player;

    public Feedback(Player p) {
        this(p, false);
    }

    public Feedback(Player p, boolean pregameFeedback) {
        super(p.game);
        this.player = p;
        setVisibility(p);
        if(p.game.isNight()){
            if(pregameFeedback && p.game.isFirstPhase()){
                p.game.getEventManager().getDayChat().add(this);
                setDay(getDay() - 1);
                setPhase(GamePhase.VOTE_PHASE);
            }else{
                p.addNightFeedback(this);
            }
        }else
            p.game.getEventManager().getDayChat().add(this);
        dontShowPrivate();
    }

    public Feedback(Player p, String feedback) {
        this(p);
        add(feedback);
    }

    @Override
    public String access(String key, EventDecoder html) {
        if(html == null)
            return super.access(key, html);
        return html.feedbackDecode(super.access(key, null));
    }

    private boolean sorted = false;

    public Message setSorted() {
        sorted = true;
        return this;
    }

    public int compareTo(Feedback o) {
        if(o.sorted && sorted)
            return 0;
        if(o.sorted)
            return -1;
        if(sorted)
            return 1;
        return 0;
    }

    public ArrayList<String> getEnclosingChats(Game n, Player p) {
        ArrayList<String> chats = new ArrayList<>();
        EventManager em = n.getEventManager();
        if(p != null){
            if(n.isInProgress() && p.isAlive())
                chats.add(Feedback.CHAT_NAME);

            if(n.phase == GamePhase.NIGHT_ACTION_SUBMISSION || n.getDayNumber() != getDay() - 1)
                if(getPhase() == GamePhase.NIGHT_ACTION_SUBMISSION || !n.getBool(SetupModifierName.DAY_START)
                        || n.getDayNumber() != 1)
                    chats.add(em.getNightLog(VoidChat.KEY).getName());
            if(getDay() != 0 || getPhase() != GamePhase.VOTE_PHASE)
                chats.add(getSpecificDayChat());
        }
        return chats;
    }
}

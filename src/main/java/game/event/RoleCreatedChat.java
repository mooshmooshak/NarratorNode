package game.event;

import game.logic.PlayerList;

public abstract class RoleCreatedChat extends NightChat {

    private int day;

    public RoleCreatedChat(PlayerList members, int day) {
        super(members.getFirst().game, members);
        this.day = day;
    }

    public int getDay() {
        return day;
    }

}

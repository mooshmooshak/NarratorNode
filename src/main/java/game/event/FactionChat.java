package game.event;

import game.abilities.Disguiser;
import game.abilities.FactionKill;
import game.abilities.Mason;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;

public class FactionChat extends NightChat {

    private GameFaction factionChat;
    private PlayerList masons;
    private Game narrator;

    public FactionChat(Game n, GameFaction t) {
        this(n, t.getMembers().filterUnquarantined());
        factionChat = t;

        masons = null;
    }

    // garunteed to have a team of 2 players that isn't architected/jailed
    public FactionChat(Game n, PlayerList masons) {
        super(n, masons.filterUnquarantined());

        this.narrator = n;
        this.masons = masons;

        for(Player p: masons){
            if(!p.isQuarintined())
                p.addChat(this);
        }
    }

    @Override
    public EventLog add(Message e) {
        if(e instanceof SelectionMessage){
            SelectionMessage sm = (SelectionMessage) e;
            sm.setVisibility(getMembers());
            sm.addToPushOut(getMembers());
        }
        return super.add(e);
    }

    @Override
    public boolean hasAccess(String... ids) {
        for(String id: ids){
            Player player = super.getMembers().getByID(id);
            if(player == null)
                continue;
            if(player.isQuarintined())
                continue;
            // private name checker
            return true;
        }
        return super.hasAccess(ids);
    }

    public GameFaction getFaction() {
        if(factionChat != null)
            return factionChat;
        return masons.getFirst().getGameFaction();
    }

    @Override
    public void refreshVisibility() {
        PlayerList members;
        if(factionChat != null)
            members = factionChat.getMembers();
        else{
            members = new PlayerList();
            for(Player p: narrator.getLivePlayers()){
                if(Mason.IsMasonType(p))
                    members.add(p);
                else if(Disguiser.hasDisguised(p) && Mason.IsMasonType(Disguiser.getDisguised(p)))
                    members.add(p);
            }
        }

        for(Player player: members){
            if(narrator.isNight()){
                if(!player.isQuarintined() && !getMembers().contains(player)){
                    player.addChat(this);
                    super.addAccess(player);
                }
            }else if(narrator.isDay()){
                if(!player.wasQuarantined(narrator.getDayNumber() - 1)){
                    player.addChat(this);
                    super.addAccess(player);
                }
            }
        }
        Player p;
        for(int i = 0; i < getMembers().size(); i++){
            p = getMembers().get(i);
            if(!p.in(members)){
                super.removeAccess(p);
                removeAccess(p);
                i--;
            }
        }
    }

    @Override
    public String getName() {
        if(factionChat != null)
            return factionChat.getName() + " Chat";
        if(masons != null)
            return "Mason Chat";
        return "Night " + narrator.getDayNumber();
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getKey(Player p) {
        if(factionChat != null)
            return factionChat.getColor();
        return masons.getFirst().getColor();
    }

    @Override
    public String getType() {
        if(factionChat == null || !factionChat.hasAbility(FactionKill.abilityType))
            return "fas fa-user-secret";
        return "fas fa-bomb";
    }
}

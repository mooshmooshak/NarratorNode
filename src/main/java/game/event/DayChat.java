package game.event;

import game.abilities.Ghost;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.Constants;

public class DayChat extends EventLog {

    public static final String KEY = Constants.DAY_CHAT;

    private Game n;

    public DayChat(Game n) {
        super(n.players);
        this.n = n;
    }

    @Override
    public boolean hasAccess(String... name) {
        return true;
    }

    @Override
    public String getName() {
        return "Day " + n.getDayNumber();
    }

    @Override
    public String getKey(Player p) {
        if(p == null)
            return null;
        else if(!isActive())
            return null;
        else if(!p.game.isInProgress())
            return KEY;
        else if(p.isPuppeted() && !p.hasPuppets())
            return null;
        else if(p.isSilenced()){
            if(p.hasPuppets())
                return p.getPuppets().getFirst().getName();
            return null;
        }else if(p.is(Ghost.abilityType) && p.hasPuppets())
            return p.getPuppets().getFirst().getName();
        else if(p.isDead())
            return null;
        else
            return KEY;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getType() {
        return "fas fa-sun";
    }

}

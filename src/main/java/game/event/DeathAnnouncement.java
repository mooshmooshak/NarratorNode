package game.event;

import java.util.ArrayList;

import game.logic.DeathType;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import models.enums.GamePhase;

public class DeathAnnouncement extends Announcement implements DaySpecificMessage {

    /*
     * System Messages are dead people announcements on the start of the day
     */

    // unvoting
    public PlayerList dead;
    private boolean dayEndDeath = false;
    private boolean nightDeath;

    public DeathAnnouncement(Player dead) {
        this(dead, !NIGHT_DEATH);
    }

    public DeathAnnouncement(Player dead, boolean nightDeath) {
        this(Player.list(dead), dead.game, nightDeath);
    }

    public DeathAnnouncement(PlayerList dead, Game n, boolean nightDeath) {
        super(n);
        this.dead = dead;
        this.nightDeath = nightDeath;
    }

    public void setDayEndDeath() {
        dayEndDeath = true;
    }

    @Override
    public String getName() {
        return n.getEventManager().getDayChat().getName();
    }

    @Override
    public ArrayList<String> getEnclosingChats(Game n, Player p) {
        ArrayList<String> list = new ArrayList<>();

        if((n.isInProgress() || n.getDayNumber() != getDay()) && dayEndDeath)
            if(n.getDayNumber() != getDay() || n.phase == GamePhase.NIGHT_ACTION_SUBMISSION)
                list.add(n.getEventManager().getNightLog(VoidChat.KEY).getName());
        list.add(getSpecificDayChat());
        if(n.isInProgress() && p != null && p.isAlive())
            list.add(Feedback.CHAT_NAME);

        return list;
    }

    @Override
    public boolean isNightToDayAnnouncement() {
        return nightDeath;
    }

    @Override
    public String getPicture() {
        if(dead != null && !dead.isEmpty()){
            // return
        }
        return "lookout";
    }

    @Override
    public ArrayList<String> getExtras() {
        ArrayList<String> ret = super.getExtras();
        if(ret == null)
            ret = new ArrayList<>();

        DeathAnnouncement da;

        for(Player p: dead){
            if(getPhase() == GamePhase.NIGHT_ACTION_SUBMISSION){
                da = new DeathAnnouncement(null, (Game) null, false);
                for(Object o: DeathType.getInfoParts(p))
                    da.add(o);
                ret.add(da.access(Message.PUBLIC));
            }
            if(p.getLastWill(null) != null && p.getLastWill(null).length() != 0)
                ret.add(p.getName() + " left a last will:");
            ret.add(p.getLastWill(null));
        }

        return ret;
    }
}

package game.event;

import game.logic.Game;
import game.logic.PlayerList;

public abstract class NightChat extends EventLog {

    public NightChat(Game n, PlayerList members) {
        super(members);
    }

    /*
     * overridden by faction chat
     */
    public void refreshVisibility() {
    }
}

package game.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game.abilities.AbilityList;
import game.abilities.Disguiser;
import game.abilities.FactionSend;
import game.abilities.GameAbility;
import game.abilities.Goon;
import game.abilities.util.AbilityUtil;
import game.event.Message;
import game.event.OGIMessage;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.condorcet.Condorcet;
import models.Ability;
import models.Faction;
import models.GameModifiers;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;

public class GameFaction {

    public static final int FACTION_INT_MAX = 127;

    public AbilityList _abilities;
    public GameModifiers<FactionModifierName> modifiers;

    public Game game;
    public Faction faction;

    public int _startingSize = 0; // this is used for telling ghosts what team

    private AbilityList attainedAbilities;

    PlayerList infiltrators;

    public GameFaction(Game game, Faction faction) {
        this.game = game;
        this.faction = faction;
        this._abilities = getFactionAbilities();
        modifiers = new GameModifiers<>(game, faction.modifiers);
    }

    // internal helper method
    private AbilityList getFactionAbilities() {
        AbilityList abilities = new AbilityList();
        GameAbility gameAbility;
        for(Ability ability: faction.abilities){
            gameAbility = AbilityUtil.CREATOR(ability.type, this.game, ability.modifiers);
            gameAbility.initialize(this);
            abilities.add(gameAbility);
        }
        return abilities;
    }

    public String getName() {
        return faction.name;
    }

    public boolean winsOver(GameFaction t) {
        if(this.getPriority() == t.getPriority())
            return isEnemy(this) == t.isEnemy(t);

        // if my priority
        return this.getPriority() > t.getPriority();
    }

    public int getPriority() {
        return this.modifiers.getInt(FactionModifierName.WIN_PRIORITY);
    }

    public String getColor() {
        return faction.color;
    }

    public void onGameStart() {
        _startingSize = members.size();
        if(_abilities.isEmpty())
            return;
        if(game.setup.hasFactionRolesThatAffectSending(game))
            _abilities = new AbilityList(_abilities, new FactionSend(game));
        for(GameAbility a: _abilities)
            a.initialize(this);
    }

    public boolean hasSharedAbilities() {
        return _abilities != null || attainedAbilities != null;
    }

    public boolean hasAbility(AbilityType abilityType) {
        return getAbility(abilityType) != null;
    }

    public GameAbility getAbility(AbilityType abilityType) {
        if(_abilities != null)
            for(GameAbility a: _abilities)
                if(a.getAbilityType() == abilityType)
                    return a;
        if(attainedAbilities != null)
            for(GameAbility a: attainedAbilities)
                if(a.getAbilityType() == abilityType)
                    return a;
        return null;
    }

    public GameAbility addAttainedAbility(AbilityType abilityType) {
        GameAbility gameAbility = AbilityUtil.CREATOR(abilityType, game, new Modifiers<>());
        if(attainedAbilities == null && _abilities == null)
            attainedAbilities = new AbilityList(gameAbility, new FactionSend(game));
        else if(attainedAbilities == null)
            attainedAbilities = new AbilityList(gameAbility);
        else
            attainedAbilities = new AbilityList(attainedAbilities, gameAbility);
        if(sender == null)
            sender = new HashMap<>();
        return gameAbility;
    }

    public boolean getAliveToWin() {
        return this.modifiers.getBoolean(FactionModifierName.LIVE_TO_WIN);
    }

    public void addAlly(GameFaction allyTeam) {
        this.faction.enemies.remove(allyTeam.faction);
        allyTeam.faction.enemies.remove(this.faction);
    }

    public Set<GameFaction> getEnemies() {
        Set<GameFaction> enemies = new HashSet<>();
        for(GameFaction gameFaction: this.game.getFactions())
            if(this.faction.enemies.contains(gameFaction.faction))
                enemies.add(gameFaction);
        return enemies;
    }

    public boolean hasEnemies() {
        return !faction.enemies.isEmpty();
    }

    public boolean isEnemy(GameFaction t) {
        return this.faction.enemies.contains(t.faction);
    }

    private PlayerList members = new PlayerList();

    public void addMember(Player player) {
        members.add(player);
    }

    public void removeMember(Player p) {
        members.remove(p);
    }

    public PlayerList getMembers() {
        return members.copy();
    }

    public int size() {
        return members.size();
    }

    public boolean isAlive() {
        if(members.isEmpty())
            return false;
        for(Player p: members){
            if(p.isAlive() && p.getGameFaction() == this)
                return true;
        }
        return false;
    }

    // players that put in their night action kill

    public static final GameFaction NOT_SUSPICIOUS = null;

    private HashMap<AbilityType, Player> sender = new HashMap<>();

    public Player determineNightActionController(AbilityType abilityType) {
        Player newSender = null;
        if(!members.getLivePlayers().isEmpty() && hasAbility(abilityType)){
            newSender = getCurrentController(abilityType, true); // night IS ending
            if(newSender != null){
                sender.put(abilityType, newSender);
                newSender.addTempAbility(this.getAbility(abilityType));
            }else
                sender.remove(abilityType);
        }
        return newSender;
    }

    public Player getFactionAbilityController(AbilityType abilityType) {
        return sender.get(abilityType);
    }

    public void overrideSenderViaWitch(Player victim, AbilityType abilityType) {
        if(!sender.containsKey(abilityType)){
            sender.put(abilityType, victim);
            victim.addTempAbility(getAbility(abilityType));
        }

    }

    public Player getCurrentController(AbilityType abilityType) {
        return getCurrentController(abilityType, false);// night isn't ending
    }

    protected Player getCurrentController(AbilityType abilityType, boolean nightEnding) {
        if(_abilities == null && this.attainedAbilities == null)
            return null;

        if(members.size() == 1)
            return members.get(0);
        PlayerList members = this.members.filterSubmittingAction(abilityType);
        if(members.isEmpty())
            return null;
        if(members.size() == 1)
            return members.get(0);

        List<List<Set<Player>>> sendRankings = FactionSend.CondorcetOrdering(this.members);
        List<Set<Player>> order = Condorcet.getOrderings(members.toSet(), sendRankings);
        if(order.isEmpty())
            return null;
        Set<Player> topChoices = order.get(0);
        if(topChoices.size() == 1)
            return topChoices.iterator().next();
        Set<Player> goons = getGoons(topChoices);
        if(goons.size() == 1)
            return goons.iterator().next();
        if(nightEnding){
            if(!goons.isEmpty())
                topChoices = goons;
            topChoices = maxActionSubmission(topChoices);
            return new PlayerList(topChoices).sortBySubmissionTime().getFirst();
        }

        return null;
    }

    private Set<Player> getGoons(Set<Player> list) {
        Set<Player> pl = new HashSet<Player>();
        for(Player p: list){
            if(p.is(Goon.abilityType)){
                pl.add(p);
                continue;
            }
            if(!Disguiser.hasDisguised(p)){
                continue;
            }
            if(Disguiser.getDisguised(p).hasAbility(Goon.abilityType))
                pl.add(p);
        }
        return pl;
    }

    private Set<Player> maxActionSubmission(Set<Player> pl) {
        int maxCount = 0;
        Set<Player> ret = new HashSet<Player>();
        int count;
        for(Player p: pl){
            count = 0;
            for(Action a: p.getActions()){
                if(a.is(getAbilities()))
                    count++;
            }
            if(maxCount > count)
                continue;
            if(maxCount < count){
                ret.clear();
                maxCount = count;
            }
            ret.add(p);
        }
        return ret;
    }

    public boolean isPlayer() {
        return false;
    }

    public AbilityList getAbilities() {
        if(_abilities == null)
            return attainedAbilities;
        else if(attainedAbilities == null)
            return _abilities;
        return new AbilityList(_abilities, attainedAbilities);
    }

    @Override
    public String toString() {
        return faction.name;
    }

    public boolean hasMember(Player p) {
        return members.contains(p);
    }

    public boolean isSolo() {
        return members.size() == 1;
    }

    public void setAllies(GameFaction... enemyTeams) {
        for(GameFaction enemy: enemyTeams)
            addAlly(enemy);
    }

    public PlayerList getTargets() {
        PlayerList ret = new PlayerList();
        for(Player p: members.copy().sortByID()){
            if(p.getGameFaction() == this){
                for(Player target: p.getVisits("team visits")){
                    if(!target.in(ret))
                        ret.add(target);
                }
            }
        }

        return ret;
    }

    public String[] getKillFlag() {
        if(game.getBool(SetupModifierName.DIFFERENTIATED_FACTION_KILLS)){
            return new String[] { Constants.FACTION_KILL_FLAG[0] + getName(),
                    Constants.FACTION_KILL_FLAG[1] + getName() };
        }
        String name = "faction";
        return new String[] { Constants.FACTION_KILL_FLAG[0] + name, Constants.FACTION_KILL_FLAG[1] + name };
    }

    public void clearSheriffDetectables() {
        faction.sheriffCheckables.clear();
    }

    public boolean canRecruitFrom() {
        return this.modifiers.getOrDefault(FactionModifierName.IS_RECRUITABLE, true);
    }

    public boolean knowsTeam() {
        return this.faction.knowsTeam(this.game);
    }

    public boolean hasNightChat() {
        return this.faction.hasNightChat(this.game);
    }

    public int getPunchSuccess() {
        return this.modifiers.getInt(FactionModifierName.PUNCH_SUCCESS);
    }

    public boolean hasLastWill() {
        return this.modifiers.getOrDefault(FactionModifierName.LAST_WILL, false);
    }

    public void infiltrate(Player owner) {
        if(infiltrators == null)
            infiltrators = new PlayerList();
        infiltrators.add(owner);
    }

    public void onDayStart() {
        if(isAlive() && infiltrators != null)
            for(Player infil: infiltrators){
                addMember(infil);
            }
        infiltrators = null;
        if(sender == null)
            return;
        for(AbilityType abilityType: sender.keySet())
            getAbility(abilityType).onDayStart(sender.get(abilityType));

        sender.clear();
    }

    public void onNightStart() {
        AbilityList abilities = getAbilities();
        if(abilities != null)
            for(GameAbility a: abilities)
                a.onNightStart(null);
    }

    public void onNightEnd() {
        if(_abilities != null)
            for(GameAbility a: _abilities)
                a.onPlayerNightEnd(null); // maybe review who's using this.
    }

    // sender is never null
    public static void warnAbilityOwnerChange(Player sender, Player intendedTarget, Player before, Player after,
            GameFaction t) {
        if(!sender.game.setup.hasFactionRolesThatAffectSending(sender.game))
            return;

        Message e = new OGIMessage();
        if(before == null){
            if(after == null)
                return;
            e.add(after, " can perform the faction kill.");
            t.getMembers().remove(sender).warn(e);
        }
        // the send is useless
        else if(before == intendedTarget)
            return;

        // the send isn't changing who is being sent out
        else if(before == after && intendedTarget != null){
            sender.warn(e.add("You voted to send ", intendedTarget, " for the kill, but ", before,
                    " is still in control of the factional kill."));
        }

        else{// after the send, no one is being sent out. tell everyone this.
            if(after == null){
                e.add("There is no longer a consensus on who should be in control of the factional kill.");
                t.getMembers().remove(after).warn(e);
            }else{
                e.add("Instead of ", before, ", ", after, " is now in control of the kill.");
                t.getMembers().warn(e);
            }
        }

    }

    public static GameFaction GetFaction(ArrayList<String> commands, Game narrator) {
        GameFaction faction = narrator.getFaction(commands.get(0));

        if(faction != null){
            commands.remove(0);
            return faction;
        }

        ArrayList<String> toAddBack = new ArrayList<>();

        StringBuilder sb = new StringBuilder(commands.remove(0));
        toAddBack.add(sb.toString());

        faction = narrator.getFactionByName(sb.toString());
        String s;
        while (!commands.isEmpty() && faction == null){
            sb.append(" ");
            s = commands.remove(0);
            toAddBack.add(s);
            sb.append(s);
            faction = narrator.getFactionByName(sb.toString());
        }

        if(faction == null){
            for(int i = toAddBack.size() - 1; i >= 0; i--){
                commands.add(0, toAddBack.remove(i));
            }
        }

        return faction;
    }

}

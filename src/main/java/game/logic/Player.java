package game.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.abilities.AbilityList;
import game.abilities.Block;
import game.abilities.Bodyguard;
import game.abilities.Bomb;
import game.abilities.BreadAbility;
import game.abilities.Bulletproof;
import game.abilities.Burn;
import game.abilities.CultLeader;
import game.abilities.Cultist;
import game.abilities.Disguiser;
import game.abilities.Doctor;
import game.abilities.Driver;
import game.abilities.DrugDealer;
import game.abilities.ElectroManiac;
import game.abilities.FactionSend;
import game.abilities.GameAbility;
import game.abilities.Ghost;
import game.abilities.Goon;
import game.abilities.Gunsmith;
import game.abilities.Interceptor;
import game.abilities.JailCreate;
import game.abilities.Janitor;
import game.abilities.Jester;
import game.abilities.Joker;
import game.abilities.Marshall;
import game.abilities.MasonLeader;
import game.abilities.MassMurderer;
import game.abilities.Miller;
import game.abilities.Poisoner;
import game.abilities.Puppet;
import game.abilities.SerialKiller;
import game.abilities.Survivor;
import game.abilities.Ventriloquist;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.abilities.Visit;
import game.abilities.support.Bounty;
import game.abilities.support.BountyList;
import game.abilities.support.Bread;
import game.abilities.support.Charge;
import game.abilities.support.CultInvitation;
import game.abilities.support.ElectrocutionException;
import game.abilities.support.Suit;
import game.abilities.support.Vest;
import game.abilities.util.AbilityUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.ArchitectChat;
import game.event.ChatMessage;
import game.event.EventList;
import game.event.EventLog;
import game.event.FactionChat;
import game.event.Feedback;
import game.event.JailChat;
import game.event.Message;
import game.event.OGIMessage;
import game.event.RoleCreatedChat;
import game.event.SelectionMessage;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.VotingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.RolePackage;
import game.logic.support.Shuffler;
import game.logic.support.Skipper;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.attacks.Attack;
import game.logic.support.saves.Heal;
import json.JSONObject;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.GameFactions;
import models.PendingRole;
import models.Role;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.PlayerStatus;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.idtypes.PlayerDBID;
import models.modifiers.Modifiers;
import services.NamingService;
import services.RoleChangeService;
import util.Util;

public class Player implements Controller {

    public Game game;
    private HashMap<PlayerStatus, Object> statusii;
    public PlayerDBID databaseID;

    // public because command handler will use this to make 'fake players' that
    // aren't added to the narrator object
    public Player(String name, Game game) {
        this.name = name;
        this.databaseID = new PlayerDBID(Math.abs(name.hashCode()));
        this.id = name;
        this.game = game;
        actions = new ActionList(this);
        rPrefers = new HashSet<>();
        tPrefers = new HashSet<>();
        jailChats = new HashMap<>();
        archChats = new HashMap<>();
        lastWillTrustees = new HashMap<>();
        chats = new ArrayList<>();

        visits = new PlayerList();
        visitors = new PlayerList();
        roleVisitors = new ArrayList<>();

        puppets = new PlayerList();
        masters = new PlayerList();

        secondaryCauses = new PlayerList();
        statusii = new HashMap<>();
    }

    public void sendMessage(Message message) {
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onMessageReceive(this, message);
    }

    public void warn(Message message) {
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onWarningReceive(this, message);
    }

    @Override
    public final boolean is(AbilityType abilityType) {
        if(gameRole == null)
            return false;
        return gameRole.hasAbility(abilityType);
    }

    public boolean willBe(AbilityType... abilityTypes) {
        if(pendingRole == null)
            return false;
        for(AbilityType abilityType: abilityTypes)
            for(Ability a: pendingRole.factionRole.role.getAbilities())
                if(a.type.equals(abilityType))
                    return true;
        return false;
    }

    public GameFaction getGameFaction() {
        // for pregame getting faction
        // TODO refactor/remove
        if(!game.isStarted())
            return null;

        String color;
        if(this.pendingRole != null)
            color = this.pendingRole.factionRole.faction.getColor();
        else
            color = this.gameRole.factionRole.getColor();
        return this.game.getFaction(color);
    }

    public boolean isOnTeam(String color) {
        for(GameFaction t: getFactions()){
            if(t.getColor().equals(color))
                return true;
        }
        return false;
    }

    public GameRole gameRole;
    public RolePackage initialAssignedRole;

    public String getInitialColor() {
        return initialAssignedRole.assignedRole.getColor();
    }

    public void addAbility(AbilityType abilityType) {
        GameAbility gameAbility = AbilityUtil.CREATOR(abilityType, game, new Modifiers<>());
        gameAbility.initialize(this);

        gameRole._abilities.add(gameAbility);
    }

    public void replaceAbility(GameAbility oldR, GameAbility newR) {
        newR.initialize(this);
        this.gameRole._abilities = this.gameRole._abilities.replace(oldR, newR);
    }

    public void setInitialRole(RolePackage rt) {
        initialAssignedRole = rt;
        setRole(rt.assignedRole);
    }

    // you really probably want changeRole
    public void setRole(FactionRole factionRole) {
        GameRole gameFactionRole = new GameRole(this.game, factionRole);
        this.gameRole = gameFactionRole;

        GameRole.initialize(this.gameRole, this);
    }

    public PendingRole pendingRole;

    public void changeRole(PendingRole factionRole) {
        this.pendingRole = factionRole;
        if(!game.isNight())
            RoleChangeService.resolveRoleChange(this);
    }

    public AbilityList getRoleAbilities() {
        return gameRole._abilities;
    }

    public Set<GameAbility> getAbilities() {
        Set<GameAbility> gameAbilities = new LinkedHashSet<>();
        for(GameAbility gameAbility: getRoleAbilities())
            gameAbilities.add(gameAbility);
        for(GameFaction gameFaction: this.getFactions()){
            for(GameAbility gameAbility: gameFaction.getAbilities()){
                gameAbilities.add(gameAbility);
            }
        }
        return gameAbilities;
    }

    public ArrayList<GameAbility> tempAbilities;

    public GameAbility addTempAbility(AbilityType abilityType) {
        GameAbility gameAbility = AbilityUtil.CREATOR(abilityType, this.game, new Modifiers<>());
        return addTempAbility(gameAbility);
    }

    public GameAbility addTempAbility(GameAbility gameAbility) {
        if(tempAbilities == null)
            tempAbilities = new ArrayList<>();
        tempAbilities.add(gameAbility);
        return gameAbility;
    }

    /*
     * public Ability getRole(){ return currentRole; }
     */
    public String getDescription() {
        return getDescription(name, gameRole);
    }

    public String getDescription(String nameToUse, GameRole prevRole) {
        boolean isInProgress = game.isInProgress();
        if(isAlive() && isInProgress || !game.isStarted())
            return nameToUse;
        if(Marshall.isCurrentMarshallLynch(this))
            return nameToUse;
        String roleName;
        if(isInProgress && this.getDeathType().isHidden())
            roleName = Constants.JANITOR_DESCRIP;
        else if(hasSuits())
            roleName = suits.get(0).factionRole.getName();
        else
            roleName = prevRole.factionRole.getName();
        return nameToUse + "(" + roleName + ")";
    }

    public Optional<FactionRole> getGraveyardFactionRole() {
        boolean isInProgress = game.isInProgress();
        if(!game.isStarted())
            return Optional.empty();
        if(!isInProgress)
            return Optional.of(gameRole.factionRole);

        if(this.getDeathType().isHidden() && isInProgress)
            return Optional.empty();
        else if(hasSuits())
            return Optional.of(suits.get(0).factionRole);
        else if(is(Miller.abilityType) && game.getBool(SetupModifierName.MILLER_SUITED))
            return Optional.of(Miller.getRoleFlip(this));
        else
            return Optional.of(gameRole.factionRole);
    }

    public Optional<String> getGraveyardRoleName() {
        Optional<FactionRole> factionRole = getGraveyardFactionRole();
        if(factionRole.isPresent())
            return Optional.of(factionRole.get().getName());
        return Optional.empty();
    }

    public Optional<String> getGraveyardTeamName() {
        Optional<FactionRole> factionRole = getGraveyardFactionRole();
        if(factionRole.isPresent())
            return Optional.of(factionRole.get().faction.name);
        return Optional.empty();
    }

    public Optional<String> getGraveyardColor() {
        Optional<FactionRole> factionRole = getGraveyardFactionRole();
        if(factionRole.isPresent())
            return Optional.of(factionRole.get().faction.color);
        return Optional.empty();
    }

    @Override
    public String getRoleName() {
        if(gameRole != null)
            return gameRole.factionRole.getName();
        return "";
    }

    @Override
    public String getColor() {
        return this.gameRole.factionRole.getColor();
    }

    // returns null if its a toggle ability
    public PlayerList getAcceptableTargets(AbilityType abilityType) {
        if(abilityType == CultInvitation.abilityType)
            return null;

        GameAbility ability = getAbility(abilityType);
        if(ability == null)
            return new PlayerList();
        return ability.getAcceptableTargets(this);
    }

    public Action action(Player target, AbilityType abilityType) {
        return new Action(this, abilityType, target);
    }

    public Action action(AbilityType command) {
        return new Action(this, command, null, null, new PlayerList());
    }

    public Action action(Player target) {
        return action(target, gameRole._abilities.get(0).getAbilityType());
    }

    public boolean isAcceptableTarget(Action a) {
        if(hasAbility(a.abilityType))
            return getAbility(a.abilityType).isAcceptableTarget(a);
        return false;
    }

    public int nightVotePower(PlayerList teamMembers) {
        int nvp = 0;
        for(GameAbility a: gameRole._abilities)
            nvp = Math.max(nvp, a.nightVotePower(this, teamMembers));
        return nvp;
    }

    public static String getName(Player p, Player target) {
        if(p.equals(target))
            return "You";
        return target.getName();
    }

    @Override
    public ArrayList<AbilityType> getCommands() {
        ArrayList<AbilityType> commands = new ArrayList<>();
        for(GameAbility a: getActionableAbilities()){
            if(a.getAbilityType().command != null)
                commands.add(a.getAbilityType());
        }
        return commands;
    }

    public ArrayList<GameAbility> getActionableAbilities() {
        ArrayList<GameAbility> list = new ArrayList<>();

        if(isJailed())
            return list;
        for(GameAbility a: gameRole._abilities)
            if(a.isAvailableAbility(this))
                list.add(a);

        GameFactions t = getFactions();
        boolean alreadyHas;
        for(GameFaction team: t){
            if(!team.hasSharedAbilities())
                continue;
            for(GameAbility s: team.getAbilities()){
                if(!s.isAvailableAbility(this))
                    continue;
                if(s.is(FactionSend.abilityType) && t.size() == 1)
                    continue;
                alreadyHas = false;
                for(GameAbility acceptableAbilities: list)
                    if(acceptableAbilities.getAbilityType() == s.getAbilityType()){
                        alreadyHas = true;
                        break;
                    }
                // team abilities must belong to the player to be available, or this is
                // 'potentially' available.
                if(game.nightPhase != GamePhase.NIGHT_ACTION_SUBMISSION
                        && team.getCurrentController(s.getAbilityType()) != this
                        && team.getCurrentController(s.getAbilityType()) != null)
                    continue;
                if(!alreadyHas)
                    list.add(s);
            }
        }
        return list;
    }

    public HashMap<AbilityType, ArrayList<Option>> getOptions() {
        HashMap<AbilityType, ArrayList<Option>> map = new HashMap<>();
        for(GameAbility a: gameRole._abilities)
            map.put(a.getAbilityType(), a.getOptions(this));
        return map;
    }

    @Deprecated
    public ArrayList<Option> getOptions(AbilityType abilityType) {
        return getAbility(abilityType).getOptions(this);
    }

    public HashMap<Option, ArrayList<Option>> getOptions2(AbilityType abilityType) {
        HashMap<Option, ArrayList<Option>> map = new HashMap<>();
        GameAbility a = getAbility(abilityType);
        for(Option s: a.getOptions(this)){
            map.put(s, a.getOptions2(this, s.getValue()));
        }

        return map;
    }

    public GameFactions getFactions() {
        Set<GameFaction> list = new HashSet<>();
        for(GameFaction t: game.getFactions()){
            if(t.hasMember(this))
                list.add(t);
        }
        if(isDead() && list.isEmpty())
            list.add(this.getGameFaction());

        return new GameFactions(list);
    }

    private HashMap<Integer, ActionList> prevNightTargets = new HashMap<>();
    private HashMap<Integer, ActionList> prevDayTargets = new HashMap<>();

    public ActionList getPrevDayTarget() {
        return prevDayTargets.get(game.getDayNumber() - 1);
    }

    public ActionList getPrevNightTarget() {
        return prevNightTargets.get(game.getDayNumber() - 1);
    }

    public ActionList getPrevNightTarget(int day) {
        return prevNightTargets.get(day);
    }

    public void saveNightTargets() {
        actions.cleanup();
        prevNightTargets.put(game.getDayNumber(), actions);
    }

    public void saveDayTargets() {
        prevDayTargets.put(game.getDayNumber(), actions);
    }

    private int submissionTime;

    public void targetingPreconditionChecks(Action a) {
        if(isDead() && (!is(Ghost.abilityType)))
            throw new PlayerTargetingException("Cannot set targets if dead");
        if(!game.isStarted())
            throw new PhaseException("Can't submit night actions if game isn't started");
        GameAbility ability = a.getAbility();
        if(ability == null)
            throw new PlayerTargetingException("You do not have this ability");

        if(!game.canDoNightAction() && a.isNightAction() && !a.isDayAction())
            throw new PhaseException("Night actions are not allowed right now!");
        if(a.getTargets() == null)
            throw new PlayerTargetingException("Cannot target nobody");
        if(game.isDay() && !a.isDayAction())
            throw new PlayerTargetingException("Cannot set targets during the day");
        if(game.endedNight(this))
            throw new PlayerTargetingException(
                    getName() + " has already ended the night. Cancel end night to change abilities.");

        if(isJailed())
            throw new PlayerTargetingException("Cannot submit actions while in jail");

        ability.checkAcceptableTarget(a);
    }

    private static final boolean CHECK = true;

    @Override
    public void setNightTarget(Action a) {
        setTarget(CHECK, a);
    }

    private boolean setTarget(boolean checkAction, Action a) {
        // untargets previous person without notificaition, because a previous command
        // was registered, and it needs to be canceled
        if(checkAction)
            targetingPreconditionChecks(a);

        SelectionMessage sm = addAction(a);// takes care of popping off abilities as needed

        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onTargetSelection(this, sm);
        return true;
    }

    private SelectionMessage addAction(Action action) {

        HashMap<GameFaction, Player> before;
        if(action.isTeamAbility())
            before = action.getFactions().getAbilityControllers(action.abilityType);
        else
            before = null;

        SelectionMessage e = new SelectionMessage(this, true);
        Action newAction = Action.pushCommand(e, action, actions)[0];// 0 IS THE NEW ACTION

        if(!nightIsEnding())
            newAction.pushCommand();

        e.pushOut();
        teamWarnChange(action, newAction, e, before);
        warnAbilityFail(newAction);

        return e;
    }

    private void teamWarnChange(Action action, Action newAction, SelectionMessage e,
            HashMap<GameFaction, Player> before) {
        HashMap<GameFaction, Player> after;
        if(!action.isTeamAbility())
            return;
        if(!game.setup.hasFactionRolesThatAffectSending(game))
            return;
        // if(getActions().isTargeting(this, Team.SEND_))
        // return;

        Set<GameFaction> changedDesignations = new HashSet<>(before.keySet());
        if(changedDesignations.isEmpty())
            return;

        e = new SelectionMessage(this, true);

        Action sendAction = action(this, FactionSend.abilityType);
        if(!newAction.is(FactionSend.abilityType))
            actions.addAction(sendAction);
        e.pushOut();

        after = action.getFactions().getAbilityControllers(action.abilityType);
        for(GameFaction t: changedDesignations)
            GameFaction.warnAbilityOwnerChange(this, this, before.get(t), after.get(t), t);
    }

    private void warnAbilityFail(Action newAction) {
        if(!this.in(newAction._targets))
            return;

        GameAbility a = newAction.getAbility();
        if(!a.isSelfTargetModifiable())
            return;

        String warningMessage = "Your attempt to use the " + a.getClass().getSimpleName()
                + " ability on yourself will fail unless somoene manipulates your action.";
        this.warn(new OGIMessage().add(warningMessage));
    }

    private boolean nightIsEnding() {
        int liveAndGhost = game.getLiveSize();
        liveAndGhost += game.players.filter(Ghost.abilityType).getDeadPlayers().size();
        return game.isNight() && game.getEndedNightPeople().size() == liveAndGhost;
    }

    public boolean alliesWith(Player p) {
        return getGameFaction().getMembers().contains(p);
    }

    public PlayerList getTargets(AbilityType abilityType) {
        return actions.getTargets(abilityType);
    }

    public Player getHouseTarget() {
        return house;
    }

    public void cancelTarget(AbilityType abilityType, double timeLeft) {
        PlayerList target = actions.getTargets(abilityType);
        if(target != null)
            cancelTarget(target, abilityType, timeLeft, true);
    }

    public void cancelTarget(PlayerList target, AbilityType abilityType, double timeLeft, boolean setCommand) {
        Action removedAction = actions.remove(target, abilityType, timeLeft, setCommand);
        if(removedAction != null){
            List<NarratorListener> listeners = game.getListeners();
            for(NarratorListener listener: listeners)
                listener.onTargetRemove(this, removedAction.abilityType, target);
        }

    }

    // this is public facing. do not use internally
    @Override
    public void clearTargets() {

        HashMap<AbilityType, HashMap<GameFaction, Player>> action = new HashMap<>();
        GameFactions playerFactions = getFactions();
        Set<GameAbility> factionAbilities = playerFactions.getAbilities();
        for(GameAbility a: factionAbilities)
            action.put(a.getAbilityType(), playerFactions.getAbilityControllers(a.getAbilityType()));

        actions.clear();

        Player controller;
        for(GameAbility a: factionAbilities){
            for(GameFaction t: action.get(a.getAbilityType()).keySet()){
                controller = t.getCurrentController(a.getAbilityType());
                GameFaction.warnAbilityOwnerChange(this, null, action.get(a.getAbilityType()).get(t), controller,
                        getGameFaction());
            }
        }
    }

    private Player house = this;

    public void setHouse(Player p) {
        house = p;
        // if(!busDriven){
        new Feedback(this, Driver.FEEDBACK).setPicture("driver").addExtraInfo(
                "Anyone who attempted to target you probably ended up targeting someone else, but also vice versa.");

        statusii.put(PlayerStatus.BUS_DRIVEN, true);
        // }
    }

    public void setHide(Player p) {
        house = p;
    }

    public void setAddress(Player p) {
        house = p;
    }

    public Player getAddress() {
        return house;
    }

    public boolean busDriven() {
        return statusii.containsKey(PlayerStatus.BUS_DRIVEN);
    }

    public boolean isInTown() {
        return house != null;
    }

    public boolean hasSuccessfulBounty() {
        return statusii.containsKey(PlayerStatus.SUCCESSFUL_BOUNTY);
    }

    public Player getSuccessfulBounties() {
        return (Player) statusii.get(PlayerStatus.SUCCESSFUL_BOUNTY);
    }

    public boolean isBountied() {
        return statusii.containsKey(PlayerStatus.JOKER_PLAYER_STATUS_KEY) && !getBounties().isEmpty();
    }

    public Bounty setBounty(Player joker) {
        BountyList bounties;
        if(isBountied()){
            bounties = getBounties();
        }else{
            bounties = new BountyList(this);
            statusii.put(PlayerStatus.JOKER_PLAYER_STATUS_KEY, bounties);
        }
        return bounties.add(joker, game.getDayNumber());
    }

    public BountyList getBounties() {
        return (BountyList) statusii.get(PlayerStatus.JOKER_PLAYER_STATUS_KEY);
    }

    private boolean completedNightAction = false;

    private ActionList actions;

    public ActionList getActions() {
        return actions;
    }

    public Action getAction(AbilityType abilityType) {
        for(Action action: actions){
            if(action.is(abilityType))
                return action;
        }
        return null;
    }

    public boolean isSubmitting(AbilityType... abilityType) {
        for(Action a: this.actions){
            if(a.is(abilityType))
                return true;
        }
        return false;
    }

    public GameAbility getFirstAbility() {
        return gameRole._abilities.get(0);
    }

    // this is necessary because some abilities don't have a command. the passives
    @SuppressWarnings("unchecked")
    public <T> T getAbility(Class<T> ability) {
        for(GameAbility abl: gameRole._abilities){
            if(abl.getClass().equals(ability))
                return (T) abl;
        }
        for(GameFaction faction: getFactions()){
            for(GameAbility factionAbility: faction.getAbilities()){
                if(factionAbility.getClass().equals(ability))
                    return (T) factionAbility;
            }
        }
        return null;
    }

    public GameAbility getAbility(AbilityType abilityType) {
        for(GameAbility a: gameRole._abilities){
            if(a.getAbilityType() == abilityType)
                return a;
        }
        for(GameFaction t: getFactions()){
            if(!t.hasSharedAbilities())
                continue;
            for(GameAbility a: t.getAbilities()){
                if(a.getAbilityType() == abilityType)
                    return a;
            }
        }
        return null;
    }

    public boolean hasAbility(AbilityType abilityType) {
        return getAbility(abilityType) != null;
    }

    public void setSubmissionTime(int time) {
        submissionTime = time;
    }

    protected boolean didNightAction() {
        return completedNightAction;
    }

    protected void setNightActionComplete() {
        completedNightAction = true;
    }

    public Skipper getSkipper() {
        return game.skipper;
    }

    public boolean stopsParity() {
        for(GameAbility a: gameRole._abilities){
            if(a.stopsParity(this))
                return true;
        }
        return false;
    }

    // player name
    private String name = "";
    private String id = "";

    @Override
    public String getName() {
        if(gameRole != null && gameRole._abilities != null && name.length() == 0){
            String t = getRoleName() + "-";
            if(getGameFaction() != null)
                t += getGameFaction().getName();
            return t;
        }
        return name;
    }

    @Override
    public Player setName(String name) {
        NamingService.playerNameCheck(name, game);
        this.name = name;
        if(!game.isStarted()){
            this.id = name;
            this.databaseID = new PlayerDBID(Math.abs(name.hashCode()));
        }
        return this;
    }

    // getStartingName
    public String getID() {
        return id;
    }

    public void ventCheck(Player controller) {
        if(!masters.isEmpty()){
            if(controller == null)
                throw new VotingException("You cannot vote today!");
            if(masters.getFirst() != controller){
                throw new VotingException("You cannot control this person's vote.");
            }
        }else if(masters.isEmpty() && controller != null){
            throw new VotingException("You cannot control this person's vote.");
        }
    }

    // deprecated

    public int getPunchRating() {
        return getGameFaction().getPunchSuccess();
    }

    public static final String END_NIGHT_TEXT = "You have moved to end the night";
    public static final String CANCEL_END_NIGHT_TEXT = "You have canceled your motion to end the night";

    @Override
    public void endNight(double timeLeft) {
        endNight(false, timeLeft);
    }

    protected void endNight(boolean forced, double timeLeft) {
        if(this == game.skipper)
            return;
        if(!game.canDoNightAction())
            throw new PlayerTargetingException(getDescription() + " cannot end night.  It is daytime!");

        if(isEliminated())
            throw new PlayerTargetingException(getDescription() + " cannot end night. This person is dead");

        if(endedNight())
            throw new IllegalActionException();

        synchronized (game){
            SelectionMessage e = new SelectionMessage(this, false);
            game.getEventManager().addCommand(this, timeLeft, CommandHandler.END_NIGHT);
            if(!forced)
                e.add(END_NIGHT_TEXT + ".");
            e.dontShowPrivate();
            e.pushOut();

            // has to be after
            game.endNight(this, forced);
        }
    }

    @Override
    public void cancelEndNight(double timeLeft) {
        synchronized (game){
            SelectionMessage e = new SelectionMessage(this, false);
            game.getEventManager().addCommand(this, timeLeft, CommandHandler.END_NIGHT);
            e.setPrivate();
            e.dontShowPrivate();
            e.add(CANCEL_END_NIGHT_TEXT + ".");

            e.pushOut();

            game.cancelEndNight(this);
        }
    }

    public boolean endedNight() {
        return game.endedNight(this);
    }

    public Message modkill(double timeLeft) {
        return game.modkill(new PlayerList(this), timeLeft);
    }

    public PlayerList positiveVotePower = new PlayerList(this);
    public PlayerList owedVotePower = new PlayerList();

    public int getVotePower() {
        return positiveVotePower.size();
    }

    public void increaseVotePower(int newVotePower) {
        for(int i = 0; i < newVotePower; i++)
            positiveVotePower.add(this);
    }

    public void transferVotePower(Player receiver) {
        if(this.positiveVotePower.isEmpty()){
            this.owedVotePower.add(receiver);
            return;
        }
        Player transfer = this.positiveVotePower.removeLast();
        while (!receiver.owedVotePower.isEmpty()){
            receiver = receiver.owedVotePower.removeFirst();
        }
        receiver.positiveVotePower.add(transfer);
    }

    public void stealVotePower(Player killedTarget) {
        if(killedTarget == this)
            return;
        Player p;
        for(int i = 0; i < positiveVotePower.size();){
            p = positiveVotePower.get(i);
            if(p == this)
                i++;
            transferVotePower(p);
        }
        positiveVotePower.clear();
        for(Player positiveVote: killedTarget.positiveVotePower){
            positiveVotePower.add(positiveVote);
        }
        killedTarget.positiveVotePower.clear();
        for(Player player: game.getLivePlayers()){
            while (player.positiveVotePower.contains(killedTarget)){
                player.positiveVotePower.remove(killedTarget);
                player.positiveVotePower.add(this);
            }
        }
    }

    /*
     * role effects
     */
    private boolean isInvulnerable = false, blockable = true, isVesting = false;

    public boolean isInvulnerable() {
        return isInvulnerable;
    }

    public void setInvulnerable(boolean b) {
        isInvulnerable = b;
    }

    public void setVesting(boolean v) {
        isVesting = v;
    }

    public boolean isVesting() {
        return isVesting;
    }

    public boolean isBlockable() {
        return blockable;
    }

    public void setBlockable(boolean b) {
        blockable = b;
    }

    private boolean jesterVote = false;

    public void votedForJester(boolean b) {
        jesterVote = b;
    }

    public boolean getVotedForJester() {
        return jesterVote;
    }

    private PlayerList cleaned;

    public void setCleaned(Player p) {
        if(cleaned == null)
            cleaned = new PlayerList();
        cleaned.add(p);
    }

    public PlayerList getCleaners() {
        return cleaned;
    }

    public String disguisedPrevName = null;

    public void setDisfranchised() {
        statusii.put(PlayerStatus.DISFRANCHISED, true);
    }

    public void setSilenced() {
        statusii.put(PlayerStatus.SILENCED, true);
    }

    public boolean isSilenced() {
        return statusii.containsKey(PlayerStatus.SILENCED);
    }

    public boolean isDisenfranchised() {
        return statusii.containsKey(PlayerStatus.DISFRANCHISED);
    }

    public void setBlocked() {
        if(game.getBool(SetupModifierName.BLOCK_FEEDBACK))
            Block.FeedbackGenerator(new Feedback(this));

        statusii.put(PlayerStatus.BLOCKED, true);
    }

    public boolean isBlocked() {
        return statusii.containsKey(PlayerStatus.BLOCKED);
    }

    private FactionRole frameStatus;

    public void setFramed(FactionRole m) {
        this.frameStatus = m;
    }

    public FactionRole getFrameStatus() {
        return frameStatus;
    }

    public int getLives() {
        if(deathType != null)
            return -1;
        for(Attack injury: injuries){
            if(!injury.isCountered() && !injury.isImmune() && injury.getType() != Constants.POISON_KILL_FLAG)
                return -1;
        }
        return 1;
    }

    // used for executioner
    public void clearHeals() {
        healList.clear();
    }

    private ArrayList<Heal> healList = new ArrayList<>();

    public void heal(Heal heal) {

        boolean successfulHeal = false;
        for(Attack a: injuries){
            if(a.isHealable(heal) && !a.isCountered()){
                a.counterAttack(heal);
                successfulHeal = true;
                break;
            }
        }

        if(!successfulHeal){
            healList.add(heal);
        }else if(getLives() >= 0){
            for(Action a: game.actionStack){
                if(a.owner == this && !a.isCompleted() && getLives() >= 0)
                    a.attemptCompletion();
            }
        }
    }

    /*
     * the attackTypeList keeps track of who attacked the person each night the
     * attackedBy list keeps track of who attacked this person the attack list keeps
     * track of who this person attacked
     */
    private ArrayList<Attack> injuries = new ArrayList<>();
    private ArrayList<Attack> assaults = new ArrayList<>();

    private boolean shouldGetImmuneFeedback() {
        if(isVesting())
            return true;
        if(is(Veteran.abilityType) && prevInvulnerability != null)
            return true;

        if(is(Bulletproof.abilityType))
            return !getAbility(Bulletproof.abilityType).isHiddenPassive();

        return false;
    }

    public void kill(Attack attack) {
        injuries.add(attack);
        if(!healList.isEmpty()){
            Heal counter = null;
            for(Heal h: healList){
                if(attack.isHealable(h)){
                    counter = h;
                    break;
                }
            }
            if(counter != null){
                healList.remove(counter);
                attack.counterAttack(counter);
                return;
            }
        }
    }

    protected Player isCharged;

    public boolean isCharged() {
        return isCharged != null;
    }

    public void setCharged(Player charger) {
        isCharged = charger;
    }

    public Player getCharger() {
        return isCharged;
    }

    private Player poisoned;

    public void setPoisoned(Player poisoner) {
        if(poisoner == this || !isInvulnerable())
            poisoned = poisoner;

        Poisoner.FeedbackGenerator(new Feedback(this), poisoner != this && isInvulnerable());
    }

    // used by bg and mm
    public ArrayList<Attack> getInjuries() {
        return injuries;
    }

    public ArrayList<Attack> getAssaults() {
        return assaults;
    }

    private DeathType deathType;

    public DeathType getDeathType() {
        return deathType;
    }

    private void setDead(String[] flag, Player cause) {
        setDead(cause);
        deathType.addDeath(flag);

        if(gameRole.modifiers.getBoolean(RoleModifierName.HIDDEN_FLIP))
            deathType.setCleaned();
    }

    public void setLynchDeath(PlayerList lynchers) {
        setDead(Constants.LYNCH_FLAG, lynchers.getLast());
        deathType.setLynchers(lynchers);

        Janitor.SendOutFeedback(this);
    }

    public void dayKill(Attack a, double timeLeft, boolean day_ending) {
        setDead(a.getCause());
        deathType.addDeath(a.getType());

        Janitor.SendOutFeedback(this);

        if(!day_ending){
            game.checkVote(timeLeft);
            game.checkProgress();
        }
    }

    public void modKillHelper() {
        setDead(Constants.MODKILL_FLAG, null);
    }

    private boolean exhumed;

    public void setExhumed() {
        exhumed = true;
    }

    public boolean isExhumed() {
        return exhumed;
    }

    private HashMap<Integer, ArrayList<JailChat>> jailChats;
    private HashMap<Integer, ArrayList<ArchitectChat>> archChats;

    public void setJailed(JailChat jc) {
        ArrayList<JailChat> jChats = jailChats.get(game.getDayNumber());
        if(jChats == null){
            jChats = new ArrayList<>();
            jailChats.put(game.getDayNumber(), jChats);
        }
        jChats.add(jc);
    }

    public boolean inArchChat() {
        return wasInArchChat(game.getDayNumber());
    }

    public boolean wasInArchChat(int dayNumber) {
        if(!game.getBool(SetupModifierName.ARCH_QUARANTINE))
            return false;
        ArrayList<ArchitectChat> aChats = archChats.get(dayNumber);
        if(aChats == null)
            return false;
        if(aChats.size() > 1)
            return false;
        return !aChats.get(0).getCreator().contains(this);
    }

    public boolean isJailed() {
        return wasJailed(game.getDayNumber());
    }

    public boolean wasJailed(int dayNumber) {
        ArrayList<JailChat> jChats = jailChats.get(dayNumber);
        if(jChats == null)
            return false;
        if(jChats.size() > 1)
            return false;
        return jChats.get(0).getCaptive() == this;
    }

    public boolean wasQuarantined(int dayNumber) {
        return wasJailed(dayNumber) || wasInArchChat(dayNumber) && game.getBool(SetupModifierName.ARCH_QUARANTINE);
    }

    public boolean isQuarintined() {
        return isJailed() || (inArchChat() && game.getBool(SetupModifierName.ARCH_QUARANTINE));
    }

    public void addArchChat(ArchitectChat ac) {
        ArrayList<ArchitectChat> aChats = archChats.get(game.getDayNumber());
        if(aChats == null){
            aChats = new ArrayList<>();
            archChats.put(game.getDayNumber(), aChats);
        }
        if(!aChats.contains(ac))
            aChats.add(ac);
    }

    public ArrayList<RoleCreatedChat> getRoleCreatedChats(int day) {
        ArrayList<RoleCreatedChat> list = new ArrayList<>();

        ArrayList<JailChat> jChats = jailChats.get(day);
        if(jChats != null)
            for(JailChat jChat: jChats){
                list.add(jChat);
            }
        ArrayList<ArchitectChat> aChats = archChats.get(day);
        if(aChats != null)
            for(ArchitectChat aChat: aChats){
                list.add(aChat);
            }
        return list;
    }

    public void onGameStart() {
        for(GameAbility a: gameRole._abilities)
            a.onGameStart(this);
    }

    protected void onDayStart() {
        if(prevInvulnerability != null){
            setInvulnerable(prevInvulnerability);
            prevInvulnerability = null;
        }

        tempAbilities = null;

        isUsingAutoVest = false;
        setVesting(false);

        cultAttempt = null;
        for(GameAbility ability: gameRole._abilities)
            ability.onDayStart(this);

        jesterVote = false;
        enforcer = null;
        feedback.clear();
        completedNightAction = false;
        this.owedVotePower.clear();
        submissionTime = Game.UNSUBMITTED;
        house = this;
        frameStatus = null;
        injuries.clear();
        assaults.clear();
        healList.clear();
        actions = new ActionList(this);
        disguisedPrevName = null;

        addChat(game.getEventManager().dayChat);
    }

    protected void onDayEnd() {
        if(ci != null)
            ci.pushOut();
        for(GameAbility a: gameRole._abilities){
            if(isAlive() || a.onDayEndTriggersWhileDead())
                a.onDayEnd(this);
        }
        saveDayTargets();
    }

    protected void onNightEnd() {
        ArrayList<GameAbility> teamAbilities = new ArrayList<>();
        for(GameFaction t: getFactions()){
            if(!t.hasSharedAbilities())
                continue;
            for(GameAbility a: t.getAbilities()){
                if(t.getFactionAbilityController(a.getAbilityType()) == this){
                    teamAbilities.add(a);
                }
            }
        }

        for(GameAbility a: getFactions().getAbilities()){
            if(teamAbilities.contains(a))
                continue;
            if(a instanceof FactionSend)
                continue;
            cancelTarget(a.getAbilityType(), Constants.NO_TIME_LEFT);
        }

        saveNightTargets();
        statusii.remove(PlayerStatus.DISFRANCHISED);
        statusii.remove(PlayerStatus.SILENCED);
        statusii.remove(PlayerStatus.BUS_DRIVEN);
        statusii.remove(PlayerStatus.BLOCKED);
        if(isAlive())
            for(GameAbility a: gameRole._abilities)
                a.onPlayerNightEnd(this);
        // i can try to move more of the narrator code into here, where it came from
    }

    protected void onNightStart() {
        // called on night start
        ci = null;
        visitors.clear();
        visits.clear();
        if(isAlive()){
            if(cleaned == null)
                cleaned = new PlayerList();
            cleaned.clear();
        }

        GameAbility ability;
        for(int i = 0; i < gameRole._abilities.size(); ++i){
            ability = gameRole._abilities.get(i);
            ability.onNightStart(this);
        }
        actions = new ActionList(this);// has to be second because jailor needs this actions

        masters.clear();
        puppets.clear();

        if(isAlive())
            secondaryCauses.clear();

        addChat(game.getEventManager().voidChat);

        if(getBounties() != null)
            getBounties().cleanse();

        if(isBountied() && getBounties().isEmpty())
            statusii.remove(PlayerStatus.JOKER_PLAYER_STATUS_KEY);

        if(!game.isFirstNight() && hasAbility(FactionSend.abilityType) && !isSilenced() && isAlive()){
            int dayNumber = game.getDayNumber() - 1;
            boolean sendSelf = false;
            while (dayNumber != -1){
                if(prevNightTargets.get(dayNumber) == null){
                    dayNumber--;
                    continue;
                }
                ArrayList<Action> sendActions = prevNightTargets.get(dayNumber).getActions(FactionSend.abilityType);
                if(sendActions.isEmpty()){
                    dayNumber--;
                    continue;
                }
                PlayerList oldSends = sendActions.get(0).getTargets();
                oldSends = oldSends.getLivePlayers();
                if(oldSends.isEmpty()){
                    dayNumber--;
                    continue;
                }
                if(oldSends.size() == 1 && oldSends.getFirst() == this){
                    sendSelf = true;
                    dayNumber--;
                    continue;
                }
                actions.addAction(new Action(this, FactionSend.abilityType, new LinkedList<>(), oldSends));
                return;
            }
            if(getFactions().getFactionMates().size() == 1)
                return;
            if(sendSelf)
                actions.addAction(new Action(this, FactionSend.abilityType, new LinkedList<>(), Player.list(this)));
        }
    }

    public ArrayList<Feedback> feedback = new ArrayList<>();

    public void addNightFeedback(Feedback s) {
        feedback.add(s);
    }

    private void setFakeCharges() {
        if(isSquelched())
            return;
        boolean hasBlockedFeedback = false;
        for(Feedback f: feedback){
            if(f.access(this).equals(Block.FEEDBACK)){
                hasBlockedFeedback = true;
                break;
            }
        }
        if(!hasBlockedFeedback)
            return;

        for(Action a: actions){
            if(a.getAbility() == null){
                continue;
            }else if(!a.isCompleted() || a.getAbility().getRealCharges() == Constants.UNLIMITED)
                continue;
            else if(a.isCompleted() && a.getAbility().isFakeBlockable())
                a.getAbility().addFakeCharge();
        }

        if(hasAbility(BreadAbility.abilityType)){
            getAbility(BreadAbility.class).setMarkedBreadToFake();
        }
    }

    public void sendFeedback() {
        // still alive
        boolean jailKilled = false;
        boolean onlyPoisoned = true; /// or no other attacks tbh
        for(Attack a: injuries){
            if(a.getType() == Constants.JAIL_KILL_FLAG)
                jailKilled = true;
            if(a.getType() != Constants.POISON_KILL_FLAG)
                onlyPoisoned = false;
        }
        boolean alive = (getLives() >= Game.NORMAL_HEALTH) || onlyPoisoned || (isUsingAutoVest() && !jailKilled);

        setFakeCharges();

        Message f;

        if(alive){
            // you were attacked and healed
            for(Attack a: injuries){
                if(a.isCountered() && !isVesting() && !isJailed())
                    determineHealFeedback(a.getHeal().getType());
                else if(shouldGetImmuneFeedback()){
                    new Feedback(this, Constants.NIGHT_IMMUNE_TARGET_FEEDBACK);
                }
            }
        }else{
            // you weren't
            String[] type;
            String killFeedback;
            for(Attack a: injuries){
                if(a.isCountered() || a.isImmune() || a.getType() == Constants.POISON_KILL_FLAG)
                    continue;
                type = a.getType();
                killFeedback = determineKillFeedback(type, game, a.getCause());
                f = new Feedback(this, killFeedback).setPicture("tombstone");
                if(a.getCause() != null && game.getBool(GameModifierName.OMNISCIENT_DEAD))
                    f.addExtraInfo(a.getCause().getName() + " caused your death.");
                deathType.addDeath(type);
            }
        }

        // dead
        Shuffler.shuffle(feedback, game.getRandom());
        Collections.sort(feedback);
        EventLog eLog = game.getEventManager().getDayChat();// hasn't quite changed yet
        // look at janitor pushing roles method if you change above

        boolean isSquelched = isSquelched();

        for(Feedback e: feedback){
            if(!isSquelched || !e.hideableFeedback){
                eLog.add(e);
                sendMessage(e);
            }
        }
        for(GameAbility a: gameRole._abilities){
            a.onNightFeedback(feedback);
        }
    }

    public static String determineKillFeedback(String[] role, Game n, Player killer) {
        if(role[0].startsWith(Constants.FACTION_KILL_FLAG[0])){
            if(!n.getBool(SetupModifierName.DIFFERENTIATED_FACTION_KILLS))
                return Goon.ANONYMOUS_DEATH_FEEDBACK;
            return Goon.DEATH_FEEDBACK + role[0].replaceAll(Constants.FACTION_KILL_FLAG[0], "") + ".";
        }

        if(role == Constants.SK_KILL_FLAG)
            return SerialKiller.DEATH_FEEDBACK;
        else if(role == Joker.BOUNTY_KILL_FLAG)
            return Joker.DEATH_FEEDBACK;

        else if(role == Constants.JESTER_KILL_FLAG)
            return Jester.DEATH_FEEDBACK;

        else if(role == Constants.VIGILANTE_KILL_FLAG)
            return Vigilante.DEATH_FEEDBACK;
        else if(role == Constants.GUN_KILL_FLAG)
            return Gunsmith.GetDeathFeedback(n);
        else if(role == Constants.GUN_SELF_KILL_FLAG)
            return Gunsmith.GetFaultyDeathFeedback(n);

        else if(role == Constants.VETERAN_KILL_FLAG)
            return Veteran.DEATH_FEEDBACK;
        else if(role == Constants.JAIL_KILL_FLAG)
            return JailCreate.DEATH_FEEDBACK;

        else if(role == Constants.BODYGUARD_KILL_FLAG)
            return Bodyguard.DEATH_TARGET_FEEDBACK;

        else if(role == Constants.MASS_MURDERER_FLAG){
            if(killer.is(Interceptor.abilityType))
                return Interceptor.DEATH_FEEDBACK;
            return MassMurderer.DEATH_FEEDBACK;
        }else if(role == Constants.MASONLEADER_KILL_FLAG)
            return MasonLeader.DEAD_FEEDBACK;
        else if(role == Constants.ARSON_KILL_FLAG)
            return Burn.DEATH_FEEDBACK;
        else if(role == Constants.ELECTRO_KILL_FLAG)
            return ElectroManiac.DEATH_FEEDBACK;
        else if(role == Constants.HIDDEN_KILL_FLAG)
            return Disguiser.DEATH_FEEDBACK;
        else if(role == Constants.BOMB_KILL_FLAG)
            return Bomb.DEATH_FEEDBACK;
        throw new IllegalArgumentException("The person cannot kill! " + role[0]);

    }

    private Message determineHealFeedback(String[] role) {
        if(role == Constants.DOCTOR_HEAL_FLAG){
            if(game.getBool(SetupModifierName.HEAL_FEEDBACK))
                return new Feedback(this, Doctor.TARGET_FEEDBACK).setPicture("doctor");
        }else if(role == Constants.BODYGUARD_KILL_FLAG)
            return new Feedback(this, Bodyguard.TARGET_FEEDBACK).setPicture("bodyguard");
        else
            throw new IllegalArgumentException(role[0] + ": cannot heal!");
        return null;
    }

    private PlayerList visits;

    protected void setVisits(PlayerList visits) {
        this.visits = visits;
    }

    protected void setVisitors(PlayerList visitors) {
        this.visitors = visitors;
    }

    public static final boolean FAKE_VISIT = true;
    private PlayerList visitors;

    public void visit(boolean fakeVisit, Player... targets) {
        PlayerList charged = new PlayerList();
        for(Player target: targets){
            charged.add(Interceptor.handleInterceptor(this, target));
            if(!in(target.visitors))
                target.visitors.add(this);
            if(!target.in(visits)){
                if(fakeVisit)
                    visits.add(0, target);
                else
                    visits.add(target);
            }

            if(isCharged() && !fakeVisit && target != this && !charged.contains(target)){
                if(target.isCharged() || target.is(ElectroManiac.abilityType))
                    charged.add(target);
            }
            if(!fakeVisit && game.getBool(SetupModifierName.CORONER_LEARNS_ROLES)){
                HTString roleHT = new HTString(this.gameRole.factionRole);
                if(!target.roleVisitors.contains(roleHT))
                    target.roleVisitors.add(roleHT);
            }
        }
        if(!charged.isEmpty())
            charged.add(this);

        ElectrocutionException.throwException(this, charged);
    }

    public void visit(Player... targets) {
        visit(false, targets); // not fake visit
    }

    public void visit(PlayerList targets) {
        for(Player p: targets)
            visit(false, p); // not fake visit
    }

    public PlayerList getVisitors(String why) {
        PlayerList list = PlayerList.clone(visitors);
        list.shuffle(game.getRandom(), why);
        return list;
    }

    public PlayerList getVisits(String why) {
        PlayerList list = PlayerList.clone(visits);
        list.shuffle(game.getRandom(), why);
        return list;
    }

    @Override
    public void doDayAction(Action action) {
        AbilityType abilityType = action.abilityType;

        if(!game.isDay())
            throw new PhaseException("Day actions can only be submitted during the day");
        if(!hasAbility(abilityType) && abilityType != CultInvitation.abilityType && abilityType != Puppet.abilityType)
            throw new PlayerTargetingException("You do not have this ability!");

        if(isPuppeted() && abilityType != Puppet.abilityType)
            throw new IllegalActionException("You cannot perform day actions while being controlled.");

        if(action.is(CultInvitation.abilityType)){
            if(ci == null)
                throw new PlayerTargetingException("Nothing to respond to");
            String option = action.getArg1();
            if(option == null)
                throw new PlayerTargetingException("Invitations need a response");
            if(option.equalsIgnoreCase(Constants.ACCEPT))
                ci.confirm(action.timeLeft);
            else if(option.equalsIgnoreCase(Constants.DECLINE)){
                ci.decline(action.timeLeft);
            }else
                throw new IllegalActionException("Unknown response to invitation");
            return;
        }

        GameAbility abilityAction = action.getAbility();

        if(abilityAction == null)
            throw new IllegalActionException(this + " does not have this day action!");
        if(!abilityAction.canUseDuringDay(this))
            throw new PhaseException("Can't do this right now");
        abilityAction.checkAcceptableTarget(action);
        ArrayList<String> commands = abilityAction.getCommandParts(action);
        game.getEventManager().addCommand(this, action.timeLeft, Util.toStringArray(commands));
        abilityAction.doDayAction(action, action);
    }

    private void cancelAction(Action canceled) {
        game.actionStack.remove(canceled);
        List<NarratorListener> listeners = game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onTargetRemove(this, canceled.abilityType, canceled.getTargets());
    }

    public void cancelDayAction(int actionNumber, double timeLeft) {
        int count = actionNumber;
        for(Action action: game.actionStack){
            if(action.owner != this)
                continue;
            if(count != 0){
                count--;
                continue;
            }
            game.actionStack.remove(action);
            action.cancel(actionNumber, timeLeft, true);
            return;
        }
        throw new NarratorException("No action found with that id.");
    }

    @Override
    public void cancelAction(int actionNumber, double timeLeft) {
        if(!game.isNight()){
            cancelDayAction(actionNumber, timeLeft);
            return;
        }
        Action a = actions.cancelActionID(actionNumber, timeLeft, true);

        HashMap<GameFaction, Player> before_set = getFactions().getAbilityControllers(a.abilityType);

        if(a.isTeamAbility() && game.setup.hasFactionRolesThatAffectSending(game)
                && actions.getTargets(a.abilityType).size() < 2){
            PlayerList sendTargets = actions.getTargets(FactionSend.abilityType);
            if(sendTargets.size() == 1 && this.in(sendTargets))
                actions.cancelActionID(actions.size() - 1, timeLeft, false);
            else if(this.in(sendTargets)){
                Action sendAction = getAction(FactionSend.abilityType);
                sendAction.setTargets(sendAction.getTargets().remove(this));
            }
        }

        cancelAction(a);

        Player before, after;
        for(GameFaction t: before_set.keySet()){
            before = before_set.get(t);
            after = t.determineNightActionController(a.abilityType);
            GameFaction.warnAbilityOwnerChange(this, null, before, after, getGameFaction());
        }
    }

    public void doNightAction(AbilityType abilityType) {
        for(Action a: getActions())
            if(a.abilityType == abilityType)
                a.doNightAction();
    }

    public ArrayList<GameAbility> getDayAbilities() {
        ArrayList<GameAbility> dayActions = new ArrayList<>();
        for(GameAbility a: gameRole._abilities){
            if(a.canUseDuringDay(this) && a.getPerceivedCharges() != 0 && !a.isOnCooldown())
                dayActions.add(a);
            else if(a.canUseDuringDay(this) && a.is(JailCreate.abilityType))
                dayActions.add(a);
        }

        if(ci != null)
            dayActions.add(ci);

        return dayActions;
    }

    public ArrayList<AbilityType> getDayCommands() {
        ArrayList<AbilityType> dCommands = new ArrayList<>();
        for(GameAbility a: getDayAbilities()){
            dCommands.add(a.getAbilityType());
        }
        return dCommands;
    }

    // unsure if this should be a 'do I have this ability in general' or 'can I use
    // this ability'
    // going with can i use this ability
    public boolean hasDayAction(AbilityType abilityType) {
        if(abilityType == Puppet.abilityType && hasAbility(abilityType))
            return getAbility(Puppet.abilityType).canUseDuringDay(this);
        if(isPuppeted())
            return false;
        if(hasAbility(abilityType))
            return getAbility(abilityType).canUseDuringDay(this);
        else if(abilityType == CultInvitation.abilityType)
            return ci != null;
        return false;

    }

    public boolean isAlive() {
        return deathType == null;
    }

    public boolean isDead() {
        return !isAlive();
    }

    public boolean isParticipating() {
        return !isEliminated();
    }

    public boolean isEliminated() {
        return isDead() && !hasAbility(Ghost.abilityType);
    }

    public int getDeathDay() {
        return deathType.getDeathDay();
    }

    private boolean winner = false;

    public boolean isWinner() {
        return winner;
    }

    protected void determineWin() {
        GameFaction t = getGameFaction();
        Set<GameFaction> enemies = t.getEnemies();

        if(enemies.isEmpty()){
            winner = true;
            Boolean winValue;
            boolean defaultToAlive = true;
            for(GameAbility a: gameRole._abilities){
                winValue = a.checkWinEnemyless(this);
                if(winValue == null)
                    continue;
                defaultToAlive = false;
                winner = winner && winValue;
            }
            if(defaultToAlive)
                winner = isAlive();
            return;
        }

        if(!isAlive() && t.getAliveToWin()){
            winner = false;
            return;
        }

        if(t.isAlive()){
            for(GameFaction enemyTeam: enemies){
                if(enemyTeam == t && t.size() == 1){

                }else if(enemyTeam.isAlive() && enemyTeam.winsOver(t)){
                    return;
                }
            }
            winner = true;
            return;
        }
    }

    public static final Comparator<Player> SubmissionTime = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            return p1.getSubmissionTime() - p2.getSubmissionTime();
        }

    };

    public static final Comparator<Player> NameSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            return p1.getName().toLowerCase().compareTo(p2.getName().toLowerCase());
        }
    };

    public static final Comparator<Player> WinDetermineSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            if(p1.is(Ghost.abilityType) && !p2.is(Ghost.abilityType))
                return 1;
            if(!p1.is(Ghost.abilityType) && p2.is(Ghost.abilityType))
                return -1;
            if(p1.is(Ghost.abilityType) && p2.is(Ghost.abilityType) && p1.isDead() && p2.isDead()){
                DeathType d1 = p1.getDeathType();
                DeathType d2 = p2.getDeathType();

                if(d1.getCause() == p2)
                    return 1;
                if(d2.getCause() == p1)
                    return -1;
            }
            return 0;
        }
    };

    public static final Comparator<Player> TeamSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            if(p1.getColor().equals(p2.getColor())){
                return p1.getName().compareTo(p2.getName());
            }
            return p1.getGameFaction().getName().compareTo(p2.getGameFaction().getName());
        }

    };

    public static final Comparator<Player> IDSort = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            return p1.getID().compareTo(p2.getID());
        }

    };

    public static final Comparator<Player> Deaths = new Comparator<Player>() {
        @Override
        public int compare(Player p1, Player p2) {
            if(p1.isAlive() && p2.isAlive())
                return p1.getName().compareTo(p2.getName());
            if(p1.isAlive())
                return -1;
            if(p2.isAlive())
                return 1;
            if(p1.getDeathDay() == p2.getDeathDay()){
                if(p1.getDeathType().isLynch() != p2.getDeathType().isLynch()){
                    if(p1.getDeathType().isLynch())
                        return -1;
                    if(p2.getDeathType().isLynch())
                        return 1;
                }
                return p1.getName().compareTo(p2.getName());
            }
            return p1.getDeathDay() - p2.getDeathDay();
        }

    };

    public int getSubmissionTime() {
        return submissionTime;
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o.getClass() != getClass())
            return false;
        Player p = (Player) o;

        if(notEqual(frameStatus, p.frameStatus))
            return false;
        if(cleaned != p.cleaned)
            return false;
        // if(notEqual(comm, p.comm))
        // return false;
        if(detectable != p.detectable)
            return false;
        if(doused != p.doused)
            return false;
        if(Util.notEqual(deathType, p.deathType))
            return false;
        // if(notEqual(feedback, p.feedback))
        // return false;
        if(notEqual(healList, p.healList))
            return false;
        if(jesterVote != p.jesterVote)
            return false;
        // if(isComputer() != p.isComputer)
        // return false;
        if(notEqual(name, p.name))
            return false;
        if(actions == null){
            if(p.actions != null)
                return false;
        }else{
            return actions.equals(p.actions);

        }

        if(notEqual(pendingRole, p.pendingRole))
            return false;
        if(notEqual(gameRole._abilities, p.gameRole._abilities))
            return false;
        if(winner != p.winner)
            return false;

        return true;
    }

    private static boolean notEqual(Object o, Object p) {
        return Util.notEqual(o, p);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        if(gameRole == null)
            return id;
        return id + "(" + getRoleName() + ")";
    }

    public static Player[] array(Player... target) {
        Player[] array = new Player[target.length];
        for(int i = 0; i < target.length; i++)
            array[i] = target[i];
        return array;
    }

    public static PlayerList list(Player... target) {
        PlayerList list = new PlayerList();
        for(Player p: target){
            list.add(p);
        }
        return list;
    }

    public static void print(ArrayList<PlayerList> perms) {
        for(PlayerList list: perms){
            for(Player p: list)
                System.out.print(p.getDescription() + " ");
            System.out.println();
        }
    }

    public static String cleanup(String message) {
        int lastComma = message.lastIndexOf(",");
        if(lastComma == -1)
            return message;
        return message.substring(0, lastComma);
    }

    public boolean isAtHome() {
        if(actions.isEmpty() || actions.getFirst().is(Survivor.abilityType))
            return true;
        return actions.visitedHome();
    }

    public DeathType setDead(Player cause) {
        deathType = new DeathType(this, game.isDay(), game.getDayNumber());
        deathType.setCause(cause);
        if(!is(Ghost.abilityType)){
            game.getEventManager().getDeadChat().addAccess(this);
            addChat(game.getEventManager().deadChat);
        }
        GameFaction t = getGameFaction();
        t.removeMember(this);
        if(t.hasLastWill() && _secretLastWill != null && !_secretLastWill.isEmpty() && t.isAlive()){
            String message = getName() + " left you a message:\n" + _secretLastWill;
            Feedback fb;
            for(Player p: t.getMembers()){
                fb = new Feedback(p, message);
                fb.hideableFeedback = false;
                if(game.isDay()){
                    p.sendMessage(fb);
                }
            }
        }
        if(game.isDay()){
            onDeath();
            game.players.sortByName();
        }
        // ventriloquist depends on currentRole 'onDeathing' first
        for(Player master: masters){
            master.puppets.remove(this);
        }
        for(Player puppet: puppets){
            puppet.masters.remove(this);
        }
        puppets.clear();
        actions.clear();
        return deathType;

    }

    protected void onDeath() {
        if(statusii.containsKey(PlayerStatus.JOKER_PLAYER_STATUS_KEY) && is(Ghost.abilityType)){
            BountyList bl = getBounties();
            int day = Integer.MAX_VALUE;

            PlayerList choices = new PlayerList();
            for(Bounty bounty: bl.list){
                if(bounty.rewarder.isAlive() && !bounty.failed() && day >= bounty.start_day){
                    if(day > bounty.start_day)
                        choices.clear();
                    else
                        day = bounty.start_day;
                    choices.add(bounty.rewarder);
                }
            }
            if(!choices.isEmpty())
                statusii.put(PlayerStatus.SUCCESSFUL_BOUNTY, game.getRandom().getPlayer(choices));
        }
        for(GameAbility a: gameRole._abilities)
            a.onDeath(this);
        if(game.isDay())
            game.masonLeaderPromotion();
        for(Player positiveVote: this.positiveVotePower){
            if(positiveVote == this)
                continue;
            positiveVote.positiveVotePower.add(positiveVote);
        }

    }

    // used for mason leader recruiting
    public boolean isPowerRole() {
        for(GameAbility a: gameRole._abilities)
            if(a.isPowerRole()) // change to isPowerAbility
                return true;
        return false;
    }

    public boolean hasActivePassive() {
        for(GameAbility a: gameRole._abilities)
            if(a.isActivePassive(this))
                return true;
        return false;
    }

    public PlayerList cultAttempt = null;

    private boolean doused = false;

    public void setDoused(boolean b) {
        doused = b;
        if(game.getBool(SetupModifierName.DOUSE_FEEDBACK)){
            Burn.FeedbackGenerator(new Feedback(this));
        }
    }

    public boolean isDoused() {
        return doused;
    }

    private boolean culted = false;

    public void setCulted() {
        culted = true;
    }

    public boolean isCulted() {
        return culted || is(CultLeader.abilityType) || is(Cultist.abilityType);
    }

    private Player enforcer = null;

    public boolean isEnforced() {
        return enforcer != null;
    }

    public void setEnforced(Player enforcer) {
        this.enforcer = enforcer;
    }

    public Player getEnforcer() {
        return enforcer;
    }

    private boolean detectable = true;

    public boolean isDetectable() {
        return detectable;
    }

    public void setDetectable(boolean b) {
        detectable = b;
    }

    private boolean isComputer = false;

    public boolean isComputer() {
        return isComputer;
    }

    public Player setComputer() {
        isComputer = true;
        return this;
    }

    @Override
    public ChatMessage say(String message, String key, Optional<String> source, JSONObject args) {
        return _say(message, key, source, args);
    }

    public ChatMessage _say(String message, String key, Optional<String> source, JSONObject args) {
        return game.say(this, message, key, source, args);
    }

    public EventList getEvents() {
        if(isEliminated() && (game.getBool(GameModifierName.OMNISCIENT_DEAD) || !game.isInProgress()))
            return game.getEventManager().getEvents(getID(), Message.PRIVATE);
        return game.getEventManager().getEvents(getID());
    }

    public boolean in(Collection<Player> pl) {
        if(pl == null)
            return false;
        return pl.contains(this);
    }

    public boolean isPoisoned() {
        return poisoned != null;
    }

    public Player getPoisoner() {
        return poisoned;
    }

    public Set<Role> rPrefers;
    public Set<Faction> tPrefers;

    @Override
    public void rolePrefer(Role role) {
        rPrefers.add(role);
    }

    public void teamPrefer(Faction faction) {
        tPrefers.add(faction);
    }

    public boolean hasPreferences() {
        return !rPrefers.isEmpty() || !tPrefers.isEmpty();
    }

    public void clearPreferences() {
        rPrefers.clear();
        tPrefers.clear();
    }

    public List<RolePackage> cleanPrefers(Map<String, Set<RolePackage>> generatedRoles,
            Iterable<RolePackage> generatedRoles2) {
        if(!hasPreferences())
            return new ArrayList<>();
        for(Role role: new HashSet<>(rPrefers))
            if(!generatedRoles.containsKey(role.getName()))
                rPrefers.remove(role);

        for(Faction factionPref: new HashSet<>(tPrefers))
            if(!generatedRoles.containsKey(factionPref.getColor()))
                tPrefers.remove(factionPref);

        // at this point, i'm only preferring possible roles and teams

        final Map<RolePackage, Integer> chosenPrefs = new HashMap<>();
        for(Role role: rPrefers){
            for(RolePackage rp: generatedRoles.get(role.getName())){
                if(chosenPrefs.containsKey(rp)){
                    int prevVal = chosenPrefs.remove(rp);
                    chosenPrefs.put(rp, prevVal + 1);
                }else
                    chosenPrefs.put(rp, 1);
            }
        }
        for(Faction team: tPrefers){
            for(RolePackage tp: generatedRoles.get(team.getColor())){
                if(chosenPrefs.containsKey(tp)){
                    int prevVal = chosenPrefs.remove(tp);
                    chosenPrefs.put(tp, prevVal + 1);
                }else
                    chosenPrefs.put(tp, 1);
            }
        }

        List<RolePackage> list = new ArrayList<>();
        for(RolePackage rolePackage: generatedRoles2)
            if(chosenPrefs.containsKey(rolePackage))
                list.add(rolePackage);
        list.sort(new Comparator<RolePackage>() {
            @Override
            public int compare(RolePackage o1, RolePackage o2) {
                return chosenPrefs.get(o2) - chosenPrefs.get(o1);
            }
        });
        return list;
    }

    public void addModBread(int breadToAdd, double timeLeft) {
        Bread bread;
        for(int i = 0; i < breadToAdd; i++){
            bread = new Bread(Charge.FROM_OTHER).initialize();
            this.addConsumableCharge(BreadAbility.abilityType, bread);
        }

        SelectionMessage e = new SelectionMessage(this, false);
        StringChoice you = new StringChoice(this);
        you.add(this, "You");
        e.add(you, " ");

        StringChoice scTarget = new StringChoice("has");
        scTarget.add(this, "have");

        e.add(scTarget, " received something : " + breadToAdd + " bread.");
        game.getEventManager().addCommand(timeLeft, Constants.MOD_ADDBREAD, "" + breadToAdd, this.getName());
        e.pushOut();

    }

    public boolean isSquelched() {
        for(Message m: feedback){
            if(m.toString().equals(DrugDealer.WIPE))
                return true;
        }
        return false;
    }

    public List<String> getRoleCardDetails() {
        List<String> specs = new LinkedList<>();
        for(GameAbility a: this.getActionableAbilities())
            specs.addAll(a.getProfileRoleCardDetails(this));

        specs.addAll(getRoleModifierDetails());

        return specs;
    }

    public List<String> getRoleModifierDetails() {
        List<String> specs = new LinkedList<>();
        if(!isDetectable())
            specs.add("You cannot be detected by investigative roles.");
        if(!isBlockable())
            specs.add("You cannot be blocked at night.");
        if(!isRecruitable() && game.setup.hasPossibleFactionRoleWithAbility(game, CultLeader.abilityType))
            specs.add("You cannot be converted.");

        if(game.getBool(SetupModifierName.PUNCH_ALLOWED))
            specs.add("Your chance of punching someone out is " + getPunchRating() + "%.");

        if(getPerceivedAutoVestCount() > 0)
            specs.add("You have " + getPerceivedAutoVestCount() + " auto " + Vest.getVestName(game)
                    + "(s) left for use.");
        return specs;
    }

    public CultInvitation ci;

    public void addCultInvitation(Player cultLeader) {
        ci = new CultInvitation(cultLeader, this);

    }

    public boolean hasInjuriesFromPreKillingPhase() {
        for(Attack a: injuries){
            if(a.isCountered())
                continue;
            if(a.isImmune())
                continue;
            if(a.sustainedBeforeAttackPhase)
                return true;
        }
        return false;
    }

    public ArrayList<Suit> suits;

    public void removeSuits(Player tailor) {
        if(suits == null || isDead())
            return;
        for(int i = 0; i < suits.size(); i++){
            if(suits.get(i).createdBy(tailor)){
                suits.remove(i);
                i--;
            }

        }
    }

    public void addSuite(Suit s) {
        if(suits == null)
            suits = new ArrayList<>();
        suits.add(0, s);
    }

    public boolean hasSuits() {
        return suits != null && !suits.isEmpty();
    }

    private ArrayList<EventLog> chats;

    public ArrayList<EventLog> getChats() {
        if(!game.isInProgress() && game.isStarted())
            return game.getEventManager().getAllChats();
        if(isEliminated())
            return game.getEventManager().getAllChats();

        ArrayList<EventLog> ret = new ArrayList<>();
        ret.addAll(chats);

        for(ArrayList<JailChat> jChats: this.jailChats.values()){
            for(JailChat jChat: jChats)
                ret.add(jChat);
        }

        for(ArrayList<ArchitectChat> aChats: this.archChats.values()){
            for(ArchitectChat aChat: aChats)
                ret.add(aChat);
        }

        return ret;
    }

    public void addChat(EventLog el) {
        if(el == null)
            throw new NullPointerException();
        if(!chats.contains(el))
            chats.add(el);
    }

    public void removeChat(EventLog el) {
        chats.remove(el);
    }

    @Override
    public ArrayList<String> getChatKeys() {
        ArrayList<String> chatKeys = new ArrayList<>();
        String key;
        for(EventLog el: getChats()){
            key = el.getKey(this);
            if(el instanceof FactionChat && el.getEvents().isEmpty()
                    && el.getMembers().filterUnquarantined().size() < 2)
                continue;
            if(!chatKeys.contains(key) && el.isActive())
                chatKeys.add(key);
        }
        return chatKeys;
    }

    public HashMap<Player, Boolean> lastWillTrustees;
    public String _lastWill;
    public String _secretLastWill;

    @Override
    public void setLastWill(String lastWill) {
        if(!game.isInProgress()){
            if(!game.isStarted())
                throw new PhaseException("Last wills cannot be set before game starts.");
        }
        if(!game.getBool(SetupModifierName.LAST_WILL)){
            throw new IllegalActionException();
        }
        if(isDead())
            throw new IllegalActionException();
        this._lastWill = lastWill;
    }

    @Override
    public void setTeamLastWill(String lastWill) {
        if(!game.isInProgress() && !game.isStarted())
            throw new PhaseException("Cannot set last wills before game starts.");

        if(!getGameFaction().hasLastWill())
            throw new IllegalActionException();

        if(isDead())
            throw new IllegalActionException();
        this._secretLastWill = lastWill;
    }

    // null requester for public last wills
    public String getLastWill(Player requester) {
        if(!game.getBool(SetupModifierName.LAST_WILL))
            return null;
        if(!this.getDeathType().isHidden())
            return _lastWill;
        if(lastWillTrustees.containsKey(requester) && lastWillTrustees.get(requester))
            return _lastWill;
        return null;
    }

    public void switchNames(Player target) {
        String temp = target.name;
        target.name = name;
        name = temp;
    }

    private PlayerList masters;
    private PlayerList puppets;

    public void addVentController(Player vent) {
        if(vent == this){
            Visit.NoNightActionVisit(vent, this);
            return;
        }
        GameAbility.happening(vent, " possessed ", this);
        if(masters.isEmpty())
            new Feedback(this, Ventriloquist.FEEDBACK).setPicture("blackmailer")
                    .addExtraInfo("You are now allowed to talk or vote today.");
        this.masters.add(vent);
        vent.puppets.add(this);
    }

    public boolean hasPuppets() {
        for(Player puppet: puppets){
            if(puppet.masters.getFirst() == this && (!puppet.isDisenfranchised() || !puppet.isSilenced()))
                return true;
        }
        return false;
    }

    public boolean isPuppeted() {
        return !masters.isEmpty();
    }

    public PlayerList getPuppets() {
        PlayerList puppets = new PlayerList();
        for(Player puppet: this.puppets){
            if(puppet.masters.getFirst() == this)
                puppets.add(puppet);
        }
        return puppets;
    }

    public Player getPuppeteer() {
        if(masters.isEmpty())
            return null;
        return masters.getFirst();
    }

    public boolean isRecruitable() {
        for(GameAbility a: gameRole._abilities){
            if(!a.isRecruitable())
                return false;
        }
        if(gameRole.modifiers == null)
            return true;
        return !gameRole.modifiers.getOrDefault(RoleModifierName.UNCONVERTABLE, false);
    }

    // used for ghost only, really.
    private PlayerList secondaryCauses;

    public void secondaryCause(Player cause) {
        secondaryCauses.add(cause);
    }

    public Player getCause() {
        return secondaryCauses.getLast();
    }

    public void addConsumableCharge(AbilityType abilityType, Charge charge) {
        if(!gameRole._abilities.contains(abilityType)){
            GameAbility gameAbility = AbilityUtil.CREATOR(abilityType, game, new Modifiers<>());
            gameAbility.initialize(this);
            gameAbility.setInitialCharges(0, false);
            gameRole._abilities.add(gameAbility);
        }
        getAbility(abilityType).charges.addCharge(charge);
    }

    public Boolean prevInvulnerability = null;

    private int fakeAutoVestCount;
    private int realAutoVestCount;
    private int hiddenAutoVestCount = 0;
    private boolean isUsingAutoVest = false;

    public void setAutoVestCount(int autoVs) {
        realAutoVestCount = autoVs;
    }

    public int getPerceivedAutoVestCount() {
        return realAutoVestCount + fakeAutoVestCount;
    }

    public int getRealAutoVestCount() {
        return realAutoVestCount + hiddenAutoVestCount;
    }

    public void silentUseAutoVest() {
        if(realAutoVestCount > 0){
            realAutoVestCount--;
            fakeAutoVestCount++;
        }
        isUsingAutoVest = true;
    }

    public void useAutoVest() {
        if(realAutoVestCount > 0){
            realAutoVestCount--;
        }else if(hiddenAutoVestCount > 0){
            hiddenAutoVestCount--;
        }else{
            return;
        }

        isUsingAutoVest = true;
    }

    public boolean isUsingAutoVest() {
        return isUsingAutoVest;
    }

    private ArrayList<HTString> roleVisitors = new ArrayList<>();

    public ArrayList<HTString> getRoleVisits() {
        return roleVisitors;
    }

    public void resolveFakeBreadActions() {
        for(GameAbility a: gameRole._abilities)
            a.resolveFakeBreadedDayActions();
    }

    @Override
    public void log(String string) {
    }

    @Override
    public boolean isTargeting(AbilityType abilityType, Controller... target_c) {
        PlayerList targets = ControllerList.list(target_c).toPlayerList(game);

        return getActions().isTargeting(targets, abilityType);
    }

    @Override
    public Player getPlayer() {
        if(this == this.game.skipper)
            return game.skipper;
        return game.getPlayerByID(this.id);
    }

}

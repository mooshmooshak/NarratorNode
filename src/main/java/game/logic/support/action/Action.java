package game.logic.support.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import game.abilities.AbilityList;
import game.abilities.BreadAbility;
import game.abilities.Burn;
import game.abilities.CultLeader;
import game.abilities.Disguiser;
import game.abilities.Driver;
import game.abilities.ElectroManiac;
import game.abilities.FactionSend;
import game.abilities.GameAbility;
import game.abilities.Ghost;
import game.abilities.GraveDigger;
import game.abilities.JailCreate;
import game.abilities.JailExecute;
import game.abilities.Janitor;
import game.abilities.Joker;
import game.abilities.Operator;
import game.abilities.ProxyKill;
import game.abilities.Survivor;
import game.abilities.Thief;
import game.abilities.Veteran;
import game.abilities.Visit;
import game.abilities.Witch;
import game.abilities.support.Bread;
import game.abilities.support.ElectrocutionException;
import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.ActionException;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import models.GameFactions;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import services.ActionService;
import util.Util;

public class Action {

    public Player owner;
    public PlayerList _targets;
    public List<String> args;
    public AbilityType abilityType;
    public boolean isSubmittedByOwner = true;
    public double timeLeft;

    private boolean isProxied;

    public Action(Player owner, AbilityType abilityType, Player... target) {
        this(owner, abilityType, new LinkedList<>(), new PlayerList(target));
    }

    public Action(Player owner, AbilityType abilityType, String option, Player target) {
        this(owner, abilityType, Util.toStringList(option), new PlayerList(target));
    }

    public Action(Player owner, AbilityType abilityType, String opt1, String opt2, Player... p1) {
        this(owner, abilityType, Util.toStringList(opt1, opt2), Player.list(p1));
    }

    public Action(Player owner, AbilityType abilityType, String option, String option2, PlayerList target) {
        this(owner, abilityType, Util.toStringList(option, option2), target);
    }

    public Action(Player owner, AbilityType abilityType, List<String> args, PlayerList targets) {
        this(owner, abilityType, Constants.ALL_TIME_LEFT, args, targets);
    }

    public Action(Player owner, AbilityType abilityType, double timeLeft, List<String> args, PlayerList target) {
        this.owner = owner;
        this._targets = target.copy();
        this.abilityType = abilityType;
        this.timeLeft = timeLeft;
        this.args = args;

        isProxied = false;
    }

    public Action setProxied() {
        this.isProxied = true;
        return this;
    }

    public Action setNotSubmittedByOwner() {
        this.isSubmittedByOwner = false;
        return this;
    }

    public ArrayList<Object> getInsteadPhrase() {
        return getAbility().getInsteadPhrase(this);
    }

    public Action cancel(int actionPosition, double timeLeft, boolean setCommand) {
        if(actionPosition == -1)
            throw new IllegalActionException("Action index out of bounds");
        if(owner.game.nightPhase != GamePhase.NIGHT_ACTION_PROCESSING && setCommand)
            owner.game.getEventManager().addCommand(owner, timeLeft, Constants.CANCEL, (actionPosition + 1) + "");

        GameAbility ability = getAbility();
        Message cancelMessage = ability.getCancelMessage(this);

        ability.onCancelAction(this, timeLeft, cancelMessage);

        return this;
    }

    public boolean isSameAbility(AbilityType abilityType) {
        return this.abilityType == abilityType;
    }

    public boolean isSameAbility(Action new_action) {
        return new_action.abilityType == this.abilityType;

    }

    public boolean isTargeting(PlayerList targets, AbilityType ability) {
        if(ability != this.abilityType)
            return false;
        if(this._targets.size() != targets.size())
            return false;

        if(is(Driver.abilityType))
            return this._targets.equals(targets);

        for(int i = 0; i < targets.size(); i++){
            if(!targets.get(i).getName().equals(this._targets.get(i).getName()))
                return false;
        }
        return true;
    }

    public boolean has(PlayerList target, AbilityType abilityType) {
        if(abilityType != this.abilityType)
            return false;
        PlayerList x1 = target;
        PlayerList x2 = this._targets;
        if(is(Driver.abilityType)){
            x1 = x1.copy().sortByName();
            x2 = x2.copy().sortByName();
        }
        return x1.equals(x2);
    }

    public boolean visitedHome() {
        if(getTarget() == owner)
            return true;
        return false;
    }

    private boolean isInvalid = false;

    public void setInvalid() {
        if(abilityType != ProxyKill.abilityType)// aka proxy killing
            isInvalid = true;
    }

    ArrayList<String> actualTargets;

    public void saveIntendedTargets() {
        if(_targets == null)
            return;
        actualTargets = new ArrayList<>();
        for(Player p: _targets)
            actualTargets.add(p.getName());
    }

    private PlayerList actuallyTargeted;

    public PlayerList getActualTargets() {
        return actuallyTargeted;
    }

    public void doNightAction() {
        if(isInvalid)
            return;
        actuallyTargeted = _targets.copy();
        boolean tNull = (_targets == null);

        boolean isDriverAction = is(Driver.abilityType, Operator.abilityType, ProxyKill.abilityType);
        if(!tNull){
            actuallyTargeted = _targets.copy();
            _targets.clear();
            for(Player p: actuallyTargeted){
                if(p.disguisedPrevName != null)
                    p = owner.game.getPlayerByName(p.disguisedPrevName);
                if(isDriverAction)
                    _targets.add(p);
                else if(p.getHouseTarget() != null)
                    _targets.add(p.getHouseTarget());
            }
        }

        GameAbility r = getAbility();

        if(r == null || r.getRealCharges() == 0 || Action.isIllegalRetarget(this))
            Visit.NoNightActionVisit(this);
        else
            r.doNightAction(this);

        if(!tNull)
            _targets = actuallyTargeted;
        actuallyTargeted = null;
    }

    public void completeNightAction() {
        if(owner.getLives() >= 0 || (owner.getRealAutoVestCount() > 0 && owner.isAlive())){
            attemptCompletion();
        }else if(!owner.hasInjuriesFromPreKillingPhase() && owner.game.nightPhase == GamePhase.KILLING){
            attemptCompletion();
        }else if(owner.isDead() && !owner.isExhumed()){
            attemptCompletion();
        }else if(is(Disguiser.abilityType) && owner.getLives() > 0){
            attemptCompletion();
        }else{
            owner.game.addActionStack(this);
        }

    }

    public ArrayList<Action> getSameActions(ActionList actions) {
        ArrayList<Action> list = new ArrayList<Action>();
        for(Action a: actions){
            if(a.isSameAbility(this))
                list.add(a);
        }

        return list;
    }

    public boolean isDoubleTargeter() {
        return false;
    }

    public boolean isTargeting(Player target2) {
        return _targets.contains(target2);
    }

    private PlayerList witchFuck = new PlayerList();

    public Action witchTouch(Player witch) {
        if(owner.game.getBool(SetupModifierName.WITCH_FEEDBACK))
            new Feedback(owner, Witch.WITCH_FEEDBACK);
        witchFuck.add(witch);
        return this;
    }

    public boolean wasChanged(Player witch) {
        return witch.in(witchFuck);
    }

    public boolean isArsonBurn() {
        return abilityType == Burn.abilityType;
    }

    public boolean isEmpty() {
        return _targets.isEmpty();
    }

    public boolean visited(Player target) {
        if(target == null || this._targets == null)
            return false;
        if(owner.is(Driver.abilityType)){
            return target.in(_targets);
        }
        if(_targets.getFirst() == null)
            return false;
        return _targets.getFirst().getAddress() == target;
    }

    public void cleanup() {
        if(is(Survivor.abilityType))
            _targets.clear();
        if(is(Veteran.abilityType))
            _targets.clear();
    }

    @Override
    public String toString() {
        return "\nowner: " + owner + "\ntarget: " + _targets + "\nability: " + abilityType;
    }

    public PlayerList getTargets() {
        if(_targets == null)
            return new PlayerList();
        return _targets.copy();
    }

    public ArrayList<String> getIntendedTargets() {
        if(_targets == null && actualTargets == null)
            return new ArrayList<>();
        if(_targets == null)
            return actualTargets;
        return _targets.getNamesToStringList();
    }

    // join used for jailor
    public void addTarget(Player p) {
        if(abilityType != JailExecute.abilityType)
            throw new IllegalActionException();
        _targets.add(p);
    }

    private boolean isCompleted = false;

    public boolean isCompleted() {
        return isCompleted;
    }

    public void markCompleted() {
        this.isCompleted = true;
    }

    public Player getTarget() {
        if(_targets == null || _targets.isEmpty())
            return null;
        return _targets.getFirst();
    }

    public String getIntendedTarget() {
        if(actualTargets != null && actualTargets.isEmpty())
            return null;
        else if(actualTargets != null)
            return actualTargets.get(0);
        else if(_targets == null || _targets.isEmpty())
            return null;
        return _targets.getFirst().getName();
    }

    // used by witch night action
    public void setTarget(Player p) {
        if(is(Witch.abilityType)){
            _targets.set(1, p);
        }else if(is(GraveDigger.abilityType)){
            _targets.set(1, p);
        }else if(is(ProxyKill.abilityType)){
            if(_targets.size() == 2)
                _targets.add(p);
            else
                _targets.set(2, p);
        }else if(is(Driver.abilityType)){
            _targets.set(0, p);
        }else if(is(Joker.abilityType) && _targets.size() > 1 && getArg1() == null){
            _targets.set(1, p);
        }else if(is(JailCreate.abilityType)){
            JailCreate card = (JailCreate) getAbility();
            _targets = card.getJailedTargets();
        }else if(_targets.isEmpty()){
            _targets.add(p);
        }else{
            _targets.set(0, p);
        }
    }

    public void setTargets(PlayerList pl) {
        _targets.clear();
        _targets.add(pl);
    }

    public String getArg1() {
        return this.args.isEmpty() ? null : this.args.get(0);
    }

    public String getArg2() {
        return this.args.size() < 2 ? null : this.args.get(1);
    }

    public String getArg3() {
        return this.args.size() < 3 ? null : this.args.get(2);
    }

    // the method narrator god file calls
    public void attemptCompletion() {
        if(this.isCompleted)
            return;
        ActionList actionList = owner.getActions();
        AbilityType abilityType = this.abilityType;
        AbilityType oldAbilityType = this.abilityType;
        boolean successWillTriggerBreadUsage = getAbility() != null
                && getAbility().successfulUsageTriggersBreadUsage(this);

        if(getAbility() != null && getAbility().getRealCharges() == 0){
            getAbility().useCharge();
            abilityType = Visit.abilityType;
        }else if(getAbility() == null)
            abilityType = Visit.abilityType;

        if(actionList.mainAction == this || actionList.freeActions.containsKey(abilityType)){
            try{
                this.abilityType = abilityType;
                doNightAction();
            }catch(ElectrocutionException e){
                electrocutionHandling(this, e);
            }
            this.abilityType = oldAbilityType;

            if(isCompleted() && successWillTriggerBreadUsage)
                owner.getAbility(BreadAbility.class).useCharge();

            useChargeIfApplicable(abilityType);

            return;
        }
        if(owner.isDead() || isProxied){
            try{
                doNightAction();
            }catch(ElectrocutionException e){
                electrocutionHandling(this, e);
            }
            useChargeIfApplicable(abilityType);
            return;
        }

        BreadAbility ba = owner.getAbility(BreadAbility.class);
        int actionIndex = actionList.extraActions.indexOf(this);
        if(actionIndex == -1)
            throw new Error();
        Bread b = (Bread) ba.charges.get(actionIndex);
        if(b.isReal()){
            try{
                doNightAction();
            }catch(ElectrocutionException e){
                electrocutionHandling(this, e);
            }
            if(isCompleted())
                b.markUsed();
            useChargeIfApplicable(abilityType);
        }else{
            isCompleted = true;
            b.markUsed();
            useChargeIfApplicable(abilityType);
        }
    }

    private void useChargeIfApplicable(AbilityType ability) {
        if(ability == this.abilityType && ability != BreadAbility.abilityType && ability != CultLeader.abilityType
                && ability != JailCreate.abilityType && ability != Thief.abilityType && ability != Janitor.abilityType
                && getAbility().getPerceivedCharges() != 0 && isCompleted){
            getAbility().useCharge();
            getAbility().triggerCooldown();
        }
    }

    private static void electrocutionHandling(Action a, ElectrocutionException e) {
        for(Player p: e.charged){
            if(!p.is(ElectroManiac.abilityType))
                ElectroManiac.ElectroKill(p);
        }
        Message m = new Happening(a.owner.game).add(e.triggerer, " caused an electrical explosion which killed ");
        PlayerList otherExploders = e.charged.copy().remove(e.triggerer);
        if(otherExploders.isEmpty()){
            m.add("only ", e.triggerer);
        }else{
            m.add(otherExploders);
        }
        m.add(".");

        PlayerList electros = e.charged.filter(ElectroManiac.abilityType);
        PlayerList nonActors = e.charged.copy().remove(electros);
        a.owner.game.doNightAction(nonActors, Disguiser.abilityType);
    }

    // doesn't take care of setting command
    // return type is {new action, old action};
    public static Action[] pushCommand(SelectionMessage e, Action action, ActionList actions) {
        Player p = action.owner;
        StringChoice you = new StringChoice(p);
        Action newAction;
        Action poppedOff = null;

        try{
            newAction = actions.addAction(action);
        }catch(ActionException ae){
            newAction = ae.newAction;
            poppedOff = ae.oldAction;
        }
        Action[] ret = { newAction, poppedOff };

        if(p.isDead() && !p.is(Ghost.abilityType))
            return ret;
        if(poppedOff != null){
            if(!poppedOff.isSameAbility(newAction)){
                e.add("Instead of ");
                e.add(poppedOff.getInsteadPhrase());
                e.add(", ");
                you.add(p, "you");
            }else{
                you.add(p, "You");
            }
        }else{
            you.add(p, "You");
        }
        e.add(you);
        if(!newAction.is(FactionSend.abilityType))
            e.add(" will ");
        e.add(newAction.getAbility().getActionDescription(newAction.getSameActions(actions)));
        e.add(".");
        return ret;
    }

    public void dayHappening() {
        owner.getAbility(abilityType).dayHappening(this);
    }

    public boolean isDayAction() {
        if(owner.getDayCommands().contains(abilityType))
            return true;
        return false;
    }

    public boolean isNightAction() {
        if(getAbility().isNightAbility(owner))
            return true;
        return false;
    }

    public boolean isTeamTargeting() {
        GameFaction t = owner.getGameFaction();
        if(!t.knowsTeam())
            return false;
        if(_targets == null)
            return false;
        String ownerColor = t.getColor();
        for(Player p: _targets){
            if(p.getGameFaction().getColor().equals(ownerColor))
                return true;
        }
        return false;
    }

    public GameAbility getAbility() {
        if(abilityType == Visit.abilityType)
            return new Visit(owner.game).initialize(owner);
        if(owner.tempAbilities != null)
            for(GameAbility a: owner.tempAbilities){
                if(abilityType == a.getAbilityType())
                    return a;
            }

        return owner.getAbility(abilityType);
    }

    public boolean isTeamAbility() {
        for(GameAbility a: owner.gameRole._abilities){
            if(abilityType == a.getAbilityType())
                return false;
        }
        for(GameFaction t: owner.getFactions()){
            if(t.hasAbility(abilityType))
                return true;
        }
        return false;
    }

    // TODO remove, I should use the int here only
    public boolean is(AbilityType abilityType) {
        return this.abilityType == abilityType;
    }

    public boolean is(AbilityList abilities) {
        for(GameAbility a: abilities){
            if(a.getAbilityType() == abilityType)
                return true;
        }
        return false;
    }

    // is one of these abilities
    public boolean is(AbilityType... abilityType) {
        for(AbilityType ability: abilityType)
            if(this.abilityType == ability)
                return true;
        return false;
    }

    public GameFactions getFactions() {
        Set<GameFaction> factions = new HashSet<>();
        for(GameFaction faction: owner.getFactions())
            if(faction.hasSharedAbilities() && is(faction.getAbilities()))
                factions.add(faction);

        return new GameFactions(factions);
    }

    public void pushCommand() {
        ArrayList<String> command = getAbility().getCommandParts(this);
        ActionService.recordCommand(owner, timeLeft, command);
    }

    public static boolean isIllegalRetarget(Action action) {
        Player player = action.owner;
        if(player.game.isFirstPhase())
            return false;
        GameAbility ability = action.getAbility();
        if(ability.modifiers.getBoolean(AbilityModifierName.BACK_TO_BACK, player.game))
            return false;
        ActionList actionList;
        if(ability.isDayAbility())
            actionList = player.getPrevDayTarget();
        else
            actionList = player.getPrevNightTarget();
        for(Action prevAction: actionList.getActions(action.abilityType)){
            if(ability.isRetarget(action, prevAction))
                return true;
        }
        return false;
    }
}

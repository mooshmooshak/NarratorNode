package game.logic.support;

import java.util.ArrayList;

import game.logic.support.action.Action;

public interface DoubleTargeter {

    public abstract ArrayList<Object> getRolePhrase(ArrayList<Action> actions);
}

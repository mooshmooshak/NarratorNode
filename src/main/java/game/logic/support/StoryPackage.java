package game.logic.support;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import game.setups.Setup;
import models.Command;
import models.FactionRole;
import models.GameModifiers;
import models.GeneratedRole;
import models.SetupHidden;
import models.enums.GameModifierName;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.GameOverviewSchema;
import models.schemas.PlayerSchema;
import services.GameService;
import util.Util;
import util.game.LookupUtil;
import util.game.SetupHiddenUtil;

public class StoryPackage { // Rename to replay

    public Game narrator;
    public String instanceID;
    public boolean isPrivate;
    public GameID replayID;
    public Set<Long> moderatorIDs;
    public HashMap<Player, Long> phoneBook;
    public Map<Long, Long> setupHiddenSpawns;
    public Map<PlayerDBID, Long> playerRoles;
    public ArrayList<Command> commands;

    public StoryPackage(GameID replayID, Game narrator) {
        this.replayID = replayID;
        this.narrator = narrator;
        phoneBook = new HashMap<>();
        commands = new ArrayList<>();
    }

    private void addPlayer(Player p, Optional<Long> userID) {
        if(userID.isPresent())
            phoneBook.put(p, userID.get());
        else
            p.setComputer();
    }

    public static StoryPackage deserialize(StoryReader sr, Setup setup, GameID replayID) {
        Game game = new Game(setup);
        StoryPackage sp = new StoryPackage(replayID, game);

        GameOverviewSchema gameOverviewSchema = sr.getGameOverview();
        game.setSeed(gameOverviewSchema.seed);
        sp.instanceID = gameOverviewSchema.lobbyID;
        sp.isPrivate = gameOverviewSchema.isPrivate;
        sp.moderatorIDs = sr.getModeratorIDs();

        Collection<Modifier<GameModifierName>> modifiers = sr.getGameModifiers();
        game.gameModifiers = new GameModifiers<GameModifierName>(game, new Modifiers<>(modifiers));

        sp.setupHiddenSpawns = sr.getSetupHiddenSpawns();
        sp.playerRoles = sr.getPlayerRoles();

        Player new_player;
        for(PlayerSchema p: sr.getPlayers()){
            new_player = game.addPlayer(p.name);
            new_player.databaseID = p.id;
            sp.addPlayer(new_player, p.userID);

            if(!p.userID.isPresent())
                new_player.setComputer();
        }

        sp.commands = sr.getCommands();

        return sp;
    }

    public static RoleAssigner GetRoleAssigner(final StoryPackage sp) {
        return new RoleAssigner() {

            @Override
            public ArrayList<GeneratedRole> pickRoles(Game game, List<SetupHidden> setupHiddens) {
                ArrayList<GeneratedRole> gRoles = new ArrayList<>();
                long factionRoleID;
                FactionRole factionRole;
                GeneratedRole role;
                Setup setup;
                for(SetupHidden setupHidden: setupHiddens){
                    setup = setupHidden.setup;
                    factionRoleID = sp.setupHiddenSpawns.get(setupHidden.id);
                    factionRole = setup.getFactionRole(factionRoleID);
                    role = new GeneratedRole(setupHidden);
                    role.factionRoles.add(factionRole);
                    gRoles.add(role);
                }
                return gRoles;
            }

            @Override
            public ArrayList<GeneratedRole> getPassOutRoles(int playerCount, ArrayList<GeneratedRole> generatedRoles) {
                ArrayList<GeneratedRole> pickedRoles = new ArrayList<>();
                Set<Long> pickedSetupHiddenIDs = new HashSet<>(sp.playerRoles.values());
                for(GeneratedRole role: generatedRoles){
                    if(pickedSetupHiddenIDs.contains(role.setupHidden.id))
                        pickedRoles.add(role);
                }
                return pickedRoles;
            }

            @Override
            public void passRolesOut(Game game, List<RolePackage> releasedRoles) {
                Map<SetupHidden, RolePackage> releaseMap = SetupHiddenUtil.rolePackageToMap(releasedRoles);

                sp.narrator.getRandom().reset();
                SetupHidden setupHidden;
                RolePackage rolePackage;
                long setupHiddenID;
                for(Player p: sp.narrator.players){
                    setupHiddenID = sp.playerRoles.get(p.databaseID);
                    setupHidden = LookupUtil.findSetupHidden(game, setupHiddenID);
                    rolePackage = releaseMap.get(setupHidden);
                    rolePackage.assign(p);
                }

            }
        };
    }

    public Throwable runCommands() {
        if(!narrator.isStarted())
            GameService.internalStart(narrator, GetRoleAssigner(this));

        Map<PlayerDBID, Player> map = narrator.players.getDatabaseMap();
        CommandHandler ch = new CommandHandler(narrator);
        Command command;
        Player player;
        for(Command command2: commands){
            command = command2;
            if(command.playerID.isPresent())
                player = map.get(command.playerID.get());
            else
                player = null;
            try{
                ch.parseCommand(player, command.text, true, command.timeLeft);
            }catch(Error | Exception e){
                Util.log("Game id : " + this.replayID);
                e.printStackTrace();
                return e;
            }
        }
        return null;
    }

    public boolean onlyComputers() {
        for(Player p: phoneBook.keySet()){
            if(!p.isComputer())
                return false;
        }
        return true;
    }

}

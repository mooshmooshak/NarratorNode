package game.logic.support.attacks;

import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Constants;
import game.logic.support.saves.BGHeal;
import game.logic.support.saves.Heal;

public class IndirectAttack extends Attack {

    String[] type;
    Player injured;
    Player cause;

    public IndirectAttack(String[] type, Player injured, Player cause) {
        super(injured);
        this.type = type;
        this.injured = injured;

        this.cause = cause;
    }

    public String[] getType() {
        return type;
    }

    public boolean isImmune() {
        if(!injured.isInvulnerable())
            return false;
        return type != Constants.JESTER_KILL_FLAG;
    }

    public boolean isHealable(Heal type) {
        return !(type instanceof BGHeal);
    }

    public void counterAttack(Heal type) {
        super.counterAttack(type);
        if(type instanceof BGHeal)
            throw new IllegalActionException();

    }

    public PlayerList getInjured() {
        return Player.list(injured);
    }

    public Player getAttacker() {
        return null;
    }

    public Player getCause() {
        return cause;
    }
}

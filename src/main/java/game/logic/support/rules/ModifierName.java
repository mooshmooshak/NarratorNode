package game.logic.support.rules;

import models.enums.AbilityModifierName;
import models.enums.FactionModifierName;
import models.enums.GameModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;

public interface ModifierName {

    ModifierName[] BOOL_MODIFIERS = new ModifierName[] { AbilityModifierName.BACK_TO_BACK,
            AbilityModifierName.DISFRANCHISED_ANNOUNCED, AbilityModifierName.HIDDEN, AbilityModifierName.SELF_TARGET,
            AbilityModifierName.SILENCE_ANNOUNCED, AbilityModifierName.SECRET_KILL, AbilityModifierName.ZERO_WEIGHTED,
            AbilityModifierName.AS_FAKE_VESTS, AbilityModifierName.GS_FAULTY_GUNS,

            FactionModifierName.HAS_NIGHT_CHAT, FactionModifierName.IS_RECRUITABLE, FactionModifierName.KNOWS_ALLIES,
            FactionModifierName.LAST_WILL, FactionModifierName.LIVE_TO_WIN,

            RoleModifierName.UNDETECTABLE, RoleModifierName.UNCONVERTABLE, RoleModifierName.UNBLOCKABLE,
            RoleModifierName.UNIQUE, RoleModifierName.HIDDEN_FLIP,

            SetupModifierName.ARCH_BURN, SetupModifierName.ARCH_QUARANTINE, SetupModifierName.CHECK_DIFFERENTIATION,
            SetupModifierName.DAY_START, SetupModifierName.HEAL_FEEDBACK, SetupModifierName.HEAL_SUCCESS_FEEDBACK,
            SetupModifierName.GD_REANIMATE, SetupModifierName.GS_DAY_GUNS, SetupModifierName.GUARD_REDIRECTS_CONVERT,
            SetupModifierName.GUARD_REDIRECTS_DOUSE, SetupModifierName.GUARD_REDIRECTS_POISON,
            SetupModifierName.HEAL_BLOCKS_POISON, SetupModifierName.MILLER_SUITED, SetupModifierName.SPY_TARGETS_ALLIES,
            SetupModifierName.SPY_TARGETS_ENEMIES, SetupModifierName.SHERIFF_PREPEEK, SetupModifierName.FOLLOW_GETS_ALL,
            SetupModifierName.FOLLOW_PIERCES_IMMUNITY, SetupModifierName.CORONER_EXHUMES,
            SetupModifierName.CORONER_LEARNS_ROLES, SetupModifierName.BOMB_PIERCE,
            SetupModifierName.MARSHALL_QUICK_REVEAL, SetupModifierName.BREAD_PASSING,
            SetupModifierName.SELF_BREAD_USAGE, SetupModifierName.LAST_WILL, SetupModifierName.MASON_PROMOTION,
            SetupModifierName.MASON_NON_CIT_RECRUIT, SetupModifierName.ENFORCER_LEARNS_NAMES,
            SetupModifierName.BLOCK_FEEDBACK, SetupModifierName.AMNESIAC_KEEPS_CHARGES,
            SetupModifierName.AMNESIAC_MUST_REMEMBER, SetupModifierName.JESTER_CAN_ANNOY,
            SetupModifierName.EXECUTIONER_WIN_IMMUNE, SetupModifierName.EXEC_TOWN,
            SetupModifierName.EXECUTIONER_TO_JESTER, SetupModifierName.WITCH_FEEDBACK, SetupModifierName.DOUSE_FEEDBACK,
            SetupModifierName.CULT_PROMOTION, SetupModifierName.CULT_KEEPS_ROLES, SetupModifierName.CONVERT_REFUSABLE,
            SetupModifierName.DIFFERENTIATED_FACTION_KILLS, SetupModifierName.JANITOR_GETS_ROLES,
            SetupModifierName.SELF_VOTE, SetupModifierName.SKIP_VOTE, SetupModifierName.SECRET_VOTES,
            SetupModifierName.TAILOR_FEEDBACK, SetupModifierName.SNITCH_PIERCES_SUIT, SetupModifierName.PROXY_UPGRADE,
            SetupModifierName.PUNCH_ALLOWED,

            GameModifierName.CHAT_ROLES, GameModifierName.OMNISCIENT_DEAD };
    ModifierName[] INT_MODIFIERS = new ModifierName[] { AbilityModifierName.CHARGES, AbilityModifierName.COOLDOWN,
            AbilityModifierName.FACTION_COUNT_MAX, AbilityModifierName.FACTION_COUNT_MIN,
            AbilityModifierName.JAILOR_CLEAN_ROLES,

            FactionModifierName.PUNCH_SUCCESS, FactionModifierName.WIN_PRIORITY,

            RoleModifierName.AUTO_VEST,

            SetupModifierName.BOUNTY_FAIL_REWARD, SetupModifierName.BOUNTY_INCUBATION,
            SetupModifierName.CULT_POWER_ROLE_CD, SetupModifierName.MM_SPREE_DELAY, SetupModifierName.ARSON_DAY_IGNITES,
            SetupModifierName.JESTER_KILLS, SetupModifierName.MARSHALL_EXECUTIONS, SetupModifierName.MAYOR_VOTE_POWER,
            SetupModifierName.CIT_RATIO, SetupModifierName.CHARGE_VARIABILITY,

            GameModifierName.DAY_LENGTH_DECREASE, GameModifierName.DAY_LENGTH_MIN, GameModifierName.DAY_LENGTH_START,
            GameModifierName.NIGHT_LENGTH, GameModifierName.TRIAL_LENGTH, GameModifierName.DISCUSSION_LENGTH,
            GameModifierName.ROLE_PICKING_LENGTH, GameModifierName.VOTE_SYSTEM };

    ModifierName[] STRING_MODIFIERS = new ModifierName[] { SetupModifierName.BREAD_NAME, SetupModifierName.GUN_NAME,
            SetupModifierName.VEST_NAME, };

    String getActiveFormat();

    String getInactiveFormat();

    String getUnsureFormat();

    String getUnlimitedFormat();

    String getLimitedFormat();

    String getLabelFormat();

    Object getDefaultValue();
}

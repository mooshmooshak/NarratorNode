package game.logic.support;

import game.event.EventDecoder;
import game.logic.GameFaction;
import game.logic.GameRole;
import models.Faction;
import models.FactionRole;
import models.Role;

public class HTString {
    private String data;
    private String color;

    public HTString(GameFaction t) {
        this.data = t.getName();
        this.color = t.getColor();
    }

    public HTString(String data, String color) {
        this.data = data;
        this.color = color;
    }

    public HTString(Faction faction, Role role) {
        this(faction.getName() + " " + role.getName(), faction.getColor());
    }

    public HTString(GameRole gameRole) {
        this(gameRole.factionRole);
    }

    public HTString(FactionRole factionRole) {
        this(factionRole.faction, factionRole.role);
    }

    public HTString(Faction faction) {
        this(faction.getName(), faction.getColor());
    }

    public String access(EventDecoder html) {
        if(html != null){
            return html.coloredText(data, color);
        }
        return data;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o.getClass() != HTString.class)
            return false;
        HTString ht = (HTString) o;
        if(ht.color.equals(color))
            if(ht.data.equals(data))
                return true;
        return false;
    }

    @Override
    public int hashCode() {
        return (color + data).hashCode();
    }
}

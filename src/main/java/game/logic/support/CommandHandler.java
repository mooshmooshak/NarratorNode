package game.logic.support;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.Vote;
import game.abilities.support.CultInvitation;
import game.abilities.util.AbilityUtil;
import game.ai.Controller;
import game.event.Message;
import game.event.OGIMessage;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import models.FactionRole;
import models.enums.AbilityType;
import services.ChatService;
import util.game.LookupUtil;
import util.game.VoteUtil;

public class CommandHandler {

    protected Game game;

    public CommandHandler(Game n) {
        this.game = n;
    }

    public void parseCommand(Player owner, String command, boolean modMode) {
        parseCommand(owner, command, modMode, Constants.ALL_TIME_LEFT);
    }

    public void parseCommand(Player owner, String command, boolean modMode, double timeLeft) {

        if(command.length() == 0)
            return;

        command = command.replace("\n", "");
        String name = owner == null ? "Skipper" : owner.getName();
        command(owner, command, name, null, modMode, timeLeft);
    }

    public static final String SAY = Constants.SAY;
    public static final String END_NIGHT = Constants.END_NIGHT;
    public static final String MODKILL = Constants.MODKILL;
    public static final String PREFER = "prefer";
    public static final String END_PHASE = "end_phase";
    public static final String END_PHASED_SPACED = "end phase";
    public static final String MOD_CHANGE_ROLE = "change_role";
    public static final String[] MOD_COMMANDS = { MOD_CHANGE_ROLE, MODKILL, Constants.MOD_ADDBREAD };

    // name is used for double command
    public static final String END_NIGHT_EXCEPTION = "You can only end night during the night";

    public void command(Player owner, String message, String name) {
        command(owner, message, name, null, true, Constants.ALL_TIME_LEFT);
    }

    public Message command(Player owner, String message, String name, AlternateLookup aLookup, boolean modMode,
            double timeLeft) {
        if(!game.isInProgress() && game.isStarted())
            return null;
        switch (message.toLowerCase()) {
        case END_PHASE:
        case END_PHASED_SPACED:
            game.endPhase(timeLeft);
            return null;
        case MODKILL:
            if(owner == null)
                throw new NarratorException("Must submit a target.");
            owner.modkill(timeLeft);
            return null;
        case END_NIGHT:
            if(game.isDay())
                throw new PhaseException(END_NIGHT_EXCEPTION);
            if(owner.endedNight())
                owner.cancelEndNight(timeLeft);
            else
                owner.endNight(timeLeft);
            return null;
        case Constants.SKIP_DAY:
            owner.doDayAction(VoteUtil.skipAction(owner, timeLeft));
            return null;

        case Vote.UNVOTE:
            if(dayAttempt())
                owner.cancelAction(0, timeLeft);
            break;
        case Constants.ACCEPT:
        case Constants.DECLINE:
            if(!game.isDay())
                throw new PhaseException("You cannot accept or decline invitations during the night.");
            if(owner.hasDayAction(CultInvitation.abilityType))
                owner.doDayAction(CultInvitation.getAction(owner, message));
            else
                throw new IllegalActionException(
                        "You don't have any recruitment iniviations to accept or decline right now.");
            break;

        case Constants.CHECK_VOTE:
            game.voteSystem.forceCheckVote(timeLeft);
            return null;

        case Constants.CANCEL:
            if(!game.isNight())
                throw new PhaseException("You can't cancel night actions during the day.");
            try{
                owner.clearTargets();
                new OGIMessage(owner, "You have canceled all your inputted actions.");

                return null;
            }catch(NumberFormatException e){
                new OGIMessage(owner, "canceling requires a number");
            }

        default:
            return tryDoubleCommand(owner, message, name, aLookup, modMode, timeLeft);
        }
        return null;
    }

    public void tryDoubleCommand(Controller owner, String message, String name, double timeLeft) {
        tryDoubleCommand(owner.getPlayer(), message, name, null, true, timeLeft);
    }

    // name is used in case owner is null
    public Message tryDoubleCommand(Player owner, String message, String name, AlternateLookup aLookup, boolean modMode,
            double timeLeft) {
        ArrayList<String> block = new ArrayList<>();
        for(String s: message.split(" "))
            block.add(s);

        String command = block.remove(0);

        if(command.equalsIgnoreCase(Constants.CANCEL)){
            try{
                int num = Integer.parseInt(block.get(0));
                if(game.isDay())
                    owner.cancelAction(num - 1, timeLeft);
                else
                    owner.cancelAction(num - 1, timeLeft);
            }catch(NumberFormatException e){
                throw new PlayerTargetingException(
                        "If you're not canceling all actions, you must input the index of the action you wish to cancel.");
            }
            return null;
        }

        if(command.equalsIgnoreCase(SAY)){
            message = message.substring(SAY.length() + 1);
            message = message.substring(block.get(0).length() + 1);

            ChatService.submit(owner, block.get(0), message);
            return null;
        }
        if(command.equalsIgnoreCase(Constants.MOD_ADDBREAD) && modMode){
            message = message.substring(command.length() + 1);
            boolean negative = message.startsWith("-");
            if(negative)
                message = message.substring(1);
            String number = "";
            while (Character.isDigit(message.charAt(0))){
                number += message.substring(0, 1);
                message = message.substring(1);
            }
            if(number.length() == 0)
                return null;
            message = message.substring(1);
            Player target = game.getPlayerByName(message, aLookup);
            if(target == null)
                return null;
            int breadToAdd = Integer.parseInt(number);
            if(negative)
                breadToAdd *= -1;

            target.addModBread(breadToAdd, timeLeft);

            return null;
        }
        if(command.equals(MOD_CHANGE_ROLE) && modMode){
            owner = game.getPlayerByName(block.get(0), aLookup);
            String color = block.get(1);
            String roleName = block.get(2);

            Optional<FactionRole> factionRole = LookupUtil.findFactionRole(game.setup, roleName, color);
            if(factionRole.isPresent())
                game.changeRole(owner, factionRole.get(), timeLeft);

            return null;
        }
        if(command.equals(MODKILL) && modMode){
            owner = game.getPlayerByName(block.get(0), aLookup);
            return owner.modkill(timeLeft);
        }

        if(command.equalsIgnoreCase(Constants.CANCEL)){
            int untarget = message.indexOf(block.get(0));
            if(game.isDay())
                throw new IllegalActionException("You can't cancel actions during the day.");
            message = message.substring(untarget);
            throw new NullPointerException();
        }
        if(command.equalsIgnoreCase(Constants.LAST_WILL)){
            owner.setLastWill(message = message.substring(Constants.LAST_WILL.length() + 1));
            return null;
        }

        if(command.equalsIgnoreCase(Constants.TEAM_LAST_WILL)){
            owner.setTeamLastWill(message = message.substring(SAY.length() + 1));
            return null;
        }

        AbilityType ability = AbilityUtil.getAbilityByCommand(command);
        if(!owner.hasAbility(ability))
            throw new IllegalActionException("Unknown ability " + command);

        block.add(0, command);

        try{
            Action a = owner.getAbility(ability).parseCommand(owner, timeLeft, block);

            if(game.isDay())
                owner.doDayAction(a);
            else
                owner.setNightTarget(a);
        }catch(Exception e){
            throw new IllegalActionException("Couldn't parse command.");
        }
        return null;
    }

    public static String parseTeam(String name, Game n) {
        GameFaction t = n.getFaction(name); // checking if color
        if(t != null)
            return name;
        t = n.getFactionByName(name);
        if(t != null)
            return t.getColor();
        return Constants.A_INVALID;
    }

    public boolean dayAttempt() {
        if(!game.isDay())
            throw new PhaseException("Can't do this during the night");

        return true;
    }

}

package game.logic.support.saves;

import game.abilities.Doctor;
import game.event.Feedback;
import game.logic.Player;
import models.enums.SetupModifierName;

public class Heal {

    String[] type;
    Player healer;

    public Heal(Player healer, String[] type) {
        this.type = type;
        this.healer = healer;
    }

    public String[] getType() {
        return type;
    }

    public Player getHealer() {
        return healer;
    }

    public void onHeal(Player healed) {
        if(healer.game.getBool(SetupModifierName.HEAL_SUCCESS_FEEDBACK) && !healed.isVesting() && !healed.isJailed())
            new Feedback(healer, Doctor.SUCCESFULL_HEAL);
    }
}

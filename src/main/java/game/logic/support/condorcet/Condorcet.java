package game.logic.support.condorcet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Condorcet {

    public static <T> List<Set<T>> getOrderings(Collection<List<Set<T>>> preferenceInput) {
        return getOrderings(getChoices(preferenceInput), preferenceInput);
    }

    public static <T> List<Set<T>> getOrderings(Set<T> choices, Collection<List<Set<T>>> preferenceInput) {
        List<Pairing<T>> rankings = getPairings(choices, preferenceInput);
        Collections.sort(rankings);
        CondorcetGraph<T> acceptedGraph = new CondorcetGraph<T>(rankings);

        return acceptedGraph.getOrderings();
    }

    public static <T> List<Pairing<T>> getPairings(Set<T> choices, Collection<List<Set<T>>> preferenceInput) {
        ArrayList<Pairing<T>> rankings = new ArrayList<>();
        T pi, pj;
        double finalRank;
        List<T> choiceList = toList(choices);
        for(int i = 0; i < choices.size(); i++){
            pi = choiceList.get(i);
            for(int j = i + 1; j < choices.size(); j++){
                pj = choiceList.get(j);
                finalRank = getDifference(pi, pj, preferenceInput);
                rankings.add(new Pairing<T>(pi, pj, finalRank));
            }
        }
        return rankings;
    }

    private static <T> List<T> toList(Set<T> set) {
        List<T> choiceList = new ArrayList<>();
        for(T choice: set)
            choiceList.add(choice);
        return choiceList;
    }

    private static <T> Set<T> getChoices(Collection<List<Set<T>>> input) {
        Set<T> choices = new HashSet<>();
        for(List<Set<T>> personPreference: input)
            for(Set<T> choiceRankingGroup: personPreference)
                for(T option: choiceRankingGroup)
                    choices.add(option);
        return choices;
    }

    private static <T> double getDifference(T p1, T p2, Collection<List<Set<T>>> input) {
        int p1Preferred = 0;
        int p2Preferred = 0;
        for(List<Set<T>> personPreference: input){
            Boolean isPreferred = p1PreferredOverP2(p1, p2, personPreference);
            if(isPreferred == null)
                continue;
            if(isPreferred)
                p1Preferred++;
            else
                p2Preferred++;
        }
        if(p1Preferred == 0 && p2Preferred == 0){
            p1Preferred++;
            p2Preferred++;
        }
        return (double) p1Preferred / (p2Preferred + p1Preferred);
    }

    private static <T> Boolean p1PreferredOverP2(T p1, T p2, List<Set<T>> personPreference) {
        int p1Index = getIndex(p1, personPreference);
        int p2Index = getIndex(p2, personPreference);
        if(p1Index == p2Index)
            return null;
        if(p1Index == -1)
            return false;
        if(p2Index == -1)
            return true;
        return p1Index < p2Index;
    }

    private static <T> int getIndex(T p, List<Set<T>> personPreference) {
        Set<T> choiceRankingGroup;
        for(int i = 0; i < personPreference.size(); i++){
            choiceRankingGroup = personPreference.get(i);
            if(choiceRankingGroup.contains(p))
                return i;
        }
        return -1;
    }
}

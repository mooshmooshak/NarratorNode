package game.logic.support.condorcet;

public class Pairing<T> implements Comparable<Pairing<T>> {
    public T winner, loser;

    double rank;

    public Pairing(T p1, T p2, double finalRank) {
        if(finalRank > 0.5){
            this.winner = p1;
            this.loser = p2;
        }else if(finalRank < 0.5){
            this.loser = p1;
            this.winner = p2;
        }else{
            this.winner = p2;
            this.loser = p1;
        }

        this.rank = Math.abs(0.5 - finalRank);
    }

    public void flip() {
        T newWinner = this.loser;
        this.loser = this.winner;
        this.winner = newWinner;
    }

    @Override
    public int compareTo(Pairing<T> p) {
        if(rank > p.rank)
            return -1;
        if(rank < p.rank)
            return 1;
        return 0;
    }

    public boolean isTie() {
        return rank == 0.0;
    }

    @Override
    public String toString() {
        String rankStr = Double.toString(rank);
        int stringLength = Math.min(4, rankStr.length());
        return "\n\t" + rankStr.substring(0, stringLength) + "\t" + winner.toString() + " - " + loser.toString();
    }
}

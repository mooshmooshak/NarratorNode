package game.logic.support.condorcet;

import java.util.HashSet;
import java.util.Set;

public class CondorcetNode<T> {

    private Set<T> node;
    public Set<CondorcetNode<T>> winners, losers;

    public CondorcetNode(T winner) {
        this.node = new HashSet<>();
        node.add(winner);
    }

    public void addLoser(CondorcetNode<T> loser) {
        if(losers == null)
            losers = new HashSet<>();
        losers.add(loser);
    }

    public void addWinner(CondorcetNode<T> winner) {
        if(winners == null)
            winners = new HashSet<>();
        winners.add(winner);

    }

    public void removeLoser(CondorcetNode<T> loser) {
        if(losers != null){
            losers.remove(loser);
            if(losers.isEmpty())
                losers = null;
        }
    }

    public void removeWinner(CondorcetNode<T> winner) {
        if(winners != null){
            winners.remove(winner);
            if(winners.isEmpty())
                winners = null;
        }
    }

    public boolean isSame(CondorcetNode<T> other) {
        if(other == this)
            return true;

        if(hasWinners() && !other.hasWinners())
            return false;
        if(!hasWinners() && other.hasWinners())
            return false;
        if(hasLosers() && !other.hasLosers())
            return false;
        if(!hasLosers() && other.hasLosers())
            return false;

        if(losers != null && !losers.equals(other.losers))
            return false;
        if(winners != null && !winners.equals(other.winners))
            return false;

        return true;
    }

    public CondorcetNode<T> mash(CondorcetNode<T> cNode) {
        if(cNode.winners != null){
            for(CondorcetNode<T> c: cNode.winners){
                c.removeLoser(cNode);
            }
        }
        if(cNode.losers != null){
            for(CondorcetNode<T> c: cNode.losers){
                c.removeWinner(cNode);
            }
        }
        for(T o: cNode.node)
            node.add(o);
        return this;
    }

    public boolean hasLosers() {
        return losers != null && !losers.isEmpty();
    }

    public boolean hasWinners() {
        return losers != null && !losers.isEmpty();
    }

    public CondorcetNode<T> getLoser() {
        return losers.iterator().next();
    }

    public Set<T> getPlayers() {
        return node;
    }

    public String toString() {
        return node.toString();
    }
}

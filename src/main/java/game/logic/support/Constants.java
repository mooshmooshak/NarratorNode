package game.logic.support;

import models.enums.AbilityType;

public class Constants {

    public static final int MAX_DB_PLAYER_LENGTH = 40;

    public static final double ALL_TIME_LEFT = 1;
    public static final double NO_TIME_LEFT = 0;

    public static final String STARTING_HASH = "";

    public static final String ANY_RANDOM_ROLE_NAME = "Any Random";

    public static final String TOWN_RANDOM_ROLE_NAME = "Town Random";
    public static final String TOWN_PROTECTIVE_ROLE_NAME = "Town Protective";
    public static final String TOWN_POWER_ROLE_NAME = "Town Power";
    public static final String TOWN_INVESTIGATIVE_ROLE_NAME = "Town Investigative";
    public static final String TOWN_KILLING_ROLE_NAME = "Town Killing";
    public static final String TOWN_GOVERNMENT_ROLE_NAME = "Town Government";

    public static final String MAFIA_RANDOM_ROLE_NAME = "Mafia Random";
    public static final String YAKUZA_RANDOM_ROLE_NAME = "Yakuza Random";

    public static final String NEUTRAL_RANDOM_ROLE_NAME = "Neutral Random";
    public static final String NEUTRAL_BENIGN_RANDOM_ROLE_NAME = "Neutral Benign";
    public static final String NEUTRAL_EVIL_RANDOM_ROLE_NAME = "Neutral Evil";
    public static final String NEUTRAL_KILLING_RANDOM_ROLE_NAME = "Neutral Killer";

    public static final String NO_NIGHT_ACTION = "You have no night action!  Type \'end night\' to end the night";
    public static final String NO_VALID_NIGHT_ACTION = "You don't have any night actions";

    public static final String CANCEL = "cancel";

    public static final String SEPERATOR = "#";

    public static final String ADD_ROLE = "ADD_role";
    public static final String REMOVE_ROLE = "REMOVE_ROLE ";

    public static final String START_GAME = "START_GAME ";

    public static final String SET_RULES = "SET_RULES ";

    // Commands
    public static final String CHECK_VOTE = "checkvote";
    public static final String MODKILL = "modkill";
    public static final String END_NIGHT = "end night";
    public static final String UNTARGET = "untarget";
    public static final String SKIP_DAY = "skip day";
    public static final String SKIP_VOTE_TITLE = "Skip Day";
    public static final String SAY = "say";

    public static final String NOTHING_DONE = "You chose to do nothing tonight.";

    public static final String AUTO_VEST_USAGE = "You used one of your bulletproof vests last night!";
    public static final String NIGHT_IMMUNE_FEEDBACK = "Your target was immune at night!";
    public static final String NIGHT_IMMUNE_TARGET_FEEDBACK = "You were attacked last night but you survived the encounter.";

    public static final String MAFIA_SENT_FEEDBACK = " was sent to kill ";

    // winstrings
    public static final String NO_WINNER = "Nobody won!";

    public static final String MODKILL_ROLE_NAME = "ModKill";

    public static final String[] DOCTOR_HEAL_FLAG = { "doctor", "doctor" };

    public static final String[] BODYGUARD_KILL_FLAG = { "bodyguard", "In a shootout" };
    public static final String[] VIGILANTE_KILL_FLAG = { "vigilante", "Taken down outside the law" };
    public static final String[] VETERAN_KILL_FLAG = { "veteran", "Riddled with military bullets" };
    public static final String[] MASONLEADER_KILL_FLAG = { "bludgeon", "Bludgeoned with a club" };
    public static final String[] BOMB_KILL_FLAG = { "bomb", "Triggered a bomb" };
    public static final String[] GUN_KILL_FLAG = { "gun", "Gunned down" };
    public static final String[] GUN_SELF_KILL_FLAG = { "gun", "Gunned down", "faulty" };
    public static final String[] JESTER_KILL_FLAG = { "jester", "With a broken heart and a noose around their neck" };
    public static final String[] MODKILL_FLAG = { "modkill", "Modkilled" };
    public static final String[] LYNCH_FLAG = { "lynch", "Democratically eliminated" };
    public static final String[] MASS_MURDERER_FLAG = { "massMurderer", "Run in with a mass murderer" };
    public static final String[] POISON_KILL_FLAG = { "poisoned", "Poisoned" };
    public static final String[] SK_KILL_FLAG = { "stabbing", "Stabbed" };
    public static final String[] JOKER_KILL_FLAG = { "joker", "Victim of the bounty" };
    public static final String[] JAIL_KILL_FLAG = { "executed", "Executed" };
    public static final String[] PUNCH_KILL_FLAG = { "punched", "Punched out" };
    public static final String[] ELECTRO_KILL_FLAG = { "electrocuted", "Electrocuted" };
    public static final String[] ARSON_KILL_FLAG = { "burning", "Burned" };
    public static final String[] HIDDEN_KILL_FLAG = { "cleaned", "Immaculately cleaned" };
    public static final String[] ASSASSIN_KILL_FLAG = { "assassinate", "Assassinated" };
    public static final String[] FACTION_KILL_FLAG = { "faction", "Shot by " };

    // public static final String[] NOT_ATTACKED = {"not attacked", "not attacked"};

    public static final String DAY_CHAT = "everyone";
    public static final String DEAD_CHAT = "dead";
    public static final String VOID_CHAT = "void";

    public static final String VEST_COMMAND = AbilityType.Survivor.command;
    public static final String VEST_COMMAND_LOWER = VEST_COMMAND.toLowerCase();
    public static final String GUN_COMMAND = AbilityType.Vigilante.command;

    public static final String INITIAL = "#E4CAA5";

    public static final String WHITE = "#FFFFFF";
    public static final String RED = "#FF0000";
    public static final String BLUE = "#6495ED";
    public static final String YELLOW = "#FFFF00";
    public static final String ORANGE = "#FF8C00";
    public static final String BROWN = "#CD853E";
    public static final String PINK = "#DDA0DD";
    public static final String GREY = "#888888";
    public static final String SICKLY_GREEN = "#D5E68C";
    public static final String GREEN = "#00FF00";
    public static final String PURPLE = "#880BFC";

    public static final String A_NORMAL = WHITE;
    public static final String A_SKIP = WHITE;
    public static final String A_RANDOM = WHITE;
    public static final String A_CLEANED = WHITE;
    public static final String A_NEUTRAL = GREY;
    public static final String A_INVALID = "#INVALID";

    public static final String PUBLIC = "PUBLIC";
    public static final String PRIVATE = "PRIVATE";

    public static final String MOD_ADDBREAD = "addbread";

    public static final String ACCEPT = "confirm";
    public static final String DECLINE = "decline";
    public static final String RECRUITMENT = "Recruitment";

    public static final String JANITOR_DESCRIP = "????";

    public static final String JAIL_CHAT_KEY_SEPERATOR = "$";

    public static final int MAX_INT_MODIFIER = 8388607;

    public static final String LAST_WILL = "lw";
    public static final String TEAM_LAST_WILL = "tlw";

    public static final int UNLIMITED = -1;

    public static final int DEFAULT_PLAYER_LIMIT = 15;

}

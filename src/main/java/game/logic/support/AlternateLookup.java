package game.logic.support;

import game.logic.Player;

public interface AlternateLookup {

    Player lookup(String name);

}

package game.logic.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import models.GeneratedRole;
import models.SetupHidden;
import services.GameSpawnsService;
import services.PlayerRoleService;

public class RoleAssigner {

    public ArrayList<GeneratedRole> pickRoles(Game game, List<SetupHidden> setupHiddens) {
        return GameSpawnsService.pickRoles(game, setupHiddens);
    }

    public ArrayList<GeneratedRole> getPassOutRoles(int playerCount, ArrayList<GeneratedRole> generatedRoles) {
        return GameSpawnsService.getPassOutRoles(playerCount, generatedRoles);
    }

    public void passRolesOut(Game narrator, List<RolePackage> releasedRoles) {
        PlayerList players = narrator.players;
        Map<String, Set<RolePackage>> preferenceMap = PlayerRoleService.getRoleLookupMap(releasedRoles);

        PlayerList roleDesirers;
        List<RolePackage> preferredRoles;
        Map<Player, List<RolePackage>> playerToRoles = new LinkedHashMap<>();
        HashMap<RolePackage, PlayerList> roleToPlayer = new LinkedHashMap<>();
        for(Player player: players){
            preferredRoles = player.cleanPrefers(preferenceMap, releasedRoles);
            playerToRoles.put(player, preferredRoles);

            for(RolePackage rp: preferredRoles){
                roleDesirers = roleToPlayer.get(rp);
                if(roleDesirers == null){
                    roleDesirers = new PlayerList(player);
                    roleToPlayer.put(rp, roleDesirers);
                }else if(!player.in(roleDesirers))
                    roleDesirers.add(player);
            }
        }
        preferenceMap = null;

        Map<Player, RolePackage> assignedRoles = PlayerRoleService.giveRolesToPlayers(narrator, playerToRoles,
                roleToPlayer, releasedRoles);

        // safe to reorder the names
        narrator.getRandom().reset();
        for(Player p: narrator.players.sortByID())
            assignedRoles.get(p).assign(p);
    }
}

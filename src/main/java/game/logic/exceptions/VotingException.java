package game.logic.exceptions;

@SuppressWarnings("serial")
public class VotingException extends NarratorException {

    public VotingException(String string) {
        super(string);
    }

}

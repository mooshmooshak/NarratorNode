package game.logic.exceptions;

@SuppressWarnings("serial")
public class TeamSettingsException extends NarratorException {

    public TeamSettingsException(String name) {
        super(name);
    }

}

package game.logic.exceptions;

@SuppressWarnings("serial")
public class UnsupportedMethodException extends NarratorException {

    public UnsupportedMethodException(String role) {
        super(role);
    }

}

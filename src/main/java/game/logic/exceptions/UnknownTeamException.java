package game.logic.exceptions;

import game.logic.Player;

@SuppressWarnings("serial")
public class UnknownTeamException extends NarratorException {

    public UnknownTeamException(String string) {
        super(string);
    }

    public UnknownTeamException(Player owner, Player target, int ability) {
        this(owner.getName() + " is trying to target " + target.getName() + " using ability " + ability);
    }

}

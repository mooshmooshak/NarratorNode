package game.logic.exceptions;

@SuppressWarnings("serial")
public class NamingException extends NarratorException {

    public NamingException(String string) {
        super(string);
    }
}

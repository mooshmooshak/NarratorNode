package game.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.exceptions.NarratorException;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityType;
import services.SetupHiddenService;
import util.game.SetupHiddenUtil;

public class RolesList {

    public ArrayList<SetupHidden> _setupHiddenList;

    public RolesList() {
        _setupHiddenList = new ArrayList<>();
    }

    public int size(Game game) {
        return getSpawningSetupHiddens(game).size();
    }

    public int size(int playerCount) {
        return getSpawningSetupHiddens(playerCount).size();
    }

    public Set<FactionRole> getSpawns(int playerCount) {
        Set<FactionRole> factionRoleSpawns = new HashSet<>();
        for(SetupHidden setupHidden: this.iterable(playerCount))
            factionRoleSpawns.add(setupHidden.spawn);
        factionRoleSpawns.remove(null);
        return factionRoleSpawns;
    }

    public SetupHidden get(int i) {
        return _setupHiddenList.get(i);
    }

    public SetupHidden get(long id) {
        for(SetupHidden setupHidden: this._setupHiddenList){
            if(setupHidden.id == id)
                return setupHidden;
        }
        throw new NarratorException("Setup hidden with that ID not found: " + id);
    }

    public boolean remove(Hidden hidden) {
        Iterator<SetupHidden> iterator = _setupHiddenList.iterator();
        while (iterator.hasNext()){
            if(iterator.next().hidden == hidden){
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public void remove(SetupHidden setupHidden) {
        this._setupHiddenList.remove(setupHidden);
    }

    public void removeAll(Hidden hidden) {
        Iterator<SetupHidden> iterator = _setupHiddenList.iterator();
        while (iterator.hasNext()){
            if(iterator.next().hidden == hidden)
                iterator.remove();
        }
    }

    public boolean contains(Hidden hidden, int playerCount) {
        for(SetupHidden setupHidden: this.iterable(playerCount)){
            if(setupHidden.hidden == hidden)
                return true;
        }
        return false;
    }

    public boolean contains(AbilityType abilityType, int playerCount) {
        for(SetupHidden setupHidden: this.iterable(playerCount)){
            for(FactionRole factionRole: setupHidden.hidden.getAllFactionRoles()){
                if(factionRole.role.hasAbility(abilityType))
                    return true;
            }
        }
        return false;
    }

    public boolean contains(Role role, int playerCount) {
        for(SetupHidden setupHidden: this.iterable(playerCount)){
            if(setupHidden.hidden.contains(role))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(SetupHidden rt: _setupHiddenList){
            sb.append(rt.hidden.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    public void removeHead() {
        _setupHiddenList.remove(0);
    }

    public void clearSpawns() {
        for(SetupHidden setupHidden: this.iterable())
            SetupHiddenService.removeForcedSpawn(setupHidden);
    }

    @Override
    public int hashCode() {
        ArrayList<SetupHidden> copy = new ArrayList<>(_setupHiddenList);
        copy.sort(SetupHidden.RandomComparator());
        return copy.hashCode();
    }

    public Iterable<SetupHidden> iterable() {
        return new Iterable<SetupHidden>() {
            @Override
            public Iterator<SetupHidden> iterator() {
                return RolesList.this._setupHiddenList.iterator();
            }
        };
    }

    public Iterable<SetupHidden> iterable(Optional<Game> game) {
        if(game.isPresent())
            return iterable(game.get());
        return iterable();
    }

    public Iterable<SetupHidden> iterable(Game game) {
        return this.iterable(game.players.size());
    }

    public Iterable<SetupHidden> iterable(int playerCount) {
        return new Iterable<SetupHidden>() {
            @Override
            public Iterator<SetupHidden> iterator() {
                return RolesList.getSpawningSetupHiddens(playerCount, _setupHiddenList).iterator();
            }
        };
    }

    public Iterator<SetupHidden> iterator(int playerCount) {
        return getSpawningSetupHiddens(playerCount).iterator();
    }

    public List<SetupHidden> getSpawningSetupHiddens(Game game) {
        return getSpawningSetupHiddens(game.players.size());
    }

    public List<SetupHidden> getSpawningSetupHiddens(int playerCount) {
        return getSpawningSetupHiddens(playerCount, this._setupHiddenList);
    }

    public static List<SetupHidden> getSpawningSetupHiddens(int playerCount, List<SetupHidden> _setupHiddenList) {
        LinkedList<SetupHidden> spawning = new LinkedList<>();
        for(SetupHidden setupHidden: _setupHiddenList){
            if(SetupHiddenUtil.spawnable(playerCount, setupHidden))
                spawning.add(setupHidden);
        }
        return spawning;
    }
}

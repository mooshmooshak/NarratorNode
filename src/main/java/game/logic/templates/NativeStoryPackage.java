package game.logic.templates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import game.logic.support.RolePackage;
import game.logic.support.StoryPackage;
import game.logic.support.StoryReader;
import models.Command;
import models.enums.GameModifierName;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.modifiers.GameModifier;
import models.modifiers.Modifier;
import models.schemas.GameOverviewSchema;
import models.schemas.PlayerSchema;
import util.Util;

public class NativeStoryPackage implements StoryReader {

    public static GameID GAME_ID = new GameID(-1);
    public static long SETUP_ID = -1;

    Game inputGame;
    boolean copyIDs; // unused for now.

    public NativeStoryPackage(Game n, boolean copyIDs) {
        inputGame = n;
        this.copyIDs = copyIDs;
    }

    @Override
    public Collection<Modifier<GameModifierName>> getGameModifiers() {
        List<Modifier<GameModifierName>> ret = new LinkedList<>();
        for(Modifier<GameModifierName> modifier: inputGame.gameModifiers.modifiers)
            ret.add(new GameModifier(getID(modifier.id), modifier.name, modifier.value));

        return ret;
    }

    @Override
    public GameOverviewSchema getGameOverview() {
        return new GameOverviewSchema(GAME_ID, SETUP_ID, "", false, false, false, inputGame.getSeed());
    }

    @Override
    public Set<Long> getModeratorIDs() {
        return new HashSet<>();
    }

    @Override
    public Set<PlayerSchema> getPlayers() {
        Set<PlayerSchema> players = new HashSet<>();

        PlayerSchema pp;
        for(Player p: inputGame.getAllPlayers()){
            pp = new PlayerSchema(p.databaseID, GAME_ID, p.getName(), Optional.empty());
            players.add(pp);
        }

        return players;
    }

    @Override
    public ArrayList<Command> getCommands() {
        ArrayList<Command> commands = new ArrayList<>();
        for(Command s: inputGame.getCommands())
            commands.add(new Command(s.playerID, s.text, s.timeLeft));
        return commands;
    }

    public static StoryPackage deserialize(Game game) {
        return StoryPackage.deserialize(new NativeStoryPackage(game, true), game.setup, GAME_ID);
    }

    @Override
    public Map<Long, Long> getSetupHiddenSpawns() {
        Map<Long, Long> spawns = new HashMap<>();
        RolePackage rolePackage;
        for(Player player: inputGame.players){
            rolePackage = player.initialAssignedRole;
            spawns.put(rolePackage.assignedSetupHidden.id, rolePackage.assignedRole.id);
        }
        for(RolePackage unpickedPackage: inputGame.unpickedRoles)
            spawns.put(unpickedPackage.assignedSetupHidden.id, unpickedPackage.assignedRole.id);

        return spawns;
    }

    @Override
    public Map<PlayerDBID, Long> getPlayerRoles() {
        Map<PlayerDBID, Long> assignments = new HashMap<>();
        for(Player player: inputGame.players)
            assignments.put(player.databaseID, player.initialAssignedRole.assignedSetupHidden.id);

        return assignments;
    }

    private long getID(long id) {
        if(this.copyIDs)
            return id;
        return Util.getID();
    }

}

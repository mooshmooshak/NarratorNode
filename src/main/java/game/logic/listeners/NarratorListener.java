package game.logic.listeners;

import java.util.Optional;

import game.event.Announcement;
import game.event.ChatMessage;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.Message;
import game.event.SelectionMessage;
import game.event.VoteAnnouncement;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import json.JSONObject;
import models.enums.AbilityType;

public interface NarratorListener {

    void onAnnouncement(Message nl);

    void onAssassination(Player assassin, Player target, DeathAnnouncement e);

    void onCancelEndNight(Player p);

    void onChatMessageSend(ChatMessage message, Optional<String> source, JSONObject args);

    void onDayActionSubmit(Player submitter, Action a);

    void onDayBurn(Player arson, PlayerList burned, DeathAnnouncement e);

    void onDayPhaseStart(PlayerList newDead);

    void onElectroExplosion(PlayerList deadPeople, DeathAnnouncement explosion);

    void onEndNight(Player p, boolean forced);

    void onGameStart();

    void onGameEnd();

    void onMessageReceive(Player player, Message message);

    void onModKill(PlayerList bad);

    void onNightEnding();

    void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList events);

    void onRolepickingPhaseStart();

    void onRoleReveal(Player mayor, Message e);

    void onTargetRemove(Player owner, AbilityType abilityType, PlayerList prev);

    void onTargetSelection(Player owner, SelectionMessage selectionMessage);

    void onTrialStart(Player trailedPlayer, Announcement trialStartEvent);

    void onVote(Player voter, VoteAnnouncement e, double timeLeft);

    void onVoteCancel(Player voter, VoteAnnouncement e);

    void onVotePhaseReset(int resetsRemaining);

    void onVotePhaseStart();

    void onWarningReceive(Player player, Message message);
}

package game.logic;

import java.util.ArrayList;
import java.util.Iterator;

import models.Faction;

public class FactionList implements FactionGroup, Iterable<Faction> {

    private ArrayList<Faction> teams;

    public FactionList(Faction... input_teams) {
        this.teams = new ArrayList<>();
        for(Faction t: input_teams){
            teams.add(t);
        }
    }

    @Override
    public void setPriority(int i) {
        for(Faction faction: teams){
            faction.setPriority(i);
        }
    }

    public void add(Faction t) {
        teams.add(t);
    }

    public void add(FactionList teams) {
        for(Faction t: this)
            teams.add(t);
    }

    public boolean isEmpty() {
        return teams.isEmpty();
    }

    @Override
    public Iterator<Faction> iterator() {
        return teams.iterator();
    }

    public String getName() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < teams.size(); i++){
            if(i != 0)
                sb.append(", ");
            sb.append(teams.get(i).getName());
        }
        return sb.toString();
    }

    public int size() {
        return teams.size();
    }

    public Faction get(int i) {
        return teams.get(i);
    }

    public boolean contains(Faction team) {
        return teams.contains(team);
    }
}

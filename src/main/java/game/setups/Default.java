package game.setups;

import game.abilities.Agent;
import game.abilities.Amnesiac;
import game.abilities.Architect;
import game.abilities.Armorsmith;
import game.abilities.Assassin;
import game.abilities.Baker;
import game.abilities.Blackmailer;
import game.abilities.Blacksmith;
import game.abilities.Block;
import game.abilities.Bodyguard;
import game.abilities.Bomb;
import game.abilities.Bulletproof;
import game.abilities.Burn;
import game.abilities.Citizen;
import game.abilities.Clubber;
import game.abilities.Commuter;
import game.abilities.Coroner;
import game.abilities.Coward;
import game.abilities.CultLeader;
import game.abilities.Cultist;
import game.abilities.Detective;
import game.abilities.Disfranchise;
import game.abilities.Disguiser;
import game.abilities.Doctor;
import game.abilities.Driver;
import game.abilities.DrugDealer;
import game.abilities.Elector;
import game.abilities.ElectroManiac;
import game.abilities.Enforcer;
import game.abilities.Executioner;
import game.abilities.Framer;
import game.abilities.Ghost;
import game.abilities.Godfather;
import game.abilities.Goon;
import game.abilities.GraveDigger;
import game.abilities.Gunsmith;
import game.abilities.Hidden;
import game.abilities.Interceptor;
import game.abilities.Investigator;
import game.abilities.JailCreate;
import game.abilities.JailExecute;
import game.abilities.Janitor;
import game.abilities.Jester;
import game.abilities.Joker;
import game.abilities.Lookout;
import game.abilities.Marshall;
import game.abilities.Mason;
import game.abilities.MasonLeader;
import game.abilities.MassMurderer;
import game.abilities.Mayor;
import game.abilities.Miller;
import game.abilities.Operator;
import game.abilities.ParityCheck;
import game.abilities.Poisoner;
import game.abilities.SerialKiller;
import game.abilities.Sheriff;
import game.abilities.Silence;
import game.abilities.Sleepwalker;
import game.abilities.Snitch;
import game.abilities.Spy;
import game.abilities.Survivor;
import game.abilities.Tailor;
import game.abilities.Ventriloquist;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.abilities.Witch;
import game.logic.FactionList;
import game.logic.Game;
import game.logic.support.Constants;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.FactionService;
import services.HiddenService;
import services.HiddenSpawnService;
import services.RoleAbilityModifierService;
import services.RoleModifierService;
import services.RoleService;
import services.SetupModifierService;
import util.game.LookupUtil;

public class Default {

    public static void Default(Setup setup) {
        Faction benigns = Setup.Benign(setup);
        Faction town = Setup.Town(setup);
        Faction outcasts = Setup.Outcast(setup);
        Faction cult = Setup.Cult(setup);
        Faction mafia = Setup.Mafia(setup, 1);
        Faction yakuza = Setup.Mafia(setup, 2);
        Faction threat = Setup.Killer(setup);

        Setup.Prioritize(threat, new FactionList(mafia, yakuza), cult, outcasts, town, benigns);

        FactionService.addSheriffCheckable(town, mafia, yakuza, cult, threat);

        FactionService.addEnemies(town, outcasts, mafia, yakuza, cult, threat);
        FactionService.addEnemies(mafia, cult, yakuza, threat);
        FactionService.addEnemies(yakuza, cult, threat);
        FactionService.addEnemies(cult, threat);

        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.NIGHT_START);
        SetupModifierService.upsertModifier(setup, SetupModifierName.FOLLOW_GETS_ALL, false);

        /*
         * finished narrator editing
         *
         * creating string to random role
         */

        Role architectRole = Architect.template(setup);
        Role busDriverRole = Driver.template(setup);
        Role jailorRole = JailCreate.template(setup);
        RoleAbilityModifierService.upsert(jailorRole, JailExecute.abilityType, AbilityModifierName.CHARGES, 3);
        Role operatorRole = Operator.template(setup);
        Role stripperRole = Block.template(setup);

        FactionRole vigi = Vigilante.template(town);
        RoleAbilityModifierService.upsert(vigi.role, Vigilante.abilityType, AbilityModifierName.CHARGES, 2);
        FactionRole vet = Veteran.template(town);
        RoleAbilityModifierService.upsert(vet.role, Veteran.abilityType, AbilityModifierName.CHARGES, 3);

        Role marshallRole = RoleService.createRole(setup, "Marshall", Marshall.abilityType);
        RoleModifierService.upsertModifier(marshallRole, RoleModifierName.UNIQUE, true);
        RoleAbilityModifierService.upsert(marshallRole, Marshall.abilityType, AbilityModifierName.CHARGES, 1);
        FactionRole marshall = FactionRoleService.createFactionRole(town, marshallRole);

        Role commuterRole = RoleService.createRole(setup, "Commuter", Commuter.abilityType);
        RoleAbilityModifierService.upsert(commuterRole, Commuter.abilityType, AbilityModifierName.CHARGES, 3);
        FactionRole commuter = FactionRoleService.createFactionRole(town, commuterRole);

        Role masonLeaderRole = RoleService.createRole(setup, "Mason Leader", MasonLeader.abilityType);
        RoleAbilityModifierService.upsert(masonLeaderRole, MasonLeader.abilityType, AbilityModifierName.CHARGES, 4);
        RoleModifierService.upsertModifier(masonLeaderRole, RoleModifierName.UNIQUE, true);
        FactionRole masonLeader = FactionRoleService.createFactionRole(town, masonLeaderRole);

        FactionRole architectTown = FactionRoleService.createFactionRole(town, architectRole);
        FactionRole armorsmith = Armorsmith.template(town);
        FactionRole baker = Baker.template(town);
        FactionRole citizen = Citizen.template(town);
        FactionRole driverTown = FactionRoleService.createFactionRole(town, busDriverRole);
        FactionRole bodyguard = Bodyguard.template(town);
        FactionRole bulletproof = Bulletproof.template(town);
        FactionRole clubber = Clubber.template(town);
        FactionRole coroner = Coroner.template(town);
        FactionRole detective = Detective.template(town);
        FactionRole doctor = Doctor.template(town);
        FactionRole enforcer = Enforcer.template(town);
        FactionRole gunsmith = Gunsmith.template(town);
        FactionRole jailor = FactionRoleService.createFactionRole(town, jailorRole);
        FactionRole lookout = Lookout.template(town);
        FactionRole mayor = Mayor.template(town);
        FactionRole mason = Mason.template(town);
        FactionRole miller = Miller.template(town);
        FactionRole operator = FactionRoleService.createFactionRole(town, operatorRole);
        FactionRole parityCop = ParityCheck.template(town);
        FactionRole sheriff = Sheriff.template(town);
        FactionRole sleepwalker = Sleepwalker.template(town, citizen);
        FactionRole snitch = Snitch.template(town);
        FactionRole spy = Spy.template(town);
        FactionRole stripperTown = FactionRoleService.createFactionRole(town, stripperRole);

        Hidden townRandom = HiddenService.createHidden(setup, Constants.TOWN_RANDOM_ROLE_NAME, architectTown, baker,
                bodyguard, bulletproof, citizen, clubber, commuter, coroner, detective, doctor, driverTown, enforcer,
                lookout, miller, parityCop, sheriff, sleepwalker, snitch, spy, operator, stripperTown, armorsmith, vigi,
                vet, gunsmith, jailor, mayor, masonLeader, marshall, mason);

        HiddenService.createHidden(setup, Constants.TOWN_KILLING_ROLE_NAME, vigi, bodyguard, jailor, vet, gunsmith);

        HiddenService.createHidden(setup, Constants.TOWN_INVESTIGATIVE_ROLE_NAME, sheriff, lookout, coroner, detective,
                spy);

        HiddenService.createHidden(setup, Constants.TOWN_GOVERNMENT_ROLE_NAME, mayor, marshall, masonLeader,
                architectTown, baker, snitch, mason, enforcer, clubber);

        HiddenService.createHidden(setup, Constants.TOWN_PROTECTIVE_ROLE_NAME, doctor, driverTown, operator,
                stripperTown, armorsmith, bodyguard);

        Role gfRole = RoleService.createRole(setup, "Godfather", Godfather.abilityType, Bulletproof.abilityType);
        RoleModifierService.upsertModifier(gfRole, RoleModifierName.UNDETECTABLE, true);
        FactionRole godfather = FactionRoleService.createFactionRole(mafia, gfRole);

        int tailorCharges = 3;
        FactionRole tailor = Tailor.template(mafia);
        RoleAbilityModifierService.upsert(tailor.role, Tailor.abilityType, AbilityModifierName.CHARGES, tailorCharges);

        int cowardCharges = 3;
        FactionRole coward = Coward.template(mafia);
        RoleAbilityModifierService.upsert(coward.role, Coward.abilityType, AbilityModifierName.CHARGES, cowardCharges);

        int disguiserCharges = 1;
        FactionRole disguiser = Disguiser.template(mafia);
        RoleAbilityModifierService.upsert(disguiser.role, Disguiser.abilityType, AbilityModifierName.CHARGES, 1);

        FactionRole[] mafiaRoles = new FactionRole[] { Assassin.template(mafia), Agent.template(mafia),
                Blackmailer.template(mafia), Disfranchise.template(mafia), DrugDealer.template(mafia),
                Framer.template(mafia), Goon.template(mafia), Investigator.template(mafia), Janitor.template(mafia),
                Silence.template(mafia),

                godfather, tailor, coward, disguiser,

                FactionRoleService.createFactionRole(mafia, architectRole),
                FactionRoleService.createFactionRole(mafia, busDriverRole),
                FactionRoleService.createFactionRole(mafia, jailorRole),
                FactionRoleService.createFactionRole(mafia, stripperRole) };

        Hidden maf_random = HiddenService.createHidden(setup, Constants.MAFIA_RANDOM_ROLE_NAME, mafiaRoles);

        FactionRole[] yakuzaRoles = new FactionRole[mafiaRoles.length];
        for(int i = 0; i < mafiaRoles.length; i++)
            yakuzaRoles[i] = FactionRoleService.createFactionRole(yakuza, mafiaRoles[i].role);

        FactionRole yakTailor = LookupUtil.findFactionRole(setup, tailor.getName(), yakuza.color).get();
        FactionRoleModifierService.addModifier(yakTailor, AbilityModifierName.CHARGES, Tailor.abilityType,
                tailorCharges);

        FactionRole yakCoward = LookupUtil.findFactionRole(setup, coward.getName(), yakuza.color).get();
        FactionRoleModifierService.addModifier(yakCoward, AbilityModifierName.CHARGES, Coward.abilityType,
                cowardCharges);

        FactionRole yakDisguiser = LookupUtil.findFactionRole(setup, disguiser.getName(), yakuza.color).get();
        FactionRoleModifierService.addModifier(yakDisguiser, AbilityModifierName.CHARGES, Disguiser.abilityType,
                disguiserCharges);

        Hidden yak_random = HiddenService.createHidden(setup, Constants.YAKUZA_RANDOM_ROLE_NAME, yakuzaRoles);

        FactionRole surv = Survivor.template(benigns);
        RoleAbilityModifierService.upsert(surv.role, Survivor.abilityType, AbilityModifierName.CHARGES, 4);

        FactionRole exec = Executioner.template(benigns);

        Hidden neut_benign = HiddenService.createHidden(setup, Constants.NEUTRAL_BENIGN_RANDOM_ROLE_NAME,
                Amnesiac.template(benigns), Ghost.template(benigns), Jester.template(benigns), exec, surv);

        Role blacksmithRole = RoleService.createRole(setup, "Blacksmith", Blacksmith.abilityType);
        FactionRole evilBlacksmith = FactionRoleService.createFactionRole(outcasts, blacksmithRole);
        FactionRoleModifierService.addModifier(evilBlacksmith, AbilityModifierName.AS_FAKE_VESTS,
                Blacksmith.abilityType, true);
        FactionRoleModifierService.addModifier(evilBlacksmith, AbilityModifierName.GS_FAULTY_GUNS,
                Blacksmith.abilityType, true);

        Role cultist = RoleService.createRole(setup, "Cultist", Cultist.abilityType);
        Role cultLeaderRole = RoleService.createRole(setup, "Cult Leader", CultLeader.abilityType);
        RoleAbilityModifierService.upsert(cultLeaderRole, CultLeader.abilityType, AbilityModifierName.COOLDOWN, 1);
        FactionRole cultLeader = FactionRoleService.createFactionRole(cult, cultLeaderRole);
        FactionRoleModifierService.upsertModifier(cultLeader, RoleModifierName.UNIQUE, true);

        Hidden neut_evil = HiddenService.createHidden(setup, Constants.NEUTRAL_EVIL_RANDOM_ROLE_NAME,
                Elector.template(outcasts), GraveDigger.template(outcasts), Interceptor.template(outcasts),
                Ventriloquist.template(outcasts), Witch.template(outcasts),

                evilBlacksmith, cultLeader, FactionRoleService.createFactionRole(cult, cultist),
                FactionRoleService.createFactionRole(outcasts, operatorRole));

        Hidden neut_killing = HiddenService.createHidden(setup, Constants.NEUTRAL_KILLING_RANDOM_ROLE_NAME,
                SerialKiller.template(threat), Burn.template(threat), MassMurderer.template(threat),
                Poisoner.template(threat), ElectroManiac.template(threat), Joker.template(threat));

        Hidden neut_random = HiddenService.createHidden(setup, Constants.NEUTRAL_RANDOM_ROLE_NAME);
        HiddenSpawnService.addSpawnableRoles(neut_random, neut_killing.getAllFactionRoles());
        HiddenSpawnService.addSpawnableRoles(neut_random, neut_benign.getAllFactionRoles());
        HiddenSpawnService.addSpawnableRoles(neut_random, neut_evil.getAllFactionRoles());

        Hidden any_rand = HiddenService.createHidden(setup, Constants.ANY_RANDOM_ROLE_NAME);
        HiddenSpawnService.addSpawnableRoles(any_rand, townRandom.getAllFactionRoles());
        HiddenSpawnService.addSpawnableRoles(any_rand, maf_random.getAllFactionRoles());
        HiddenSpawnService.addSpawnableRoles(any_rand, yak_random.getAllFactionRoles());
        HiddenSpawnService.addSpawnableRoles(any_rand, neut_random.getAllFactionRoles());

        Bomb.template(town);
        FactionRoleService.createFactionRole(town, blacksmithRole);
    }
}

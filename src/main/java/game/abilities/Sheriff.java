package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.support.HTString;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.FactionUtil;
import util.game.GameUtil;

public class Sheriff extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Sheriff;
    public static final String COMMAND = abilityType.command;

    public Sheriff(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone to determine if they are suspicious or not.";

    public String n0ClearName;

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    public static final String NOT_SUSPICIOUS = "Your target is not suspicious.";

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner, target = action.getTarget();
        if(target == null)
            return;

        GameFaction sheriffFaction = owner.game.getFaction(owner.getInitialColor());

        GameFaction targetGameFaction = target.getGameFaction();

        FactionRole status = target.getFrameStatus();
        if(status != null)
            targetGameFaction = owner.game.getFaction(status.getColor());

        if(!target.isDetectable())
            targetGameFaction = null;
        else if(!sheriffFaction.faction.enemies.contains(targetGameFaction.faction))
            targetGameFaction = null;
        else if(!owner.game.getBool(SetupModifierName.CHECK_DIFFERENTIATION)
                && !sheriffFaction.isEnemy(targetGameFaction))
            targetGameFaction = null;

        new Feedback(owner).add(generateFeedback(targetGameFaction, owner.game)).setPicture("lookout")
                .addExtraInfo("As a reminder, you attempted to check " + action.getIntendedTarget() + ".");

        happening(owner, " checked ", target);
        action.markCompleted();
        owner.visit(target);
    }

    public static ArrayList<Object> generateFeedback(GameFaction faction, Game narrator) {
        ArrayList<Object> list = new ArrayList<Object>();
        if(faction == null){
            list.add(NOT_SUSPICIOUS);
            return list;
        }

        if(narrator.getBool(SetupModifierName.CHECK_DIFFERENTIATION)){
            if(faction.knowsTeam())// this is just for text
                list.add("Your target is a member of the ");
            else
                list.add("Your target is a ");
            list.add(new HTString(faction));
        }else{
            list.add("Your target is suspicious");
        }

        list.add(".");
        return list;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.CHECK_DIFFERENTIATION);
        names.add(SetupModifierName.SHERIFF_PREPEEK);

        return names;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player player) {
        ArrayList<String> ret = new ArrayList<>();

        boolean knowsDifference = player.game.getBool(SetupModifierName.CHECK_DIFFERENTIATION);

        if(player.getGameFaction().faction.sheriffCheckables.size() == 1){

        }else if(knowsDifference)
            ret.add("You will be able to tell the difference between targets.");
        else
            ret.add("You can only tell if a target is suspicious or not.");
        ret.add(getDetectText(Optional.of(player.game), player.game.setup, player.getInitialColor(),
                Optional.of(player)));

        if(this.n0ClearName != null)
            ret.add(n0ClearName + " started the game off as your ally.");
        else
            ret.add("You do not get a pregame check.");

        return ret;
    }

    private static String getDetectText(Optional<Game> game, Setup setup, String color, Optional<Player> player) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        boolean knowsDifference = setup.modifiers.getBoolean(SetupModifierName.CHECK_DIFFERENTIATION, playerCount);
        Faction sheriffFaction = setup.getFactionByColor(color);
        ArrayList<Faction> checkableFactions = new ArrayList<>();

        Faction faction;
        for(Faction checkableFaction: sheriffFaction.sheriffCheckables){
            if(knowsDifference || sheriffFaction.enemies.contains(checkableFaction)){
                faction = setup.getFactionByColor(checkableFaction.color);
                checkableFactions.add(faction);
            }
        }

        if(checkableFactions.isEmpty()){
            return ("Unfortunately, the host created a ridiculous save that doesn't allow you to do anything.  Please report this individual.");
        }
        StringBuffer sb = new StringBuffer();
        if(player.isPresent()){
            sb.append("You");
        }else{
            Set<FactionRole> sMembers = setup.getPossibleFactionRolesWithAbility(game, Sheriff.abilityType);
            if(sMembers.isEmpty())
                sb.append("Sheriffs");
            else
                sb.append(sMembers.iterator().next().getName());
        }

        if(checkableFactions.size() == 1){
            sb.append(" can detect who " + checkableFactions.iterator().next().getName() + " are.");
        }else{
            checkableFactions.sort(FactionUtil.nameComparator);
            sb.append(" can detect the following team(s): ");
            for(Faction detectable: checkableFactions){
                sb.append(detectable.getName());
                sb.append(", ");
            }
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.toString();
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> color, Set<Ability> abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(game, actionOriginatorName, color, abilities);
        if(color.isPresent())
            ruleTextList.add(getDetectText(game, setup, color.get(), Optional.empty()));
        return ruleTextList;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public void onGameStart(Player sheriff) {
        super.onGameStart(sheriff);
        if(!sheriff.game.getBool(SetupModifierName.SHERIFF_PREPEEK))
            return;
        Player n0Clear = sheriff.getGameFaction().getMembers().remove(sheriff).getRandom(sheriff.game.getRandom());
        if(n0Clear == null)
            return;
        n0ClearName = n0Clear.getName();

        Happening e = new Happening(sheriff.game);

        StringChoice sc = new StringChoice(sheriff);
        sc.add(sheriff, "Your");
        e.add(sc);

        e.add(sc, " pregame clear is ", n0Clear, ".");
        e.setPrivate();
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return setup.modifiers.getBoolean(SetupModifierName.SHERIFF_PREPEEK, playerCount)
                || super.isNightless(playerCount);
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Sheriff", abilityType);
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }
}

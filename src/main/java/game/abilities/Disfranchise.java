package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Announcement;
import game.event.AnnouncementCheckCondition;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Disfranchise extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Disfranchise;
    public static final String COMMAND = abilityType.command;

    public Disfranchise(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String FEEDBACK = "You find your voting privileges have been revoked today.";
    public static final String NIGHT_ACTION_DESCRIPTION = "Stop players from voting.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        setDisfranchised(owner, target);

        a.markCompleted();
        owner.visit(target);
    }

    static void setDisfranchised(Player owner, final Player target) {
        target.setDisfranchised();
        Feedback f = new Feedback(target, FEEDBACK);
        f.setPicture("blackmailer");
        f.addExtraInfo("You may not vote today.");
        f.hideableFeedback = false;
        happening(owner, " disfranchised ", target);

        if(announcedDisfranchises(owner)){
            Announcement announcement = new Announcement(owner.game);
            announcement.add(target, " may not vote today.");
            final int disfranchiseDay = owner.game.getDayNumber() + 1;
            announcement.setCheckCondition(new AnnouncementCheckCondition() {
                @Override
                public boolean isAnnounceable() {
                    if(target.isAlive())
                        return true;
                    return target.getDeathDay() > disfranchiseDay;
                }
            });
            owner.game.announcement(announcement);
        }
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Vote Remover", Disfranchise.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    private static boolean announcedDisfranchises(Player disfranchiser) {
        Modifiers<AbilityModifierName> modifierNames;
        Game game = disfranchiser.game;
        if(disfranchiser.hasAbility(Disfranchise.abilityType)){
            modifierNames = disfranchiser.getAbility(Disfranchise.class).modifiers;
            if(modifierNames.getBoolean(AbilityModifierName.DISFRANCHISED_ANNOUNCED, game))
                return true;
        }
        if(!disfranchiser.hasAbility(Blackmailer.abilityType))
            return false;
        modifierNames = disfranchiser.getAbility(Blackmailer.class).modifiers;
        return modifierNames.getBoolean(AbilityModifierName.DISFRANCHISED_ANNOUNCED, game);
    }
}

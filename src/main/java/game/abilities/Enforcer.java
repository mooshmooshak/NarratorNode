package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Enforcer extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Enforcer;
    public static final String COMMAND = abilityType.command;

    public Enforcer(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "An acolyte of the masonry, prevent someone from being recruited.";
    public static final String ROLE_NAME = "Mason Enforcer";
    public static final String FEEDBACK = " tried to recruit your target.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        MasonLeader.MasonryCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        Player enforcer = a.owner;
        if(target == null)
            return;

        target.setEnforced(enforcer);

        happening(enforcer, " enforced ", target);
        a.markCompleted();
        enforcer.visit(target);

        return;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.ENFORCER_LEARNS_NAMES);
        return names;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.ENFORCER_LEARNS_NAMES)){
            ret.add("You will learn the names of everyone that attempts to convert your target.");
        }else{
            ret.add("YOu will not know who tries to convert your target.");
        }

        return ret;
    }

    public static ArrayList<Object> FeedbackGenerator(Player cultLeader) {
        ArrayList<Object> parts = new ArrayList<>();
        parts.add(cultLeader);
        parts.add(FEEDBACK);
        return parts;
    }

    public static void addFeedback(Player enforcer, Player cultLeader) {
        if(enforcer.game.getBool(SetupModifierName.ENFORCER_LEARNS_NAMES)){
            Feedback f = new Feedback(enforcer);
            f.setPicture("lookout");
            f.add(FeedbackGenerator(cultLeader));
        }

    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Enforcer", Enforcer.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}

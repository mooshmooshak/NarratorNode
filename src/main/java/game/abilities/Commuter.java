package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Happening;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class Commuter extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Commuter;
    public static final String COMMAND = abilityType.command;
    public static final String COMMAND_LOWERCASE = COMMAND.toLowerCase();

    public Commuter(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Leave town at night, making you untargetable.";

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        if(getPerceivedCharges() != 0)
            return null;
        return new PlayerList();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(" leave the town");
        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("leaving the town");
        return list;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public void mainAbilityCheck(Action a) {

    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 0)
            AbilityValidationUtil.Exception("You can't target players with this ability");
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        owner.setAddress(null);

        // completedNightAction = true;

        Message e = new Happening(a.owner.game);
        e.add(owner, " commuted away from the game.");
        a.markCompleted();
    }

    @Override
    protected String getProfileChargeText() {
        int charges = getPerceivedCharges();
        if(charges == Constants.UNLIMITED){
            return ("You can commute as many times as you want.");
        }else if(charges == 0){
            return ("You are unable to commute anymore.");
        }else if(charges == 1){
            return ("You can commute away from the game 1 more time.");
        }else{
            return ("You can commute away from the game " + charges + " more times.");
        }
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.of(new Action(owner, abilityType));
    }

    public static boolean CommutingTargets(Action a) {
        Player commuter = a.getActualTargets().getFirst();
        if(commuter == null)
            return false;
        if(!commuter.is(Commuter.abilityType))
            return false;
        return !commuter.getActions().getActions(Commuter.abilityType).isEmpty();
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }
}

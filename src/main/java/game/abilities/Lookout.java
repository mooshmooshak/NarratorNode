package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Lookout extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Lookout;
    public static final String COMMAND = abilityType.command;

    public Lookout(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Find out all who visit someone.";

    // private boolean visited = false;
    public static final String FEEDBACK = "These are the people who visited your target: ";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(owner.game.pre_visiting_phase){
            owner.visit(target);
            return;
        }
        a.markCompleted();
        watch(owner, target, a.getIntendedTarget());
        happening(owner, " watched ", target);
    }

    public static final String NO_VISIT = "No one visited your target";

    public static void watch(Player owner, Player target, String ogTarget) {
        PlayerList visitors = target.getVisitors("Lookout");// this is copied.
        for(int i = 0; i < visitors.size(); i++){
            if(!visitors.get(i).isDetectable()){
                visitors.remove(i);
                i--;
            }
        }
        visitors.remove(owner);
        visitors.sortByName();
        ArrayList<Object> parts = FeedbackGenerator(visitors);
        Message m = new Feedback(owner).add(parts).setPicture("lookout");

        m.addExtraInfo("As a reminder, you attempted to follow " + ogTarget + ".");
    }

    public static ArrayList<Object> FeedbackGenerator(PlayerList visitors) {
        ArrayList<Object> parts = new ArrayList<>();
        if(visitors.isEmpty())
            parts.add(NO_VISIT);
        else{
            parts.add(FEEDBACK);
            for(Player p: visitors){
                parts.add(p);
                if(visitors.getLast() != p)
                    parts.add(", ");
            }
            parts.add(".");
        }
        return parts;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Lookout", Lookout.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

}

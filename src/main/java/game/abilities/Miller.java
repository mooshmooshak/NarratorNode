package game.abilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class Miller extends Passive {

    public static final AbilityType abilityType = AbilityType.Miller;

    public Miller(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    private FactionRole flip;

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void onNightStart(Player player) {
        super.onNightStart(player);
        Game game = player.game;
        String millerColor = MillerColor(game);

        Faction faction = setup.getFactionByColor(millerColor);
        player.setFramed(new FactionRole(Util.getID(), faction, player.gameRole.factionRole.role));
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> nullableColor, Set<Ability> abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(game, actionOriginatorName, nullableColor,
                abilities);
        Optional<String> color = Miller.MillerColor(game, setup);
        Set<FactionRole> sheriffRoles = setup.getPossibleFactionRolesWithAbility(game, Sheriff.abilityType);
        if(color.isPresent() && !sheriffRoles.isEmpty()){
            String sheriffName = sheriffRoles.iterator().next().getName();
            ruleTextList.add("Will turn up as " + setup.getFactionByColor(color.get()) + " to a " + sheriffName + ".");
        }
        return ruleTextList;
    }

    public static String MillerColor(Game game) {
        return MillerColor(Optional.of(game), game.setup).get();
    }

    public static Optional<String> MillerColor(Optional<Game> game, Setup setup) {
        List<SetupHidden> rolesList = game.isPresent() ? setup.rolesList.getSpawningSetupHiddens(game.get())
                : new LinkedList<>(setup.rolesList._setupHiddenList);
        Set<Faction> factions = setup.getSpawnableFactions(game);
        if(factions.isEmpty())
            return Optional.empty();

        for(Faction faction: new HashSet<>(factions)){
            if(faction.hasPossibleRolesWithAbility(game, Miller.abilityType))
                factions.remove(faction);
        }

        if(factions.isEmpty())
            factions = setup.getSpawnableFactions(game);

        ArrayList<Faction> factionList = new ArrayList<>(factions);
        Collections.sort(factionList, new Comparator<Faction>() {
            private int teamSize(Faction t) {
                int i = 0;
                for(SetupHidden setupHidden: rolesList){
                    if(setupHidden.hidden.getColors().size() == 1)
                        if(setupHidden.hidden.getColors().contains(t.getColor()))
                            i++;
                }
                if(game.isPresent()){
                    for(Player player: game.get().getDeadPlayers()){
                        if(player.getDeathType().isHidden())
                            continue;
                        else if(player.hasSuits()){
                            if(player.suits.get(0).factionRole.faction == t)
                                i--;
                        }else if(player.getColor().equals(t.getColor()))
                            i--;
                    }
                }
                return i;
            }

            @Override
            public int compare(Faction o1, Faction o2) {
                if(!o1.abilities.isEmpty() && !o2.abilities.isEmpty())
                    return teamSize(o2) - teamSize(o1);
                if(!o2.abilities.isEmpty())
                    return -1;
                if(!o1.abilities.isEmpty())
                    return 1;
                int size1 = teamSize(o1);
                int size2 = teamSize(o2);
                if(size1 == size2)
                    return o1.getName().compareTo(o2.getName());
                return size1 - size2;
            }

        });
        return Optional.of(factionList.get(0).getColor());
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Looks evil to investigative actions";

    @Override
    protected ArrayList<String> getProfileHints(Player player) {
        ArrayList<String> ret = new ArrayList<>();

        String color = MillerColor(narrator);
        GameFaction t = narrator.getFaction(color);
        if(narrator.getBool(SetupModifierName.MILLER_SUITED))
            ret.add("On your death, you will look like a " + t.getName() + " member.");
        else
            ret.add("On your death, you'll look normal.");

        Set<FactionRole> factionRoles = setup.getPossibleFactionRolesWithAbility(player.game, Sheriff.abilityType);
        if(factionRoles.isEmpty())
            return ret;

        String sheriffName = factionRoles.iterator().next().getName();
        ret.add("You will turn up as " + player.game.getFaction(color).getName() + " to " + sheriffName + "s.");

        return ret;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.MILLER_SUITED);
        return names;
    }

    @Override
    public void onDeath(Player p) {
        super.onDeath(p);
        if(!narrator.getBool(SetupModifierName.MILLER_SUITED))
            return;
        Faction faction = narrator.setup.getFactionByColor(MillerColor(p.game));
        List<FactionRole> members = new LinkedList<>(faction.getFactionRoles());
        int index = p.game.getRandom().nextInt(members.size());
        flip = members.get(index);
    }

    public static FactionRole getRoleFlip(Player p) {
        Miller m = p.getAbility(Miller.class);
        return m.flip;
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    @Override
    public boolean isHiddenModifiable() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Miller", abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

}

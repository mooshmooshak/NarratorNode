package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.GameModifiers;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Janitor extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Janitor;
    public static final String COMMAND = abilityType.command;

    public Janitor(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Hide the role of a person from being revealed to everyone.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.JANITOR_GETS_ROLES);
        return names;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        target.setCleaned(a.owner);
        happening(owner, " cleaned ", target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        if(p.game.getBool(SetupModifierName.JANITOR_GETS_ROLES))
            ret.add("You will uncover the roles of your successful cleans.");

        return ret;
    }

    public static void Reveal(Player jan, Player player) {
        if(!jan.is(Janitor.abilityType))
            return;

        Investigator.Feedback(jan, player, null, true);
    }

    public static void SendOutFeedback(Player p) {
        if(p.getCleaners() == null)
            return;
        for(Player jan: p.getCleaners()){
            if(p.game.getBool(SetupModifierName.JANITOR_GETS_ROLES))
                Janitor.Reveal(jan, p);
            if(jan.is(Janitor.abilityType))
                jan.getAbility(Janitor.abilityType).useCharge().triggerCooldown();
        }
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static boolean isBeingCleaned(Player p) {
        boolean janitorCleaned = p.getCleaners() != null && !p.getCleaners().isEmpty();
        GameModifiers<RoleModifierName> modifiers = p.gameRole.modifiers;
        boolean attributeCleaned = modifiers.getOrDefault(RoleModifierName.HIDDEN_FLIP, false);
        return janitorCleaned || attributeCleaned;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Janitor", Janitor.abilityType);
    }

}

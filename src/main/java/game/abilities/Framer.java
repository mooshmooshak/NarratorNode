package game.abilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class Framer extends GameAbility {
    public static final AbilityType abilityType = AbilityType.Framer;
    public static final String COMMAND = abilityType.command;

    public Framer(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public ArrayList<Option> getOptions(Player player) {
        return getOptions(player.game);
    }

    public static ArrayList<Option> getOptions(Game game) {
        ArrayList<Option> availableTeams = new ArrayList<>();
        for(GameFaction t: game.getFactions())
            availableTeams.add(new Option(t));

        return availableTeams;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Frame other people for murder, changing most investigation results on them.";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();
        Action a = actionList.get(0);
        Player owner = a.owner;

        for(Action act: actionList){
            Player target = act.getTarget();
            list.add(COMMAND.toLowerCase());
            list.add(" ");
            list.add(StringChoice.YouYourselfSingle(target));
            GameFaction parse = owner.game.getFaction(act.getArg1());
            if(parse == null)
                parse = owner.game.getFactionByName(act.getArg1());
            if(parse != null){
                list.add(" as " + parse.getName());
            }
            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        String input = a.getArg1();
        Player owner = a.owner;
        GameFaction t = owner.game.getFaction(input);
        if(t != null)
            return;

        t = owner.game.getFactionByName(input);
        if(t != null)
            return;

        throw new UnknownTeamException("Unknown team: " + input);
    }

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner, target = action.getTarget();
        if(target == null)
            return;
        if(action.getArg1() == null){
            Visit.NoNightActionVisit(action);
            return;
        }
        Faction framedFaction = narrator.setup.getFactionByColor(action.getArg1());
        Player sender;
        for(GameFaction gameFaction: owner.getFactions()){
            if(!gameFaction.hasSharedAbilities()){
                continue;
            }
            for(GameAbility tAbility: gameFaction.getAbilities()){
                sender = gameFaction.getFactionAbilityController(tAbility.getAbilityType());
                if(sender == null)
                    continue;

                for(Action senderAction: sender.getActions().getActions()){
                    if(senderAction.is(tAbility.getAbilityType()) && senderAction.isCompleted()){
                        // won't trigger the electro here.
                        target.visit(Player.FAKE_VISIT, senderAction.getTarget());
                    }
                }
            }

        }

        List<FactionRole> tMembers = new ArrayList<>(framedFaction.getPossibleFactionRoles(narrator));
        if(tMembers.isEmpty())
            tMembers = new ArrayList<>(framedFaction.getFactionRoles());
        if(tMembers.isEmpty())
            return;

        int elem = narrator.getRandom().nextInt(tMembers.size());

        FactionRole factionRole = tMembers.get(elem);
        target.setFramed(factionRole);

        new Happening(narrator).add(owner, " framed ", target, " as ", new HTString(factionRole));
        action.markCompleted();
        owner.visit(target);
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getTarget().getName());
        parsedCommands.add(a.getArg1());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {

        String overflow, toAdd;
        while (commands.size() > 3){
            overflow = commands.remove(3);
            toAdd = commands.remove(2);
            commands.add(2, toAdd + overflow);
        }

        if(commands.size() != 3){
            throw new PlayerTargetingException("You need a player target and a team name to frame.");
        }
        commands.remove(0);
        Game n = p.game;

        String playerName = commands.remove(0);
        Player pi = n.getPlayerByName(playerName);
        if(pi == null)
            throw new UnknownPlayerException(playerName);
        String frame = commands.get(0);
        String teamColor = CommandHandler.parseTeam(frame, n);
        if(Constants.A_INVALID.equals(teamColor))
            throw new UnknownTeamException(frame);

        return new Action(p, abilityType, timeLeft, Util.toStringList(frame), Player.list(pi));
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> playerList, String actionOriginatorName,
            Optional<String> color, Set<Ability> abilityList) {
        ArrayList<String> list = super.getPublicDescription(playerList, actionOriginatorName, color, abilityList);
        list.add("May choose to frame as any alignment");

        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("framing ");
        return list;
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        String ret = super.getUsage(p, r) + " " + options.get(index);
        if(options.size() != 1){
            ret += "\nAll frameable teams are are: ";
            for(int i = 0; i < options.size(); i++){
                ret += options.get(i);
                if(i != options.size() - 1){
                    ret += ", ";
                }
            }
        }
        return ret;
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.of(new Action(owner, abilityType, owner.getColor(), (String) null, owner));
    }

    @Override
    public Optional<Action> getExampleWitchAction(Player owner, Player target) {
        return Optional.of(new Action(owner, Framer.abilityType, owner.getColor(), target));
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Framer", Framer.abilityType);
    }

    public static Action getAction(Controller framer, Controller target, String factionColor, Game game) {
        return new Action(framer.getPlayer(), abilityType, Util.toStringList(factionColor),
                ControllerList.ToPlayerList(game, target));
    }
}

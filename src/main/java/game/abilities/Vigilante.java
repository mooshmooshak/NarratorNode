package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;

import game.abilities.support.Charge;
import game.abilities.support.Gun;
import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.ActionUtil;
import util.game.GameUtil;

public class Vigilante extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Vigilante;
    public static final String COMMAND = Gun.COMMAND;

    public Vigilante(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    private boolean usedGunToday = false;

    public static final String NIGHT_ACTION_DESCRIPTION = "Takes justice into own hands and kills people at night.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("gunning down ");
        list.add(StringChoice.YouYourselfSingle(a.getTarget()));
        return list;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        Player owner = actions.get(0).owner;
        PlayerList targets = ActionUtil.getActionTargets(actions);

        ArrayList<Object> list = new ArrayList<>();
        list.add(Gun.getGunName(owner.game) + " down ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        return "use a " + Gun.getGunName(playerCount, setup) + " to attack someone.";
    }

    @Override
    public void mainAbilityCheck(Action a) {
        Player owner = a.owner;
        Game n = owner.game;
        if(a.getTargets().size() != 1)
            AbilityValidationUtil.Exception("You must select one and only one target");
        AbilityValidationUtil.deadCheck(a);
        if(owner.game.getBool(SetupModifierName.GS_DAY_GUNS)){
            boolean isDay = owner.game.isDay();
            Gun gun;
            for(Charge charge: this.charges){
                gun = (Gun) charge;
                if(gun.getFlag() == Constants.GUN_KILL_FLAG){
                    if(isDay)
                        return;
                }else{
                    if(!isDay)
                        return;
                }
            }
            if(isDay)
                AbilityValidationUtil
                        .Exception("You don't have any " + Gun.getGunName(n) + "s given to you by a gunsmith");
            else
                AbilityValidationUtil.Exception("You don't have any " + Gun.getGunName(n) + "s you can use right now.");
        }
        return;
    }

    @Override
    public void checkPerformDuringDay(Player owner) {
        Game n = owner.game;
        if(!n.getBool(SetupModifierName.GS_DAY_GUNS))
            throw new IllegalActionException("Cannot shoot guns during the day.");
        if(usedGunToday)
            throw new IllegalActionException("Cannot shoot guns twice");
    }

    public static final String DEATH_FEEDBACK = "You were killed by a Vigilante!";

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        if(target == null)
            return;

        Gun g;
        if(this.charges.isUnlimited())
            g = new Gun();
        else
            g = (Gun) this.charges.get(0);
        g.shoot(a);
        a.markCompleted();
    }

    @Override
    public void doDayAction(Action action, Action parentAction) {
        Gun g;
        for(Charge charge: this.charges){
            g = (Gun) charge;
            if(g.getFlag() == Constants.GUN_KILL_FLAG){
                g.shoot(action);
                this.charges.remove(charge);
                usedGunToday = true;
                action.owner.game.parityCheck();
                return;
            }
        }
        throw new IllegalActionException("No guns to shoot");
    }

    @Override
    public boolean isNightAbility(Player p) {
        if(this.charges.isUnlimited())
            return true;
        for(Gun gun: this.charges.iterable(Gun.class)){
            if(gun.getFlag() == Constants.VIGILANTE_KILL_FLAG)
                return true;
            if(!p.game.getBool(SetupModifierName.GS_DAY_GUNS))
                return true;
        }

        return false;
    }

    @Override
    public void onDayEnd(Player p) {
        super.onDayEnd(p);
        usedGunToday = false;
    }

    @Override
    public boolean isOnCooldown() {
        if(player.game.isDay()){
            if(!player.game.getBool(SetupModifierName.GS_DAY_GUNS) || usedGunToday)
                return false;
        }

        return super.isOnCooldown();
    }

    public static boolean isVigiGun(Gun g) {
        return g.getFlag() == Constants.VIGILANTE_KILL_FLAG;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(!p.game.getBool(SetupModifierName.GS_DAY_GUNS))
            return false;

        Gun g;
        for(Charge charge: this.charges){
            g = (Gun) charge;
            if(g.isReal() && !Vigilante.isVigiGun(g))
                return true;
        }
        return false;
    }

    @Override
    public String getRuleChargeText(Optional<Integer> game, int chargeCount) {
        int chargeVariability = setup.modifiers.getInt(SetupModifierName.CHARGE_VARIABILITY, game);
        StringBuilder sb = new StringBuilder("Starts off with ");
        if(chargeVariability != 0)
            sb.append("~");
        sb.append(chargeCount);
        sb.append(" ");
        sb.append(Gun.getGunName(game, setup));
        if(chargeCount != 1)
            sb.append("s");
        return sb.toString();
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> specs = new ArrayList<>();
        String gunName = Gun.getGunName(p.game);
        if(!this.charges.hasCharges())
            specs.add("You've used up all your " + gunName + "s.");
        else if(this.charges.isUnlimited())
            specs.add("You can use your " + gunName + " as many times as you want.");

        return specs;
    }

    public static int GetRealVigiGunCount(Player vigi) {
        return GetVigiGunCount(vigi, true);
    }

    public static int GetVigiGunCount(Player vigi, Boolean real) {
        int vigiGuns = 0;
        for(Gun g: vigi.getAbility(Vigilante.class).charges.iterable(Gun.class)){
            if(g.getFlag() != Constants.VIGILANTE_KILL_FLAG)
                continue;
            if(real == null){
                vigiGuns++;
            }else if(real && g.isReal())
                vigiGuns++;
        }
        return vigiGuns;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        if(p.isSilenced() || p.isDisenfranchised() || p.isPuppeted() || !p.game.getBool(SetupModifierName.GS_DAY_GUNS))
            return false;
        for(Gun gun: this.charges.iterable(Gun.class)){
            if(gun.getFlag() == Constants.GUN_KILL_FLAG)
                return true;
        }
        return false;
    }

    @Override
    public Charge getCharge(boolean isFromSelf) {
        return new Gun();
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean isZeroWeightModifiable() {
        return false;
    }

    @Override
    public boolean isItemAbility() {
        return true;
    }

    public static Action getAction(Controller shooter, Controller target, Game game) {
        return new Action(shooter.getPlayer(), abilityType, new LinkedList<>(),
                ControllerList.ToPlayerList(game, target));
    }

    public static boolean HasNightGuns(Player p) {
        if(!p.hasAbility(Vigilante.abilityType))
            return false;
        Vigilante ga = p.getAbility(Vigilante.class);
        return ga.charges.getRealCharges() != 0;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Vigilante", abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}

package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.support.Bread;
import game.abilities.support.Gun;
import game.abilities.util.AbilityUtil;
import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Feedback;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class GraveDigger extends GameAbility {

    public static final AbilityType abilityType = AbilityType.GraveDigger;
    public static final String COMMAND = abilityType.command;

    public GraveDigger(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String ROLE_NAME = "Grave Digger";

    private PlayerList prevTargeted;

    @Override
    public GameAbility initialize(Player p) {
        if(!p.game.getBool(SetupModifierName.GD_REANIMATE))
            prevTargeted = new PlayerList();
        return super.initialize(p);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Reanimate a dead person to target living people with their actions";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();
        PlayerList targets;
        GameAbility ability;
        Action victimAction;
        for(Action da: actionList){

            Player victim = da.getTarget();
            ability = getGameAbilityFromActionArg(da.getArg1());

            list.add("dig up ");
            list.add(StringChoice.YouYourselfSingle(victim));
            if(da.getTargets().size() > 1){
                targets = da.getTargets().copy().remove(victim);
                if(victim.getDeathType().isHidden()){
                    list.add(" to target ");
                    list.addAll(StringChoice.YouYourself(targets));
                    continue;
                }
                if(ability == null || ability instanceof Spy){
                    list.add(" to target ");
                    list.addAll(StringChoice.YouYourself(targets));
                }else{
                    victimAction = new Action(victim, ability.getAbilityType(), da.getArg2(), da.getArg3(), targets);
                    try{
                        ArrayList<Object> parts = ability.getActionDescription(victimAction);
                        list.add(" to ");
                        list.addAll(parts);
                    }catch(IndexOutOfBoundsException e){
                        list.add(" to target ");
                        list.addAll(StringChoice.YouYourself(targets));
                    }
                }
            }else if(victim.is(Douse.abilityType)){
                list.add(" to burn");
            }

            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    public GameAbility getGameAbilityFromActionArg(String arg) {
        AbilityType abilityType = AbilityUtil.findAbilityByCommand(arg).orElse(Visit.abilityType);
        return AbilityUtil.CREATOR(abilityType, narrator, new Modifiers<>());
    }

    @Override
    public void selfTargetableCheck(Action a) {
        if(!a.owner.in(a._targets.subList(1)))
            return;
        if(!canSelfTarget())
            AbilityValidationUtil.Exception("You may not target yourself with this action.");
    }

    @Override
    public void targetSizeCheck(Action a) {
        Player corpse = a.getTarget();
        if(corpse == null)
            AbilityValidationUtil.Exception("You need to select a target with this ability");
    }

    public static final String WITCH_FEEDBACK = "You were witched!";

    @Override
    public void mainAbilityCheck(Action a) {
        Player corpse = a._targets.getFirst();
        if(corpse.isAlive())
            AbilityValidationUtil.Exception("Can't reanimate living people");

        PlayerList targets = a._targets.copy();
        targets.remove(corpse);

        if(targets.hasDead())
            AbilityValidationUtil.Exception("Can't make dead people target dead people.");

        Game game = corpse.game;
        Optional<AbilityType> optAbilityType = AbilityUtil.findAbilityByCommand(a.getArg1());
        if(a.getArg1() != null && (!optAbilityType.isPresent() || !actionInGame(optAbilityType.get(), game)))
            AbilityValidationUtil.Exception("No one has the ability: '" + a.getArg1() + "'");

        if(a.getArg1() != null && isBannedAction(AbilityUtil.getAbilityByCommand(a.getArg1())))
            AbilityValidationUtil.Exception("This ability can't be triggered by Grave Diggers.");

        if(a.owner.game.getBool(SetupModifierName.GD_REANIMATE))
            return;
        if(prevTargeted != null && prevTargeted.contains(corpse))
            AbilityValidationUtil.Exception("You may not reanimate this corse again.");

    }

    public static final AbilityType[] BANNED_ACTIONS = { Agent.abilityType, Amnesiac.abilityType, Architect.abilityType,
            ArmsDetector.abilityType, Coroner.abilityType, CultLeader.abilityType, Detective.abilityType,
            Disguiser.abilityType, FactionKill.abilityType, FactionSend.abilityType, Ghost.abilityType,
            GraveDigger.abilityType, Investigator.abilityType, JailCreate.abilityType, Lookout.abilityType,
            MasonLeader.abilityType, Mayor.abilityType, Sheriff.abilityType, Snitch.abilityType, Spy.abilityType,
            Ventriloquist.abilityType };

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        Game n = p.game;
        if(n.players.hasDead())
            return n.getDeadPlayers().sortByName().add(n.getLivePlayers().sortByName());
        return new PlayerList();
    }

    private static boolean actionInGame(AbilityType abilityType, Game game) {
        Setup setup = game.setup;
        if(Visit.abilityType.equals(abilityType))
            return true;
        if(Vigilante.abilityType.equals(abilityType))
            return setup.hasPossibleFactionRoleWithAbility(game, Gunsmith.abilityType, Blacksmith.abilityType,
                    Vigilante.abilityType);
        if(BreadAbility.abilityType.equals(abilityType))
            return setup.hasPossibleFactionRoleWithAbility(game, Baker.abilityType);
        if(Survivor.abilityType.equals(abilityType))
            return setup.hasPossibleFactionRoleWithAbility(game, Armorsmith.abilityType, Blacksmith.abilityType,
                    Survivor.abilityType);

        return game.setup.hasFactionRoleWithAbility(abilityType);
    }

    public static final String LIVE_FEEDBACK = "You tried to dig up the grave of a living person.";

    @Override
    public void doNightAction(Action action) {
        Player gd = action.owner;
        Player dead = action.getTargets().getFirst();
        if(dead.isAlive()){
            new Feedback(gd, LIVE_FEEDBACK).setPicture("amnesiac");
            Visit.NoNightActionVisit(action);
            return;
        }

        PlayerList targets = action.getTargets().remove(dead);

        AbilityType ability_parse = AbilityUtil.findAbilityByCommand(action.getArg1()).orElse(Visit.abilityType);
        if(isBannedAction(ability_parse))
            ability_parse = Visit.abilityType;

        if(ability_parse == Visit.abilityType){
            for(GameAbility a: dead.getRoleAbilities()){
                if(a.getRealCharges() == 0)
                    continue;
                if(a.is(Joker.abilityType) && action.getArg2() == null && !Joker.CanAddBountyActionTonight(dead))
                    continue;
                if(!a.is(BANNED_ACTIONS) && a.getCommand() != null && a.isNightAbility(dead)){
                    ability_parse = a.getAbilityType();
                    break;
                }
            }
        }

        if(dead.is(Ghost.abilityType) && !dead.getActions().isEmpty()){
            dead.getActions().getFirst().setTarget(targets.getFirst());
        }else if(ability_parse != Visit.abilityType){
            addCorpseAction(gd, dead, ability_parse, action.getArg2(), action.getArg3(), targets);
        }else{
            boolean foundActionToSubmit = false;
            for(GameAbility a: dead.getRoleAbilities()){
                if(a.is(GraveDigger.abilityType, Ventriloquist.abilityType, Disguiser.abilityType))
                    continue;
                if(a.isAcceptableTarget(new Action(dead, ability_parse, action.getArg1(), action.getArg2(), targets))){
                    foundActionToSubmit = true;
                    addCorpseAction(gd, dead, a.getAbilityType(), action.getArg2(), action.getArg3(), targets);
                    break;
                }
            }
            if(!foundActionToSubmit)
                addCorpseAction(gd, dead, Visit.abilityType, null, null, targets);
        }

        happening(gd, " reanimated ", dead);

        if(prevTargeted != null)
            prevTargeted.add(dead);
        action.markCompleted();
        gd.visit(dead);
    }

    private void addCorpseAction(Player gd, Player dead, AbilityType abilityType, String option, String option2,
            PlayerList targets) {
        if(dead.getActions().mainAction == null)
            dead.getActions().mainAction = new Action(dead, abilityType, option, option2, targets);
        else
            dead.getActions().extraActions.add(new Action(dead, abilityType, option, option2, targets));
        dead.setSubmissionTime(gd.getSubmissionTime());

    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        PlayerList targets = a.getTargets().copy();

        parsedCommands.add(COMMAND);
        for(Player target: targets){
            parsedCommands.add(target.getName());
        }
        if(a.getArg1() != null)
            parsedCommands.add(a.getArg1());
        if(a.getArg2() != null)
            parsedCommands.add(a.getArg2());
        if(a.getArg3() != null)
            parsedCommands.add(a.getArg3());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        commands.remove(0);
        PlayerList targets = new PlayerList();

        String name;
        Player pi;
        for(int i = 0; i < commands.size();){
            name = commands.get(0);
            pi = p.game.getPlayerByName(name);
            if(pi == null)
                break;
            targets.add(pi);
            commands.remove(0);
        }

        if(targets.isEmpty())
            throw new PlayerTargetingException("Need to target a corpse with this ability");

        String command;
        if(commands.isEmpty() || AbilityUtil.getAbilityByCommand(commands.get(0)) == null){
            command = Visit.COMMAND;
        }else{
            command = commands.remove(0);
        }

        List<String> args = Util.toStringList(command);

        if(!commands.isEmpty()){
            GameFaction t = GameFaction.GetFaction(commands, p.game);
            if(t != null)
                args.add(t.getColor());
            else
                args.add(commands.remove(0));
        }

        if(!commands.isEmpty()){
            String t = AbilityUtil.GetRoleName(commands, setup);
            if(t != null)
                args.add(t);
            else
                args.add(commands.remove(0));
        }

        // need to account for 3rd option.
        return new Action(p, abilityType, timeLeft, args, targets);
    }

    public static boolean isBannedAction(AbilityType abilityType) {
        for(AbilityType bannedAbilityType: GraveDigger.BANNED_ACTIONS)
            if(bannedAbilityType == abilityType)
                return true;
        return false;
    }

    @Override
    public ArrayList<Option> getOptions(Player owner) {
        ArrayList<Option> aList = new ArrayList<>();
        ArrayList<String> vList = new ArrayList<>();
        Role role;
        GameAbility gameAbility;
        int playerCount = owner.game.players.size();
        for(FactionRole factionRole: player.game.setup.getPossibleFactionRoles(owner.game)){
            role = factionRole.role;
            for(Ability ability: role.getAbilities()){
                gameAbility = ability.getGameAbility(factionRole.getAbilityModifiers(ability.type, playerCount));
                if(gameAbility.isNightAbility(owner) && !vList.contains(gameAbility.getCommand())
                        && !isBannedAction(gameAbility.getAbilityType())){
                    aList.add(new Option(gameAbility.getCommand(), ability.getClass().getSimpleName()));
                    vList.add(gameAbility.getCommand());
                }
            }
            if(role.hasAbility(Vigilante.abilityType) && !vList.contains(Gun.COMMAND))
                aList.add(new Option(Gun.COMMAND));
            if(role.hasAbility(Baker.abilityType) && !vList.contains(Bread.COMMAND))
                aList.add(new Option(Bread.COMMAND));
        }
        aList.add(new Option(Visit.COMMAND));
        return aList;
    }

    private static ArrayList<Option> getVestCreatorOptions(Game game) {
        boolean isFakeVestPossible = isFakeItemPossible(game, Armorsmith.abilityType,
                AbilityModifierName.AS_FAKE_VESTS);
        return Blacksmith.getGunOptions(isFakeVestPossible, game);

    }

    private static ArrayList<Option> getCraftGunOptions(Game game) {
        boolean isFakeGunPossible = isFakeItemPossible(game, Blacksmith.abilityType,
                AbilityModifierName.GS_FAULTY_GUNS);
        return Blacksmith.getGunOptions(isFakeGunPossible, game);
    }

    private static ArrayList<Option> getCraftArmorOptions(Game game) {
        boolean isFakeArmorPossible = isFakeItemPossible(game, Blacksmith.abilityType,
                AbilityModifierName.AS_FAKE_VESTS);
        return Blacksmith.getArmorOptions(isFakeArmorPossible, game);
    }

    private static ArrayList<Option> getGunCreatorOptions(Game game) {
        boolean isFakeGunPossible = isFakeItemPossible(game, Gunsmith.abilityType, AbilityModifierName.GS_FAULTY_GUNS);
        return Gunsmith.getOptions(isFakeGunPossible, game);
    }

    private static boolean isFakeItemPossible(Game game, AbilityType abilityType, AbilityModifierName modifier) {
        Set<FactionRole> factionRoles = game.setup.getPossibleFactionRolesWithAbility(game, abilityType);
        if(factionRoles.isEmpty())
            return false;
        for(FactionRole factionRole: factionRoles){
            if(factionRole.getModifierValue(game, abilityType, modifier, false))
                return true;
        }
        return false;
    }

    @Override
    public ArrayList<Option> getOptions2(Player player, String option1) {
        if(option1 == null)
            return new ArrayList<>();
        Game game = player.game;
        if(option1.equalsIgnoreCase(Armorsmith.COMMAND))
            return getVestCreatorOptions(player.game);
        else if(option1.equalsIgnoreCase(DrugDealer.COMMAND))
            return DrugDealer.getDrugOptions(game);
        else if(option1.equalsIgnoreCase(Blacksmith.COMMAND))
            return getCraftGunOptions(game);
        else if(option1.equalsIgnoreCase(Framer.COMMAND))
            return Framer.getOptions(game);
        else if(option1.equalsIgnoreCase(Gunsmith.COMMAND))
            return getGunCreatorOptions(game);
        else if(option1.equalsIgnoreCase(Tailor.COMMAND))
            return Tailor.getOptions(game);
        return new ArrayList<>();
    }

    @Override
    public ArrayList<Option> getOptions3(Player p, String option1, String option2) {
        if(option1 == null)
            return new ArrayList<>();
        Game game = p.game;
        if(option1.equalsIgnoreCase(Blacksmith.COMMAND))
            return getCraftArmorOptions(game);
        else if(option1.equalsIgnoreCase(Tailor.COMMAND))
            return Tailor.getOptions2(game, option2);
        return new ArrayList<>();
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(p.game.getDeadPlayers().isEmpty())
            return "Can't use this until people die";
        Player live = p.game.getLivePlayers().getRandom(r);
        Player dead = p.game.getDeadPlayers().getRandom(r);

        return getCommand() + " *" + live.getName() + " " + dead.getName() + "*";
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(!p.game.getBool(SetupModifierName.GD_REANIMATE)){
            ret.add("You may not target the same corpse twice.");
        }

        return ret;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Grave Digger", abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    public static Action getAction(Controller digger, Game game, Controller... targets) {
        return new Action(digger.getPlayer(), abilityType, new LinkedList<>(),
                ControllerList.ToPlayerList(game, targets));
    }
}

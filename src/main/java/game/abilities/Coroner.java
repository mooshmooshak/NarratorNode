package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Coroner extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Coroner;
    public static final String COMMAND = abilityType.command;

    public Coroner(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String ROLE_NAME = "Coroner";

    public static final String NIGHT_ACTION_DESCRIPTION = "Target a dead player to learn their true identity, and who they visited each night.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.aliveCheck(a);
    }

    // public static final
    public static final String LIVE_FEEDBACK = "You tried to perform an autopsy on a living person.";

    @Override
    public void doNightAction(Action a) {
        Player coro = a.owner;
        Player target = a.getTarget();
        if(target == null)
            return;

        if(target.isAlive()){
            new Feedback(coro, LIVE_FEEDBACK).setPicture("amnesiac");
            Visit.NoNightActionVisit(a);
            return;
        }

        HTString team = new HTString(target.gameRole.factionRole);
        Message fb = new Feedback(coro, "Your target was a ").add(team).add(".").setPicture("lookout");

        int i = 0;
        if(coro.game.getBool(SetupModifierName.DAY_START))
            i = 1;

        PlayerList prevTargets;
        ActionList al;
        for(int day = i; day <= coro.game.getDayNumber() - 1; day++){
            al = target.getPrevNightTarget(day);
            prevTargets = new PlayerList();
            for(Action pAction: al.getActions()){
                if(pAction.abilityType == FactionSend.abilityType)
                    continue;
                prevTargets.add(pAction.getTargets());
            }
            if(!prevTargets.isEmpty()){
                fb.add(" On Night " + day + ", your target visited ", prevTargets, ".");
            }
        }

        if(target.getDeathType().isHidden()){
            ArrayList<String[]> attacks = target.getDeathType().attacks;
            String deathString;
            if(attacks.size() == 1)
                deathString = attacks.get(0)[1].toLowerCase() + ".";
            else{
                StringBuilder sb = new StringBuilder();
                for(int j = 0; j < attacks.size(); j++){
                    if(j == attacks.size() - 1)
                        sb.append(" and ");
                    sb.append(attacks.get(j)[1].toLowerCase());
                    sb.append(", ");
                }
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
                sb.append(".");
                deathString = sb.toString();
            }
            fb.add(" Your target was actually " + deathString);
        }

        if(target.game.getBool(SetupModifierName.CORONER_LEARNS_ROLES)){
            fb.add(" Your target was visited by the following roles: ");
            for(int j = 0; j < target.getRoleVisits().size(); j++){
                if(j != 0)
                    fb.add(", ");
                fb.add(target.getRoleVisits().get(j));
            }
            fb.add(".");
        }

        if(target.getDeathType().isHidden())
            target.lastWillTrustees.put(coro, true);

        if(target.game.getBool(SetupModifierName.CORONER_EXHUMES))
            target.setExhumed();

        happening(coro, " performed an autopsy on ", target);
        a.markCompleted();
        coro.visit(target);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.CORONER_EXHUMES);
        names.add(SetupModifierName.CORONER_LEARNS_ROLES);
        return names;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.CORONER_LEARNS_ROLES))
            ret.add("You will learn what roles visited your targets.");
        else
            ret.add("You will not learn what roles visited your targets.");

        if(p.game.getBool(SetupModifierName.CORONER_LEARNS_ROLES)
                && p.game.setup.hasPossibleFactionRoleWithAbility(p.game, GraveDigger.abilityType))
            ret.add("Autopsies stop bodies from being reanimated.");
        else
            ret.add("Autopsies do not stop bodies from being reanimated.");

        return ret;
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(p.game.getDeadPlayers().isEmpty())
            return "Can't use this until people die";
        return super.getUsage(p, r);
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    public static FactionRole template(Faction town) {
        Role role = RoleService.createRole(town.setup, "Coroner", Coroner.abilityType);
        return FactionRoleService.createFactionRole(town, role);
    }
}

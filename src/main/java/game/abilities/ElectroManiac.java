package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class ElectroManiac extends GameAbility {

    public static final AbilityType abilityType = AbilityType.ElectroManiac;

    public static final String COMMAND = abilityType.command;

    public ElectroManiac(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String ROLE_NAME = "Electro Maniac";

    @Override
    public GameAbility initialize(Player p) {
        fakeCharge = false;
        usedDoubleAction = null;
        return super.initialize(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Polarize people, so that they will die when visiting each other.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    public static final String DEATH_FEEDBACK = "You were killed by an Electro Maniac!";
    public static final String CHARGED_FEEDBACK = "You were charged last night!";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        PlayerList targets = a.getTargets();
        if(targets == null || targets.isEmpty())
            return;
        for(Player target: targets){
            if(target.isCharged() && !target.is(ElectroManiac.abilityType))
                Kill(owner, target, Constants.ELECTRO_KILL_FLAG);// visiting taken care of already in kill
            else{
                Feedback fb = new Feedback(target);
                FeedbackGenerator(fb);
                target.setCharged(owner);
                owner.visit(target);
                happening(owner, " charged ", target);
            }
            if(fakeCharge)
                break;
        }
        if(targets.size() == 2)
            usedDoubleAction = owner.game.getDayNumber();

        a.markCompleted();
    }

    public static void FeedbackGenerator(Feedback fb) {
        fb.add(CHARGED_FEEDBACK).setPicture("electromaniac");
        fb.addExtraInfo("If you visit or get visited by someone else that's charged, you'll be shocked to death!");
    }

    public Integer usedDoubleAction;
    private boolean fakeCharge;
    private boolean wasBlocked = false;

    @Override
    public void targetSizeCheck(Action a) {
        if(usedDoubleAction != null && !fakeCharge){
            super.targetSizeCheck(a);
            return;
        }
        if(a.getTargets().size() == 1)
            return;
        if(a.getTargets().size() != 2){
            AbilityValidationUtil.Exception("You may select one or two targets only.");
        }
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        int dayNumber = p.game.getDayNumber() - 1;
        if(usedDoubleAction == null || dayNumber != usedDoubleAction)
            return;
        if(!wasBlocked)
            return;
        wasBlocked = false;
        usedDoubleAction = null;
        fakeCharge = true;
    }

    @Override
    public void onNightFeedback(ArrayList<Feedback> feedback) {
        super.onNightFeedback(feedback);
        for(Feedback f: feedback){
            if(f.access(player).equals(Block.FEEDBACK)){
                wasBlocked = true;
                break;
            }
        }
    }

    public static void ElectroKill(Player p) {
        if(p.isAlive())
            p.kill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, p, p.getCharger()));
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(usedDoubleAction == null)
            return super.getUsage(p, r);
        PlayerList live = p.game.getLivePlayers().remove(p);
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean canAddAnotherAction(Action a) {
        if(a.getTargets().size() == 1)
            return super.canAddAnotherAction(a);
        for(Action current: a.owner.getActions().getActions(abilityType))
            if(current._targets.size() == 2)
                return false;
        return super.canAddAnotherAction(a);
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Electromaniac", ElectroManiac.abilityType,
                Bulletproof.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    public static Action getAction(Controller electromaniac, Controller targets, Game game) {
        return new Action(electromaniac.getPlayer(), abilityType, new LinkedList<>(),
                ControllerList.ToPlayerList(game, targets));
    }

}

package game.abilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import game.logic.Game;
import game.logic.Player;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Bulletproof extends Passive {

    public static final AbilityType abilityType = AbilityType.Bulletproof;

    public Bulletproof(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void adjustRoleSpecs(List<String> specs) {
        specs.remove(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public GameAbility initialize(Player p) {
        p.setInvulnerable(true);
        happening(p, " is immune to night kills.");
        return super.initialize(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Invulnerable at night";

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    @Override
    public boolean isHiddenModifiable() {
        return true;
    }

    @Override
    public ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        ret.add("You are invulnerable at night.");
        return ret;
    }

    @Override
    public boolean canSelfTarget() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Bulletproof", Bulletproof.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}

package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.support.Charge;
import game.abilities.support.Charges;
import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownTeamException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.Ability;
import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import models.serviceResponse.AbilityMetadata;
import util.Util;
import util.game.ActionUtil;
import util.game.ChargeUtil;
import util.game.GameUtil;
import util.game.ModifierUtil;

public abstract class GameAbility {

    /*
     * HOW TO ADD ANOTHER ABILITY
     *
     * add to witch ability, if not passive
     */

    protected Game narrator;
    protected Setup setup;
    protected Player player; // TODO use this more often
    public Modifiers<AbilityModifierName> modifiers;
    private int abilityCD = 0;

    public Charges charges;

    public GameAbility(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        this.setup = setup;
        this.narrator = game;
        this.modifiers = modifiers;
    }

    public GameAbility initialize(GameFaction t) {
        int initialCharges = modifiers.getInt(AbilityModifierName.CHARGES, t.game);
        setInitialCharges(initialCharges, true);
        return this;
    }

    public GameAbility initialize(Player p) {
        this.player = p;
        int initialCharges = modifiers.getInt(AbilityModifierName.CHARGES, p.game);
        setInitialCharges(initialCharges, true);
        return this;
    }

    public void setInitialCharges(int charge, boolean shouldAddNoise) {
        if(shouldAddNoise)
            charge = ChargeUtil.AddNoise(charge, narrator);

        if(charge != Constants.UNLIMITED)
            this.charges = new Charges(this, charge);
        else
            this.charges = new Charges(this);
    }

    public abstract AbilityType getAbilityType();

    final public String getCommand() {
        return getAbilityType().command;
    }

    public String getDayCommand() {
        return null;
    }

    // i think player param can be removed
    public ArrayList<Option> getOptions(Player p) {
        return new ArrayList<>();
    }

    public ArrayList<Option> getOptions2(Player p, String option1) {
        return new ArrayList<>();
    }

    public ArrayList<Option> getOptions3(Player p, String option1, String option2) {
        return new ArrayList<>();
    }

    public boolean is(AbilityType... cs) {
        for(AbilityType c: cs){
            if(getAbilityType() == c)
                return true;
        }
        return false;
    }

    // null return indicates that it's active, but there is no 'target' persay

    public PlayerList getAcceptableTargets(Player pi) {
        if(isOnCooldown())
            return new PlayerList();
        List<String> args;
        if(this instanceof DrugDealer)
            args = Util.toStringList(DrugDealer.WIPE);
        else if(this instanceof Spy || this instanceof Framer){
            ArrayList<Option> options = getOptions(pi);
            if(options.isEmpty())
                return new PlayerList();
            args = Util.toStringList(options.get(0).value);
        }else if(this instanceof Tailor){
            args = Util.toStringList(pi.getGameFaction().getColor(), pi.getRoleName());
        }else
            args = new LinkedList<>();
        PlayerList acceptableTargets = new PlayerList();

        Action a;
        for(Player p: pi.game.getAllPlayers()){
            try{
                a = new Action(pi, getAbilityType(), args, Player.list(p));
            }catch(UnknownTeamException e){
                continue;
            }

            if(isAcceptableTarget(a))
                acceptableTargets.add(p);
        }
        return acceptableTargets.sortByName();
    }

    public boolean isAcceptableTarget(Action a) {
        try{
            checkAcceptableTarget(a);
            return true;
        }catch(PlayerTargetingException | IllegalActionException | UnknownTeamException | PhaseException e){
            return false;
        }
    }

    public boolean isAcceptableTarget(Player owner) {
        return isAcceptableTarget(new Action(owner, getAbilityType()));
    }

    public void skipDayTargetCheck(Action action) {
        throw new NarratorException("Dont use internal use object");
    }

    public void factionSizeChecks(Action a) {
        Game game = a.owner.game;
        int factionSize = a.owner.getGameFaction().getMembers().getLivePlayers().size();
        int minFactionSize = modifiers.getInt(AbilityModifierName.FACTION_COUNT_MIN, game);
        if(factionSize < minFactionSize)
            AbilityValidationUtil.Exception("Your faction isn't big enough to use this ability.");
        int maxFactionSize = modifiers.getInt(AbilityModifierName.FACTION_COUNT_MAX, game);
        if(factionSize > maxFactionSize)
            AbilityValidationUtil.Exception("Your faction is too big to use this ability.");
    }

    public void checkAcceptableTarget(Action a) {
        Player owner = a.owner;

        if(owner.isDead() && (a.abilityType != Ventriloquist.abilityType && a.abilityType != Puppet.abilityType))
            AbilityValidationUtil.Exception("Dead people cannot submit this action.");

        if(isOnCooldown() && getRemainingCooldown() == 1)
            AbilityValidationUtil.Exception("You must wait an additional night to use this ability.");
        else if(isOnCooldown())
            AbilityValidationUtil
                    .Exception("You must wait " + getRemainingCooldown() + "additional nights to use this ability");

        PlayerList allowedTargets;
        if(isNegativeAbility() && !owner.game.setup.allowsForFriendlyFire(owner.game))
            for(GameFaction t: owner.getFactions()){
                if(!t.knowsTeam())
                    continue;
                allowedTargets = owner.game.getLivePlayers().remove(t.getMembers());
                if(allowedTargets.contains(a.getTarget()))
                    continue;
                AbilityValidationUtil.Exception("Can't target teammates with this ability");
            }
        targetSizeCheck(a);
        if(a.getTargets().contains(owner.getSkipper()))
            skipDayTargetCheck(a);
        factionSizeChecks(a);
        chargeCheck();
        mainAbilityCheck(a);
        if(Action.isIllegalRetarget(a))
            AbilityValidationUtil.Exception("Can't target same person twice in a row");
        selfTargetableCheck(a);
        if(owner.game.isDay())
            checkPerformDuringDay(owner);
        if(owner.game.isNight() && !isNightAbility(a.owner))
            throw new PhaseException("Can't do that during the night.");

    }

    public abstract void mainAbilityCheck(Action a);

    public void targetSizeCheck(Action action) {
        AbilityValidationUtil.defaultTargetSizeCheck(getDefaultTargetSize(), action);
    }

    public void chargeCheck() {
        if(this.getPerceivedCharges() == 0)
            AbilityValidationUtil.Exception("You don't have any more charges of this ability.");
    }

    public void checkPerformDuringDay(Player owner) {
        if(owner.game.isDay() && !canUseDuringDay(owner))
            throw new PhaseException("You can't do this during the day.");
    }

    public void selfTargetableCheck(Action a) {
        if(!a.owner.in(a._targets))
            return;
        if(!canSelfTarget())
            AbilityValidationUtil.Exception("You may not target yourself with this action.");
    }

    // null return indicates that it's active, but there is no 'target' persay

    public boolean isRetarget(Action action, Action action2) {
        return action.getTargets().sortByID().equals(action2.getTargets().sortByID());
    }

    // cost of adding this action, given the actions that are submitted
    // also used to calculate the action's given weight, if its in the set of
    // actions already

    @SuppressWarnings("unused")
    public boolean canAddAnotherAction(Action a) {
        return true;
    }

    @SuppressWarnings("unused")
    public boolean canSubmitWithoutBread(Action a) {
        return true;
    }

    public void resolveFakeBreadedDayActions() {
        ActionList dayActions = player.getActions();
        if(dayActions == null || dayActions.isEmpty())
            return;
        Player submitter = dayActions.getFirst().owner;
        int realBreads = 1 + BreadAbility.getUseableBread(submitter, true);

        Action overflow;
        for(int i = realBreads; i < dayActions.size(); i++){
            overflow = dayActions.getActions().get(i);
            dayActions.remove(overflow);
            overflow.owner.game.removeActionStack(overflow);
        }
    }

    public void doDayAction(Action infoAction, Action parentAction) {
        throw new IllegalActionException();
    }

    public void dayHappening(Action action) {
    }

    public abstract void doNightAction(Action action);

    public void pushReplaceMessage(Action oldAction, Action newAction) {
        Player owner = newAction.owner;
        SelectionMessage descrip = new SelectionMessage(owner, true, true);
        descrip.add("Instead of ");
        descrip.add(oldAction.getInsteadPhrase());
        descrip.add(", ");

        StringChoice you = new StringChoice(owner);
        you.add(owner, "you");

        ArrayList<Action> aList = new ArrayList<>();
        aList.add(newAction);

        descrip.add(" will ");
        descrip.add(owner.getAbility(newAction.abilityType).getActionDescription(aList));
        descrip.add(".");

        newAction.pushCommand();

        descrip.pushOut();

        List<NarratorListener> listeners = owner.game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onTargetSelection(owner, descrip);
    }

    public void onReplaceAdd(Action newAction) {
        if(isDayAbility())
            doDayAction(newAction, newAction);
        else
            newAction.owner.getActions().addAction(newAction);
    }

    public void onReplaceCancel(Action oldAction) {
        if(isDayAbility()){
            int actionNumber = oldAction.owner.getActions().getActions().indexOf(oldAction);
            oldAction.owner.cancelDayAction(actionNumber, oldAction.timeLeft);
        }else
            oldAction.owner.getActions().remove(oldAction);
    }

    public Optional<Action> getExampleAction(Player owner) {
        PlayerList pl = owner.getAcceptableTargets(getAbilityType());
        if(pl.isEmpty())
            return Optional.empty();
        return Optional.of(new Action(owner, getAbilityType(), pl.getFirst()));
    }

    public Optional<Action> getExampleWitchAction(Player owner, Player target) {
        return Optional.of(new Action(owner, getAbilityType(), target));
    }

    public static boolean Kill(Player owner, Player target, String[] flag) {
        return Kill(owner, target, flag, Optional.of(owner));
    }

    public static boolean Kill(Player owner, Player target, String[] flag, Optional<Player> cause) {
        if(owner.game.isNight() && flag != Constants.POISON_KILL_FLAG) // otherwise I have other happenings to
                                                                       // record this
            happening(owner, " attacked ", target);
        DirectAttack da = new DirectAttack(owner, target, flag);
        if(cause.isPresent())
            da.setCause(cause.get());
        target.kill(da);
        if(target.is(Bomb.abilityType)){
            new Happening(owner.game).add(target, " blew up in ", target).add("'s face");
            owner.kill(new IndirectAttack(Constants.BOMB_KILL_FLAG, owner, target));
        }
        owner.visit(target);
        return true;
    }

    public boolean isActivePassive(Player p) {
        return false;
    }

    public boolean isAllowedFactionAbility() {
        return true;
    }

    public boolean isChatRole() {
        return false;
    }

    public boolean isNightless(Optional<Integer> playerCount) {
        return false;
    }

    public boolean isRoleUniqueAbility() {
        return false;
    }

    public boolean isFactionUniqueAbility() {
        return false;
    }

    public boolean isRecruitable() {
        return true;
    }

    public boolean isPowerRole() {
        return charges.getPerceivedCharges() != 0;
    }

    public boolean isPositiveAbility() {
        return !isNegativeAbility();
    }

    public boolean isNegativeAbility() {
        return false;
    }

    public boolean isRolePickingPhaseAbility() {
        return false;
    }

    public boolean isDayAbility() {
        return false;
    }

    public boolean isAvailableAbility(Player p) {
        if(getCommand() == null)
            return false;
        if(getPerceivedCharges() == 0)
            return false;
        if(isOnCooldown())
            return false;
        if(p.isDead() && !is(Ventriloquist.abilityType) && !is(Puppet.abilityType))
            return false;
        if(!is(AbilityType.Vigilante) && p.is(Ghost.abilityType) && p.isAlive())
            return false;

        // handling phase restrictions
        boolean isNightAbility = isNightAbility(p);
        boolean isDayAbility = canUseDuringDay(p);
        if(isNightAbility && isDayAbility)
            return true;
        else if(isNightAbility && p.game.isNight())
            return true;
        else if(isDayAbility && p.game.isDay())
            return true;
        return false;
    }

    public boolean canUseDuringDay(Player p) {
        return false;
    }

    public boolean isNightAbility(Player p) {
        return true;
    }

    public int getDefaultTargetSize() {
        return 1;
    }

    public boolean getDefaultSelfTargetValue() {
        return false;
    }

    public boolean hasCooldownAbility() {
        return true;
    }

    public boolean isBackToBackModifiable() {
        return true;
    }

    public boolean isChargeModifiable() {
        return true;
    }

    public boolean isHiddenModifiable() {
        return false;
    }

    public boolean isSelfTargetModifiable() {
        return true;
    }

    public boolean isZeroWeightModifiable() {
        return !this.isDayAbility();
    }

    public int nightVotePower(Player p, PlayerList pl) {
        return 1;
    }

    public boolean isFakeBlockable() {
        return true;
    }

    public int maxNumberOfSubmissions() {
        if(this.charges.isUnlimited())
            return Integer.MAX_VALUE;
        return this.charges.getPerceivedCharges();
    }

    public boolean isItemAbility() {
        return false;
    }

    public boolean successfulUsageTriggersBreadUsage(Action a) {
        return false;
    }

    public boolean canStartWithEnemies() {
        return true;
    }

    public boolean isDatabaseAbility() {
        return true;
    }

    public boolean isHiddenPassive() {
        if(!isHiddenModifiable())
            return false;
        return this.modifiers.getBoolean(AbilityModifierName.HIDDEN, narrator);
    }

    public boolean allowsForFriendlyFire() {
        return false;
    }

    public boolean affectsSending() {
        return false;
    }

    public boolean stopsParity(Player player) {
        return false;
    }

    public boolean canSelfTarget() {
        boolean defaultValue = this.getDefaultSelfTargetValue()
                || (!isPositiveAbility() && narrator.setup.allowsForFriendlyFire(narrator));
        return modifiers.getBoolean(AbilityModifierName.SELF_TARGET, Optional.of(narrator.players.size()),
                defaultValue);
    }

    final public boolean canSubmitFree() {
        return canSubmitFree(Optional.of(narrator.players.size()));
    }

    public boolean canSubmitFree(Optional<Integer> playerCount) {
        return this.modifiers.getBoolean(AbilityModifierName.ZERO_WEIGHTED, playerCount);
    }

    public boolean unlimitedCharges() {
        return charges.isUnlimited();
    }

    public GameAbility useCharge() {
        charges.useCharge();
        return this;
    }

    public int getFakeCharges() {
        return charges.getFakeCharges();
    }

    public int getPerceivedCharges() {
        return charges.getPerceivedCharges();
    }

    public int getRealCharges() {
        return charges.getRealCharges();
    }

    public void addFakeCharge() {
        charges.addFakeCharge();
    }

    public Charge getCharge(boolean isFromSelf) {
        return new Charge(isFromSelf);
    }

    protected int getAbilityCooldown() {
        return modifiers.getInt(AbilityModifierName.COOLDOWN, narrator);
    }

    public void triggerCooldown() {
        abilityCD = getAbilityCooldown() + 1;
    }

    private void decrementCooldown() {
        if(abilityCD > 0)
            abilityCD--;
    }

    public int getRemainingCooldown() {
        return abilityCD;
    }

    public boolean isOnCooldown() {
        return abilityCD > 0;
    }

    public void onDeath(Player p) {
    }

    public Boolean checkWinEnemyless(Player p) {
        return null;
    }

    public void resolveDayAction(Action action) {
        return;
    }

    public void onGameStart(Player player) {
    }

    public void onDayStart(Player player) {
    }

    public void onDayEnd(Player player) {
    }

    public void onNightStart(Player player) {
        decrementCooldown();
    }

    public void onPlayerNightEnd(Player player) {
    }

    public void onAbilityLoss(Player p) {
    }

    public void onNightFeedback(ArrayList<Feedback> feedback) {
    }

    public void onCancelAction(Action action, double timeLeft, Message voteAnnouncement) {
        action.owner.getActions().remove(action);
        List<NarratorListener> listeners = narrator.getListeners();
        for(NarratorListener listener: listeners)
            listener.onTargetRemove(action.owner, action.abilityType, action.getTargets());
    }

    public static Happening happening(Player player, String string, Player... targets) {
        Happening happening = new Happening(player.game);

        happening.add(player, string);
        happening.add(Player.list(targets));
        if(targets.length != 0 || !string.endsWith("."))
            happening.add(".");

        return happening;
    }

    public List<SetupModifierName> getSetupModifierNames() {
        return new LinkedList<>();
    }

    public List<AbilityModifierName> getAbilityModifiers() {
        List<AbilityModifierName> modifierNames = new LinkedList<>();
        if(this.isBackToBackModifiable())
            modifierNames.add(AbilityModifierName.BACK_TO_BACK);
        if(this.isChargeModifiable())
            modifierNames.add(AbilityModifierName.CHARGES);
        if(this.hasCooldownAbility())
            modifierNames.add(AbilityModifierName.COOLDOWN);
        if(this.isHiddenModifiable())
            modifierNames.add(AbilityModifierName.HIDDEN);
        if(this.isSelfTargetModifiable())
            modifierNames.add(AbilityModifierName.SELF_TARGET);
        if(this.isZeroWeightModifiable())
            modifierNames.add(AbilityModifierName.ZERO_WEIGHTED);
        return modifierNames;
    }

    public List<AbilityModifierName> getAbilityModifiersForRoleDetails() {
        List<AbilityModifierName> modifierNames = this.getAbilityModifiers();
        modifierNames.removeAll(AbilityModifierName.GENERIC_MODIFIERS);
        return modifierNames;
    }

    public List<SetupModifierName> getSetupModifiersForRoleDetails(Optional<Game> game, Setup setup) {
        return this.getSetupModifierNames();
    }

    public ArrayList<String> getCommandParts(Action action) {
        PlayerList targets = action.getTargets();
        String command = action.abilityType.command;

        ArrayList<String> parts = new ArrayList<>();
        parts.add(command);
        for(Player target: targets)
            parts.add(target.getName());

        return parts;
    }

    // it is safe to edit commands
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        commands.remove(0);
        Game n = p.game;
        Player pi;
        try{
            AbilityType abilityType = this.getAbilityType();
            PlayerList playerList = new PlayerList();
            for(int i = 0; i < commands.size(); i++){
                if(isSkipDay(commands, i)){
                    playerList.add(n.skipper);
                    i++;
                    continue;
                }
                pi = n.getPlayerByName(commands.get(i));
                playerList.add(pi);
            }
            return new Action(p, abilityType, timeLeft, new LinkedList<>(), playerList);
        }catch(NullPointerException e){
            throw new PlayerTargetingException("Couldn't find the playerName in question. " + commands);
        }
    }

    private static boolean isSkipDay(ArrayList<String> commands, int offset) {
        if(offset + 1 >= commands.size())
            return false;
        String potentialSkipDay = commands.get(offset) + " " + commands.get(offset + 1);
        return potentialSkipDay.equalsIgnoreCase(Constants.SKIP_DAY);
    }

    public boolean onDayEndTriggersWhileDead() {
        return false;
    }

    // cost of adding this action, given the actions that are submitted
    // also used to calculate the action's given weight, if its in the set of
    // actions already

    public static String NQuote(String s) {
        return SQuote(s + " playerName");
    }

    public static String SQuote(String s) {
        // i think this can be deleted
        return "\'" + s + "\'";
    }

    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        String abilityDescription = getAbilityDescription(game, setup);
        return Util.ToStringArray(abilityDescription);
    }

    public boolean showSelfTargetTextDefault() {
        return false;
    }

    public String getSelfTargetMemberDescription(boolean singleAbility, boolean canSelfTarget) {
        if(singleAbility && canSelfTarget)
            return "May target themselves.";
        if(!singleAbility && canSelfTarget)
            return "May target themselves with the " + getClass().getSimpleName().toLowerCase() + " ability.";
        if(singleAbility && !canSelfTarget)
            return "May not target themselves.";
        if(!singleAbility && !canSelfTarget)
            return "May not target themselves with the " + getClass().getSimpleName().toLowerCase() + " ability.";
        return "";
    }

    public ArrayList<Object> getActionDescription(Action a) {
        ArrayList<Action> actions = new ArrayList<>();
        actions.add(a);
        return getActionDescription(actions);
    }

    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = ActionUtil.getActionTargets(actions);

        list.add(getCommand().toLowerCase() + " ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add(a.abilityType.command.toLowerCase() + "ing ");
        list.add(StringChoice.YouYourselfSingle(a.getTarget()));

        return list;
    }

    public Message getCancelMessage(Action action) {
        Player owner = action.owner;
        SelectionMessage cancelMessage = new SelectionMessage(owner, false, true);
        StringChoice sc = new StringChoice(owner);
        sc.add(owner, "You");
        cancelMessage.add(sc, " decided not to ");
        ArrayList<Action> toCancel = new ArrayList<>();
        toCancel.add(action);
        cancelMessage.add(getActionDescription(toCancel));
        cancelMessage.add(".");
        cancelMessage.dontShowPrivate();
        cancelMessage.pushOut();
        return cancelMessage;
    }

    public String getProfileCooldownText(Player p) {
        if(getCommand() == null)
            return null;
        int cd = getAbilityCooldown();
        if(cd == 0)
            return null;
        if(cd == 1)
            return getCommand() + " may be used every other day";
        return getCommand() + " may be used every day";
    }

    public String getBackToBackLabel() {
        String phaseLabel = this.isDayAbility() ? "days" : "nights";
        return "May target the same people on consecutive " + phaseLabel + " using \""
                + this.getClass().getSimpleName().toLowerCase() + "\".";
    }

    // the only one that is significantly modified is amnesiac. Make sure that
    // matches any changes here.
    // extras
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> nullableColor, Set<Ability> abilities) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        ArrayList<String> ruleTextList = new ArrayList<>();
        String tag;
        if(getCommand() != null)
            tag = getCommand().toLowerCase();
        else
            tag = getClass().getSimpleName().toLowerCase();
        for(AbilityModifierName name: getAbilityModifiersForRoleDetails()){
            if(ModifierUtil.IsStringModifier(name))
                continue;
            ruleTextList.add(name.getTextValue(playerCount, setup.modifiers, this.modifiers));
        }
        for(SetupModifierName name: getSetupModifiersForRoleDetails(game, setup))
            ruleTextList.add(name.getTextValue(playerCount, setup.modifiers));

        int chargeCount = modifiers.getInt(AbilityModifierName.CHARGES, playerCount);
        if(chargeCount != Constants.UNLIMITED)
            ruleTextList.add(getRuleChargeText(playerCount, chargeCount));

        int cooldown = modifiers.getInt(AbilityModifierName.COOLDOWN, playerCount);
        if(0 != cooldown){
            if(cooldown == 1)
                ruleTextList.add("Can only use " + tag + " every other night.");
            else
                ruleTextList.add("Can only use " + tag + " every " + cooldown + "nights.");
        }

        int minFactionSizeToUse = modifiers.getInt(AbilityModifierName.FACTION_COUNT_MIN, playerCount);
        if(minFactionSizeToUse >= 2)
            ruleTextList.add("Can start to use " + tag + " once faction size reaches " + minFactionSizeToUse);

        Optional<ModifierValue> modifier = modifiers.getIfSingle(AbilityModifierName.FACTION_COUNT_MAX, playerCount);
        if(modifier.isPresent()){
            int maxFactionSizeToUse = modifier.get().getIntValue();
            if(maxFactionSizeToUse != (int) AbilityModifierName.FACTION_COUNT_MAX.defaultValue)
                ruleTextList.add("Can no longer use " + tag + " once faction size reaches " + maxFactionSizeToUse);
        }

        if(modifiers.getBoolean(AbilityModifierName.HIDDEN, playerCount))
            ruleTextList.add(getHiddenMemberDescription());

        boolean defaultSelfTargetValue = this.getDefaultSelfTargetValue();
        boolean canSelfTarget = modifiers.getBoolean(AbilityModifierName.SELF_TARGET, playerCount,
                defaultSelfTargetValue);
        if(canSelfTarget != defaultSelfTargetValue)
            ruleTextList.add(getSelfTargetMemberDescription(abilities.size() == 1, canSelfTarget));

        return ruleTextList;
    }

    final public ArrayList<String> getProfileRoleCardDetails(Player p) {
        if(isHiddenPassive())
            return new ArrayList<>();
        Game game = p.game;
        ArrayList<String> ret = new ArrayList<String>(), profileHints = getProfileHints(p);

        String[] roleInfo = getRoleBlurb(Optional.of(p.game), p.game.setup);
        if(roleInfo != null)
            for(String s: roleInfo)
                ret.add(s);

        String chargeText = getProfileChargeText();
        if(chargeText != null)
            ret.add(chargeText);

        String cdText = getProfileCooldownText(p);
        if(cdText != null)
            ret.add(cdText);

        if(profileHints != null)
            for(String s: profileHints)
                ret.add(s);

        String selfTargetText = getProfileSelfTargetText(p);
        if(selfTargetText != null)
            ret.add(selfTargetText);

        if(getCommand() != null){
            String tag = getCommand().toLowerCase();
            int minFactionSizeToUse = modifiers.getInt(AbilityModifierName.FACTION_COUNT_MIN, game);
            if(minFactionSizeToUse >= 2)
                ret.add("You may " + tag + " once your faction size reaches " + minFactionSizeToUse);

            int maxFactionSizeToUse = modifiers.getInt(AbilityModifierName.FACTION_COUNT_MAX, game);
            if(maxFactionSizeToUse >= game.players.size() && maxFactionSizeToUse != Setup.MAX_PLAYER_COUNT)
                ret.add("You may no longer use " + tag + " once your faction size reaches " + maxFactionSizeToUse);
        }

        adjustRoleSpecs(ret);

        return ret;
    }

    // used so bulletproof doesn't specify twice that they're invulnerable
    // needs better solution
    // probably one where it's specified exactly what's the description in "you/me"
    // words
    public void adjustRoleSpecs(List<String> specs) {

    }

    public String getProfileSelfTargetText(Player player) {
        if(getDefaultSelfTargetValue() != canSelfTarget() || showSelfTargetTextDefault()){
            String notText;
            if(canSelfTarget())
                notText = "";
            else
                notText = " not";

            if(player.getRoleAbilities().filterKnown().filterNot(Vote.abilityType).size() == 1)
                return "You may" + notText + " target yourself.";
            return "You may" + notText + " target yourself with the " + getClass().getSimpleName().toLowerCase()
                    + " ability.";
        }
        return null;
    }

    protected String getProfileChargeText() {
        if(getCommand() == null)
            return null;
        int pCharges = getPerceivedCharges();
        if(pCharges == Constants.UNLIMITED)
            return null;
        if(pCharges == 0)
            return "You may not " + getCommand().toLowerCase() + " anymore.";
        if(pCharges == 1)
            return "You have one more use of " + getCommand().toLowerCase() + " left.";
        return "You have " + pCharges + " more uses of " + getCommand().toLowerCase() + ".";
    }

    // only used in this file
    protected ArrayList<String> getProfileHints(Player p) {
        return null;
    }

    public String getRuleChargeText(Optional<Integer> playerCount, int chargeCount) {
        String tag;
        if(getCommand() != null)
            tag = getCommand().toLowerCase();
        else
            tag = getClass().getSimpleName().toLowerCase();
        int chargeVariability = setup.modifiers.getInt(SetupModifierName.CHARGE_VARIABILITY, playerCount);
        if(chargeVariability != 0){
            if(chargeCount == 1)
                return "Limited to ~1 use of " + tag + ".";
            return "Limited to ~" + chargeCount + " uses of " + tag + ".";
        }
        if(chargeCount == 1)
            return "Limited to 1 use of " + tag + ".";
        return "Limited to " + chargeCount + " uses of " + tag + ".";
    }

    public AbilityMetadata getMetadata(Player player, Random random) {
        String usage;
        try{
            usage = getUsage(player, random);
        }catch(Throwable e){
            usage = "Usage broken for this command.  Please report it!";
        }
        // setup ability refactor
        boolean isPublicAbility = this.is(Punch.abilityType, Vote.abilityType);

        return new AbilityMetadata(this.getCommand(),
                getAbilityDescription(Optional.of(player.game), player.game.setup), usage, isPublicAbility);
    }

    public abstract String getAbilityDescription(Optional<Game> game, Setup setup);

    public static String getDefaultUsage(GameAbility a, Player p, Random r) {
        return a.getCommand() + " *" + p.getAcceptableTargets(a.getAbilityType()).getRandom(r).getName() + "*";
    }

    public String getUsage(Player player, Random random) {
        return getDefaultUsage(this, player, random);
    }

    public String getChargeDescription(Optional<Integer> playerCount, Setup setup) {
        return "Charge count for " + getCommand();
    }

    public String getCooldownDescription() {
        return "Usage cooldown for " + getClass().getSimpleName().toLowerCase();
    }

    public String getZeroWeightDescription() {
        return "Can freely submit :" + getCommand();
    }

    public String getHiddenStatusRuleLabel() {
        return "Unbeknownst passive: " + getClass().getSimpleName();
    }

    public String getHiddenMemberDescription() {
        return "Is unaware of " + getClass().getSimpleName() + " passive";
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "<C>";
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o.getClass() != getClass())
            return false;

        return true;
    }

}

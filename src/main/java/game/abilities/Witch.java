package game.abilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.abilities.util.WitchOrdering;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class Witch extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Witch;
    public static final String COMMAND = abilityType.command;

    public Witch(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.WITCH_FEEDBACK);
        return names;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Manipulate someone else's action target to do your bidding.";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();

        for(Action action: actionList){
            Player victim = action._targets.getFirst();
            Player newTarget = action._targets.getLast();
            // String message;

            if(victim != null && newTarget != null){
                list.add("make ");
                list.add(StringChoice.YouYourselfSingle(victim));
                list.add(" target ");
                list.add(StringChoice.YouYourselfSingle(newTarget));
            }else if(newTarget != null){
                list.add("make the victim target ");
                list.add(StringChoice.YouYourselfSingle(newTarget));
            }else if(victim != null){
                list.add("control ");
                list.add(StringChoice.YouYourselfSingle(victim));
            }else{
                list.add("not control anyone tonight");
            }
            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    public static final String WITCH_FEEDBACK = "Your intended action was manipulated last night!";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        if(a.getTargets().getFirst() == a.owner)
            AbilityValidationUtil.Exception("You can't control yourself.");
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 2)
            AbilityValidationUtil.Exception("This ability requires a target and an target-target");
    }

    @Override
    public void doNightAction(Action witchAction) {
        Player witch = witchAction.owner;
        if(witchAction._targets.isEmpty())
            return;
        Player victim = witchAction._targets.getFirst();
        if(witchAction._targets.size() < 2){
            Visit.NoNightActionVisit(witchAction);
            return;
        }

        ActionList victimActions = victim.getActions();
        Player newTarget = witchAction._targets.getLast();
        Game n = witch.game;

        new Happening(n).add(witch, " changed ", victim, "\'s target to ", newTarget, ".");

        ArrayList<Action> actions = new ArrayList<>();
        for(Action victimAction: victimActions)
            if(!victimAction.wasChanged(witch) && !victimAction.is(FactionSend.abilityType))
                actions.add(victimAction);

        ArrayList<GameAbility> abilities = victim.getActionableAbilities();
        Collections.sort(abilities, WitchOrdering.byAbilityDamageComparator);

        for(GameAbility ability: abilities){
            if(ability.is(Spy.abilityType))
                continue;
            if(ability.is(FactionSend.abilityType))
                continue;

            Optional<Action> victimActionOpt = ability.getExampleWitchAction(victim, newTarget);
            if(!victimActionOpt.isPresent())
                continue;
            if(!ability.canAddAnotherAction(victimActionOpt.get()))
                continue;

            if(ability.is(JailExecute.abilityType)){
                if(jailActionUpdated(witch, victim, newTarget)){
                    witchAction.markCompleted();
                    witch.visit(victim);
                    return;
                }
                continue;
            }

            Action victimAction = victimActionOpt.get();
            if(!victimActions.canAddAnotherAction(ability.getAbilityType(), newTarget))
                continue;

            if(jokerKillAdded(witch, victim, newTarget, victimActions)){
                witchAction.markCompleted();
                witch.visit(victim);
                return;
            }

            Action newAction = victim.getActions().addAction(victimAction.witchTouch(witch)).setNotSubmittedByOwner();
            if(!newAction.isTeamAbility()){
                if(ability.is(Vigilante.abilityType))
                    victim.secondaryCause(witch);
                witchAction.markCompleted();
                witch.visit(victim);
                return;
            }
            for(GameFaction t: newAction.getFactions()){
                if(t.getFactionAbilityController(newAction.abilityType) == victim){
                    witchAction.markCompleted();
                    witch.visit(victim);
                    return;
                }
            }
            for(GameFaction t: newAction.getFactions()){
                if(t.getFactionAbilityController(newAction.abilityType) == null){
                    t.overrideSenderViaWitch(victim, newAction.abilityType);
                    witchAction.markCompleted();
                    witch.visit(victim);
                    return;
                }
            }
            Util.log(new Error("Witch wasn't able to manipulate action."));

        }

        Collections.sort(actions, WitchOrdering.byActionDamageComparator);

        for(Action action: actions){
            if(action.is(JailExecute.abilityType)){
                PlayerList jailedTargets = ((JailExecute) action.getAbility()).getJailedTargets(victim);
                if(PlayerList.SameShuffle(jailedTargets, action.getTargets()))
                    continue;
            }
            if(action.is(Joker.abilityType) && action.getTargets().size() == 1 && action.getArg1() == null){
                continue;
            }
            if(action.is(Vigilante.abilityType))
                victim.secondaryCause(witch);
            action.setTarget(newTarget);
            witchAction.markCompleted();
            witch.visit(victim);
            return;
        }

        Action visitAction = victim.action(newTarget, Visit.abilityType);
        if(victimActions.canAddAnotherAction(visitAction)){
            victimActions.addAction(visitAction.witchTouch(witch));
            witchAction.markCompleted();
            witch.visit(victim);
        }
    }

    private boolean jailActionUpdated(Player witch, Player victim, Player newTarget) {
        PlayerList jailedTargets = victim.getAbility(JailExecute.class).getJailedTargets(victim);
        if(jailedTargets.isEmpty())
            return false;
        Action victimAction = victim.getAction(JailExecute.abilityType);
        PlayerList victimTargets = victimAction == null ? new PlayerList() : victimAction.getTargets();
        PlayerList unexecutedTargets = jailedTargets.copy().remove(victimTargets);

        Player newExecuteTarget;
        if(newTarget.in(unexecutedTargets))
            newExecuteTarget = newTarget;
        else if(!newTarget.in(victimTargets) && unexecutedTargets.isNonempty())
            newExecuteTarget = unexecutedTargets.getRandom(narrator.getRandom());
        else
            return false;
        if(victimAction == null){
            victimAction = new Action(victim, AbilityType.JailExecute, newExecuteTarget);
            victim.getActions().addAction(victimAction);
        }else
            victimAction.addTarget(newExecuteTarget);
        victimAction.witchTouch(witch);
        return true;
    }

    private boolean jokerKillAdded(Player witch, Player victim, Player newTarget, ActionList victimActions) {
        if(!victim.is(Joker.abilityType))
            return false;
        Joker card = victim.getAbility(Joker.class);

        int submittedKills = 0;
        for(Action a: victimActions){
            if(!a.is(Joker.abilityType))
                continue;
            if(a.getArg1() == null && a.getTargets().size() == 1)
                continue;
            submittedKills += a.getTargets().size();
            if(a.getArg1() != null)
                submittedKills--;
        }

        if(card.bounty_kills <= submittedKills)
            return false;

        victimActions.addAction(new Action(victim, Joker.abilityType, Joker.NO_BOUNTY, newTarget)).witchTouch(witch);

        return true;
    }

    public static void ManipulatedFeedback(Player... p) {
        for(Player x: p)
            new Feedback(x, Witch.WITCH_FEEDBACK).setPicture("witch");
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.WITCH_FEEDBACK)){
            ret.add("Targeting people will give them 'witch' feedback");
        }else{
            ret.add("Targets will be unaware that you targeted them.");
        }
        return ret;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return p.game.getLivePlayers().sortByName();
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.game.getLivePlayers().remove(p);
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    @Override
    public String getProfileSelfTargetText(Player player) {
        String notText;
        if(canSelfTarget())
            notText = "";
        else
            notText = " not";

        if(player.getRoleAbilities().filterKnown().filterNot(Vote.abilityType).size() == 1)
            return "You may" + notText + " cause self-targets.";
        return "You may" + notText + " use your " + getClass().getSimpleName() + " ability to cause self-targets.";
    }

    @Override
    public String getSelfTargetMemberDescription(boolean singleAbility, boolean canSelfTarget) {
        if(singleAbility && canSelfTarget)
            return "May cause self-targets.";
        if(!singleAbility && canSelfTarget)
            return "May cause self-targets with the " + getClass().getSimpleName() + " ability.";
        if(singleAbility && !canSelfTarget)
            return "May not cause self-targets.";
        if(!singleAbility && !canSelfTarget){
            return "May not cause self-targets with the " + getClass().getSimpleName() + " ability.";
        }
        return "";
    }

    @Override
    public void selfTargetableCheck(Action a) {
        PlayerList targets = a._targets;
        if(targets.getFirst() != targets.getLast())
            return;
        if(!canSelfTarget())
            AbilityValidationUtil.Exception("You may not cause self-targets.");
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Witch", Witch.abilityType);
    }

}

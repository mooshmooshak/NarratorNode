package game.abilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Detective extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Detective;
    public static final String COMMAND = abilityType.command;

    public Detective(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Find out who someone visits at night.";

    // private boolean visited = false;
    public static final String FEEDBACK = "Your target visited: ";
    public static final String NO_VISIT = "Your target didn't visit anyone.";
    public static final String NO_ONE_ELSE = "Your target didn't visit anyone else.";
    private HashMap<Player, PlayerList> seenTargets;

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(owner.game.pre_visiting_phase){
            owner.visit(target);
            return;
        }

        if(seenTargets == null)
            seenTargets = new HashMap<>();
        PlayerList formerTargets = seenTargets.get(target);
        if(formerTargets == null){
            formerTargets = new PlayerList();
            seenTargets.put(target, formerTargets);
        }
        Message feedback = follow(owner, target, a.getIntendedTarget(), formerTargets);
        if(feedback != null)
            formerTargets.add(feedback.getPlayers());
        happening(owner, " followed ", target);
        a.markCompleted();

    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        if(seenTargets != null)
            seenTargets.clear();
    }

    public static boolean seesAll(Player owner) {
        return owner.game.getBool(SetupModifierName.FOLLOW_GETS_ALL) || owner.is(Agent.abilityType);
    }

    public static Message follow(Player owner, Player target, String ogTarget, PlayerList seenTargets) {
        boolean seesAll = seesAll(owner);
        PlayerList visits;
        if(target.isDetectable()
                || (owner.is(Detective.abilityType) && owner.game.getBool(SetupModifierName.FOLLOW_PIERCES_IMMUNITY)))
            visits = target.getVisits("Detective");
        else
            visits = new PlayerList();
        for(int i = 0; i < visits.size(); i++){
            if(!seesAll && visits.get(i).in(seenTargets)){
                visits.remove(i);
                i--;
            }
        }
        if(seesAll)
            visits.sortByName();
        ArrayList<Object> parts = FeedbackGenerator(owner, visits, seenTargets);
        Feedback f = new Feedback(owner);
        f.add(parts);
        if(!parts.isEmpty() && parts.get(0).toString().equalsIgnoreCase(NO_ONE_ELSE))
            f.setSorted();
        f.setPicture("detective");
        f.addExtraInfo("As a reminder, you attempted to follow " + ogTarget + ".");
        return f;
    }

    public static ArrayList<Object> FeedbackGenerator(Player owner, PlayerList visits, PlayerList seenTargets) {
        ArrayList<Object> parts = new ArrayList<>();
        if(visits.isEmpty()){
            if(seenTargets == null || seenTargets.isEmpty())
                parts.add(NO_VISIT);
            else
                parts.add(NO_ONE_ELSE);
        }else{
            parts.add(FEEDBACK);
            for(Player p: visits){
                parts.add(p);
                if(!seesAll(owner))
                    break;
                if(visits.getLast() != p)
                    parts.add(", ");
            }
            parts.add(".");
        }
        return parts;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.FOLLOW_GETS_ALL)){
            ret.add("You will see all targets visited by the person you follow.");
        }else{
            ret.add("You will only see one target visited by the person you follow.");
        }
        if(p.game.getBool(SetupModifierName.FOLLOW_PIERCES_IMMUNITY)){
            ret.add("You will be able to track detection immune targets");
        }else{
            ret.add("You cannot track detection immune targets");
        }

        return ret;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.FOLLOW_GETS_ALL);
        names.add(SetupModifierName.FOLLOW_PIERCES_IMMUNITY);
        return names;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Detective", Detective.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}

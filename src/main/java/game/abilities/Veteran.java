package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Happening;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.DirectAttack;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Veteran extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Veteran;
    public static final String COMMAND = abilityType.command;
    public static final String COMMAND_LOWERCASE = "alert";

    public Veteran(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    private boolean alertCausingImmunity = false;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public String getNightText(Game n) {
        return "Type " + NQuote(COMMAND) + " to kill all who visit you tonight";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Kill everyone who visits this person when on alert.";

    public static final String DEATH_FEEDBACK = "You were killed by a Veteran!";

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        if(getPerceivedCharges() != 0)
            return null;
        return new PlayerList();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("go on alert");
        return list;
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        if(alertCausingImmunity)
            p.setInvulnerable(false);
        alertCausingImmunity = false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public void mainAbilityCheck(Action a) {
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 0)
            AbilityValidationUtil.Exception("You can't target players with this ability");
    }

    @Override
    public void doNightAction(Action a) {
        Player veteran = a.owner, target = a.getTarget();
        Game narrator = veteran.game;
        // witched
        if(target != veteran){
            Visit.NoNightActionVisit(a);
        }
        if(!veteran.isInvulnerable()){
            veteran.setInvulnerable(true);
            alertCausingImmunity = true;
        }

        PlayerList people = new PlayerList();
        for(Player pQ: narrator.getLivePlayers()){
            if(pQ == veteran)
                continue;
            if(pQ.getActions().visited(veteran)){
                pQ.kill(new DirectAttack(veteran, pQ, Constants.VETERAN_KILL_FLAG));
                // pQ.visit(owner);
                people.add(pQ);
            }
        }
        Object add;
        if(people.isEmpty())
            add = "no one.";
        else
            add = people;

        Message e = new Happening(narrator);
        e.add(veteran, " went on alert, killing ", add);
        a.markCompleted();
    }

    public static boolean isImmuneVet(Player owner) {
        if(!owner.is(Veteran.abilityType))
            return false;
        Player target = owner.getTargets(abilityType).getFirst();
        if(target == null)
            return false;

        Veteran v = owner.getAbility(Veteran.class);
        if(v.getPerceivedCharges() != 0)
            return true;
        return false;
    }

    @Override
    protected String getProfileChargeText() {
        int charges = getPerceivedCharges();
        if(charges == Constants.UNLIMITED){
            return ("You can go on alert as many times as you want.");
        }else if(charges == 0){
            return ("You are too tired to go on alert anymore.");
        }else if(charges == 1){
            return ("You can guard your house 1 more time.");
        }else{
            return ("You can guard your house " + charges + " more times.");
        }
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public void onAbilityLoss(Player p) {
        if(alertCausingImmunity){
            p.setInvulnerable(false);
            if(p.isAlive())
                p.getInjuries().clear();
        }
        alertCausingImmunity = false;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.of(new Action(owner, abilityType));
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Veteran", abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}

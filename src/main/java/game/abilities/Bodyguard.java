package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.saves.BGHeal;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Bodyguard extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Bodyguard;
    public static final String COMMAND = abilityType.command;

    public Bodyguard(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    private boolean isProtecting = false;

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.GUARD_REDIRECTS_CONVERT);
        names.add(SetupModifierName.GUARD_REDIRECTS_POISON);
        names.add(SetupModifierName.GUARD_REDIRECTS_DOUSE);
        return names;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Guard someone from death, dying and killing the attacker.";

    public static final String TARGET_FEEDBACK = "You were saved by a bodyguard!";
    public static final String DEATH_TARGET_FEEDBACK = "You were killed by a bodyguard that was protecting your target";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> color, Set<Ability> abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(game, actionOriginatorName, color, abilities);

        ruleTextList.add("Target will know that they were protected, if they are attacked");

        return ruleTextList;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        if(owner.game.pre_bodyguard){
            owner.visit(target);
            return;
        }
        if(!getProtected()){
            happening(owner, " protected ", target);
            setProtected(false);
        }

        target.heal(new BGHeal(owner, Constants.BODYGUARD_KILL_FLAG));
        a.markCompleted();
    }

    private boolean getProtected() {
        return isProtecting;
    }

    private void setProtected(boolean b) {
        this.isProtecting = b;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        ret.add("Your target will know that they were protected if someone attacks them.");

        return ret;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    public static Player redirect(Player target) {
        PlayerList visitors = target.getVisitors("bg -> douse").filterIsTargeting(target, Bodyguard.abilityType);
        if(target.game.nightPhase == GamePhase.POST_KILLING){
            for(int i = 0; i < visitors.size(); i++){
                if(visitors.get(i).getLives() < 0){
                    visitors.remove(i);
                    i--;
                }
            }
        }
        if(!visitors.isEmpty())
            target = visitors.getFirst();

        return target;
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Bodyguard", Bodyguard.abilityType);
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }
}

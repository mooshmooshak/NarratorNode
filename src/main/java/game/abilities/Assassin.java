package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Assassin extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Assassin;
    public static final String COMMAND = "Assassinate";
    public static final String ASSASSINATE_LOWERCASE = "assassinate";

    public Assassin(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String[] SECRET_KILL = new String[] { "assassin_secret", "Day killer name is silent.", };

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    private boolean hasKnife;

    @Override
    public GameAbility initialize(Player p) {
        super.initialize(p);
        hasKnife = true;

        return this;
    }

    public String getNightText(Game n) {
        return Constants.NO_NIGHT_ACTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Day-kill someone once during the day!";

    @Override
    public void mainAbilityCheck(Action action) {
        Player owner = action.owner;
        Player target = action.getTarget();
        Game n = owner.game;
        if(owner.game.isNight())
            AbilityValidationUtil.noAcceptableTargets();
        boolean convertedAlly = narrator.setup.hasPossibleFactionRoleWithAbility(narrator, CultLeader.abilityType)
                && !n.isFirstDay();
        boolean disguisedAlly = narrator.setup.hasPossibleFactionRoleWithAbility(narrator, Disguiser.abilityType)
                && !n.getDeadPlayers().filterHiddenDeath().isEmpty();
        if(owner.getGameFaction().knowsTeam() && (!convertedAlly && !disguisedAlly)){
            if(target.getGameFaction() == owner.getGameFaction() && !enemyHasAssassin(owner.getGameFaction())){
                AbilityValidationUtil.Exception("Don't assassinate allies!");
            }
        }
        AbilityValidationUtil.deadCheck(action);
    }

    @Override
    public void doNightAction(Action a) {
        Visit.NoNightActionVisit(a);
    }

    @Override
    public String getDayCommand() {
        if(hasKnife())
            return COMMAND;
        return null;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return hasKnife() && !p.isPuppeted();
    }

    private boolean hasKnife() {
        return hasKnife;
    }

    @Override
    public void doDayAction(Action action, Action parentAction) {
        Player owner = action.owner;
        Game game = owner.game;
        Player target = action.getTarget();

        DeathAnnouncement e = new DeathAnnouncement(target);
        e.setPicture("assassin");

        StringChoice sc = new StringChoice(owner);
        sc.add(owner, "You");
        if(secretKill())
            sc.add(Message.PUBLIC, "Someone");
        e.add(sc, " assassinated ");

        StringChoice target_sc = new StringChoice(target);
        target_sc.add(target, "you");
        e.add(target_sc);
        e.add(".");

        hasKnife = false;

        PlayerList deadPeople = new PlayerList(target);
        DeathAnnouncement explosion = null;

        // was formerly daykill
        target.setDead(owner);
        target.getDeathType().addDeath(Constants.ASSASSIN_KILL_FLAG);

        game.announcement(e);

        if((target.is(ElectroManiac.abilityType) || target.isCharged()) && owner.isCharged()){
            if(!owner.isInvulnerable()){
                owner.dayKill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, owner, owner.getCharger()),
                        parentAction.timeLeft, Game.DAY_NOT_ENDING);
                deadPeople.add(owner);
            }
            if(!target.isInvulnerable() && !target.is(ElectroManiac.abilityType))
                target.getDeathType().addDeath(Constants.ELECTRO_KILL_FLAG);
            explosion = Burn.ExplosionAnnouncement(game, deadPeople, "electromaniac");
            game.announcement(explosion);
        }

        game.voteSystem.removeVotersOf(deadPeople);
        game.checkVote(parentAction.timeLeft);
        game.checkProgress();

        if(game.isInProgress()){
            List<NarratorListener> listeners = owner.game.getListeners();
            for(NarratorListener listener: listeners)
                listener.onAssassination(owner, target, e);
        }
    }

    private boolean secretKill() {
        return this.modifiers.getBoolean(AbilityModifierName.SECRET_KILL, narrator);
    }

    private boolean enemyHasAssassin(GameFaction gameFaction) {
        Set<Faction> teamsWithAssassins = narrator.setup.getPossibleFactionsWithRolesWithAbility(narrator,
                Assassin.abilityType);
        teamsWithAssassins.retainAll(gameFaction.faction.enemies);
        return !teamsWithAssassins.isEmpty();
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public void checkPerformDuringDay(Player owner) {
        if(canUseDuringDay(owner) && owner.game.isDay())
            return;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(canUseDuringDay(p)){
            ret.add("You can assassinate during the day.");
            if(secretKill())
                ret.add("No one will know you commited the assassinatino.");
            else
                ret.add("Everyone will know that you commited the assassination.");
        }else{
            ret.add("You cannot assassinate again.");
        }

        return ret;
    }

    @Override
    public boolean isPowerRole() {
        return hasKnife();
    }

    @Override
    public boolean isChargeModifiable() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted())
            return false;
        return hasKnife();
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public List<AbilityModifierName> getAbilityModifiers() {
        List<AbilityModifierName> modifierNames = new LinkedList<>();
        modifierNames.add(AbilityModifierName.SECRET_KILL);
        modifierNames.addAll(super.getAbilityModifiers());
        return modifierNames;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Assassin", abilityType);
    }
}

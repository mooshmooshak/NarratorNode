package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;

public class ProxyKill extends GameAbility {

    public static final AbilityType abilityType = AbilityType.ProxyKill;
    public static final String COMMAND = abilityType.command;

    public ProxyKill(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Ability to order a teammate to kill";

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 2)
            AbilityValidationUtil.Exception("You must select 2 and only 2 targets for murdering");
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.PROXY_UPGRADE);
        return names;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        if(!a.getTargets().get(0).in(a.owner.getGameFaction().getMembers()))
            AbilityValidationUtil.Exception("Killer must be in team");
    }

    @Override
    public void doNightAction(Action a) {
        if(a.owner.game.proxyWitchVisiting){
            a.markCompleted();
            if(a._targets.size() > 2){
                Visit.NoNightActionVisit(a.owner, a._targets.get(2));
                return;
            }
            return;
        }
        Player sender, target;
        if(a.getTargets().size() < 2 && a.owner.isDead()){
            sender = a.owner;
            target = a.getTargets().getFirst();
        }else{
            sender = a.getTargets().getFirst();
            target = a.getTargets().get(1);
        }
        if(target == null || sender == null || sender.isBlocked())
            return;

        sender.addTempAbility(FactionKill.abilityType).initialize(a.owner.getGameFaction());

        sender.getActions().extraActions.add(new Action(sender, FactionKill.abilityType, target).setProxied());

        if(a.owner != sender){
            Happening e = new Happening(a.owner.game);
            e.add(a.owner, " sent ", sender, " to kill ", target, ".");
        }
    }

    @Override
    public void onDeath(Player p) {
        if(p.game.getBool(SetupModifierName.PROXY_UPGRADE) && p.getGameFaction().knowsTeam()){
            GameFaction faction = p.getGameFaction();
            faction.addAttainedAbility(FactionKill.abilityType).initialize(faction);
        }
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();

        for(Action da: actionList){
            if(da.getTargets().getFirst() == da.owner){
                list.add("kill ");
                list.add(StringChoice.YouYourselfSingle(da.getTargets().getLast()));
                list.add(" and ");
                continue;
            }

            list.add("send ");
            list.add(StringChoice.YouYourselfSingle(da.getTargets().getFirst()));
            list.add(" to kill ");
            list.add(StringChoice.YouYourselfSingle(da.getTargets().getLast()));
            list.add(" and ");

        }
        list.remove(list.size() - 1);

        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("sending ");
        list.add(StringChoice.YouYourselfSingle(a.getTargets().getFirst()));
        list.add(" to kill ");
        list.add(StringChoice.YouYourselfSingle(a.getTargets().getLast()));
        return list;
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        PlayerList livesy = p.game.getLivePlayers().sortByName();

        PlayerList teamMembers = p.getGameFaction().getMembers().sortByName();

        PlayerList ret = new PlayerList();
        ret.add(p);

        for(Player tm: teamMembers){
            if(!tm.in(ret))
                ret.add(tm);
        }

        for(Player live: livesy){
            if(!live.in(ret))
                ret.add(live);
        }

        return ret;
    }

    @Override
    public String getUsage(Player p, Random r) {
        Player team = p.getGameFaction().getMembers().getRandom(r);
        PlayerList live = p.game.getLivePlayers();
        live.shuffle(r, null);
        return getCommand() + " *" + team.getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }
}

package game.abilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class ParityCheck extends GameAbility {

    public static final AbilityType abilityType = AbilityType.ParityCheck;
    public static final String COMMAND = abilityType.command;

    public ParityCheck(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Compare player allegiances each night for differences.";

    public static final String GOTTA_WAIT_FEEDBACK = "You don't have previous alignments to compare last night's target(s) to.";
    public static final String FEEDBACK_SAME_ALIGNMENT = "Your target's faction is the same as last night's targets.";
    public static final String FEEDBACK_ALL_DIFFERENT_ALIGNMENT = "Your target's faction is different than last night's targets.";

    private PlayerList lastChecked;
    private PlayerList checkedTonight;
    private List<String> intendedNamesCheckedTonight;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public GameAbility initialize(Player player) {
        this.checkedTonight = new PlayerList();
        this.lastChecked = new PlayerList();
        return super.initialize(player);
    }

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner;
        Player target = action.getTarget();
        if(target == null)
            return;

        if(lastChecked.isEmpty())
            new Feedback(owner).add(GOTTA_WAIT_FEEDBACK);
        else{
            Feedback feedback;
            int differentAlignmentCount = getDifferentAlignmentCount(target, lastChecked);
            if(differentAlignmentCount == 0)
                feedback = (Feedback) new Feedback(owner).add(FEEDBACK_SAME_ALIGNMENT);
            else if(differentAlignmentCount == lastChecked.size())
                feedback = (Feedback) new Feedback(owner).add(FEEDBACK_ALL_DIFFERENT_ALIGNMENT);
            else
                feedback = (Feedback) new Feedback(owner)
                        .add(getFeedback(lastChecked.size() - differentAlignmentCount));
            if(action.isSubmittedByOwner){
                feedback.addExtraInfo(
                        "As a reminder, you attempted to check " + action.getIntendedTarget() + " last night.");
                feedback.addExtraInfo("You also attempted to check " + Util.SpacedString(intendedNamesCheckedTonight)
                        + " two nights ago.");
            }
        }
        checkedTonight.add(target);
        intendedNamesCheckedTonight.add(action.getIntendedTarget());

        happening(owner, " parity checked ", target);
        action.markCompleted();
        owner.visit(target);
    }

    private static int getDifferentAlignmentCount(Player target, PlayerList lastChecked) {
        int count = 0;
        GameFaction targetFaction = target.getGameFaction();
        for(Player player: lastChecked){
            if(player.getGameFaction() != targetFaction)
                count++;
        }
        return count;
    }

    public static String getFeedback(int sameAlignmentCount) {
        return "Your target's faction matches that of " + sameAlignmentCount
                + " other(s) from the last time you checked.";
    }

    @Override
    public void onNightStart(Player player) {
        super.onNightStart(player);
        intendedNamesCheckedTonight = new ArrayList<>();
    }

    @Override
    public void onDayStart(Player player) {
        super.onDayStart(player);
        if(checkedTonight.isEmpty())
            return;
        if(player.isSquelched())
            lastChecked.clear();
        else{
            lastChecked.add(checkedTonight);
            checkedTonight.clear();
        }
    }

    @Override
    public void mainAbilityCheck(Action action) {
        AbilityValidationUtil.deadCheck(action);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Parity Cop", abilityType);
    }

    public static FactionRole template(Faction faction) {
        Role role = template(faction.setup);
        return FactionRoleService.createFactionRole(faction, role);
    }

}

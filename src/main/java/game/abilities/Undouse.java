package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import util.game.ActionUtil;

public class Undouse extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Undouse;
    public static final String COMMAND = abilityType.command;

    public Undouse(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public String getNightText(ArrayList<GameFaction> t) {
        return "  Type " + NQuote(COMMAND) + " to undouse.";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Remove dangerous flammable liquid from your target.";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        PlayerList target = ActionUtil.getActionTargets(actions);
        ArrayList<Object> list = new ArrayList<>();

        list.add(COMMAND.toLowerCase() + " ");
        list.addAll(StringChoice.YouYourself(target));

        return list;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();

        target.setDoused(false);
        happening(owner, " undoused ", target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("undousing ");
        list.addAll(StringChoice.YouYourself(a.getTargets()));
        return list;
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }
}

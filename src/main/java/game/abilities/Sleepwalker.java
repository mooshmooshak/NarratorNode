package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Sleepwalker extends Passive {

    public static final AbilityType abilityType = AbilityType.Sleepwalker;

    public Sleepwalker(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Wander around at night visiting people.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.noAcceptableTargets();
    }

    private static Player getRandomPlayer(Player walker) {
        Random r = walker.game.getRandom();
        PlayerList living = walker.game.getLivePlayers();
        GameAbility ability = walker.getAbility(Sleepwalker.class);
        if(!ability.modifiers.getBoolean(AbilityModifierName.SELF_TARGET, Optional.of(walker.game.players.size()),
                ability.getDefaultSelfTargetValue()))
            living.remove(walker);
        return living.getRandom(r);
    }

    @Override
    public void onPlayerNightEnd(Player walker) {
        super.onPlayerNightEnd(walker);
        if(walker.isJailed())
            return;

        ActionList actions = walker.getActions();
        Action vestAction = walker.getAction(Survivor.abilityType);
        if(vestAction != null){
            vestAction.setTarget(getRandomPlayer(walker));
            return;
        }

        if(actions.mainAction == null)
            actions.mainAction = new Action(walker, Visit.abilityType, getRandomPlayer(walker));

        // possibility of making mainAction an array and adding it here. needs option
        // for visits someone if they have other abilities.
    }

    @Override
    public boolean isChargeModifiable() {
        return false;
    }

    @Override
    public boolean isHiddenModifiable() {
        return true;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return true;
    }

    public static FactionRole template(Faction faction, FactionRole citizen) {
        Role sleepwalker = RoleService.createRole(faction.setup, "Sleepwalker", Sleepwalker.abilityType);
        FactionRole factionRole = FactionRoleService.createFactionRole(faction, sleepwalker);
        FactionRoleService.setRoleReceive(factionRole, citizen);
        return factionRole;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }
}

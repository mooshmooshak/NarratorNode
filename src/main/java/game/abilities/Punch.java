package game.abilities;

import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.DeathAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class Punch extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Punch;
    public static final String COMMAND = abilityType.command;

    // this should never be null, right?
    public Punch(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    private boolean usedPunchToday = false;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public void doNightAction(Action a) {
        return;
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        return new String[0];
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return "Attacks someone, with a random chance of it succeeding.";
    }

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.owner.isSilenced())
            throw new PlayerTargetingException("You can't punch people because you're silenced.");
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public PlayerList getAcceptableTargets(Player puncher) {
        return puncher.game.getLivePlayers().remove(puncher);
    }

    @Override
    public boolean canUseDuringDay(Player player) {
        return !usedPunchToday && !player.isSilenced() && !player.isPuppeted() && player.game.isDay();
    }

    @Override
    public void doDayAction(Action action, Action parentAction) {
        this.usedPunchToday = true;
        punch(action);
    }

    public static void punch(Action action) {
        Player puncher = action.owner;
        Player target = action.getTarget();
        Game narrator = puncher.game;
        int punchRating = puncher.getPunchRating();

        boolean success;
        if(punchRating == 0){
            success = false;
        }else if(punchRating >= 100){
            success = true;
        }else{
            success = (narrator.getRandom().nextDouble() * 100) < punchRating;
        }

        if(target.isInvulnerable())
            success = false;

        DeathAnnouncement e = new DeathAnnouncement(target);
        e.setPicture("armorsmith");

        if(success)
            e.add("Crack! ", puncher, "'s mean right hook knocked out ", target, ". For good...");
        else
            e.add("Swing! ", puncher, " was too slow for ", target, "'s quick moves. Punch evaded!");

        narrator.announcement(e);

        PlayerList deadPeople = new PlayerList();
        if(success){
            target.setDead(puncher);
            target.getDeathType().addDeath(Constants.PUNCH_KILL_FLAG);
            deadPeople.add(target);
        }

        DeathAnnouncement explosion = null;

        if((target.is(ElectroManiac.abilityType) || target.isCharged())
                && (puncher.isCharged() || puncher.is(ElectroManiac.abilityType))){
            if(!puncher.isInvulnerable() && !puncher.is(ElectroManiac.abilityType)){
                boolean overrideChecks = true;
                puncher.dayKill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, puncher, puncher.getCharger()),
                        action.timeLeft, overrideChecks);
                deadPeople.add(puncher);
            }
            if(!target.isInvulnerable() && !target.is(ElectroManiac.abilityType))
                target.getDeathType().addDeath(Constants.ELECTRO_KILL_FLAG);
            explosion = Burn.ExplosionAnnouncement(narrator, deadPeople, "electromaniac");
            narrator.announcement(explosion);
        }

        narrator.voteSystem.removeVotersOf(deadPeople);
        narrator.checkProgress();
        if(!narrator.isInProgress())
            return;
        narrator.checkVote(action.timeLeft);

        if(!success)
            target = null;
        if(narrator.isInProgress()){
            List<NarratorListener> listeners = narrator.getListeners();
            for(NarratorListener listener: listeners){
                listener.onAssassination(puncher, target, e);
                if(explosion != null)
                    listener.onElectroExplosion(deadPeople, explosion);
            }

            if(narrator.isDay())
                narrator.parityCheck();
        }
    }

    @Override
    public String getUsage(Player p, Random r) {
        return GameAbility.getDefaultUsage(this, p, r);
    }

    @Override
    public boolean stopsParity(Player p) {
        return canUseDuringDay(p);
    }

    @Override
    public void onDayStart(Player p) {
        this.usedPunchToday = false;
    }

}

package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.ActionUtil;

public class Coward extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Coward;
    public static final String COMMAND = abilityType.command;

    public Coward(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String ROLE_NAME = "Coward";

    public static final String NIGHT_ACTION_DESCRIPTION = "Hide behind someone, redirecting all actions to the target";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        target.secondaryCause(owner);

        owner.setHide(target);
        happening(owner, " hid behind ", target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    protected String getProfileChargeText() {

        int charges = getPerceivedCharges();
        if(charges == Constants.UNLIMITED){
            return ("You can hide as many times as you want.");
        }else if(charges == 0){
            return ("You can't hide anymore.");
        }else if(charges == 1){
            return ("You can hide " + charges + " more time.");
        }else{
            return ("You can hide " + charges + " more times.");
        }
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = ActionUtil.getActionTargets(actions);

        list.add("hide behind ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Coward", abilityType);
    }
}

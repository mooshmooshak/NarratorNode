package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Happening;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Operator extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Operator;
    public static final String COMMAND = abilityType.command;

    public Operator(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Switch the intended targets of two people.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public int getDefaultTargetSize() {
        return 2;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);

        if(a.getTargets().getFirst() == a.getTargets().getLast())
            AbilityValidationUtil.Exception("This ability can't be used on the same person");
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();

        for(Action da: actionList){
            Player t1 = da._targets.getFirst();
            Player t2 = da._targets.getLast();

            list.add("switch ");
            list.add(StringChoice.YouYourselfSingle(t1));
            list.add(" and ");
            list.add(StringChoice.YouYourselfSingle(t2));

            list.add(", ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    private static int getTelePlayerSize(Player p) {
        int pCount = 0;

        PlayerList targets;
        for(Action a: p.getActions()){
            if(a.abilityType == FactionSend.abilityType)
                continue;
            targets = a.getTargets();
            if(targets.isEmpty())
                pCount += 1;
            else
                pCount += targets.size();
        }

        return pCount;
    }

    private static Player getTeleIndex(int i, Player p) {
        for(Action a: p.getActions()){
            if(a.abilityType == FactionSend.abilityType)
                continue;
            if(a.getTargets().isEmpty()){
                if(i == 0)
                    return p;
                i--;
            }else{
                if(i < a.getTargets().size())
                    return a.getTargets().get(i);
                i -= a.getTargets().size();
            }
        }
        return p;
    }

    public static void setTeleIndex(int i, Player p, Player newTarget) {
        for(Action a: p.getActions()){
            if(a.abilityType == FactionSend.abilityType)
                continue;
            if(a.getTargets().isEmpty()){
                if(i == 0){
                    a._targets.add(newTarget);
                    return;
                }
                i--;
            }else{
                if(i < a.getTargets().size()){
                    a._targets.set(i, newTarget);
                    return;
                }
                i -= a.getTargets().size();

            }
        }
    }

    public static final String FEEDBACK = Witch.WITCH_FEEDBACK;

    @Override
    public void doNightAction(Action da) {
        Player op = da.owner, t1 = da._targets.getFirst(), t2 = da._targets.getLast();
        // these are the intended targets. making sure they aren't null
        if(t1 == null)
            return;

        if(!t1.isInTown() && !t2.isInTown())
            return;

        if(!t1.isInTown()){
            da.markCompleted();
            Visit.NoNightActionVisit(op, t2);
            return;
        }

        if(!t2.isInTown()){
            da.markCompleted();
            Visit.NoNightActionVisit(op, t1);
            return;
        }

        if(t1 == t2){
            Visit.NoNightActionVisit(da);
            return;
        }

        int t1ActionSize = getTelePlayerSize(t1);
        int t2ActionSize = getTelePlayerSize(t2);

        Player target1, target2;
        for(int i = 0; i < t1ActionSize && i < t2ActionSize; i++){
            target1 = getTeleIndex(i, t1);
            target2 = getTeleIndex(i, t2);

            setTeleIndex(i, t1, target2);
            setTeleIndex(i, t2, target1);
        }
        new Happening(op.game).add(op, " switched ", t1, " and ", t2, ".");

        Witch.ManipulatedFeedback(t1, t2);

        da.markCompleted();
        op.visit(t1, t2);
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("switching ");
        list.add(StringChoice.YouYourself(a.getTargets()));

        return list;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return p.game.getLivePlayers().sortByName();
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    @Override
    public boolean isPositiveAbility() {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.game.getLivePlayers().remove(p);
        live.shuffle(r, null);
        return COMMAND + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    public static FactionRole template(Faction faction) {
        Role role = template(faction.setup);
        return FactionRoleService.createFactionRole(faction, role);
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Operator", Operator.abilityType);
    }
}

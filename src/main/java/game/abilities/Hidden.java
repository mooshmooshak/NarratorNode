package game.abilities;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.ai.RoleGroup;
import game.logic.Game;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.HiddenSpawn;
import models.Role;
import models.enums.AbilityType;
import models.schemas.HiddenSchema;
import util.game.HiddenUtil;
import util.game.LookupUtil;

public class Hidden {

    public String name;
    public Setup setup;
    public long id;

    // this needs to be a linked hash map, or else the order from values() is not
    // consistent
    public List<HiddenSpawn> spawns = new LinkedList<>();

    public Hidden(HiddenSchema schema, Setup setup) {
        this.name = schema.name;
        this.setup = setup;
        this.id = schema.id;
    }

    public String getName() {
        return name;
    }

    public boolean contains(FactionRole factionRole, Game game) {
        return this.getSpawningFactionRoles(game).contains(factionRole);
    }

    public boolean contains(Role role) {
        for(HiddenSpawn hiddenSpawn: spawns){
            if(hiddenSpawn.factionRole.role == role)
                return true;
        }
        return false;
    }

    public Set<FactionRole> getAllFactionRoles() {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(HiddenSpawn hiddenSpawn: spawns)
            factionRoles.add(hiddenSpawn.factionRole);
        return factionRoles;
    }

    public RoleGroup combine(Hidden r2) {
        RoleGroup roleGroup = new RoleGroup();
        roleGroup.add(this, r2);
        return roleGroup;
    }

    public Set<String> getFactionColors() {
        Set<String> colors = new HashSet<>();
        for(HiddenSpawn hiddenSpawn: spawns)
            colors.add(hiddenSpawn.factionRole.getColor());
        return colors;
    }

    public void removeFactionRole(FactionRole m) {
        for(HiddenSpawn hiddenSpawn: new LinkedList<>(this.spawns)){
            if(hiddenSpawn.factionRole == m)
                spawns.remove(hiddenSpawn);
        }
    }

    public boolean hasMultiAlignment() {
        return getColors().size() > 1;
    }

    // rename to size()
    public int size() {
        return this.getAllFactionRoles().size();
    }

    public boolean isHiddenSingle() {
        return spawns.size() == 1;
    }

    public static Hidden TownRandom() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.TOWN_RANDOM_ROLE_NAME).get();
    }

    public static Hidden TownInvestigative() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.TOWN_INVESTIGATIVE_ROLE_NAME).get();
    }

    public static Hidden TownProtective() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.TOWN_PROTECTIVE_ROLE_NAME).get();
    }

    public static Hidden TownKilling() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.TOWN_KILLING_ROLE_NAME).get();
    }

    public static Hidden TownGovernment() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.TOWN_GOVERNMENT_ROLE_NAME).get();
    }

    public static Hidden MafiaRandom() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.MAFIA_RANDOM_ROLE_NAME).get();
    }

    public static Hidden YakuzaRandom() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.YAKUZA_RANDOM_ROLE_NAME).get();
    }

    public static Hidden NeutralRandom() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.NEUTRAL_RANDOM_ROLE_NAME).get();
    }

    public static Hidden NeutralKillingRandom() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.NEUTRAL_KILLING_RANDOM_ROLE_NAME).get();
    }

    public static Hidden NeutralBenignRandom() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.NEUTRAL_BENIGN_RANDOM_ROLE_NAME).get();
    }

    public static Hidden AnyRandom() {
        return LookupUtil.findHidden(BasicRoles.setup, Constants.ANY_RANDOM_ROLE_NAME).get();
    }

    @Override
    public String toString() {
        if(spawns.size() == 1)
            return spawns.iterator().next().factionRole.toString();
        return name;
    }

    public Set<String> getColors() {
        Set<String> colors = new HashSet<String>();
        for(HiddenSpawn hiddenSpawn: spawns)
            colors.add(hiddenSpawn.factionRole.getColor());
        return colors;
    }

    public Set<Faction> getFactions() {
        Set<Faction> factions = new HashSet<>();
        for(HiddenSpawn hiddenSpawn: spawns)
            factions.add(hiddenSpawn.factionRole.faction);
        return factions;
    }

    public boolean contains(AbilityType abilityType) {
        for(HiddenSpawn hiddenSpawn: this.spawns){
            if(hiddenSpawn.factionRole.role.hasAbility(abilityType))
                return true;
        }
        return false;
    }

    public Set<FactionRole> getSpawningFactionRoles(Optional<Game> game) {
        if(game.isPresent())
            return getSpawningFactionRoles(game.get());
        return getAllFactionRoles();
    }

    public Set<FactionRole> getSpawningFactionRoles(Game game) {
        return getSpawningFactionRoles(game, this.spawns);
    }

    public static Set<FactionRole> getSpawningFactionRoles(Game game, Collection<HiddenSpawn> factionRoles) {
        Set<FactionRole> spawning = new HashSet<>();
        for(HiddenSpawn hiddenSpawn: factionRoles){
            if(HiddenUtil.spawnable(game, hiddenSpawn))
                spawning.add(hiddenSpawn.factionRole);
        }
        return spawning;
    }

    public Set<HiddenSpawn> getSpawningSpawns(Optional<Game> optGame) {
        if(!optGame.isPresent())
            return new HashSet<>(this.spawns);
        Game game = optGame.get();
        Set<HiddenSpawn> spawning = new HashSet<>();
        for(HiddenSpawn hiddenSpawn: this.spawns){
            if(HiddenUtil.spawnable(game, hiddenSpawn))
                spawning.add(hiddenSpawn);
        }
        return spawning;
    }

}

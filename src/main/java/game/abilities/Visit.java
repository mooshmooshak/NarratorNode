package game.abilities;

import java.util.LinkedList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class Visit extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Visit;
    public static final String COMMAND = abilityType.command;

    public Visit(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public Visit(Game game) {
        this(game, game.setup, new Modifiers<>());
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        return new String[] {};
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return null;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.noAcceptableTargets();
    }

    @Override
    public void doNightAction(Action a) {
        Visit.NoNightActionVisit(a);
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static void NoNightActionVisit(Action a) {
        Player owner = a.owner;
        Player target = a.getTarget();
        a.markCompleted();
        NoNightActionVisit(owner, target);
    }

    public static void NoNightActionVisit(Player owner, Player target) {
        if(target == null)
            return;
        GameAbility.happening(owner, " visited ", target);
        owner.visit(target);
    }

    public static Action getAction(Controller visitor, Controller target, Game game) {
        return new Action(visitor.getPlayer(), abilityType, new LinkedList<>(),
                ControllerList.ToPlayerList(game, target));
    }
}

package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Block extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Block;
    public static final String COMMAND = "Block";

    public Block(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Entertain at night. Targets will not be able to complete any pending night actions.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.BLOCK_FEEDBACK);
        return names;
    }

    public static final String FEEDBACK = "An attractive visitor occupied your night. You were unable to do anything else.";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();

        if(target == null)
            return;

        target.setBlocked();
        happening(owner, " blocked ", target);

        a.markCompleted();
        if(target.getAction(Veteran.abilityType) != null){

        }else if(!target.isBlockable()){

        }else{
            for(Action ab: target.getActions().getActions()){
                ab.setInvalid();
            }
        }
        owner.visit(target);
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.BLOCK_FEEDBACK)){
            ret.add("Your targets will be know that they couldn't complete their action(s).");
        }else{
            ret.add("Your targets will not know that you visited them.");
        }
        return ret;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    public static void FeedbackGenerator(Feedback f) {
        f.add(FEEDBACK).setPicture("stripper").addExtraInfo("None of your actions went through last night!");
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Stripper", Block.abilityType);
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }
}

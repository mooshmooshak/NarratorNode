package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.modifiers.Modifiers;

public abstract class Passive extends GameAbility {

    public Passive(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public boolean hasCooldownAbility() {
        return false;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.noAcceptableTargets();
    }

    @Override
    public void doNightAction(Action a) {
        Visit.NoNightActionVisit(a);
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    @Override
    public boolean isChargeModifiable() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return null;
    }

    @Override
    public boolean canSubmitFree(Optional<Integer> playerCount) {
        return false;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public boolean isZeroWeightModifiable() {
        return false;
    }
}

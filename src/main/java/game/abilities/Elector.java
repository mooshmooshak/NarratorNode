package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Elector extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Elector;
    public static final String COMMAND = abilityType.command;

    public Elector(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Steal vote power from one person, giving it to another";

    public static final String LOST_FEEDBACK = "You've lost vote power!";
    public static final String GAINED_FEEDBACK = "You've gained vote power!";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void doNightAction(Action a) {
        if(a._targets.isEmpty())
            return;
        Player giver = a._targets.getFirst();
        if(a._targets.size() < 2){
            Visit.NoNightActionVisit(a);
            return;
        }
        Player receiver = a._targets.getLast();
        if(receiver == giver){
            Visit.NoNightActionVisit(a);
            return;
        }

        giver.transferVotePower(receiver);

        giveFeedback(giver, receiver);

        new Happening(a.owner.game).add(a.owner, " removed a vote from ", giver, " and gave it to ", receiver, ".");
        a.markCompleted();
        a.owner.visit(giver, receiver);
    }

    private static void giveFeedback(Player giver, Player receiver) {
        Feedback feedback;
        boolean giveGainedFeedback = true;
        for(int i = 0; i < receiver.feedback.size(); i++){
            feedback = receiver.feedback.get(i);
            if(feedback.access(receiver).equals(LOST_FEEDBACK)){
                giveGainedFeedback = false;
                receiver.feedback.remove(i);
                break;
            }
        }
        if(giveGainedFeedback)
            new Feedback(receiver, GAINED_FEEDBACK).hideableFeedback = false;

        boolean giveLostFeedback = true;
        for(int i = 0; i < giver.feedback.size(); i++){
            feedback = giver.feedback.get(i);
            if(feedback.access(giver).equals(GAINED_FEEDBACK)){
                giveLostFeedback = false;
                giver.feedback.remove(i);
                break;
            }
        }
        if(giveLostFeedback)
            new Feedback(giver, LOST_FEEDBACK).hideableFeedback = false;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() != 2)
            AbilityValidationUtil.Exception("This ability requires two targets.");
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    @Override
    public int getDefaultTargetSize() {
        return 2;
    }

    @Override
    public void selfTargetableCheck(Action action) {
        PlayerList targets = action._targets;
        if(targets.getFirst() == targets.getLast())
            throw new PlayerTargetingException("No reason to cause self-targets");
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();

        for(Action da: actionList){
            Player victim = da._targets.getFirst();
            Player newTarget = da._targets.getLast();

            if(victim == null && newTarget == null)
                continue;
            list.add("remove a vote from ");
            list.add(StringChoice.YouYourselfSingle(victim));
            list.add(" and give it to ");
            list.add(StringChoice.YouYourselfSingle(newTarget));

            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return p.game.getLivePlayers().sortByName();
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Elector", Elector.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

}

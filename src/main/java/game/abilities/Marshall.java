package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Announcement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.listeners.NarratorListener;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;

public class Marshall extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Marshall;
    public static final String COMMAND = abilityType.command;
    public static final String ORDER_LOWERCASE = COMMAND.toLowerCase();

    public Marshall(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    private boolean revealedToday = false;
    private boolean revealed = false;

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }
    // private int voteCount;

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(revealedToday)
            return new PlayerList();
        return null;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.MARSHALL_EXECUTIONS);
        names.add(SetupModifierName.MARSHALL_QUICK_REVEAL);
        return names;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Call the town to order to enable multiple public executions.";

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.owner.isSilenced())
            throw new IllegalActionException("Can't reveal if blackmailed");
    }

    @Override
    public void doNightAction(Action a) {
        Visit.NoNightActionVisit(a);
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return !revealedToday && !p.isSilenced() && !p.isPuppeted() && this.getRealCharges() != 0;
    }

    @Override
    public void doDayAction(Action action, Action parentAction) {
        Player owner = action.owner;
        Game narrator = owner.game;
        Announcement e = new Announcement(narrator);
        e.setPicture("mayor");

        StringChoice sc = new StringChoice(owner);
        sc.add(owner, "You");
        e.add(sc, " ");

        sc = new StringChoice("has");
        sc.add(owner, "have");

        e.add(sc, " revealed as the ", new HTString(owner.gameRole), "!");

        revealedToday = true;
        revealed = true;
        useCharge();

        e.finalize();

        narrator.addDayLynches(narrator.getInt(SetupModifierName.MARSHALL_EXECUTIONS)); // how many you're adding

        List<NarratorListener> listeners = narrator.getListeners();
        for(NarratorListener listener: listeners)
            listener.onRoleReveal(owner, e);
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        int executionCount = this.getPerceivedCharges();

        if(this.revealed)
            ret.add("You've already revealed yourself, but you can still enable another " + executionCount
                    + " public execution day tomorrow.");
        else
            ret.add("You may reveal yourself during the day.  In addition you will enable a special " + executionCount
                    + " public execution day.");

        if(p.game.getBool(SetupModifierName.MARSHALL_QUICK_REVEAL))
            ret.add("You will immediately know what the role flips are after day executions.");
        else
            ret.add("You will know role flips after day is over.");

        return ret;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        this.revealedToday = false;
    }

    public static boolean isCurrentMarshallLynch(Player p) {
        return !p.game.getBool(SetupModifierName.MARSHALL_QUICK_REVEAL) && p.isDead() && p.getDeathType().isLynch()
                && p.getDeathDay() == p.game.getDayNumber() && p.game.isDay() && !p.game.isTransitioningToNight;
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.empty();
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            AbilityValidationUtil.Exception("You can't target anyone with that ability");
    }
}

package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.support.Gun;
import game.abilities.support.Vest;
import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.Util;
import util.game.GameUtil;

public class Blacksmith extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Blacksmith;
    public static final String COMMAND = abilityType.command;

    public Blacksmith(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String FAULTY = Gunsmith.FAULTY;
    public static final String REAL = Gunsmith.REAL;
    public static final String FAKE = Armorsmith.FAKE;
    public static final String NOGUN = "no";
    public static final String NOARMOR = NOGUN;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        return GetNightActionDescription(playerCount, setup);
    }

    public static String GetNightActionDescription(Optional<Integer> playerCount, Setup setup) {
        return "Hand out " + Gun.getGunName(playerCount, setup) + " or " + Vest.getVestName(playerCount, setup)
                + " so others can use them.";
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() == 0)
            AbilityValidationUtil.Exception("You must select at least one target.");
        if(a.getTargets().size() > 2)
            AbilityValidationUtil.Exception("You can't select that many targets");
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        AbilityValidationUtil.noTeamMateTargeting(a);

        Game n = a.owner.game;
        PlayerList targets = a.getTargets();
        String gunOpt = a.getArg1();
        String vestOpt = a.getArg2();

        String gunName = Gun.getGunName(n);
        String vestName = Vest.getVestName(n);

        if(gunOpt == null || vestOpt == null)
            AbilityValidationUtil
                    .Exception("Must specify if you're using real " + gunName + "s or " + vestName + "s with options.");

        if(faultyGuns()){
            if(!REAL.equalsIgnoreCase(gunOpt) && !FAULTY.equalsIgnoreCase(gunOpt) && !NOGUN.equalsIgnoreCase(gunOpt))
                AbilityValidationUtil.Exception("Can't tell whether " + gunName + " should be fake or real.");
        }else if(FAULTY.equalsIgnoreCase(gunOpt))
            AbilityValidationUtil.Exception("Can't pass out faulty " + gunName + "s.");

        if(fakeVests()){
            if(!REAL.equalsIgnoreCase(vestOpt) && !FAKE.equalsIgnoreCase(vestOpt) && !NOGUN.equalsIgnoreCase(vestOpt))
                AbilityValidationUtil.Exception("Can't tell whether " + vestName + " should be fake or real.");
        }else if(FAKE.equalsIgnoreCase(vestOpt))
            AbilityValidationUtil.Exception("Can't pass out fake " + vestName + "s.");

        if(gunOpt == null && vestOpt == null && targets.size() == 1){
            AbilityValidationUtil.Exception("Unable to determine what single item should be passed out.");
        }

        if(targets.size() == 2){
            if(targets.getLast() == targets.getFirst())
                AbilityValidationUtil
                        .Exception("Cannot give a " + gunName + " and " + vestName + " to the same person");
            if(NOGUN.equalsIgnoreCase(gunOpt))
                AbilityValidationUtil.Exception("Cannot give two " + vestName + "s to a person");
        }

        if(a.getTargets().size() == 1){
            if(!NOGUN.equalsIgnoreCase(a.getArg1())){
                a.args = Util.toStringList(a.getArg1(), NOARMOR);
            }else if(a.getArg1() == null){
                a.args = Util.toStringList(NOGUN);
            }
        }

    }

    protected static ArrayList<Option> getGunOptions(boolean canFakeGun, Game game) {
        String g_alias = Gun.getGunName(game);
        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option(REAL, REAL + " " + g_alias));
        if(canFakeGun)
            options.add(new Option(FAULTY, FAULTY + " " + g_alias));
        options.add(new Option(NOGUN, NOGUN + " " + g_alias));
        return options;
    }

    protected static ArrayList<Option> getArmorOptions(boolean canFakeArmor, Game game) {
        String v_alias = Vest.getVestName(game);

        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option(REAL, REAL + " " + v_alias));
        if(canFakeArmor)
            options.add(new Option(FAKE, FAKE + " " + v_alias));
        options.add(new Option(NOGUN, NOGUN + " " + v_alias));
        return options;
    }

    @Override
    public ArrayList<Option> getOptions(Player player) {
        return getGunOptions(faultyGuns(), player.game);
    }

    @Override
    public ArrayList<Option> getOptions2(Player player, String option1) {
        return getArmorOptions(fakeVests(), player.game);
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        if(actions.isEmpty())
            return list;
        Game n = actions.get(0).owner.game;

        PlayerList targets;
        Player target;
        String gunOpt, vestOpt;
        String gunName = Gun.getGunName(n);
        String vestName = Vest.getVestName(n);
        for(Action a: actions){
            targets = a.getTargets();
            gunOpt = a.getArg1();
            vestOpt = a.getArg2();
            for(int i = 0; i < targets.size(); i++){
                target = targets.get(i);
                if(i == 0 && !NOGUN.equalsIgnoreCase(gunOpt)){
                    if(FAULTY.equalsIgnoreCase(gunOpt))
                        list.add("give a faulty " + gunName + " to ");
                    else
                        list.add("give a " + gunName + " to ");
                }else{
                    if(FAKE.equals(vestOpt)){
                        list.add("give a fake " + vestName + " to ");
                    }else{
                        list.add("give a " + vestName + " to ");
                    }
                }

                list.add(StringChoice.YouYourselfSingle(target));
                if(i == 0 && targets.size() > 1){
                    list.add(" and ");
                }
            }
        }

        return list;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, gTarget = a.getTargets().getFirst(), vTarget = a.getTargets().getLast();

        if(vTarget == gTarget)
            vTarget = null;

        if(vTarget == null && gTarget == null)
            return;

        String gOpt = a.getArg1(), vOpt = a.getArg2();

        if(NOGUN.equalsIgnoreCase(gOpt) || (gOpt == null && FAKE.equalsIgnoreCase(vOpt))){
            vTarget = gTarget;
            gTarget = null;
        }

        Armorsmith.ArmorsmithAction(owner, vTarget, vOpt);
        Gunsmith.GunsmithAction(owner, gTarget, gOpt);

        a.markCompleted();
        if(vTarget != null && gTarget != null)
            owner.visit(vTarget, gTarget);
        else if(vTarget != null)
            owner.visit(vTarget);
        else
            owner.visit(gTarget);
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        String gunName = Gun.getGunName(p.game);
        String vestName = Vest.getVestName(p.game);

        if(p.getGameFaction().knowsTeam() && p.getGameFaction().getMembers().size() > 1){
            ret.add("Your " + gunName + " and " + vestName + " cannot go to teammates.");
        }
        if(faultyGuns())
            ret.add("Your faulty " + gunName + "s will kill the shooter.");
        if(fakeVests())
            ret.add("Your fake " + vestName + "s will not protect the ");

        return ret;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.GS_DAY_GUNS);
        return names;
    }

    @Override
    public List<AbilityModifierName> getAbilityModifiers() {
        List<AbilityModifierName> modifierNames = new LinkedList<>();
        modifierNames.add(AbilityModifierName.GS_FAULTY_GUNS);
        modifierNames.add(AbilityModifierName.AS_FAKE_VESTS);
        modifierNames.addAll(super.getAbilityModifiers());
        return modifierNames;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        Action a;
        Game n = p.game;

        String last = commands.get(commands.size() - 1);
        // no options
        if(n.getPlayerByName(last) != null)
            return super.parseCommand(p, timeLeft, commands);

        String last2 = commands.get(commands.size() - 2);
        // just one option
        if(n.getPlayerByName(last2) != null){
            a = super.parseCommand(p, timeLeft, commands);
            // it's a gun option
            if(last.equalsIgnoreCase(NOGUN) || last.equalsIgnoreCase(FAULTY))
                a.args = Util.toStringList(last);
            else
                a.args = Util.toStringList("", last);
            return a;
        }

        commands.remove(commands.size() - 1);
        if(commands.isEmpty())
            throw new PlayerTargetingException("Couldn't find the playerName in question.");
        commands.remove(commands.size() - 1);
        if(commands.isEmpty())
            throw new PlayerTargetingException("Couldn't find the playerName in question.");
        a = super.parseCommand(p, timeLeft, commands);
        a.args = Util.toStringList(last2, last);
        return a;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.game.getLivePlayers();
        live.shuffle(r, null);
        String ret = getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "* " + REAL
                + " " + REAL + " (For both a gun and a vest)";
        ret += "\n" + getCommand() + " *" + live.getFirst().getName() + "* " + NOGUN + " " + REAL
                + " (To only give armor)";
        ret += "\n" + getCommand() + " *" + live.getFirst().getName() + "* " + REAL + " " + NOARMOR
                + " (To only give a gun)";
        if(faultyGuns())
            ret += "\n" + getCommand() + " *" + live.getFirst().getName() + " " + FAULTY + " " + NOARMOR;
        if(fakeVests())
            ret += "\n" + getCommand() + " *" + live.getFirst().getName() + " " + NOGUN + " " + FAKE;
        return ret;
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> parts = super.getCommandParts(action);
        if(action.getArg1() != null)
            parts.add(action.getArg1());
        if(action.getArg2() != null)
            parts.add(action.getArg2());

        return parts;
    }

    public boolean faultyGuns() {
        return FaultyGuns(this);
    }

    public boolean fakeVests() {
        return FakeVests(this);
    }

    public static boolean FaultyGuns(GameAbility r) {
        return Gunsmith.FaultyGuns(r);
    }

    public static boolean FakeVests(GameAbility r) {
        return Armorsmith.FakeVests(r);
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return p.game.getLivePlayers().sortByName().remove(p);
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Blacksmith", abilityType);
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static FactionRole templateEvil(Faction faction) {
        FactionRole factionRole = template(faction);
        RoleAbilityModifierService.upsert(factionRole.role, Blacksmith.abilityType, AbilityModifierName.AS_FAKE_VESTS,
                true);
        RoleAbilityModifierService.upsert(factionRole.role, Blacksmith.abilityType, AbilityModifierName.GS_FAULTY_GUNS,
                true);
        return factionRole;
    }

    public static Action getAction(Controller bs, ControllerList players, String opt2, String opt3, Game game) {
        List<String> args = Util.toStringList(opt2, opt3);
        return new Action(bs.getPlayer(), abilityType, args, players.toPlayerList(game));
    }
}

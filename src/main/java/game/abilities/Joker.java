package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.support.Bounty;
import game.abilities.support.BountyList;
import game.abilities.support.ElectrocutionException;
import game.abilities.util.AbilityValidationUtil;
import game.event.Announcement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;
import util.game.GameUtil;

public class Joker extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Joker;
    public static final String COMMAND = abilityType.command;

    public Joker(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Place a bounty every night. Your target will be publicly revealed. If your bounty is still alive after a delay, you may kill.";

    public int bounty_kills = 0;
    public Bounty activeBounty;
    private ArrayList<Bounty> sideBounties;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public GameAbility initialize(Player p) {
        if(narrator.setup.hasPossibleFactionRoleWithAbility(narrator, Baker.abilityType))
            sideBounties = new ArrayList<>();

        return super.initialize(p);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        int days = setup.modifiers.getInt(SetupModifierName.BOUNTY_INCUBATION, playerCount);
        int kills = setup.modifiers.getInt(SetupModifierName.BOUNTY_INCUBATION, playerCount);
        return "set a bounty on someone.  If they aren't killed within " + days + " days, you may kill " + kills
                + " people.";
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        return Util.ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    public static boolean CanAddBountyActionTonight(Player joker) {
        Joker card = joker.getAbility(Joker.class);
        int bountySubmissions = 0;
        for(Action a: joker.getActions()){
            if(a.is(Joker.abilityType) & a.getArg1() == null)
                bountySubmissions++;
        }
        if(card.activeBounty == null && 0 == bountySubmissions)
            return true;
        if(card.sideBounties == null)
            return false;
        int useableBread = BreadAbility.getUseableBread(joker);
        return bountySubmissions < useableBread;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        if(BreadAbility.getUseableBread(a.owner) == 0 && activeBounty != null)
            AbilityValidationUtil.Exception("You already have a bounty out");
        if(a.getArg1() == null){
            PlayerList targets = a.getTargets();
            Player bounty = targets.remove(0);
            if(bounty.in(targets)){
                AbilityValidationUtil.Exception("Can't kill someone you're trying to bounty");
            }
        }
    }

    public static final String DEATH_FEEDBACK = "You are a casualty of the failed bounty!";

    public static final String[] BOUNTY_KILL_FLAG = Constants.JOKER_KILL_FLAG;

    public static final String NO_BOUNTY = "kill";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        if(a.getTarget() == null)
            return;
        PlayerList targets = a.getTargets();
        Player newBounty = null;

        if(a.getArg1() == null && !Commuter.CommutingTargets(a)){ // this mean a bounty is to be submitted
            targets.remove(0);
            newBounty = a.getActualTargets().getFirst();
        }

        PlayerList electrod = new PlayerList();

        for(Player p: targets){
            this.bounty_kills--;
            try{
                Kill(owner, p, BOUNTY_KILL_FLAG);// visiting taken care of already in kill
            }catch(ElectrocutionException e){
                electrod.add(e.charged);
            }
        }

        Player bountySetter = null;
        if(owner.isAlive()){
            bountySetter = owner;
        }else{
            for(Player p: owner.game.getLivePlayers()){
                if(p.is(GraveDigger.abilityType)){
                    for(Action gd_action: p.getActions().getActions(GraveDigger.abilityType)){
                        if(gd_action.getTargets().getFirst() == owner && gd_action.getArg2() == null){
                            bountySetter = p;
                            break;
                        }
                    }
                }
            }
        }

        if(newBounty != null && bountySetter != null){

            Bounty b = newBounty.setBounty(bountySetter);
            if(activeBounty == null)
                activeBounty = b;
            else
                sideBounties.add(b);
            happening(owner, " placed a bounty on ", newBounty);
            try{
                owner.visit(newBounty);
            }catch(ElectrocutionException e){
                electrod.add(e.charged);
            }
        }
        a.markCompleted();
        ElectrocutionException.throwException(owner, electrod);
    }

    private PlayerList getBounties() {
        PlayerList pl = new PlayerList();

        if(activeBounty != null){
            pl.add(activeBounty.target);
        }

        if(sideBounties != null)
            for(Bounty b: sideBounties){
                pl.add(b.target);
            }

        return pl;
    }

    @Override
    public void onDayStart(Player p) {
        if(!p.game.isInProgress())
            return;

        if(activeBounty != null)
            activeBounty.dayAnnounce();
        if(sideBounties != null)
            for(Bounty b: sideBounties)
                b.dayAnnounce();
    }

    @Override
    public void onDayEnd(Player p) {
        super.onDayEnd(p);

        int reward = p.game.getInt(SetupModifierName.BOUNTY_FAIL_REWARD);

        BountyList bs;
        for(Player bounty: getBounties()){
            if(!bounty.isBountied())
                continue;
            bs = bounty.getBounties();
            this.bounty_kills += bs.awardBounties(p).size() * reward;
        }
        if(activeBounty != null && (activeBounty.target.isDead() || activeBounty.failed())){
            activeBounty.onFail();
            activeBounty = null;
        }

        Bounty b;
        if(sideBounties != null)
            for(int i = 0; sideBounties != null && i < sideBounties.size(); i++){
                b = sideBounties.get(i);
                if(b.target.isDead() || b.failed()){
                    b.onFail();
                    sideBounties.remove(i);
                    i--;
                }
            }
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.BOUNTY_INCUBATION);
        names.add(SetupModifierName.BOUNTY_FAIL_REWARD);
        return names;
    }

    @Override
    public void targetSizeCheck(Action a) {
        int size = a.getTargets().size();
        if(size == 0)
            AbilityValidationUtil.Exception("You must select at least one target");
        if(a.getArg1() == null){
            size--;
        }
        if(size > bounty_kills)
            AbilityValidationUtil.Exception("You can't submit more than " + bounty_kills + " kill(s)");
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList bounties = new PlayerList();
        PlayerList toBeKilled = new PlayerList();
        PlayerList tempTargs;
        for(Action a: actions){
            tempTargs = a.getTargets();
            if(a.getArg1() == null){
                bounties.add(tempTargs.removeFirst());
            }
            toBeKilled.add(tempTargs);
        }

        if(!bounties.isEmpty()){
            list.add("place a bounty on ");
            list.addAll(StringChoice.YouYourself(bounties));
            if(!toBeKilled.isEmpty()){
                list.add(" and ");
            }
        }

        if(!toBeKilled.isEmpty()){
            list.add("kill ");
            list.addAll(StringChoice.YouYourself(toBeKilled));
        }

        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("Instead of ");

        PlayerList targets = a.getTargets();

        if(a.getArg1() != null){
            list.add("placing a bounty on ");
            list.add(StringChoice.YouYourselfSingle(a.getTarget()));
            targets.removeFirst();
            if(targets.isEmpty())
                return list;
            list.add(" and ");
        }

        list.add("killing ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> commands = super.getCommandParts(action);

        if(action.getArg1() != null)
            commands.add(NO_BOUNTY);

        return commands;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        boolean submittingBounty = true;
        if(commands.get(commands.size() - 1).equalsIgnoreCase(NO_BOUNTY)){
            commands.remove(commands.size() - 1);
            submittingBounty = false;
        }
        Action a = super.parseCommand(p, timeLeft, commands);
        if(!submittingBounty)
            a.args = Util.toStringList(NO_BOUNTY);
        return a;
    }

    @Override
    public boolean onDayEndTriggersWhileDead() {
        return true;
    }

    @Override
    public boolean successfulUsageTriggersBreadUsage(Action a) {
        return activeBounty != null && a.getArg1() == null;
    }

    public static Announcement DayStartAnnouncementCreator(Player p, int days) {
        Announcement a = new Announcement(p.game);
        a.add("There is a bounty on ", p, "'s head that will expire in ");
        a.add(Integer.toString(days));
        a.add(Util.plural(" day", days), ".");
        a.setPicture("executioner");
        return a;
    }

    public static Announcement DayEndAnnouncementCreator(Player p) {
        Announcement a = new Announcement(p.game);
        a.setPicture("executioner");
        a.add("The bounty on ", p, " has expired.  Expect deaths.");
        return a;
    }

    /*
     * @Override public int totalActionWeight(ArrayList<Action> actions) {
     * if(activeBounty == null) return super.totalActionWeight(actions); int weight
     * = 0; boolean initialBountySubmitted = false;
     *
     * for(Action a: actions){ if(a.getOption() != null){ weight++; continue; }
     * weight++; if(!initialBountySubmitted){ weight++; initialBountySubmitted =
     * true; }
     *
     * }
     *
     * return weight; }
     */

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        Game n = p.game;
        ret.add("Your bounties last " + n.getInt(SetupModifierName.BOUNTY_INCUBATION) + " day(s) long.");
        ret.add("Upon bounty failures, you may kill " + n.getInt(SetupModifierName.BOUNTY_FAIL_REWARD)
                + " individual(s)");

        return ret;
    }

    @Override
    public boolean canAddAnotherAction(Action a) {
        if(!Joker.CanAddBountyActionTonight(a.owner) && a.getArg1() == null)
            return false;
        return super.canAddAnotherAction(a);
    }

    @Override
    public boolean canSubmitWithoutBread(Action a) {
        if(a.getArg1() != null)
            return super.canAddAnotherAction(a);
        if(this.activeBounty == null)
            return true;
        return false;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Joker", Joker.abilityType, Bulletproof.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    @Override
    public String getSelfTargetMemberDescription(boolean singleAbility, boolean canSelfTarget) {
        return "May " + (canSelfTarget ? "" : "not ") + "set a bounty on yourself.";
    }
}

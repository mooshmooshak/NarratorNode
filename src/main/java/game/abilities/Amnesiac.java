package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.PendingRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleChangeService;
import services.RoleService;
import util.game.ActionUtil;
import util.game.GameUtil;

public class Amnesiac extends GameAbility {

    public Amnesiac(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final AbilityType abilityType = AbilityType.Amnesiac;
    public static final String COMMAND = AbilityType.Amnesiac.command;
    public static final String NIGHT_ACTION_DESCRIPTION = "Permanently change  your role and team to one in the graveyard.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    // any changes must be reflected in getRuleTexsts as well! (next method)
    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.AMNESIAC_KEEPS_CHARGES);
        names.add(SetupModifierName.AMNESIAC_MUST_REMEMBER);
        return names;
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> color, Set<Ability> abilities) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        ArrayList<String> ruleTextList = new ArrayList<String>();

        ruleTextList.add(SetupModifierName.AMNESIAC_KEEPS_CHARGES.getTextValue(playerCount, setup.modifiers));

        if(color.isPresent() && setup.getFactionByColor(color.get()).enemies.isEmpty())
            ruleTextList.add(SetupModifierName.AMNESIAC_MUST_REMEMBER.getTextValue(playerCount, setup.modifiers));

        return ruleTextList;
    }

    public static final String LIVE_FEEDBACK = "You tried to remember who you were, but the person got up and walked away.";
    public static final String ROLE_NAME = "Amnesiac";

    @Override
    public void doNightAction(Action action) {
        Player target = action.getTarget();
        if(target == null)
            return;
        Player owner = action.owner;
        if(target.isAlive()){
            new Feedback(owner, LIVE_FEEDBACK).setPicture("amnesiac")
                    .addExtraInfo("Someone probably changed who you intended to target")
                    .addExtraInfo("Try again tomorrow night!");
            Visit.NoNightActionVisit(action);
            return;
        }
        if(owner.isEnforced()){
            Visit.NoNightActionVisit(action);
            return;
        }
        if(isEnemy(target, owner)){
            Visit.NoNightActionVisit(action);
            return;
        }

        RoleChangeService.changeRoleAndTeam(owner, new PendingRole(target.gameRole));
        happening(owner, " has become like ", target);
        Feedback fb = new Feedback(owner);
        fb.hideableFeedback = false;
        fb.setPicture("amnesiac");

        StringChoice sc = new StringChoice(target);
        sc.add(target, "You");
        fb.add(sc, " remembered you were like ", target, ".");

        action.markCompleted();
        owner.visit(target);
    }

    private static boolean isEnemy(Player target, Player owner) {
        return owner.getGameFaction().faction.enemies.contains(target.getGameFaction().faction);
    }

    @Override
    public void mainAbilityCheck(Action action) {
        AbilityValidationUtil.aliveCheck(action);
        Player target = action.getTarget();
        if(isUniqueAttempt(target))
            AbilityValidationUtil.Exception("Can't target unique roles");
        Player owner = action.owner;
        Faction originalTeam = owner.getGameFaction().faction;

        if(originalTeam.enemies.isEmpty())
            return;

        Optional<FactionRole> factionRoleOpt = target.getGraveyardFactionRole();
        if(!factionRoleOpt.isPresent())
            return;
        FactionRole factionRole = factionRoleOpt.get();

        Set<Faction> factionsWithRole = narrator.setup.getPossibleFactionsWithRoles(narrator, factionRole.role);
        for(Faction t: factionsWithRole)
            if(!originalTeam.enemies.contains(t))// is an allied team
                return;

        AbilityValidationUtil.Exception("Can't target roles that are only available on enemy teams.");
    }

    private boolean isUniqueAttempt(Player player) {
        Optional<FactionRole> factionRoleOpt = player.getGraveyardFactionRole();
        if(!factionRoleOpt.isPresent())
            return false;

        FactionRole factionRole = factionRoleOpt.get();
        int playerCount = narrator.players.size();
        if(factionRole.role.isRoleUnique(playerCount))
            return true;

        for(Ability ability: factionRole.role.getAbilities())
            if(ability.isFactionUniqueAbility() || ability.isRoleUniqueAbility())
                return true;

        return factionRole.isFactionUnique(playerCount);
    }

    public ArrayList<Object> getInsteadText(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add("Instead of remembering you were like ");
        list.add(a.getTarget());
        return list;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.AMNESIAC_KEEPS_CHARGES)){
            ret.add("You will get the number of charges as your target had, or 1, whichever is more.");
        }else{
            ret.add("You will only get to use one charge on the role that you pick up, if the role has limited charges.");
        }

        if(p.getGameFaction().getEnemies().isEmpty() && p.game.getBool(SetupModifierName.AMNESIAC_MUST_REMEMBER)){
            ret.add("You must adopt another role card to win.");
        }

        return ret;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = ActionUtil.getActionTargets(actions); // should just be 1

        list.add("remember they were like ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public boolean isChargeModifiable() {
        return false;
    }

    @Override
    public Boolean checkWinEnemyless(Player p) {
        if(p.game.getBool(SetupModifierName.AMNESIAC_MUST_REMEMBER))
            return false;
        return null;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Amnesiac", abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}

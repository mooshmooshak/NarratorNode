package game.abilities.support;

import java.util.List;
import java.util.Optional;

import game.abilities.CultLeader;
import game.abilities.GameAbility;
import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.FactionRole;
import models.PendingRole;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import util.PictureConstants;
import util.Util;

public class CultInvitation extends GameAbility {

    public static final AbilityType abilityType = AbilityType.CultInvitation;
    public static final String COMMAND = abilityType.command;

    private Player cultLeader, recruit;
    public boolean accepted = false;

    public CultInvitation(Player cultLeader, Player recruit) {
        super(cultLeader.game, cultLeader.game.setup, new Modifiers<>());
        this.cultLeader = cultLeader;
        this.recruit = recruit;
        new Feedback(recruit,
                "You have been invited to join the " + cultLeader.getGameFaction().getName() + " faction.")
                        .setPicture(PictureConstants.CULTIST)
                        .addExtraInfo("You have the option to accept or reject the invitation.");
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public void confirm(double timeLeft) {
        accepted = true;

        SelectionMessage e = new SelectionMessage(recruit, false);
        e.recordCommand(recruit, timeLeft, Constants.ACCEPT);
        e.add("You've accepted the invitation to join the cult.");
        e.dontShowPrivate();
        e.pushOut();

        List<NarratorListener> listeners = recruit.game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onTargetSelection(recruit, e);
    }

    public void decline(double timeLeft) {
        accepted = false;

        SelectionMessage e = new SelectionMessage(recruit, false);
        e.recordCommand(recruit, timeLeft, Constants.DECLINE);
        e.add("You've declined the invitation to join the cult.");
        e.dontShowPrivate();
        e.pushOut();

        List<NarratorListener> listeners = recruit.game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onTargetSelection(recruit, e);
    }

    public void pushOut() {
        Message q = new Happening(cultLeader.game); // day has already ended, but i want it on the previous day
        if(!accepted){
            q.add(recruit, " rejected ", cultLeader, "'s request to join the " + cultLeader.getGameFaction().getName());
            return;
        }
        GameFaction cultLeaderFaction = cultLeader.getGameFaction();
        FactionRole factionRole = cultLeaderFaction.faction.roleMap.get(recruit.gameRole.factionRole.role);
        if(factionRole == null){
            q.add(recruit, " was unable to join the " + cultLeader.getGameFaction().getName());
            return;
        }
        q.add(recruit, " accepted ", cultLeader, "'s request to join the " + cultLeader.getGameFaction().getName());

        CultLeader cl = cultLeader.getAbility(CultLeader.class);
        Game n = cultLeader.game;
        if(n.getBool(SetupModifierName.CULT_KEEPS_ROLES) && recruit.isPowerRole())
            cl.setPowerCD(n.getInt(SetupModifierName.CULT_POWER_ROLE_CD) + 1);

        // reseting cooldown since its a successful cooldown
        cl.triggerCooldown();
        cl.useCharge();

        recruit.changeRole(new PendingRole(factionRole));

        HTString ht = new HTString(cultLeader.gameRole.factionRole);

        SelectionMessage e = new SelectionMessage(cultLeader, false);
        // e.setCommand(recruit, Constants.DECLINE);
        e.add("Your recruitment was succesful.");
        e.dontShowPrivate();
        e.pushOut();

        e = new SelectionMessage(recruit, false);
        // e.setCommand(recruit, Constants.DECLINE);
        e.add(cultLeader, " converted you to the ", ht, ". Your teammates are : ",
                cultLeader.getGameFaction().getMembers(), ".");
        e.dontShowPrivate();
        e.pushOut();

        cultLeader.getGameFaction().addMember(recruit);

    }

    @Override
    public void doNightAction(Action a) {
        return;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        return new String[0];
    }

    @Override
    public void mainAbilityCheck(Action a) {
        // TODO Auto-generated method stub
    }

    @Override
    public String getAbilityDescription(Optional<Game> optGame, Setup setup) {
        String defaultValue = "accept recruitment invitation";
        if(!optGame.isPresent())
            return defaultValue;
        Game game = optGame.get();
        for(FactionRole factionRole: setup.getPossibleFactionRolesWithAbility(game, CultLeader.abilityType))
            return defaultValue + " to the " + factionRole.faction.name;
        return defaultValue;
    }

    public static Action getAction(Player owner, String message) {
        return new Action(owner, abilityType, Util.toStringList(message), new PlayerList());
    }
}

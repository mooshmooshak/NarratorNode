package game.abilities.support;

import java.util.List;
import java.util.Optional;

import game.abilities.Burn;
import game.abilities.ElectroManiac;
import game.abilities.GameAbility;
import game.abilities.Visit;
import game.event.DeathAnnouncement;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.enums.SetupModifierName;

public class Gun extends Charge {

    public static final String IMAGINARY_GUN = "You raise your gun to eye level, take a deep breathe to aim, and notice you actually have no gun in your hand!";
    public static final String COMMAND = Constants.GUN_COMMAND;
    public static final String ALIAS = "gun_alias";

    Optional<Player> smith;

    public Gun() {
        super(Charge.FROM_ROLE);
    }

    public Gun(Player smith) {
        super(Charge.FROM_OTHER);
        this.smith = Optional.of(smith);
    }

    public static Gun DrugGun() {
        Gun gun = new Gun();
        gun.setReal(false);
        gun.isFromRole = false;
        return gun;
    }

    @Override
    public String toString() {
        if(isReal()){
            if(this.getFlag() == Constants.VIGILANTE_KILL_FLAG)
                return "VigiGun";
            return "Gun";
        }
        return "FakeGun";
    }

    @Override
    public int compareTo(Charge q) {
        if(!(q instanceof Gun))
            return 0;

        Gun o = (Gun) q;

        if(getFlag() == o.getFlag()){
            if(isReal() == o.isReal())
                return 0;
            else if(o.isReal())
                return 1;
            else
                return -1;
        }
        if(getFlag() == Constants.VIGILANTE_KILL_FLAG)
            return 1;
        return -1;

    }

    public String[] getFlag() {
        if(this.isFromRole)
            return Constants.VIGILANTE_KILL_FLAG;
        return Constants.GUN_KILL_FLAG;
    }

    public boolean shoot(Action a) {
        if(a.getTarget() == null)
            return false;
        Player target = a.getTarget(), owner = a.owner;
        Game narrator = a.owner.game;
        if(narrator.isNight()){
            handleNightGunShooting(a, target);
        }else{
            if(!isFake())
                return handleRealDayGunShooting(a, target, owner, narrator);
            Feedback f = new Feedback(a.owner, IMAGINARY_GUN);
            a.owner.sendMessage(f);

            GameAbility.happening(owner, " used their fake gun ", target);
        }

        return true;

    }

    private boolean handleRealDayGunShooting(Action a, Player target, Player owner, Game game) {
        PlayerList deadPeople = new PlayerList();
        DeathAnnouncement e = new DeathAnnouncement(deadPeople, game, !DeathAnnouncement.NIGHT_DEATH);
        e.add("BANG! ");
        if(!faulty){
            if(target.isInvulnerable())
                e.add("An attack rang through the courtyard, but no one appears to know where it came from.");
            else
                e.add(" ", target, " crumpled to the floor, blood gushing from their stomach.");
        }else{
            if(a.owner.isInvulnerable())
                e.add(" The shot rang through the courtyard, but no one appears to know where it came from.");
            else
                e.add(" ", a.owner, " crumpled to the floor, blood gushing from their stomach.");
        }

        game.announcement(e);

        if(!faulty){
            if(!target.isInvulnerable()){
                target.setDead(a.owner);
                target.getDeathType().addDeath(this.getFlag());
                deadPeople.add(target);
            }
        }else{
            if(!a.owner.isInvulnerable()){
                a.owner.setDead(this.smith.get()); // faulty guns will always have a smith
                a.owner.getDeathType().addDeath(this.getFlag());
                deadPeople.add(a.owner);
            }
        }

        DeathAnnouncement explosion = null;

        if(!faulty){
            if((target.is(ElectroManiac.abilityType) || target.isCharged())
                    && (owner.isCharged() || owner.is(ElectroManiac.abilityType))){
                if(!owner.isInvulnerable() && !owner.is(ElectroManiac.abilityType)){
                    owner.dayKill(new IndirectAttack(Constants.ELECTRO_KILL_FLAG, owner, owner.getCharger()),
                            a.timeLeft, true); // dont want the checks to run = true
                    deadPeople.add(owner);
                }
                if(!target.isInvulnerable() && !target.is(ElectroManiac.abilityType))
                    target.getDeathType().addDeath(Constants.ELECTRO_KILL_FLAG);
                explosion = Burn.ExplosionAnnouncement(game, deadPeople, "electromaniac");
                game.announcement(explosion);
            }
        }

        game.voteSystem.removeVotersOf(deadPeople);
        game.checkProgress();
        if(!game.isInProgress())
            return true;
        game.checkVote(a.timeLeft);

        if((!faulty && target.isInvulnerable()) || (faulty && a.owner.isInvulnerable()))
            target = null;
        if(game.isInProgress()){
            List<NarratorListener> listeners = game.getListeners();
            for(NarratorListener listener: listeners){
                listener.onAssassination(a.owner, target, e);
                if(explosion != null)
                    listener.onElectroExplosion(deadPeople, explosion);
            }

            game.parityCheck();
        }
        return true;
    }

    private void handleNightGunShooting(Action a, Player target) {
        if(faulty){
            if(a.owner.isInvulnerable()){
                Visit.NoNightActionVisit(
                        new Action(a.owner, Visit.abilityType, (String) null, null, Player.list(a.owner)));
            }else
                GameAbility.Kill(a.owner, a.owner, Constants.GUN_SELF_KILL_FLAG, this.smith);
        }else if(isReal()){
            GameAbility.Kill(a.owner, target, this.getFlag());// visiting taken care of already in kill
        }else
            Visit.NoNightActionVisit(a);
    }

    private boolean faulty = false;

    public void setFaulty() {
        faulty = true;

    }

    public boolean isFaulty() {
        return faulty;
    }

    public static String getGunName(Game game) {
        return getGunName(Optional.of(game.players.size()), game.setup);
    }

    public static String getGunName(Optional<Integer> playerCount, Setup setup) {
        return setup.modifiers.getString(SetupModifierName.GUN_NAME, playerCount);
    }
}

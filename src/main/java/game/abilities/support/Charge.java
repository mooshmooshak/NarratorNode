package game.abilities.support;

public class Charge implements Comparable<Charge> {

    public static final boolean FROM_ROLE = true;
    public static final boolean FROM_OTHER = false;

    private boolean isUsed = false;
    private boolean isReal = true;
    protected boolean isFromRole;

    public Charge(boolean isFromSelf) {
        this.isFromRole = isFromSelf;
    }

    public Charge setReal(boolean real) {
        isReal = real;
        return this;
    }

    public boolean isReal() {
        return isReal;
    }

    public boolean isFake() {
        return !isReal;
    }

    public boolean isFromRole() {
        return isFromRole;
    }

    public void markUsed() {
        isUsed = true;
    }

    public void markUnused() {
        isUsed = false;
    }

    public boolean isUsed() {
        return isUsed;
    }

    @Override
    public int compareTo(Charge q) {
        if(!(q instanceof Charge))
            return 0;
        Charge o = q;
        if(o.isReal() == isReal())
            return 0;
        if(o.isReal())
            return 1;
        return -1;
    }
}

package game.abilities.support;

import game.abilities.Joker;
import game.logic.Game;
import game.logic.Player;
import models.enums.SetupModifierName;

public class Bounty {

    public int start_day;
    public Player rewarder, target;

    public Bounty(Player bountyPlacer, Player bountyHolder, int day) {
        if(bountyPlacer == null || bountyHolder == null)
            throw new NullPointerException();
        this.rewarder = bountyPlacer;
        this.target = bountyHolder;
        this.start_day = day;
    }

    public boolean failed() {
        int incubation = rewarder.game.getInt(SetupModifierName.BOUNTY_INCUBATION);
        return target.isAlive() && rewarder.game.getDayNumber() >= this.start_day + incubation;
    }

    public void dayAnnounce() {
        Game n = rewarder.game;
        n.announcement(Joker.DayStartAnnouncementCreator(target,
                start_day + n.getInt(SetupModifierName.BOUNTY_INCUBATION) - n.getDayNumber() + 1));
    }

    public void onFail() {
        Game n = rewarder.game;
        n.announcement(Joker.DayEndAnnouncementCreator(target));
    }

}

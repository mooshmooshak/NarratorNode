package game.abilities.support;

import java.util.Optional;

import game.logic.Game;
import game.setups.Setup;
import models.enums.SetupModifierName;

public class Vest extends Charge {

    public static final String ALIAS = "vest_alias";

    public Vest(boolean isFromRole) {
        super(isFromRole);
    }

    @Override
    public String toString() {
        if(isReal())
            return "Vest";
        return "FakeVest";
    }

    @Override
    public int compareTo(Charge q) {
        if(!(q instanceof Vest))
            return 0;
        Vest o = (Vest) q;
        if(o.isReal() == isReal())
            return 0;
        if(o.isReal())
            return 1;
        return -1;
    }

    public static String getVestName(Game game) {
        return getVestName(Optional.of(game.players.size()), game.setup);
    }

    public static String getVestName(Optional<Integer> playerCount, Setup setup) {
        return setup.modifiers.getString(SetupModifierName.VEST_NAME, playerCount);
    }

    public static Vest FakeVest(boolean isFromRole) {
        Vest v = new Vest(isFromRole);
        v.setReal(false);
        return v;
    }
}

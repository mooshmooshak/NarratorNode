package game.abilities.support;

import java.util.ArrayList;

import game.logic.Player;
import models.enums.SetupModifierName;

public class BountyList {

    public ArrayList<Bounty> list;
    public Player rewarder; // not who made the bounty

    public BountyList(Player rewarder) {
        list = new ArrayList<>();
        this.rewarder = rewarder;
    }

    public Bounty add(Player bountyPlacer, int dayNumber) {
        Bounty b = new Bounty(bountyPlacer, rewarder, dayNumber);
        list.add(b);
        return b;
    }

    // called at onNightStart of Joker
    public ArrayList<Bounty> awardBounties(Player bountyPlacer) {
        ArrayList<Bounty> ret = new ArrayList<>();

        Bounty b;
        for(int i = 0; i < list.size(); i++){
            b = list.get(i);
            if(b.failed() && bountyPlacer == b.rewarder){
                ret.add(b);
            }
        }
        return ret;
    }

    public void cleanse() {
        Bounty b;
        for(int i = 0; i < list.size(); i++){
            b = list.get(i);
            if(b.start_day + rewarder.game.getInt(SetupModifierName.BOUNTY_INCUBATION) <= rewarder.game
                    .getDayNumber()){
                list.remove(i);
                i--;
            }
        }

    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

}

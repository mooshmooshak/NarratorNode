package game.abilities.support;

import game.logic.Player;
import game.logic.PlayerList;

@SuppressWarnings("serial")
public class ElectrocutionException extends Error {

    public PlayerList charged;
    public Player triggerer;

    public ElectrocutionException(Player triggerer, PlayerList charged) {
        this.triggerer = triggerer;
        this.charged = charged;
    }

    public static void throwException(Player triggerer, PlayerList electrocuted) {
        if(!electrocuted.isEmpty()){
            electrocuted = PlayerList.GetUnique(electrocuted);
            throw new ElectrocutionException(triggerer, electrocuted);
        }
    }

}

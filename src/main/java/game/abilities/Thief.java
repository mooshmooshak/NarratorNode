package game.abilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.RolePackage;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.PendingRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleChangeService;
import services.RoleService;
import util.Util;

public class Thief extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Thief;
    public static final String COMMAND = abilityType.command;

    public Thief(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Become one of the unselected roles.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void doNightAction(Action action) {
        Set<FactionRole> choices = getValidRoleChoices(action.owner);
        FactionRole factionRole = getPickedRole(action.getArg1(), action.owner, choices);
        addThiefChangingRoleHappening(action.owner, factionRole);
        RoleChangeService.changeRoleAndTeam(action.owner, new PendingRole(factionRole));
        action.owner.getActions().clear();
    }

    @Override
    public void mainAbilityCheck(Action action) {
        Set<FactionRole> allChoices = getUnpickedRoles(action.owner.game);
        Set<FactionRole> validChoices = getValidRoleChoices(action.owner);
        FactionRole foundRole = getPickedRole(action.getArg1(), action.owner, allChoices);
        FactionRole foundValidRole = getPickedRole(action.getArg1(), action.owner, validChoices);

        if(foundValidRole != null)
            return;

        if(foundRole == null)
            throw new UnknownRoleException(action.getArg1() + " is not a valid role or team.");

        String message = "Roles with lower priority are not allowed to be picked.";
        if(validChoices.size() == 1){
            FactionRole role = validChoices.iterator().next();
            GameFaction team = action.owner.game.getFaction(role.getColor());
            message += "  '" + team.getName() + "' or '" + role.getName() + "' are the only allowed choices.";
        }
        throw new UnknownRoleException(message);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String getUsage(Player player, Random random) {
        String option = Thief.getOptionValue(player);
        return COMMAND + " " + option;
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        String option = Thief.getOptionValue(owner);
        return Optional.of(new Action(owner, abilityType, option, (String) null));
    }

    @Override
    public boolean isRoleUniqueAbility() {
        return true;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public boolean isZeroWeightModifiable() {
        return false;
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public int getDefaultTargetSize() {
        return 0;
    }

    @Override
    public boolean isRolePickingPhaseAbility() {
        return true;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        return this.getActionDescription(actions.get(0));
    }

    @Override
    public ArrayList<Object> getActionDescription(Action action) {
        Set<FactionRole> choices = getValidRoleChoices(action.owner);
        FactionRole pickedRole = getPickedRole(action.getArg1(), action.owner, choices);
        ArrayList<Object> list = new ArrayList<>();
        list.add(getCommand().toLowerCase() + " ");
        list.add(new HTString(pickedRole));
        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action action) {
        Set<FactionRole> choices = getValidRoleChoices(action.owner);
        FactionRole pickedRole = getPickedRole(action.getArg1(), action.owner, choices);
        ArrayList<Object> list = new ArrayList<>();
        list.add(getCommand().toLowerCase() + "ing ");
        list.add(new HTString(pickedRole));
        return list;
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getArg1());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player player, double timeLeft, ArrayList<String> commands) {
        commands.remove(0);

        if(commands.isEmpty())
            throw new PlayerTargetingException("Need to pick a role with this action");

        String pick = commands.get(0);

        Action a = new Action(player, abilityType, timeLeft, Util.toStringList(pick), new PlayerList());
        mainAbilityCheck(a);
        return a;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Thief", Thief.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    public static void fillInMissingActions(Game narrator) {
        PlayerList thieves = narrator.players.filter(Thief.abilityType);
        String option;
        Action action;
        for(Player thief: thieves){
            if(!thief.getActions().isEmpty())
                continue;
            option = getOptionValue(thief);
            action = new Action(thief, abilityType, option, (String) null);
            narrator.getEventManager().addCommand(thief, Constants.ALL_TIME_LEFT, COMMAND, option);
            thief.getActions().addAction(action);
        }
    }

    private static String getOptionValue(Player thief) {
        FactionRole forcedPickedRole = getValidRoleChoices(thief).iterator().next();
        if(getPickedRole(forcedPickedRole.getName(), thief, getValidRoleChoices(thief)) == null)
            return forcedPickedRole.getColor();
        return forcedPickedRole.getName();
    }

    private static FactionRole getPickedRole(String pickName, Player thief, Set<FactionRole> roleChoices) {
        GameFaction team = thief.game.getFaction(pickName);
        if(team == null)
            team = thief.game.getFactionByName(pickName);

        FactionRole found = null;
        if(team != null){
            for(FactionRole roleChoice: roleChoices){
                if(!roleChoice.getColor().equals(team.getColor()))
                    continue;
                if(found != null)
                    return null;
                found = roleChoice;
            }
        }else{
            for(FactionRole roleChoice: roleChoices){
                if(!roleChoice.getName().equalsIgnoreCase(pickName))
                    continue;
                if(found != null)
                    return null;
                found = roleChoice;
            }
        }

        return found;
    }

    private static Set<FactionRole> getUnpickedRoles(Game narrator) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(RolePackage rolePackage: narrator.unpickedRoles)
            factionRoles.add(rolePackage.assignedRole);
        return factionRoles;
    }

    private static Set<FactionRole> getValidRoleChoices(Player thief) {
        Set<FactionRole> choices = new HashSet<>();
        GameFaction team;
        int priority = Integer.MIN_VALUE;
        for(RolePackage choice: thief.game.unpickedRoles){
            team = thief.game.getFaction(choice.assignedRole.getColor());
            if(team.getPriority() > priority){
                priority = team.getPriority();
                choices.clear();
            }
            if(team.getPriority() == priority)
                choices.add(choice.assignedRole);
        }
        return choices;
    }

    private static void addThiefChangingRoleHappening(Player player, FactionRole rolePackage) {
        Happening happening = new Happening(player.game);
        HTString htRoleString = new HTString(rolePackage);
        happening.add(player, " became a ", htRoleString, ".");
    }
}

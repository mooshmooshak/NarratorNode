package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;

public class FactionKill extends GameAbility {

    public static final AbilityType abilityType = AbilityType.FactionKill;
    public static final String COMMAND = abilityType.command;

    public FactionKill(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Ability to kill at night.";

    private GameFaction faction;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public GameAbility initialize(GameFaction faction) {
        super.initialize(faction);
        this.faction = faction;
        return this;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void doNightAction(Action a) {
        GameFaction faction = this.faction == null ? a.owner.getGameFaction() : this.faction;
        Player owner = a.owner, target = a.getTarget();
        if(target == null || faction == null)
            return;

        a.markCompleted();
        GameAbility.Kill(owner, target, faction.getKillFlag());
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.DIFFERENTIATED_FACTION_KILLS);
        return names;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("killing ");
        list.add(StringChoice.YouYourselfSingle(a.getTarget()));
        return list;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }
}

package game.abilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.ModifierValue;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.GameUtil;

public class Mason extends Passive {

    public static final AbilityType abilityType = AbilityType.Mason;

    public Mason(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "You are part of the masons and may communicate with others like you at night.";

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.MASON_PROMOTION);
        names.add(SetupModifierName.MASON_NON_CIT_RECRUIT);
        return names;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.noAcceptableTargets();
    }

    @Override
    protected ArrayList<String> getProfileHints(Player player) {
        return getProfileHints(player.game);
    }

    public static ArrayList<String> getProfileHints(Game game) {
        ArrayList<String> ret = new ArrayList<>();

        ret.add(SetupModifierName.MASON_PROMOTION.getTextValue(Optional.of(game.players.size()), game.setup.modifiers));

        addMasonText(game, ret);
        return ret;
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> color, Set<Ability> abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(game, actionOriginatorName, color, abilities);
        addMasonText(GameUtil.getPlayerCount(game), setup, ruleTextList);
        return ruleTextList;
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    public static void addMasonText(Game game, ArrayList<String> list) {
        addMasonText(Optional.of(game.players.size()), game.setup, list);
    }

    public static void addMasonText(Optional<Integer> playerCount, Setup setup, ArrayList<String> list) {
        Optional<ModifierValue> promotionModifier = setup.modifiers.getIfSingle(SetupModifierName.MASON_PROMOTION,
                playerCount);

        if(!promotionModifier.isPresent())
            list.add("Depending on player count, this might spawn by itself.");
        else if(promotionModifier.get().getBoolValue())
            list.add("This will never spawn by itself.");

        list.add(SetupModifierName.MASON_NON_CIT_RECRUIT.getLabel(playerCount, setup.modifiers));
    }

    public static boolean IsMasonType(Player p) {
        for(AbilityType abilityType: getMasonTypes()){
            if(p.hasAbility(abilityType))
                return true;
        }
        return false;
    }

    public static boolean IsMasonType(Role role) {
        for(AbilityType abilityType: getMasonTypes()){
            if(role.is(abilityType))
                return true;
        }
        return false;
    }

    public static Set<AbilityType> getMasonTypes() {
        Set<AbilityType> masonTypes = new HashSet<>();
        masonTypes.add(Mason.abilityType);
        masonTypes.add(MasonLeader.abilityType);
        masonTypes.add(Enforcer.abilityType);
        masonTypes.add(Clubber.abilityType);
        return masonTypes;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Mason", Mason.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}

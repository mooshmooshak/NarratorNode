package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.ArchitectChat;
import game.event.Happening;
import game.event.NightChat;
import game.event.RoleCreatedChat;
import game.event.SelectionMessage;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.ActionUtil;

public class Architect extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Architect;
    public static final String COMMAND = abilityType.command;

    public Architect(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.ARCH_QUARANTINE);
        names.add(SetupModifierName.ARCH_BURN);
        return names;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Construct rooms at night so others can converse securely.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        PlayerList targets = a.getTargets();
        if(targets.hasDead())
            AbilityValidationUtil.Exception("Cannot target dead people");
        if(targets.getFirst() == targets.getLast())
            AbilityValidationUtil.Exception("Cannot target the same people with this ability");
    }

    @Override
    public void doNightAction(Action a) {
        Visit.NoNightActionVisit(a);
    }

    @Override
    public boolean isChatRole() {
        return true;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return true;
    }

    @Override
    public void doDayAction(Action action, Action parentAction) {
        PlayerList targets = action.getTargets();
        Player arch = action.owner;
        if(arch.getActions().isTargeting(targets, abilityType))
            return;
        SelectionMessage e = new SelectionMessage(arch, false);
        Action[] newOld = Action.pushCommand(e, action, arch.getActions());
        e.recordCommand(arch, parentAction.timeLeft, COMMAND, targets.get(0).getName(), targets.get(1).getName());
        e.dontShowPrivate();

        arch.game.addActionStack(newOld[0]);// new is 0
        arch.game.removeActionStack(newOld[1]);// old is 1

        List<NarratorListener> listeners = arch.game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onDayActionSubmit(arch, newOld[0]);
    }

    @Override
    public int getDefaultTargetSize() {
        return 2;
    }

    @Override
    public void resolveDayAction(Action a) {
        Happening e = new Happening(a.owner.game);
        e.add(a.owner, " set up a secret meeting for ", a.getTargets(), ".");
        a.owner.game.getEventManager().createArchitectChat(a);

        useCharge();
        triggerCooldown();
    }

    @Override
    public void dayHappening(Action a) {
        happening(a.owner, " attempted to create a room for ", a.getTargets().toArray());
    }

    @Override
    public void onNightStart(Player architect) {
        super.onNightStart(architect);
        int architectChats = getArchitectChatsMadeCount(architect);
        int breadToRemove = architectChats - 1;
        for(int i = 0; i < breadToRemove; i++)
            architect.getAbility(BreadAbility.class).useCharge();
    }

    private int getArchitectChatsMadeCount(Player p) {
        int count = 0;
        // i might need to filter on day number here
        ArchitectChat ac;
        for(NightChat nc: p.game.getEventManager().tempChats){
            if(nc instanceof ArchitectChat){
                ac = (ArchitectChat) nc;
                if(ac.getCreator().contains(p) && ac.getDay() == p.game.getDayNumber()){
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        if(actions.isEmpty())
            return list;
        Game n = actions.get(0).owner.game;
        if(n.isNight())
            return super.getActionDescription(actions);

        PlayerList targets = ActionUtil.getActionTargets(actions);

        list.add(COMMAND.toLowerCase() + " a room for ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        PlayerList pl = pi.game.getLivePlayers();
        if(!super.canSelfTarget())
            pl.remove(pi);
        return pl;
    }

    @Override
    public void checkPerformDuringDay(Player owner) {
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("building a room for ");
        list.addAll(StringChoice.YouYourself(a.getTargets()));

        return list;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.game.getLivePlayers();
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.ARCH_QUARANTINE)){
            ret.add("Participants in the rooms you build will be in no other temporary chats.");
        }else{
            ret.add("Participants in the rooms you build can also be in other temporary chats.");
        }

        if(narrator.setup.hasPossibleFactionRoleWithAbility(p.game, Burn.abilityType)){
            if(p.game.getBool(SetupModifierName.ARCH_BURN)){
                ret.add("Participants that get burned in chats will burn other chat participants.");
            }else{
                ret.add("Participants that get burned in chats won't affect other chat participants.");
            }
        }

        return ret;
    }

    public static boolean RoomFire(Player p) {
        if(!p.game.getBool(SetupModifierName.ARCH_BURN))
            return false;
        for(RoleCreatedChat rc: p.getRoleCreatedChats(p.game.getDayNumber())){
            if(!(rc instanceof ArchitectChat))
                continue;
            for(Player q: rc.getMembers()){
                if(q.isDoused())
                    return true;
            }
        }
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Architect", abilityType);
    }
}

package controllers;

import java.sql.SQLException;
import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.Optional;

import game.logic.Game;
import game.logic.exceptions.NarratorException;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.Role;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.json_output.RoleJson;
import models.modifiers.RoleModifier;
import models.serviceResponse.ModifierServiceResponse;
import models.serviceResponse.RoleCreateResponse;
import models.serviceResponse.RoleGetResponse;
import services.RoleModifierService;
import services.RoleService;

public class RoleController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            createRoleOrCreateModifier(request, response);
        else if(request.getString("method").equals("GET"))
            get(request, response);
        else if(request.getString("method").equals("DELETE"))
            delete(request);
    }

    private static void createRoleOrCreateModifier(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        String name = args.getString("name");
        if(args.has("setupID"))
            createRole(userID, name, args, response);
        else
            createModifier(userID, name, args, response);
    }

    private static void createRole(long userID, String name, JSONObject args, JSONObject response)
            throws JSONException, SQLException {
        long setupID = args.getLong("setupID");
        JSONArray abilityList = args.getJSONArray("abilities");
        LinkedHashSet<AbilityType> abilityTypes = new LinkedHashSet<>();
        for(int i = 0; i < abilityList.length(); ++i)
            abilityTypes.add(AbilityType.narratorParse(abilityList.getString(i)));

        RoleCreateResponse serviceResponse = RoleService.createRole(userID, setupID, name, abilityTypes);

        Role role = serviceResponse.role;
        Game game = serviceResponse.game;
        response.put("response", new RoleJson(role, Optional.of(game)));
    }

    private static void createModifier(long userID, String modifierName, JSONObject args, JSONObject response)
            throws JSONException, SQLException {
        RoleModifierName name;
        try{
            name = RoleModifierName.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown role modifier name: " + modifierName);
        }
        long roleID = args.getLong("roleID");
        int minPlayerCount = args.getInt("minPlayerCount");
        int maxPlayerCount = args.getInt("maxPlayerCount");
        ModifierValue value = new ModifierValue(args.get("value"));
        RoleModifier modifier = new RoleModifier(name, value, minPlayerCount, maxPlayerCount, Instant.now());

        ModifierServiceResponse<RoleModifierName> serviceResponse = RoleModifierService.upsertModifier(userID, roleID,
                modifier);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.modifier.value.internalValue);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("joinID", serviceResponse.joinID);
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }

    private static void get(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long roleID = args.getLong("roleID");

        RoleGetResponse serviceResponse = RoleService.get(userID, roleID);
        Role role = serviceResponse.role;
        Optional<Game> game = serviceResponse.game;
        response.put("response", new RoleJson(role, game));
    }

    private static void delete(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long roleID = args.getLong("roleID");

        RoleService.delete(userID, roleID);
    }

}

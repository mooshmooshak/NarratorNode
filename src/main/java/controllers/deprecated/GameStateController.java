package controllers.deprecated;

import java.sql.SQLException;
import java.util.Optional;
import java.util.Set;

import game.logic.Player;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.json_output.SetupJson;
import nnode.Lobby;
import nnode.StateObject;
import repositories.GameUserRepo;
import services.LobbyService;
import services.ModeratorService;
import services.PlayerService;

public class GameStateController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("GET"))
            getGameState(request, response);
    }

    private static void getGameState(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject body = request.getJSONObject("body");
        long userID = body.getLong(StateObject.userID);

        Optional<GameID> gameIDOpt = GameUserRepo.getActiveGameIDByUserID(userID);
        if(!gameIDOpt.isPresent())
            return;
        GameID gameID = gameIDOpt.get();
        Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(gameID);

        Lobby lobby = LobbyService.getByGameID(gameID);
        Optional<Player> player = PlayerService.getByUserID(userID);

        StateObject gameState = Lobby.getGameState(lobby, player, moderatorIDs);
        JSONObject gameStateJSON = gameState.construct(player, lobby);
        gameStateJSON.put("setup", new SetupJson(lobby.game));
        response.put("response", gameStateJSON);
    }
}

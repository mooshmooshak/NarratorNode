package controllers;

import java.sql.SQLException;
import java.time.Instant;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.modifiers.AbilityModifier;
import models.serviceResponse.ModifierServiceResponse;
import services.RoleAbilityModifierService;

public class RoleAbilityModifierController {
    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            upsertModifier(request, response);
    }

    private static void upsertModifier(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        String modifierName = args.getString("name");
        AbilityModifierName name;
        try{
            name = AbilityModifierName.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown role ability modifier name: " + modifierName);
        }
        long userID = args.getLong("userID");
        long roleID = args.getLong("roleID");
        long abilityID = args.getLong("abilityID");
        ModifierValue value = new ModifierValue(args.get("value"));
        int minPlayerCount = args.getInt("minPlayerCount");
        int maxPlayerCount = args.getInt("maxPlayerCount");
        AbilityModifier modifier = new AbilityModifier(name, value, minPlayerCount, maxPlayerCount, Instant.now());

        ModifierServiceResponse<AbilityModifierName> serviceResponse = RoleAbilityModifierService.upsertModifier(userID,
                roleID, abilityID, modifier);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.modifier.value.internalValue);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("joinID", serviceResponse.joinID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }
}

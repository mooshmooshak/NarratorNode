package controllers;

import json.JSONException;
import json.JSONObject;
import services.PhaseService;

public class PhaseController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("PUT"))
            endPhaseController(request);
    }

    private static void endPhaseController(JSONObject request) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        String joinID = args.getString("joinID");
        String phaseHash = args.getString("phaseHash");
        double timeLeft = args.getDouble("timeLeft");
        if(args.has("checkVotes"))
            PhaseService.checkVotes(joinID, phaseHash, timeLeft);
        else
            PhaseService.endPhase(joinID, phaseHash, timeLeft);
    }
}

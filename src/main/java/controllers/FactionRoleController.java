package controllers;

import java.sql.SQLException;

import json.JSONException;
import json.JSONObject;
import models.json_output.FactionRoleJson;
import models.serviceResponse.FactionRoleCreateResponse;
import models.serviceResponse.FactionRoleGetResponse;
import services.FactionRoleService;

public class FactionRoleController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            create(request, response);
        else if(request.getString("method").equals("GET"))
            get(request, response);
        else if(request.getString("method").equals("PUT"))
            update(request);
        else if(request.getString("method").equals("DELETE"))
            delete(request);
    }

    private static void create(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long factionID = args.getLong("factionID");
        long roleID = args.getLong("roleID");

        FactionRoleCreateResponse serviceResponse = FactionRoleService.createFactionRole(userID, factionID, roleID);
        response.put("response", new FactionRoleJson(serviceResponse.factionRole, serviceResponse.game));
    }

    private static void get(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long factionRoleID = args.getLong("factionRoleID");

        FactionRoleGetResponse serviceResponse = FactionRoleService.get(userID, factionRoleID);
        response.put("response", new FactionRoleJson(serviceResponse.factionRole, serviceResponse.game));
    }

    private static void update(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long factionRoleID = args.getLong("factionRoleID");
        Long receivedFactionRoleID = args.isNull("receivedFactionRoleID") ? null
                : args.getLong("receivedFactionRoleID");

        FactionRoleService.setRoleReceive(userID, factionRoleID, receivedFactionRoleID);
    }

    private static void delete(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long factionRoleID = args.getLong("factionRoleID");

        FactionRoleService.deleteFactionRole(userID, factionRoleID);
    }
}

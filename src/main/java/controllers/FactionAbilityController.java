package controllers;

import java.sql.SQLException;
import java.time.Instant;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.modifiers.AbilityModifier;
import models.serviceResponse.ModifierServiceResponse;
import services.FactionAbilityModifierService;

public class FactionAbilityController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            upsertModifier(request, response);
    }

    private static void upsertModifier(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        ModifierValue value = new ModifierValue(args.get("value"));
        String modifierNameS = args.getString("name");
        int minPlayerCount = args.getInt("minPlayerCount");
        int maxPlayerCount = args.getInt("maxPlayerCount");
        long factionAbilityID = args.getLong("factionAbilityID");

        AbilityModifierName modifiername;
        try{
            modifiername = AbilityModifierName.valueOf(modifierNameS);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown ability modifier name: " + modifierNameS);
        }
        AbilityModifier modifier = new AbilityModifier(modifiername, value, minPlayerCount, maxPlayerCount,
                Instant.now());

        ModifierServiceResponse<AbilityModifierName> serviceResponse = FactionAbilityModifierService.upsertModifier(userID,
                factionAbilityID, modifier);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierNameS);
        modifierJSON.put("value", serviceResponse.modifier.value.internalValue);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("joinID", serviceResponse.joinID);
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);

    }

}

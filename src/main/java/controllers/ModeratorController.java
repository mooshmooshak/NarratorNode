package controllers;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import services.ModeratorService;

public class ModeratorController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST")){
            JSONObject args = request.getJSONObject("body");
            if(args.has("repickTarget"))
                repickHostController(request, response);
            else
                createModeratorController(request);
        }else if(request.getString("method").equals("DELETE")){
            deleteModeratorController(request);
        }
    }

    private static void createModeratorController(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        ModeratorService.create(userID);
    }

    private static void deleteModeratorController(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        String playerName = args.getString("playerName");
        ModeratorService.delete(userID, playerName);
    }

    private static void repickHostController(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        JSONArray activeUserIDsJson = args.getJSONArray("activeUserIDs");
        String repickTarget = args.getString("repickTarget");
        long userID = args.getLong("userID");
        Set<Long> activeUserIDs = new HashSet<>();
        for(int i = 0; i < activeUserIDsJson.length(); i++)
            activeUserIDs.add(activeUserIDsJson.getLong(i));
        Set<Long> moderatorIDs = ModeratorService.repick(userID, repickTarget, activeUserIDs);
        JSONObject repickResponse = new JSONObject();
        repickResponse.put("moderatorIDs", moderatorIDs);
        response.put("response", repickResponse);
    }

}

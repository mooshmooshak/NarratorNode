package controllers;

import java.sql.SQLException;
import java.util.List;

import game.setups.Setup;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GeneratedRole;
import models.OpenLobby;
import models.idtypes.GameID;
import models.json_output.GameJson;
import models.json_output.SetupHiddenSpawnJson;
import models.json_output.SetupJson;
import models.schemas.SetupSchema;
import services.SetupService;

public class SetupController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("GET")){
            getSetups(request, response);
        }else if(request.getString("method").equals("PUT")){
            setSetup(request, response);
        }else if(request.getString("method").equals("POST")){
            createOrCloneSetup(request, response);
        }
    }

    private static void createOrCloneSetup(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");

        if(args.has("setupID")){
            long setupID = args.getLong("setupID");
            Setup setup = SetupService.cloneSetup(setupID, userID);
            response.put("response", new SetupJson(setup));
            return;
        }
        JSONObject setupRequest = args.getJSONObject("setup");

        SetupSchema setupSchema = new SetupSchema();
        setupSchema.name = setupRequest.getString("name");
        setupSchema.description = setupRequest.optString("description", "");
        setupSchema.slogan = setupRequest.optString("slogan", "");
        setupSchema.ownerID = userID;
        boolean isDefault = setupRequest.optBoolean("isDefault", true);

        Setup setup = SetupService.createSetup(setupSchema, isDefault);
        response.put("response", new SetupJson(setup));
    }

    private static void getSetups(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONArray setupsResponse = new JSONArray();
        response.put("response", setupsResponse);
        JSONObject args = request.getJSONObject("body");
        long setupID = args.getLong("setupID");
        if(args.has("playerCount"))
            getSetupIterations(setupID, args.getInt("playerCount"), response);
        else
            getSetup(setupID, response);
    }

    private static void getSetup(long setupID, JSONObject response) throws JSONException, SQLException {
        Setup setup = SetupService.getSetup(setupID);
        response.put("response", new SetupJson(setup));
    }

    private static void getSetupIterations(long setupID, int playerCount, JSONObject response)
            throws JSONException, SQLException {
        List<GeneratedRole> iterations = SetupService.getIterations(setupID, playerCount);
        JSONArray array = new JSONArray();
        array.put(SetupHiddenSpawnJson.toJson(iterations));
        response.put("response", array);
    }

    private static void setSetup(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        OpenLobby lobby;
        long setupID = args.getLong("setupID");
        if(args.has("joinID")){
            String joinID = args.getString("joinID");
            lobby = SetupService.setSetup(joinID, setupID);
        }else{
            GameID gameID = new GameID(args.getLong("gameID"));
            lobby = SetupService.setSetup(gameID, setupID);
        }

        response.put("response", new GameJson(lobby));
    }

}

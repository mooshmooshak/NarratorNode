package controllers;

import java.sql.SQLException;
import java.util.Optional;

import json.JSONException;
import json.JSONObject;
import models.json_output.HiddenJson;
import models.serviceResponse.HiddenCreateResponse;
import services.HiddenService;

public class HiddenController {
    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            createHidden(request, response);
        else if(request.getString("method").equals("DELETE"))
            deleteHidden(request);
    }

    private static void createHidden(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long setupID = args.getLong("setupID");
        String hiddenName = args.getString("name");

        HiddenCreateResponse serviceResponse = HiddenService.createHidden(userID, setupID, hiddenName);
        response.put("response", new HiddenJson(serviceResponse.hidden, Optional.of(serviceResponse.game)));
    }

    private static void deleteHidden(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long hiddenID = args.getLong("hiddenID");
        HiddenService.delete(userID, hiddenID);
    }
}

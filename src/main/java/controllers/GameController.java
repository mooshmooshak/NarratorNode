package controllers;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Set;

import game.logic.exceptions.NarratorException;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.OpenLobby;
import models.idtypes.GameID;
import models.json_output.GameJson;
import models.schemas.GameCreateRequest;
import models.serviceResponse.GameLobbyGetResponse;
import services.GameService;
import services.LobbyService;

public class GameController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        if(request.getString("method").equals("GET")){
            if(args.has("setupID"))
                getGameBySetupID(request, response);
            else if(args.has("gameID"))
                getGameByID(request, response);
            else
                getActiveGames(response);
        }else if(request.getString("method").equals("POST")){
            if(args.has("start"))
                startGame(request, response);
            else
                createGame(request, response);
        }else if(request.getString("method").equals("DELETE"))
            deleteGame(request, response);
    }

    private static void createGame(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");

        long userID = args.getLong("userID");
        long setupID = args.getLong("setupID");
        boolean isPublic = args.getBoolean("isPublic");
        String name = args.getString("hostName");
        GameCreateRequest gameSchema = new GameCreateRequest(name, setupID, isPublic);

        OpenLobby gameLobby = LobbyService.create(userID, gameSchema);
        response.put("response", new GameJson(gameLobby));
    }

    private static void deleteGame(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        GameID gameID = new GameID(args.getLong("gameID"));
        GameService.delete(gameID);
    }

    private static void getActiveGames(JSONObject response) throws JSONException, SQLException {
        Collection<GameLobbyGetResponse> instances = LobbyService.getAll();
        JSONArray responseArray = new JSONArray();
        for(GameLobbyGetResponse activeGames: instances)
            responseArray.put(GameJson.from(activeGames).toJson());

        response.put("response", responseArray);
    }

    private static void getGameBySetupID(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long setupID = args.getLong("setupID");

        Set<OpenLobby> games = LobbyService.getOpenGamesBySetupID(setupID);
        Set<GameJson> gameViews = GameJson.from(games);
        response.put("response", gameViews);
    }

    private static void getGameByID(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        GameID gameID = new GameID(args.getLong("gameID"));

        JSONObject instanceJSON;
        try{
            GameLobbyGetResponse serviceResponse = LobbyService.getGameLobbyByGameID(gameID);
            instanceJSON = GameJson.from(serviceResponse).toJson();
        }catch(NarratorException e){
            instanceJSON = new JSONObject();
        }

        response.put("response", instanceJSON);
    }

    private static void startGame(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        GameID gameID = new GameID(args.getLong("gameID"));
        GameLobby game = GameService.start(gameID);
        response.put("response", new GameJson(game));
    }
}

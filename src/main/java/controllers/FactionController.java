package controllers;

import java.sql.SQLException;
import java.util.Optional;

import json.JSONException;
import json.JSONObject;
import models.enums.AbilityType;
import models.json_output.AbilityJson;
import models.json_output.FactionJson;
import models.requests.FactionCreateRequest;
import models.requests.FactionUpdateRequest;
import models.serviceResponse.FactionAbilityCreateResponse;
import models.serviceResponse.FactionCreateResponse;
import models.serviceResponse.FactionDeleteResponse;
import services.FactionAbilityService;
import services.FactionService;

public class FactionController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            createFactionOrAddAbilityOrEnemyOrDetectable(request, response);
        else if(request.getString("method").equals("PUT"))
            editFaction(request, response);
        else if(request.getString("method").equals("DELETE"))
            deleteFactionOrEnemy(request, response);
    }

    private static void createFactionOrAddAbilityOrEnemyOrDetectable(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        if(args.has("description")){
            long setupID = args.getLong("setupID");
            String name = args.getString("name");
            String color = args.getString("color");
            String description = args.getString("description");

            FactionCreateRequest factionRequest = new FactionCreateRequest(name, color, description);
            FactionCreateResponse serviceResponse = FactionService.createFaction(userID, setupID, factionRequest);
            response.put("setupID", serviceResponse.faction.setup.id);
            response.put("joinID", serviceResponse.lobbyID);
            response.put("response", new FactionJson(serviceResponse.faction, Optional.of(serviceResponse.game)));
        }else if(args.has("abilityName")){
            createFactionAbility(userID, args, response);
            return;
        }else if(args.has("enemyID")){
            long factionID = args.getLong("factionID");
            long enemyID = args.getLong("enemyID");
            FactionService.addEnemy(userID, factionID, enemyID);
        }else if(args.has("checkableID")){
            long factionID = args.getLong("factionID");
            long checkableID = args.getLong("checkableID");
            FactionService.addSheriffCheckable(userID, factionID, checkableID);
        }
    }

    private static void createFactionAbility(long userID, JSONObject args, JSONObject response)
            throws JSONException, SQLException {
        AbilityType abilityType = AbilityType.narratorParse(args.getString("abilityName"));
        long factionID = args.getLong("factionID");

        FactionAbilityCreateResponse serviceResponse = FactionAbilityService.createFactionAbility(userID, factionID,
                abilityType);
        response.put("response", new AbilityJson(serviceResponse));
    }

    private static void editFaction(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long setupID = args.getLong("setupID");
        long factionID = args.getLong("factionID");
        FactionUpdateRequest serviceRequest = new FactionUpdateRequest(args);
        FactionService.update(setupID, factionID, serviceRequest);
    }

    private static void deleteFactionOrEnemy(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");

        if(args.has("enemyFactionID")){
            deleteFactionEnemy(request, response);
            return;
        }
        if(args.has("abilityID")){
            deleteFactionAbility(request, response);
            return;
        }

        long userID = args.getLong("userID");
        long factionID = args.getLong("factionID");

        FactionDeleteResponse serviceResponse = FactionService.deleteFaction(userID, factionID);
        JSONObject deleteResponse = new JSONObject();
        response.put("setupID", serviceResponse.setupID);
        response.put("joinID", serviceResponse.joinID);
        response.put("response", deleteResponse);
    }

    private static void deleteFactionEnemy(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");

        long setupID = args.getLong("setupID");
        long factionID = args.getLong("factionID");
        long enemyFactionID = args.getLong("enemyFactionID");

        FactionService.deleteEnemy(setupID, factionID, enemyFactionID);
    }

    private static void deleteFactionAbility(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");

        long factionID = args.getLong("factionID");
        long abilityID = args.getLong("abilityID");

        FactionAbilityService.deleteFactionAbility(factionID, abilityID);
    }

}

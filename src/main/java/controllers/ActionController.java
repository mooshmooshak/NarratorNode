package controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityUtil;
import game.event.Message;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.controllerResponse.AbilityMetadataJson;
import models.enums.AbilityType;
import models.idtypes.GameID;
import models.serviceResponse.AbilityMetadata;
import models.serviceResponse.ActionResponse;
import services.ActionService;

public class ActionController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("GET"))
            getAbilityMetadata(request, response);
        if(request.getString("method").equals("PUT"))
            submitActionController(request, response);
        else if(request.getString("method").equals("DELETE"))
            deleteActionController(request);
    }

    private static void getAbilityMetadata(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        GameID gameID = new GameID(args.getLong("gameID"));

        Set<AbilityMetadata> abilities = ActionService.getAbilityMetadata(userID, gameID);

        Set<AbilityMetadataJson> abilitiesJson = AbilityMetadataJson.toSet(abilities);
        response.put("response", abilitiesJson);
    }

    private static void submitActionController(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        double timeLeft = args.getDouble("timeLeft");
        ActionResponse actionResponse;
        if(args.has("message"))
            actionResponse = ActionService.submitAction(userID, args.getString("message"), timeLeft);
        else{
            String command = args.getString("command");
            AbilityType abilityType = AbilityUtil.getAbilityByCommand(command);
            List<String> actionArgs = new LinkedList<>();
            if(args.has("option"))
                actionArgs.add(args.getString("option"));
            if(args.has("option2"))
                actionArgs.add(args.getString("option2"));
            if(args.has("option3"))
                actionArgs.add(args.getString("option3"));
            Optional<Integer> replacingAction = Optional
                    .ofNullable(args.has("oldAction") ? args.getInt("oldAction") : null);

            JSONArray jTargets = args.getJSONArray("targets");
            List<String> targets = new ArrayList<>();
            for(int i = 0; i < jTargets.length(); i++)
                targets.add(jTargets.getString(i));
            actionResponse = ActionService.submitAction(userID, abilityType, targets, actionArgs, timeLeft,
                    replacingAction);
        }
        JSONObject actionResponseJson = new JSONObject();
        actionResponseJson.put("actions", actionResponse.actions);
        actionResponseJson.put("voteInfo", actionResponse.voteInfo);
        if(actionResponse.newActionConfirmMessage.isPresent())
            actionResponseJson.put("newActionConfirmText",
                    actionResponse.newActionConfirmMessage.get().access(Message.PUBLIC)); // probably need to put in
        // corresponding player
        response.put("response", actionResponseJson);
    }

    private static void deleteActionController(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        int actionIndex = args.getInt("actionIndex");
        String command = args.getString("command");
        AbilityType abilityType = AbilityUtil.getAbilityByCommand(command);
        double timeLeft = args.getDouble("timeLeft");
        ActionService.deleteAction(userID, abilityType, actionIndex, timeLeft);
    }
}

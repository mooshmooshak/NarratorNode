package controllers;

import java.sql.SQLException;

import json.JSONException;
import json.JSONObject;
import services.PreferService;

public class PreferController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        String method = request.getString("method");
        if(method.equals("POST"))
            prefer(request);

    }

    public static void prefer(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long setupID = args.getLong("setupID");
        String preferName = args.getString("preferName");

        PreferService.createPreference(userID, setupID, preferName);
    }

}

package controllers;

import java.sql.SQLException;
import java.util.Optional;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.json_output.SetupHiddenJson;
import models.serviceResponse.SetupHiddenAddResponse;
import services.SetupHiddenService;

public class SetupHiddenController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            createSetupHidden(request, response);
        else if(request.getString("method").equals("PUT"))
            editSetupHidden(request);
        else if(request.getString("method").equals("DELETE"))
            deleteHidden(request, response);
    }

    private static void createSetupHidden(JSONObject request, JSONObject response)
            throws JSONException, SQLException, NarratorException {
        JSONObject body = request.getJSONObject("body");
        long userID = body.getLong("userID");
        long hiddenID = body.getLong("hiddenID");
        int minPlayerCount = body.getInt("minPlayerCount");
        int maxPlayerCount = body.getInt("maxPlayerCount");
        boolean isExposed = body.getBoolean("isExposed");
        boolean mustSpawn = body.getBoolean("mustSpawn");
        SetupHiddenAddResponse setupHiddenAddResponse = SetupHiddenService.addSetupHidden(userID, hiddenID, isExposed,
                mustSpawn, minPlayerCount, maxPlayerCount);
        JSONObject setupHiddenAddJsonResponse = new SetupHiddenJson(setupHiddenAddResponse.setupHidden,
                Optional.empty()).toJson();
        setupHiddenAddJsonResponse.put("gameID", setupHiddenAddResponse.gameID);
        setupHiddenAddJsonResponse.put("joinID", setupHiddenAddResponse.joinID);
        response.put("response", setupHiddenAddJsonResponse);
    }

    private static void editSetupHidden(JSONObject request) throws JSONException, SQLException {
        JSONObject body = request.getJSONObject("body");
        long userID = body.getLong("userID");
        long setupHiddenID = body.getLong("setupHiddenID");
        boolean isExposed = body.getBoolean("isExposed");
        SetupHiddenService.setExposed(userID, setupHiddenID, isExposed);
    }

    private static void deleteHidden(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long setupHiddenID = args.getLong("setupHiddenID");

        GameID gameID = SetupHiddenService.deleteSetupHidden(userID, setupHiddenID);
        JSONObject deleteResponse = new JSONObject();
        deleteResponse.put("gameID", gameID);
        response.put("response", deleteResponse);
    }

}

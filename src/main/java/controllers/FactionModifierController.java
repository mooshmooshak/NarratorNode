package controllers;

import java.sql.SQLException;
import java.time.Instant;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.FactionModifierName;
import models.modifiers.FactionModifier;
import models.serviceResponse.ModifierServiceResponse;
import services.FactionModifierService;

public class FactionModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            upsertModifier(request, response);
    }

    private static void upsertModifier(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        String modifierName = args.getString("name");
        FactionModifierName name;
        try{
            name = FactionModifierName.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown faction modifier name: " + modifierName);
        }
        long userID = args.getLong("userID");
        long factionID = args.getLong("factionID");
        ModifierValue value = new ModifierValue(args.get("value"));
        int minPlayerCount = args.getInt("minPlayerCount");
        int maxPlayerCount = args.getInt("maxPlayerCount");

        FactionModifier modifier = new FactionModifier(name, value, minPlayerCount, maxPlayerCount, Instant.now());
        ModifierServiceResponse<FactionModifierName> serviceResponse = FactionModifierService.upsertModifier(userID,
                factionID, modifier);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.modifier.value.internalValue);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        updateResponse.put("joinID", serviceResponse.joinID);
        response.put("response", updateResponse);
    }

}

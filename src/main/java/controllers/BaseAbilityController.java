package controllers;

import java.util.Set;

import json.JSONException;
import json.JSONObject;
import models.BaseAbility;
import models.json_output.BaseAbilityView;
import services.BaseAbilityService;

public class BaseAbilityController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET"))
            getBaseAbilities(response);
    }

    private static void getBaseAbilities(JSONObject response) throws JSONException {
        Set<BaseAbility> baseAbilities = BaseAbilityService.getBaseAbilities();
        response.put("response", BaseAbilityView.getSet(baseAbilities));
    }

}

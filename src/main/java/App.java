import java.sql.SQLException;

import json.JSONObject;
import nnode.LobbyManager;
import nnode.LobbyManager.SwitchListener;
import repositories.BaseRepo;
import repositories.Connection;

public class App {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        BaseRepo.StartDriver();

        try{
            BaseRepo.connection = new Connection();
            runMain();
        }catch(Throwable t){
            t.printStackTrace();
        }
        BaseRepo.connection.close();
    }

    public static void runMain() {
        LobbyManager lobbies = new LobbyManager();

        LobbyManager.switchListener = new SwitchListener() {
            @Override
            public void onSwitchMessage(JSONObject jo) {
                System.out.println(jo.toString() + "$$");
            }
        };

        try{
            LobbyManager.reloadInstances();
        }catch(Throwable e){
            e.printStackTrace();
        }

        lobbies.start();
        System.err.println("Java ended unexpectedly.");
    }
}

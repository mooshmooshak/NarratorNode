package services;

import java.sql.SQLException;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.setups.Setup;
import models.LobbySetupSchema;
import models.ModifierValue;
import models.Role;
import models.SetupHidden;
import models.dbo.SetupDbo;
import models.enums.RoleModifierName;
import models.modifiers.Modifier;
import models.modifiers.RoleModifier;
import models.schemas.RoleModifierSchema;
import models.serviceResponse.ModifierServiceResponse;
import repositories.RoleModifierRepo;
import util.game.ModifierUtil;

public class RoleModifierService {

    public static void upsertModifier(Role role, RoleModifierName name, Object value) {
        upsertModifier(role, name, value, 0, Setup.MAX_PLAYER_COUNT);
    }

    public static void upsertModifier(Role role, RoleModifierName name, Object value, int minPlayerCount,
            int maxPlayerCount) {
        upsertModifier(role,
                new RoleModifier(name, new ModifierValue(value), minPlayerCount, maxPlayerCount, Instant.now()));
    }

    public static void upsertModifier(Role role, RoleModifier modifier) {
        runValidations(role, modifier);
        role.modifiers.add(modifier);
    }

    private static void checkForUnique(Role role, Modifier<RoleModifierName> modifier) {
        Setup setup = role.setup;
        if(ModifierUtil.IsBoolModifier(modifier.name))
            return;
        if(modifier.name != RoleModifierName.UNIQUE)
            return;
        int spawnCount = 0;
        for(SetupHidden setupHidden: setup.rolesList.iterable()){
            if(setupHidden.spawn == null)
                continue;
            if(setupHidden.spawn.role != role)
                continue;
            spawnCount++;
            if(spawnCount > 1)
                throw new IllegalGameSettingsException(
                        "This role cannot be unique because it's set to be spawned twice.");
        }

        boolean seenBefore = false;
        for(SetupHidden setupHidden: setup.rolesList.iterable()){
            if(!setupHidden.hidden.isHiddenSingle())
                continue;
            if(!setupHidden.hidden.contains(role))
                continue;
            if(seenBefore)
                throw new IllegalGameSettingsException(
                        "This role cannot be unique because it's already in the roles list more than once.");
            seenBefore = true;
        }
    }

    public static ModifierServiceResponse<RoleModifierName> upsertModifier(long userID, long roleID,
            RoleModifier newModifier) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        Role role = RoleService.get(userID, roleID).role;

        runValidations(role, newModifier);

        RoleModifierRepo.upsertModifier(roleID, newModifier);
        upsertModifier(role, newModifier);

        return new ModifierServiceResponse<>(lobbySetup.game, newModifier);
    }

    private static void runValidations(Role role, Modifier<RoleModifierName> newModifier) {
        ModifierUtil.checkModifierValueType(newModifier);
        checkRange(newModifier);
        checkForUnique(role, newModifier);
    }

    private static void checkRange(Modifier<RoleModifierName> newModifier) {
        if(ModifierUtil.IsIntModifier(newModifier.name)){
            int intValue = newModifier.value.getIntValue();
            if(intValue < Constants.UNLIMITED || intValue > Constants.MAX_INT_MODIFIER)
                throw new NarratorException("Invalid range input");
        }
    }

    public static void loadModifiers(Setup setup, SetupDbo setupDbo) {
        Map<Long, Set<RoleModifierSchema>> allModifiers = setupDbo.roleModifiers;

        Role role;
        for(long roleID: allModifiers.keySet()){
            role = setup.getRole(roleID);
            for(RoleModifierSchema roleModifier: allModifiers.get(roleID))
                upsertModifier(role, new RoleModifier(roleModifier));
        }
    }
}

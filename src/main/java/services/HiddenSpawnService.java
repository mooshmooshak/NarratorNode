package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import game.abilities.Hidden;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.FactionRole;
import models.HiddenSpawn;
import models.LobbySetupSchema;
import models.requests.HiddenSpawnCreateRequest;
import models.schemas.HiddenSpawnSchema;
import repositories.HiddenSpawnsRepo;
import util.Util;
import util.game.LookupUtil;

public class HiddenSpawnService {

    public static Collection<HiddenSpawnSchema> addSpawnable(long userID, ArrayList<HiddenSpawnCreateRequest> schemas)
            throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        // i pull setup, because I check whether the faction role is actually in game
        Setup setup = SetupService.getSetup(lobbySetup.setup.id);

        for(HiddenSpawnCreateRequest schema: schemas){
            // throws exceptions if not found
            setup.getHidden(schema.hiddenID);
            setup.getFactionRole(schema.factionRoleID);
        }

        ArrayList<Long> hiddenSpawnIDs = HiddenSpawnsRepo.create(setup.id, schemas);
        ArrayList<HiddenSpawnSchema> returnedSchemas = HiddenSpawnSchema.from(schemas, hiddenSpawnIDs);

        Hidden hidden;
        FactionRole factionRole;
        HiddenSpawn hiddenSpawn;
        for(HiddenSpawnSchema schema: returnedSchemas){
            hidden = LookupUtil.findHidden(setup, schema.hiddenID);
            factionRole = setup.getFactionRole(schema.factionRoleID);
            hiddenSpawn = new HiddenSpawn(schema.id, factionRole, schema.minPlayerCount, schema.maxPlayerCount);
            addSpawnableRoles(hidden, hiddenSpawn);
        }

        return returnedSchemas;
    }

    public static void addSpawnableRoles(Hidden hidden, Collection<FactionRole> roleList) {
        FactionRole[] roles = new FactionRole[roleList.size()];
        Iterator<FactionRole> iterator = roleList.iterator();
        int counter = 0;
        while (iterator.hasNext())
            roles[counter++] = iterator.next();
        addSpawnableRoles(hidden, roles);
    }

    public static void addSpawnableRoles(Hidden hidden, FactionRole... factionRoles) {
        for(FactionRole factionRole: factionRoles)
            addSpawnableRoles(hidden, factionRole, 0, Setup.MAX_PLAYER_COUNT);
    }

    public static void addSpawnableRoles(Hidden hidden, List<HiddenSpawn> hiddenSpawns) {
        for(HiddenSpawn hiddenSpawn: hiddenSpawns)
            addSpawnableRoles(hidden, hiddenSpawn);
    }

    public static void addSpawnableRoles(Hidden hidden, HiddenSpawn hiddenSpawn) {
        if(!hiddenSpawn.factionRole.faction.roleMap.values().contains(hiddenSpawn.factionRole))
            throw new NarratorException("This role is not in the setup.");
        hidden.spawns.add(new HiddenSpawn(hiddenSpawn.id, hiddenSpawn.factionRole, hiddenSpawn.minPlayerCount,
                hiddenSpawn.maxPlayerCount));
    }

    public static void addSpawnableRoles(Hidden hidden, FactionRole factionRole, int minPlayerCount,
            int maxPlayerCount) {
        hidden.spawns.add(new HiddenSpawn(Util.getID(), factionRole, minPlayerCount, maxPlayerCount));
    }

    public static void deleteHiddenSpawn(Hidden hidden, FactionRole factionRole) {
        for(HiddenSpawn hiddenSpawn: new LinkedList<>(hidden.spawns))
            if(hiddenSpawn.factionRole == factionRole)
                hidden.spawns.remove(hiddenSpawn);
    }

    public static void deleteHiddenSpawn(long userID, long hiddenSpawnID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        HiddenSpawnsRepo.delete(lobbySetup.setup.id, hiddenSpawnID);
    }

    public static void deleteHiddenSpawn(Game game, HiddenSpawn hiddenSpawn) {
        for(Hidden hidden: game.setup.hiddens)
            hidden.spawns.remove(hiddenSpawn);
    }
}

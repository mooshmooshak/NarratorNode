package services;

import java.sql.SQLException;
import java.util.Set;

import game.logic.Game;
import models.enums.GameModifierName;
import models.idtypes.GameID;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import repositories.GameModifierRepo;

public class GameModifierService {

    public static Modifiers<GameModifierName> getByGameID(GameID gameID) throws SQLException {
        Set<Modifier<GameModifierName>> modifiers = GameModifierRepo.getByGameID(gameID);
        return new Modifiers<>(modifiers);
    }

    public static void loadModifiers(Game game, GameID gameID) throws SQLException {
        Modifiers<GameModifierName> modifiers = getByGameID(gameID);
        for(Modifier<GameModifierName> modifier: modifiers)
            game.gameModifiers.modifiers.add(modifier);
    }

}

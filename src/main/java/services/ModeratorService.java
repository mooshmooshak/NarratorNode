package services;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import models.events.HostChangeEvent;
import models.idtypes.GameID;
import models.schemas.GameOverviewSchema;
import models.schemas.GameUserSchema;
import models.schemas.PlayerSchema;
import nnode.Permission;
import nnode.WebCommunicator;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.UserPermissionRepo;
import util.CollectionUtil;
import util.Util;

public class ModeratorService {
    public static Map<GameID, Set<Long>> lobbyRepickers = new HashMap<>();

    public static void create(long userID) throws SQLException {
        Optional<GameUserSchema> gameUserOpt = GameUserRepo.getActiveByUserID(userID);
        if(!gameUserOpt.isPresent())
            throw new NarratorException("User is not in any game.");

        GameOverviewSchema game = GameRepo.getOverview(gameUserOpt.get().gameID);

        if(game.isFinished)
            return;

        if(game.isStarted)
            throw new NarratorException("Moderators may not be created midgame.");

        Optional<PlayerSchema> player = PlayerRepo.getActiveByUserID(userID);
        if(!player.isPresent())
            return;

        if(!GameUserRepo.getModeratorIDs(game.id).contains(userID))
            throw new NarratorException("Only hosts can use this command.");

        PlayerRepo.deleteByUserAndGame(userID, game.id);
        setModerator(game, userID);
    }

    public static Set<Long> getModeratorIDs(GameID gameID) throws SQLException {
        return GameUserRepo.getModeratorIDs(gameID);
    }

    public static void delete(long userID, String playerName) throws SQLException {
        Optional<GameID> gameID = GameUserRepo.getActiveGameIDByUserID(userID);
        if(!gameID.isPresent())
            throw new NarratorException("User is not in any lobby.");

        PlayerService.addToGame(gameID.get(), userID, playerName);
    }

    private static Set<Long> setHost(GameID gameID, long userID) throws SQLException {
        resetRepickers(gameID);
        GameUserRepo.setModerator(userID, gameID);
        try{
            GameOverviewSchema game = GameRepo.getOverview(gameID);
            WebCommunicator.pushOut(HostChangeEvent.request(game.lobbyID, userID));
        }catch(JSONException e){
            Util.log(e, "Failed to send host change event.");
        }
//      resetSetupHiddenSpawns();
        return CollectionUtil.toSet(userID);
    }

    private static Set<Long> setAnyHostFromActives(Set<Long> oldModeratorIDs, GameID gameID, Set<Long> activeUserIDs)
            throws SQLException {
        activeUserIDs.removeAll(oldModeratorIDs);
        if(activeUserIDs.isEmpty())
            return setAnyHost(oldModeratorIDs, gameID);
        long newHostID = activeUserIDs.iterator().next();
        return setHost(gameID, newHostID);
    }

    static Set<Long> setAnyHost(Set<Long> oldModeratorIDs, GameID gameID) throws SQLException {
        Set<Long> userIDs = GameUserRepo.getIDs(gameID);
        userIDs.removeAll(oldModeratorIDs);
        long newHostID = userIDs.isEmpty() ? oldModeratorIDs.iterator().next() : userIDs.iterator().next();
        return setHost(gameID, newHostID);
    }

    private static Optional<Long> getUserID(GameID id, String message) throws SQLException {
        Optional<PlayerSchema> player = PlayerRepo.getByNameAndGameID(message, id);
        if(!player.isPresent())
            return Optional.empty();
        if(player.get().userID.isPresent())
            return Optional.of(player.get().userID.get());
        return Optional.empty();
    }

    public static Set<Long> repick(long repickerID, String message, Set<Long> activeUserIDs) throws SQLException {
        GameID gameID = LobbyService.assertInLobby(repickerID);
        Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(gameID);
        if(!moderatorIDs.contains(repickerID)
                && UserPermissionRepo.containsPermission(moderatorIDs, Permission.GAME_EDITING))
            throw new NarratorException("Admins cannot be repicked.");

        Optional<Long> targetID = getUserID(gameID, message);

        if(moderatorIDs.contains(repickerID) || UserPermissionRepo.hasPermission(repickerID, Permission.GAME_EDITING)){
            if(targetID.isPresent())
                return setHost(gameID, targetID.get());

            // game editing permission
            if(!moderatorIDs.contains(repickerID))
                return setHost(gameID, repickerID);

            return setAnyHostFromActives(moderatorIDs, gameID, new HashSet<>(activeUserIDs));
        }

        if(!lobbyRepickers.containsKey(gameID))
            lobbyRepickers.put(gameID, new HashSet<>());
        Set<Long> repickerIDs = lobbyRepickers.get(gameID);
        repickerIDs.add(repickerID);

        int gameUserCount = GameUserRepo.getIDs(gameID).size();
        if(repickerIDs.size() >= gameUserCount / 2 + 1)
            return setAnyHostFromActives(moderatorIDs, gameID, activeUserIDs);
        return moderatorIDs;
    }

    private static void setModerator(GameOverviewSchema game, long userID) throws SQLException {
        GameUserRepo.setModerator(userID, game.id);
        PlayerRepo.deleteByUserAndGame(userID, game.id);
        LobbyService.getByGame(game).sendGameState();
    }

    public static void assertIsGameModerator(GameID id, long userID, String errorMessage) throws SQLException {
        if(!GameUserRepo.getModeratorIDs(id).contains(userID))
            throw new NarratorException(errorMessage);
    }

    public static void resetRepickers(GameID gameID) {
        lobbyRepickers.remove(gameID);
    }
}

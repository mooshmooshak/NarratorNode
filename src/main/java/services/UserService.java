package services;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import json.JSONException;
import json.JSONObject;
import models.PlayerUserIDMap;
import models.User;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.schemas.PlayerSchema;
import nnode.Lobby;
import nnode.LobbyManager;
import nnode.WebCommunicator;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.UserRepo;

public class UserService {

    public static Set<Long> getUserIDs(GameID gameID) throws SQLException {
        return GameUserRepo.getIDs(gameID);
    }

    public static Set<Long> getUserIDs(PlayerList players) throws SQLException {
        Set<PlayerDBID> playerIDs = players.getDatabaseMap().keySet();
        return PlayerRepo.getActiveUserIDs(playerIDs);
    }

    public static Optional<Long> getUserID(Player player) throws SQLException {
        PlayerSchema schema = PlayerRepo.getByID(player.databaseID);
        return schema.userID;
    }

    public static PlayerUserIDMap getPlayerUserMap(Game game) throws SQLException {
        Map<Player, Optional<Long>> playerUserMap = new HashMap<>();
        for(Player player: game.players)
            playerUserMap.put(player, getUserID(player));
        return new PlayerUserIDMap(playerUserMap);
    }

    public static User getUser(long userID) throws SQLException {
        String name = UserRepo.getName(userID);
        return new User(userID, name);
    }

    private static final long MINUTE = 60000;
    private static Map<Long, Long> userIDToLastPinged = new HashMap<>();

    public static void ping(String playerName, GameID id, long pingerUserID) throws JSONException, SQLException {
        Optional<PlayerSchema> optPlayer = PlayerRepo.getByNameAndGameID(playerName, id);
        if(!optPlayer.isPresent()){
            WebCommunicator.pushMessageTextToUser("Could not find that player.", pingerUserID);
            return;
        }
        PlayerSchema player = optPlayer.get();
        if(!player.userID.isPresent())
            return;

        long pingedUserID = player.userID.get();
        Long lastPinged = userIDToLastPinged.get(pingedUserID);
        long currentTime = System.currentTimeMillis();
        if(currentTime - lastPinged < 2 * MINUTE){
            String warning = "This user cannot be pinged again so soon.";
            WebCommunicator.pushWarningToUser(warning, pingerUserID);
        }else{
            JSONObject jo = Lobby.GetGUIObject();
            jo.put("ping", true);
            WebCommunicator.pushToUser(jo, pingedUserID);
            lastPinged = currentTime;
        }
    }

    public static void updatePointsAndWinRate(GameID gameID, String name, int pointIncrease) throws SQLException {
        Optional<Long> optUserID = PlayerRepo.getUserID(gameID, name);
        if(!optUserID.isPresent())
            return;
        UserRepo.updatePointsAndWinRate(optUserID.get(), pointIncrease);
    }

    public static Map<Long, Optional<Player>> getUserPlayerMap(GameID gameID) throws SQLException {
        Lobby lobby = null;
        Map<Long, Optional<Player>> userPlayerMap = new HashMap<>();
        for(Lobby pLobby: LobbyManager.idToLobby.values()){
            if(pLobby.gameID.equals(gameID))
                lobby = pLobby;
        }
        if(lobby == null)
            return userPlayerMap;
        Set<PlayerSchema> players = PlayerRepo.getByGameID(gameID);

        for(PlayerSchema player: players){
            if(player.userID.isPresent())
                userPlayerMap.put(player.userID.get(), Optional.of(lobby.game.players.getByID(player.name)));
        }

        for(long userID: UserService.getUserIDs(gameID)){
            if(userPlayerMap.containsKey(userID))
                continue;
            userPlayerMap.put(userID, Optional.empty());
        }

        return userPlayerMap;
    }

    public static Map<Long, String> getNamesMap(Set<Long> userIDs) throws SQLException {
        return UserRepo.getNames(userIDs);
    }
}

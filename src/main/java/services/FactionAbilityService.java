package services;

import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.LobbySetupSchema;
import models.dbo.SetupDbo;
import models.enums.AbilityType;
import models.requests.AbilityCreateRequest;
import models.schemas.AbilitySchema;
import models.serviceResponse.FactionAbilityCreateResponse;
import repositories.FactionAbilityRepo;
import util.Util;

public class FactionAbilityService {

    public static void loadFactionAbilities(Setup setup, SetupDbo setupDbo) {
        Map<Long, Set<AbilitySchema>> allAbilities = setupDbo.factionAbilities;

        Faction faction;
        Set<AbilitySchema> abilities;
        for(long factionID: allAbilities.keySet()){
            faction = setup.factions.get(factionID);
            abilities = allAbilities.get(factionID);
            for(AbilitySchema schema: abilities)
                FactionAbilityService.createFactionAbility(faction, schema);
        }
        FactionAbilityModifierService.loadFactionModifiers(setup, setupDbo);
    }

    public static FactionAbilityCreateResponse createFactionAbility(long userID, long factionID,
            AbilityType abilityType) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        Setup setup = SetupService.getSetup(lobbySetup.setup.id);
        Faction faction = setup.getFaction(factionID);

        AbilityCreateRequest request = new AbilityCreateRequest(abilityType);
        long factionAbilityID = FactionAbilityRepo.create(factionID, request);

        Ability ability = FactionAbilityService.createFactionAbility(faction, abilityType);

        ability.id = factionAbilityID;
        Game game = GameService.getGame(lobbySetup.game.id, setup);

        return new FactionAbilityCreateResponse(ability, faction.color, faction.name, Optional.of(game),
                faction.abilities);
    }

    public static void deleteFactionAbility(long factionID, long abilityID) throws SQLException {
        FactionAbilityRepo.delete(factionID, abilityID);
    }

    public static Ability createFactionAbility(Faction faction, AbilityType abilityClass) {
        AbilitySchema schema = new AbilitySchema(Util.getID(), abilityClass);
        return FactionAbilityService.createFactionAbility(faction, schema);
    }

    public static Ability createFactionAbility(Faction faction, AbilitySchema schema) {
        Ability ability = new Ability(faction.setup, schema);
        if(ability.type.dbName == null)
            throw new IllegalGameSettingsException("Cannot add this ability.");

        if(!ability.getGameAbility().isAllowedFactionAbility())
            throw new IllegalActionException("This ability cannot be shared.");

        for(Ability factionAbility: faction.abilities)
            if(ability.type.equals(factionAbility.type))
                throw new NarratorException("This faction already has this ability.");

        // if team has shared ability, they need to know their teammates
        faction.abilities.add(ability);
        return ability;
    }

    public static void deleteFactionAbility(Faction faction, AbilityType abilityType) {
        Ability ability = faction.getAbility(abilityType);
        if(ability != null)
            faction.abilities.remove(ability);
    }

}

package services;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import game.abilities.GameAbility;
import game.abilities.Vote;
import game.abilities.support.Charge;
import game.event.Feedback;
import game.event.Message;
import game.logic.Game;
import game.logic.GameRole;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.support.HTString;
import models.FactionRole;
import models.PendingRole;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import util.game.ChargeUtil;

public class RoleChangeService {

    public static void changeRoleAndTeam(Player owner, PendingRole pendingRole) {
        if(isAlreadyTakenUniqueFactionRole(owner.game, pendingRole))
            return;
        owner.getGameFaction().removeMember(owner);
        owner.game.getFaction(pendingRole.factionRole.getColor()).addMember(owner);

        owner.changeRole(pendingRole);
    }

    public static void resolveRoleChange(Player player) {
        if(player.pendingRole == null)
            return;
        if(isAlreadyTakenUniqueFactionRole(player.game, player.pendingRole))
            return;
        PendingRole pendingRole = player.pendingRole;
        for(GameAbility a: player.gameRole._abilities)
            a.onAbilityLoss(player);

        GameRole oldRole = player.gameRole;
        player.setRole(pendingRole.factionRole);

        transferNonFactionRoleAbilities(oldRole, player.gameRole, player);

        // happening message is already taken care of
        // works because i'm going to keep the modifier map the same in all
        if(player.pendingRole.sourceRole.isPresent()){
            adjustAmnesiacCharges(player, player.pendingRole.sourceRole.get());
            return;
        }

        // I don't want really want a feedback being sent before game start
        // but I guess it's not the end of the world
        // adding this in to stop chat message error
        //
        // I could always set game phases before i do mason leader promos
        if(player.game.phase == GamePhase.UNSTARTED)
            return;

        Message e = new Feedback(player, "You are now a ");
        HTString ht = new HTString(pendingRole.factionRole);
        e.add(ht);
        e.add(".");
    }

    private static void transferNonFactionRoleAbilities(GameRole oldRole, GameRole newRole, Player player) {
        for(GameAbility gameAbility: oldRole._abilities){
            if(gameAbility instanceof Vote){
                newRole._abilities.add(gameAbility);
                continue;
            }

            List<Charge> gainedCharges = new LinkedList<>();
            for(Charge charge: gameAbility.charges){
                if(!charge.isFromRole())
                    gainedCharges.add(charge);
            }

            if(gainedCharges.isEmpty())
                continue;

            for(Charge charge: gainedCharges)
                player.addConsumableCharge(gameAbility.getAbilityType(), charge);
        }
    }

    private static boolean isAlreadyTakenUniqueFactionRole(Game game, PendingRole pendingRole) {
        FactionRole factionRole = pendingRole.factionRole;

        // a promotion bypasses unique roles.
        // it's intended for there to be more than one
        if(pendingRole.isPromotion)
            return false;

        if(!factionRole.isUnique(game.players.size()))
            return false;
        for(Player player: game.players){
            if(player.gameRole.factionRole == factionRole)
                return true;
        }
        return false;
    }

    private static void adjustAmnesiacCharges(Player player, GameRole sourceRole) {
        GameRole newRole = player.gameRole;
        Game game = newRole.game;
        GameAbility newAbility;
        boolean amnesiacKeepsCharges = game.getBool(SetupModifierName.AMNESIAC_KEEPS_CHARGES);

        for(GameAbility sourceAbility: sourceRole._abilities){
            newAbility = newRole.getAbility(sourceAbility.getAbilityType());
            if(amnesiacKeepsCharges){
                int initialCharges = sourceAbility.getRealCharges();
                if(initialCharges == 0)
                    initialCharges++;
                setRoleCharges(player, newAbility, initialCharges);
            }else if(sourceAbility.unlimitedCharges())
                setRoleCharges(player, newAbility, Constants.UNLIMITED);
            else{
                int initialCharges = ChargeUtil.AddNoise(1, game);
                setRoleCharges(player, newAbility, initialCharges);
            }
        }
    }

    private static void setRoleCharges(Player player, GameAbility ability, int initialCharges) {
        if(initialCharges == Constants.UNLIMITED)
            ability.charges.setUnlimited();
        else{
            Iterator<Charge> iterator = ability.charges.iterator();
            while (iterator.hasNext()){
                if(!iterator.next().isFromRole())
                    continue;
                if(initialCharges == 0)
                    iterator.remove();
                else
                    initialCharges--;
            }
            for(int i = 0; i < initialCharges; i++)
                player.addConsumableCharge(ability.getAbilityType(), ability.getCharge(true));
        }
    }
}

package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.ai.Computer;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.OpenLobby;
import models.enums.GameModifierName;
import models.events.PlayerAddedEvent;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.modifiers.Modifiers;
import models.schemas.GameOverviewSchema;
import models.schemas.GameUserSchema;
import models.schemas.PlayerCreateSchema;
import models.schemas.PlayerSchema;
import models.serviceResponse.BotAddResponse;
import models.serviceResponse.PlayerKickResponse;
import models.serviceResponse.PlayerNameUpdateResponse;
import nnode.Lobby;
import nnode.WebCommunicator;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.PlayerRoleRepo;
import util.PlayerUtil;
import util.game.GameUtil;

public class PlayerService {

    public static BotAddResponse addBots(long userID, int botCount) throws SQLException {
        GameID gameID = LobbyService.assertIsModerator(userID);
        GameOverviewSchema game = GameService.assertNotStarted(gameID, "Cannot add bots to a started game.");
        Setup setup = SetupService.getSetup(game.setupID);

        Set<PlayerSchema> players = PlayerRepo.getByGameID(gameID);

        Set<String> botNames = getBotNames(botCount, players);

        ArrayList<PlayerCreateSchema> newBotRequests = new ArrayList<>();
        PlayerCreateSchema playerModel;
        for(String name: botNames){
            playerModel = new PlayerCreateSchema(name, Optional.empty());
            newBotRequests.add(playerModel);
        }

        ArrayList<PlayerDBID> playerIDs = PlayerRepo.create(gameID, newBotRequests);
        PlayerDBID dbID;
        PlayerCreateSchema schema;
        for(int i = 0; i < playerIDs.size(); i++){
            dbID = playerIDs.get(i);
            schema = newBotRequests.get(i);
            players.add(new PlayerSchema(dbID, gameID, schema.name, Optional.empty()));
        }

        Game unstartedGame = GameService.getGame(gameID, setup);
        return new BotAddResponse(unstartedGame, players);
    }

    private static Set<String> getBotNames(int botCount, Set<PlayerSchema> players) {
        Map<String, PlayerSchema> map = PlayerSchema.mapByName(players);

        Set<String> botNames = new HashSet<>();
        String name;
        int index = 1;
        while (botNames.size() < botCount){
            name = Computer.toLetter(index);
            if(!map.containsKey(name))
                botNames.add(name);
            index += 1;
        }

        return botNames;
    }

    public static PlayerKickResponse deleteGameUser(long userID, long kickedUserID) throws SQLException {
        GameID gameID = LobbyService.assertIsModerator(userID);
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        Set<Long> moderatorIDs = LobbyService.kickGameUserPlayer(userID, kickedUserID);
        Game unstartedGame = GameService.getGame(game);
        return new PlayerKickResponse(moderatorIDs, unstartedGame);
    }

    public static OpenLobby joinAny(long userID, String playerName) throws SQLException {
        GameID gameID = LobbyService.getLargestUnfilledLobby();
        return addToGame(gameID, userID, playerName);
    }

    public static OpenLobby joinSpecific(long userID, String lobbyID, String playerName) throws SQLException {
        lobbyID = lobbyID.toUpperCase();
        Optional<GameOverviewSchema> game = GameRepo.getByLobbyID(lobbyID);
        if(game.isPresent())
            return addToGame(game.get().id, userID, playerName);
        throw new NarratorException("No lobby with that lobbyID found.");
    }

    public static void modkill(long userID, double timeLeft) throws SQLException {
        Optional<Player> optPlayer = getByUserID(userID);
        if(!optPlayer.isPresent())
            throw new NarratorException("User is not participating in any game.");
        Player player = optPlayer.get();
        if(!player.game.isInProgress())
            throw new NarratorException("Game is not in progress.");
        if(player.isDead())
            throw new NarratorException("Player is already dead.");

        player.modkill(timeLeft);

        GameService.endGameIfNoParticipatingPlayersAlive(player.game);
    }

    public static void savePlayersToGame(GameID gameID, Game game) {
        Map<PlayerDBID, Long> assignments = new HashMap<>();
        for(Player p: game.getAllPlayers())
            assignments.put(p.databaseID, p.initialAssignedRole.assignedSetupHidden.id);

        try{
            PlayerRoleRepo.insertAssignments(gameID, assignments);
        }catch(SQLException e){
            throw new NarratorException("Failed to save player assignments");
        }
    }

    public static Optional<PlayerNameUpdateResponse> updateName(long userID, String name) throws SQLException {
        PlayerSchema player = assertInActiveGame(userID);
        if(player.name.equals(name))
            return Optional.empty();
        GameOverviewSchema game = GameRepo.getOverview(player.gameID);
        if(game.isStarted)
            throw new NarratorException("Cannot change names in a started game.");

        Set<PlayerSchema> players = PlayerRepo.getByGameID(player.gameID);
        Set<String> names = PlayerUtil.getNames(players);
        Setup setup = SetupService.getSetup(game.setupID);
        NamingService.playerNameCheck(name, names, setup);
        PlayerRepo.updateName(player.id, name);

        LobbyService.getByGame(game).sendPlayerLists();
        return Optional.of(new PlayerNameUpdateResponse(game.lobbyID, player.id));
    }

    public static Game deletePlayer(GameID gameID, PlayerDBID dbID) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        if(game.isStarted)
            throw new NarratorException("Can't delete bots from started game.");
        PlayerRepo.deleteByID(gameID, dbID);
        return GameService.getGame(game);
    }

    static OpenLobby addToGame(GameID gameID, long userID, String playerName) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        if(game.isStarted)
            throw new NarratorException("Game is already started.");

        if(!game.isFinished && PlayerRepo.getActiveByUserID(userID).isPresent())
            throw new NarratorException("User is already in a game.");

        Set<PlayerSchema> players = PlayerRepo.getByGameID(gameID);

        Setup setup = SetupService.getSetup(game.setupID);
        Modifiers<GameModifierName> modifiers = GameModifierService.getByGameID(gameID);
        Game genericGame = GameUtil.getGeneric(setup, players.size(), modifiers);
        if(SetupService.isFull(genericGame)){
            Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(gameID);
            WebCommunicator.pushWarningToUser(playerName + " tried to join your game.", moderatorIDs);
            throw new NarratorException("Game is full.");
        }

        Set<String> playerNames = PlayerUtil.getNames(players);
        playerName = NamingService.getAcceptablePlayerName(playerName, playerNames, setup);
        PlayerDBID dbID = null;

        try{
            GameUserRepo.setExitedExcept(userID, gameID);
            dbID = PlayerRepo.create(gameID, userID, playerName);
            PlayerSchema player = new PlayerSchema(dbID, gameID, playerName, Optional.of(userID));
            GameUserRepo.insert(new GameUserSchema(userID, gameID, false, false));
            players.add(player);
        }catch(SQLException e){
            if(dbID == null)
                throw e;
            try{
                PlayerRepo.deleteByID(gameID, dbID);
            }catch(SQLException e1){
                e1.printStackTrace();
            }
            try{
                GameUserRepo.deleteByUserIDGameID(userID, gameID);
            }catch(SQLException e1){
                e1.printStackTrace();
            }
            throw e;
        }

        Set<Long> userIDs = GameUserRepo.getIDs(gameID);
        userIDs.remove(userID);
        if(userIDs.size() == 1)
            ModeratorService.resetRepickers(gameID);

        OpenLobby openLobby = LobbyService.getOpenLobby(game, players, setup);
        Game unstartedGame = openLobby.game;
        WebCommunicator.pushOut(
                new PlayerAddedEvent(openLobby.game, game.lobbyID, game.id, userID, dbID, playerName, players));

        int playerCount = players.size();
        if(playerCount == setup.rolesList.size(unstartedGame))
            Lobby.pushToSlackUsers(game.lobbyID, game.id, "Game is ready to start!");
        ChatService.pushUnreads(gameID, userID);
        return openLobby;
    }

    public static Optional<Player> getByUserID(long userID) throws SQLException {
        Optional<PlayerSchema> player = PlayerRepo.getActiveByUserID(userID);
        return getByUserID(player);
    }

    public static Optional<Player> getByUserID(Optional<PlayerSchema> player) throws SQLException {
        if(!player.isPresent())
            return Optional.empty();
        GameID gameID = player.get().gameID;
        Lobby lobby = LobbyService.getByGameID(gameID);
        return Optional.ofNullable(lobby.game.getPlayerByID(player.get().name));
    }

    public static Optional<Player> getByUserID(long userID, Lobby lobby) throws SQLException {
        Optional<PlayerSchema> player = PlayerRepo.getActiveByUserID(userID);
        if(!player.isPresent())
            return Optional.empty();
        return Optional.ofNullable(lobby.game.getPlayerByID(player.get().name));
    }

    public static Player assertUserInStartedGame(long userID) throws SQLException {
        Optional<Player> player = getByUserID(userID);
        if(player.isPresent())
            return player.get();
        throw new NarratorException("User is not in a player in a game.");
    }

    public static PlayerSchema assertInActiveGame(long userID) throws SQLException {
        Optional<PlayerSchema> player = PlayerRepo.getActiveByUserID(userID);
        if(player.isPresent())
            return player.get();
        throw new NarratorException("User is not a player in game.");
    }

    public static void loadPlayers(Game game, Set<PlayerSchema> players) {
        Player player;
        for(PlayerSchema schema: players){
            player = game.addPlayer(schema.name);
            player.databaseID = schema.id;
            if(!schema.userID.isPresent())
                player.setComputer();
        }
    }
}

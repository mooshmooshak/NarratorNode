package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.abilities.Punch;
import game.abilities.Thief;
import game.abilities.Vote;
import game.ai.Brain;
import game.event.EventManager;
import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.RoleAssigner;
import game.logic.support.RolePackage;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.GameLobby;
import models.GeneratedRole;
import models.ModifierValue;
import models.SetupHidden;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import models.enums.VoteSystemTypes;
import models.idtypes.GameID;
import models.logic.vote_systems.DiminishingPoolVoteSystem;
import models.logic.vote_systems.MultiVotePluralitySystem;
import models.logic.vote_systems.NoEliminationVoteSystem;
import models.logic.vote_systems.PluralityVoteSystem;
import models.modifiers.GameModifier;
import models.schemas.GameOverviewSchema;
import models.schemas.PlayerSchema;
import models.serviceResponse.ModifierServiceResponse;
import nnode.Config;
import nnode.Lobby;
import nnode.LobbyManager;
import repositories.GameModifierRepo;
import repositories.GameRepo;
import repositories.PlayerRepo;
import util.Util;
import util.game.ModifierUtil;

public class GameService {

    public static Map<GameID, Game> gamesMap = new HashMap<>();

    public static Game getByGameID(GameID gameID) throws SQLException {
        Game game = gamesMap.get(gameID);
        if(game != null)
            return game;
        GameOverviewSchema overview = GameRepo.getOverview(gameID);
        if(overview.isFinished)
            throw new NarratorException("Game is finished.");
        return getGame(overview);
    }

    public static Game getGame(GameOverviewSchema game) throws SQLException {
        Set<PlayerSchema> players = PlayerRepo.getByGameID(game.id);
        return getGame(game, players);
    }

    public static Game getGame(GameOverviewSchema game, Set<PlayerSchema> players) throws SQLException {
        Setup setup = SetupService.getSetup(game.setupID);
        return getGame(game.id, players, setup);
    }

    public static Game getGame(GameID gameID, Setup setup) throws SQLException {
        Set<PlayerSchema> players = PlayerRepo.getByGameID(gameID);
        return getGame(gameID, players, setup);
    }

    public static Game getGame(GameID gameID, Set<PlayerSchema> players, Setup setup) throws SQLException {
        Game game = new Game(setup);
        GameModifierService.loadModifiers(game, gameID);
        PlayerService.loadPlayers(game, players);
        return game;
    }

    public static void insertModifiers(GameID gameID, Game game) throws SQLException {
        GameModifierRepo.insert(gameID, game.gameModifiers.modifiers);
    }

    public static ModifierServiceResponse<GameModifierName> upsertModifier(long userID, GameModifierName name,
            ModifierValue value) throws SQLException {
        GameID gameID = LobbyService.assertIsModerator(userID);
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        GameModifier modifier = new GameModifier(name, value);

        if(game.isStarted)
            throw new NarratorException("Cannot edit games that are in progress.");

        ModifierUtil.checkModifierValueType(modifier);
        if(name == GameModifierName.VOTE_SYSTEM && VoteSystemTypes.getByValue(value.getIntValue()) == null)
            throw new NarratorException("No vote system type found with that value.");
        checkBounds(modifier);

        GameModifierRepo.upsertModifier(modifier, gameID);

        return new ModifierServiceResponse<>(game, modifier);
    }

    private static void checkBounds(GameModifier modifier) {
        if(!ModifierUtil.IsIntModifier(modifier.name))
            return;
        int value = modifier.value.getIntValue();
        GameModifierName name = modifier.name;
        switch (name) {
        case DAY_LENGTH_MIN:
        case DAY_LENGTH_START:
            ModifierUtil.minOutOfBoundsException(name, value, 60);
            break;
        case ROLE_PICKING_LENGTH:
            ModifierUtil.minOutOfBoundsException(name, value, 30);
            break;
        case DISCUSSION_LENGTH:
        case NIGHT_LENGTH:
        case TRIAL_LENGTH:
            ModifierUtil.minOutOfBoundsException(name, value, 0);
            break;
        default:
            ModifierUtil.minOutOfBoundsException(name, value, 0);
            return;
        }
    }

    // move to test util, rename to update
    public static void upsertModifier(Game narrator, GameModifierName name, boolean value) {
        GameModifier modifier = new GameModifier(name.hashCode(), name, new ModifierValue(value));
        ModifierUtil.checkModifierValueType(modifier);
        narrator.gameModifiers.modifiers.add(modifier);
    }

    // move to test util, rename to update
    public static void upsertModifier(Game narrator, GameModifierName name, int value) {
        GameModifier modifier = new GameModifier(name.hashCode(), name, new ModifierValue(value));
        ModifierUtil.checkModifierValueType(modifier);
        narrator.gameModifiers.modifiers.add(modifier);
    }

    public static void delete(GameID gameID) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        LobbyService.delete(game);
    }

    public static GameOverviewSchema assertNotStarted(GameID gameID, String message) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        if(game.isStarted)
            throw new NarratorException(message);
        return game;
    }

    public static GameLobby start(GameID gameID) throws SQLException {
        GameOverviewSchema gameOverview = GameRepo.getOverview(gameID);
        if(gameOverview.isStarted)
            throw new NarratorException("Game is already started.");

        Set<PlayerSchema> players = PlayerRepo.getByGameID(gameID);
        Game game = getGame(gameOverview, players);
        PreferService.loadPreferences(players, game);

        gamesMap.put(gameID, game);

        Lobby lobby = new Lobby(game, gameOverview);
        LobbyManager.idToLobby.put(gameOverview.lobbyID, lobby);

        try{
            lobby.startGame(gameOverview.isPrivate);
        }catch(Exception e){
            LobbyManager.idToLobby.remove(gameOverview.lobbyID);
            gamesMap.remove(gameID);
            throw e;
        }
        return LobbyService.getGameLobby(lobby);
    }

    public static void internalStart(Game game) {
        internalStart(game, new RoleAssigner());
    }

    public static void internalStart(Game game, RoleAssigner roleAssigner) {
        if(game.isStarted())
            throw new NarratorException("Cannot start game again.");

        game.getRandom().reset();
        SetupValidationService.runSetupChecks(game);
        createGameFactions(game);

        PlayerList players = game.players;
        players.sortByID(); // TOOD really don't need this

        if(Config.getBoolean("random_role_assignment"))
            game.players = game.players.shuffle(new Random(), "initial shuffling for role assignments");

        for(Player p: players){
            game.events.dayChat.addAccess(p);
            game.events.voidChat.addAccess(p);
        }

        game.prehappenings = new ArrayList<>();

        int playerCount = players.size();
        List<SetupHidden> rolesListByPlayerCount = game.setup.rolesList.getSpawningSetupHiddens(game);
        ArrayList<GeneratedRole> generatedRoles = roleAssigner.pickRoles(game, rolesListByPlayerCount);
        ArrayList<GeneratedRole> assignableRoles = roleAssigner.getPassOutRoles(playerCount, generatedRoles);
        game.unpickedRoles = getUnpickedRoles(generatedRoles, assignableRoles);

        List<RolePackage> releasedRoles = new LinkedList<>();
        for(GeneratedRole role: assignableRoles)
            releasedRoles.add(new RolePackage(role.setupHidden, role.getFactionRole()));
        roleAssigner.passRolesOut(game, releasedRoles);

        game.remainingRoles = null;

        game.actionStack = new ArrayList<Action>();

        game.isStarted = true;

        game.getRandom().reset();
        game.masonLeaderPromotion();
        game.firstPhaseStarted = true;

        game.players.sortByName();

        boolean dayStart = game.getBool(SetupModifierName.DAY_START);
        boolean hasPotentialThief = game.setup.rolesList.contains(Thief.abilityType, game.players.size());
        if(dayStart == Game.DAY_START)
            game.dayNumber = 1;
        else
            game.dayNumber = 0;

        initializeVoteSystem(game);

        if(dayStart == Game.NIGHT_START){
            game.phase = GamePhase.NIGHT_ACTION_SUBMISSION;
            game.events.initializeTeamChats();

            for(Happening h: game.prehappenings)
                game.events.voidChat.add(h);
        }else{
            for(Happening h: game.prehappenings)
                game.events.getDayChat().add(h);
        }
        game.prehappenings = null;

        // I might, in the future, have to do a onGameStarting and onGameStarted
        for(NarratorListener element: game.listeners)
            element.onGameStart();
        if(hasPotentialThief)
            startRolePickingPhase(game);
        else
            startPlayerPhases(game);
    }

    private static void createGameFactions(Game game) {
        game._factions = new HashMap<>();
        GameFaction gameFaction;
        for(Faction faction: game.setup.getPossibleFactions(game)){
            gameFaction = new GameFaction(game, faction);
            game._factions.put(faction.color, gameFaction);
        }
        HashSet<String> teamColors = new HashSet<String>();
        for(SetupHidden setupHidden: game.setup.rolesList.iterable(game)){
            for(FactionRole factionRole: setupHidden.hidden.getAllFactionRoles()){
                teamColors.add(factionRole.faction.getColor());
            }
        }

        Faction faction;
        for(String validColor: teamColors){
            faction = game.setup.getFactionByColor(validColor);
            game._factions.put(validColor, new GameFaction(game, faction));
        }
        Faction skipFaction = new Faction(game.setup, Util.getID(), "Skip Team", Constants.A_SKIP, "");
        GameFaction skipGameFaction = new GameFaction(game, skipFaction);
        game._factions.put(skipGameFaction.getColor(), skipGameFaction);
    }

    private static Set<RolePackage> getUnpickedRoles(ArrayList<GeneratedRole> generatedRoles,
            ArrayList<GeneratedRole> assignableRoles) {
        generatedRoles = new ArrayList<>(generatedRoles);
        for(GeneratedRole generatedRole: assignableRoles)
            generatedRoles.remove(generatedRole);

        Set<RolePackage> unpickedRoles = new HashSet<>();
        for(GeneratedRole generatedRole: generatedRoles)
            unpickedRoles.add(new RolePackage(generatedRole.setupHidden, generatedRole.getFactionRole()));

        return unpickedRoles;
    }

    private static void initializeVoteSystem(Game narrator) {
        VoteSystemTypes voteSystemType = VoteSystemTypes.getByValue(narrator.getInt(GameModifierName.VOTE_SYSTEM));
        switch (voteSystemType) {
        case PLURALITY:
            narrator.voteSystem = new PluralityVoteSystem(narrator);
            break;

        case DIMINISHING_POOL:
            narrator.voteSystem = new DiminishingPoolVoteSystem(narrator);
            break;
        case MULTI_VOTE_PLURALITY:
            narrator.voteSystem = new MultiVotePluralitySystem(narrator);
            break;
        case NO_ELIMINATION:
            narrator.voteSystem = new NoEliminationVoteSystem(narrator);
            break;
        default:
            break;
        }
    }

    private static void startRolePickingPhase(Game narrator) {
        narrator.phase = GamePhase.ROLE_PICKING_PHASE;
        List<NarratorListener> listeners = narrator.getListeners();
        for(NarratorListener listener: listeners)
            listener.onRolepickingPhaseStart();
    }

    public static void startPlayerPhases(Game narrator) {
        // assigns targets to possible executioners
        narrator.getRandom().reset();
        for(GameFaction t: narrator.getFactions())
            t.onGameStart();

        EventManager events = narrator.events;
        for(Player p: narrator.players.copy().sortByID()){
            if(narrator.getBool(SetupModifierName.DAY_START))
                p.addChat(events.dayChat);
            else
                p.addChat(events.voidChat);
            p.onGameStart();

            // refactor out
            // When punching is toggled on, everyone should get the ability
            // if punching is toggled off, everyone should lose the ability
            // this could even be a 'setup ablity', where everyone has this ability because
            // of the setup, and not because of the individual role
            if(narrator.getBool(SetupModifierName.PUNCH_ALLOWED))
                p.addAbility(Punch.abilityType);
            p.addAbility(Vote.abilityType);
        }
        if(narrator.getBool(SetupModifierName.DAY_START) == Game.NIGHT_START)
            narrator.startNight(null, null, null);
        else
            narrator.startDay(new PlayerList());
    }

    public static void endGameIfNoParticipatingPlayersAlive(Game game) {
        for(Player player: game.players){
            if(player.isComputer())
                continue;
            if(player.isParticipating())
                return;
        }
        if(!game.isFinished())
            Brain.EndGame(game, 0);
    }

}

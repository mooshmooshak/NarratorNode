package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.FactionRole;
import models.HiddenSpawn;
import models.LobbySetupSchema;
import models.SetupHidden;
import models.dbo.SetupDbo;
import models.requests.HiddenSpawnCreateRequest;
import models.schemas.HiddenSchema;
import models.schemas.HiddenSpawnSchema;
import models.serviceResponse.HiddenCreateResponse;
import repositories.HiddenRepo;
import repositories.HiddenSpawnsRepo;
import util.Util;
import util.game.HiddenUtil;

public class HiddenService {

    // roleIDs size will be greater than one. checked
    public static HiddenCreateResponse createHidden(long userID, long setupID, String name) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        if(setupID != lobbySetup.setup.id)
            throw new NarratorException("Setup id does not match the game's setup id.");

        Setup setup = SetupService.getSetup(lobbySetup.setup.id);
        NamingService.hiddenNameCheck(name, setup);
        long hiddenID = HiddenRepo.create(setupID, name);
        Hidden hidden = createValidatedHidden(setup, new HiddenSchema(hiddenID, name));
        Game game = GameService.getGame(lobbySetup.game.id, setup);
        return new HiddenCreateResponse(hidden, game);
    }

    public static Hidden createHidden(Setup setup, String hiddenName, FactionRole... factionRoles) {
        Hidden hidden = createHidden(setup, hiddenName);
        for(FactionRole factionRole: factionRoles)
            HiddenSpawnService.addSpawnableRoles(hidden, factionRole, 0, Setup.MAX_PLAYER_COUNT);
        return hidden;
    }

    public static Hidden createHidden(Setup setup, String hiddenName) {
        return createValidatedHidden(setup, new HiddenSchema(Util.getID(), hiddenName));
    }

    public static Hidden createHidden(Setup setup, HiddenSchema schema) {
        NamingService.hiddenNameCheck(schema.name, setup);
        return createValidatedHidden(setup, schema);
    }

    private static Hidden createValidatedHidden(Setup setup, HiddenSchema schema) {
        Hidden hidden = new Hidden(schema, setup);
        setup.hiddens.add(hidden);
        return hidden;
    }

    public static void loadHiddens(Setup setup, SetupDbo setupDbo) {
        Set<HiddenSchema> hiddens = setupDbo.hiddenDbos;
        for(HiddenSchema schema: hiddens)
            createHidden(setup, schema);

        Hidden hidden;
        FactionRole factionRole;
        Set<HiddenSpawnSchema> setupfactionRoleIDs = setupDbo.hiddenSpawns;
        for(HiddenSpawnSchema schema: setupfactionRoleIDs){
            hidden = setup.getHidden(schema.hiddenID);
            factionRole = setup.getFactionRole(schema.factionRoleID);
            addFactionRole(schema.id, hidden, factionRole, schema.minPlayerCount, schema.maxPlayerCount);
        }

    }

    public static void insertHiddens(long setupID, ArrayList<Hidden> hiddens) throws SQLException {
        ArrayList<Long> hiddenIDs = new ArrayList<>();

        try{
            // create hiddens
            hiddenIDs.addAll(HiddenRepo.create(setupID, HiddenUtil.getArrayNames(hiddens)));
            for(int i = 0; i < hiddens.size(); i++)
                hiddens.get(i).id = hiddenIDs.get(i);

            // add hidden spawns
            ArrayList<HiddenSpawn> hiddenSpawns = new ArrayList<>();
            ArrayList<HiddenSpawnSchema> hiddenSpawnSchemas = new ArrayList<>();
            for(Hidden hidden: hiddens){
                for(HiddenSpawn hiddenSpawn: hidden.spawns){
                    hiddenSpawns.add(hiddenSpawn);
                    hiddenSpawnSchemas.add(new HiddenSpawnSchema(hidden, hiddenSpawn));
                }
            }
            ArrayList<Long> spawnIDs = HiddenSpawnsRepo.create(setupID,
                    HiddenSpawnCreateRequest.from(hiddenSpawnSchemas));
            for(int i = 0; i < spawnIDs.size(); i++)
                hiddenSpawns.get(i).id = spawnIDs.get(i);
        }catch(SQLException | IndexOutOfBoundsException e){
            try{
                HiddenRepo.deleteByIDs(setupID, hiddenIDs);
            }catch(SQLException f){
                Util.log("Failed to cleanup hiddens after a bad insert.");
            }
            throw e;
        }
        for(int i = 0; i < hiddens.size(); i++)
            hiddens.get(i).id = hiddenIDs.get(i);
    }

    public static void addFactionRole(long spawnID, Hidden hidden, FactionRole factionRole, int minPlayerCount,
            int maxPlayerCount) {
        HiddenSpawn hiddenSpawn = new HiddenSpawn(spawnID, factionRole, minPlayerCount, maxPlayerCount);
        hidden.spawns.add(hiddenSpawn);
    }

    public static void removeFactionRole(Hidden hidden, HiddenSpawn hiddenSpawn) {
        for(SetupHidden setupHidden: hidden.setup.rolesList.iterable())
            if(setupHidden.hidden == hidden && setupHidden.spawn == hiddenSpawn.factionRole)
                SetupHiddenService.removeForcedSpawn(setupHidden);

        hidden.spawns.remove(hiddenSpawn);
    }

    public static void delete(long userID, long hiddenID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        HiddenRepo.deleteByID(lobbySetup.setup.id, hiddenID);
    }

    public static void delete(Hidden hidden) {
        Setup setup = hidden.setup;
        setup.rolesList.removeAll(hidden);
        setup.hiddens.remove(hidden);
    }
}

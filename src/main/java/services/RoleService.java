package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityUtil;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.LobbySetupSchema;
import models.Role;
import models.dbo.SetupDbo;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.PreferType;
import models.idtypes.GameID;
import models.modifiers.Modifiers;
import models.schemas.GameOverviewSchema;
import models.schemas.RoleSchema;
import models.serviceResponse.RoleCreateResponse;
import models.serviceResponse.RoleGetResponse;
import repositories.GameRepo;
import repositories.PlayerRepo;
import repositories.PreferRepo;
import repositories.RoleAbilityModifierRepo;
import repositories.RoleAbilityRepo;
import repositories.RoleModifierRepo;
import repositories.RoleRepo;
import util.CollectionUtil;
import util.Util;
import util.game.RoleUtil;

public class RoleService {

    public static RoleCreateResponse createRole(long userID, long setupID, String name,
            LinkedHashSet<AbilityType> abilityTypes) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        Setup setup = SetupService.getSetup(lobbySetup.setup.id);
        if(setupID != lobbySetup.setup.id)
            throw new NarratorException("Setup id does not match the game's setup id.");

        NamingService.roleNameCheck(name, setup);
        long roleID = RoleRepo.create(setupID, name);
        Role role = createValidatedRole(setup, new RoleSchema(roleID, name));

        try{
            RoleAbilityService.createRoleAbilities(setup, role.id, abilityTypes);
        }catch(SQLException e){
            try{
                delete(setupID, roleID);
            }catch(SQLException f){
                Util.log(f);
            }
            throw e;
        }

        Game game = GameService.getGame(lobbySetup.game.id, setup);
        return new RoleCreateResponse(role, game);
    }

    public static Role createRole(Setup setup, String name, AbilityType... abilitiyTypes) {
        NamingService.roleNameCheck(name, setup);
        Role role = createValidatedRole(setup, new RoleSchema(Util.getID(), name));
        RoleAbilityService.createRoleAbilities(role, CollectionUtil.toSet(abilitiyTypes));
        return role;
    }

    public static Role createRole(Setup setup, RoleSchema schema) {
        NamingService.roleNameCheck(schema.name, setup);
        return createValidatedRole(setup, schema);
    }

    private static Role createValidatedRole(Setup setup, RoleSchema roleSchema) {
        Role role = new Role(setup, roleSchema);
        setup.roles.add(role);
        return role;
    }

    static void insert(long setupID, ArrayList<Role> roles) throws SQLException {
        ArrayList<Long> roleIDs = new ArrayList<>();
        try{
            // create roles
            roleIDs.addAll(RoleRepo.create(setupID, RoleUtil.getNamesList(roles)));
            for(int i = 0; i < roleIDs.size(); i++)
                roles.get(i).id = roleIDs.get(i);

            // add role modifiers
            RoleModifierRepo.create(roles);

            // add role abilities
            ArrayList<Ability> roleAbilities = new ArrayList<>();
            ArrayList<Long> roleAbilityRoleIDs = new ArrayList<>();
            for(Role role: roles)
                for(Ability ability: role.getAbilities()){
                    roleAbilities.add(ability);
                    roleAbilityRoleIDs.add(role.id);
                }
            ArrayList<Long> roleAbilityIDs = RoleAbilityRepo.create(roleAbilityRoleIDs,
                    AbilityUtil.toCreateRequests(roleAbilities));
            for(int i = 0; i < roleAbilities.size(); i++)
                roleAbilities.get(i).id = roleAbilityIDs.get(i);

            // add role ability modifiers
            ArrayList<Modifiers<AbilityModifierName>> modifiersList = new ArrayList<>();
            for(Role role: roles)
                for(Ability ability: role.getAbilities())
                    modifiersList.add(ability.modifiers);
            RoleAbilityModifierRepo.create(roleAbilityIDs, modifiersList);
        }catch(SQLException | IndexOutOfBoundsException e){
            try{
                RoleRepo.deleteByIDs(setupID, roleIDs);
            }catch(SQLException f){
                Util.log(f, "Failed to cleanup roles after a bad insert.");
            }
            throw e;
        }
    }

    public static RoleGetResponse get(long userID, long roleID) throws SQLException {
        GameID gameID = LobbyService.assertInLobby(userID);
        GameOverviewSchema gameOverview = GameRepo.getOverview(gameID);

        Setup setup;
        Optional<Game> optGame;
        int playerCount;
        if(gameOverview.isInProgress()){
            Game game = GameService.getByGameID(gameID);
            setup = game.setup;
            optGame = Optional.of(game);
            playerCount = game.players.size();
        }else{
            setup = SetupService.getSetup(gameOverview.setupID);
            optGame = Optional.empty();
            playerCount = PlayerRepo.getByGameID(gameID).size();
        }
        Role role = setup.getRole(roleID);
        return new RoleGetResponse(role, optGame, playerCount);
    }

    public static void loadRoles(Setup setup, SetupDbo setupDbo) {
        Set<RoleSchema> roles = setupDbo.roleDbos;
        for(RoleSchema schema: roles)
            createRole(setup, schema);
        RoleModifierService.loadModifiers(setup, setupDbo);
        RoleAbilityService.loadAbilities(setup, setupDbo);
    }

    public static void delete(long userID, long roleID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        RoleRepo.deleteByIDs(lobbySetup.setup.id, roleID);
        PreferRepo.delete(lobbySetup.setup.id, roleID, PreferType.ROLE);
    }

    public static void delete(Role role) {
        List<FactionRole> factionRoles = new ArrayList<>();
        for(Faction faction: role.setup.factions.values())
            factionRoles.add(faction.roleMap.get(role));
        for(FactionRole factionRole: factionRoles)
            if(factionRole != null)
                FactionRoleService.deleteFactionRole(factionRole);

        role.setup.roles.remove(role);
    }

}

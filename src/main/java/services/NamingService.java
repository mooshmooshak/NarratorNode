package services;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import game.abilities.Armorsmith;
import game.abilities.Blacksmith;
import game.abilities.DrugDealer;
import game.abilities.Gunsmith;
import game.abilities.Hidden;
import game.abilities.Joker;
import game.event.ArchitectChat;
import game.logic.Game;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.setups.Setup;
import models.Faction;
import models.Role;
import models.enums.AbilityType;
import util.FactionUtil;
import util.Util;
import util.game.HiddenUtil;

public class NamingService {
    public static final String[] INVALID_NAMES = { Armorsmith.FAKE, Blacksmith.NOGUN, Gunsmith.FAULTY, Gunsmith.REAL,
            Joker.NO_BOUNTY, Constants.DAY_CHAT, Constants.DEAD_CHAT, "null", Constants.LAST_WILL,
            Constants.TEAM_LAST_WILL };

    private static void nameCheck(String name, boolean allowSpaces, boolean canCollideWithHiddens, Setup setup) {
        Set<String> factionNames = FactionUtil.getNames(setup.getFactions());
        Set<String> hiddenNames = HiddenUtil.getNames(setup.hiddens);
        nameCheck(name, allowSpaces, canCollideWithHiddens, factionNames, hiddenNames);
    }

    private static void nameCheck(String name, boolean allowSpaces, boolean canCollideWithHiddens,
            Set<String> factionNames, Set<String> hiddenNames) {
        if(name == null)
            throw new NamingException("Name cannot be null");
        if(name.length() > Constants.MAX_DB_PLAYER_LENGTH)
            throw new NamingException("Name too long");
        if(name.equals(""))
            throw new NamingException("Cannot have empty names");

        if(name.contains(" ") && !allowSpaces)
            throw new NamingException("No spaces in names");
        if(Util.hasNonAlphaNumericString(name))
            throw new NamingException("Names must be alphanumeric.");
        if(Util.isInt(name))
            throw new NamingException("Name cannot be a number!");
        for(String invalid: INVALID_NAMES){
            if(invalid.equalsIgnoreCase(name))
                throw new NamingException("Reserved name");
        }
        for(AbilityType abilityType: AbilityType.values())
            if(abilityType.command != null && abilityType.command.equalsIgnoreCase(name))
                throw new NamingException("Reserved name");

        if(name.contains(ArchitectChat.KEY_SEPERATOR))
            throw new NamingException("Cannot use " + ArchitectChat.KEY_SEPERATOR + " in name.");

        for(String invalid: DrugDealer.DRUGS)
            if(invalid.equalsIgnoreCase(name))
                throw new NamingException("Reserved name");
        if(name.startsWith("#") && name.length() == 7)
            throw new NamingException("Cannot use a color value as a name");

        if(factionNames.contains(name))
            throw new NamingException("Cannot name yourself a name that's already being used by a team name");

        if(!canCollideWithHiddens && hiddenNames.contains(name))
            throw new NamingException("Name in use by hidden.");
    }

    public static void roleNameCheck(String name, Setup setup) {
        Matcher m = pattern.matcher(name);
        if(!m.matches())
            throw new NarratorException("Team has invalid characters");
        for(String r_name: NamingService.RESERVED_FACTION_NAMES){
            if(r_name.equalsIgnoreCase(name))
                throw new NamingException("This name is reserved.");
        }
        for(Role role: setup.roles){
            if(role.getName().equalsIgnoreCase(name))
                throw new NamingException("Team name is taken by a role.");
        }

        // allow Spaces, allow hidden name collision
        nameCheck(name, true, true, setup);
    }

    private static final String factionRegex = "^[ a-zA-Z0-9]+$";
    public static final Pattern pattern = Pattern.compile(factionRegex);
    public static final String[] RESERVED_FACTION_NAMES = { "Random", "Randoms", "Neutral", "Neutrals" };

    public static void factionNameCheck(String name, Setup setup) {
        roleNameCheck(name, setup);
        for(Hidden hidden: setup.hiddens){
            if(hidden.name.equals(name))
                throw new NamingException("Team name is already taken by a hidden.");
        }

        // allow spaces, don't allow hidden name collision
        NamingService.nameCheck(name, true, false, setup);
    }

    public static void hiddenNameCheck(String name, Setup setup) {
        for(Faction faction: setup.factions.values()){
            if(faction.name.equals(name))
                throw new NarratorException("Hidden name is already in use by a faction.");
        }
    }

    private final static Pattern lastIntPattern = Pattern.compile("[^0-9]+([0-9]+)$");

    public static String getAcceptablePlayerName(String name, Set<String> playerNames, Setup setup) {
        name = Util.stripNonAlphaNumericCharacters(name);
        if(Util.isInt(name))
            name = "guest" + name;
        if(name.length() > Constants.MAX_DB_PLAYER_LENGTH)
            name = "guest";

        if(NamingService.isAcceptablePlayerName(name, playerNames, setup))
            return name;

        Matcher matcher = lastIntPattern.matcher(name);
        if(matcher.find()){
            String someNumberStr = matcher.group(1);
            int lastNumberInt = Integer.parseInt(someNumberStr) + 1;
            name = name.replace(someNumberStr, Integer.toString(lastNumberInt));
        }else{
            name = name + "2";
        }
        if(!NamingService.isAcceptablePlayerName(name, playerNames, setup))
            return getAcceptablePlayerName(name, playerNames, setup);

        return name;
    }

    public static boolean isAcceptablePlayerName(String name, Game game) {
        try{
            playerNameCheck(name, game);
            return true;
        }catch(NamingException e){
            return false;
        }
    }

    private static boolean isAcceptablePlayerName(String name, Set<String> playerNames, Setup setup) {
        try{
            playerNameCheck(name, playerNames, setup);
            return true;
        }catch(NamingException e){
            return false;
        }
    }

    public static void playerNameCheck(String name, Game game) {
        Set<String> playerNames = new HashSet<>(game.players.getNamesToStringList());
        playerNameCheck(name, playerNames, game.setup);
    }

    public static void playerNameCheck(String name, Set<String> playerNames, Setup setup) {
        for(String playerName: playerNames){
            if(playerName.equalsIgnoreCase(name))
                throw new NamingException("player name is already taken.");
        }
        // no spaces in name, no hidden name collision
        nameCheck(name, false, false, setup);
    }
}

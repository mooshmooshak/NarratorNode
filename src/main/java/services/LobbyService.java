package services;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.GameLobby;
import models.OpenLobby;
import models.events.LobbyDeleteEvent;
import models.events.PlayerRemovedEvent;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.schemas.GameCreateRequest;
import models.schemas.GameOverviewSchema;
import models.schemas.GameUserSchema;
import models.schemas.PlayerSchema;
import models.serviceResponse.GameLobbyGetResponse;
import nnode.Lobby;
import nnode.LobbyManager;
import nnode.WebCommunicator;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import util.CollectionUtil;
import util.Util;
import util.game.GameUtil;
import util.game.RandomUtil;

public class LobbyService {

    public static Collection<GameLobbyGetResponse> getAll() throws SQLException {
        Set<GameOverviewSchema> unfinishedGames = GameRepo.getUnfinished();
        Set<GameID> gameIDs = GameUtil.getGameIDs(unfinishedGames);

        Map<GameID, Set<Long>> gameIDToHostID = GameUserRepo.getModeratorIDs(gameIDs);
        Collection<GameLobbyGetResponse> games = new LinkedList<>();

        Set<Long> moderatorIDs;
        Lobby lobby;
        OpenLobby openLobby;
        GameLobby gameLobby;
        for(GameOverviewSchema game: unfinishedGames){
            if(game.isStarted){
                if(!gameIDToHostID.containsKey(game.id))
                    continue;
                lobby = LobbyManager.idToLobby.get(game.lobbyID);
                moderatorIDs = gameIDToHostID.get(lobby.gameID);
                gameLobby = new GameLobby(lobby, moderatorIDs, UserService.getUserPlayerMap(lobby.gameID));
                games.add(new GameLobbyGetResponse(gameLobby));
            }else{
                openLobby = LobbyService.getOpenLobby(game);
                games.add(new GameLobbyGetResponse(openLobby));
            }
        }
        return games;

    }

    public static Set<OpenLobby> getOpenGamesBySetupID(long setupID) throws SQLException {
        Set<GameOverviewSchema> games = GameRepo.getOpenGames();

        Set<OpenLobby> openLobbies = new HashSet<>();
        for(GameOverviewSchema game: games)
            openLobbies.add(LobbyService.getOpenLobby(game));

        return openLobbies;
    }

    public static GameLobbyGetResponse getGameLobbyByGameID(GameID gameID) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        if(game.isStarted)
            return new GameLobbyGetResponse(getGameLobby(getByGame(game)));

        return new GameLobbyGetResponse(LobbyService.getOpenLobby(game));
    }

    public static Optional<Lobby> getStartedByUserID(long userID) throws SQLException {
        Optional<GameID> gameID = GameUserRepo.getActiveGameIDByUserID(userID);
        if(!gameID.isPresent())
            return Optional.empty();
        return Optional.of(getByGameID(gameID.get()));
    }

    public static GameID assertIsModerator(long hostID, String message) throws SQLException {
        Optional<GameUserSchema> optUser = GameUserRepo.getActiveByUserID(hostID);
        if(!optUser.isPresent())
            throw new NarratorException("No game found.");
        if(!optUser.get().isModerator)
            throw new NarratorException(message);
        return optUser.get().gameID;
    }

    public static GameID assertIsModerator(long hostID) throws SQLException {
        return assertIsModerator(hostID, "User is not a moderator.");
    }

    public static GameLobbyGetResponse getByLobbyID(String joinID) throws SQLException {
        Lobby lobby = LobbyManager.idToLobby.get(joinID.toUpperCase());
        if(lobby != null)
            return new GameLobbyGetResponse(LobbyService.getGameLobby(lobby));
        Optional<GameOverviewSchema> schema = GameRepo.getByLobbyID(joinID);
        if(schema.isPresent())
            return new GameLobbyGetResponse(LobbyService.getOpenLobby(schema.get()));
        throw new NarratorException("No game with that lobby ID: " + joinID);

    }

    private static Random r = new Random();
    private static final int offset = 'A';

    private static String getLobbyID(Set<String> lobbyIDs) {
        int id = r.nextInt(456976) + 1;
        StringBuilder sb = new StringBuilder();

        int i;
        while (sb.length() < 4){
            i = (id % 26) + offset;
            sb.append(((char) i));
            id -= (i - offset);
            id /= 26;
        }
        String word = sb.toString();
        if(Arrays.asList(LobbyManager.BAD_WORDS).contains(word))
            return getLobbyID(lobbyIDs);
        if(lobbyIDs.contains(word))
            return getLobbyID(lobbyIDs);
        return word;
    }

    public static OpenLobby create(long userID, GameCreateRequest request) throws SQLException {
        GameID gameID;
        try{
            Set<String> currentLobbyIDs = GameRepo.getOpenLobbyIDs();
            String lobbyID = getLobbyID(currentLobbyIDs);
            gameID = GameRepo.create(lobbyID, !request.isPublic, RandomUtil.getLong(), request.setupID);
        }catch(SQLException e){
            throw e;
        }

        try{
            OpenLobby lobby = PlayerService.addToGame(gameID, userID, request.hostName);
            GameUserRepo.setModerator(userID, gameID);
            lobby.users.iterator().next().isModerator = true;
            return lobby;
        }catch(Throwable e){
            GameRepo.deleteByID(gameID);
            throw e;
        }
    }

    public static void delete(GameOverviewSchema game) {
        GameID gameID = game.id;
        try{
            if(game.isFinished){
                GameUserRepo.setExitedForAllPlayers(gameID);
                GameRepo.removeLobbyID(gameID);
            }else
                GameRepo.deleteByID(gameID);
        }catch(SQLException e){
            Util.log(e);
        }

        WebCommunicator.pushOut(new LobbyDeleteEvent(game.lobbyID, game.id));

        if(game.isStarted){
            Lobby lobby = LobbyManager.idToLobby.get(game.lobbyID);
            lobby.killDBConnection();
            LobbyManager.idToLobby.remove(game.lobbyID);
        }
        GameService.gamesMap.remove(gameID);
    }

    public static OpenLobby getOpenLobby(GameOverviewSchema gameOverview, Set<PlayerSchema> players, Setup setup)
            throws SQLException {
        GameID gameID = gameOverview.id;
        Game game = GameService.getGame(gameID, players, setup);
        Set<GameUserSchema> users = GameUserRepo.getByGameID(gameID);
        return new OpenLobby(gameID, gameOverview.lobbyID, game, players, users);
    }

    public static OpenLobby getOpenLobby(GameOverviewSchema game) throws SQLException {
        Setup setup = SetupService.getSetup(game.setupID);
        Set<PlayerSchema> players = PlayerRepo.getByGameID(game.id);
        return getOpenLobby(game, players, setup);
    }

    public static Lobby getByGameID(GameID gameID) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        return getByGame(game);
    }

    public static Lobby getByGame(GameOverviewSchema schema) throws SQLException {
        for(Lobby lobby: LobbyManager.idToLobby.values()){
            if(lobby.getGameID().equals(schema.id))
                return lobby;
        }

        // unstarted games
        if(!schema.isFinished){
            Game game = GameService.getGame(schema);
            return new Lobby(game, schema);
        }
        throw new NarratorException("Game with that ID not found.");
    }

    public static GameLobby getGameLobby(Lobby lobby) throws SQLException {
        Set<Long> moderatorIDs = GameUserRepo.getModeratorIDs(lobby.gameID);
        Map<Long, Optional<Player>> userPlayerMap = UserService.getUserPlayerMap(lobby.gameID);
        return new GameLobby(lobby, moderatorIDs, userPlayerMap);
    }

    public static GameID assertInLobby(long userID) throws SQLException {
        Optional<GameID> optGameID = GameUserRepo.getActiveGameIDByUserID(userID);
        if(optGameID.isPresent())
            return optGameID.get();
        throw new NarratorException("User is not in a game.");
    }

    // returns moderatorIDs
    public static Set<Long> kickGameUserPlayer(long kickerID, long kickedID) throws SQLException {
        GameID gameID = assertInLobby(kickerID);
        GameOverviewSchema game = GameRepo.getOverview(gameID);

        Set<Long> moderatorIDs = null;
        if(kickerID != kickedID){
            moderatorIDs = ModeratorService.getModeratorIDs(gameID);
            if(!moderatorIDs.contains(kickerID))
                throw new NarratorException("Only hosts can use this command.");
        }

        // throws error if player shouldn't leave.
        if(game.isInProgress())
            assertCanLeaveInProgressGame(kickedID);

        if(game.isStarted){
            try{
                GameUserRepo.setExited(gameID, kickedID);
            }catch(SQLException e){
                Util.log(e, "Failed to set player exit");
            }
        }

        Set<Long> activeUserIDs = GameUserRepo.getActiveIDsByGameID(gameID);
        if(activeUserIDs.isEmpty() || (activeUserIDs.size() == 1 && activeUserIDs.contains(kickedID))){
            delete(game);
            return new HashSet<>();
        }

        if(moderatorIDs == null)
            moderatorIDs = ModeratorService.getModeratorIDs(gameID);

        Optional<PlayerSchema> playerOpt = PlayerRepo.getActiveByUserID(kickedID);
        if(!game.isStarted){
            PlayerRepo.deleteByUserAndGame(kickedID, gameID);
            GameUserRepo.deleteByUserIDGameID(kickedID, gameID);
            activeUserIDs.remove(kickedID);

            if(moderatorIDs.contains(kickedID))
                ModeratorService.setAnyHost(CollectionUtil.toSet(kickedID), gameID);
        }
        ChatService.deleteUserFromGame(gameID, kickedID);

        if(game.isStarted || !playerOpt.isPresent())
            return moderatorIDs;

        String playerName = playerOpt.get().name;
        PlayerDBID playerID = playerOpt.get().id;
        Game unstartedGame = GameService.getGame(game);
        PlayerRemovedEvent event = new PlayerRemovedEvent(kickedID, playerID, playerName, unstartedGame, game.lobbyID,
                moderatorIDs);
        WebCommunicator.pushOut(event);
        return moderatorIDs;
    }

    private static void assertCanLeaveInProgressGame(long leaverID) throws SQLException {
        Optional<Player> optPlayer = PlayerService.getByUserID(leaverID);
        if(!optPlayer.isPresent())
            return;

        Player player = optPlayer.get();
        if(player.isEliminated())
            return;

        for(Player other: player.game.players)
            if(other != player && !other.isEliminated() && !other.isComputer())
                throw new NarratorException("You cannot leave if you are alive.");
    }

    public static GameID getLargestUnfilledLobby() throws SQLException {
        Set<GameOverviewSchema> games = GameRepo.getOpenGames();
        if(games.isEmpty())
            throw new NarratorException("No game to be joined.");

        GameOverviewSchema selectedGame = null;
        Integer selectedPlayerCount = null;
        Set<PlayerSchema> players;
        Game game;
        for(GameOverviewSchema gameOverview: games){
            players = PlayerRepo.getByGameID(gameOverview.id);
            if(selectedGame == null){
                selectedGame = gameOverview;
                selectedPlayerCount = players.size();
            }else{
                game = GameService.getGame(gameOverview);
                if(players.size() > selectedPlayerCount && SetupService.isFull(game)){
                    selectedGame = gameOverview;
                    selectedPlayerCount = players.size();
                }
            }
        }

        if(selectedGame == null)
            throw new NarratorException("No game to be joined.");
        return selectedGame.id;
    }

}

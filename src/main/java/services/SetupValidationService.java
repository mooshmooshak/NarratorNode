package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.abilities.AbilityList;
import game.abilities.CultLeader;
import game.abilities.Cultist;
import game.abilities.Executioner;
import game.abilities.Ghost;
import game.abilities.Hidden;
import game.abilities.Jester;
import game.abilities.Mason;
import game.abilities.MasonLeader;
import game.abilities.Sheriff;
import game.abilities.Thief;
import game.logic.Game;
import game.logic.RolesList;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.NamingException;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.GeneratedRole;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import util.FactionUtil;
import util.Util;
import util.game.HiddenUtil;
import util.game.RoleUtil;

public class SetupValidationService {

    /**
     * Determines if a the setup is okay to run as is. If not, it'll throw an error
     * describing what is wrong.
     *
     */
    public static void runSetupChecks(Game game) {
        randomChecks(game);
        sizeCheck(game);
        opponentCheck(game);

        cultLeaderChecks(game);
        executionerChecks(game);
        masonLeaderChecks(game);
        sheriffChecks(game);

        invalidRulesCheck(game);
        if(!game.getBool(GameModifierName.CHAT_ROLES))
            chatRoleChecks(game);

        playerNamesCheck(game);
    }

    private static void masonLeaderChecks(Game game) {
        for(FactionRole factionRole: game.setup.getPossibleFactionRolesWithAbility(game, MasonLeader.abilityType)){
            if(factionRole.faction.getFactionRolesWithAbility(Mason.abilityType).isEmpty())
                throw new IllegalGameSettingsException(factionRole.getName() + " needs a mason role to be added to "
                        + factionRole.faction.getName() + ".");
        }
    }

    private static void cultLeaderChecks(Game game) {
        if(game.getBool(SetupModifierName.CULT_KEEPS_ROLES))
            return;
        for(FactionRole factionRole: game.setup.getPossibleFactionRolesWithAbility(game, CultLeader.abilityType)){
            if(factionRole.faction.getFactionRolesWithAbility(Cultist.abilityType).isEmpty()){
                throw new IllegalGameSettingsException(
                        factionRole.faction.getName() + " needs a cultist like role to be added to the game.");
            }
        }
    }

    private static void executionerChecks(Game game) {
        if(!game.setup.hasSpawnableFactionRoleWithAbility(game, Executioner.abilityType))
            return;
        if(!game.setup.modifiers.getBoolean(SetupModifierName.EXECUTIONER_TO_JESTER, game))
            return;
        if(game.setup.hasFactionRoleWithAbility(Jester.abilityType))
            return;
        throw new IllegalGameSettingsException(
                "Jester faction role needs to be created with Executioner to Jester rule turned on.");
    }

    private static void sizeCheck(Game game) {
        if(game.players.size() < 3){
            String message = "You need at THE VERY least 3 players to start a mafia game.";
            throw new IllegalGameSettingsException(message);
        }
        if(game.players.size() > game.setup.rolesList.size(game))
            throw new IllegalGameSettingsException(
                    "You don't have enough roles to pass out to everyone in this setup.");
    }

    private static void playerNamesCheck(Game game) {
        Setup setup = game.setup;
        Set<String> playerNames = new HashSet<>(game.players.getNamesToStringList());

        Set<String> factionNames = FactionUtil.getNames(setup.getPossibleFactions(game));
        intersectionCheck(playerNames, factionNames, "faction");

        Set<String> roleNames = RoleUtil.getNamesSet(setup.roles);
        intersectionCheck(playerNames, roleNames, "role");

        Set<String> hiddenNames = HiddenUtil.getNames(setup.hiddens);
        intersectionCheck(playerNames, hiddenNames, "hidden");
    }

    private static void intersectionCheck(Set<String> playerNames, Set<String> entityNames, String label) {
        // retainAll is intersection
        entityNames.retainAll(playerNames);
        if(!entityNames.isEmpty()){
            String oneIntersection = entityNames.iterator().next();
            throw new NamingException(oneIntersection + "'s name + intersects with a " + label + " name.");
        }
    }

    /**
     * Checks if there are two opposing teams in the game
     *
     */
    public static void opponentCheck(Game game) {
        RolesList rolesList = game.setup.rolesList;
        for(SetupHidden setupHidden: rolesList.iterable(game)){
            for(SetupHidden setupHidden2: rolesList.iterable(game)){
                if(setupHidden == setupHidden2)
                    continue;
                for(FactionRole factionRole1: setupHidden.hidden.getAllFactionRoles()){
                    for(FactionRole factionRole2: setupHidden2.hidden.getAllFactionRoles()){
                        if(factionRole1.faction.enemies.contains(factionRole2.faction))
                            return;
                    }
                }
            }
        }
        throw new IllegalGameSettingsException("There is no conflict in this setup");
    }

    /**
     * Makes sure that if a sheriff is spawned, it has something to detect
     *
     */
    private static void sheriffChecks(Game game) {
        List<SetupHidden> rolesList = game.setup.rolesList.getSpawningSetupHiddens(game);
        Set<Hidden> okayHiddens = new HashSet<>();

        Hidden hidden;
        for(SetupHidden setupHidden: rolesList){
            hidden = setupHidden.hidden;
            if(!hidden.contains(Sheriff.abilityType))
                continue;
            if(okayHiddens.contains(hidden))
                continue;

            for(FactionRole factionRole: setupHidden.hidden.getSpawningFactionRoles(game)){
                for(SetupHidden setupHidden2: rolesList){
                    if(setupHidden2 == setupHidden)
                        continue;
                    if(sheriffDetectsAll(factionRole.faction, setupHidden2.hidden.getFactionColors())){
                        okayHiddens.add(hidden);
                        break;
                    }
                }
                if(okayHiddens.contains(hidden))
                    break;
            }
            if(!okayHiddens.contains(hidden))
                throw new IllegalGameSettingsException(
                        hidden.getName() + " spawns a Sheriff role that is unable to detect.");
        }
    }

    private static boolean sheriffDetectsAll(Faction faction, Set<String> teams) {
        Set<String> sheriffCheckables = new HashSet<>();
        for(Faction sheriffFaction: faction.sheriffCheckables)
            sheriffCheckables.add(sheriffFaction.color);
        return sheriffCheckables.containsAll(teams);
    }

    private static void invalidRulesCheck(Game game) {
        if(game.getBool(SetupModifierName.CULT_KEEPS_ROLES) && game.getBool(SetupModifierName.CULT_PROMOTION))
            throw new IllegalGameSettingsException(
                    "Invalid cult settings.  Recruits cannot keep roles and also be 'powered up'");

        if(game.getBool(SetupModifierName.DAY_START) == Game.NIGHT_START
                && 0 == game.getInt(GameModifierName.NIGHT_LENGTH))
            throw new IllegalGameSettingsException("Can't have night start with a nightless setup");

        for(Faction faction: game.setup.getPossibleFactions(game)){
            if(!faction.knowsTeam(game)){
                if(faction.hasSharedAbilities())
                    throw new IllegalGameSettingsException(
                            faction.getName() + " can kill but doesn't know their teammates");

                if(faction.hasNightChat(game))
                    throw new IllegalGameSettingsException(
                            faction.getName() + " has a night chat but doesn't know their teammates");
            }

            if(!faction.enemies.isEmpty() && (faction.hasPossibleRolesWithAbility(Ghost.abilityType, Jester.abilityType,
                    Executioner.abilityType))){
                throw new IllegalGameSettingsException(faction.getName()
                        + " cannot have enemies and have ghosts, jesters, or executioners as possible roles");
            }
        }
        boolean nightLess = game.getInt(GameModifierName.NIGHT_LENGTH) == 0;

        Faction faction;
        Set<String> seenColors = new HashSet<>();
        AbilityList abilities;
        Optional<Integer> playerCount = Optional.of(game.players.size());
        for(FactionRole factionRole: game.setup.getPossibleFactionRoles(game)){
            faction = factionRole.faction;
            abilities = new AbilityList(game, factionRole.role.abilityMap, factionRole.abilityModifiers);
            if(factionRole.role.hasAbility(CultLeader.abilityType) && !seenColors.contains(faction.getColor())){
                if(!faction.knowsTeam(game))
                    throw new IllegalGameSettingsException(
                            factionRole.getName() + " of faction " + faction.getName() + " must know teammates");
                seenColors.add(factionRole.faction.color);
            }
            if(!abilities.canStartWithEnemies() && !faction.enemies.isEmpty())
                throw new IllegalGameSettingsException(faction.getName()
                        + " cannot have enemies and have ghosts, jesters, or executioners as possible abilities");

            if(!nightLess)
                continue;

            if(!abilities.isNightless())
                throw new IllegalGameSettingsException("Illegal nightless ability in " + factionRole.getName());
            if(!faction.abilities.isEmpty()){
                for(Ability a: faction.abilities)
                    if(!a.getGameAbility().isNightless(playerCount))
                        throw new IllegalGameSettingsException(
                                "Illegal nightless ability: " + a.getClass().getSimpleName());
                if(faction.hasNightChat(game))
                    throw new IllegalGameSettingsException("No nightchats allowed in nightless: " + faction.getName());
            }

        }
    }

    private static void chatRoleChecks(Game game) {
        List<SetupHidden> rolesList = game.setup.rolesList.getSpawningSetupHiddens(game);
        Hidden hidden;
        for(SetupHidden setupHidden: rolesList){
            hidden = setupHidden.hidden;
            if(hidden.getSpawningFactionRoles(game).isEmpty())
                throw new IllegalRoleCombinationException(hidden.getName() + " cannot spawn any valid roles!");
        }
    }

    /**
     * Checks for invalid random roles that don't spawn more than 2 choices
     *
     */
    private static void randomChecks(Game narrator) {
        for(SetupHidden setupHidden: narrator.setup.rolesList.iterable(narrator))
            if(setupHidden.hidden.spawns.isEmpty())
                throw new IllegalRoleCombinationException(setupHidden.hidden.getName() + " cannot spawn any roles!");
    }

    public static int hasMultipleUniques(ArrayList<GeneratedRole> roleGroup, int playerCount) {
        Set<Role> seenUniqueRoles = new HashSet<>();
        Set<FactionRole> seenUniqueFactionRoles = new HashSet<>();
        Set<AbilityType> seenUniqueRoleAbilitie = new HashSet<>();
        Map<String, Set<AbilityType>> seenUniqueFactionAbilities = new HashMap<>();
        FactionRole factionRole;
        GeneratedRole generatedRole;
        AbilityType abilityType;
        String color;
        for(GeneratedRole element: roleGroup){
            generatedRole = element;
            factionRole = generatedRole.getFactionRole();

            if(factionRole.isRoleUnique(playerCount)){
                if(seenUniqueRoles.contains(factionRole.role))
                    return getIndexOfRole(roleGroup, factionRole.role);
                seenUniqueRoles.add(factionRole.role);
            }

            if(factionRole.isFactionUnique(playerCount)){
                if(seenUniqueFactionRoles.contains(factionRole))
                    return getIndexOfFactionRole(roleGroup, factionRole);
                seenUniqueFactionRoles.add(factionRole);
            }

            for(Ability ability: factionRole.role.abilityMap.values()){
                abilityType = ability.type;
                if(seenUniqueRoleAbilitie.contains(abilityType))
                    return getIndexOfRoleAbility(roleGroup, abilityType);
                if(ability.getGameAbility().isRoleUniqueAbility())
                    seenUniqueRoleAbilitie.add(abilityType);

                if(!ability.getGameAbility().isFactionUniqueAbility())
                    continue;

                color = factionRole.getColor();
                if(!seenUniqueFactionAbilities.containsKey(color))
                    seenUniqueFactionAbilities.put(color, new HashSet<>());
                if(seenUniqueFactionAbilities.get(color).contains(abilityType))
                    return getIndexOfRoleAbility(roleGroup, color, abilityType);
                seenUniqueFactionAbilities.get(color).add(abilityType);
            }
        }
        return -1;
    }

    private static int getIndexOfRoleAbility(ArrayList<GeneratedRole> roleGroup, AbilityType abilityType) {
        for(int i = 0; i < roleGroup.size(); i++)
            if(roleGroup.get(i).getFactionRole().role.hasAbility(abilityType))
                return i;
        Util.log("ability not found.  algorithm off.");
        return 0;
    }

    private static int getIndexOfRoleAbility(ArrayList<GeneratedRole> roleGroup, String color,
            AbilityType abilityType) {
        FactionRole factionRole;
        for(int i = 0; i < roleGroup.size(); i++){
            factionRole = roleGroup.get(i).getFactionRole();
            if(factionRole.getColor().equals(color) && factionRole.role.hasAbility(abilityType))
                return i;
        }
        Util.log("ability not found.  algorithm off.");
        return 0;
    }

    private static int getIndexOfRole(List<GeneratedRole> roleGroup, Role role) {
        for(int i = 0; i < roleGroup.size(); i++)
            if(roleGroup.get(i).getFactionRole().role == role)
                return i;
        Util.log("role not found.  algorithm off.");
        return 0;
    }

    private static int getIndexOfFactionRole(List<GeneratedRole> roleGroup, FactionRole factionRole) {
        for(int i = 0; i < roleGroup.size(); i++)
            if(roleGroup.get(i).getFactionRole() == factionRole)
                return i;
        Util.log("faction role not found.  algorithm off.");
        return 0;
    }

    public static int hasSingleMason(int playerCount, ArrayList<GeneratedRole> roleGroup,
            List<GeneratedRole> thiefRoles) {
        if(playerCount < roleGroup.size())
            return -1;
        Map<String, Integer> indexWithSingleMason = new HashMap<>();
        FactionRole factionRole;
        String factionColor;
        for(int i = 0; i < roleGroup.size(); i++){
            factionRole = roleGroup.get(i).getFactionRole();
            if(!Mason.IsMasonType(factionRole.role))
                continue;

            factionColor = factionRole.getColor();
            if(indexWithSingleMason.containsKey(factionColor))
                indexWithSingleMason.put(factionColor, null);
            else if(factionRole.role.is(Mason.abilityType))
                indexWithSingleMason.put(factionColor, i);
            else
                indexWithSingleMason.put(factionColor, null);
        }

        String willSpawnMasonColor = getThiefMasonColor(thiefRoles);
        if(willSpawnMasonColor != null)
            indexWithSingleMason.remove(willSpawnMasonColor);

        int index = -1;
        for(Integer singleIndexMason: indexWithSingleMason.values()){
            if(singleIndexMason != null)
                index = Math.max(index, singleIndexMason);
        }
        return index;
    }

    private static String getThiefMasonColor(List<GeneratedRole> thiefRoles) {
        String masonColor = null, roleColor;
        for(GeneratedRole generatedRole: thiefRoles){
            if(!Mason.IsMasonType(generatedRole.getFactionRole().role))
                return null;
            roleColor = generatedRole.getFactionRole().getColor();
            if(masonColor == null)
                masonColor = roleColor;
            if(!masonColor.equals(roleColor))
                return null;
        }
        return masonColor;
    }

    public static int hasThiefWithNotEnoughChoices(int playerCount, ArrayList<GeneratedRole> roleGroup,
            List<GeneratedRole> thiefRoles) {
        for(int i = 0; i < roleGroup.size(); i++){
            if(!roleGroup.get(i).getFactionRole().role.hasAbility(Thief.abilityType))
                continue;
            if(playerCount == roleGroup.size()){
                if(thiefRoles.size() >= 2)
                    break;
            }else if(playerCount + 2 <= roleGroup.size())
                break;

            return i;

        }
        return -1;
    }

    public static int allMustSpawnedRolesSpawned(List<GeneratedRole> thiefRoles) {
        for(int i = 0; i < thiefRoles.size(); i++){
            if(thiefRoles.get(i).setupHidden.mustSpawn)
                return i;
        }
        return -1;
    }

}

package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.FactionRole;
import models.LobbySetupSchema;
import models.SetupHidden;
import models.dbo.SetupDbo;
import models.idtypes.GameID;
import models.schemas.SetupHiddenSchema;
import models.serviceResponse.SetupHiddenAddResponse;
import nnode.Permission;
import repositories.SetupHiddenRepo;
import repositories.UserPermissionRepo;
import util.Util;
import util.game.LookupUtil;

public class SetupHiddenService {

    public static SetupHiddenAddResponse addSetupHidden(long userID, long hiddenID, boolean isExposed,
            boolean mustSpawn, int minPlayerCount, int maxPlayerCount) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);

        Game game = GameService.getGame(lobbySetup.game);
        Setup setup = game.setup;
        int rolesListSize = setup.rolesList.size(game);

        if(rolesListSize > 15 && !UserPermissionRepo.hasPermission(userID, Permission.GAME_EDITING))
            throw new NarratorException("You may not add any more roles in a public game.");

        Hidden hidden = setup.getHidden(hiddenID);
        long setupHiddenID = SetupHiddenRepo.create(setup.id, hidden.id, isExposed, mustSpawn, minPlayerCount,
                maxPlayerCount);
        SetupHiddenSchema schema = new SetupHiddenSchema(setupHiddenID, hiddenID, isExposed, mustSpawn, minPlayerCount,
                maxPlayerCount);
        SetupHidden setupHidden = addSetupHidden(hidden, schema);
        return new SetupHiddenAddResponse(setupHidden, lobbySetup.game.id, lobbySetup.game.lobbyID);
    }

    public static SetupHidden addSetupHidden(Hidden hidden, boolean isExposed, boolean mustSpawn, int minPlayerCount,
            int maxPlayerCount) {
        return addSetupHidden(hidden,
                new SetupHiddenSchema(Util.getID(), hidden.id, isExposed, mustSpawn, minPlayerCount, maxPlayerCount));
    }

    public static SetupHidden addSetupHidden(Hidden hidden, SetupHiddenSchema schema) {
        SetupHidden setupHidden = new SetupHidden(hidden, schema);
        hidden.setup.rolesList._setupHiddenList.add(setupHidden);
        return setupHidden;
    }

    public static void loadSetupHiddens(Setup setup, SetupDbo setupDbo) {
        ArrayList<SetupHiddenSchema> schemas = setupDbo.setupHiddenDbos;
        for(SetupHiddenSchema schema: schemas)
            addSetupHidden(setup.getHidden(schema.hiddenID), schema);
    }

    public static Hidden removeSetupHiddenByHiddenID(long setupID, Game narrator, long hiddenID) throws SQLException {
        Hidden markedSetupHidden = LookupUtil.findHidden(narrator.setup, hiddenID);
        if(markedSetupHidden == null)
            throw new NarratorException("Could not find hidden with that id in the setup.");

        SetupHiddenRepo.delete(setupID, markedSetupHidden.id);
        narrator.removeSetupHidden(markedSetupHidden);
        return markedSetupHidden;
    }

    public static GameID deleteSetupHidden(long userID, long setupHiddenID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        SetupHiddenRepo.delete(setupHiddenID, lobbySetup.setup.id);
        return lobbySetup.game.id;
    }

    public static void deleteSetupHidden(Game game, SetupHidden setupHidden) {
        game.setup.rolesList.remove(setupHidden);
    }

    public static void setForcedSpawn(Game game, long setupHiddenID, long factionRoleID) {
        SetupHidden setupHidden = game.setup.rolesList.get(setupHiddenID);
        FactionRole spawn = game.setup.getFactionRole(factionRoleID);

        setForcedSpawn(game, setupHidden, spawn);
    }

    public static void setForcedSpawn(Game game, SetupHidden setupHidden, FactionRole spawn) {
        Setup setup = setupHidden.hidden.setup;
        Set<FactionRole> spawns = setup.rolesList.getSpawns(game.players.size());
        int playerCount = game.players.size();
        if(spawn.isFactionUnique(playerCount) && spawns.contains(spawn))
            throw new NarratorException("This faction unique role is already being spawned.");
        if(spawn.isRoleUnique(playerCount))
            for(FactionRole factionRole: spawns){
                if(spawn.role == factionRole.role)
                    throw new NarratorException("This role unique role is already being spawned.");
            }
        setupHidden.spawn = spawn;
    }

    public static void removeForcedSpawn(SetupHidden setupHidden) {
        setupHidden.spawn = null;
    }

    public static void setExposed(long userID, long setupHiddenID, boolean isExposed) throws SQLException {
        SetupService.assertModeratorAndOwner(userID);
        SetupHiddenRepo.setExposed(setupHiddenID, isExposed);
    }

    public static void setExposed(SetupHidden setupHidden) {
        setupHidden.isExposed = true;
    }
}

DB_USERNAME=$1
DB_PASSWORD=$2
NARRATOR_ENVIRONMENT=$3
FIREBASE_TOKEN=$4
DISCORD_MASTER_KEY=$5
SC2MAFIA_API_KEY=$6;
SC2MAFIA_PASSWORD=$7;
CI_COMMIT_REF_NAME=$8;

. scripts/create_config.sh

curl https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/6.5.5/flyway-commandline-6.5.5-linux-x64.tar.gz -o flyaway.tar.gz
tar -xvzf flyaway.tar.gz > /dev/null 2>&1
rm flyaway.tar.gz
./flyway-6.5.5/flyway migrate -user=$DB_USERNAME -password=$DB_PASSWORD -url=jdbc:mariadb://$DB_HOSTNAME/$DB_NAME -locations=filesystem:src
rm -rf flyway*

echo 'Compiling Java files.'
sh scripts/compile.sh

sudo iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 4501
sudo iptables -t nat -I PREROUTING -p tcp --dport 443 -j REDIRECT --to-port 4501

rm -rf package-lock.json
npm install

ln -s /home/voss/"$CI_COMMIT_REF_NAME"/ui/public /home/voss/"$CI_COMMIT_REF_NAME"/back-end/public

